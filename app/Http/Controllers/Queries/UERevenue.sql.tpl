SELECT
    inv.id_invoice,
    c.id_customer,
    inv.inv_code,
    DATE_FORMAT(inv.inv_date, '%%d/%%m/%%Y') as inv_date,
    DATE_FORMAT(inv.expired_date, '%%d/%%m/%%Y') as expired_date,
    o.outlet_name,
    c.induk_customer,
    c.full_name,
    pt.product_treatment_code,
    pt.product_treatment_name,
    FORMAT(invpkg.qty + invpkg.used, 0) as paid_qty,
    FORMAT((invpkg.qty + invpkg.used) * invpkg.nominal, 0) as paid_amount,
    invpkg.used,
    invpkg.qty,
    FORMAT(invpkg.used * invpkg.nominal, 0) as earned,
    FORMAT(invpkg.qty * invpkg.nominal, 0) as unearned
FROM
    invoice_packages invpkg
        JOIN
    invoice_details invdet ON invpkg.inv_detail_id = invdet.id_inv_detail
        JOIN
    invoices inv ON invdet.inv_id = inv.id_invoice
        JOIN
    outlets o ON inv.outlet_id = o.id_outlet
        JOIN
    customers c ON inv.customer_id = c.id_customer
    join product_treatments pt on invpkg.product_id = pt.id_product_treatment
WHERE
    inv.outlet_id IN (%s)
        AND inv.void_invoice = 0
        AND inv.inv_date BETWEEN ? AND ?
SELECT 
    inv.id_invoice,
    o.outlet_name,
	inv.inv_code,
    DATE_FORMAT(inv.inv_date, '%%d/%%m/%%Y') as inv_date,
    inv.customer_id,
    c.induk_customer,
    c.full_name,
    c.phone,
    c.birth_date,
    c.address,
    u.username AS received_by,
    ms.marketing_source_name,
    ac.activity_name,
    cs.consultant_name,
    t.therapist_name,
    FORMAT(invdet.discount_1, 2) as discount_1,
    FORMAT(invdet.discount_2, 2) as discount_2,
	if (invdet.type_line = 0, '%%', 'Rp') as type_line,
    CONCAT(IFNULL(pt.product_treatment_name, '')) as item_name,
    CONCAT(IFNULL(pt.product_treatment_code, '')) as item_sku,
    (invpkg.qty + invpkg.used) as qty,
    FORMAT((invpkg.qty + invpkg.used) * invpkg.nominal, 0) as total,
    inv.remarks
FROM
    invoices inv
        JOIN
    invoice_details invdet ON inv.id_invoice = invdet.inv_id
        JOIN
    invoice_packages invpkg ON invdet.id_inv_detail = invpkg.inv_detail_id
        LEFT JOIN
    product_treatments pt ON invpkg.product_id = pt.id_product_treatment
        JOIN
    customers c ON inv.customer_id = c.id_customer
        JOIN
    outlets o ON inv.outlet_id = o.id_outlet
        LEFT JOIN
    consultants cs ON inv.consultant_id = cs.id_consultant
        LEFT JOIN
    therapists t ON inv.therapist_id = t.id_therapist
        LEFT JOIN
    marketing_sources ms ON inv.marketing_source_id = ms.id_marketing_source
    LEFT JOIN activities ac ON invdet.activity_id = ac.id_activity
        JOIN
    users u ON inv.staff_id = u.id
WHERE
    inv.outlet_id IN (%s)
        AND void_invoice = 0
        AND invdet.package_code IN ("%s")
        AND inv.inv_date BETWEEN ? AND ?;
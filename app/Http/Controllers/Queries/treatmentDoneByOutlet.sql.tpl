SELECT 
    ud.id_usage_detail,
    u.id_usage,
    u.usage_code,
    DATE_FORMAT(u.usage_date, "%%d/%%m/%%Y") as usage_date,
    inv.id_invoice,
    inv.inv_code,
    DATE_FORMAT(inv.inv_date, "%%d/%%m/%%Y") as inv_date,
    DATE_FORMAT(u.created_at, "%%H:%%i:%%s") as usage_time,
    o.outlet_name,
    c.induk_customer,
    u.customer_id,
    c.full_name,
    YEAR(NOW()) - YEAR(c.birth_date) AS age_in_year,
    c.gender,
    c.phone,
    c.address,
    usr.username,
    pt.product_treatment_code,
    pt.product_treatment_name,
    ud.value * ud.used AS used_value,
    ud.used as used_qty,
    ud.value * ud.qty AS balance_value,
    ud.qty as balance_qty,
    ud.duration,
    ud.point,
    JSON_ARRAYAGG(t.therapist_name) AS therapists,
    u.remarks
FROM
    usage_details ud
        JOIN
    usages u ON ud.usage_id = u.id_usage
        JOIN
    customers c ON u.customer_id = c.id_customer
        JOIN
    outlets o ON u.outlet_id = o.id_outlet
        JOIN
    invoices inv ON ud.inv_code = inv.inv_code
        JOIN
    users usr ON u.staff_id = usr.id
        JOIN
    invoice_packages ip ON ud.invoice_package_id = ip.id_invoice_package
        JOIN
    product_treatments pt ON ud.product_id = pt.id_product_treatment
        AND SUBSTRING(pt.product_treatment_code,
        1,
        1) = 'T'
        JOIN
    usage_therapists ut ON ud.id_usage_detail = ut.usage_detail_id
        JOIN
    therapists t ON ut.therapist_id = t.id_therapist
WHERE
    u.status_usage = 0
        AND u.outlet_id IN (%s)
        AND u.usage_date BETWEEN ? AND ?
GROUP BY ud.id_usage_detail
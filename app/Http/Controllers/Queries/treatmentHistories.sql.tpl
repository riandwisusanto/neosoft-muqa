select 
  u.id_usage,
  ud.id_usage_detail,
  u.usage_code,
  u.usage_date,
  ud.used,
  json_arrayagg(t.therapist_name) as therapists 
from 
  usage_details ud
  join usage_therapists ut on ud.id_usage_detail = ut.usage_detail_id 
  join therapists t on ut.therapist_id = t.id_therapist
  join product_treatments pt on pt.id_product_treatment = ud.product_id
  join usages u on u.id_usage = ud.usage_id
  join invoice_packages ip on ip.id_invoice_package = ud.invoice_package_id
where
  ip.id_invoice_package = ?
group by 
 ud.id_usage_detail
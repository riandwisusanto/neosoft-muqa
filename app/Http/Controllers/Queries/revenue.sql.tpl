SELECT
    SUM(ud.value * ud.used) AS revenue
FROM
    usage_details ud
    join usages u on u.id_usage = ud.usage_id
WHERE
	u.outlet_id IN (%s)
  	AND u.usage_date BETWEEN '%s' AND '%s'
	AND u.status_usage = 0
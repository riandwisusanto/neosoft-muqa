select MONTHNAME(activities.visit_date) as visit_month, COUNT(activities.visit_date) as total_visit from 
(
	select distinct usage_date as visit_date from usages where customer_id = ? union
	select inv_date from invoices where customer_id = ?
) activities
GROUP BY visit_month
ORDER BY visit_month ASC
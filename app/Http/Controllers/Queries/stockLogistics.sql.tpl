SELECT
    i.id,
    i.name,
    SUM(IF(m.transdate < :froma, m.qty, 0)) AS stock_begin,
    SUM(IF(m.transtype = 'in' AND m.adjustment_id IS NULL AND m.transdate BETWEEN :fromb AND :toa, m.qty, 0)) as stock_in,
    SUM(IF(m.transtype = 'out' AND m.adjustment_id IS NULL AND u.status_usage = 0 AND m.transdate BETWEEN :fromc AND :tob, m.qty, 0) * -1) as stock_out,
    SUM(IF(m.transtype = 'in' AND m.adjustment_id IS NOT NULL AND m.transdate BETWEEN :fromd AND :toc, m.qty, 0)) as stock_adj_in,
    SUM(IF(m.transtype = 'out' AND m.adjustment_id IS NOT NULL AND m.transdate BETWEEN :frome AND :tod, m.qty, 0) * -1) as stock_adj_out,
    SUM(IF(m.transdate <= :toe, m.qty, 0)) as stock_result
FROM
    items i
LEFT JOIN
    mutations m ON i.id = m.item_id
    AND (m.warehouse_origin_id = :wr OR m.warehouse_destination_id = :wra)
LEFT JOIN usages u on m.sales_invoice_id = u.id_usage
WHERE
    i.type = 'product'
GROUP BY m.item_id
ORDER BY i.name ASC;

SELECT 
    u.id_usage,
    u.usage_code,
    DATE_FORMAT(u.usage_date, "%%d/%%m/%%Y") as usage_date,
    inv.inv_code,
    DATE_FORMAT(inv.inv_date, "%%d/%%m/%%Y") as inv_date,
    DATE_FORMAT(u.created_at, "%%H:%%i:%%s") as usage_time,
    inv.id_invoice,
    o.outlet_name,
    u.customer_id,
    c.induk_customer,
    c.full_name,
    DATE_FORMAT(c.birth_date, "%%d/%%m/%%Y") as birth_date,
    c.phone,
    c.address,
    c.gender,
    us.username,
    pt.product_treatment_code,
    pt.product_treatment_name,
    (ud.value * ud.used) as used_value,
    ud.used as used_qty,
    (ud.value * ud.qty) as balance_value,
    ud.qty as balance_qty,
    ud.duration,
    ud.point,
    u.remarks,
    pv.province
FROM
    usages u
    join usage_details ud on u.id_usage = ud.usage_id
    join product_treatments pt on ud.product_id = pt.id_product_treatment AND substring(pt.product_treatment_code, 1, 1) = 'P' 
    join customers c on u.customer_id = c.id_customer
    join invoices inv on inv.inv_code = ud.inv_code
    join outlets o on o.id_outlet = u.outlet_id
    join users us on us.id = u.staff_id
    join provinces pv on c.city_id = pv.id_province
WHERE
    u.usage_date BETWEEN ? AND ?
        AND u.status_usage = 0
        AND o.id_outlet IN (%s)
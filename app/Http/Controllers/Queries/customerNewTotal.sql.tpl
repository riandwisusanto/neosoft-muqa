SELECT 
    SUM(IF(purchases_period.row_of_existing_patients > 0,
        1,
        0)) AS existing_patients,
    SUM(IF(purchases_period.row_of_new_patients > 0,
        1,
        0)) AS new_patients
FROM
    (SELECT 
        SUM(CASE
                WHEN c.join_date < :from_1 THEN 1
                ELSE 0
            END) AS row_of_existing_patients,
            SUM(CASE
                WHEN c.join_date >= :from_2 THEN 1
                ELSE 0
            END) AS row_of_new_patients
    FROM
        customers c
    JOIN invoices inv ON c.id_customer = inv.customer_id
    JOIN customer_outlets co ON c.id_customer = co.customer_id
    WHERE
        inv.void_invoice = 0
            AND co.outlet_id IN (%s)
            AND inv.inv_date BETWEEN :from_3 AND :to_1
    GROUP BY inv.customer_id) purchases_period
SELECT
    ot.outlet_name,
	SUM(pa.amount) as collection
FROM
	invoices inv
    INNER JOIN outlets ot ON ot.id_outlet = inv.outlet_id
	join payments pa ON pa.inv_id = inv.id_invoice

WHERE
	inv.outlet_id IN (%s)
    AND inv.inv_date BETWEEN '%s' AND '%s'
	AND inv.void_invoice = 0
GROUP BY
	inv.outlet_id
ORDER BY
	ot.id_outlet ASC
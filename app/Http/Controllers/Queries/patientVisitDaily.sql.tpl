select dayofmonth(activities.visit_date) visit_day from 
(
	select distinct usage_date as visit_date from usages where customer_id = ? and usage_date BETWEEN ? AND ? UNION
	select inv_date from invoices where customer_id = ? and inv_date BETWEEN ? AND ?
) activities
SELECT 
    inv.id_invoice,
    o.outlet_name,
	inv.inv_code,
    DATE_FORMAT(inv.inv_date, '%%d/%%m/%%Y') as inv_date,
    c.induk_customer,
    c.full_name,
    u.username AS received_by,
    cs.consultant_name,
    t.therapist_name,
    bk.bank_name,
    pay.izzibook_sync_status,
    FORMAT(pay.amount, 0) as amount
FROM
    invoices inv
        JOIN 
    payments pay ON inv.id_invoice = pay.inv_id
        LEFT JOIN 
    banks bk ON bk.id_bank = pay.bank_id
        JOIN 
    users u ON inv.staff_id = u.id
        JOIN
    customers c ON inv.customer_id = c.id_customer
        JOIN
    outlets o ON inv.outlet_id = o.id_outlet
        LEFT JOIN
    consultants cs ON inv.consultant_id = cs.id_consultant
        LEFT JOIN
    therapists t ON inv.therapist_id = t.id_therapist
    
WHERE 
        void_invoice = 0
        AND pay.bank_id IN (%s)
        AND inv.inv_date BETWEEN ? AND ?;
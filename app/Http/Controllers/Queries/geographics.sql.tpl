SELECT 
  p.id_province as city_id, 
  p.province as city_name, 
  COUNT(sales.id_customer) as num_of_customers, 
  SUM(sales.num_of_inv) as num_of_inv, 
  SUM(sales.num_of_items) as num_of_items, 
  SUM(sales.invoice_total) as invoice_total 
FROM 
  provinces p 
  LEFT JOIN (
    SELECT 
      c.id_customer, 
      c.city_id, 
      summary_inv.num_of_inv, 
      summary_inv.num_of_items, 
      summary_inv.invoice_total 
    FROM 
      customers c 
      LEFT JOIN (
        SELECT 
          inv.customer_id, 
          COUNT(inv.inv_code) as num_of_inv, 
          SUM(invdet.num_of_items) as num_of_items, 
          SUM(pa.amount) as invoice_total 
        FROM 
          invoices inv 
          join payments pa ON pa.inv_id = inv.id_invoice
          LEFT JOIN (
            SELECT 
              inv_id, 
              id_inv_detail,
              SUM(ip.sum_qtyused) as num_of_items 
            FROM 
              invoice_details id
              LEFT JOIN (
            	SELECT
                  inv_detail_id,
                  SUM(qty + used) as sum_qtyused
                FROM
                  invoice_packages
                GROUP BY
                  inv_detail_id
              ) ip ON ip.inv_detail_id = id.id_inv_detail
            GROUP BY 
              inv_id
          ) invdet ON inv.id_invoice = invdet.inv_id 
        WHERE 
          inv.void_invoice = 0 
        GROUP BY 
          inv.customer_id
      ) summary_inv ON summary_inv.customer_id = c.id_customer 
      INNER JOIN customer_outlets co ON co.customer_id = c.id_customer 
    WHERE 
      co.outlet_id IN (%s) 
      AND c.join_date BETWEEN '%s' AND '%s' 
    GROUP BY 
      c.id_customer
  ) sales ON p.id_province = sales.city_id 
GROUP BY 
  sales.city_id 
HAVING 
  num_of_customers > 0  
ORDER BY `invoice_total`  DESC
LIMIT 10
SELECT
	SUM(pa.amount) as collection
FROM
	invoices inv
	join payments pa ON pa.inv_id = inv.id_invoice

WHERE
	outlet_id IN (%s)
    AND inv_date BETWEEN '%s' AND '%s'
	AND void_invoice = 0
<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Agent;
use App\Models\Bank;
use App\Models\Consultant;
use App\Models\Invoice;
use App\Models\InvoiceDetail;
use App\Models\InvoicePackage;
use App\Models\LogInvoice;
use App\Models\MarketingSource;
use App\Models\MedicalAlert;
use App\Models\Outlet;
use App\Models\UserOutlet;
use App\Models\Package;
use App\Models\Logistics\Item;
use App\Models\Payment;
use App\Models\Customer;
use App\Models\ProductTreatment;
use App\Models\Therapist;
use App\Models\Anamnesa;
use App\Models\AnamnesaDetail;
use Auth;
use App\Models\Usage;
use App\Models\UsageDetail;
use App\Models\UsageTherapist;
use App\Models\Logistics\Mutation;
use App\Models\Logistics\InventoryAdjustment;
use App\Models\LogUsage;
use App\Models\ProductTreatmentPoint;
use App\Models\Warehouse;
use App\Models\WarehouseOutlet;
use Carbon\carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use KrmPesan\Client;
use App\Models\Queue;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Services\InvoiceService;
use App\Services\PaymentService;
use App\Models\ActivityProductTreatment;

class InvoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('sales') || Gate::allows('therapist')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $warehouse =  Warehouse::whereHas('warehouseOutlets', function ($q) use ($outlet_id) {
            $q->whereIn('outlet_id', $outlet_id);
        })->select('id_warehouse', 'name')->get();

        $package = Package::where('status_package', 1)
            ->whereHas('package_outlet', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();

        $product = ProductTreatment::where('status_product_treatment', 1)
            ->whereHas('product_treatment_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        $allItem = $package->concat($product);
        $count = count($allItem);

        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $bank = Bank::where('status_bank', 1)
            ->whereHas('bank_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        $consultant = Consultant::where('status_consultant', 1)
            ->whereHas('consultant_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        $marketing_source = MarketingSource::where('status_marketing_source', 1)
            ->whereHas('marketing_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        $therapist = Therapist::where('status_therapist', 1)
            ->whereHas('therapist_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();

        $activity = Activity::where('status_activity', 1)
            ->where('end_date', '>=', now()->toDateString())
            ->where('start_date', '<=', now()->toDateString())
            ->whereHas('activity_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })
            ->get();

        $agent = Agent::where('status', 0)->whereNotNull('uniq_code')->get();

        return view('invoice.create.index')
            ->with('outlet', $outlet)
            ->with('warehouse', $warehouse)
            ->with('consultant', $consultant)
            ->with('marketing_source', $marketing_source)
            ->with('activity', $activity)
            ->with('therapist', $therapist)
            ->with('package', $allItem)
            ->with('count', $count)
            ->with('agent', $agent)
            ->with('bank', $bank);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $totalProductTreatmentPoint = 0;
        $unpaid = str_replace(array('.', ','), "", $request->unpaid);
        if ($unpaid > 0) {
            return response()->json(['message' => 'sistem mendeteksi kesalahan browser mengirim nilai outstanding lebih dari 0, mohon untuk refresh halaman'], 422);
        }
        $getRestraction = MedicalAlert::where('customer_id', $request->customer_id)->pluck('product_id')->toArray();
        $save_json = json_decode($request->save_json);
        $shouldDelete = [];
        foreach ($save_json as $key => $data) {
            $hasBonusPoint = in_array($data->activity, ['dp appointment', 'Harga Normal']);
            if (str_replace(array(','), "", $data->discount_1) <= 0 && str_replace(array(','), "", $data->discount_2) <= 0 && $hasBonusPoint) {
                if ($data->package_id) {
                    $package = Package::find($data->package_id);
                    $getIdPackage = [];
                    foreach ($package->package_detail as $set) {
                        $getIdPackage[] = $set->product_id;
                        $productTreatmentPoint = ProductTreatmentPoint::where('product_treatment_id', $set->product_id)->latest('id_product_treatment_points')->first();
                        if ($productTreatmentPoint) {
                            $totalProductTreatmentPoint += ((int) $productTreatmentPoint->point) * (int) $set->qty;
                        }
                    }
                    $shouldDelete = array_intersect($getRestraction, $getIdPackage);
                } else {
                    $productTreatmentPoint = ProductTreatmentPoint::where('product_treatment_id', $data->productTreatment_id)->latest('id_product_treatment_points')->first();
                    if ($productTreatmentPoint) {
                        $totalProductTreatmentPoint += ((int) $productTreatmentPoint->point) * (int) $data->qty;
                    }
                }
            }
        }
        $discount_inv = false;
        if (count($shouldDelete)) {
            echo "error";
        } else {
            DB::beginTransaction();
            try {
                $outlet = Outlet::first();
                $store_invoice = new Invoice;

                if ($request->outlet) {
                    $store_invoice->outlet_id = $request->outlet;
                    $generateCode = $request->outlet;
                } else {
                    $store_invoice->outlet_id = $outlet->id_outlet;
                    $generateCode = $outlet->id_outlet;
                }
                $store_invoice->inv_code = Invoice::generateInvoiceCode($generateCode);
                $store_invoice->inv_date = Carbon::createFromFormat('d/m/Y', $request->date_invoice)->format('Y-m-d');
                $store_invoice->expired_date = Carbon::createFromFormat('d/m/Y', $request->date_expired)->format('Y-m-d');
                $store_invoice->customer_id = $request->customer_id;
                $store_invoice->ongkir = $request->ongkir;

                $therapist = Therapist::where('therapist_name', "LIKE", '%NONE%')->first();
                $consultant = Consultant::where('consultant_name', "LIKE", '%NONE%')->first();
                $marketingSource = MarketingSource::where('marketing_source_name', "LIKE", '%NONE%')->first();

                if ($request->therapist) {
                    $store_invoice->therapist_id = $request->therapist;
                } else {
                    $store_invoice->therapist_id = $therapist->id_therapist;
                }

                if ($request->consultant) {
                    $store_invoice->consultant_id = $request->consultant;
                } else {
                    $store_invoice->consultant_id = $consultant->id_consultant;
                }

                if ($request->marketing_source) {
                    $store_invoice->marketing_source_id = $request->marketing_source;
                } else {
                    $store_invoice->marketing_source_id = $marketingSource->id_marketing_source;
                }

                $store_invoice->remarks = $request->remarks;
                $store_invoice->inv_remarks = $request->old_invoice;
                $store_invoice->total = str_replace(array('.', ','), "", $request->total);
                $store_invoice->staff_id = Auth::user()->id;
                $store_invoice->warehouse_id = $request->warehouse_id;
                $store_invoice->agent_id = $request->agent;
                $store_invoice->remaining = str_replace(array('.', ','), "", $request->unpaid);
                $store_invoice->discount = $request->discount;
                $store_invoice->type = $request->type;
                $store_invoice->point =  0;

                if ($request->kembali > 0) {
                    $store_invoice->remaining = 0;
                } else {
                    $store_invoice->remaining = str_replace(array('.', ','), "", $request->unpaid);
                }

                if ($request->kembali > 0) {
                    $store_invoice->status_inv = 1;
                } else if ($request->unpaid == 0) {
                    $store_invoice->status_inv = 1;
                }

                //cek activity if customer dan activity sama tidak bisa dipakai

                $idAct = $request->activity_id;
                $activity = Activity::where('id_activity', $idAct)->where('discount_1', '>', 0)->first();
                $inv = $store_invoice->whereHas('invoice_detail', function ($q) use ($idAct) {
                    $q->where('activity_id', $idAct);
                })->where('customer_id', $request->customer_id);

                $msg = "";
                if (isset($activity->reusable_activity)) {
                    if ($activity->reusable_activity == 1) {
                        $inv->whereMonth('created_at', Carbon::now()->month);
                        $msg = "hanya bisa dipakai 1 bulan 1x";
                    } elseif ($activity->reusable_activity == 2) {
                        $inv->whereYear('created_at', Carbon::now()->year);
                        $msg = "hanya bisa dipakai 1 tahun 1x";
                    } else {
                        $msg = "sudah pernah digunakan pasien";
                    }
                    $invs = $inv->get();
                    if (count($invs) > 0) {
                        return response()->json([
                            'status'   => 'error',
                            'message'   => 'Promo ' . $activity->activity_name . " " . $msg
                        ], 500);
                    }
                }

                $store_invoice->save();

                // initialization new invoice
                $invoice = Invoice::find($store_invoice->id_invoice);

                $immediates = [];
                $invoiceDetails = [];
                $invoicePackages = [];
                $totalForPoint = 0;
                $save_json = json_decode($request->save_json);
                foreach ($save_json as $key => $data) {
                    $subtotal = str_replace(array('.', ','), "", $data->subtotal);
                    $save = new InvoiceDetail;
                    $save->inv_id = $store_invoice->id_invoice;
                    $save->package_code = $data->code;
                    $save->current_price = $data->price;
                    $save->qty = $data->qty;
                    $save->notes = isset($data->notes) ? $data->notes : null;
                    if ($request->discount) {
                        $discount_inv = true;
                        if ($store_invoice->type == 0) {
                            $sum = $store_invoice->discount + str_replace(array('.', ','), "", $store_invoice->total);
                            $sub = ($store_invoice->discount / $sum)  * 100;
                            $tot = $subtotal - ($sub / 100) * $subtotal;
                            $save->subtotal = $tot;
                            //    $totalForPoint += $tot;
                        } else {
                            $tot = $subtotal - ($store_invoice->discount / 100) * $subtotal;
                            $save->subtotal = $tot;
                            //    $totalForPoint += $tot;
                        }
                    } else {
                        $save->subtotal = $subtotal;
                        if (strtolower($data->activity) == 'dp appointment') {
                            $totalForPoint += $subtotal;
                        } else if (str_replace(array(','), "", $data->discount_1) > 0 || str_replace(array(','), "", $data->discount_2) > 0) {
                            $totalForPoint += 0;
                        } else {
                            $totalForPoint += $subtotal;
                        }
                    }
                    //    if (str_replace(array(','), "", $data->discount_1) > 0) $discount_inv = true;

                    $save->discount_1 = str_replace(array(','), "", $data->discount_1);
                    if ($data->discount_2) {
                        $save->discount_2 = str_replace(array(','), "", $data->discount_2);
                    }
                    $save->anamnesa_detail_id = isset($data->anamnesa_detail_id) ? $data->anamnesa_detail_id : null;
                    $save->type_line = $data->type_line_id;

                    $activity = Activity::where('activity_name', 'LIKE', '%Harga Normal%')->first();

                    if ($data->activity_id) {
                        $save->activity_id = $data->activity_id;
                    } else {
                        $save->activity_id = $activity->id_activity ?? 15;
                    }
                    if ($data->immediate_usage) {
                        $invoiceDetails[] = $save;
                        $save->immediate_usage = 1;
                    }

                    $save->save();

                    if (isset($data->anamnesa_detail_id)) {
                        $anamnesaDetail = AnamnesaDetail::find($data->anamnesa_detail_id);
                        $anamnesaDetail->is_paid = 1;
                        $anamnesaDetail->save();
                    }

                    Log::info('immediate_usage', [$data->immediate_usage]);



                    if ($data->package_id) {
                        $package = Package::find($data->package_id);

                        //    if(strtolower($data->activity) == 'dp appointment')
                        //    {
                        //        $totalForPoint += remove_format_money($package->price);
                        //    }else if(str_replace(array(','), "", $data->discount_1) > 0 || str_replace(array(','), "", $data->discount_2) > 0)
                        //    {
                        //        $totalForPoint += 0;
                        //    }else{
                        //        $totalForPoint += remove_format_money($package->price);
                        //    }

                        foreach ($package->package_detail as $set) {
                            $save_package = new InvoicePackage;
                            $save_package->inv_detail_id = $save->id_inv_detail;
                            $save_package->product_id = $set->product_id;
                            $save_package->qty = $set->qty * $data->qty;
                            $save_package->used = 0;

                            $item = Item::where('sales_information_id', $set->product_id)->first();


                            $totalx = $set->price * $data->qty;

                            if ($data->discount_2) {
                                if ($data->type_line_id == 0) {
                                    $total1 = ($totalx - (($data->discount_1 / 100) * $totalx)) / ($set->qty * $data->qty);
                                    $grandTotal = ($total1 - (($data->discount_2 / 100) * $total1));
                                } else {
                                    $total1 = ($totalx - $data->discount_1) / ($set->qty * $data->qty);
                                    $grandTotal = $total1 - $data->discount_2;
                                }
                            } else {
                                //rp = 1, % = 0
                                if ($data->type_line_id == 0) {
                                    if ($data->discount_1 > 0) {
                                        $grandTotal = ($totalx - (($data->discount_1 / 100) * $totalx)) / ($set->qty * $data->qty);
                                    } else {
                                        $grandTotal = $totalx / ($set->qty * $data->qty);
                                    }
                                } else {
                                    $grandTotal = ($totalx - $data->discount_1) / ($set->qty * $data->qty);
                                }
                            }

                            $save_package->nominal = $grandTotal;
                            $save_package->save();

                            if ($data->immediate_usage) {
                                $invoicePackages[] = $save_package;
                            }
                        }
                    } else {
                        $save_package = new InvoicePackage;
                        $save_package->inv_detail_id = $save->id_inv_detail;
                        $save_package->product_id = $data->productTreatment_id;
                        $save_package->anamnesa_detail_id = isset($data->anamnesa_detail_id) ? $data->anamnesa_detail_id : null;
                        $save_package->qty = $data->qty;

                        $save_package->used = 0;
                        $save_package->nominal = $subtotal / $data->qty;
                        if (isset($data->therapistUsage_id)) {
                            $save_package->therapist_id = json_encode($data->therapistUsage_id);
                        } else {
                            $save_package->therapist_id = '[]';
                        }
                        $save_package->save();

                        if ($data->immediate_usage) {
                            $invoicePackages[] = $save_package;
                        }
                    }
                }


                // // send to izzibook
                // $izzibookInvoice = new InvoiceService();
                // $izzibookInvoice->store($invoice);

                // $paymentService = new PaymentService();

                for ($i = 0; $i < count($request->payment_type); $i++) {
                    $payment = new Payment;
                    $payment->inv_id = $store_invoice->id_invoice;
                    $payment->bank_id = $request->payment_type[$i];
                    if ($request->kembali > 0) {
                        $bank = Bank::find($request->bank_type[$i]);
                        if (!$bank->bank_type) {
                            $payment->amount = $request->amount[$i] - str_replace(array('.', ','), "", $request->kembali);
                        } else {
                            $payment->amount = $request->amount[$i];
                        }
                    } else {
                        $payment->amount = $request->amount[$i];
                    }
                    $payment->info = addslashes($request->payment_info[$i]);
                    $payment->save();

                    //    $paymentService->store($payment);
                }

                $agentccc = Agent::where('agent_code', $request->customer)->first();
                $sum_agentcc = 0;
                if ($agentccc) {
                    $agentcc = Invoice::where('void_invoice', 0)
                        ->where('agent_id', $agentccc->id_agent)
                        ->sum('total');
                    $ongkir = Invoice::where('void_invoice', 0)
                        ->where('agent_id', $agentccc->id_agent)
                        ->sum('ongkir');
                    $agentcc = $agentcc - $ongkir;
                    $sum_agentcc = ($agentcc * 0.1) / 100000;
                }

                // siap siap
                $invcc = $totalForPoint;
                $sum_client = $invcc / 100000;
                $sumPoint =  floor($sum_client + $sum_agentcc);
                $update_point = Customer::find($request->customer_id);

                $update_point->point = $update_point->point + $sumPoint;
                $store_invoice->point = $store_invoice->point - $sumPoint;
                $store_invoice->update();
                $update_point->update();
                //    $agent = Invoice::where('void_invoice', 0)
                //        ->where('agent_id', $request->agent)
                //        ->sum('total');
                //     $ongkirAgent = Invoice::where('void_invoice', 0)
                //     ->where('agent_id', $request->agent)
                //     ->sum('ongkir');
                //     $agent = $agent - $ongkirAgent;

                //    if ($request->agent) {
                //        $id_agent = Agent::find($request->agent);
                //        if ($id_agent->customer) {
                //            $agentx = $totalForPoint;

                //            $totalBefore = $agentx / 100000;
                //            $sum_agent = ($agent * 0.1) / 100000;

                //            $point_agent = Customer::find($id_agent->customer->id_customer);
                //            $sumPoint =  floor($sum_agent + $totalBefore);
                //            $point_agent->point = !$discount_inv ? $point_agent->point + $sumPoint : $point_agent->point;
                //            $store_invoice->point = !$discount_inv ? $store_invoice->point - $sumPoint : $store_invoice->point;
                //            $store_invoice->update();
                //            $point_agent->update();
                //        }
                //    }

                $queue = Queue::where('customer_id', $store_invoice->customer_id)->count();
                if ($queue) {
                    $updateQueue = Queue::where('customer_id', $store_invoice->customer_id)->latest('created_at')->first();
                    $updateQueue->status_payment = 2;
                    $updateQueue->update();
                }

                if ($totalProductTreatmentPoint > 0) {
                    Customer::find($store_invoice->customer_id)->increment('point', $totalProductTreatmentPoint);
                    $store_invoice->bonus_point = $totalProductTreatmentPoint;
                    $store_invoice->update();
                }

                if (count($invoicePackages) > 0) {
                    $this->storeUsage($request, $store_invoice, $invoiceDetails, $invoicePackages);
                }

                DB::commit();

                $log = new LogInvoice;
                $log->invoice_id = $store_invoice->id_invoice;
                $log->staff_id = Auth::user()->id;
                $log->save();

                return response()->json([
                    'id_invoice' => $store_invoice->id_invoice,
                ]);
            } catch (\Exception $e) {
                DB::rollback();

                return response()->json([
                    'message'   => $e->getMessage()
                ], 500);
            }
        }
    }

    public function storeUsage(Request $request, $invoice, $invoiceDetails, $invoicePackages)
    {
        $outlet = Outlet::first();

        DB::beginTransaction();
        try {
            $usage = new Usage;

            if ($request->outlet) {
                $outlet_id = $request->outlet;
            } else {
                $outlet_id = $outlet->id_outlet;
            }
            $usage->usage_code = Usage::generateUsageCode($outlet_id);
            $usage->usage_date = Carbon::createFromFormat('d/m/Y', $request->date_invoice)->format('Y-m-d');
            $usage->customer_id = $request->customer_id;
            $usage->outlet_id = $outlet_id;
            $usage->staff_id = Auth::user()->id;
            $usage->remarks = 'Immediate Usage';
            $usage->status_usage = 0;
            // $filename       = date('mdYHis') . "-signature.png";
            $usage->file    = null;
            $usage->save();

            $therapists = Therapist::where('therapist_name', 'NONE')->first();

            foreach ($invoicePackages as $invoice_package) {
                $usage_detail = new UsageDetail;
                $usage_detail->usage_id = $usage->id_usage;
                $usage_detail->inv_code = $invoice->inv_code;
                $usage_detail->product_id = $invoice_package->product_id;
                $usage_detail->invoice_package_id = $invoice_package->id_invoice_package;
                $usage_detail->point = 0;

                $usage_detail->qty = 0;
                $usage_detail->used = $invoice_package->qty;
                $usage_detail->duration = 0;
                $usage_detail->value = $invoice_package->nominal;
                $usage_detail->save();

                $invoice_package->used = $invoice_package->qty;
                $invoice_package->qty = 0;
                $invoice_package->update();

                $therapistDecode = json_decode($invoice_package->therapist_id);
                if (count($therapistDecode) >= 1) {
                    foreach ($therapistDecode as $therapist) {
                        $usage_therapist = new UsageTherapist;
                        $usage_therapist->usage_detail_id = $usage_detail->id_usage_detail;
                        $usage_therapist->therapist_id = $therapist;
                        $usage_therapist->save();
                    }
                } else {
                    $usage_therapist = new UsageTherapist;
                    $usage_therapist->usage_detail_id = $usage_detail->id_usage_detail;
                    $usage_therapist->therapist_id = $therapists->id_therapist;
                    $usage_therapist->save();
                }
            }

            $this->createMutations($request, $invoicePackages, $invoice, $usage);
            DB::commit();
            return json_encode($usage->id_usage);
        } catch (Exception $e) {
            DB::rollback();

            return response()->json([
                'message'   => $e->getMessage()
            ], 500);
        }
    }


    /**
     * Create mutations.
     *
     * @return void
     **/
    public function createMutations(Request $request, $invoicePackages, $invoice, $usage)
    {
        foreach ($invoicePackages as $package) {
            $product = ProductTreatment::find($package->product_id);

            $userOutlets = $request->user()->userOutlets()->pluck('outlet_id');
            // $grantedWarehouses = WarehouseOutlet::whereIn('outlet_id', $userOutlets)->pluck('warehouse_id');

            if (isset($package->anamnesa_detail_id)) {
                $anamnesaDetail = AnamnesaDetail::find($package->anamnesa_detail_id);
                if (isset($anamnesaDetail->boms) && count($anamnesaDetail->boms) > 0) {
                    foreach ($anamnesaDetail->boms as $material) {
                        $usageCount = $package->used;
                        $sort = $material->rawItem->dispense_method == 'lifo' ? 'asc' : 'desc';
                        $mutation = [
                            'warehouse_origin_id' => $request->warehouse_id,
                            'rack_origin_id' => null,
                            'sales_invoice_id' => $usage->id_usage,
                            'item_id' => $material->raw_item_id,
                            'transdate' => Carbon::createFromFormat('d/m/Y', $request->date_invoice)->format('Y-m-d'),
                            'transtype' => 'out',
                            'cogs'     => 0,
                            'price'     => str_replace(',', '', $material->rawItem->sales->price),
                            'ref_id'    => null,
                            'balance'   => 0,
                            'transdesc' => 'USAGE',
                            'qty' => $usageCount * $material->qty * -1
                        ];

                        Mutation::create($mutation);
                    }
                } else {
                    $usageCount = $package->used;
                    $sort = $product->item->dispense_method == 'lifo' ? 'asc' : 'desc';
                    $mutation = [
                        'warehouse_origin_id' => $request->warehouse_id,
                        'rack_origin_id' => null,
                        'sales_invoice_id' => $usage->id_usage,
                        'item_id' => $product->item->id,
                        'transdate' => Carbon::createFromFormat('d/m/Y', $request->date_invoice)->format('Y-m-d'),
                        'transtype' => 'out',
                        'cogs'     => 0,
                        'price'     => str_replace(',', '', $product->item->sales->price),
                        'ref_id'    => 0,
                        'balance'   => 0,
                        'transdesc' => 'USAGE',
                        'qty' => $usageCount * -1
                    ];

                    Mutation::create($mutation);
                }
            } else {
                // check if current usage is treatment.
                if ($product->item->type == 'service') {
                    foreach ($product->item->boms as $material) {
                        $usageCount = $package->used;
                        //    $sort = $material->rawItem->dispense_method == 'lifo' ? 'asc' : 'desc';

                        //    $currentStock = $material->rawItem->stock - ($usageCount * $material->qty);

                        //    $material->rawItem()->update([
                        //        'stock' => $currentStock
                        //    ]);

                        //    while ($usageCount > 0) {
                        //    $mutationLog = Mutation::where('transtype', 'in')
                        //                            ->where('transdesc', '<>', 'BEGIN EMPTY')
                        //                            ->where('item_id', $material->raw_item_id)
                        //                            ->where('warehouse_destination_id', $request->warehouse_id)
                        //                            ->orderBy('transdate', $sort)
                        //                            ->where('balance', '>', 0)
                        //                            ->first();

                        //    if ( ! $mutationLog) break;


                        //    $outQty = 0;

                        // current item at this warehouse has not enough stock.
                        //    if ($mutationLog->balance < $usageCount) {
                        //        $usageCount -= $mutationLog->balance;
                        //        $outQty = $mutationLog->balance;

                        //        $mutationLog->balance = 0;
                        //    } else {
                        //        $mutationLog->balance -= $usageCount;
                        //        $outQty = $usageCount;

                        //        $usageCount = 0;
                        //    }

                        //    $mutationLog->save();

                        $mutation = [
                            'warehouse_origin_id' => $request->warehouse_id,
                            'rack_origin_id' => null,
                            'sales_invoice_id' => $usage->id_usage,
                            'item_id' => $material->raw_item_id,
                            'transdate' => Carbon::createFromFormat('d/m/Y', $request->date_invoice)->format('Y-m-d'),
                            'transtype' => 'out',
                            'cogs'     => 0,
                            'price'     => str_replace(',', '', $material->rawItem->sales->price),
                            'ref_id'    => null,
                            'balance'   => 0,
                            'transdesc' => 'USAGE',
                            'qty' => $usageCount * $material->qty * -1
                        ];

                        Mutation::create($mutation);
                    }
                    //    }
                } else {

                    $usageCount = $package->used;
                    //    $sort = $product->item->dispense_method == 'lifo' ? 'asc' : 'desc';

                    //    $currentStock = $product->item->stock - $usageCount;

                    //    $product->item()->update([
                    //        'stock' => $currentStock
                    //    ]);

                    //    while ($usageCount > 0) {
                    //    $mutationLog = Mutation::where('transtype', 'in')
                    //                            ->where('transdesc', '<>', 'BEGIN EMPTY')
                    //                            ->where('item_id', $product->item->id)
                    //                            ->where('warehouse_destination_id', $request->warehouse_id)
                    //                            ->orderBy('transdate', $sort)
                    //                            ->where('balance', '>', 0)
                    //                            ->first();

                    //    if ( ! $mutationLog) break;


                    //    $outQty = 0;

                    //    // current item at this warehouse has not enough stock.
                    //    if ($mutationLog->balance < $usageCount) {
                    //        $usageCount -= $mutationLog->balance;
                    //        $outQty = $mutationLog->balance;

                    //        $mutationLog->balance = 0;
                    //    } else {
                    //        $mutationLog->balance -= $usageCount;
                    //        $outQty = $usageCount;

                    //        $usageCount = 0;
                    //    }

                    //    $mutationLog->save();

                    $mutation = [
                        'warehouse_origin_id' => $request->warehouse_id,
                        'rack_origin_id' => null,
                        'sales_invoice_id' => $usage->id_usage,
                        'item_id' => $product->item->id,
                        'transdate' => Carbon::createFromFormat('d/m/Y', $request->date_invoice)->format('Y-m-d'),
                        'transtype' => 'out',
                        'cogs'     => 0,
                        'price'     => str_replace(',', '', $product->item->sales->price),
                        'ref_id'    => null,
                        'balance'   => 0,
                        'transdesc' => 'USAGE',
                        'qty' => $usageCount * -1
                    ];

                    Mutation::create($mutation);
                }

                //    }
            }
        }
    }

    public function sendWhatsApp($id)
    {
        $wa = new Client([
            'region' => env('WA_REGION'),
            'token' => env('WA_TOKEN')
        ]);
        $dataInv = Invoice::find($id);
        $nameTreatment = '';
        foreach ($dataInv->invoice_detail as $inv) {
            if ($inv->product_treatment) {
                $nameTreatment .= "▪" . $inv->product_treatment->product_treatment_name . "\n";
            } else if ($inv->package) {
                $nameTreatment .= "▪" . $inv->package->package_name . "\n";
            }
        }
        $message = "*" . $dataInv->outlet->outlet_name . "*( _By:" . $dataInv->user->name . "_ )"
            . "\n▪ Nomor Invoice : " . $dataInv->inv_code
            . "\n▪ Total : " . $dataInv->total
            . "\n▪ Invoice Date : " . $dataInv->inv_date
            . "\n*ITEM*\n" . $nameTreatment
            . "\n\nTerima kasih.";

        if ($dataInv->customer->phone) {
            $send = $wa->sendMessageText($dataInv->customer->phone, $message);
        }

        return $send;
    }

    public function sign($id)
    {
        $invoice = Invoice::find($id);
        $payment = Payment::where('inv_id', $id)->get();
        $inv_detail = InvoiceDetail::where('inv_id', $id)->get();
        // $dompdf = PDF::loadView('invoice.print', compact('invoice', 'payment', 'inv_detail'));
        // $dompdf->setPaper('a4', 'potrait');
        // return $dompdf->stream('allright.pdf');

        return view('invoice.create.sign', compact('invoice', 'payment', 'inv_detail'));
    }

    public function updateSign(Request $request, $id)
    {
        $invoice = Invoice::find($id);

        $filename = "inv-" . date('mdYHis') . "-signature.png";
        $invoice->customer_sign = $filename;

        $data_uri = explode(',', $request->signature);
        $encoded_image = $data_uri[1];
        $decoded_image = base64_decode($encoded_image);
        file_put_contents(__DIR__ . '/../../../storage/app/public/customer/' . $filename, $decoded_image);

        $invoice->save();
        return json_encode($invoice->id_invoice);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        InvoiceDetail::find($id)->delete();
        InvoicePackage::where('inv_detail_id', $id)->delete();
    }

    public function generatePdf($id)
    {
        $invoice = Invoice::find($id);
        $payment = Payment::where('inv_id', $id)->get();
        $inv_detail = InvoiceDetail::where('inv_id', $id)->get();
        // $dompdf = PDF::loadView('invoice.print', compact('invoice', 'payment', 'inv_detail'));
        // $dompdf->setPaper('a4', 'potrait');
        // return $dompdf->stream('allright.pdf');


        $ageInYear = date('Y') - Carbon::createFromFormat('d/m/Y', $invoice->customer->birth_date)->format('Y');

        return view('invoice.create.print', compact('invoice', 'payment', 'inv_detail', 'ageInYear'));
    }
    public function generateThermalPdf($id)
    {
        $invoice = Invoice::find($id);
        $payment = Payment::where('inv_id', $id)->get();
        $inv_detail = InvoiceDetail::where('inv_id', $id)->get();
        // $dompdf = PDF::loadView('invoice.print', compact('invoice', 'payment', 'inv_detail'));
        // $dompdf->setPaper('a4', 'potrait');
        // return $dompdf->stream('allright.pdf');
        $invoice_date = Carbon::createFromFormat('d/m/Y', $invoice->inv_date)->format('Y-m-d');
        $queue = Queue::select('id_queue', 'queue_number', 'outlet_id', 'customer_id', 'come_date')
            ->where('outlet_id', $invoice->outlet_id)
            ->where('customer_id', $invoice->customer_id)
            ->where('come_date', $invoice_date)
            ->latest('created_at')
            ->first();

        $ageInYear = date('Y') - Carbon::createFromFormat('d/m/Y', $invoice->customer->birth_date)->format('Y');
        return view('invoice.create.print_thermal', compact('invoice', 'payment', 'inv_detail', 'queue', 'ageInYear'));
    }
    public function getPackage($id)
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $package = Package::where('status_package', 1)
            ->whereHas('package_outlet', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();

        $product = ProductTreatment::where('status_product_treatment', 1)
            ->whereHas('product_treatment_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        $allItem = $package->concat($product);
        $oke = $allItem[$id];
        // dd($oke);
        return json_encode($oke);
    }

    public function getActivity($activity_id,$item_type,$item_id)
    {
        $activity = Activity::find($activity_id);

        if ($item_type === 'package') {
            $type = 'package';
        } else if ($item_type === 'product' || $item_type === 'treatment') {
            $type = 'product_treatment';
        } 

        $activityItemCheck = ActivityProductTreatment::selectRaw('COUNT(activity_id) as check_activity')->where('activity_id',$activity->id_activity)->first();

        $activityItem = ActivityProductTreatment::where('activity_id',$activity->id_activity)
            ->when($type === 'package', function ($query) use ($item_id) {
                $query->where('package_id',$item_id);
            })
            ->when($type === 'product_treatment', function ($query) use ($item_id) {
                $query->where('product_treatment_id',$item_id);
            })
            ->first();
        
        return response()->json([
            'activity' => $activity,
            'activityItem' => $activityItem,
            'activityItemCheck' => $activityItemCheck
        ]);
    }
    public function getAnamnesa($id)
    {
        $anamnesa = Anamnesa::where('customer_id', $id)
            ->where('date_anamnesa', date('Y-m-d'))
            ->get();

        //    $select = "<option>- Select Anamnesa -</option>";
        $select = '';
        foreach ($anamnesa as $anam) {
            $select .= "<option value=" . $anam->id_anamnesa . ">" . $anam->anamnesa . "</option>";
        }

        return response()->json($select);
    }

    public function getWarehouse($id)
    {
        $warehouse =  Warehouse::whereHas('warehouseOutlets', function ($q) use ($id) {
            $q->where('outlet_id', $id);
        })->select('id_warehouse', 'name')->get();

        return response()->json($warehouse);
    }

    public function loadAnamnesa($id)
    {
        // $creator = Anamnesa:find($id);
        $anamnesa = Anamnesa::find($id);

        $creatorName = $anamnesa->creator->name;
        $load = AnamnesaDetail::where('anamnesa_id', $id)->get();
        foreach ($load as $row) {
            if ($row->kind == "package") {
                $row->load("package");
            } else {
                $row->load("product_treatment");
            }
        }
        return response()->json(['load' => $load, 'creator_name' => $creatorName]);
    }

    //    public function loadAnamnesa($id)
    //    {
    //        $load = AnamnesaDetail::where('anamnesa_id',$id)
    //        ->join('product_treatments', 'product_treatments.id_product_treatment', 'anamnesa_details.product_id')
    //        ->get();
    //        return response()->json(['load' => $load,]);

    //    }
}

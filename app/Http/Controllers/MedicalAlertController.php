<?php

namespace App\Http\Controllers;

use App\Models\MedicalAlert;
use App\Models\Customer;
use Illuminate\Http\Request;

class MedicalAlertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $before = MedicalAlert::where('customer_id', $request->id)->pluck('product_id')->toArray();

        $save_json = json_decode($request->save_json);
        $getMedicalId = [];
        foreach ($save_json as $key => $data) {
            $getMedicalId[] = $data->medical_id;
        }

        $shouldDelete = array_diff($before, $getMedicalId);

        if (count($shouldDelete)) {
            MedicalAlert::where('customer_id', $request->id)
                                ->whereIn('product_id', $shouldDelete)
                                ->delete();
        }

        $customer = Customer::find($request->id);
        $customer->restriction = $request->restriction;
        $customer->save();

        foreach ($save_json as $key => $data) {
            MedicalAlert::firstOrCreate([
                'customer_id'     => $request->id,
                'product_id'      => $data->medical_id
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = MedicalAlert::leftJoin('product_treatments', 'product_treatments.id_product_treatment', 'medical_alerts.product_id')
            ->where('customer_id', $id)->get();
        $customer = Customer::find($id);
        return json_encode([
            'medical'   => $data,
            'customer'  => $customer
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = MedicalAlert::where('customer_id', $id)->get();

        $medical = array();
        foreach ($data as $list) {
            $medical[] = array(
                'medical' => $list->product->product_treatment_name,
                'medical_id' => $list->product->id_product_treatment,
            );
        }

        $customer = Customer::find($id);

        return response()->json([
            'medical' => $medical,
            'customer'  => $customer
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Therapist;
use App\Models\Queue;
use App\Models\QueueScreen;
use App\Models\TherapistSchedule;
use App\Models\TherapistLeave;
use App\Models\Outlet;
use App\Models\UserOutlet;
use App\Models\CustomerOutlet;

use Carbon\carbon;
use Auth;
use Illuminate\Support\Facades\Gate;
class QueueController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function __construct()
{
   $this->middleware('auth');
   $this->middleware(function ($request, $next) {
       if (Gate::allows('queue')) {
           return $next($request);
       }
       abort(403, 'You do not have enough access rights');
   });
}

public function index(Request $request)
{
   $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
   $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
   $therapist = Therapist::where('status_therapist', 1)
               ->whereHas('therapist_outlets', function ($q) use ($outlet_id) {
                        return $q->whereIn('outlet_id', $outlet_id);
                })
               ->paginate(4);
   
   if($request->date)
   {
      $date = Carbon::createFromFormat('d/m/Y',$request->date)->format('Y-m-d');
      $day = Carbon::createFromFormat('d/m/Y',$request->date)->format('l');

   }else{
      $date = Carbon::now()->format('Y-m-d');
      $day = Carbon::now()->format('l');

   }
   

   $queue = Queue::where('come_date',$date)->count();

   return view('queue.index',compact('therapist','outlet','day','date','queue'));
}

public function queueScreen()
{
   $therapist = Therapist::where('status_therapist', 1)->get();
   $queueScreen = QueueScreen::latest('created_at')->first();
   $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
   $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
   $day = Carbon::now()->format('F d, Y | ');
   $days = Carbon::now()->format('l');
   $date = Carbon::now()->format('Y-m-d');
 
  
   return view('queue-screen.index',compact('therapist','queueScreen','outlet','date','day','days'));
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
   //
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
   $save = new Queue;
   $save->queue_number = Queue::generateQueueNumber($request->outlet);
   $save->customer_id = $request->customer_id;
   $save->therapist_id = $request->id;
   $save->outlet_id = $request->outlet;
   $save->status_payment =0;
   $save->status_queue=0;
   $save->come_date = Carbon::createFromFormat('d/m/Y', $request->come_date)->format('Y-m-d');
   $save->save();
}

public function storeNew(Request $request, $id){
   $therapist = Therapist::where('therapist_name', 'NONE')->first();
   // $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
   // $customer_idOutlet = CustomerOutlet::where('customer_id',$id)->whereIn('outlet_id', $outlet_id)->pluck('outlet_id')->first();
   $date = Carbon::now()->format('Y-m-d');
   
   $checkQueue = Queue::where('come_date',$date)->where('outlet_id',$request->outlet_id)->where('customer_id',$id)->latest()->first();
   $array = Queue::select('id_queue','come_date','queue_number','outlet_id')
                     ->where('outlet_id', $request->outlet_id)
                     ->where('come_date', $date)
                     ->latest('queue_number')
                     ->first();
  if (isset($array)) {
      $number = $array->queue_number + 1;
  } else {
      $number = 1;
  }
  
   if ($checkQueue) {
      $save = null;
   } else {
      $save = new Queue;
      $save->queue_number = $number;
      $save->customer_id = $id;
      $save->therapist_id = $therapist->id_therapist;
      $save->outlet_id = $request->outlet_id;
      $save->status_payment = 0;
      $save->status_queue = 0;
      $save->come_date = Carbon::now()->format('Y-m-d');
      $save->save();
   }

   return response()->json(['save'=> $save]);
}

/**
* Display the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/

public function printThermalQueue($id)
{
    $queue = Queue::find($id);

   $birthDate = Carbon::createFromFormat('d/m/Y', $queue->customer->birth_date)->format('Y');
    $year = date('Y') - $birthDate;

    return view('queue.print-thermal', compact('queue', 'year', 'birthDate'));
}

public function show($id)
{
   //
}
/**
* Show the form for editing the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
   $model = Queue::where('therapist_id',$id)->get();
   return response()->json($model);
}

public function status($id)
{
   $model = Queue::find($id);
   return response()->json($model);
}

public function status_store(Request $request,$id)
{
   $model = Queue::find($id);
   if($request->status_queue || $request->status_payment)
   {
    $model->status_payment = $request->status_payment;
    $model->status_queue = $request->status_queue;
    $model->update();
  
    if($request->status_queue == 1)
    {
      $before = QueueScreen::where('therapist_id', $request->therapist_id)->pluck('therapist_id')->toArray();
         if (count($before)) {
            QueueScreen::where('therapist_id', $request->therapist_id)
                              ->whereIn('therapist_id', $before)
                              ->delete();
         }
        QueueScreen::create([
            'queue_id' => $id,
            'therapist_id'  => $request->therapist_id
        ]);
     }
   }
 }

 public function screen_store(Request $request,$id)
 {
   $before = QueueScreen::where('therapist_id', $request->therapist_id)->pluck('therapist_id')->toArray();
   if (count($before)) {
      QueueScreen::where('therapist_id', $request->therapist_id)
                        ->whereIn('therapist_id', $before)
                        ->delete();
   }
   QueueScreen::create([
         'queue_id' => $id,
         'therapist_id'  => $request->therapist_id
   ]);
   
   return redirect()->back();

 }
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function update(Request $request, $id)
{
   //
}
/**
* Remove the specified resource from storage.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
   //
}
}
 
 
 


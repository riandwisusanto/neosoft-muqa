<?php

namespace App\Http\Controllers;

use App\Exports\CustomerExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Carbon\carbon;
use DataTables;
use Hash;
use Auth;
use Redirect;
use App\Models\Customer;
use App\Models\Country;
use App\Models\Province;
use App\Models\Outlet;
use App\Models\Consultant;
use App\Models\MarketingSource;
use App\Models\CustomerConsultant;
use App\Models\CustomerOutlet;
use App\Models\Invoice;
use App\Models\ProductTreatment;
use App\Models\InvoiceDetail;
use App\Models\LogVoidInvoice;
use App\Models\LogInvoice;
use App\Models\Logistics\Item;
use App\Models\LogVoidBalance;
use App\Models\Therapist;
use App\Models\Bank;
use App\Models\Payment;
use App\Models\Appointment;
use App\Models\Usage;
use App\Models\UsageDetail;
use App\Models\InvoicePoint;
use App\Models\LogVoidInvoicePoint;
use App\Models\LogConvert;
use App\Models\MedicalAlert;
use App\Models\InvoicePackage;
use App\Models\InvoiceBalance;
use App\Models\UserOutlet;
use App\Models\Anamnesa;
use App\Models\AnamnesaDetail;
use App\Models\CustomerConsignment;
use App\Models\MedicalConsign;
use App\Models\PhysicalExamination;
use App\Services\CustomerService;
use App\Services\InvoiceService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx as ExcelReader;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate as ExcelCoordinate;
use App\Models\OldAnamnesa;
use App\Models\Package;
use App\Models\Queue;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('customer')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();

        $country = Country::all();
        $productTreatment = ProductTreatment::where('status_product_treatment', 1)
            ->whereHas('product_treatment_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();

        $consultant = Consultant::where('status_consultant', 1)
            ->whereHas('consultant_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();

        $marketing_source = MarketingSource::where('status_marketing_source', 1)
            ->whereHas('marketing_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        return view('customers.index', compact('country', 'productTreatment', 'outlet', 'consultant', 'marketing_source'));
    }

    public function getProvince($id)
    {
        $province = Province::where('country_id', $id)->pluck('province', 'id_province');
        return json_encode($province);
    }

    public function getCustomer(Request $request)
    {
        $query = $request->get('query');
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        // $customer_id = CustomerOutlet::whereIn('outlet_id', $outlet_id)->pluck('customer_id')->all();
        $customer = Customer::where(function ($q) use ($query){
            $q->orWhere('induk_customer', 'LIKE', "{$query}")
            ->orWhere('full_name', 'LIKE', "{$query}%")
            ->orWhere('address', 'LIKE', "%{$query}%")
            ->orWhere('phone', 'LIKE', "%{$query}%");
        })
        ->orderBy('created_at', 'desc')
        ->limit(13)
        ->get();


        if (count($customer) == 0) {
            $output = '<ul id="country-list" class="form-control">';
            $output .= '<li>Customer not Registered</li>';
            $output .= '</ul>';
        } else {
            $output = '<ul id="country-list" class="form-control">';

            foreach ($customer as $row) {
                // $birthDate = Carbon::createFromFormat('d/m/Y', $row->birth_date)->format('Y');
                // $year = date('Y') - $birthDate;

                $output .= '<li onclick="detailCustomer(
                    \'' . $row->id_customer . '\',
                    \'' . $row->induk_customer . '\',
                    \'' . addslashes($row->full_name) . '\',
                    \'' . addslashes($row->nick_name) . '\',
                    \'' . addslashes($row->birth_date) . '\',
                    \'' . addslashes($row->address) . '\',
                    \'' . addslashes($row->phone) . '\',
                    \'' . $row->birth_date . '\',
                    \'' . $row->point . '\',
                    \'' . $row->email . '\',
                    \'' . $row->address . '\',
                    )">
                    <strong>'
                    . strtoupper($row->full_name) .
                    '</strong></li>';
            }
            $output .= '</ul>';
        }

        return json_encode($output);
    }

    public function getCustomerQueue(Request $request)
    {
        $keyword = $request->get('query');
        $outlet_id = $request->outlet_id;
        $date = Carbon::now()->format('Y-m-d');
        $output = '';

        // $customer_id = Queue::where('come_date',$date)
        //                 ->Where('queue_number', 'LIKE', "%{$query}%")
        //                 ->where('outlet_id', $outlet_id)
        //                 ->pluck('customer_id')->toArray();

        // $customer = Customer::whereIn('id_customer', $customer_id)
        //     ->get()->take(13);
        $q = Queue::where('outlet_id', $outlet_id)
                ->where('queue_number', $keyword)
                ->where('come_date', date('Y-m-d'))
                ->first();


        if ( ! $q) {
            $output = '<ul id="country-list" class="form-control">';
            $output .= '<li>Customer not Registered</li>';
            $output .= '</ul>';
        } else {
            $row = Customer::find($q->customer_id);
            $output = '<ul id="country-list" class="form-control">';

            $birthDate = Carbon::createFromFormat('d/m/Y', $row->birth_date)->format('Y');
            $year = date('Y') - $birthDate;

            $output .= '<li onclick="detailCustomer(
                \'' . $row->id_customer . '\',
                \'' . $row->induk_customer . '\',
                \'' . addslashes($row->full_name) . '\',
                \'' . addslashes($row->nick_name) . '\',
                \'' . addslashes($year) .' tahun'. '\',
                \'' . addslashes($row->address) . '\',
                \'' . addslashes($row->phone) . '\',
                \'' . $row->birth_date . '\',
                \'' . $row->point . '\',
                \'' . $row->email . '\',
                \'' . $row->address . '\',
                )">
                <strong>'
                . strtoupper($row->full_name) .
                '</strong></li>';
            $output .= '</ul>';
        }

        return json_encode($output);
    }
    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function upload(Request $request)
    {
        if ($request->hasFile('anamnesa')) {
            $filePath = $request->anamnesa->storeAs('data_lama', $request->rm. '.xlsx');


            OldAnamnesa::where('customer_id', $request->customer_id)->delete();

            $file = storage_path('app/'.$filePath);
            $reader = new ExcelReader();
            $reader->setReadDataOnly(true);


            $spreadsheet = $reader->load($file);
            $worksheet = $spreadsheet->getActiveSheet();

            $highestRow = $worksheet->getHighestRow(); // e.g. 10
            $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
            $highestColumnIndex = ExcelCoordinate::columnIndexFromString($highestColumn);


            for ($row = 19; $row <= $highestRow; $row++) {
                $data = [
                    'customer_id'           => $request->customer_id,
                    'checkup_date'          => $worksheet->getCellByColumnAndRow(3, $row)->getValue(),
                    'checkup_time'          => $worksheet->getCellByColumnAndRow(4, $row)->getValue(),
                    'username'              => $worksheet->getCellByColumnAndRow(5, $row)->getValue(),
                    'ax_dx'                 => $worksheet->getCellByColumnAndRow(6, $row)->getValue(),
                    'treatment_name'        => $worksheet->getCellByColumnAndRow(7, $row)->getValue(),
                    'prescription_name'     => $worksheet->getCellByColumnAndRow(15, $row)->getValue(),
                    'outlet_name'           => $worksheet->getCellByColumnAndRow(26, $row)->getValue(),
                    'record_type'           => $worksheet->getCellByColumnAndRow(27, $row)->getValue()
                ];

                OldAnamnesa::create($data);
            }


            return redirect()->back();
        }
    }

    public function getAllCustomer()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $customer = Customer::orderBy('full_name', 'asc')
            ->whereHas('outlet', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })
            ->get()->take(15);
        $output = '<ul id="country-list" class="form-control">';
        foreach ($customer as $row) {
            $output .= '<li onclick="detailCustomer(
                \'' . $row->id_customer . '\',
                \'' . $row->induk_customer . '\',
                \'' . addslashes($row->full_name) . '\',
                \'' . addslashes($row->phone) . '\',
                \'' . $row->birth_date . '\',
                \'' . $row->point . '\',
                \'' . $row->email . '\',
                \'' . $row->address . '\',
                )">
                <strong>'
                . strtoupper($row->full_name) .
                '</strong></li>';
        }
        $output .= '</ul>';
        return json_encode($output);
    }

    public function listData()
    {
        $outlet_id = UserOutlet::pluck('outlet_id')->toArray();
        $customer_id = CustomerOutlet::whereIn('outlet_id', $outlet_id)->pluck('customer_id')->toArray();
        $customer = Customer::with([
            'outlet:id_outlet,outlet_name',
            'consultant:id_consultant,consultant_name',
        ])
            ->whereIn('id_customer', $customer_id)
            ->get();

        $data = array();
        foreach ($customer as $list) {
            $outlets = '';
            foreach ($list->outlet as $out) {
                $outlets .= '&#10004;' . $out->outlet_name . '<br>';
            }
            // $consultants = '';
            // foreach ($list->consultant as $consult) {
            //     $consultants .= '&#10004;' . $consult->consultant_name . '<br>';
            // }
            $rank = rank($list->id_customer);
            $row = array();
            $row[] = '';
            $row[] = $list->induk_customer;
            $row[] = $list->full_name;
            $row[] = $list->phone;
            $row[] = ($rank) ? $rank->name : 'No Membership';
            $row[] = Carbon::parse($list->join_date)->format('d/m/Y');
            $row[] = $outlets;
            // $row[] = $consultants;

            if (count($list->invoice) || count($list->appointment)) {
                $delete = "<a disabled class='btn btn-danger btn-sm'><i class='fa fa-trash'></i> Delete</a>";
            } else {
                $delete = "<a onclick='deleteData(" . $list->id_customer . ")' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i> Delete</a>";
            }

            $row[] = "
                <a href='customers/" . $list->id_customer . "' class='btn btn-primary btn-sm'><i class='fa fa-eye'></i> Detail</a>
                <a onclick='editForm(" . $list->id_customer . ")' class='btn btn-success btn-sm'><i class='fa fa-edit'></i> Edit</a>
                " . $delete . "
                ";
            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => [
                'required'
            ],
            // 'email' => [
            //     'required',
            //     Rule::unique('customers')->ignore($id, 'id_customer'),
            // ],
        ]);
        if (!$validator->fails()) {
            $status = true;
            $message = "Successfully Created Customer";
            $save = new Customer;
            $save->induk_customer = Customer::generateIndukCustomer($request->outlet);
            $save->full_name = $request->fullname;
            $save->nick_name = $request->nick_name;
            $save->phone = $request->phone;
            $save->email = $request->email;
            $save->birth_place = $request->place;
            $save->birth_date = Carbon::createFromFormat('d/m/Y', $request->birth_date)->format('Y-m-d');
            $save->join_date = Carbon::createFromFormat('d/m/Y', $request->join_date)->format('Y-m-d');
            $save->gender = $request->gender;
            $save->religion = $request->religion;
            $save->ktp = $request->noktp;
            $save->address = $request->address;
            $save->occupation = $request->occupation;
            $save->country_id = $request->country;
            $save->city_id = $request->city;
            $save->zip = $request->zip;
            $save->nextofikin = $request->familyname;
            $save->nextofikin_number = $request->familyphone;
            $save->source_id = $request->source;
            $save->source_remarks = $request->source_remarks;
            $save->purpose_treatment = $request->purpose_treatment;
            $save->staf_id = Auth::user()->id; //auth user login
            $save->password = Hash::make('123456789');
            // $save->sendEmailVerificationNotification();
            $save->save();

            $outlet_save = new CustomerOutlet;
            $outlet_save->customer_id = $save->id_customer;
            $outlet_save->outlet_id = $request->outlet;
            $outlet_save->save();

            $consultant_save = new CustomerConsultant;
            $consultant_save->customer_id = $save->id_customer;
            $consultant_save->consultant_id = $request->consultant;
            $consultant_save->save();

            // initialization new customer
            $customer = Customer::find($save->id_customer);

            // send to izzibook
            // $izzibookCustomer = new CustomerService();
            // $izzibookCustomer->store($customer);
        } else {
            $errors = $validator->errors();
            $message = $errors;
            $status = false;
        }


        return response()->json([
            'message' => $message,
            'status' => $status,
        ]);
    }

    public function storeCustomerConsultant(Request $request, $id)
    {
        $storeConsultant = new CustomerConsultant;
        $storeConsultant->customer_id = $id;
        $storeConsultant->consultant_id = $request->consultant;
        $storeConsultant->save();

        return redirect(route('customers.show', $id));
    }

    public function storeCustomerOutlet(Request $request, $id)
    {
        $storeOutlet = new CustomerOutlet;
        $storeOutlet->customer_id = $id;
        $storeOutlet->outlet_id = $request->outlet;
        $storeOutlet->save();

        return redirect(route('customers.show', $id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $now = Carbon::now()->toDateString();

        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $listOutlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $listConsultant = Consultant::where('status_consultant', 1)
            ->whereHas('consultant_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();

        $overview = Customer::findOrFail($id);
        $appointment = Appointment::where('customer_id', $id)->get();
        $active_invoice = Invoice::with([
                'invoice_detail' => function ($q) {
                    $q->with(['invoice_package']);
                },
                'payment'
            ])
            ->where('customer_id', $id)
            ->where('void_invoice', 0)
            ->where('expired_date', '>=', $now)
            ->orderBy('created_at', 'DESC')
            ->get();
        $expired_invoice = Invoice::with([
                'invoice_detail' => function ($q) {
                    $q->with(['invoice_package']);
                },
                'payment'
            ])
            ->where('customer_id', $id)
            ->where('void_invoice', 0)
            ->where('expired_date', '<=', $now)
            ->get();
        $invoice = Invoice::where('void_invoice', 0)->where('customer_id', $id)->orderBy('created_at', 'DESC')->get();
        $outstandingInvoice = Invoice::with(['payment'])
        ->where('customer_id', $id)
        ->where('void_invoice', 0)
        ->orderBy('created_at', 'DESC')
        ->get();;

        $sales_summary = Invoice::where('customer_id', $id)->where('void_invoice', 0)->get()->groupBy('outlet_id');
        $void_invoice = Invoice::with(['user','log_void_invoice'])
        ->where('customer_id', $id)
        ->where('void_invoice', 1)
        ->orderBy('created_at', 'DESC')
        ->get();


        $treatments = DB::select('select inv.inv_code, p.product_treatment_name, ip.qty from invoice_packages ip join invoice_details invdet on ip.inv_detail_id = invdet.id_inv_detail join invoices inv on inv.id_invoice = invdet.inv_id join product_treatments p on p.id_product_treatment = ip.product_id where inv.customer_id = ? order by inv.inv_date DESC LIMIT 3', [$id]);
        $salesSummary = Invoice::selectRaw('SUM(total) as total')->where('customer_id', $id)->where('void_invoice', 0)->first();
        $outstandingSummary = Invoice::selectRaw('SUM(remaining) as total')->where('customer_id', $id)->where('void_invoice', 0)->first();

        $balance = InvoiceBalance::with(['invoice', 'payment', 'user'])
        ->where('customer_id', $id)
        ->where('status_balance', 0)
        ->orderBy('created_at', 'DESC')
        ->get();
        $void_balance = InvoiceBalance::with([
            'user',
            'log_void_balance' => function ($q) {
                $q->with(['user']);
            }
        ])
        ->where('customer_id', $id)
        ->where('status_balance', 1)
        ->orderBy('created_at', 'DESC')
        ->get();

        $usage = Usage::with('usage_detail', 'usage_detail.product_treatment')->where('customer_id', $id)->where('status_usage', 0)->get();
        $void_usage = Usage::where('customer_id', $id)->where('status_usage', 1)->get();

        $medical_alert = MedicalAlert::where('customer_id', $id)->get();

        $redeem_points = $overview->active_invoice_points;
        foreach ($redeem_points as $redeem_point) {
            $redeem_point->created_date = (new Carbon($redeem_point->created_at))->format('d/m/Y');
        }
        $void_redeem_points = $overview->void_invoice_points;
        foreach ($void_redeem_points as $redeem_point) {
            $redeem_point->created_date = (new Carbon($redeem_point->created_at))->format('d/m/Y');
            $redeem_point->void->created_date = (new Carbon($redeem_point->void->created_at))->format('d/m/Y');
        }

        $anamnesa = Anamnesa::with(['anamnesa_detail'])
        ->where('customer_id', $id)
        ->orderBy('created_at', 'DESC')
        ->get();
        $consignment = CustomerConsignment::where('customer_id', $id)->orderBy('created_at', 'DESC')->get();
        $physical = PhysicalExamination::where('customer_id', $id)->orderBy('created_at', 'DESC')->get();
        $productTreatment = ProductTreatment::where('status_product_treatment', 1)
        ->whereHas('product_treatment_outlets', function ($q) use ($outlet_id) {
            return $q->whereIn('outlet_id', $outlet_id);
        })->get();

        $overviews = [
            'anamnesas' => array_slice($anamnesa->toArray(), 0, 3),
            'usage' => array_slice($usage->toArray(), 0, 3),
        ];

        $ageInYear = date('Y') - Carbon::createFromFormat('d/m/Y', $overview->birth_date)->format('Y');

        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $country = Country::all();
        $consultant = Consultant::where('status_consultant', 1)
            ->whereHas('consultant_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        $marketing_source = MarketingSource::where('status_marketing_source', 1)
            ->whereHas('marketing_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        // $customers = Customer::select('id_customer', 'full_name', 'induk_customer')
        //     ->whereHas('outlet', function ($q) use ($outlet_id) {
        //         return $q->whereIn('outlet_id', $outlet_id);
        //     })
        //     ->get();
        $dataDiagnosa = [
            ["id" => 1, "name" => "ACNE", "isChecked" => false ],
            ["id" => 2, "name" => "BAU BADAN", "isChecked" => false ],
            ["id" => 3, "name" => "CR DOKTER", "isChecked" => false ],
            ["id" => 4, "name" => "HIPERHIDROSIS", "isChecked" => false ],
            ["id" => 5, "name" => "HIPERPIGMENTASI", "isChecked" => false ],
            ["id" => 6, "name" => "HIPOPIGMENTASI", "isChecked" => false ],
            ["id" => 7, "name" => "HIRSUTISME", "isChecked" => false ],
            ["id" => 8, "name" => "HS", "isChecked" => false ],
            ["id" => 9, "name" => "IRITASI", "isChecked" => false ],
            ["id" => 10, "name" => "KELOID", "isChecked" => false ],
            ["id" => 11, "name" => "KERATORIS SEBOROIK", "isChecked" => false ],
            ["id" => 12, "name" => "KERING", "isChecked" => false ],
            ["id" => 13, "name" => "KERONTOKAN RAMBUT", "isChecked" => false ],
            ["id" => 14, "name" => "KETOMBE", "isChecked" => false ],
            ["id" => 15, "name" => "KUSAM", "isChecked" => false ],
            ["id" => 16, "name" => "MENOLAK TAPER", "isChecked" => false ],
            ["id" => 17, "name" => "MERAH", "isChecked" => false ],
            ["id" => 18, "name" => "MERINTIS", "isChecked" => false ],
            ["id" => 19, "name" => "NEVUS", "isChecked" => false ],
            ["id" => 20, "name" => "OBESITAS", "isChecked" => false ],
            ["id" => 21, "name" => "OCHORONOSIS", "isChecked" => false ],
            ["id" => 22, "name" => "PARUT ACNE", "isChecked" => false ],
            ["id" => 23, "name" => "PENUAAN KULIT", "isChecked" => false ],
            ["id" => 24, "name" => "PX TIDAK DATANG", "isChecked" => false ],
            ["id" => 25, "name" => "SELULIT", "isChecked" => false ],
            ["id" => 26, "name" => "STRETCH MARK", "isChecked" => false ],
            ["id" => 27, "name" => "TAPER HQ", "isChecked" => false ],
            ["id" => 28, "name" => "TAPER ST", "isChecked" => false ],
            ["id" => 29, "name" => "TATTO", "isChecked" => false ],
            ["id" => 30, "name" => "TIDAK ADA KELUHAN", "isChecked" => false ],
            ["id" => 31, "name" => "UPSELLING", "isChecked" => false ],
            ["id" => 32, "name" => "VERUKA KUTIL", "isChecked" => false ],
            ["id" => 33, "name" => "ALERGI", "isChecked" => false ],
            ["id" => 34, "name" => "FLEK", "isChecked" => false ],
            ["id" => 35, "name" => "GATAL", "isChecked" => false ],
            ["id" => 36, "name" => "KANTUNG MATA", "isChecked" => false ],
            ["id" => 37, "name" => "KERUTAN", "isChecked" => false ],
            ["id" => 38, "name" => "KOMEDO", "isChecked" => false ],
            ["id" => 39, "name" => "MENGELUPAS", "isChecked" => false ],
            ["id" => 40, "name" => "NORMAL", "isChecked" => false ],
            ["id" => 41, "name" => "OLILY", "isChecked" => false ],
            ["id" => 42, "name" => "PEMULA", "isChecked" => false ],
            ["id" => 43, "name" => "PERIH", "isChecked" => false ],
            ["id" => 44, "name" => "POST DOKTER", "isChecked" => false ],
            ["id" => 45, "name" => "POST TREATMENT", "isChecked" => false ],
            ["id" => 46, "name" => "PX BARU", "isChecked" => false ],
            ["id" => 47, "name" => "SCAR", "isChecked" => false ],
            ["id" => 48, "name" => "SENSITIF", "isChecked" => false ],
            ["id" => 49, "name" => "TELANG EKSTASIS", "isChecked" => false ],
        ];

        $oldAnamnesas = OldAnamnesa::where('customer_id', $id)->get();
        $consent = MedicalConsign::with(['customer.outlet'])->where('customer_id', $id)->get();

        return view('customers.detail')
            ->with('overview', $overview)
            ->with('medical_alert', $medical_alert)
            ->with('active_invoice', $active_invoice)
            ->with('expired_invoice', $expired_invoice)
            ->with('appointment', $appointment)
            ->with('invoice', $invoice)
            ->with('void_invoice', $void_invoice)
            ->with('void_usage', $void_usage)
            ->with('void_balance', $void_balance)
            ->with('list_consultant', $listConsultant)
            ->with('list_outlet', $listOutlet)
            ->with('balance', $balance)
            ->with('usage', $usage)
            ->with('sales_summary', $sales_summary)
            ->with('redeem_points', $redeem_points)
            ->with('void_redeem_points', $void_redeem_points)
            ->with('anamnesa', $anamnesa)
            ->with('consignment', $consignment)
            ->with('physical', $physical)
            ->with('productTreatment', $productTreatment)
            ->with('country', $country)
            ->with('outlet', $outlet)
            ->with('consultant', $consultant)
            ->with('marketing_source', $marketing_source)
            ->with('oldAnamnesas', $oldAnamnesas)
            // ->with('customers', $customers)
            ->with('overviews', $overviews)
            ->with('ageInYear', $ageInYear)
            ->with('treatments', $treatments)
            ->with('salesSummary', $salesSummary)
            ->with('outstandingSummary', $outstandingSummary)
            ->with('outstandingInvoice', $outstandingInvoice)
            ->with('consent', $consent)
            ->with('id', $id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = Customer::find($id);
        $date_birth = Carbon::createFromFormat('d/m/Y', $edit->birth_date)->format('d F Y');
        $edit->join_date = Carbon::createFromFormat('Y-m-d', $edit->join_date)->format('d/m/Y');

        $outlet = CustomerOutlet::where('customer_id', $edit->id_customer)->first();

        $consultant = CustomerConsultant::where('customer_id', $edit->id_customer)->first();
        $result = array('edit' => $edit, 'date_birth' => $date_birth, 'outlet' => $outlet, 'consultant' => $consultant);
        echo json_encode($result);
    }


    public function update(Request $request, $id)
    {
        $update = Customer::findOrFail($id);

        if ($request->verifikasi == true) {
            $update->status_customer = 1;
            $update->update();
            return response()->json(true);
        }

        if ($request->not == true) {
            $update->status_customer = 0;
            $update->update();
            return response()->json(true);
        }

        $validator = Validator::make($request->all(), [
            'phone' => [
                'required',
                Rule::unique('customers')->ignore($id, 'id_customer')
            ],
            // 'email' => [
            //     'required',
            //     Rule::unique('customers')->ignore($id, 'id_customer'),
            // ],
        ]);

        $email = $update->email;

        if (!$validator->fails()) {
            $status = true;
            $message = "Update Created Customer";
            $update->full_name = $request->fullname;
            $update->nick_name = $request->nick_name;
            $update->phone = $request->phone;
            $update->email = $request->email;
            $update->birth_place = $request->place;
            $update->birth_date = Carbon::createFromFormat('d/m/Y', $request->birth_date)->format('Y-m-d');
            $update->join_date = Carbon::createFromFormat('d/m/Y', $request->join_date)->format('Y-m-d');
            $update->gender = $request->gender;
            $update->religion = $request->religion;
            $update->ktp = $request->noktp;
            $update->address = $request->address;
            $update->occupation = $request->occupation;
            $update->country_id = $request->country;
            $update->city_id = $request->city;
            $update->zip = $request->zip;
            $update->nextofikin = $request->familyname;
            $update->nextofikin_number = $request->familyphone;
            $update->source_id = $request->source;
            $update->source_remarks = $request->source_remarks;
            $update->purpose_treatment = $request->purpose_treatment;
            $update->staf_id = Auth::user()->id; //auth user login
            if (!$update->password) {
                $update->password = Hash::make('123456789');
            }

            // if (!$update->email_verified_at && $email != $request->email) {
            //     $update->sendEmailVerificationNotification();
            // }
            $update->update();

            // initialization new customer
            $customer = Customer::find($id);

            // send to izzibook
            // $izzibookCustomer = new CustomerService();
            // $izzibookCustomer->update($customer);
        } else {
            $errors = $validator->errors();
            $message = $errors;
            $status = false;
        }

        return response()->json([
            'message' => $message,
            'status' => $status,
        ]);
    }


    public function destroyCustomerConsultant($id)
    {
        $destroyConsultant = new CustomerConsultant;
        $customer_id = $destroyConsultant->find($id)->customer_id;
        if ($destroyConsultant->where('customer_id', $customer_id)->count() > 1) {
            $destroyConsultant->find($id)->delete();
        }
        return redirect(route('customers.show', $customer_id));
    }

    public function destroyCustomerOutlet($id)
    {
        $destroyOutlet = new CustomerOutlet;
        $customer_id = $destroyOutlet->find($id)->customer_id;
        if ($destroyOutlet->where('customer_id', $customer_id)->count() > 1) {
            $destroyOutlet->find($id)->delete();
        }
        return redirect(route('customers.show', $customer_id));
    }

    public function activeInvoice($id)
    {
        $invoice = Invoice::find($id);
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $bank = Bank::whereHas('bank_outlets', function ($q) use ($outlet_id) {
            return $q->whereIn('outlet_id', $outlet_id);
        })->get();
        $consultant = Consultant::where('status_consultant', 1)
            ->whereHas('consultant_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        $marketing_source = MarketingSource::where('status_marketing_source', 1)
            ->whereHas('marketing_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        $therapist = Therapist::where('status_therapist', 1)
            ->whereHas('therapist_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        $log = LogInvoice::where('invoice_id', $id)->get();
        $log_usage = UsageDetail::where('inv_code', $invoice->inv_code)->get();
        $log_convert = LogConvert::where('from_invoice_id', $invoice->inv_code)->get();

        return view('customers.active-expired.detail', compact('invoice', 'marketing_source', 'therapist', 'consultant', 'bank', 'log', 'log_usage', 'log_convert'));
    }

    public function voidActiveInvoice(Request $request, $id)
    {
        $saveLog = new LogVoidInvoice;
        $saveLog->inv_id = $id;
        $saveLog->staff_id = Auth::user()->id;
        $saveLog->remarks = $request->remarks;
        $saveLog->save();

        $invoice = Invoice::find($id);
        $invoice->void_invoice = 1;
        $invoice->update();

        if($invoice->bonus_point > 0) {
            Customer::find($invoice->customer_id)->decrement('point', (int) $invoice->bonus_point);
        }


        $void = InvoiceBalance::where('invoice_id', $id)->first();
        if ($void) {
            $log = new LogVoidBalance;
            $log->balance_id = $void->id_balance;
            $log->staff_id = Auth::user()->id;
            $log->remarks = $request->remarks;
            $log->save();

            $void->status_balance = 1;
            $void->update();
        }

        $getDetail = InvoiceDetail::where('inv_id', $id)->pluck('id_inv_detail')->toArray();
        $getDetail2 = InvoiceDetail::where('inv_id', $id)->get();
        foreach($getDetail2 as $details)
        {
            if (isset($details->anamnesa_detail_id)) {
                $anamnesaDetail = AnamnesaDetail::find($details->anamnesa_detail_id);
                $anamnesaDetail->is_paid = 0;
                $anamnesaDetail->update();
            }
        }
        // $this->decrementSellable($invoice, $getDetail);
        InvoicePackage::whereIn('inv_detail_id', $getDetail)->delete();
        InvoiceDetail::where('inv_id', $id)->delete();

        $rank = $invoice->point;

        //dd($rank);

        $update_point = Customer::find($invoice->customer_id);
        $update_point->point = $update_point->point + $rank;
        $update_point->update();

        if(count($invoice->log_convert) > 0) {
            $invBefore = Invoice::where('inv_code',$invoice->log_convert->first()->from_invoice_id)->first();
            foreach($invBefore->invoice_detail as $inv_detail) {
                foreach($inv_detail->invoice_package as $inv_package) {
                    if (substr($inv_detail->package_code,0,3) == 'PKG') {
                        $package = Package::where('package_code', $inv_detail->package_code)->first();
                        foreach ($package->package_detail as $set) {
                            if($inv_package->product_id === $set->product_id) {
                                if ($inv_package->used + $inv_package->qty < $set->qty * $inv_detail->qty) {
                                    $inv_package->qty += ($set->qty * $inv_detail->qty) - ($inv_package->used + $inv_package->qty);
                                    $inv_package->save();
                                }
                            }
                        }
                    } else {
                        if ($inv_package->used + $inv_package->qty < $inv_detail->qty) {
                            $inv_package->qty += $inv_detail->qty - ($inv_package->used + $inv_package->qty);
                            $inv_package->save();
                        }
                    }

                }
            }
        }

        // initialization new customer
        $invoice = Invoice::find($id);


        // // send to izzibook
        // $izzibookInvoice = new InvoiceService();
        // $izzibookInvoice->destroy($invoice);
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function decrementSellable($invoice, $details)
    {
        foreach ($details as $id) {
            $invoiceDetail = InvoiceDetail::find($id);

            if ( ! isset($invoiceDetail->anamnesa_detail_id)) {
                $productTreatment = ProductTreatment::where('product_treatment_code', $invoiceDetail->package_code)->first();

                if ($productTreatment) {
                    $item = Item::where('sales_information_id', $productTreatment->id_product_treatment)->first();
                    if ($item->type == 'product') {
                        $item->sellable_stock += $invoiceDetail->qty;
                        $item->save();
                    } else {
                        foreach ($item->boms as $material) {
                            $sellable = $material->rawItem->sellable_stock;
                            $material->rawItem()->update([
                                'sellable_stock' => $sellable + ($invoiceDetail->qty * $material->qty)
                            ]);
                        }
                    }
                }
            } else {
                $anamnesaDetail = AnamnesaDetail::find($invoiceDetail->anamnesa_detail_id);
                foreach ($anamnesaDetail->boms as $material) {
                    $sellable = $material->rawItem->sellable_stock;
                    $material->rawItem()->update([
                        'sellable_stock' => $sellable + ($invoiceDetail->qty * $material->qty)
                    ]);
                }
            }
        }
    }

    public function redeemInvoice($id)
    {
        $invoice = InvoicePoint::findOrFail($id);
        $invoice->created_date = (new Carbon($invoice->created_at))->format('d/m/Y');
        if ($invoice->void) {
            $invoice->void->created_date = (new Carbon($invoice->void->created_at))->format('d/m/Y');
        }
        return view('customers.point.detail', compact('invoice'));
    }

    public function voidInvoicePoint(Request $request, $id)
    {
        $invoice = InvoicePoint::findOrFail($id);
        $invoice->status_inv_point = 0;
        $invoice->update();

        $saveLog = new LogVoidInvoicePoint();
        $saveLog->inv_point_id = $id;
        $saveLog->staff_id = Auth::user()->id;
        $saveLog->remarks = $request->remarks ? $request->remarks : '';
        $saveLog->save();
        $update_point = Customer::find($invoice->customer->id_customer);
        $update_point->point = round($update_point->point + $invoice->total);
        $update_point->update();
    }

    public function updateActiveInvoice(Request $request, $id)
    {
        $log = new LogInvoice;
        $log->invoice_id = $id;
        $log->staff_id = Auth::user()->id;
        $log->save();

        $update_invoice = Invoice::find($id);
        $update_invoice->inv_date = Carbon::createFromFormat('d/m/Y', $request->date_invoice)->format('Y-m-d');
        $update_invoice->expired_date = Carbon::createFromFormat('d/m/Y', $request->date_expired)->format('Y-m-d');
        $update_invoice->therapist_id = $request->therapist;
        $update_invoice->consultant_id = $request->consultant;
        $update_invoice->marketing_source_id = $request->marketing_source;
        $update_invoice->update();

        for ($i = 0; $i < count($request->payment_type); $i++) {
            $payment = Payment::find($request->id_payment[$i]);
            $payment->bank_id = $request->payment_type[$i];
            $payment->update();
        }

        // initialization new customer
        $invoice = Invoice::find($id);

        // send to izzibook
        $izzibookInvoice = new InvoiceService();
        $izzibookInvoice->update($invoice);
    }


    public function treatmentBalance(Request $request, $id)
    {
        if ($request->check_code) {
            $check_code = [];
            for ($i = 0; $i < count($request->check_code); $i++) {
                $check_code[] = $request->check_code[$i];
            }

            $invoice = Invoice::where('customer_id', $id)->first();
            $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
            $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();

            $product = InvoicePackage::whereIn('id_invoice_package', $check_code)->get();
            $therapist = Therapist::where('status_therapist', 1)
                ->whereHas('therapist_outlets', function ($q) use ($outlet_id) {
                    return $q->whereIn('outlet_id', $outlet_id);
                })->get();

            $materials = Item::with('uom')
                    ->where('type','product')
                    ->leftJoin('mutations','mutations.item_id','=','items.id')
                    ->selectRaw('items.*, sum(mutations.qty) as maxStock')
                    ->groupBy('items.id')
                    ->whereHas('sales', function ($query) {
                        $query->where('status_product_treatment', 1);
                    })
                    ->get();

            return view('customers.treatment-balance.usage', compact('invoice', 'outlet', 'product', 'therapist', 'materials'));
        }

        return Redirect::back()->withErrors('No Treatment Selected');
    }

    public function destroy($id)
    {
        $user = Customer::find($id);
        if (!count($user->invoice) || count($user->appointment)) {
            $user->delete();

            // send to izzibook
            // $izzibookCustomer = new CustomerService();
            // $izzibookCustomer->destroy($user);

        }
    }

    public function storeAnamnesa(Request $request, $id)
    {
        $custom = Customer::findOrFail($id);

        $saveAnamnesa = new Anamnesa;
        $saveAnamnesa->anamnesa_code = Anamnesa::generateTreatmentCode('ANM');
        $saveAnamnesa->customer_id = $custom->id_customer;
        $saveAnamnesa->anamnesa = $request->anamnesas;
        $saveAnamnesa->suggestion = $request->suggestion;
        $saveAnamnesa->date_anamnesa =  Carbon::createFromFormat('d/m/Y', $request->date_anamnesa)->format('Y-m-d');
        $saveAnamnesa->save();

        $save_json = json_decode($request->save_json);
        foreach ($save_json as $key => $data) {
            $anamnesaDetail = new AnamnesaDetail;
            $anamnesaDetail->anamnesa_id = $saveAnamnesa->id_anamnesa;
            $anamnesaDetail->product_id = $data->type_id;
            $anamnesaDetail->qty = $data->qty;
            $anamnesaDetail->price = intval(str_replace(array('.', ','), "", $data->price));
            $anamnesaDetail->save();
        }

        return redirect(route('customers.show', $id));

    }
    public function editAnamnesa($id)
    {
        $anamnesa =Anamnesa::find($id);
        $product = array();
        foreach ($anamnesa->anamnesa_detail as $productx) {
            if($productx->product_treatment)
            {
             $product[] = array(
                'type_name' => $productx->product_treatment->product_treatment_name,
                'type_id' => $productx->product_id,
                'qty' => $productx->qty,
                'price' => $productx->price,
                'current_price' => $productx->product_price,
                'id_anamnesa_detail' => $productx->id_anamnesa_detail,
            );
            }
            //dd($productx->product_price);

        }
        return response()->json([
            'product' => $product,
            'anamnesa' => $anamnesa,
        ]);
    }

    public function updateAnamnesa(Request $request, $id)
    {
        $update =Anamnesa::find($request->id_anamnesa);

        $update->anamnesa = $request->anamnesas;
        $update->suggestion = $request->suggestion;
        $update->date_anamnesa =  Carbon::createFromFormat('d/m/Y', $request->date_anamnesa)->format('Y-m-d');


        $save_json = json_decode($request->save_json);
        $product_id = [];
        foreach ($save_json as $key => $data) {
            $product_id[] = $data->type_id;
        }

        $product = AnamnesaDetail::where('anamnesa_id', $update->id_anamnesa)->pluck('product_id')->toArray();
        $productDelete = array_diff($product, $product_id);

        if (count($productDelete)) {
            AnamnesaDetail::where('anamnesa_id', $update->id_anamnesa)
                ->whereIn('product_id', $productDelete)
                ->delete();
        }
        foreach ($save_json as $key => $data) {
            $cek = '';
            if (isset($data->id_anamnesa_detail)) {
                $cek = $data->id_anamnesa_detail;
            }
            AnamnesaDetail::firstOrCreate(
                ['id_anamnesa_detail' => $cek],
                [
                    'anamnesa_id' => $update->id_anamnesa,
                    'product_id' => $data->type_id,
                    'qty' => $data->qty,
                    'price' => intval(str_replace(array('.', ','), "", $data->price)),
                ]
            );
        }
        $update->update();
    }
    public function destroyAnamnesa($id)
    {
        $destroyAnamnesa = new Anamnesa;
        $customer_id = $destroyAnamnesa->find($id)->customer_id;
        $destroyAnamnesa->find($id)->delete();
        AnamnesaDetail::where('anamnesa_id', $id)->delete();

        return redirect(route('customers.show', $customer_id));
    }


    public function storeConsignment(Request $request, $id)
    {
        $custom = Customer::findOrFail($id);

        request()->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,svg',
        ]);

        $saveConsignment = new CustomerConsignment;
        $saveConsignment->consignment_date =  Carbon::createFromFormat('d/m/Y', $request->consignment_date)->format('Y-m-d');
        $saveConsignment->customer_id = $custom->id_customer;

        $image = $request->file('image');
        if ($image) {
            $img_path = $image->store('customerConsigment', 'public');
            $saveConsignment->image = $img_path;
        }

        $saveConsignment->save();
    }
    public function storePhysical(Request $request, $id)
    {
        $custom = Customer::findOrFail($id);

        request()->validate([

            'photodata' => 'required',

        ]);
        $savePhysical = new PhysicalExamination;
        $savePhysical->examination_date =  Carbon::createFromFormat('d/m/Y', $request->examination_date)->format('Y-m-d');
        $savePhysical->customer_id = $custom->id_customer;
        $savePhysical->description = $request->description;

        $filename  = date('mdYHis') . "-physicalexam.png";
        $encoded_image = explode(",", $request->photodata)[1];
        $decoded_image = base64_decode($encoded_image);
        file_put_contents(__DIR__. '/../../../storage/app/public/physicalExamination/'. $filename, $decoded_image);

        $savePhysical->image = 'physicalExamination/'. $filename;


        // $image = $request->file('image');
        // if ($image) {
        //     $img_path = $image->store('physicalExamination', 'public');
        //     $savePhysical->image = $img_path;
        // }

        $savePhysical->save();
    }

    public function destroyPhysical($id)
    {
        $destroyPhysical = new PhysicalExamination;
        $customer_id = $destroyPhysical->find($id)->customer_id;
        $destroyPhysical->find($id)->delete();

        return redirect(route('customers.show', $customer_id));
    }

    public function showPict($id)
    {

        $model =CustomerConsignment::find($id);
        return response()->json($model);

    }
    public function showPictPhysical($id)
    {

        $model =PhysicalExamination::find($id);
        return response()->json($model);

    }

    public function chooseProduct($id)
    {
        $product = ProductTreatment::find($id);
        return json_encode($product);
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function export(Request $request)
    {
        if ( ! in_array("SUPER_USER", json_decode($request->user()->level))) {
            return abort(403);
        }


        $sql = "
            SELECT
                c.induk_customer_old,
                c.induk_customer,
                c.full_name,
                c.nick_name,
                DATE_FORMAT(c.birth_date, '%d/%m/%Y') as birth_date,
                c.phone,
                c.gender,
                c.address,
                prov.province
            FROM
                customers c
                    LEFT JOIN
                provinces prov ON c.city_id = prov.id_province
        ";

        $customers = DB::select($sql);

        $str = "RM LAMA;RM NEOSOFT;NAMA LENGKAP;NAMA PANGGILAN;TANGGAL LAHIR;TELEPON;GENDER;ALAMAT;KOTA\n";

        foreach ($customers as $cust) {
            $str .= sprintf("%s;%s;%s;%s;%s;%s;%s;%s;%s\n",
                $cust->induk_customer_old,
                $cust->induk_customer,
                $cust->full_name,
                $cust->nick_name,
                $cust->birth_date,
                $cust->phone,
                $cust->gender,
                $cust->address,
                $cust->province
            );
        }

        $filename = "customer_export_".date('Y_m_d_').time().".csv";

        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=".$filename);
        header("Pragma: no-cache");
        header("Expires: 0");


        return $str;
    }

    public function exportExcel()
    {
        return Excel::download(new CustomerExport, "customer_excel_".date('Y_m_d_').time().".xlsx");
    }

    public function get_dp_balance($id){

        $invoice_id = Invoice::where('void_invoice', 0)
        ->where('customer_id',$id)
        ->pluck('id_invoice')->toArray();

        $invoiceDetail_id = InvoiceDetail::whereIn('inv_id',$invoice_id)->pluck('id_inv_detail')->toArray();

        $invoicePackage = InvoicePackage::select('id_invoice_package','inv_detail_id','product_id','qty')
        ->whereIn('inv_detail_id',$invoiceDetail_id)->get();

        $dpBalance = 0 ;
        foreach($invoicePackage as $list)
        {
            if(strtolower($list->product_treatment->product_treatment_name) == "dp appointment treatment")
            {
                $dpBalance += $list->qty;
            }
        }

        return json_encode($dpBalance);

    }

    public function saveMedicalConsign(Request $request) {
        $fileUrl = $this->base64PdfSave($request->file,'medicalConsign');
        $model = new MedicalConsign();
        $model->staff = Auth::user()->id;
        $model->customer_id = $request->id_customer;
        $model->file = $fileUrl;
        $model->save();
    }

    public function delMedicalConsign($id) {
        $model = MedicalConsign::find($id);
        Storage::disk('public')->delete(str_replace("/storage/",'', $model->file));
        $model->delete();
    }

    private function base64PdfSave($pdfBase64, $folder) {
        $file = $pdfBase64;  // your base64 encoded
        $safeName = Str::random() . ".pdf";
        $file = str_replace('data:application/pdf;filename=generated.pdf;base64,', '', $file);
        $file = base64_decode($file);
        Storage::disk('public')->put("$folder/" . $safeName, $file);
        return Storage::url("$folder/".$safeName);
    }
}

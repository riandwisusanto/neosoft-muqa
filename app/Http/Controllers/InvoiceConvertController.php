<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Agent;
use App\Models\Bank;
use App\Models\Consultant;
use App\Models\Invoice;
use App\Models\InvoiceDetail;
use App\Models\InvoicePackage;
use App\Models\LogConvert;
use App\Models\MarketingSource;
use App\Models\Outlet;
use App\Models\Package;
use App\Models\Payment;
use App\Models\ProductTreatment;
use App\Models\Therapist;
use App\Models\UserOutlet;
use Auth;
use Carbon\carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class InvoiceConvertController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('sales')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $package = Package::where('status_package', 1)
            ->whereHas('package_outlet', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();

        $product = ProductTreatment::where('status_product_treatment', 1)
            ->whereHas('product_treatment_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        $allItem = $package->concat($product);
        $count = count($allItem);

        $bank = Bank::where('status_bank', 1)
            ->whereHas('bank_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        $consultant = Consultant::where('status_consultant', 1)
            ->whereHas('consultant_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        $marketing_source = MarketingSource::where('status_marketing_source', 1)
            ->whereHas('marketing_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();
        $therapist = Therapist::where('status_therapist', 1)
            ->whereHas('therapist_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })->get();

        $activity = Activity::where('status_activity', 1)
            ->where('end_date', '>=', now()->toDateString())
            ->where('start_date', '<=', now()->toDateString())
            ->whereHas('activity_outlets', function ($q) use ($outlet_id) {
                return $q->whereIn('outlet_id', $outlet_id);
            })
            ->get();

        $agent = Agent::where('status', 0)->whereNotNull('uniq_code')->get();

        return view('invoice.convert.index')
            ->with('outlet', $outlet)
            ->with('consultant', $consultant)
            ->with('marketing_source', $marketing_source)
            ->with('activity', $activity)
            ->with('therapist', $therapist)
            ->with('package', $allItem)
            ->with('count', $count)
            ->with('agent', $agent)
            ->with('bank', $bank);
    }

    public function listData($customer_id)
    {
        $invoice = Invoice::join('invoice_details', 'invoice_details.inv_id', 'invoices.id_invoice')
            ->join('invoice_packages', 'invoice_packages.inv_detail_id', 'invoice_details.id_inv_detail')
            ->join('product_treatments', 'product_treatments.id_product_treatment', 'invoice_packages.product_id')
            ->where('customer_id', $customer_id)->get()->toArray();

        return response()->json($invoice);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store_invoice = new Invoice;
        $store_invoice->inv_code = Invoice::generateInvoiceCode($request->outlet);
        $store_invoice->inv_date = Carbon::createFromFormat('d/m/Y', $request->date_invoice)->format('Y-m-d');
        $store_invoice->expired_date = Carbon::createFromFormat('d/m/Y', $request->date_expired)->format('Y-m-d');
        $store_invoice->customer_id = $request->customer_id;
        $store_invoice->outlet_id = $request->outlet;
        $store_invoice->therapist_id = $request->therapist;
        $store_invoice->consultant_id = $request->consultant;

        $marketingSource = MarketingSource::where('marketing_source_name', 'NONE')->first();
        if($request->marketing_source){
            $store_invoice->marketing_source_id = $request->marketing_source;
        }else{
            $store_invoice->marketing_source_id = $marketingSource->id_marketing_source;
        }
        
        $store_invoice->remarks = $request->remarks;
        $store_invoice->inv_remarks = $request->old_invoice;
        $store_invoice->total = str_replace(array('.', ','), "", $request->total);
        $store_invoice->staff_id = Auth::user()->id;
        $store_invoice->agent_id = $request->agent;
        $store_invoice->remaining = str_replace(array('.', ','), "", $request->unpaid);
        if ($request->unpaid == 0) {
            $store_invoice->status_inv = 1;
        }
        $store_invoice->save();

        $save_json = json_decode($request->save_json);
        foreach ($save_json as $key => $data) {
            $subtotal = str_replace(array('.', ','), "", $data->subtotal);
            $save = new InvoiceDetail;
            $save->inv_id = $store_invoice->id_invoice;
            $save->package_code = $data->code;
            $save->current_price = $data->price;
            $save->qty = $data->qty;
            $save->subtotal = $subtotal;
            $save->discount_1 = $data->discount_1;
            if ($data->discount_2) {
                $save->discount_2 = $data->discount_2;
            }
            $save->type_line = $data->type_line_id;
            $save->activity_id = $data->activity_id;
            $save->save();

            if ($data->package_id) {
                $package = Package::find($data->package_id);
                foreach ($package->package_detail as $set) {
                    $save_package = new InvoicePackage;
                    $save_package->inv_detail_id = $save->id_inv_detail;
                    $save_package->product_id = $set->product_id;
                    $save_package->qty = $set->qty * $data->qty;
                    $save_package->used = 0;

                    $totalx = $set->price * $data->qty;

                    if ($data->discount_2) {
                        if ($data->type_line_id == 0) {
                            $total1 = ($totalx - (($data->discount_1 / 100) * $totalx)) / ($set->qty * $data->qty);
                            $grandTotal = ($total1 - (($data->discount_2 / 100) * $total1));
                        } else {
                            $total1 = ($totalx - $data->discount_1) / ($set->qty * $data->qty);
                            $grandTotal = $total1 - $data->discount_2;
                        }
                    } else {
                        //rp = 1, % = 0
                        if ($data->type_line_id == 0) {
                            if ($data->discount_1 > 0) {
                                $grandTotal = ($totalx - (($data->discount_1 / 100) * $totalx)) / ($set->qty * $data->qty);
                            } else {
                                $grandTotal = $totalx / ($set->qty * $data->qty);
                            }
                        } else {
                            $grandTotal = ($totalx - $data->discount_1) / ($set->qty * $data->qty);
                        }
                    }

                    $save_package->nominal = $grandTotal;

                    $save_package->save();
                }
            } else {
                $save_package = new InvoicePackage;
                $save_package->inv_detail_id = $save->id_inv_detail;
                $save_package->product_id = $data->productTreatment_id;
                $save_package->qty = $data->qty;
                $save_package->used = 0;
                $save_package->nominal = $subtotal / $data->qty;
                $save_package->save();
            }
        }

        $count = count($request->invoice_code);
        for ($i = 0; $i < $count; $i++) {
            if ($request->jumlah[$i] > 0) {
                $log_convert = new LogConvert;
                $log_convert->from_invoice_id = $request->invoice_code[$i];
                $log_convert->to_invoice_id = $store_invoice->inv_code;
                $log_convert->convert_value = $request->total_convert[$i] * $request->jumlah[$i];
                $log_convert->staff_id = Auth::user()->id;
                $log_convert->save();

                $invoice_package = InvoicePackage::where('id_invoice_package', $request->id_inv_package[$i])->first();
                $invoice_package->qty = $request->tot_qty[$i] - $request->jumlah[$i];
                // $invoice_package->used = $request->jumlah[$i];
                $invoice_package->update();
            }
        }

        for ($i = 0; $i < count($request->payment_type); $i++) {
            $payment = new Payment;
            $payment->inv_id = $store_invoice->id_invoice;
            $payment->bank_id = $request->payment_type[$i];
            $payment->amount = $request->amount[$i];
            $payment->info = addslashes($request->payment_info[$i]);
            $payment->save();
        }

        return response()->json([
            'id_invoice' => $store_invoice->id_invoice,
        ]);
    }
    public function generatePdf($id)
    {
        $invoice = Invoice::find($id);
        $payment = Payment::where('inv_id', $id)->get();
        $inv_detail = InvoiceDetail::where('inv_id', $id)->get();
        $log_convert = Invoice::leftJoin('log_converts', 'log_converts.to_invoice_id', 'invoices.inv_code')
            ->where('id_invoice', $id)
            ->get();

        // $dompdf = PDF::loadView('invoice.print', compact('invoice', 'payment', 'inv_detail'));
        // $dompdf->setPaper('a4', 'potrait');
        // return $dompdf->stream('allright.pdf');

        return view('invoice.convert.print', compact('invoice', 'payment', 'inv_detail', 'log_convert'));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Carbon\carbon;
use Auth;
use App\Models\Country;
use App\Models\Outlet;
use App\Models\UserOutlet;
use App\Models\Consultant;
use App\Models\MarketingSource;
use App\Models\Activity;
use App\Models\Therapist;
use App\Models\Appointment;
use App\Models\ProductTreatment;
use App\Models\AppointmentTherapist;
use App\Models\TherapistSchedule;
use App\Models\TherapistLeave;
use App\Models\Room;
use App\Models\LogAppointment;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('appointment')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }

    public function byTherapist()
    {
        $country = Country::all();
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $consultant = Consultant::all();
        $marketing_source = MarketingSource::all();
        $activity = Activity::all();
        $therapist = Therapist::where('status_therapist', 1)->get();
        $productTreatment = ProductTreatment::all();
        $appointment = Appointment::join('customers', 'customers.id_customer', 'appointments.customer_id')
            ->join('appointment_therapists', 'appointment_therapists.appointment_id', 'appointments.id_appointments')
            ->whereIn('outlet_id', $outlet_id)
            ->get();

        $room = Room::all();

        $restraction = Therapist::with([
            'therapist_leave',
            'therapist_schedule',
        ])->get();

        //dd($restraction);

        return view('appointment.therapist')
            ->with('country', $country)
            ->with('outlet', $outlet)
            ->with('consultant', $consultant)
            ->with('productTreatment', $productTreatment)
            ->with('marketing_source', $marketing_source)
            ->with('activity', $activity)
            ->with('restraction', $restraction)
            ->with('therapist', $therapist)
            ->with('room', $room)
            ->with('appointment', $appointment);
    }

    public function byRoom()
    {
        $country = Country::all();
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $consultant = Consultant::all();
        $marketing_source = MarketingSource::all();
        $activity = Activity::all();
        $therapist = Therapist::all();
        $productTreatment = ProductTreatment::all();
        $appointment = Appointment::whereIn('outlet_id', $outlet_id)->get();
        $therapist = Therapist::all();
        $room = Room::all();
        //dd($appointment);
        return view('appointment.room')
            ->with('country', $country)
            ->with('outlet', $outlet)
            ->with('consultant', $consultant)
            ->with('marketing_source', $marketing_source)
            ->with('productTreatment', $productTreatment)
            ->with('activity', $activity)
            ->with('therapist', $therapist)
            ->with('room', $room)
            ->with('appointment', $appointment);
    }

    public function byOutlet()
    {
        $country = Country::all();
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $outlet_change = Outlet::find($outlet->first()->id_outlet);
        $consultant = Consultant::all();
        $marketing_source = MarketingSource::all();
        $activity = Activity::all();
        $therapist = Therapist::all();
        $productTreatment = ProductTreatment::all();
        $appointment = Appointment::whereIn('outlet_id', $outlet_id)->get();
        $therapist = Therapist::all();
        $room = Room::all();
        return view('appointment.outlet')
            ->with('country', $country)
            ->with('outlet', $outlet)
            ->with('outlet_change', $outlet_change)
            ->with('consultant', $consultant)
            ->with('marketing_source', $marketing_source)
            ->with('productTreatment', $productTreatment)
            ->with('activity', $activity)
            ->with('therapist', $therapist)
            ->with('room', $room)
            ->with('appointment', $appointment);
    }

    public function byOutletwithChange($id)
    {
        $country = Country::all();
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $outlet_change = Outlet::find($id);
        $consultant = Consultant::all();
        $marketing_source = MarketingSource::all();
        $activity = Activity::all();
        $therapist = Therapist::all();
        $productTreatment = ProductTreatment::all();
        $appointment = Appointment::where('outlet_id', $id)->get();
        //dd($appointment);
        $therapist = Therapist::all();
        $room = Room::all();
        //dd($appointment);
        return view('appointment.outlet')
            ->with('country', $country)
            ->with('outlet', $outlet)
            ->with('outlet_change', $outlet_change)
            ->with('consultant', $consultant)
            ->with('marketing_source', $marketing_source)
            ->with('productTreatment', $productTreatment)
            ->with('activity', $activity)
            ->with('therapist', $therapist)
            ->with('room', $room)
            ->with('appointment', $appointment);
    }

    public function byTherapistwithChange($id)
    {
        $country = Country::all();
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $consultant = Consultant::all();
        $marketing_source = MarketingSource::all();
        $activity = Activity::all();
        $therapist = Therapist::all();
        $productTreatment = ProductTreatment::all();
        $appointment = Appointment::leftJoin('appointment_therapists', 'appointment_therapists.appointment_id', 'appointments.id_appointments')
            ->where('appointment_therapists.therapist_id', $id)
            ->get();
        //dd($appointment);
        $therapist = Therapist::all();
        $therapistChoose = Therapist::find($id);
        $room = Room::all();
        //dd($appointment);
        return view('appointment.therapistChoose')
            ->with('country', $country)
            ->with('outlet', $outlet)
            ->with('consultant', $consultant)
            ->with('marketing_source', $marketing_source)
            ->with('productTreatment', $productTreatment)
            ->with('activity', $activity)
            ->with('therapist', $therapist)
            ->with('therapistChoose', $therapistChoose)
            ->with('room', $room)
            ->with('appointment', $appointment);
    }

    public function detailAppointment($id_customer, $id_appointment, $date)
    {
        $appointment = Appointment::with([
            'outlet:id_outlet,outlet_name',
            'customer:id_customer,full_name,phone',
            'user:id,name'
        ])
            ->find($id_appointment);
        $data = Appointment::with([
            'product_treatment:id_product_treatment,product_treatment_name',
            'therapist:id_therapist,therapist_name'
        ])
            ->where('customer_id', $id_customer)
            ->where('come_date', $date)
            ->get();

        $result = array('appointment' => $appointment, 'data' => $data);

        echo json_encode($result);
    }

    public function changeStatus(Request $request, $id)
    {
        $update = Appointment::find($id);
        $update->status_appointment = $request->status;
        $update->update();

        $log = new LogAppointment;
        $log->appointment_id = $id;
        $log->user_id = Auth::user()->id;
        $log->log_status = $request->status;
        $log->date_time = now();
        $log->save();
    }

    public function chooseProduct($id, $start_time)
    {
        $treatment = ProductTreatment::find($id);

        $duration = $treatment->duration;
        $start    = date('H:i:s', strtotime($start_time));
        $end    = date('H:i:s', strtotime('+' . $duration . ' minutes', strtotime($start)));

        $result = array('treatment' => $treatment, 'end' => $end, 'start' => $start);
        echo json_encode($result);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return response()->json(" no schedule for time ", 422);
        $dayname = Carbon::createFromFormat('d/m/Y', $request->point_date)->format('l');
        $date = Carbon::createFromFormat('d/m/Y', $request->point_date)->format('Y-m-d');
        foreach ($request->therapist as $key => $value) {
            $therapist = Therapist::whereIdTherapist($value)->first();
            // check leaves
            $therapistLeave = TherapistLeave::whereTherapistId($value)
                ->where('start_date', '>=', $date)
                ->where('end_date', '<=', $date)
                ->first();

            if (!empty($therapistLeave)) {
                return response()->json($therapist->therapist_name . " leave on date " . $therapistLeave->start_date . " to " .  $therapistLeave->end_date, 422);
            }

            // check schedule
            $therapistSchedules = TherapistSchedule::where('day', $dayname)
                ->whereTherapistId($value)
                ->whereTime('start_time', '<=', $request->start)
                ->whereTime('end_time', '>=', date('H:i:s', strtotime('+60 minutes', strtotime($request->start))))
                ->get();
            if (count($therapistSchedules) == 0) {
                return response()->json($therapist->therapist_name . " no schedule", 422);
            }
        }

        $save = new Appointment;
        $save->customer_id = $request->customer_id;
        $save->outlet_id = $request->outlet;
        $save->activity_id = $request->activity;
        $save->come_date = Carbon::createFromFormat('d/m/Y', $request->point_date)->format('Y-m-d');
        $save->status_appointment = $request->status;
        $save->start_time = $request->start;
        if ($request->end_time) {
            $save->end_time = $request->end_time;
            $save->treatment_id = $request->pack_name;
        } else {
            $start = date('H:i:s', strtotime($request->start));
            $end = date('H:i:s', strtotime('+60 minutes', strtotime($start)));
            $save->end_time = $end;
            // $save->treatment_id = 0;
        }
        $save->room_id = $request->id_rooms;
        $save->remarks = $request->remarks;
        $save->staff_id = Auth::user()->id; //get user login;
        $save->save();

        foreach ($request->therapist as $key => $value) {
            $appointmentThe = new AppointmentTherapist;
            $appointmentThe->therapist_id = $value;
            $appointmentThe->appointment_id = $save->id_appointments;
            $appointmentThe->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $appointment = Appointment::find($id);
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $consultant = Consultant::all();
        $marketing_source = MarketingSource::all();
        $activity = Activity::all();
        $therapist = Therapist::all();
        $productTreatment = ProductTreatment::all();
        $room = Room::all();

        $data = Appointment::where('customer_id', $appointment->customer_id)
            ->get();

        $log = LogAppointment::leftJoin('appointments', 'appointments.id_appointments', 'log_appointments.appointment_id')
            ->join('users', 'users.id', 'log_appointments.user_id')
            ->where('appointments.id_appointments', $id)
            ->get();

        return view('appointment.action.detail')
            ->with('appointment', $appointment)
            ->with('outlet', $outlet)
            ->with('consultant', $consultant)
            ->with('marketing_source', $marketing_source)
            ->with('activity', $activity)
            ->with('therapist', $therapist)
            ->with('productTreatment', $productTreatment)
            ->with('room', $room)
            ->with('data', $data)
            ->with('log', $log);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $appointment = Appointment::with([
            'outlet:id_outlet,outlet_name',
            'customer:id_customer,induk_customer,full_name,phone,birth_date',
            'user:id,name'
        ])
            ->find($id);

        // $show = Appointment::join('product_treatments', 'product_treatments.id_product_treatment', 'appointments.treatment_id')
        //     ->join('customers', 'customers.id_customer', 'appointments.customer_id')
        //     ->where('id_appointments', $id)
        //     ->first();

        $therapist = AppointmentTherapist::where('appointment_id', $id)->get();
        $array = [];
        foreach ($therapist as $value) {
            $array[] = "" . $value->therapist_id . "";
        }

        //dd($array);
        $result = array('show' => $appointment, 'therapist' => $array);
        //dd($therapist);

        //dd($show->come_date);
        return json_encode($result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function storeReschedule(Request $request, $id)
    {
        $dayname = Carbon::createFromFormat('d/m/Y', $request->point_date)->format('l');
        $date = Carbon::createFromFormat('d/m/Y', $request->point_date)->format('Y-m-d');
        foreach ($request->therapist as $key => $value) {
            $therapist = Therapist::whereIdTherapist($value)->first();
            // check leaves
            $therapistLeave = TherapistLeave::whereTherapistId($value)
                ->where('start_date', '>=', $date)
                ->where('end_date', '<=', $date)
                ->first();

            if (!empty($therapistLeave)) {
                return response()->json($therapist->therapist_name . " leave on date " . $therapistLeave->start_date . " to " .  $therapistLeave->end_date, 422);
            }

            // check schedule
            $therapistSchedules = TherapistSchedule::where('day', $dayname)
                ->whereTherapistId($value)
                ->whereTime('start_time', '<=', $request->start)
                ->whereTime('end_time', '>=', date('H:i:s', strtotime('+60 minutes', strtotime($request->start))))
                ->get();
            if (count($therapistSchedules) == 0) {
                return response()->json($therapist->therapist_name . " no schedule", 422);
            }
        }

        $save = new Appointment;
        $save->customer_id = $request->customer_id;
        $save->outlet_id = $request->outlet;
        $save->activity_id = $request->activity;
        $save->come_date = Carbon::createFromFormat('d/m/Y', $request->point_date)->format('Y-m-d');
        $save->status_appointment = $request->status;
        $save->start_time = $request->start;
        if ($request->end_time) {
            $save->end_time = $request->end_time;
            $save->treatment_id = $request->pack_name;
        } else {
            $start = date('H:i:s', strtotime($request->start));
            $end = date('H:i:s', strtotime('+60 minutes', strtotime($start)));
            $save->end_time = $end;
            $save->treatment_id = $request->pack_name;

            // $save->treatment_id = 0;
        }
        $save->room_id = $request->id_rooms;
        $save->remarks = $request->remarks;
        $save->staff_id = Auth::user()->id; //get user login;
        $save->save();

        foreach ($request->therapist as $key => $value) {
            $appointmentThe = new AppointmentTherapist;
            $appointmentThe->therapist_id = $value;
            $appointmentThe->appointment_id = $save->id_appointments;
            $appointmentThe->save();
        }
        $appointmentOld = Appointment::find($id);
        $appointmentOld->status_appointment = 4;
        $appointmentOld->update();

    }
    public function update(Request $request, $id)
    {
        $dayname = Carbon::createFromFormat('d/m/Y', $request->point_date)->format('l');
        $date = Carbon::createFromFormat('d/m/Y', $request->point_date)->format('Y-m-d');
        foreach ($request->therapist as $key => $value) {
            $therapist = Therapist::whereIdTherapist($value)->first();
            // check leaves
            $therapistLeave = TherapistLeave::whereTherapistId($value)
                ->where('start_date', '>=', $date)
                ->where('end_date', '<=', $date)
                ->first();

            if (!empty($therapistLeave)) {
                return response()->json($therapist->therapist_name . " leave on date " . $therapistLeave->start_date . " to " .  $therapistLeave->end_date, 422);
            }

            // check schedule
            $therapistSchedules = TherapistSchedule::where('day', $dayname)
                ->whereTherapistId($value)
                ->whereTime('start_time', '<=', $request->start)
                ->whereTime('end_time', '>=', date('H:i:s', strtotime('+60 minutes', strtotime($request->start))))
                ->get();
            if (count($therapistSchedules) == 0) {
                return response()->json($therapist->therapist_name . " no schedule", 422);
            }
        }

        $save = Appointment::find($id);
        $save->customer_id = $request->customer_id;
        $save->outlet_id = $request->outlet;
        $save->activity_id = $request->activity;
        $save->come_date = Carbon::createFromFormat('d/m/Y', $request->point_date)->format('Y-m-d');
        //$save->come_date = $request->point_date;
        $save->start_time = $request->start;
        $save->status_appointment = $request->status;
        if ($request->end_time) {
            $save->end_time = $request->end_time;
            $save->treatment_id = $request->pack_name;
        } else {
            $start = date('H:i:s', strtotime($request->start));
            $end = date('H:i:s', strtotime('+60 minutes', strtotime($start)));
            $save->end_time = $end;
            // $save->treatment_id = 0;
        }
        // $save->start_time = $request->start_input;
        // $save->end_time = $request->end_time;
        $save->treatment_id = $request->pack_name;
        $save->room_id = $request->id_rooms;
        $save->remarks = $request->remarks;
        $save->staff_id = Auth::user()->id; //get user login;
        $save->update();


        $before = AppointmentTherapist::where('appointment_id', $id)->pluck('therapist_id')->toArray();
        $shouldDelete = array_diff($before, $request->therapist);

        if (count($shouldDelete)) {
            AppointmentTherapist::where('appointment_id', $id)
                ->whereIn('therapist_id', $shouldDelete)
                ->delete();
        }

        foreach ($request->therapist as $therapist_id) {
            AppointmentTherapist::firstOrCreate([
                'appointment_id'     => $id,
                'therapist_id'   => $therapist_id
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Appointment::find($id);
        $user->delete();

        $before = AppointmentTherapist::where('appointment_id', $id)->pluck('therapist_id')->toArray();
        AppointmentTherapist::where('appointment_id', $id)
            ->whereIn('therapist_id', $before)
            ->delete();
    }
}

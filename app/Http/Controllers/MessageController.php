<?php
 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Message;
use App\Models\MessageDetail;
use App\Models\Customer;
use Illuminate\Support\Facades\Gate;
use Auth;
use DataTables;
use KrmPesan\Client;

class MessageController extends Controller
{
   public function __construct()
   {
       $this->middleware('auth');
       $this->middleware(function ($request, $next) {
           if (Gate::allows('admin')) {
               return $next($request);
           }
 
           abort(403, 'You do not have enough access rights');
       });
   }
   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index()
   {
        $customer = Customer::select('id_customer','birth_date','full_name','point')->orderBy('full_name', 'asc')->get();
        $customer1 = Customer::select('id_customer','birth_date','full_name','point')->orderBy('id_customer', 'asc')->skip(0)->take(900)->get();
        $customer2 = Customer::select('id_customer','birth_date','full_name','point')->orderBy('id_customer', 'asc')->skip(900)->take(1800)->get();
        return view('message.index')
            ->with('customer', $customer)
            ->with('customer1', $customer1)
            ->with('customer2', $customer2);
   }
 
   public function listData()
   {
 
       $collection = Message::all();
 
       $data = array();
       foreach ($collection as $messageIndex => $message) {
           $row = [];
           $row[] = $messageIndex + 1;
           $row[] = $message->message;

           $custom= '';
           foreach($message->detail as $det)
           {
               
               $custom .= "&middot;" .$det->customer->phone."<br>";
 
           }
           $row[] = $custom;
            $row[] = '<img src=\''.'storage/' . $message->image . '\' alt = \'no image\' class=\'img-responsive\'>';
           $row[] = '<div class="btn-group-vertical">
                       <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="printWa(\'' . $message->id_message . '\')">
                           <i class="fa fa-edit"></i>
                           Send Wa
                       </a>
                   </div>';
           $data[] = $row;
       }
 
       return DataTables::of($data)->escapeColumns([])->make(true);
   }
 
   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {
       //
   }
 
   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {
    //    dd($request->all());
        // $img = $request->file('image');
        // dd(mime_content_type($img));
        $save = new Message;
        $save->message = $request->message;

        $img = $request->file('image');
        if ($img != '') {
            $img_path = $img->store('imageMessage', 'public');
            $save->image = $img_path;
        }
  
        $save->save();
 
       foreach ($request->customer_id as $key => $value) {
           $customer = new MessageDetail;
           $customer->message_id = $save->id_message;
           $customer->customer_id = $value;
           $customer->save();
       }
    //    return response()->json([
    //     'id_message' => $save->id_message,
    //     ]);
        return redirect()->action('MessageController@index');
   }
 
   public function sendWhatsApp($id)
   {
       $wa = new Client([
        'region' => env('WA_REGION'),
        'token' => env('WA_TOKEN')
       ]);

       $dataMessage = Message::find($id);
       foreach ($dataMessage->detail as $det) {
            $phone = $det->customer->phone;  
            $message = $dataMessage->message;
            $img = $dataMessage->image;
            $file = realpath('storage/'.$img);
            $filename = basename($file);
            $filemime = mime_content_type($file);
            if ($img == '') {
                $send = $wa->sendMessageText($phone, $message);   
            } else {
                $send = $wa->sendMessageImage($phone, $file, $message);
            }
       }

       return $send;
   }
   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function show($id)
   {
       //
   }
 
   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function edit($id)
   {
       //
   }
 
   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request, $id)
   {
       //
   }
 
   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function destroy($id)
   {
       //
   }
}
 

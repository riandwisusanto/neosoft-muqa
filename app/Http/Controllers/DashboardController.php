<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\InvoiceDetail;
use App\Models\Payment;
use App\Models\Province;
use App\Models\Customer;
use App\Models\CustomerOutlet;
use App\Models\Outlet;
use App\Models\Usage;
use App\Models\UserOutlet;
use Auth;
use Carbon\carbon;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /** @var Type $var description */
    protected $dateRanges = [];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->dateRanges = [
            'today' => [
                'from' => date('Y-m-d'),
                'to' => date('Y-m-d'),
                'groupType' => 1
            ],
            'yesterday' => [
                'from' => now()->yesterday()->format('Y-m-d'),
                'to' => now()->yesterday()->format('Y-m-d'),
                'groupType' => 1
            ],
            'thisweek' => [
                'from' => now()->startOfWeek()->format('Y-m-d'),
                'to' => now()->endOfWeek()->format('Y-m-d'),
                'groupType' => 1
            ],
            'lastweek' => [
                'from' => now()->sub(1, 'week')->startOfWeek()->format('Y-m-d'),
                'to' => now()->sub(1, 'week')->endOfWeek()->format('Y-m-d'),
                'groupType' => 1
            ],
            'thismonth' => [
                'from' => now()->startOfMonth()->format('Y-m-d'),
                'to' => now()->endOfMonth()->format('Y-m-d'),
                'groupType' => 1
            ],
            'lastmonth' => [
                'from' => now()->sub(1, 'month')->startOfMonth()->format('Y-m-d'),
                'to' => now()->sub(1, 'month')->endOfMonth()->format('Y-m-d'),
                'groupType' => 1
            ],
            'thisyear' => [
                'from' => now()->startOfYear()->format('Y-m-d'),
                'to' => now()->endOfYear()->format('Y-m-d'),
                'groupType' => 2
            ],
            'lastyear' => [
                'from' => now()->sub(1, 'year')->startOfYear()->format('Y-m-d'),
                'to' => now()->sub(1, 'year')->endOfYear()->format('Y-m-d'),
                'groupType' => 2
            ],
            'quarter1' => [
                'from' => date('Y').'-01-01',
                'to' => date('Y').'-03-31',
                'groupType' => 2
            ],
            'quarter2' => [
                'from' => date('Y').'-04-01',
                'to' => date('Y').'-06-30',
                'groupType' => 2
            ],
            'quarter3' => [
                'from' => date('Y').'-07-01',
                'to' => date('Y').'-09-30',
                'groupType' => 2
            ],
            'quarter4' => [
                'from' => date('Y').'-10-01',
                'to' => date('Y').'-12-31',
                'groupType' => 2
            ],
            'semester1' => [
                'from' => date('Y').'-01-01',
                'to' => date('Y').'-06-30',
                'groupType' => 2
            ],
            'semester2' => [
                'from' => date('Y').'-07-01',
                'to' => date('Y').'-12-31',
                'groupType' => 2
            ],
            'customperiod' => [
                'from' => null,
                'to' => null,
                'groupType' => 1
            ]
        ];

        $this->middleware('auth');
        $this->middleware(function($request, $next){
            if(Gate::allows('admin')) {
                return $next($request);
            }else if(Gate::allows('pharmacy')){
                return redirect('/suppliers');
            }else{
                return redirect('/by-room');
            }
        })->except('changePassword', 'updatePassword');
    }

    public function changePassword()
    {
        return view('users.changepassword');
    }

    public function updatePassword(Request $request)
    {
        if (!(Hash::check($request->get('current_password'), auth()->user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error", "Your current password does not matches with the password you provided. Please try again.");
        }
        if (Hash::check(auth()->user()->password, $request->get('new_password'))) {
            //Current password and new password are same
            return redirect()->back()->with("error", "New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|string|min:6|confirmed',
        ]);

        $user = auth()->user();
        $user->password = bcrypt($request->get('new_password'));
        $user->save();
        return redirect()->back()->with("success", "Password changed successfully !");
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $outlet_user = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_user)->get();

        return view('dashboard')->with('outlet', $outlet);
    }

    public function allData(Request $request)
    {
        $outlet_user = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $cekRange = isset($request->daterange) ? $request->daterange : 'today';
        $outlet = isset($request->outlet) ? $request->outlet : $outlet_user;
        $count_outlet = count($outlet);
        if ($cekRange == 'customperiod') {
            $date = explode(' - ', $request->daterangepicker);
            $this->dateRanges['customperiod']['from'] = Carbon::parse($date[0])->format('Y-m-d');
            $this->dateRanges['customperiod']['to'] = Carbon::parse($date[1])->format('Y-m-d');
        }
        $range = $this->dateRanges[$cekRange];
        $from = $range['from'];
        $to = $range['to'];
        $groupType = $range['groupType'];
        $date = isset($range['date']) ? $range['date'] : '';

        $fileCustomerNewTotal = file_get_contents(__DIR__. '/Queries/customerNewTotalDashboard.sql.tpl');
        $queryCustomerTotal = sprintf($fileCustomerNewTotal, implode(',', $outlet), '2000-01-01', $to);
        $queryCustomerNew = sprintf($fileCustomerNewTotal, implode(',', $outlet), $from, $to);
        $customer = count(DB::select($queryCustomerTotal));
        $new_customer = count(DB::select($queryCustomerNew));
        $old_customer = $customer - $new_customer;
        $new_customer_percent = $customer > 0 ? round($new_customer / $customer * 100, 1) ."%" : "0%";
        $old_customer_percent = $customer > 0 ? round($old_customer / $customer * 100, 1) ."%" : "0%";
        $patientChart = [$new_customer, $old_customer];
        // $patientChart = [
        //     ['Status', 'Total'],
        //     ['New Patient', $new_customer],
        //     ['Existing Patient', $old_customer]
        // ];

        $fileCollection = file_get_contents(__DIR__. '/Queries/collection.sql.tpl');
        $queryCollection = sprintf($fileCollection, implode(',', $outlet), $from, $to);
        $collection = DB::select($queryCollection);
        foreach ($collection as $col) {
            $collection_total = "Rp. ". format_money($col->collection);
        }
        $fileRevenue = file_get_contents(__DIR__. '/Queries/revenue.sql.tpl');
        $queryRevenue = sprintf($fileRevenue, implode(',', $outlet), $from, $to);
        $revenue = DB::select($queryRevenue);
        $revenue_total = "Rp. ". 0;
        foreach ($revenue as $rev) {
            $revenue_total = "Rp. ". format_money($rev->revenue);
        }

        if ($groupType == 1) $groupBy = 'inv_date';
        else if ($groupType == 2) $groupBy = 'MONTH(inv_date)';
        $fileChartProductTreatment = file_get_contents(__DIR__. '/Queries/chartProductTreatment.sql.tpl');
        $queryChartProductTreatment = $count_outlet <= 1 ? sprintf($fileChartProductTreatment, implode(',', $outlet), $from, $to, $groupBy, '') : sprintf($fileChartProductTreatment, implode(',', $outlet), $from, $to, 'all_data.outlet_name', 'ORDER BY all_data.outlet_id ASC');
        $chartProductTreatment = DB::select($queryChartProductTreatment);
        $chartProductTreatmentLabels = [];
        $chartProduct = [];
        $chartTreatment = [];
        foreach ($chartProductTreatment as $key => $cpt) {
            if ($count_outlet <= 1) {
                if ($groupType == 1) $indicator = date('d', strtotime($cpt->inv_date));
                else if ($groupType == 2) $indicator = date('M', strtotime($cpt->inv_date));
            } else {
                $indicator = $cpt->outlet_name;
            }
            $chartProductTreatmentLabels[$key] = $indicator;
            $chartProduct[$key] = (int)$cpt->product;
            $chartTreatment[$key] = (int)$cpt->treatment;
        }
        // $chart_product_treatment[] = ['', 'Product', 'Treatment'];
        // foreach ($chartProductTreatment as $key => $cpt) {
        //     $no = $key + 1;
        //     if ($count_outlet <= 1) {
        //         if ($groupType == 1) $indicator = date('d', strtotime($cpt->inv_date));
        //         else if ($groupType == 2) $indicator = date('M', strtotime($cpt->inv_date));
        //     } else {
        //         $indicator = $cpt->outlet_name;
        //     }
        //     $chart_product_treatment[$no] = [$indicator, (int)$cpt->product, (int)$cpt->treatment];
        // }

        $linkFileChartCollection = $count_outlet <= 1 ? '/Queries/chartCollection.sql.tpl' : '/Queries/chartCollectionMultipleOutlet.sql.tpl';
        $fileChartCollection = file_get_contents(__DIR__. $linkFileChartCollection);
        $queryChartCollection = sprintf($fileChartCollection, implode(',', $outlet), $from, $to, $groupBy);
        $invoice_collection = DB::select($queryChartCollection);
        $chartCollectionLabels = [];
        $chartCollectionData = [];
        foreach ($invoice_collection as $key => $invc) {
            if ($count_outlet <= 1) {
                if ($groupType == 1) $indicator = date('d', strtotime($invc->inv_date));
                else if ($groupType == 2) $indicator = date('M', strtotime($invc->inv_date));
            } else {
                $indicator = $invc->outlet_name;
            }
            $total_collection = (int)$invc->collection;
            $chartCollectionLabels[$key] = $indicator;
            $chartCollectionData[$key] = $total_collection;
        }
        // $chart_collection[] = ['', 'Collection'];
        // foreach ($invoice_collection as $key => $invc) {
        //     $no = $key + 1;
        //     if ($count_outlet <= 1) {
        //         if ($groupType == 1) $indicator = date('d', strtotime($invc->inv_date));
        //         else if ($groupType == 2) $indicator = date('M', strtotime($invc->inv_date));
        //     } else {
        //         $indicator = $invc->outlet_name;
        //     }
        //     $total_collection = (int)$invc->collection;
        //     $chart_collection[$no] = [$indicator, $total_collection];
        // }

        return response()->json([
            'from' => $from,
            'to' => $to,
            // 'chart_product_treatment' => $chart_product_treatment,
            'chart_product_treatment_labels' => $chartProductTreatmentLabels,
            'chart_product' => $chartProduct,
            'chart_treatment' => $chartTreatment,
            // 'chart_collection' => $chart_collection,
            'chart_collection_labels' => $chartCollectionLabels,
            'chart_collection_data' => $chartCollectionData,
            'patient_chart' => $patientChart,
            'customer' => $customer,
            'new_customer' => $new_customer,
            'old_customer' => $old_customer,
            'new_customer_percent' => $new_customer_percent,
            'old_customer_percent' => $old_customer_percent,
            'collection' => $collection_total,
            'revenue' => $revenue_total
        ]);
    }

    public function geografis(Request $request) {
        $outlet_user = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $cekRange = isset($request->daterange) ? $request->daterange : 'today';
        $outlet = isset($request->outlet) ? $request->outlet : $outlet_user;
        if ($cekRange == 'customperiod') {
            $date = explode(' - ', $request->daterangepicker);
            $this->dateRanges['customperiod']['from'] = Carbon::parse($date[0])->format('Y-m-d');
            $this->dateRanges['customperiod']['to'] = Carbon::parse($date[1])->format('Y-m-d');
        }
        $range = $this->dateRanges[$cekRange];
        $from = $range['from'];
        $to = $range['to'];
        $date = isset($range['date']) ? $range['date'] : '';

        $tplQuery = file_get_contents(__DIR__. '/Queries/geographics.sql.tpl');
        $query = sprintf($tplQuery, implode(',', $outlet), $from, $to);
        $result = DB::select($query);
        $data = array_map(function ($row) {
            return [
                0,
                $row->city_name,
                $row->num_of_customers,
                $row->num_of_inv ?? 0,
                $row->num_of_items ?? 0,
                format_money($row->invoice_total),
            ];
        }, $result);

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function productRanking(Request $request)
    {
        $outlet_user = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $cekRange = isset($request->daterange) ? $request->daterange : 'today';
        $outlet = isset($request->outlet) ? $request->outlet : $outlet_user;
        if ($cekRange == 'customperiod') {
            $date = explode(' - ', $request->daterangepicker);
            $this->dateRanges['customperiod']['from'] = Carbon::parse($date[0])->format('Y-m-d');
            $this->dateRanges['customperiod']['to'] = Carbon::parse($date[1])->format('Y-m-d');
        }
        $range = $this->dateRanges[$cekRange];
        $from = $range['from'];
        $to = $range['to'];
        $date = isset($range['date']) ? $range['date'] : '';

        $tplQuery = file_get_contents(__DIR__. '/Queries/productTreatmentRanking.sql.tpl');
        $query = sprintf($tplQuery, implode(',', $outlet), '%P%', $from, $to);
        $result = DB::select($query);
        $data = array_map(function ($row) {
            return [
                $row->product_treatment_code,
                $row->product_treatment_name,
                $row->total_qty ?? 0,
            ];
        }, $result);

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function treatmentRanking(Request $request)
    {
        $outlet_user = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $cekRange = isset($request->daterange) ? $request->daterange : 'today';
        $outlet = isset($request->outlet) ? $request->outlet : $outlet_user;
        if ($cekRange == 'customperiod') {
            $date = explode(' - ', $request->daterangepicker);
            $this->dateRanges['customperiod']['from'] = Carbon::parse($date[0])->format('Y-m-d');
            $this->dateRanges['customperiod']['to'] = Carbon::parse($date[1])->format('Y-m-d');
        }
        $range = $this->dateRanges[$cekRange];
        $from = $range['from'];
        $to = $range['to'];

        $tplQuery = file_get_contents(__DIR__. '/Queries/productTreatmentRanking.sql.tpl');
        $query = sprintf($tplQuery, implode(',', $outlet), '%T%', $from, $to);
        $result = DB::select($query);
        $data = array_map(function ($row) {
            return [
                $row->product_treatment_code,
                $row->product_treatment_name,
                $row->total_qty ?? 0,
            ];
        }, $result);

        return DataTables::of($data)->escapeColumns([])->make(true);
    }
}

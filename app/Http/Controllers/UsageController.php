<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Auth;
use Carbon\Carbon;
use App\Models\Usage;
use App\Models\UsageDetail;
use App\Models\UsageTherapist;
use App\Models\InvoicePackage;
use App\Models\LogUsage;
use App\Models\Logistics\Item;
use App\Models\Service;
use App\Models\ServiceDetail;
use App\Models\ProductTreatment;
use App\Models\Therapist;
use App\Models\UserOutlet;
use App\Models\Logistics\Mutation;
use App\Models\Logistics\InventoryAdjustment;
use App\Models\Logistics\Bom;

use App\Models\Outlet;
use App\Models\WarehouseOutlet;
use App\Models\Invoice;
use App\Models\AnamnesaDetail;
use App\Models\AnamnesaBom;

use Illuminate\Support\Facades\Log;
use App\Models\Queue;
use Illuminate\Support\Facades\DB;

class UsageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('customer')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }

    public function store(Request $request)
    {

        // $dayname = Carbon::createFromFormat('d/m/Y', $request->point_date)->format('l');
        // $date = Carbon::createFromFormat('d/m/Y', $request->point_date)->format('Y-m-d');
        // foreach ($request->therapist as $key => $value) {
        //     $therapist = Therapist::whereIdTherapist($value)->first();
        //     // check leaves
        //     $therapistLeave = TherapistLeave::whereTherapistId($value)
        //         ->where('start_date', '>=', $date)
        //         ->where('end_date', '<=', $date)
        //         ->first();

        //     if (!empty($therapistLeave)) {
        //         return response()->json($therapist->therapist_name . " leave on date " . $therapistLeave->start_date . " to " .  $therapistLeave->end_date, 422);
        //     }

        //     // check schedule
        //     $therapistSchedules = TherapistSchedule::where('day', $dayname)
        //         ->whereTherapistId($value)
        //         ->whereTime('start_time', '<=', $request->start)
        //         ->whereTime('end_time', '>=', date('H:i:s', strtotime('+60 minutes', strtotime($request->start))))
        //         ->get();
        //     if (count($therapistSchedules) == 0) {
        //         return response()->json($therapist->therapist_name . " no schedule", 422);
        //     }
        // }

        for ($i = 0; $i < count($request->product_id); $i++) {
            $invoice_package = InvoicePackage::find($request->inv_package_id[$i]);
            $hasil = $invoice_package->qty - $request->usage[$i];
            if ($hasil < 0 || $invoice_package->qty <= 0) {
                return false;
            }
        }
        DB::beginTransaction();
        try {
            $usage = new Usage;
            $usage->usage_code = Usage::generateUsageCode($request->outlet);
            $usage->usage_date = Carbon::createFromFormat('d/m/Y', $request->utilisationdate)->format('Y-m-d');
            $usage->customer_id = $request->customer_id;
            $usage->outlet_id = $request->outlet;
            $usage->staff_id = Auth::user()->id;
            $usage->remarks = $request->remarks;
            $usage->status_usage = 0;
            $usage->rating = $request->rating;

            $filename       = date('mdYHis') . "-signature.png";
            if (isset($request->signature)) {
                $usage->file    = $filename;
                    $data_uri = explode(',', $request->signature);
                    $encoded_image = $data_uri[1];
                    $decoded_image = base64_decode($encoded_image);
                    file_put_contents(__DIR__ . '/../../../storage/app/public/customer/' . $filename, $decoded_image);
            }

            $usage->save();

            for ($i = 0; $i < count($request->product_id); $i++) {
                $invoice_package = InvoicePackage::find($request->inv_package_id[$i]);
                $usage_detail = new UsageDetail;
                $usage_detail->usage_id = $usage->id_usage;
                $usage_detail->inv_code = $request->inv_code[$i];
                $usage_detail->product_id = $request->product_id[$i];
                $usage_detail->invoice_package_id = $request->inv_package_id[$i];
                $usage_detail->point = $request->point[$i];

                $usage_detail->qty = $invoice_package->qty - $request->usage[$i];
                $usage_detail->used = $request->usage[$i];
                $usage_detail->duration = $request->durasi[$i];
                $usage_detail->value = $invoice_package->nominal;
                $usage_detail->save();

                $invoice_package->used = $invoice_package->used + $request->usage[$i];
                $invoice_package->qty = $invoice_package->qty - $request->usage[$i];
                $invoice_package->update();

                $therapist = "therapist_" . $request->inv_package_id[$i];
                for ($z = 0; $z < count($request->$therapist); $z++) {
                    $usage_therapist = new UsageTherapist;
                    $usage_therapist->usage_detail_id = $usage_detail->id_usage_detail;
                    $usage_therapist->therapist_id = $request->$therapist[$z];
                    $usage_therapist->save();
                }
            }

            $queue = Queue::where('customer_id',$usage->customer_id)->count();
            if($queue)
            {
                $updateQueue = Queue::where('customer_id',$usage->customer_id)->latest('created_at')->first();
                $updateQueue->status_queue = 2;
                $updateQueue->update();
            }

            $failed = $this->createMutations($request, $usage);
            $materialsFailed = $this->createMaterialMutations($request, $usage);

            if ($failed || $materialsFailed) {
                DB::rollback();
                return response()->json([
                    'status'    => 'failed',
                    'message'   => $materialsFailed.$failed
                ],500);
            }

            DB::commit();
            return response()->json([
                'status' => 'success',
                'id_usage' => $usage->id_usage,
            ]);

        } catch (Exception $e) {
            DB::rollback();

            return response()->json([
                'message'   => $e->getMessage()
            ], 500);
        }

    }

   /**
     * Create mutations.
     *
     * @return void
     **/
    public function createMutations(Request $request, $usage)
    {
        foreach ($request->product_id as $index => $id) {
            $product = ProductTreatment::find($id);
            $check = '';

            if ($product->item->type == 'product') {
                $usageCount = $request->usage[$index];
                $currentStock = $this->getStock($product->item->id, $request->warehouse_id);

                if ($usageCount > $currentStock) {
                    $check = 'Stok produk '. $product->item->name. ' tidak mencukupi';
                }

                $mutation = [
                    'warehouse_origin_id' => $request->warehouse_id,
                    'rack_origin_id' => null,
                    'sales_invoice_id' => $usage->id_usage,
                    'item_id' => $product->item->id,
                    'transdate' => Carbon::createFromFormat('d/m/Y', $request->utilisationdate)->format('Y-m-d'),
                    'transtype' => 'out',
                    'cogs'     => 0,
                    'price'     => str_replace(',', '', $product->item->sales->price),
                    'ref_id'    => null,
                    'balance'   => 0,
                    'transdesc' => 'USAGE',
                    'qty' => $usageCount * -1
                ];

                Mutation::create($mutation);
            }
        }

        return $check;
    }

    public function createMaterialMutations(Request $request, $usage)
    {
        $materials = json_decode($request->materials);
        if (count($materials) == 0) {
            return false;
        }
        $check = [];
        foreach ($materials as $material) {
            if(!empty($material->fix))
            {
                $usageCount = $request->usage[$material->treatment_index] * $material->qty;
            }else{
                $usageCount = $material->qty;
            }
            $currentStock = $this->getStock($material->material_id, $request->warehouse_id);

            if ($usageCount > $currentStock) {
                $item = Item::find($material->material_id);
                $check[] =  "- Stok material " . $item->name . " tidak mencukupi"."\n";
            }

            $mutation = [
                'warehouse_origin_id' => $request->warehouse_id,
                'rack_origin_id' => null,
                'sales_invoice_id' => $usage->id_usage,
                'raw_item_id'   => $material->treatment_id,
                'item_id'   => $material->material_id,
                'transdate' => Carbon::createFromFormat('d/m/Y', $request->utilisationdate)->format('Y-m-d'),
                'transtype' => 'out',
                'cogs'      => 0,
                'price'     => 0,
                'ref_id'    => null,
                'balance'   => 0,
                'transdesc' => 'USAGE',
                'qty' => $usageCount * -1
            ];

            Mutation::create($mutation);
        }
        return implode('',$check);
    }

    public function printUsage($id)
    {
        $usage = Usage::find($id);
        // $dompdf = PDF::loadView('invoice.print', compact('invoice', 'payment', 'inv_detail'));
        // $dompdf->setPaper('a4', 'potrait');
        // return $dompdf->stream('allright.pdf');

        return view('customers.treatment-balance.print', compact('usage'));
    }

    public function voidUsage(Request $request, $id)
    {

        DB::beginTransaction();
        try {
            $saveLog = new LogUsage;
            $saveLog->usage_id = $id;
            $saveLog->staff_id = Auth::user()->id;
            $saveLog->remarks = $request->remarks;
            $saveLog->save();

            $usage = Usage::find($id);
            $usage->status_usage = 1;
            $usage->remarks = $request->remarks;
            $usage->update();

            for ($i = 0; $i < count($request->inv_package); $i++) {
                $invoice_package = InvoicePackage::find($request->inv_package[$i]);
                $invoice_package->qty = $request->used[$i] + $invoice_package->qty;
                $invoice_package->used = $invoice_package->used - $request->used[$i];
                $invoice_package->update();
            }

            $this->createVoidAdjustment($request, $usage);

            $getDetail = UsageDetail::where('usage_id', $id)->pluck('id_usage_detail')->toArray();
            UsageTherapist::whereIn('usage_detail_id', $getDetail)->delete();
            UsageDetail::where('usage_id', $id)->delete();
            DB::commit();

            return response()->json([
                'model' => $usage
            ]);
        }catch (Exception $e) {
            DB::rollback();

            return response()->json([
                'message'   => $e->getMessage()
            ], 500);
        }
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function getStock($item_id, $warehouse_id)
    {
        $sql = 'select IFNULL(SUM(qty), 0) as qty from mutations where (warehouse_origin_id = ? or warehouse_destination_id = ?) and item_id = ?';
        $result = DB::select($sql, [$warehouse_id, $warehouse_id, $item_id]);

        if (count($result) == 0) return 0;

        return $result[0]->qty;
    }

    /**
     * Inventory adjustment for void usage.
     **/
    public function createVoidAdjustment(Request $request, $usage)
    {
        $logs = Mutation::where('sales_invoice_id', $usage->id_usage)->get();
        foreach ($logs as $log) {
            $stockBefore = $this->getStock($log->item_id, $log->warehouse_origin_id);
            $currentStock = $stockBefore + ($log->qty * -1);
            $adjustmentData = [
                'item_id'               => $log->item_id,
                'warehouse_id'          => $log->warehouse_origin_id,
                'created_by'            => $request->user()->id,
                'adjusted_by'           => $request->user()->id,
                'transdate'             => now()->format('Y-m-d'),
                'transdesc'             => 'ADJ VOID USAGE',
                'stock_before'          => $stockBefore,
                'stock_after'           => $currentStock,
                'stock_adjustment'      => $log->qty * -1,
                'stock_adjustment_type' => 'in',
                'ref_id'                => $log->id,
                'reason'                => $usage->remarks
            ];

            $adjustment = new InventoryAdjustment($adjustmentData);
            $adjustment->code = InventoryAdjustment::generateCode();
            $adjustment->save();

            $mutationData = [
                'warehouse_destination_id'  => $log->warehouse_origin_id,
                'rack_destination_id'       => $log->rack_origin_id,
                'item_id'                   => $log->item_id,
                'transdate'                 => now()->format('Y-m-d'),
                'transtype'                 => 'in',
                'cogs'                      => $log->cogs,
                'price'                     => str_replace(',', '', $log->price),
                'adjustment_id'             => $adjustment->id,
                'ref_id'                    => $log->id,
                'balance'                   => 0,
                'transdesc'                 => 'ADJ VOID USAGE',
                'qty'                       => $log->qty * -1
            ];

            $mutation = Mutation::create($mutationData);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usage = Usage::find($id);

        $usage_last = '';
        foreach ($usage->usage_detail as $detail) {
            $usage_last = UsageDetail::where('inv_code', $detail->inv_code)
            ->latest('id_usage_detail')
            ->first();
        }
        // dd($usage_last);

        // $delete = Usage::leftJoin('usage_details', 'usage_details.usage_id', 'usages.id_usage')
        // ->where('customer_id', $usage->customer_id)
        // ->where('status_usage', 0)
        // ->where('usage_details.inv_code', $usage->usage_detail->inv_code)
        // ->get();

        // dd($delete);
        // ->latest('id_usage')
        // ->first();
        $log_usage = LogUsage::where('usage_id', $id)->whereNull('remarks')->get();

        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $therapist_all = Therapist::whereHas('therapist_outlets', function ($q) use ($outlet_id) {
            return $q->whereIn('outlet_id', $outlet_id);
        })->get();
        return view('customers.past-usage.detail', compact('usage', 'therapist_all', 'usage_last', 'log_usage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Usage::find($id);
        $update->usage_date = Carbon::createFromFormat('d/m/Y', $request->date_usage)->format('Y-m-d');
        $update->remarks = $request->remarks;
        $update->update();

        $saveLog = new LogUsage;
        $saveLog->usage_id = $id;
        $saveLog->staff_id = Auth::user()->id;
        $saveLog->save();

        for ($i = 0; $i < count($request->product_id); $i++) {
            $usage_detail = UsageDetail::find($request->id_usage[$i]);
            $usage_detail->point = $request->point[$i];
            $usage_detail->update();

            $therapist = "therapist_" . $request->inv_package_id[$i];

            $before = UsageTherapist::where('usage_detail_id', $request->id_usage)->pluck('therapist_id')->toArray();
            $shouldDelete = array_diff($before, $request->$therapist);

            if (count($shouldDelete)) {
                UsageTherapist::where('usage_detail_id', $request->id_usage)
                    ->whereIn('therapist_id', $shouldDelete)
                    ->delete();
            }

            for ($z = 0; $z < count($request->$therapist); $z++) {
                UsageTherapist::updateOrCreate([
                    'usage_detail_id' => $request->id_usage[$i],
                    'therapist_id' => $request->$therapist[$z]
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}

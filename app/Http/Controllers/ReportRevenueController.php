<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;

use Carbon\carbon;
use DataTables;
use App\Models\Outlet;
use App\Models\Invoice;
use App\Models\UserOutlet;

use Auth;

class ReportRevenueController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('sales')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }
    public function index_revenue()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();

        $from = Carbon::parse(now())->format('d/m/Y');
        $to = Carbon::parse(now())->format('d/m/Y');
        $detail = [];
        return view('report.revenue.index')
            ->with('from', $from)
            ->with('to', $to)
            ->with('outlet', $outlet)
            ->with('detail', $detail);
    }

    // public function revenue(Request $request)
    // {
    //     $from = Carbon::parse(now())->format('Y-m-d');
    //     $to = Carbon::parse(now())->format('Y-m-d');
    //     if ($request->from && $request->to) {
    //         $from = Carbon::parse($request->from)->format('Y-m-d');
    //         $to = Carbon::parse($request->to)->format('Y-m-d');
    //     }
    //     $details = Invoice::whereBetween('inv_date', [$from, $to])
    //         ->where('void_invoice', 0);
    //     if ($request->outlet) {
    //         $details->whereIn('outlet_id', $request->outlet);
    //     }
    //     $details = $details->get();
    //     $data = array();
    //     foreach ($details as $list) {
    //         foreach ($list->invoice_detail as $detail) {
    //             foreach ($detail->invoice_package as $package) {
    //                 $row = array();
    //                 if (count($list->log_convert) > 0) {
    //                     $row[] = "<a href=\"".route('invoice-convert.print', $list->id_invoice)."\" target=\"_blank\">".$list->inv_code."</a>";
    //                 } else {
    //                     $row[] = "<a href=\"".route('invoice.print', $list->id_invoice)."\" target=\"_blank\">".$list->inv_code."</a>";
    //                 }
    //                 $row[] = $list->inv_date;
    //                 $row[] = $list->expired_date;
    //                 $row[] = $list->outlet->outlet_name;
    //                 $row[] = "<a href=\"".route('customers.show', $list->customer_id)."\" target=\"_blank\">".$list->customer->induk_customer."</a>";
    //                 $row[] = $list->customer->full_name;
    //                 $row[] = $package->product_treatment->product_treatment_code;
    //                 $row[] = $package->product_treatment->product_treatment_name;
    //                 $row[] = $package->qty + $package->used;
    //                 $row[] = format_money($package->nominal * ($package->qty + $package->used));
    //                 $row[] = $package->used;
    //                 $row[] = format_money($package->nominal * $package->used);
    //                 $row[] = $package->qty;
    //                 $row[] = format_money($package->nominal * $package->qty);
    //                 $row[] = "<a href=\"".route('customer.active', $list->id_invoice)."\" class='btn btn-info btn-xs' target=\"_blank\"><i class=\"fa fa-eye\"></i></a>";
    //                 $data[] = $row;
    //             }
    //         }
    //     }

    //     return DataTables::of($data)->escapeColumns([])->make(true);
    // }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function revenue(Request $request)
    {
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        $outlets = [0];
        if (isset($request->outlet)) {
            $outlets = $request->outlet;
        }
    
        $sql = file_get_contents(__DIR__ . '/Queries/UERevenue.sql.tpl');
        $result = DB::select(sprintf($sql, implode(',', $outlets)), [$from, $to]);

        $data = [];
        foreach ($result as $res) {
            $row = [];
            $row[] = "<a href=\"".route('invoice.print', $res->id_invoice)."\" target=\"_blank\">".$res->inv_code."</a>";
            $row[] = $res->inv_date;
            $row[] = $res->expired_date;
            $row[] = $res->outlet_name;
            $row[] = "<a href=\"".route('customers.show', $res->id_customer)."\" target=\"_blank\">".$res->induk_customer."</a>";
            $row[] = $res->full_name;
            $row[] = $res->product_treatment_code;
            $row[] = $res->product_treatment_name;
            $row[] = $res->paid_qty;
            $row[] = $res->paid_amount;
            $row[] = $res->used;
            $row[] = $res->earned;
            $row[] = $res->qty;
            $row[] = $res->unearned;
            $row[] = "<a href=\"".route('customer.active', $res->id_invoice)."\" class='btn btn-info btn-xs' target=\"_blank\"><i class=\"fa fa-eye\"></i></a>";

            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
        
    }
}

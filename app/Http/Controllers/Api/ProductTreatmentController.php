<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\InvoicePoint;
use App\Models\InvoicePointDetail;
use App\Models\Outlet;
use App\Models\ProductPoint;
use App\Models\Customer;

class ProductTreatmentController extends Controller
{
    public function allProduct()
    {
        $status = true;
        $message = "get data success";
        $product = ProductPoint::all();
        $code = 200;

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $product,
        ], $code);
    }

    public function redeemProduct(Request $request, $id)
    {
        $status = true;
        $message = "redeem success";

        $outlet = Outlet::first();
        $store_invoice = new InvoicePoint();
        $store_invoice->inv_point_code = InvoicePoint::generateInvoiceCode($outlet->id_outlet);
        $store_invoice->customer_id = $id;
        $store_invoice->outlet_id = $outlet->id_outlet;
        $store_invoice->total = $request->point;
        $store_invoice->status_inv_point = 1;
        $store_invoice->save();

        $save = new InvoicePointDetail();
        $save->invoice_point_id = $store_invoice->id_invoice_point;
        $save->product_point_id = $request->product_id;
        $save->current_point = $request->point;
        $save->qty = 1;
        $save->subtotal = $request->point;
        $save->save();

        $update_point = Customer::find($id);
        if ($update_point->point >= $request->point) {
            $update_point->point = round($update_point->point - $store_invoice->total);
            $update_point->update();
        } else {
            $message = "not enough point";
        }

        $data = InvoicePoint::find($store_invoice->id_invoice_point);
        $code = 200;

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ], $code);
    }

    public function myRedeem(Request $request)
    {
        $status = false;
        $message = "param required";
        $code = 401;

        $id = $request->get('id_customer');
        if ($id) {
            $status = true;
            $message = "get data success";
            $code = 200;
            $data = InvoicePoint::with([
                'invoice_details.product_point',
            ])
                ->where('customer_id', $id)
                ->get();
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ], $code);
    }
}

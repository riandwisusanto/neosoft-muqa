<?php

namespace App\Http\Controllers\Api\v1\Whatsapp;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class CollectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCollections($month, $year)
    {
        $sql = "SELECT
                    outlet_name,
                    (
                    SELECT
                        IFNULL(SUM(pa.amount), 0) as amount
                    FROM
                        invoices inv
                        JOIN payments pa ON pa.inv_id = inv.id_invoice 
                    WHERE
                        outlet_id = o.id_outlet 
                        AND MONTH ( inv_date ) = %s
                        AND YEAR ( inv_date ) = %s
                        AND void_invoice = 0 
                    )  AS collection 
                FROM
                    outlets o";
        $query = sprintf($sql, $month, $year);
        $result = DB::select($query);
        
        return response()->json($result);
    }
}

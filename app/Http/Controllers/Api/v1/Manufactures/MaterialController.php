<?php

namespace App\Http\Controllers\Api\v1\Manufactures;

use App\Models\Manufactures\Material;
use App\Models\Manufactures\MaterialRelease;
use App\Models\Logistics\Item;
use App\Models\WarehouseOutlet;
use App\Models\Logistics\Mutation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function releases()
    {
        $relations = [
            'releaser' => function ($query) {
                $query->select('id', 'name');
            },
            'rawItem'   => function ($query) {
                $query->select('id', 'name');
            },
            'warehouseOrigin' => function ($query) {
                $query->select('id_warehouse', 'name');
            },
            'material'  => function ($query) {
                $relations = [
                    'workOrderDetail'   => function ($query) {
                        $relations = [
                            'workOrder' => function ($query) {
                                $query->select('id', 'code');
                            }    
                        ];
                        
                        $query->with($relations)
                                ->select('id', 'work_order_id');
                    }
                ];

                $query->with($relations)
                        ->select('id', 'work_order_detail_id');
            }
        ];

        $collection = MaterialRelease::with($relations)
                                        ->get();

        return response()->json(['collection' => $collection]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeReleases(Request $request)
    {
        $creator = $request->user()->id;
        
        DB::beginTransaction();

        try {
            foreach ($request->releases as $row) {

                if ($row['released_qty'] > 0 && $row['warehouse_origin_id']) {
                    $material = Material::find($row['material_id']);
                    $material->released_qty += $row['released_qty'];
        
                    $release = new MaterialRelease($row);
                    $release->code = MaterialRelease::generateCode();
                    $release->created_by = $creator;
                    $release->raw_item_id = $material->raw_item_id;
                    $release->save();
                
                    $material->save();
        
                    $this->createReleaseMutation($request, $release);
                }
            } 

            DB::commit();

            return response()->json(['message' => 'all material releases have been stored']);
        } catch (Exception $e) {
            DB::rollback();

            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function createReleaseMutation(Request $request, $release)
    {
        $item = Item::find($release->raw_item_id);
        $sort = $item->dispense_method == 'lifo' ? 'asc' : 'desc';

        $currentStock = $item->stock - $release->released_qty;
        $item->stock = $currentStock;

        if ($item->sellable_stock > 0) {
            if ($item->sellable_stock >= $release->released_qty) {
                $item->sellable_stock -= $release->released_qty;
            } else {
                $item->sellable_stock = 0;
            }
        } 

        $item->save();

        $releasedQty = $release->released_qty;


        $userOutlets = $request->user()->userOutlets()->pluck('outlet_id');
        $grantedWarehouses = WarehouseOutlet::whereIn('outlet_id', $userOutlets)->pluck('warehouse_id');

        while ($releasedQty > 0) {
            $mutationLog = Mutation::where('transtype', 'in')
                                    ->where('transdesc', '<>', 'BEGIN EMPTY')
                                    ->where('item_id', $item->id)
                                    ->whereIn('warehouse_destination_id', $grantedWarehouses)
                                    ->orderBy('transdate', $sort)
                                    ->where('balance', '>', 0)
                                    ->first();

            if ( ! $mutationLog) break;

            $outQty = 0;
            
            // current item at this warehouse has not enough stock.
            if ($mutationLog->balance < $releasedQty) {
                $releasedQty -= $mutationLog->balance;
                $outQty = $mutationLog->balance;

                $mutationLog->balance = 0;
            } else {
                $mutationLog->balance -= $releasedQty;
                $outQty = $releasedQty;

                $releasedQty = 0;
            }

            $mutationLog->save();

            $mutation = [
                'warehouse_origin_id' => $mutationLog->warehouse_destination_id,
                'rack_origin_id' => $mutationLog->rack_destination_id,
                'material_release_id' => $release->id,
                'item_id' => $item->id,
                'transdate' => $release->released_date,
                'transtype' => 'out',
                'cogs'     => $mutationLog->cogs,
                'price'     => $item->cogs,
                'ref_id'    => $mutationLog->id,
                'balance'   => 0,
                'transdesc' => 'MATERIAL RELEASE',
                'qty' => $outQty * -1
            ];
            
            Mutation::create($mutation);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function showByWorkOrderDetail($work_order_detail_id)
    {
        $relations = [
            'rawItem',
            'uom'
        ];

        $collection = Material::where('work_order_detail_id', $work_order_detail_id)
                            ->with($relations)
                            ->get();

        return response()->json(['collection' => $collection]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Api\v1\Manufactures;

use App\Models\Manufactures\FinishedProduct;
use App\Models\Logistics\Item;
use App\Models\Logistics\Mutation;
use App\Models\Logistics\InventoryAdjustment;
use App\Models\Manufactures\WorkOrder;
use App\Models\Manufactures\WorkOrderDetail;
use App\Services\FinishedProductService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $relations = [
            'warehouseDestination'  => function ($query) {
                $query->select('id_warehouse', 'name');
            },

            'workOrderDetail'   => function ($query) {
                $relations = [
                    'workOrder' => function ($query) {
                        $query->select('id', 'code');
                    }
                ];

                $query->select('id', 'work_order_id')
                        ->with($relations);
            },

            'rackDestination'   => function ($query) {
                $query->select('id_rack', 'main_rack_code', 'sub_rack_code');
            },

            'checker' => function ($query) {
                $query->select('id', 'name');
            },

            'item'  => function ($query) {
                $query->select('id', 'name');
            }
        ];

        $collection = FinishedProduct::with($relations)
                                        ->get();

        return response()->json(['collection' => $collection]);
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function showByLine($id)
    {
        $relations = [
            'warehouseDestination',
            'checker'
        ];

        $collection = FinishedProduct::where('work_order_detail_id', $id)
                                        ->with($relations)
                                        ->get();

        return response()->json(['collection' => $collection]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $productData = $request->only([
            'work_order_detail_id',
            'checked_by',
            'checked_at',
            'qty',
            'marked_as',
            'warehouse_destination_id',
            'rack_destination_id'
        ]);

        $workOrderDetail = WorkOrderDetail::find($request->work_order_detail_id);

        $product = new FinishedProduct($productData);
        $product->code = FinishedProduct::generateCode();
        $product->item_id = $workOrderDetail->item_id;
        $product->created_by = $request->user()->id;

        $izzibookService = new FinishedProductService();
        
        DB::beginTransaction();
        try {
            $product->save();

            $this->updateWorkOrder($request, $product);

            $izzibookService->store($product);
            DB::commit();

            return response()->json(['product' => $product]);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function updateWorkOrder(Request $request, $product)
    {
        $line = WorkOrderDetail::find($product->work_order_detail_id);

        if ($product->marked_as == 'CANCELED') {
            $this->createCancelMutation($request, $product);
            $line->canceled_qty += $product->qty;
        }

        if ($product->marked_as == 'REJECTED') {
            $line->rejected_qty += $product->qty;
        }

        if ($product->marked_as == 'ACCEPTED') {
            $line->finished_qty += $product->qty;

            if ($line->finished_qty >= $line->qty) {
                $line->mark_as_close = 1;
            }

            $this->createFinishedMutation($request, $product);
        }

        $line->save();
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function createFinishedMutation(Request $request, $product)
    {
        $mutationData = [
            'finished_product_id'       => $product->id,
            'item_id'                   => $product->item_id,
            'transdate'                 => $product->checked_at,
            'transtype'                 => 'in',
            'transdesc'                 => 'FINISHED PRODUCT',
            'balance'                   => $product->qty,
            'qty'                       => $product->qty
        ];

        $mutationData['warehouse_destination_id'] = $product->warehouse_destination_id;
        $mutationData['rack_destination_id'] = $product->rack_destination_id;
        
        $item = Item::find($product->item_id);
        $item->sellable_stock += $product->qty;
        $item->stock += $product->qty;
        $item->save();

        Mutation::create($mutationData);
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function createCancelMutation(Request $request, $product)
    {
        // get product materials.
        foreach ($product->workOrderDetail->materials as $material) {
            
            // check if product has been released some materials.
            if ($material->released_qty > 0) {
                // create opposite mutations, get released material back to warehouse.
                foreach ($material->releases as $release) {
                    Log::info('release '. json_encode($release));
                    $refs = Mutation::where('material_release_id', $release->id)->get();


                    foreach ($refs as $ref) {
                        $adjustmentData = [
                            'item_id'                   => $ref->item_id,
                            'warehouse_id'              => $ref->warehouse_origin_id,
                            'adjusted_by'               => $product->checked_by,
                            'transdate'                 => $product->checked_at,
                            'transdesc'                 => 'MATERIAL RELEASE RETURN',
                            'stock_before'              => $ref->item->stock,
                            'stock_after'               => $ref->item->stock + ($ref->qty * -1),
                            'stock_adjustment'          => $ref->qty * -1,
                            'stock_adjustment_type'     => 'in',
                            'ref_id'                    => $ref->ref_id,
                            'reason'                    => '[system] material release return for work order '.$product->workOrderDetail->workOrder->code
                        ];

                        $adjustment = new InventoryAdjustment($adjustmentData);
                        $adjustment->code = InventoryAdjustment::generateCode();
                        $adjustment->created_by = $request->user()->id;
                        $adjustment->save();

                        $this->createCancelMutationAdjustment($adjustment, $ref);
                    }
                }
            }
        }
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function createCancelMutationAdjustment($adjustment, $oldRef)
    {
        $qty = $adjustment->stock_adjustment;

        $ref = Mutation::find($oldRef->ref_id);

        $mutationData = [
            'warehouse_destination_id'  => null,
            'warehouse_destination_id'  => null,
            'rack_origin_id'            => null,
            'rack_destination_id'       => null,
            'sales_invoice_id'          => null,
            'purchase_invoice_id'       => null,
            'item_id'                   => $adjustment->item_id,
            'transdate'                 => $adjustment->transdate,
            'transtype'                 => $adjustment->stock_adjustment_type,
            'transdesc'                 => $adjustment->transdesc,
            'adjustment_id'             => $adjustment->id,
            'balance'                   => 0,
            'qty'                       => $qty
        ];

        $mutationData['warehouse_destination_id'] = $ref->warehouse_destination_id;
        $mutationData['rack_destination_id'] = $ref->rack_destination_id;
        
        $item = Item::find($adjustment->item_id);
        $item->sellable_stock += $qty;
        $item->stock += $qty;
        $item->save();

        Mutation::create($mutationData);

        $ref->balance += $qty;
        $ref->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

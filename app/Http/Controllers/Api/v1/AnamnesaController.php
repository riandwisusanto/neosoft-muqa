<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Anamnesa;
use App\Models\AnamnesaDetail;
use App\Models\AnamnesaBom;
use App\Models\AnamnesaDiagnosa;
use App\Models\Queue;
use App\Models\InvoicePackage;
use Illuminate\Support\Facades\DB;
use Auth;

class AnamnesaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $anamnesaData = $request->only(['customer_id', 'suggestion', 'complain', 'anamnesa', 'date_anamnesa']);
        $anamnesa = new Anamnesa($anamnesaData);
        $anamnesa->anamnesa_code = Anamnesa::generateCode();
        $anamnesa->created = Auth::user()->id;

        try {
            DB::beginTransaction();
            $anamnesa->save();
            $this->storeDetail($request, $anamnesa);
            $this->storeDiagnosas($request, $anamnesa);

            $queue = Queue::where('customer_id',$request->customer_id)->count();
            if($queue)
            {
                $updateQueue = Queue::where('customer_id',$request->customer_id)->latest('created_at')->first();
                $updateQueue->status_queue = 1;
                $updateQueue->status_payment = 1;
                $updateQueue->update();
            }

            DB::commit();
            return response()->json(['model' => $anamnesa], 200);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function storeDiagnosas(Request $request, $anamnesa)
    {
        if ( ! isset($request->diagnosas) || ! is_array($request->diagnosas)) {
            return false;
        }

        foreach ($request->diagnosas as $item) {
            $diagnosa = new AnamnesaDiagnosa($item);
            $diagnosa->anamnesa_id = $anamnesa->id_anamnesa;
            $diagnosa->save();
        }
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function storeDetail(Request $request, $anamnesa)
    {
        if ( ! isset($request->details) || ! is_array($request->details)) {
            throw new Exception('expected details length to be > 0, got empty');
        }

        try {
            foreach ($request->details as $detailData) {
                $detail = new AnamnesaDetail($detailData);
                $detail->anamnesa_id = $anamnesa->id_anamnesa;
                $detail->save();

                if (count($detailData['boms']) > 0) {
                    $this->storeBoms($detail, $detailData['boms']);
                }
            }
        } catch (Exception $e) {
            throw new Exception('error while store anamnesa detail: '. $e->getMessage());
        }
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function storeBoms($detail, $boms)
    {
        foreach ($boms as $bomData) {
            $bom = new AnamnesaBom($bomData);
            $bom->anamnesa_detail_id = $detail->id_anamnesa_detail;
            $bom->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $relations = [
            'details.product_treatment' => function ($query) {
                $query->select('id_product_treatment', 'product_treatment_name', 'product_treatment_code');
            },

            'details.boms.rawItem' => function ($query) {
                $query->select('id', 'name', 'uom_id');
            },

            'details.boms.rawItem.uom' => function ($query) {
                $query->select('id', 'alias');
            },
            'details.invoice_package',
            'diagnosas',
            'customer.city'
        ];

        $model = Anamnesa::with($relations)->find($id);


        if ( ! $model) {
            return response()->json(['message' => 'anamnesa with id '. $id. ' does not exists.'], 404);
        }

        return response()->json(['model' => $model]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Anamnesa::findOrFail($id);

        $model->fill($request->except('date_anamnesa'));

        DB::beginTransaction();

        try {
            $model->save();
            $this->updateDetails($model, $request->details);
            $this->updateDiagnosas($model, $request->diagnosas);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage()
            ], 500);
        }


        DB::commit();
        return response()->json(['status' => 'success']);
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function updateDetails($model, $detailReq)
    {
       $currentIDs = array_map(function ($r) {
           return $r['id_anamnesa_detail'];
       }, $model->details()->get()->toArray());

       $olds = array_filter($detailReq, function ($row) {
            return isset($row['id_anamnesa_detail']);
       });

       $existIDs = array_map(function ($r) {
            return $r['id_anamnesa_detail'];
       }, $olds);

       // destroy deleted record.
       $diff = array_diff($currentIDs, $existIDs);
       if (count($diff) > 0) {
           AnamnesaDetail::whereIn('id_anamnesa_detail', $diff)->delete();
           AnamnesaBom::whereIn('anamnesa_detail_id', $diff)->delete();
       }

        foreach ($detailReq as $req) {
            $detail = null;
            $newRecord = false;
            // if id_anamnesa_detail was setted, so it's modify request. otherwise, it's new record.
            if (isset($req['id_anamnesa_detail'])) {
                $detail = AnamnesaDetail::find($req['id_anamnesa_detail']);
                $detail->fill($req);
            } else {
                $newRecord = true;
                $detail = new AnamnesaDetail($req);
                $detail->anamnesa_id = $model->id_anamnesa;
            }

            $detail->save();
            if ($newRecord) {
                $this->storeBoms($detail, $req['boms']);
            } else {
                $this->updateBoms($detail, $req['boms']);
            }
        }
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function updateDiagnosas($model, $diagnosas)
    {
        $olds = AnamnesaDiagnosa::where('anamnesa_id', $model->id_anamnesa)->pluck('diagnosa_id')->toArray();

        $requested = array_map(function ($row) {
            return $row['diagnosa_id'];
        }, $diagnosas);

        $diff = array_diff($olds, $requested);
        if (count($diff) > 0) {
            AnamnesaDiagnosa::where('anamnesa_id', $model->id_anamnesa)->whereIn('diagnosa_id', $diff)->delete();
        }

        foreach ($diagnosas as $item) {
            $exists = AnamnesaDiagnosa::firstOrNew(['anamnesa_id' => $model->id_anamnesa, 'diagnosa_id' => $item['diagnosa_id']]);
            $exists->save();
        }
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function updateBoms($detail, $boms)
    {
       $currentIDs = array_map(function ($r) {
           return $r['id'];
       }, $detail->boms()->get()->toArray());

       $olds = array_filter($boms, function ($row) {
            return isset($row['id']);
       });

       $existIDs = array_map(function ($r) {
            return $r['id'];
       }, $olds);

       // destroy deleted record.
       $diff = array_diff($currentIDs, $existIDs);
       if (count($diff) > 0) {
           AnamnesaBom::whereIn('id', $diff)->delete();
       }

        foreach ($boms as $req) {
            $bom = null;
            // if id was setted, so it's modify request. otherwise, it's new record.
            if (isset($req['id'])) {
                $bom = AnamnesaBom::find($req['id']);
                $bom->fill($req);
            } else {
                $bom = new AnamnesaBom($req);
                $bom->anamnesa_detail_id = $detail->id_anamnesa_detail;
            }

            $bom->save();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

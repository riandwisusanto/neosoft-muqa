<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use App\Models\Country;
use App\Models\Province;
use App\Models\Outlet;
use App\Models\Consultant;
use App\Models\MarketingSource;
use App\Models\Customer;
use Auth;
use Hash;
use App\Models\CustomerOutlet;
use App\Models\CustomerConsultant;
use App\Services\CustomerService;
use Carbon\carbon;
use DataTables;
use App\Models\UserOutlet;
use App\Models\Queue;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CustomerController extends Controller
{
    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function index(Request $request)
    {
        $userOutlets = $request->user()->userOutlets()->pluck('outlet_id')->toArray();
        $perPage = 20;
        $page = 1;

        $search = '%'. $request->search . '%';

        if (isset($request->perPage)) {
            $perPage = $request->perPage;
        }

        if (isset($request->page)) {
            $page = $request->page;
        }

        $offset = $perPage * $page - $perPage;

        $sql = '
        SELECT
            c.id_customer,
            c.induk_customer_old,
            c.induk_customer,
            c.full_name,
            c.nick_name,
            c.gender,
            c.phone,
            c.address,
            c.izzibook_sync_status,
            DATE_FORMAT(c.birth_date, "%d %M %Y") AS birth_date
        FROM
            customers c
        WHERE c.full_name LIKE ? OR c.phone LIKE ? OR c.induk_customer_old LIKE ? OR c.induk_customer LIKE ? OR c.address LIKE ? OR DATE_FORMAT(c.birth_date, "%d %M %Y") LIKE ? OR c.nick_name LIKE ?
        ORDER BY c.join_date DESC LIMIT ? OFFSET ?';

        $sqlTotal = '
        SELECT
            COUNT(raw.id_customer) AS total
        FROM
        (SELECT
            c.id_customer
        FROM
            customers c
        WHERE c.full_name LIKE ? OR c.phone LIKE ? OR c.induk_customer_old LIKE ? OR c.induk_customer LIKE ? OR c.address LIKE ? OR DATE_FORMAT(c.birth_date, "%d %M %Y") LIKE ? OR c.nick_name LIKE ?
        ORDER BY c.join_date DESC) raw
        ';

        // $total = sprintf($sqlTotal, implode(',', $userOutlets));
        $customerTotal = DB::select($sqlTotal, [$search, $search, $search, $search, $search, $search, $search]);

        // $sql = sprintf($sql, implode(',', $userOutlets));
        $collections =  DB::select($sql, [$search, $search, $search, $search, $search, $search, $search, $perPage, $offset]);


        return response()->json([
            'collections'   => $collections,
            'perPage'       => $offset,
            'limit'         => $perPage,
            'count'         => $customerTotal[0] // customerTotal returns array of single object. just return the object.
        ]);
    }

    public function listQueueToday(Request $request)
    {
        $date = Carbon::now()->format('Y-m-d');
        // $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        // $customer_id = CustomerOutlet::whereIn('outlet_id', $outlet_id)->pluck('customer_id')->toArray();
        // $customer_id = Queue::where('come_date',$date)->whereIn('outlet_id',$request->outlet_id)->pluck('customer_id')->toArray();
        // $outlet_id = $requet->outlet_id;
        // $customer = Customer::with([
        //     'outlet:id_outlet,outlet_name',
        //     'consultant:id_consultant,consultant_name',
        //     'queue' => function($q) use ($date){
        //         $q->where('come_date',$date);
        //     }
        //     ])
        //     ->whereIn('id_customer', $customer_id)
        //     ->get();
        $outlet_id = $request->outlet_id;

        $customer = Customer::with([
            'outlet:id_outlet,outlet_name',
            'consultant:id_consultant,consultant_name',
        ])
        ->join('queues','queues.customer_id','customers.id_customer')
        ->where('queues.come_date',$date)
        ->where('queues.outlet_id',$outlet_id)
        ->get();

        return response()->json(['collection' => $customer]);
    }

    public function goQueue(Request $request)
    {
        $date = Carbon::now()->format('Y-m-d');
        $queue = Queue::where('queue_number',$request->queue_number)
        ->where('outlet_id',$request->outlet_id)
        ->where('come_date',$date)
        ->pluck('customer_id')
        ->first();

        return response()->json(['id_customer' => $queue]);

    }

    public function destroy($id)
    {
        $user = Customer::find($id);
        if (!count($user->invoice) || count($user->appointment))
        {
            $user->delete();
            CustomerConsultant::where('customer_id', $id)->delete();
            CustomerOutlet::where('customer_id', $id)->delete();
        }

        return response()->json(['message' => 'Patient Deleted'], 200);

    }


    public function update(Request $request, $id)
    {
        $update = Customer::findOrFail($id);
        try {
            $update->full_name = $request->full_name;
            $update->nick_name = $request->nick_name;
            $update->phone = $request->phone;
            $update->email = $request->email;
            $update->birth_place = $request->birth_place;
            $update->birth_date =  $request->birth_date;
            $update->join_date = $request->join_date;
            $update->gender = $request->gender;
            $update->religion = $request->religion;
            $update->ktp = $request->ktp;
            $update->address = $request->address;
            $update->occupation = $request->occupation;
            $update->country_id = $request->country_id;
            $update->city_id = $request->city_id;
            $update->zip = $request->zip;
            $update->nextofikin = $request->nextofikin;
            $update->nextofikin_number = $request->nextofikin_number;
            $update->source_id = $request->source_id;
            $update->source_remarks = $request->source_remarks;
            $update->purpose_treatment = $request->purpose_treatment;
            $update->staf_id = Auth::user()->id; //auth user login
            if (!$update->password) {
                $update->password = Hash::make('123456789');
            }

            $update->update();

            $outlet_update=array('outlet_id'=>$request->outlet_id);
            CustomerOutlet::where('customer_id',$id)->update($outlet_update);

            $consultant_update =array('consultant_id'=>$request->consultant_id);
            CustomerConsultant::where('customer_id',$id)->update($consultant_update);

            // send to izzibook
            $izzibookCustomer = new CustomerService();
            $izzibookCustomer->update($update);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }

        return response()->json(['model' => $update], 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'full_name' => 'required',
            'nick_name' => 'required',
            'country_id' => 'required|numeric|exists:countries,id_country',
            'city_id' => 'required|numeric|exists:provinces,id_province',
            'outlet_id' => 'required|numeric|exists:outlets,id_outlet',
            'address' => 'required',
            // 'nick_name' => 'required',
        ]);

        if($validator->fails()){
            $errors = $validator->errors()->first();
            $message = $errors;
            $status = false;

            return response()->json([
                'message' => $message,
                'status' => $status,
            ], 422);
        }

        try {
            $save = new Customer;
            $save->induk_customer = Customer::generateIndukCustomer($request->outlet_id);
            $save->full_name = $request->full_name;
            $save->nick_name = $request->nick_name;
            $save->phone = $request->phone;
            $save->email = $request->email;
            $save->birth_place = $request->birth_place;
            $save->birth_date =  $request->birth_date;
            $save->join_date = $request->join_date;
            $save->gender = $request->gender;
            $save->religion = $request->religion;
            $save->ktp = $request->ktp;
            $save->address = $request->address;
            $save->occupation = $request->occupation;
            $save->country_id = $request->country_id;
            $save->city_id = $request->city_id;
            $save->zip = $request->zip;
            $save->nextofikin = $request->nextofikin;
            $save->nextofikin_number = $request->nextofikin_number;
            $save->source_id = $request->source_id;
            $save->source_remarks = $request->source_remarks;
            $save->purpose_treatment = $request->purpose_treatment;
            $save->staf_id = Auth::user()->id; //auth user login
            $save->password = Hash::make('123456789');
            // $save->sendEmailVerificationNotification();
            $save->save();

			$outlet_save = new CustomerOutlet;
            $outlet_save->customer_id = $save->id_customer;
            $outlet_save->outlet_id = $request->outlet_id;
            $outlet_save->save();

            if (isset($request->consultant_id)) {
                $consultant_save = new CustomerConsultant;
                $consultant_save->customer_id = $save->id_customer;
                $consultant_save->consultant_id = $request->consultant_id;
                $consultant_save->save();
            }

            // send to izzibook
            // $izzibookCustomer = new CustomerService();
            // $izzibookCustomer->store($save);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }

        return response()->json(['model' => $save], 200);
    }

    public function optioncountry(Request $request)
    {

		$options = [];
		$options = Country::select('id_country', 'country')->get();

		return response()->json($options);

    }

    public function optionprovince(Request $request, $country_id)
    {

		$options = [];
		$options = Province::select('id_province', 'province')->where('country_id', $country_id)->get();

		return response()->json($options);

    }

    public function optionoutlet(Request $request)
    {

		$options = [];
		$options = Outlet::select('id_outlet', 'outlet_name')->get();

		return response()->json($options);

    }

    public function optionconsultant(Request $request)
    {

		$options = [];
		$options = Consultant::select('id_consultant', 'consultant_name')->get();

		return response()->json($options);

    }

    public function optionsource(Request $request)
    {

		$options = [];
		$options = MarketingSource::select('id_marketing_source', 'marketing_source_name')->get();

		return response()->json($options);

    }

    public function editform($id)
    {
        $options = [];
       	$options = Customer:: select('customers.*', 'customer_outlets.outlet_id','customer_consultants.consultant_id')
                   ->leftjoin('customer_outlets', 'customer_outlets.customer_id','customers.id_customer')
                   ->leftjoin('customer_consultants', 'customer_consultants.customer_id','customers.id_customer')
                   ->where('customers.id_customer',$id)
                   ->get();

		return response()->json($options);

    }


    /**
     * Patient overview
     *
     * returns patient visit data and it's all activities
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function activities(Request $request, $id)
    {
        $yearlySQL = file_get_contents(__DIR__ .'/../../Queries/patientVisitYearly.sql.tpl');
        $rawYearly = DB::select($yearlySQL, [$id, $id]);

        $yearly = [];
        foreach ($rawYearly as $row) {
            $yearly[$row->visit_month] = $row->total_visit;
        }

        $startMonth = now()->firstOfMonth();
        $endOfMonth = now()->endOfMonth();

        $startWeek = now();
        $endOfWeek = now()->sub(1, 'week');

        $dailySQL = file_get_contents(__DIR__. '/../../Queries/patientVisitDaily.sql.tpl');
        $rawMonthly = DB::select($dailySQL, [
            $id, $startMonth, $endOfMonth,
            $id, $startMonth, $endOfMonth
        ]);

        $monthly = array_map(function($row) {
            return $row->visit_day;
        }, $rawMonthly);

        $rawWeekly = DB::select($dailySQL, [
            $id, $endOfWeek, $startWeek,
            $id, $endOfWeek, $startWeek
        ]);

        $weekly = array_map(function($row) {
            return $row->visit_day;
        }, $rawWeekly);

        $monthData = [];
        for ($i = (int) $startMonth->format('d'); $i <= (int) $endOfMonth->format('d'); $i++) {
            if (in_array($i, $monthly)) {
                $monthData[$i] = 1;
            } else {
                $monthData[$i] = 0;
            }
        }

        $weekData = [];
        for ($i = (int) $endOfWeek->format('d'); $i <= (int) $startWeek->format('d'); $i++) {
            Log::info('loop: ' .$i. ', DB: '. json_encode($weekly));
            if (in_array($i, $weekly)) {
                $weekData[$i] = 1;
            } else {
                $weekData[$i] = 0;
            }
        }

        $monthLabel = ['January', 'Febuary', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        $yearData = [];
        foreach ($monthLabel as $month) {
            if (isset($yearly[$month])) {
                $yearData[$month] = $yearly[$month];
            } else {
                $yearData[$month] = 0;
            }
        }

        $visit = [
            'yearly'    => $yearData,
            'monthly'   => $monthData,
            'weekly'    => $weekData
        ];

        return response()->json($visit);
    }

    /**
     * Treatment history.
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function histories($invoicePackageId)
    {

        $sql = file_get_contents(__DIR__. '/../../Queries/treatmentHistories.sql.tpl');
        $result = DB::select($sql, [$invoicePackageId]);

        $res = array_map(function ($row) {
            $therapists = json_decode($row->therapists);
            $row->therapists = implode(', ', $therapists);
            return $row;
        }, $result);

        return response()->json($result);
    }

    public function show($id)
    {
        $customer = Customer::find($id);

        return response()->json(['model' => $customer]);
    }
}


<?php

namespace App\Http\Controllers\Api\v1\Logistics;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductTreatment;
use App\Models\ProductTreatmentOutlet;
use App\Models\ProductTreatmentPoint;
use App\Models\Package;
use App\Models\LogProductTreatment;
use App\Models\Logistics\Item;
use App\Models\Logistics\Bom;
use App\Models\Logistics\Mutation;
use App\Models\Logistics\InventoryTaking;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

use App\Models\Warehouse;
use App\Models\UserOutlet;
use App\Models\WarehouseOutlet;
use Auth;
class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();

        $warehouse =  Warehouse::whereHas('warehouseOutlets', function ($query) use ($outlet_id) {
            $query->whereIn('outlet_id', $outlet_id);
        })->select('id_warehouse', 'name')->get();

        $warehouse2 =  Warehouse::whereHas('warehouseOutlets', function ($query) use ($outlet_id) {
            $query->whereIn('outlet_id', $outlet_id);
        })->pluck('id_warehouse')->toArray();
        $warehouse_id = isset(request()->warehouse) ? request()->warehouse : $warehouse2;

        $perPage = 20;
        $page = 1;

        $search = '%'. $request->search . '%';

        if (isset($request->perPage)) {
            $perPage = isset(request()->perPage) ? request()->perPage : $perPage;
        }

        if (isset($request->page)) {
            $page = $request->page;
        }

        $offset = $perPage * $page - $perPage;
        $sql = '
        SELECT
           i.id as id,
           i.name as name,
           i.type as type,
           i.cogs as cogs,
           p.price as price,
           stockSum.stock
        FROM
           items i
           JOIN product_treatments p ON i.sales_information_id = p.id_product_treatment
           LEFT JOIN
                (SELECT
                    m.item_id as item_id,
                    SUM(m.qty) as stock
                FROM
                    mutations m
                WHERE m.warehouse_destination_id IN (%s)
                OR m.warehouse_origin_id IN (%s)
                GROUP BY
                    m.item_id
                ) stockSum ON stockSum.item_id = i.id
            WHERE (i.name LIKE ? OR i.type LIKE ?)
            ORDER BY i.created_at DESC LIMIT ? OFFSET ?
        ';

        $sqlTotal = '
            SELECT
                COUNT(raw.id) AS total
            FROM
                (SELECT
                    i.id
                FROM
                    items i
                JOIN product_treatments p ON i.sales_information_id = p.id_product_treatment
            WHERE (i.name LIKE ? OR i.type LIKE ?)
            GROUP BY i.id) raw

        ';

        $total = sprintf($sqlTotal);
        $itemTotal = DB::select($sqlTotal, [$search, $search]);
        $sql = sprintf($sql, implode(',', $warehouse_id),implode(',', $warehouse_id));
        $collections =  DB::select($sql, [$search, $search, $perPage, $offset]);
        return response()->json([
            'collections' => $collections,
            'warehouse' =>$warehouse,
            'perPage'       => $offset,
            'limit'         => $perPage,
            'count'         => $itemTotal[0]
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validateForm($request);

        $sellableId = null;

        DB::beginTransaction();

        try {
            if (!$validator->fails()) {
                $status =true;
                $message = "Successfully Created Item";
                $sellableId = $this->storeSalesInformation($request);

                $itemData = $request->only([
                    'category_id',
                    'uom_id',
                    'name',
                    'brand',
                    'unit_value',
                    'type',
                    'cogs',
                    'dispense_method',
                    'buffer_stock'
                ]);

                $item = new Item($itemData);
                $item->sales_information_id = $sellableId;
                $item->save();

                if (isset($request->boms) && count($request->boms)) {
                    // current item is having bill of material.
                    $this->storeBoms($request, $item);
                    $item->has_bom = 1;
                }

                if ($item->type == 'product') {
                    // product is stock category.
                    list($stock, $sellable_stock) = $this->storeBeginStock($request, $item);
                    $item->stock = $stock;
                    $item->sellable_stock = $sellable_stock;
                }

                $item->save();

                DB::commit();
            }else{
                $message = $validator->messages();
                $status = false;
            }
            return response()->json([
                'message' =>$message,
                'status' => $status,
            ]);

        } catch (Exception $e) {
            return response()->json(['message' => 'an error occured: '. $e->getMessage()], 500);
            DB::rollback();
        }
    }

    /**
     * Display a listing for select options.
     *
     * @return \Illuminate\Http\Response
     **/
    public function options(Request $request)
    {
        $options = [];
        $compact = isset($request->compact);
        $compactCols = ['id', 'sales_information_id', 'unit_value', 'uom_id', 'name', 'type', 'has_bom'];

        if (isset($request->type)) {
            if ( ! in_array($request->type, ['product', 'service'])) {
                return response()->json(['message' => 'unsupported type'], 400);
            }

            $hasBom = isset($request->has_bom) && $request->has_bom == 1;


            $options = Item::where('type', $request->type)
                            ->when($hasBom, function ($query) {
                                $query->whereHas('boms');
                            })
                            ->when($compact, function ($query) use ($compactCols) {
                                $query->select($compactCols);
                            })
                            ->get();

        } else {
            $relations = [
                'sales' => function ($query) {
                    $query->select('id_product_treatment', 'product_treatment_code', 'product_treatment_name', 'price', 'status_product_treatment');
                }
            ];

            $options =  Item::when($compact, function ($query) use ($compactCols, $relations) {
                                $query->select($compactCols)
                                            ->with($relations);
                            })->get();
        }

        return response()->json(['options' => $options]);
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    protected function storeSalesInformation(Request $request)
    {
        $sales = $request->sales;

        $product = new ProductTreatment;
        $product->product_treatment_name = $sales['name'];
        $product->price = $sales['price'];
        $product->duration = $sales['duration'];
        $product->status_product_treatment = $sales['status_product_treatment'];

        $itemType = 'P';

        if ($request->type == 'service') {
            $itemType = 'T';
            // $product->commissionDr = $sales['commission_dr'];
            // $product->commissionTr = $sales['commission_tr'];
            // $product->commtype = $sales['comm_type'] == 'percent' ? 0 : 1;
        }

        $product->product_treatment_code = ProductTreatment::generateTreatmentCode($itemType);

        $product->save();

        $outlets = [];

        foreach ($sales['outlets'] as $outlet_id) {
            $outlet = new ProductTreatmentOutlet;
            $outlet->outlet_id = $outlet_id;
            $outlets[] = $outlet;
        }

        $product->product_treatment_outlets()->saveMany($outlets);

        $points = [];
        foreach ($sales['points'] as $data) {
            $point = new ProductTreatmentPoint;
            $point->point = $data['point'];
            $points[] = $point;
        }
        $product->product_treatment_points()->saveMany($points);

        $log = new LogProductTreatment;
        $log->product_treatment_id = $product->id_product_treatment;
        $log->staff_id = auth()->user()->id;
        $log->price = $sales['price'];
        $log->save();

        return $product->id_product_treatment;
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    protected function storeBoms(Request $request, $item)
    {
        $itemBoms = [];

        foreach ($request->boms as $bom) {
            array_push($itemBoms, new Bom($bom));
        }

        $item->boms()->saveMany($itemBoms);
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    protected function updateBoms(Request $request, $item)
    {
        $currentIds = $item->boms()->pluck('id')->toArray();

        $oldRecords = array_filter($request->boms, function ($row) {
            return isset($row['id']);
        });

        $ids = array_map(function ($row) {
            return $row['id'];
        }, $oldRecords);

        $diff = array_diff($currentIds, $ids);

        if (count($diff)) {
            Bom::whereIn('id', $diff)->delete();
        }


        foreach ($request->boms as $bom) {
            $model = Bom::firstOrNew([
                'raw_item_id'   => $bom['raw_item_id'],
                'item_id'       => $item->id
            ]);

            $model->qty = $bom['qty'];
            $model->uom_id = $bom['uom_id'];
            $model->unit_value = $bom['unit_value'];
            $model->item_id = $item->id;
            $model->save();
        }
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    protected function storeBeginStock(Request $request, $item)
    {
        $stockData = [];

        $stockTotal = 0;
        $sellableStockTotal = 0;

        if (isset($request->stocks) && count($request->stocks) > 0) {
            foreach ($request->stocks as $stock) {
                $stockModel = new Mutation($stock);
                $stockModel->transdesc = 'BEGIN';
                $stockModel->balance = $stock['qty'];

                array_push($stockData, $stockModel);

                $stockTotal += $stock['qty'];
                $sellableStockTotal += $stock['sellable_stock'];
            }

            $item->mutations()->saveMany($stockData);
            // order matter: stock, sellable stock.
            return [$stockTotal, $sellableStockTotal];
        }

        return $this->storeDefaultBeginStock($item);
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function storeDefaultBeginStock($item)
    {
        Mutation::create([
            'warehouse_origin_id'       => null,
            'warehouse_destination_id'  => null,
            'rack_origin_id'            => null,
            'rack_destination_id'       => null,
            'sales_invoice_id'          => null,
            'purchase_invoice_id'       => null,
            'item_id'                   => $item->id,
            'transdate'                 => now(),
            'transtype'                 => 'in',
            'balance'                   => 0,
            'transdesc'                 => 'BEGIN EMPTY',
            'qty'                       => 0
        ]);

        return [0, 0];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $relations = [
            'boms'  => function ($query) {
                $query->with(['rawItem.uom', 'rawItem.sales' => function ($query) {
                    $query->select('id_product_treatment', 'price');
                }]);
            },
            'sales.outlets.outlet',
            'sales.points',
            'uom',
        ];

        $mutationRelations = [
            'mutations.warehouseDestination',
            'mutations.rackDestination',
            'mutations.warehouseOrigin',
            'mutations.rackOrigin',
            'mutations.purchaseOrder',
            'mutations.usage',
            'mutations.adjustment',
            'mutations.release',
            'mutations.finishedProduct'
        ];

        $item = Item::with($relations)->find($id);
        if ( ! $item) {
            return response()->json([
                'message'   => 'not found'
            ], 404);
        }

        if ( ! isset($request->compact)) {
            $item->load($mutationRelations);
        }

        return response()->json(['model' => $item]);
    }

    /**
     * Check current sellable stock by sales information.
     *
     * @param  int  $sales_information_id
     * @return \Illuminate\Http\Response
     **/
    public function sellableBySalesInformation($type, $sales_information_id,$warehouse_id)
    {
        if ($warehouse_id == 0 || $warehouse_id == "" || $warehouse_id == null) {
            $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->first();
            $warehouse_outlet = WarehouseOutlet::where('outlet_id', $outlet_id)->first();
            $warehouse_id = $warehouse_outlet->warehouse_id;
        }

        if ($type == 'package') {
            return $this->getSellablePackage($sales_information_id,$warehouse_id);
        }

        $relationCount = [
            'mutations'     => function ($query) use ($warehouse_id) {
                $query->select(DB::raw('SUM(qty) as qty_sum'))
                        ->where(function ($query) use ($warehouse_id) {
                            $query->where('warehouse_origin_id', $warehouse_id)
                                    ->orWhere('warehouse_destination_id', $warehouse_id);
                        });
            }
        ];

        $model = Item::withCount($relationCount)->where('sales_information_id', $sales_information_id)->first();
        if ( ! $model) {
            return response()->json(['message'  => 'Sales information not found'], 404);
        }

        $result = [
            'type'  => 'product',
            'name'  => $model->name,
            'sellable'  => $model->mutations_count,
            'buffer_stock' => $model->buffer_stock
        ];
        $warehouse = array($warehouse_id);
        if ($model->type == 'service') {
            $result['type'] = 'service';
            $bom = $model->boms->pluck('raw_item_id')->toArray();
            $boms = $model->boms->pluck('item_id')->toArray();
            $model2= Item::select('items.id','items.type','items.name','items.buffer_stock','boms.qty')
            ->withCount($relationCount)
            ->join('boms','boms.raw_item_id','items.id')
            ->whereIn('boms.item_id',$boms)
            ->whereIn('items.id', $bom)
            ->get();
            $result['sellable']= $model2;
        }

        return response()->json(['sellable' => $result]);
    }

    /**
     * Package sellable
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     **/
    public function getSellablePackage($id,$warehouse_id)
    {
        $package = Package::find($id);

        if ( ! $package) {
            return response()->json(['message'  => 'Package information not found'], 404);
        }

        $sellable = [
            'type'  => 'package',
            'name'  => $package->package_name,
            'sellable' => []
        ];

        $relationCount = [
            'mutations'     => function ($query) use ($warehouse_id) {
                $query->select(DB::raw('SUM(qty) as qty_sum'))
                        ->where(function ($query) use ($warehouse_id) {
                            $query->where('warehouse_origin_id', $warehouse_id)
                                    ->orWhere('warehouse_destination_id', $warehouse_id);
                        });
            }
        ];


        foreach ($package->package_detail as $detail) {
            $model = Item::withCount($relationCount)->where('sales_information_id', $detail->product_id)->first();

            $result = [
                'type'  => 'product',
                'name'  => $model['name'],
                'sellable'  => $model['mutations_count'],
                'buffer_stock' => $model['buffer_stock']
            ];

            if ($model['type'] == 'service') {
                $result['type'] = 'service';

                $bom = $model->boms->pluck('raw_item_id')->toArray();
                $boms = $model->boms->pluck('item_id')->toArray();
                $model2= Item::select('items.id','items.type','items.name','items.buffer_stock','boms.qty')
                ->withCount($relationCount)
                ->join('boms','boms.raw_item_id','items.id')
                ->whereIn('boms.item_id',$boms)
                ->whereIn('items.id', $bom)
                ->get();
                $result['sellable']= $model2;
            }

            array_push($sellable['sellable'], $result);
        }

        return response()->json(['sellable' => $sellable]);
    }

    /**
     * Inventory taking
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     **/
    public function takings(Request $request)
    {
        $relationCount = [
            'mutations'     => function ($query) use ($request) {
                $query->select(DB::raw('SUM(qty) as qty_sum'))
                        ->where('transdate', '<=', $request->date)
                        ->where(function ($query) use ($request) {
                            $query->where('warehouse_origin_id', $request->warehouse_id)
                                    ->orWhere('warehouse_destination_id', $request->warehouse_id);
                        });
            }
        ];

        $relations = [
            'uom'  => function ($query) {
                $query->select('id', 'alias');
            }
        ];

        $certainItems = [];

        if (isset($request->item_id)) {
            $certainItems = explode(',', $request->item_id);
        }

        $takings = Item::select(DB::raw("*, if(verified_at, if(verified_at >= '{$request->date}', 1, 0), 0) as lock_verified"))
                            ->withCount($relationCount)
                            ->with($relations)
			    ->whereHas('uom')
                            ->when(count($certainItems) && $request->all_item == 'false', function ($query) use ($certainItems) {
                                $query->whereIn('id', $certainItems);
                            })
                            ->where('type', 'product')
                            ->get();

        return response()->json([
            'takings'   => $takings
        ]);
    }

    /**
     * Store inventory taking.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     **/
    public function storeTaking(Request $request)
    {
        if (count($request->takings) <= 0) {
            return response()->json([
                'message'   => 'no checked items'
            ], 422);
        }

        $taking = new InventoryTaking();

        $taking->transdate   = $request->date;
        $taking->remarks     = $request->remarks;
        $taking->verified_by = $request->user()->id;

        DB::beginTransaction();

        try {
            $taking->save();

            Item::whereIn('id', $request->takings)->update([
                'verified_at'       => $taking->transdate,
                'verification_id'   => $taking->id
            ]);

            DB::commit();

            return response()->json([
                'status'    => 'success',
                'message'   => 'stored'
            ]);
        } catch (Exception $e) {
            DB::rollback();

            return response()->json([
                'message'   => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Item::find($id);

        $itemData = $request->only([
            'category_id',
            'uom_id',
            'name',
            'brand',
            'unit_value',
            'type',
            'cogs',
            'dispense_method',
            'buffer_stock'
        ]);


        $item->fill($itemData);

        try {
            DB::beginTransaction();

            $pt = ProductTreatment::find($item->sales_information_id);

            if ($pt) {
                $this->updateProductTreatment($request, $pt);
            }

            if (isset($request->boms) && count($request->boms)) {
                // current item is having bill of material.
                $this->updateBoms($request, $item);
                $item->has_bom = 1;
            } else {
                $item->has_bom = 0;
            }

            $item->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['message' => ''], 500);
        }
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function updateProductTreatment(Request $request, ProductTreatment $pt)
    {
        $pt->product_treatment_name = $request['sales']['name'];
        $pt->product_treatment_price = $request['sales']['price'];
        $pt->status_product_treatment = $request['sales']['status_product_treatment'];

        $currentOutlets = $pt->outlets()->pluck('outlet_id')->toArray();
        $updateOutlets = $request['sales']['outlets'];

        $points = [];

        if(isset($request['sales']['points'])) {
            $pt->points()->delete();
            foreach($request['sales']['points'] as $point) {
                $pt->points()->create([
                    'point' => (int) $point['point']
                ]);
            }
        }

        // $diff = array_diff($currentOutlets, $updateOutlets);
        // if (count($diff) > 0) {
        //     ProductTreatmentOutlet::where('product_id', $pt->id_product_treatment)->whereIn('outlet_id', $diff)->delete();
        // }


        // foreach ($currentOutlets as $outlet) {
        //     // ProductTreatmentOutlet::where('product_id', $pt->id_product_treatment)->where('outlet_id', $);
        // }

        // $pt->save();
    }

    /**
     * Get item stock on certain warehouse.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id item_id
     * @param  int  $warehouse_id warehouse_id
     * @return \Illuminate\Http\Response
     **/
    public function itemOnWarehouse(Request $request, $id, $warehouse_id)
    {
        $relationCount = [
            'mutations'     => function ($query) use ($warehouse_id) {
                $query->select(DB::raw('SUM(qty) as qty_sum'))
                        ->where(function ($query) use ($warehouse_id) {
                            $query->where('warehouse_origin_id', $warehouse_id)
                                    ->orWhere('warehouse_destination_id', $warehouse_id);
                        });
            }
        ];

        $relations = [
            'mutations' => function ($query) use ($warehouse_id) {
                $query->whereIn('transdesc', ['RECEIVE', 'BEGIN'])
                        ->where('warehouse_destination_id', $warehouse_id)
                        ->with('purchaseOrder');
            }
        ];

        $model = Item::withCount($relationCount)
                        ->with($relations)
                        ->find($id);


        return response()->json(['model' => $model]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    private function validateForm(Request $request)
    {
        if ($request->type == 'product') {
            return Validator::make($request->all(), [
                'uom_id' => 'required',
                'name' => 'required',
                'brand' => 'required',
                'unit_value' => 'required',
                'type' => 'required',
                'cogs' => 'required',
                'dispense_method' => 'required'
            ]);
        }

        return Validator::make($request->all(), [
            'name' => 'required',
            'type' => 'required',
        ]);
    }
}

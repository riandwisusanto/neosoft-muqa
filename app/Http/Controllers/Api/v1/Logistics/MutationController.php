<?php

namespace App\Http\Controllers\Api\v1\Logistics;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Logistics\Mutation;
use App\Models\Logistics\InventoryAdjustment;
use App\Models\Logistics\Item;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class MutationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function logs(Request $request)
    {
        $relations = [
            'usage'  => function ($query) {
                $query->select('id_usage', 'usage_code','customer_id','outlet_id');
            },
            'usage.customer'  => function ($query) {
                $query->select('id_customer', 'full_name');
            },
            
            'item' => function ($query) {
                $query->select('id', 'name','sales_information_id');
            },

            'purchaseOrder'  => function ($query) {
                $query->select('id', 'code');
            },

            'adjustment'    => function ($query) {
                $query->select('id', 'code');
            },

            'release'   => function ($query) {
                $query->select('id', 'code');
            },

            'finishedProduct' => function ($query) {
                $query->select('id', 'code');
            }
        ];

        $itemId = '';

        if($request->item_id) {
            $itemId =$request->item_id;
        }

        $previousDate = Carbon::create($request->date_from)->sub('1d')->format('d F Y');

        $previousSum = Mutation::where('item_id', $itemId)
                                ->where('transdate', '<', $request->date_from)
                                ->where(function ($query) use ($request) {
                                    $query->where('warehouse_origin_id', $request->warehouse_id)
                                            ->orWhere('warehouse_destination_id', $request->warehouse_id);
                                })
                                ->where('transdesc', '<>', 'BEGIN EMPTY')
                                ->sum('qty');

        $mutations = Mutation::with($relations)->where('item_id', $itemId)
                                ->whereBetween('transdate', [$request->date_from, $request->date_to])
                                ->where(function ($query) use ($request) {
                                    $query->where('warehouse_origin_id', $request->warehouse_id)
                                            ->orWhere('warehouse_destination_id', $request->warehouse_id);
                                })
                                ->where('transdesc', '<>', 'BEGIN EMPTY')
                                ->get();
        // return $mutations;

        return response()->json([
            'mutations'     => $mutations,
            'previous'      => [
                'date'      => $previousDate,
                'sum'       => $previousSum
            ]
        ]);
    }
    /**
     * Inventory Adjustment.
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function storeAdjustment(Request $request)
    {
        $adjustment = new InventoryAdjustment($request->all());
        $adjustment->code = InventoryAdjustment::generateCode();
        $adjustment->created_by = $request->user()->id;

        DB::beginTransaction();

        try {
            $adjustment->save();

            $qty = $adjustment->stock_adjustment;

            if ($adjustment->stock_adjustment_type == 'out') {
                $qty = $adjustment->stock_adjustment * -1;
            }

            $ref = Mutation::find($adjustment->ref_id);


            $mutationData = [
                'warehouse_destination_id'  => null,
                'warehouse_destination_id'  => null,
                'rack_origin_id'            => null,
                'rack_destination_id'       => null,
                'sales_invoice_id'          => null,
                'purchase_invoice_id'       => null,
                'item_id'                   => $adjustment->item_id,
                'transdate'                 => $adjustment->transdate,
                'transtype'                 => $adjustment->stock_adjustment_type,
                'transdesc'                 => $adjustment->transdesc,
                'adjustment_id'             => $adjustment->id,
                'balance'                   => 0,
                'qty'                       => $qty
            ];


            if ($adjustment->stock_adjustment_type == 'in') {
                $mutationData['warehouse_destination_id'] = $ref->warehouse_destination_id;
                $mutationData['rack_destination_id'] = $ref->rack_destination_id;
            } else {
                $mutationData['warehouse_origin_id'] = $ref->warehouse_destination_id;
                $mutationData['rack_origin_id'] = $ref->rack_destination_id;
            }

            $item = Item::find($adjustment->item_id);
            $item->sellable_stock += $qty;
            $item->stock += $qty;
            $item->save();

            Mutation::create($mutationData);

            $ref->balance += $qty;
            $ref->save();

            DB::commit();

            return response()->json(['model' => $adjustment]);
        } catch (Exception $e) {
            DB::rollback();

            return response()->json(['message' => $e->getMessage()], 500);
        }
        
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function byPurchaseOrder($purchase_invoice_id)
    {
        $relations = [
            'item',
            'warehouseDestination',
            'rackDestination'
        ];

        $mutations = Mutation::with($relations)
                            ->where('purchase_invoice_id', $purchase_invoice_id)
                            ->where('transdesc', 'RECEIVE')
                            ->get();
                        

        return response()->json(['collection' => $mutations]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showAdjustment($id)
    {
        $relations = [
            'warehouse', 'item', 'ref.purchaseOrder', 'adjuster'
        ];

        $model = InventoryAdjustment::with($relations)->findOrFail($id);

        return response()->json(['model' => $model]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Api\v1\Logistics;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Logistics\Mutation;
use App\Models\Logistics\InventoryAdjustment;
use App\Models\Logistics\WarehouseMutation;
use App\Models\Logistics\WarehouseMutationDetail;
use App\Models\Logistics\Purchases\Order;
use App\Models\Logistics\Purchases\OrderDetail;
use App\Models\Logistics\Item;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class WarehouseMutationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $relations = [
            'purchaseOrder', 'warehouseDestination', 'warehouseOrigin', 'outlet', 'sender'
        ];

        $collection = WarehouseMutation::with($relations)->orderBy('id', 'desc')->get();
        return response()->json(['collection' => $collection]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $purchaseOrder = Order::find($request->purchase_order_id);

        // if ( ! $purchaseOrder) {
        //     return response()->json([
        //         'message' => 'purchase order not found'
        //     ], 402);
        // }

        $model = new WarehouseMutation($request->all());
        $model->code = WarehouseMutation::generateCode();
        $model->transdate = date('Y-m-d');
        $model->created_by = $request->user()->id;

        DB::beginTransaction();

        try {
            $model->save();
            
            $this->updateDeliveredOrderDetail($request);
            $this->storeDetails($request, $model);
            $this->createMutation($request, $model);
            DB::commit();

            return response()->json([
                'model' => $model 
            ]);

        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                'message'   => $e->getMessage()
            ], 500);
        }
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function storeDetails(Request $request, $model)
    {
        foreach ($request->details as $detail) {
            $mutationDetail = new WarehouseMutationDetail($detail);
            $mutationDetail->warehouse_mutation_id = $model->id;
            $mutationDetail->save();
        }
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function updateDeliveredOrderDetail(Request $request)
    {
        foreach ($request->details as $detail) {
            $orderDetail = OrderDetail::find($detail['order_detail_id']);
            $orderDetail->delivered_qty = $orderDetail->delivered_qty + $detail['delivered_qty'];
            $orderDetail->save();
        }
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function createMutation(Request $request, $model)
    {
        foreach ($model->details as $detail) {
            $mutation = [
                'warehouse_origin_id'   => $model->warehouse_origin_id,
                'rack_origin_id'        => null,
                'purchase_invoice_id'     => $model->purchase_order_id,
                'item_id'               => $detail->item_id,
                'transdate'             => $model->transdate,
                'transtype'             => 'out',
                'cogs'                  => 0,
                'price'                 => str_replace(',', '', $detail->item->sales->price),
                'ref_id'                => null,
                'balance'               => 0,
                'transdesc'             => 'MUTATION DELIVERY',
                'qty'                   => $detail->delivered_qty * -1
            ];

            Mutation::create($mutation);
        }
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function byPurchaseOrder($order_id)
    {
        $relations = [
            'sender'
        ];

        $order_detail_id = OrderDetail::where('order_id', $order_id)->pluck('id')->toArray();

        $collection = WarehouseMutation::with($relations)
                            ->whereHas('details', function ($query) use ($order_detail_id) {
                                $query->whereIn('order_detail_id', $order_detail_id);
                            })
                            ->get();

        return response()->json(['collection' => $collection]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $relations = [
            'purchaseOrder', 'details.item.uom', 'warehouseDestination', 'warehouseOrigin', 'outlet', 'sender'
        ];

        $model = WarehouseMutation::with($relations)->findOrFail($id);

        return response()->json([
            'model' => $model
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

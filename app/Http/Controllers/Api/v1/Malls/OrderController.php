<?php

namespace App\Http\Controllers\Api\v1\Malls;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

class OrderController extends Controller
{

    /** @var Type $var description */
    protected $client = null;

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function __construct()
    {
        $this->client = new Client([
            'base_uri'  => env('SHOP_SERVICE'),
            'headers'   => [
                'Authorization' => 'Bearer '. env('SHOP_TOKEN')
            ]
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $res = null;
        try {
            $res = $this->client->request('GET', 'mall/api/v1/orders');
            // return $res->getBody()->getContents();
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
        

        $decoded = json_decode($res->getBody()->getContents());

        return response()->json($decoded);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $res = $this->client->request('GET', 'mall/api/v1/orders/'. $id);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }

        return $res->getBody()->getContents();

        $decoded = json_decode($res->getBody()->getContents());

        if ( ! $decoded->model) {
            return response()->json(['message' => 'not found'], 404);
        }

        return response()->json($decoded);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $form = $request->only([
            'status', 'resi_no'
        ]);

        try {
            $res = $this->client->request('PUT', 'mall/api/v1/orders/'. $id, [
                'form_params' => $form
            ]);
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }

        $decoded = json_decode($res->getBody()->getContents());

        return response()->json($decoded);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

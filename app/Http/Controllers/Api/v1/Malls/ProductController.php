<?php

namespace App\Http\Controllers\Api\v1\Malls;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Malls\Product;
use App\Models\Malls\Media;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $relations = [
            'posProduct'    => function ($query) {
                $relations = [
                    'item' => function ($query) {

                        $relations = [
                            'uom'   => function ($query) {
                                $query->select('id', 'alias');
                            }
                        ];

                        $query->select('id', 'sales_information_id', 'sellable_stock', 'brand', 'uom_id', 'unit_value')
                                ->with($relations);
                    }
                ];

                $query->select('id_product_treatment', 'product_treatment_name', 'price')
                        ->with($relations);
            },

            'weightUom'   => function ($query) {
                $query->select('id', 'alias');
            },

            'media'
        ];
        
        $products = Product::with($relations)->get()->toArray();

        $relationMapper = function ($row) {
            $posProduct = $row['pos_product'];
            $item = $posProduct['item'];

            $row['name'] = $posProduct['product_treatment_name'];
            $row['price'] = str_replace(',', '', $posProduct['price']);
            $row['uom_display'] = $item['uom']['alias'];
            $row['unit_value'] = $item['unit_value'];
            $row['sellable_stock'] = $item['sellable_stock'];
            $row['brand'] = $item['brand'];
            $row['weight_uom_display'] = $row['weight_uom']['alias'];
            $row['informations'] = json_decode($row['informations']);
            
            
            unset($row['pos_product']);
            unset($row['weight_uom']);
            return $row;
        };
        
        $collection = array_map($relationMapper, $products);

        return response()->json(['collection' => $collection]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $productData = $request->only([
            'product_id',
            'discount_1',
            'discount_2',
            'discount_type',
            'weight_uom_id',
            'weight',
            'description',
            'informations'
        ]);

        DB::beginTransaction();

        try {
            $product = Product::create($productData);

            $this->storeMedia($request, $product);

            DB::commit();
            return response()->json(['product' => $product]);
        } catch (Exception $e) {

            DB::rollback();
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function storeMedia(Request $request, $product)
    {
        foreach ($request->media as $index => $media) {
            $filename = "product-". date('mdYHis') . $index .".png";
            
            $data_uri = explode(',', $media['url']);
            $encoded_image = $data_uri[1];
            $decoded_image = base64_decode($encoded_image);
            file_put_contents(__DIR__. '/../../../../../../storage/app/public/malls/media/'. $filename, $decoded_image);

            Media::create([
                'mall_product_id'   => $product->id,
                'title'             => $media['title'],
                'url'               => $filename
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $relations = ['media', 'weightUom', 'posProduct.item.uom'];
        $model = Product::with($relations)->find($id)->toArray();

        if ( ! $model) {
            return response()->json(['message' => 'not found'], 404);
        }

        $posProduct = $model['pos_product'];
        $item = $posProduct['item'];

        $model['name'] = $posProduct['product_treatment_name'];
        $model['price'] = str_replace(',', '', $posProduct['price']);
        $model['uom_display'] = $item['uom']['alias'];
        $model['unit_value'] = $item['unit_value'];
        $model['sellable_stock'] = $item['sellable_stock'];
        $model['brand'] = $item['brand'];
        $model['weight_uom_display'] = $model['weight_uom']['alias'];
        $model['informations'] = json_decode($model['informations']);
        $model['use_dicount_gimmick'] = $model['use_dicount_gimmick'];

        
        unset($model['pos_product']);
        unset($model['weight_uom']);
        
        return response()->json(['model' => $model]);
    }

    public function updateMedia(Request $request, $product)
    {
        $currentIds = $product->media()->pluck('id')->toArray();

        $oldRecords = array_filter($request->media, function ($row) {
            return isset($row['id']);
        });

        $ids = array_map(function ($row) {
            return $row['id'];
        }, $oldRecords);

        $shouldDelete = array_diff($currentIds, $ids);

        if (count($shouldDelete)) {
            Media::whereIn('id', $shouldDelete)->delete();
        }
        foreach ($request->media as $index => $media) {
            $filename = "product-". date('mdYHis') . $index .".png";
            $data_uri = explode(',', $media['url']);

            $encoded_image = $data_uri[1];
            $decoded_image = base64_decode($encoded_image);
            file_put_contents(__DIR__. '/../../../../../../storage/app/public/malls/media/'. $filename, $decoded_image);

            if(isset($media['id'])){
                $update = Media::find($media['id']);
                $update->mall_product_id = $product->id;
                $update->url = $filename;
                $update->title = $media['title'];
                $update->save();
            }else{
                $insert = new Media;
                $insert->mall_product_id = $product->id;
                $insert->url = $filename;
                $insert->title = $media['title'];
                $insert->save();
            }
        }

        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);

        $productData = $request->only([
            'product_id',
            'discount_1',
            'discount_2',
            'discount_type',
            'weight_uom_id',
            'weight',
            'description'
        ]);

      
        DB::beginTransaction();

        try {
            $product->fill($productData);
            if (isset($request->media) && count($request->media)) {
                $this->updateMedia($request, $product);
            }

            $product->save();        
            
            DB::commit();
            return response()->json(['product' => $product]);
        } catch (Exception $e) {

            DB::rollback();
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Media::where('mall_product_id', $id)->delete();
        Product::where('id', $id)->delete();

        return response()->json(['status' => 'deleted']);
    }
}

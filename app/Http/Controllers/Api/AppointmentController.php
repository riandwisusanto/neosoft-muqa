<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Appointment;
use App\Models\ProductTreatment;
use App\Models\Outlet;
use App\Models\Therapist;
use App\Models\Customer;
use Carbon\carbon;
use App\Models\AppointmentTherapist;

class AppointmentController extends Controller
{
    public function AppointmentData(Request $request)
    {
        $status = true;
        $message = "get data success";

        $status_treatment = false;
        $customer = Customer::find($request->get('id_customer'));

        if ($customer->invoice) {
            $status_treatment = true;
        }

        $outlet = Outlet::where('status_outlet', 1)
            ->select('id_outlet', 'outlet_name')
            ->get();
        $therapist = Therapist::where('status_therapist', 1)
            ->select('id_therapist', 'therapist_name')
            ->get();
        $treatment = ProductTreatment::where('product_treatment_code', 'like', 'T-%')
            ->get();
        $data = collect([
            'outlet' => $outlet,
            'therapist' => $therapist,
            'treatment' => $treatment,
        ]);
        $code = 200;
        return response()->json([
            'status' => $status,
            'status_treatment' => $status_treatment,
            'message' => $message,
            'data' => $data,
        ], $code);
    }

    public function AppointmentCreate(Request $request)
    {
        $status = false;
        $message = "";
        $data = null;
        $code = 401;

        $validator = Validator::make($request->all(), [
            'customer_id' => 'required',
            'outlet_id' => 'required',
            'come_date' => 'required',
            'start_time' => 'required',
            'therapist_id' => 'required',
        ]);

        if (!$validator->fails()) {
            $status = true;
            $message = "create appointment success";

            $save = new Appointment;
            if ($request->treatment_id) {
                $save->treatment_id = $request->treatment_id;
                $duration = ProductTreatment::find($request->treatment_id);
                $start = date('H:i:s', strtotime($request->start_time));
                $end = date('H:i:s', strtotime('+' . $duration->duration . ' minutes', strtotime($start)));
            } else {
                $start = date('H:i:s', strtotime($request->start_time));
                $end = date('H:i:s', strtotime('+60 minutes', strtotime($start)));
            }

            $save->customer_id = $request->customer_id;
            $save->outlet_id = $request->outlet_id;
            $save->come_date = Carbon::createFromFormat('d/m/Y', $request->come_date)->format('Y-m-d');
            $save->start_time = $start;
            $save->end_time = $end;
            $save->status_appointment = 1;
            $save->save();

            $appointmentThe = new AppointmentTherapist;
            $appointmentThe->therapist_id = $request->therapist_id;
            $appointmentThe->appointment_id = $save->id_appointments;
            $appointmentThe->save();

            $code = 200;

            $data = Appointment::with([
                'product_treatment',
                'therapist',
                'outlet',
            ])
                ->find($save->id_appointments);
        } else {
            $errors = $validator->errors();
            $message = $errors;
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ], $code);
    }

    public function AppointmentDetail(Request $request)
    {
        $status = false;
        $message = "Param Required";
        $data = null;
        $code = 401;
        $reschedule = false;

        $id_appointment = $request->get('id_appointment');
        if ($id_appointment) {
            $status = true;
            $message = "get data success";
            $data = Appointment::find($id_appointment);

            $now = now('Asia/Jakarta')->diffInMinutes($data->created_at);
            if ($now < 120 && $data->status_appointment == 1) {
                $reschedule = true;
            }

            // $data = collect($app);
            // $data->prepend($now, 'time_server');
            // $data->all();
            $code = 200;
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'reschedule' => $reschedule,
            'data' => $data,
        ], $code);
    }

    public function AppointmentReschedule(Request $request, $id)
    {
        $status = false;
        $message = "";
        $data = null;
        $code = 401;

        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'time' => 'required',
        ]);

        if (!$validator->fails()) {
            $status = true;
            $message = "update appointment success";

            $update = Appointment::find($id);
            if ($update->product_treatment) {
                $start = date('H:i:s', strtotime($request->time));
                $end = date('H:i:s', strtotime('+' . $update->product_treatment->duration . ' minutes', strtotime($start)));
            } else {
                $start = date('H:i:s', strtotime($request->time));
                $end = date('H:i:s', strtotime('+60 minutes', strtotime($start)));
            }
            $update->start_time = $start;
            $update->end_time = $end;
            $update->come_date = Carbon::createFromFormat('d/m/Y', $request->date)->format('Y-m-d');
            $update->update();

            $data = $update;
            $code = 200;
        } else {
            $errors = $validator->errors();
            $message = $errors;
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ], $code);
    }

    public function cancelAppointment(Request $request, $id)
    {
        // $status = false;
        // $message = "id null";
        // $code = 401;

        // if ($id) {
        // }
        $status = true;
        $message = "delete success";
        Appointment::find($id)->delete();
        $code = 200;

        return response()->json([
            'status' => $status,
            'message' => $message,
        ], $code);
    }
}

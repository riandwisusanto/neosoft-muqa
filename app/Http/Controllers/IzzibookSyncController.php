<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Invoice;
use App\Models\Logistics\Purchases\Order;
use App\Models\Manufactures\FinishedProduct;
use App\Models\LogIzzibook;
use App\Services\CustomerService;
use App\Services\InvoiceService;
use App\Services\PurchaseOrderService;
use App\Services\PaymentService;
use App\Services\FinishedProductService;
use Illuminate\Http\Request;
use App\Models\Payment;
use Illuminate\Support\Facades\Gate;
use Auth;
use DataTables;

class IzzibookSyncController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function patients()
    {
        return view('izzibook.patients');
    }

    public function patientsData()
    {
        $data = LogIzzibook::where('table_name', 'customers');
        return DataTables::of($data)
                ->addColumn('divider', function($row) {
                    return '';
                })
                ->escapeColumns([])
                ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function retryPatient($code)
    {
        // initialization new customer
        $customer = Customer::where('induk_customer', $code)->firstOrFail();

        // send to izzibook
        $izzibookCustomer = new CustomerService();
        $izzibookCustomer->store($customer);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function invoices()
    {
        return view('izzibook.invoices');
    }

    public function invoicesData()
    {
        $data = LogIzzibook::where('table_name', 'invoices');
        return DataTables::of($data)
                ->addColumn('divider', function($row) {
                    return '';
                })
                ->escapeColumns([])
                ->make(true);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function payments()
    {
        return view('izzibook.payments');
    }

    public function paymentsData()
    {
        $data = LogIzzibook::where('table_name', 'payments');
        return DataTables::of($data)
                ->addColumn('divider', function($row) {
                    return '';
                })
                ->escapeColumns([])
                ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function purchaseOrders()
    {
        $data = LogIzzibook::where('table_name', 'purchase_order')->orderBy('req_time', 'desc')->get();
        return view('izzibook.orders', compact('data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function warehouseMutations()
    {
        $data = LogIzzibook::where('table_name', 'warehouse_mutation')->orderBy('req_time', 'desc')->get();
        return view('izzibook.warehouse_mutations', compact('data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function finishedProduct()
    {
        $data = LogIzzibook::where('table_name', 'finished_product')->orderBy('req_time', 'desc')->get();
        return view('izzibook.finished_product', compact('data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function retryInvoice($code)
    {
        // initialization new invoice
        $invoice = Invoice::where('inv_code', $code)->firstOrFail();

        // send to izzibook
        $izzibookInvoice = new InvoiceService();
        $izzibookInvoice->store($invoice);
    
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function retryFinishedProduct($code)
    {
        // initialization new invoice
        $invoice = FinishedProduct::where('code', $code)->firstOrFail();

        // send to izzibook
        $izzibookInvoice = new InvoiceService();
        $izzibookInvoice->store($invoice);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function retryPayment($id)
    {
        $payment = Payment::where('id_payment', $id)->firstOrFail();

        // send to izzibook
        $izzibookPayment = new PaymentService();
        $izzibookPayment->store($payment);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function retryPurchaseOrder($code)
    {
        $order = Order::where('code', $code)->firstOrFail();

        // send to izzibook
        $izzibookService = new PurchaseOrderService();
        $izzibookService->store($order);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

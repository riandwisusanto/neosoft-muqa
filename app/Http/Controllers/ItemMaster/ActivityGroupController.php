<?php

namespace App\Http\Controllers\ItemMaster;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use DataTables;
use App\Models\ActivityGroup;
use Illuminate\Support\Facades\Gate;

class ActivityGroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin') || Gate::allows('sales')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return redirect(route('item-master.activities.index'));
    }

    /**
    * undocumented function summary
    *
    * Undocumented function long description
    *
    * @param Type $var Description
    * @return type
    * @throws conditon
    **/
    public function listActivityGroup()
    {
        $collection = ActivityGroup::all();
        $data = array();
        
        foreach ($collection as $activityGroups) {
            $row = [];
            $extra_field = '';
            $status_activity_group = '';

            if ($activityGroups->extra_field) {
                $extra_field = '&#10004;';
            }
            if ($activityGroups->status_activity_group) {
                $status_activity_group = '&#10004;';
            }
            
            $row[] = $activityGroups->activity_group_name;
            $row[] = $status_activity_group;
            $row[] = '
                        <a class="btn btn-success btn-sm" href="#" onclick="editFormActivityGroup(\''.$activityGroups->id_activity_group. '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a> 
                        <a class="btn btn-danger btn-sm" href="#" onclick="deleteDataActivityGroup(\''.$activityGroups->id_activity_group. '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>
                    ';
            
            $data[] = $row;
        }


        return DataTables::of($data)->escapeColumns([])->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $save = new ActivityGroup;
        $save->activity_group_name = $request->activity_group_name;
        $save->status_activity_group = 1;
        $save->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return response()->json(ActivityGroup::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $update= ActivityGroup::findOrFail($id);
        if ($request->disable == true) {
            $update->status_activity_group = 0;
            $update->update();
            return response()->json(true);
        }
        

        $update->activity_group_name = $request->activity_group_name;
        if ($request->status_activity_group) {
            $update->status_activity_group = 1;
        } else {
            $update->status_activity_group = 0;
        }

        $update->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delete= ActivityGroup::findOrFail($id);
        if ($delete->activity_groups) {
            echo "error";
        } else {
            $delete->delete();
        }
    }
}

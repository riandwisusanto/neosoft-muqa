<?php

namespace App\Http\Controllers\ItemMaster;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use DataTables;
use App\Models\Outlet;
use App\Models\Consultant;
use App\Models\ConsultantDetail;
use App\Models\UserOutlet;
use Auth;

class ConsultantController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $branch = Outlet::whereIn('id_outlet', $outlet_id)->get();
        return view('item-master.consultants.index', compact('branch'));
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function listData()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlets = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $collection = Consultant::where('id_consultant', 'like', '%')
                    ->whereHas('consultant_outlets', function ($q) use ($outlet_id) {
                        return $q->whereIn('outlet_id', $outlet_id);
                    })
                    ->orderBy('created_at', 'desc')->get();

        $data = array();
        foreach ($collection as $consultant) {
            $row = [];
            $branch = '';
            if (count($consultant->details)) {
                foreach ($consultant->details as $detail) {
                    $branch .= $detail->outlet->outlet_name. '<br>';
                }
            }
            $status_consultant = '';
            if ($consultant->status_consultant) {
                $status_consultant = '&#10004;';
            }

            $row[] = $consultant->consultant_name;
            $row[] = $consultant->join_date->format('d F Y');
            $row[] = $branch;
            $row[] = $status_consultant;
            $row[] = '
                        <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="editForm(\''.$consultant->id_consultant. '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                        <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="deleteData(\''.$consultant->id_consultant. '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>
                    ';

            $data[] = $row;
        }


        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Consultant;
        $model->consultant_name = $request->consultant_name;
        $model->join_date = now();
        $model->status_consultant = 1;
        $model->save();


        if (count($request->branch)) {
            foreach ($request->branch as $item) {
                ConsultantDetail::create([
                    'outlet_id'     => $item,
                    'consultant_id' => $model->id_consultant
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $relations = [
            'details' => function ($query) {
                $query->select('consultant_id', 'outlet_id');
            }
        ];

        $model = Consultant::with($relations)->find($id);

        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Consultant::find($id);
        if ($request->disable == true) {
            $model->status_consultant = 0;
            $model->update();
            return response()->json(true);
        }

        $model->consultant_name = $request->consultant_name;

        if (empty($request->status_consultant)) {
            $model->status_consultant = 0;
        } else {
            $model->status_consultant = $request->status_consultant;
        }

        $model->save();

        $before = ConsultantDetail::where('consultant_id', $id)->pluck('outlet_id')->toArray();
        $shouldDelete = array_diff($before, $request->branch);

        if (count($shouldDelete)) {
            ConsultantDetail::where('consultant_id', $id)
                                ->whereIn('outlet_id', $shouldDelete)
                                ->delete();
        }

        foreach ($request->branch as $outlet_id) {
            ConsultantDetail::firstOrCreate([
                'consultant_id'     => $model->id_consultant,
                'outlet_id'         => $outlet_id
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Consultant::findOrFail($id);

        if ($delete->invoice) {
            echo "error";
        } else {
            $delete->delete();
        }
    }
}

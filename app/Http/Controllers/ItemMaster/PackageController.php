<?php

namespace App\Http\Controllers\ItemMaster;

use App\Http\Controllers\Controller;
use App\Models\LogPricePackage;
use App\Models\Outlet;
use App\Models\Package;
use App\Models\PackageDetail;
use App\Models\PackageOutlet;
use App\Models\PackagePoint;
use App\Models\ProductTreatment;
use App\Models\UserOutlet;
use Carbon\Carbon;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Auth;

class PackageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlets = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $user_id = Auth::user()->id;
        $productTreatment = ProductTreatment::where('status_product_treatment', 1)
                                ->whereHas('product_treatment_outlets', function ($q) use ($outlet_id) {
                                    return $q->whereIn('outlet_id', $outlet_id);
                                })->get();


        return view('item-master.package.index', compact('outlets', 'productTreatment'));
    }

    public function listData()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $package = Package::where('status_package', 1)
        ->whereHas('package_outlet', function ($q) use ($outlet_id) {
            return $q->whereIn('outlet_id', $outlet_id);
        })->get();
        $data = array();
        foreach ($package as $list) {
            $row = array();

            $row[] = $list->package_code;
            $row[] = $list->package_name;
            $row[] = Carbon::parse($list->created_at)->format('d/m/Y');
            $row[] = $list->price;
            $row[] = $list->remarks;
            $row[] = $list->status_package ? '&#10004;' : '';
            $row[] = "
                    <a onclick='editForm(" . $list->id_package . ")' class='btn btn-success btn-sm'><i class='fa fa-edit'></i> Edit</a>
                    <a onclick='deleteItem(" . $list->id_package . ")' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i> Delete</a>";
            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function chooseProduct($id)
    {
        $product = ProductTreatment::find($id);
        return json_encode($product);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $total = 0;
        $save_json = json_decode($request->save_json);

        foreach ($save_json as $key => $data) {
            // $oke = str_replace(array('.', ','), "", $data->price);
            // dd($oke);
            $total += intval(str_replace(array('.', ','), "", $data->price));
        }

        //dd($total);
        $package = new Package;
        $package->package_code = Package::generatePackageCode($request->type);
        $package->package_name = $request->package_name;
        $package->status_package = 1;
        $package->remarks = $request->remarks;
        $package->price = $total;
        $package->commission = $request->commission;
        $package->commissionType = $request->commissionType;
        $package->save();

        $log = new LogPricePackage();
        $log->package_id = $package->id_package;
        $log->staff_id = auth()->user()->id;
//        $log->price = str_replace(",", "", $total);
        $log->price = $total;
        $log->save();

        $save_json_point = json_decode($request->save_json_point);
        foreach ($save_json_point as $key => $data) {
            $point = new PackagePoint;
            $point->package_id = $package->id_package;
            $point->point = $data->point;
            $point->save();
        }

        $save_json = json_decode($request->save_json);
        foreach ($save_json as $key => $data) {
            $package_detail = new PackageDetail;
            $package_detail->package_id = $package->id_package;
            $package_detail->product_id = $data->type_id;
            $package_detail->qty = $data->qty;
            $package_detail->price = intval(str_replace(array('.', ','), "", $data->price));
            $package_detail->save();
        }

        foreach ($request->outlet as $key => $value) {
            $package_outlet = new PackageOutlet;
            $package_outlet->outlet_id = $value;
            $package_outlet->package_id = $package->id_package;
            $package_outlet->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package = Package::with('price_logs')->find($id);
        //$package_d = PackageDetail::all()->pluck('id_package_detail');
        //dd(last($package));
        //dd($package_d);

        foreach ($package->price_logs as $priceLog) {
            $priceLog->price = format_money($priceLog->price);
            $priceLog->edited_by;
        }

        $product = array();
        foreach ($package->package_detail as $productx) {
            //dd($productx->product_price);
            $product[] = array(
                'type_name' => $productx->product_treatment->product_treatment_name,
                'type_id' => $productx->product_id,
                'qty' => $productx->qty,
                'price' => $productx->price,
                'current_price' => $productx->product_price,
                'id_package_detail' => $productx->id_package_detail,
            );
        }

        $point = array();
        foreach ($package->package_point as $list) {
            $point[] = array('point' => $list->point);
        }

        $outlet = PackageOutlet::where('package_id', $id)->pluck('outlet_id');

        return response()->json([
            'point' => $point,
            'product' => $product,
            'outlet' => $outlet,
            'package' => $package,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $package = Package::find($id);

        if ($request->disable == true) {
            $package->status_package = 0;
            $package->update();
            return response()->json(true);
        }

        $total = 0;
        $save_json = json_decode($request->save_json);

        foreach ($save_json as $key => $data) {
            $total += intval(str_replace(array('.', ','), "", $data->price));
        }

        $package->package_name = $request->package_name;
        $package->status_package = 1;
        $package->remarks = $request->remarks;
        $package->commission = $request->commission;
        $package->commissionType = $request->commissionType;

        $save_json = json_decode($request->save_json);
        $product_id = [];
        foreach ($save_json as $key => $data) {
            $product_id[] = $data->type_id;
        }

        $product = PackageDetail::where('package_id', $id)->pluck('product_id')->toArray();
        $productDelete = array_diff($product, $product_id);

        if (count($productDelete)) {
            PackageDetail::where('package_id', $id)
                ->whereIn('product_id', $productDelete)
                ->delete();
        }

        foreach ($save_json as $key => $data) {
            $cek = '';
            if (isset($data->id_package_detail)) {
                $cek = $data->id_package_detail;
            }
            PackageDetail::firstOrCreate(
                ['id_package_detail' => $cek],
                [
                    'package_id' => $id,
                    'product_id' => $data->type_id,
                    'qty' => $data->qty,
                    'price' => intval(str_replace(array('.', ','), "", $data->price)),
                ]
            );
        }

        $outlet = PackageOutlet::where('package_id', $id)->pluck('outlet_id')->toArray();
        $outletDelete = array_diff($outlet, $request->outlet);

        if (count($outletDelete)) {
            PackageOutlet::where('package_id', $id)
                ->whereIn('outlet_id', $outletDelete)
                ->delete();
        }

        foreach ($request->outlet as $key => $value) {
            PackageOutlet::updateOrCreate([
                'package_id' => $id,
                'outlet_id' => $value,
            ]);
        }

        if (str_replace(",", "", $package->price) != str_replace(",", "", $total)) {
            $log = new LogPricePackage();
            $log->package_id = $package->id_package;
            $log->staff_id = auth()->user()->id;
//        $log->price = str_replace(",", "", $total);
            $log->price = $total;
            $log->save();
        }

        $package->price = $total;
        $package->update();

        $save_json_point = json_decode($request->save_json_point);
        $point_id = [];
        foreach ($save_json_point as $key => $data) {
            $point_id[] = $data->point;
        }

        $point = PackagePoint::where('package_id', $id)->pluck('point')->toArray();
        $pointDelete = array_diff($point, $point_id);

        if (count($pointDelete)) {
            PackagePoint::where('package_id', $id)
                ->whereIn('point', $pointDelete)
                ->delete();
        }

        foreach ($save_json_point as $key => $data) {
            PackagePoint::updateOrCreate([
                'package_id' => $id,
                'point' => $data->point,
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Package::findOrFail($id);
        //dd($delete->invoice_detail);
        if ($delete->invoice_detail) {
            echo "error";
        } else {
            Package::findOrFail($id)->delete();
            PackageDetail::where('package_id', $id)->delete();
            PackageOutlet::where('package_id', $id)->delete();
            PackagePoint::where('package_id', $id)->delete();
        }

        // $delete->status_package = 0;
        // $delete->update();
    }
}

<?php

namespace App\Http\Controllers\ItemMaster;

use App\Http\Controllers\Controller;
use App\Models\Province;
use App\Models\Country;

use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Auth;

class CityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = Country::all();
        return view('item-master.cities.index', compact('countries'));
    }

    public function listData()
    {
        $collection = Province::orderBy('created_at', 'desc')->get();

        $data = array();
        foreach ($collection as $city) {
            $row = [];

            $row[] = $city->country->country;
            $row[] = $city->province;
            
            $row[] = '
                        <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="editForm(\'' . $city->id_province . '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                        <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="deleteData(\'' . $city->id_province . '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>
                    ';

            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $save = new Province;
        $save->country_id   = $request->country_id;
        $save->province     = $request->province;
        $save->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = Province::find($id);

        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $model = Province::find($id);

        $model->country_id  = $request->country_id;
        $model->province    = $request->province;
        $model->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Province::find($id);
        if ($model->customer) {
            echo "error";
        } else {
            $model->delete();
        }
    }
}

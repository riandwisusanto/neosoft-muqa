<?php

namespace App\Http\Controllers\ItemMaster;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Http\Controllers\Controller;

use App\Models\Therapist;
use App\Models\TherapistSchedule;

use DataTables;
use Auth;

class TherapistScheduleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        $id = $req->id;
        $days = [
            'Sunday',
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
        ];
        $therapist = Therapist::findOrFail($id);
        return view('item-master.therapistSchdeules.index', compact('therapist', 'id', 'days'));
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function listData($id)
    {
        $collection = TherapistSchedule::where('therapist_id', $id)->orderBy('day', 'asc')->get();
        $data = array();
        foreach ($collection as $r) {
            $row = [];
            $row[] = $r->therapist->therapist_name;
            $row[] = $r->day;
            $row[] = $r->start_time;
            $row[] = $r->end_time;

            $row[] = "
                    <a onclick='editForm(".$r->id_therapist_schedule.")' class='btn btn-success btn-sm'><i class='fa fa-edit'></i> Edit</a>
                    <a onclick='deleteData(".$r->id_therapist_schedule.")' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i> Delete</a>
                    ";
            $data[] = $row;
        }


        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $model = new TherapistSchedule($req->all());
        $model->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = TherapistSchedule::find($id);
        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $therapist = TherapistSchedule::findOrFail($id);
        $therapist->update($request->except('therapist_id'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = TherapistSchedule::findOrFail($id);
        $delete->delete();
    }
}

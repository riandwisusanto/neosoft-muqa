<?php

namespace App\Http\Controllers\ItemMaster;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DataTables;
use Illuminate\Support\Facades\Gate;
use App\Models\Rank;
use App\Models\Invoice;
use Auth;

class RankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('all')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });
    }

    public function index()
    {
        return view('item-master.ranks.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function listData()
    {
        $ranks = Rank::all();
        //dd($ranks);
        $data = array();
        foreach ($ranks as $list) {
            $row = array();
            $row[] = $list->name;
            $row[] = format_money($list->value_1);
            $row[] = format_money($list->value_2);
            $row[] = '
                        <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="editForm(\'' . $list->id_rank . '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                        <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="deleteData(\'' . $list->id_rank . '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>
                    ';
            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function rankCustomer($id)
    {
        $summary = Invoice::where('customer_id', $id)
        ->where('void_invoice', 0)
        ->where('status_inv', 1)
        ->sum('total');

        $getRank = Rank::where('value_1', '<=', $summary)
        ->where('value_2', '>=', $summary)
        ->first();

        return json_encode($getRank);
    }

    public function rankStar($id)
    {
        $summary = Invoice::where('customer_id', $id)
        ->where('void_invoice', 0)
        ->where('status_inv', 1)
        ->sum('total');

        return json_encode($summary);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ranks = new Rank;
        $ranks->name = $request->name;
        $ranks->desc = $request->desc;
        $ranks->value_1 = str_replace(",", "", $request->value_1);
        $ranks->value_2 = str_replace(",", "", $request->value_2);

        $ranks->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ranks = Rank::findOrFail($id);
        return response()->json($ranks);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ranks = Rank::findOrFail($id);

        $ranks->name = $request->name;
        $ranks->desc = $request->desc;
        $ranks->value_1 = str_replace(",", "", $request->value_1);
        $ranks->value_2 = str_replace(",", "", $request->value_2);

        $ranks->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Rank::findOrFail($id)->delete();
    }
}

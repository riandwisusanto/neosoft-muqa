<?php

namespace App\Http\Controllers\ItemMaster;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Http\Controllers\Controller;

use Carbon\Carbon;

use App\Models\Therapist;
use App\Models\TherapistLeave;

use DataTables;
use Auth;

class TherapistLeaveController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        $id = $req->id;
        $therapist = Therapist::findOrFail($id);
        return view('item-master.therapistLeaves.index', compact('therapist', 'id'));
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function listData($id)
    {
        $collection = TherapistLeave::where('therapist_id', $id)->orderBy('id_therapist_leave', 'desc')->get();
        $data = array();
        foreach ($collection as $r) {
            $row = [];
            $row[] = $r->therapist->therapist_name;
            $row[] = $r->start_date;
            $row[] = $r->end_date;
            $row[] = $r->description;

            $row[] = "
                    <a onclick='editForm(".$r->id_therapist_leave.")' class='btn btn-success btn-sm'><i class='fa fa-edit'></i> Edit</a>
                    <a onclick='deleteData(".$r->id_therapist_leave.")' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i> Delete</a>
                    ";
            $data[] = $row;
        }


        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        $model = new TherapistLeave($req->all());
        $model->start_date = Carbon::createFromFormat('d/m/Y', $req->start_date)->format('Y-m-d');
        $model->end_date = Carbon::createFromFormat('d/m/Y', $req->end_date)->format('Y-m-d');

        $model->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = TherapistLeave::find($id);
        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $therapist = TherapistLeave::findOrFail($id);
        $therapist->start_date = Carbon::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d');
        $therapist->end_date = Carbon::createFromFormat('d/m/Y', $request->end_date)->format('Y-m-d');
        $therapist->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = TherapistLeave::findOrFail($id);
        $delete->delete();
    }
}

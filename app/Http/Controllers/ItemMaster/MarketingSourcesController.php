<?php

namespace App\Http\Controllers\ItemMaster;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Gate;
use App\Models\MarketingSource;
use App\Models\Outlet;
use App\Models\MarketingOutlet;
use App\Models\UserOutlet;
use Auth;

class MarketingSourcesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();

        return view('item-master.marketingSources.index', compact('outlet'));
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function listData()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlets = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $collection = MarketingSource::where('id_marketing_source', 'like', '%')
                    ->whereHas('marketing_outlets', function ($q) use ($outlet_id) {
                        return $q->whereIn('outlet_id', $outlet_id);
                    })
                    ->orderBy('created_at', 'desc')->get();
                    
        $data = array();
        foreach ($collection as $marketingSources) {
            $row = [];
            $extra_field = '';
            $status_marketing_source = '';

            if ($marketingSources->extra_field) {
                $extra_field = '&#10004;';
            }
            if ($marketingSources->status_marketing_source) {
                $status_marketing_source = '&#10004;';
            }
            
            $row[] = $marketingSources->marketing_source_name;
            $row[] = $status_marketing_source;
            $row[] = '
                        <a class="btn btn-success btn-sm" href="#" onclick="editForm(\''.$marketingSources->id_marketing_source. '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a> 
                        <a class="btn btn-danger btn-sm" href="#" onclick="deleteData(\''.$marketingSources->id_marketing_source. '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>
                    ';
            
            $data[] = $row;
        }


        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $save = new MarketingSource;
        $save->marketing_source_name = $request->marketing_source_name;
        $save->status_marketing_source = 1;
   
        $save->save();

        foreach ($request->outlet as $key => $value) {
            $marketing_outlets = new MarketingOutlet;
            $marketing_outlets->outlet_id = $value;
            $marketing_outlets->marketing_id = $save->id_marketing_source;
            $marketing_outlets->save();
        }
        // return response()->json($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = MarketingSource::find($id);
        $model->outlet = MarketingOutlet::where('marketing_id', $id)->pluck('outlet_id');

        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $model = MarketingSource::find($id);


        if ($request->disable == true) {
            $model->status_marketing_source = 0;
            $model->update();
            return response()->json(true);
        }
        $model->fill($request->all());
        if (empty($request->status_marketing_source)) {
            $model->status_marketing_source = 0;
        } else {
            $model->status_marketing_source = $request->status_marketing_source;
        }

        $model->update();


        $before = MarketingOutlet::where('marketing_id', $id)->pluck('outlet_id')->toArray();
        $shouldDelete = array_diff($before, $request->outlet);
        if (count($shouldDelete)) {
            MarketingOutlet::where('marketing_id', $id)
                            ->whereIn('outlet_id', $shouldDelete)
                            ->delete();
        }
        foreach ($request->outlet as $outlet_id) {
            MarketingOutlet::firstOrCreate([
                'marketing_id' => $id,
                'outlet_id'  => $outlet_id
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $model = MarketingSource::find($id);
        if ($model->invoice || $model->id_marketing_source == 16) {
            echo "error";
        } else {
            $model->delete();
            $before = MarketingOutlet::where('marketing_id', $id)->pluck('outlet_id')->toArray();
            MarketingOutlet::where('marketing_id', $id)
              ->whereIn('outlet_id', $before)
              ->delete();
        }
    }
}

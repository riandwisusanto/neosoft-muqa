<?php

namespace App\Http\Controllers\ItemMaster;

use App\Http\Controllers\Controller;

use App\Models\ProductPoint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use DataTables;

class ProductPointController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin') || Gate::allows('sales')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('item-master.productPoints.index');
    }

    public function listData()
    {
        $productPoints = ProductPoint::with('invoice_point_detail')->get();
        $data = array();
        foreach ($productPoints as $list) {
            $delete = '';

            if(!$list->invoice_point_detail) {
                $delete = '<a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="deleteData(\'' . $list->id_product_point . '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>';
            }

            $row = array();
            $row[] = $list->product_point_name;
            $row[] = $list->point;
            $row[] = $list->description;
            $row[] = '<img src=\''.'storage/' . $list->img . '\' alt = \'no image\' class=\'img-responsive\' style=\'width:100px;\'>';
            $row[] = $list->status_product_point?'&#10004;':'';
            $row[] = '
                        <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="editForm(\'' . $list->id_product_point . '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                        '.$delete.'
                    ';
            $data[] = $row;
        }
        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = new ProductPoint;
        $product->product_point_name = $request->name;
        $product->point = $request->point;
        $product->description = $request->description;
        $img = $request->file('img');
        if ($img) {
            $img_path = $img->store('product', 'public');
            $product->img = $img_path;
        }

        $product->status_product_point = 1;

        $product->save();
    }

    public function chooseProduct($id)
    {
        $product = ProductPoint::find($id);
        return response()->json($product);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductPoint  $productPoint
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductPoint  $productPoint
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $productPoint = ProductPoint::findOrFail($id);
        return response()->json($productPoint);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductPoint  $productPoint
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $productPoint = ProductPoint::findOrFail($id);
        if ($request->disable == true) {
            $productPoint->status_product_point = 0;
            $productPoint->update();
            return response()->json(true);
        }
        $productPoint->product_point_name = $request->name;
        $productPoint->point = $request->point;
        if (empty($request->status_product_point)) {
            $productPoint->status_product_point = 0;
        } else {
            $productPoint->status_product_point = $request->status_product_point;
        }

        $img = $request->file('img');
        if ($img) {
            if ($productPoint->img && file_exists(storage_path('app/public/' . $productPoint->img))) {
                \Storage::delete('public/' . $productPoint->img);
            }
            $img_path = $img->store('product', 'public');
            $productPoint->img = $img_path;
        }



        $productPoint->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductPoint  $productPoint
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProductPoint::findOrFail($id)->delete();
    }
}

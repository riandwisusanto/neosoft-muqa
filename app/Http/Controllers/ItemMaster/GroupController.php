<?php

namespace App\Http\Controllers\ItemMaster;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use Yajra\DataTables\DataTables;
use App\Models\Group;
use App\Models\Therapist;
use Auth;

class GroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin-bm-spv-fo-therapist')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('item-master.groups.index');
    }

    public function listData()
    {
        $collection = Group::all();
        $data = array();
        $allowEdit = array_intersect(['ADMINISTRATOR', 'SUPER_USER', 'BRANCH_MANAGER', 'OUTLET_SUPERVISOR'], json_decode(Auth::user()->level));
        foreach ($collection as $group) {
            $row = array();
            $status_group = '';

            if ($group->status == 1) {
                $status_group = '&#10004;';
            }

            $row[] = '';
            $row[] = $group->group_name;
            $row[] = $group->name;
            $row[] = $status_group;

            if ($allowEdit) {
                $row[] = '
                        <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="editForm(\''.$group->group_id. '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                        <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="deleteData(\''.$group->group_id. '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>
                    ';
            } else {
                $row[] = '-';
            }   
            

            $data[] = $row;
        }
//        dd($data);

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Group();
        $model->group_name = $request->group_name;
        $model->name = $request->name;
        $model->status = 1;
        $model->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        //
        return response()->json($group);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group)
    {
        if ($request->disable == true) {
            $model = Group::whereGroupName($group->group_name)->first();
            $model->status = 0;
            $model->update();
            return response()->json(true);
        }
        $group->group_name = $request->group_name;
        $group->name = $request->name;
        if (empty($request->status)) {
            $group->status = 0;
        } else {
            $group->status = $request->status;
        }
        $group->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Group::findOrFail($id);
        
        if ($delete->therapist) {
            echo "error";
        } else {
            $delete->delete();
        }
    }
}

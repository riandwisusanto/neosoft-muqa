<?php

namespace App\Http\Controllers\ItemMaster;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use DataTables;
use App\Models\Machine;

class MachineController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('item-master.machines.index');
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function listData()
    {
        $collection = Machine::all();

        $data =array();
        foreach ($collection as $machine) {
            $row = [];
            $status_machine = '';

            if ($machine->status_machine) {
                $status_machine = '&#10004;';
            }

            $row[] = $machine->machine_name;
            $row[] = '<div class="btn-group-vertical">
                                    <a class="btn btn-success btn-sm" href="#" onclick="editForm(\''.$machine->id_machine. '\')">
                                        <i class="fa fa-edit"></i>
                                        Edit
                                    </a> 
                                    <a class="btn btn-danger btn-sm" href="#" onclick="deleteData(\''.$machine->id_machine. '\')">
                                        <i class="fa fa-trash"></i>
                                        Delete
                                    </a>
                                </div>';

            $data[] = $row;
        }


        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $model = new Machine($request->all());
        $model->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Machine  $machine
     * @return \Illuminate\Http\Response
     */
    public function show(Machine $machine)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Machine  $machine
     * @return \Illuminate\Http\Response
     */
    public function edit(Machine $machine)
    {
        //
        return response()->json($machine);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Machine  $machine
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Machine $machine)
    {
        //
        $machine->fill($request->all());
        $machine->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Machine  $machine
     * @return \Illuminate\Http\Response
     */
    public function destroy(Machine $machine)
    {
        //
        $machine->delete();
    }
}

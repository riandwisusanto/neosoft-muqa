<?php

namespace App\Http\Controllers\ItemMaster;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\BankOutlet;

use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Models\Outlet;
use App\Models\UserOutlet;
use Auth;

class BankController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();

        return view('item-master.banks.index', compact('outlet'));
    }

    public function listData()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlets = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $collection = Bank::where('id_bank', 'like', '%')
                    ->whereHas('bank_outlets', function ($q) use ($outlet_id) {
                        return $q->whereIn('outlet_id', $outlet_id);
                    })
                    ->orderBy('created_at', 'desc')->get();

        $data = array();
        foreach ($collection as $bank) {
            $row = [];
            $status_bank = '';

            if ($bank->status_bank) {
                $status_bank = '&#10004;';
            }

            $row[] = $bank->bank_code;
            $row[] = $bank->bank_name;
            $row[] = $status_bank;
            
            $row[] = '
                        <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="editForm(\'' . $bank->id_bank . '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                        <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="deleteData(\'' . $bank->id_bank . '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>
                    ';

            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $save = new Bank;
        $save->bank_name = $request->bank_name;
        $save->bank_code = $request->bank_code;
        $save->bank_charge = $request->bank_charge;
        $save->bank_type = $request->bank_type;
        $save->status_bank = 1;
        $save->save();

        foreach ($request->outlet as $key => $value) {
            $bank_outlets = new BankOutlet;
            $bank_outlets->outlet_id = $value;
            $bank_outlets->bank_id = $save->id_bank;
            $bank_outlets->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = Bank::find($id);
        $model->outlet = BankOutlet::where('bank_id', $id)->pluck('outlet_id');

        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $model = Bank::find($id);

        if ($request->disable == true) {
            $model->status_bank = 0;
            $model->update();
            return response()->json(true);
        }
        $model->bank_name = $request->bank_name;
        $model->bank_code = $request->bank_code;
        $model->bank_charge = $request->bank_charge;
        $model->bank_type = $request->bank_type;
        if (empty($request->status_bank)) {
            $model->status_bank = 0;
        } else {
            $model->status_bank = $request->status_bank;
        }
        $model->update();

        $before = BankOutlet::where('bank_id', $id)->pluck('outlet_id')->toArray();
        $shouldDelete = array_diff($before, $request->outlet);
        if (count($shouldDelete)) {
            BankOutlet::where('bank_id', $id)
                            ->whereIn('outlet_id', $shouldDelete)
                            ->delete();
        }
        foreach ($request->outlet as $outlet_id) {
            BankOutlet::firstOrCreate([
                'bank_id' => $id,
                'outlet_id'  => $outlet_id
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $model = Bank::find($id);
        // dd($model);
        if ($model->payment) {
            echo "error";
        } else {
            $model->delete();
            $before = BankOutlet::where('bank_id', $id)->pluck('outlet_id')->toArray();
            BankOutlet::where('bank_id', $id)
              ->whereIn('outlet_id', $before)
              ->delete();
        }
    }
}

<?php

namespace App\Http\Controllers\ItemMaster;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\PointValue;

use DataTables;
use Illuminate\Support\Facades\Gate;
use Auth;

class PointValueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });
    }

    public function index()
    {
        return view('item-master.pointValues.index');
    }

    public function listData()
    {
        $point = PointValue::all();
        $data = array();
        foreach ($point as $list) {
            $row = array();
            $row[] = $list->point;
            $row[] = "<div class='btn-group'>
                      <a onclick='editForm(".$list->id_point_value.")' title='Edit Data' class='btn btn-success btn-sm'><i class='fa fa-edit'></i></a>
                      <a onclick='deleteData(".$list->id_point_value.")' title='Delete Data' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></a>";
            $data[] = $row;
        }
        return DataTables::of($data)->escapeColumns([])->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new PointValue;
        $model->point = $request->point;
        $model->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = PointValue::find($id);

        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = PointValue::find($id);

        $model->point    = $request->point;
        $model->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (PointValue::count() > 1) {
            PointValue::find($id)->delete();
        }
    }
}

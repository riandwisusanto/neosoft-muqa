<?php

namespace App\Http\Controllers;

use App\Models\Logistics\Mutation;
use App\Models\Outlet;
use App\Models\Therapist;
use App\Models\Usage;
use App\Models\UsageDetail;
use Carbon\carbon;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use App\Models\UserOutlet;

use Auth;

class ReportDoneController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin-bm-spv-fo-therapist')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });
    }
    //collection by Outlet
    public function index_treatmentdone_outlet()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();

        $from = Carbon::parse(now())->format('d/m/Y');
        $to = Carbon::parse(now())->format('d/m/Y');
        $summary = [];
        $detail_usage = [];

        return view('report.done.outlet.treatment.index')
            ->with('summary', $summary)
            ->with('from', $from)
            ->with('to', $to)
            ->with('outlet', $outlet)
            ->with('detail_usage', $detail_usage);
    }
    public function treatmentdone_outlet_material(Request $request)
    {
        $mutation = [];
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        if ($request->outlet) {
            $outlet_id = [];
            for ($i = 0; $i < count($request->outlet); $i++) {
                $outlet_id[] = $request->outlet[$i];
            }

            $usage_id = Usage::whereIn('outlet_id', $outlet_id)
                ->whereBetween('usage_date', [$from, $to])
                ->where('status_usage', 0)
                ->pluck('id_usage')
                ->toArray();
            $usage_detail_id = UsageDetail::whereIn('usage_id', $usage_id)->pluck('product_id')->toArray();

            $mutation =  Mutation::whereIn('sales_invoice_id', $usage_id)
                ->whereIn('item_id', $usage_detail_id)
                ->get()->groupBy('item_id');
        }
        $data = array();
        $no = 1;
        foreach ($mutation as $index => $list) {
            $row = array();
            $row[] = $no++;
            $qty = 0;
            foreach ($list as $b) {
                $qty += $b->qty * -1;
            }
            $row[] = $list[0]->item->name;
            $row[] = $qty;
            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }
    public function treatmentdone_outlet_summary(Request $request)
    {
        $summary = [];
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        if ($request->outlet) {
            $outlet_id = [];
            for ($i = 0; $i < count($request->outlet); $i++) {
                $outlet_id[] = $request->outlet[$i];
            }
            $summary = Usage::whereBetween('usage_date', [$from, $to])
                ->where('status_usage', 0)
                ->whereIn('outlet_id', $outlet_id)
                ->get()
                ->groupBy('outlet_id');
        }
        $data = array();
        foreach ($summary as $list) {
            $total_usage = 0;
            $total = 0;
            $count_data = 0;
            foreach ($list as $item) {
                $row = array();
                foreach ($item->usage_detail as $usage) {
                    foreach ($item->usage_detail as $usage) {
                        if (substr($usage->product_treatment->product_treatment_code, 0, 1) == 'T') {
                            $total_usage = $total_usage + $usage->used;
                            $total += $usage->value * $usage->used;
                            $count_data++;
                        }
                    }
                }
                $row[] = $list[0]->outlet->outlet_name;
                $row[] = $count_data;
                $row[] = $total_usage;
                $row[] = format_money($total);
                $data[] = $row;
            }
        }
        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function treatmentdone_outlet_detail(Request $request)
    {
        $detail_usage = [];
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        if ($request->outlet) {
            $outlet_id = [];
            for ($i = 0; $i < count($request->outlet); $i++) {
                $outlet_id[] = $request->outlet[$i];
            }
            $usage = Usage::whereBetween('usage_date', [$from, $to])
                ->where('status_usage', 0)
                ->whereIn('outlet_id', $outlet_id)
                ->pluck('id_usage');
            $detail_usage = UsageDetail::whereIn('usage_id', $usage)->get();
        }

        $data = array();
        foreach ($detail_usage as $detail) {
            if (substr($detail->product_treatment->product_treatment_code, 0, 1) == 'T') {
                $mutationDetail = Mutation::where('sales_invoice_id', $detail->usage_id)->where('item_id', $detail->product_id)->get();
                if (count($mutationDetail) > 0) {
                    foreach ($mutationDetail as $mutIndex => $mut) {
                        $row = array();

                        $nurseDoctor = '';
                        $nurseOnloop = '';

                        $commDrSpKK = 0;
                        $commDrGp = 0;
                        $commDrSpBP = 0;
                        $commDrAnas = 0;
                        $commNurse = 0;
                        $totalValue = $detail->value * $detail->used;

                        foreach ($detail->usage_therapist as $gettherapist) {
                            if ($gettherapist->is_on_loop == 1) {
                                @$nurseOnloop .= $gettherapist->therapist->therapist_name . ".<br>";
                            } else {
                                @$nurseDoctor .= $gettherapist->therapist->therapist_name . ".<br>";
                            }

                            if ($gettherapist->therapist->type == 0) {
                                if ($detail->product_treatment->commTypeDrSpKK == 0) {
                                    $commDrSpKK += ($detail->product_treatment->commissionDrSpKK / 100) * $totalValue;
                                } else {
                                    $commDrSpKK += $detail->product_treatment->commissionDrSpKK * $detail->used;
                                }
                            } else if ($gettherapist->therapist->type == 1) {
                                if ($detail->product_treatment->commTypeDrSpBp == 0) {
                                    $commDrSpBP += ($detail->product_treatment->commissionDrSpBP / 100) * $totalValue;
                                } else {
                                    $commDrSpBP += $detail->product_treatment->commissionDrSpBP * $detail->used;
                                }
                            } else if ($gettherapist->therapist->type == 2) {
                                if ($detail->product_treatment->commTypeDrGp == 0) {
                                    $commDrGp += ($detail->product_treatment->commissionDrGP / 100) * $totalValue;
                                } else {
                                    $commDrGp += $detail->product_treatment->commissionDrGP * $detail->used;
                                }
                            } else if ($gettherapist->therapist->type == 3) {
                                if ($detail->product_treatment->commTypeDrAnas == 0) {
                                    $commDrAnas += ($detail->product_treatment->commissionDrAnas / 100) * $totalValue;
                                } else {
                                    $commDrAnas += $detail->product_treatment->commissionDrAnas * $detail->used;
                                }
                            } else if ($gettherapist->therapist->type == 4) {
                                if ($gettherapist->is_on_loop == 1) {
                                    if ($detail->product_treatment->commTypeNurseOnLoop == 0) {
                                        $commNurse += ($detail->product_treatment->commissionNurseOnLoop / 100) * $totalValue;
                                    } else {
                                        $commNurse += $detail->product_treatment->commissionNurseOnLoop * $detail->used;
                                    }
                                } else {
                                    if ($detail->product_treatment->commTypeNurse == 0) {
                                        $commNurse += ($detail->product_treatment->commissionNurse / 100) * $totalValue;
                                    } else {
                                        $commNurse += $detail->product_treatment->commissionNurse * $detail->used;
                                    }
                                }
                            }
                        }

                        if ($mutIndex == 0) {
                            $row[] = '<a href=' . route('usage-treatment.print', $detail->usage_id) . ' target="_blank">' . $detail->usage->usage_code . '</a>';
                            $row[] = $detail->usage->usage_date;
                            $row[] = $detail->invoice ? $detail->invoice['inv_date'] : '-';
                            $row[] = $detail->usage->outlet['outlet_name'];
                            $row[] = '<a href=' . route('customers.show', $detail->usage->customer_id) . ' target="_blank">' . $detail->usage->customer['induk_customer'] . '</a>';
                            $row[] = $detail->usage->customer['full_name'];
                            $row[] = $detail->usage->user['username'];
                            $row[] = $detail->product_treatment->product_treatment_code;
                            $row[] = $detail->product_treatment->product_treatment_name;
                            if (substr($detail->product_treatment->product_treatment_code, 0, 1) == 'T') {
                                $row[] = 'Treatment';
                            } else {
                                $row[] = 'Product';
                            }
                            $row[] = format_money($detail->value * $detail->used);
                            $row[] = $detail->used;
                            $row[] = format_money($detail->value * $detail->qty);
                            $row[] = $detail->qty;
                            $row[] = $detail->duration;
                            $row[] = $detail->point;
                            $row[] = format_money($commDrSpKK);
                            $row[] = format_money($commDrGp);
                            $row[] = format_money($commDrSpBP);
                            $row[] = format_money($commDrAnas);
                            $row[] = format_money($commNurse);

                            $row[] = $detail->usage->remarks;
                        } else {
                            if ($mutationDetail[$mutIndex]->raw_item_id == $mutationDetail[$mutIndex - 1]->raw_item_id) {
                                $row[] = "";
                                $row[] = "";
                                $row[] = "";
                                $row[] = "";
                                $row[] = "";
                                $row[] = "";
                                $row[] = "";
                                $row[] = "";
                                $row[] = "";
                                $row[] = "";
                                $row[] = "";
                                $row[] = "";
                                $row[] = "";
                                $row[] = "";
                                $row[] = "";
                                $row[] = "";
                                $row[] = "";
                                $row[] = "";
                                $row[] = "";
                                $row[] = "";
                                $row[] = "";
                                $row[] = "";
                            } else {
                                $row[] = '<a href=' . route('usage-treatment.print', $detail->usage_id) . ' target="_blank">' . $detail->usage->usage_code . '</a>';
                                $row[] = $detail->usage->usage_date;
                                $row[] = $detail->invoice ? $detail->invoice['inv_date'] : '-';
                                $row[] = $detail->usage->outlet['outlet_name'];
                                $row[] = '<a href=' . route('customers.show', $detail->usage->customer_id) . ' target="_blank">' . $detail->usage->customer['induk_customer'] . '</a>';
                                $row[] = $detail->usage->customer['full_name'];
                                $row[] = $detail->usage->user['username'];
                                $row[] = $detail->product_treatment->product_treatment_code;
                                $row[] = $detail->product_treatment->product_treatment_name;
                                if (substr($detail->product_treatment->product_treatment_code, 0, 1) == 'T') {
                                    $row[] = 'Treatment';
                                } else {
                                    $row[] = 'Product';
                                }
                                $row[] = format_money($detail->value * $detail->used);
                                $row[] = $detail->used;
                                $row[] = format_money($detail->value * $detail->qty);
                                $row[] = $detail->qty;
                                $row[] = $detail->duration;
                                $row[] = $detail->point;
                                $row[] = format_money($commDrSpKK);
                                $row[] = format_money($commDrGp);
                                $row[] = format_money($commDrSpBP);
                                $row[] = format_money($commDrAnas);
                                $row[] = format_money($commNurse);

                                $row[] = $detail->usage->remarks;
                            }
                        }

                        $row[] = $mut->item->name;
                        $qty = $mut->qty * -1;
                        $row[] = $qty;
                        $row[] = @$mut->item->uom['alias'];
                        $row[] = format_money($mut->item->cogs);
                        $row[] = format_money($mut->item->cogs * $qty);

                        if ($mutIndex == 0) {
                            $row[] = @$nurseDoctor;
                            $row[] = @$nurseOnloop;
                            $row[] = '<a href=' . route('usage-treatment.show', $detail->usage_id) . ' class="btn btn-info btn-xs" target="_blank"><i class="fa fa-eye"></i></a>';
                        } else {
                            if ($mutationDetail[$mutIndex]->raw_item_id == $mutationDetail[$mutIndex - 1]->raw_item_id) {
                                $row[] = "";
                                $row[] = "";
                                $row[] = "";
                            } else {
                                $row[] = @$nurseDoctor;
                                $row[] = @$nurseOnloop;
                                $row[] = '<a href=' . route('usage-treatment.show', $detail->usage_id) . ' class="btn btn-info btn-xs" target="_blank"><i class="fa fa-eye"></i></a>';
                            }
                        }
                        $data[] = $row;
                    }
                } else {
                    $nurseDoctor = '';
                    $nurseOnloop = '';

                    $commDrSpKK = 0;
                    $commDrGp = 0;
                    $commDrSpBP = 0;
                    $commDrAnas = 0;
                    $commNurse = 0;
                    $totalValue = $detail->value * $detail->used;

                    foreach ($detail->usage_therapist as $gettherapist) {
                        if ($gettherapist->is_on_loop == 1) {
                            @$nurseOnloop .= $gettherapist->therapist->therapist_name . ".<br>";
                        } else {
                            @$nurseDoctor .= $gettherapist->therapist->therapist_name . ".<br>";
                        }

                        if ($gettherapist->therapist->type == 0) {
                            if ($detail->product_treatment->commTypeDrSpKK == 0) {
                                $commDrSpKK += ($detail->product_treatment->commissionDrSpKK / 100) * $totalValue;
                            } else {
                                $commDrSpKK += $detail->product_treatment->commissionDrSpKK * $detail->used;
                            }
                        } else if ($gettherapist->therapist->type == 1) {
                            if ($detail->product_treatment->commTypeDrSpBp == 0) {
                                $commDrSpBP += ($detail->product_treatment->commissionDrSpBP / 100) * $totalValue;
                            } else {
                                $commDrSpBP += $detail->product_treatment->commissionDrSpBP * $detail->used;
                            }
                        } else if ($gettherapist->therapist->type == 2) {
                            if ($detail->product_treatment->commTypeDrGp == 0) {
                                $commDrGp += ($detail->product_treatment->commissionDrGP / 100) * $totalValue;
                            } else {
                                $commDrGp += $detail->product_treatment->commissionDrGP * $detail->used;
                            }
                        } else if ($gettherapist->therapist->type == 3) {
                            if ($detail->product_treatment->commTypeDrAnas == 0) {
                                $commDrAnas += ($detail->product_treatment->commissionDrAnas / 100) * $totalValue;
                            } else {
                                $commDrAnas += $detail->product_treatment->commissionDrAnas * $detail->used;
                            }
                        } else if ($gettherapist->therapist->type == 4) {
                            if ($gettherapist->is_on_loop == 1) {
                                if ($detail->product_treatment->commTypeNurseOnLoop == 0) {
                                    $commNurse += ($detail->product_treatment->commissionNurseOnLoop / 100) * $totalValue;
                                } else {
                                    $commNurse += $detail->product_treatment->commissionNurseOnLoop * $detail->used;
                                }
                            } else {
                                if ($detail->product_treatment->commTypeNurse == 0) {
                                    $commNurse += ($detail->product_treatment->commissionNurse / 100) * $totalValue;
                                } else {
                                    $commNurse += $detail->product_treatment->commissionNurse * $detail->used;
                                }
                            }
                        }
                    }

                    //noMaterial
                    $row = array();
                    $row[] = '<a href=' . route('usage-treatment.print', $detail->usage_id) . ' target="_blank">' . $detail->usage->usage_code . '</a>';
                    $row[] = $detail->usage->usage_date;
                    $row[] = $detail->invoice ? $detail->invoice['inv_date'] : '-';
                    $row[] = $detail->usage->outlet['outlet_name'];
                    $row[] = '<a href=' . route('customers.show', $detail->usage->customer_id) . ' target="_blank">' . $detail->usage->customer['induk_customer'] . '</a>';
                    $row[] = $detail->usage->customer['full_name'];
                    $row[] = $detail->usage->user['username'];
                    $row[] = $detail->product_treatment->product_treatment_code;
                    $row[] = $detail->product_treatment->product_treatment_name;
                    if (substr($detail->product_treatment->product_treatment_code, 0, 1) == 'T') {
                        $row[] = 'Treatment';
                    } else {
                        $row[] = 'Product';
                    }
                    $row[] = format_money($detail->value * $detail->used);
                    $row[] = $detail->used;
                    $row[] = format_money($detail->value * $detail->qty);
                    $row[] = $detail->qty;
                    $row[] = $detail->duration;
                    $row[] = $detail->point;

                    $row[] = format_money($commDrSpKK);
                    $row[] = format_money($commDrGp);
                    $row[] = format_money($commDrSpBP);
                    $row[] = format_money($commDrAnas);
                    $row[] = format_money($commNurse);

                    $row[] = $detail->usage->remarks;
                    $row[] = '';
                    $row[] = '';
                    $row[] = '';
                    $row[] = '';
                    $row[] = '';
                    $row[] = @$nurseDoctor;
                    $row[] = @$nurseOnloop;
                    $row[] = '<a href=' . route('usage-treatment.show', $detail->usage_id) . ' class="btn btn-info btn-xs" target="_blank"><i class="fa fa-eye"></i></a>';
                    $data[] = $row;
                }
            }
        }
        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function index_productdone_outlet()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();

        $from = Carbon::parse(now())->format('d/m/Y');
        $to = Carbon::parse(now())->format('d/m/Y');
        $summary = [];
        $detail_usage = [];

        return view('report.done.outlet.product.index')
            ->with('summary', $summary)
            ->with('from', $from)
            ->with('to', $to)
            ->with('outlet', $outlet)
            ->with('detail_usage', $detail_usage);
    }

    public function productdone_outlet_detail(Request $request)
    {
        $detail_usage = [];
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        $outlet_id = [0];

        if ($request->outlet) {
            $outlet_id = [];
            for ($i = 0; $i < count($request->outlet); $i++) {
                $outlet_id[] = $request->outlet[$i];
            }
        }

        $sql = file_get_contents(__DIR__ . '/Queries/productDoneByOutlet.sql.tpl');
        $sql = sprintf($sql, implode(',', $outlet_id));
        $rows = DB::select($sql, [$from, $to]);
        $data = array();

        foreach ($rows as $list) {
            $row = [];
            $ageInYear = date('Y') - Carbon::createFromFormat('d/m/Y', $list->birth_date)->format('Y');
            $row[] = '<a href=' . route('usage-treatment.print', $list->id_usage) . ' target="_blank">' . $list->usage_code . '</a>';
            $row[] = $list->usage_date;
            $row[] = $list->usage_time;
            $row[] = '<a href=' . route('invoice.print', $list->id_invoice) . ' target="_blank">' . $list->inv_code . '</a>';
            $row[] = $list->inv_date;

            $row[] = $list->outlet_name;
            $row[] = '<a href=' . route('customers.show', $list->customer_id) . ' target="_blank">' . $list->induk_customer . '</a>';
            $row[] = $list->full_name;
            $row[] = $ageInYear;
            $row[] = $list->gender;
            if (array_intersect(["OUTLET_SUPERVISOR"], json_decode(Auth::user()->level))) {
                $row[] = $list->phone;
                $row[] = $list->address . ', ' . $list->province;
            }
            $row[] = $list->username;
            $row[] = $list->product_treatment_name;
            $row[] = $list->product_treatment_code;
            $row[] = 'Product';

            $row[] = format_money($list->used_value);
            $row[] = $list->used_qty;
            $row[] = format_money($list->balance_value);
            $row[] = $list->balance_qty;
            $row[] = $list->duration;
            $row[] = $list->point;
            $row[] = 'NONE';
            $row[] = $list->remarks;
            $row[] = '<a href=' . route('usage-treatment.show', $list->id_usage) . ' class="btn btn-info btn-xs" target="_blank"><i class="fa fa-eye"></i></a>';

            $data[] = $row;
        }
        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function index_treatmentdone_therapist()
    {
        $therapist_data = Therapist::all();
        $from = Carbon::parse(now())->format('d/m/Y');
        $to = Carbon::parse(now())->format('d/m/Y');
        $summary = [];
        $detail_usage = [];

        return view('report.done.therapist.treatment.index')
            ->with('summary', $summary)
            ->with('from', $from)
            ->with('to', $to)
            ->with('therapist_data', $therapist_data)
            ->with('detail_usage', $detail_usage);
    }

    public function summaryCustomerSatisfaction(Request $request)
    {
        $ratingTherapist = [];
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        if ($request->therapist) {
            $therapist = [];
            for ($i = 0; $i < count($request->therapist); $i++) {
                $therapist[] = $request->therapist[$i];
            }

            $fileRating = file_get_contents(__DIR__ . '/Queries/therapist-rating.sql.tpl');
            $queryRating = sprintf($fileRating, $from, $to, implode(',', $therapist));
            $ratingTherapist = DB::select($queryRating);
        }

        $data = array();
        foreach ($ratingTherapist as $list) {
            $row = array();
            $rating = "-";
            if ($list->average_rating > 0 and $list->average_rating < 2) {
                $rating = "Tidak Puas";
            } elseif ($list->average_rating >= 2 and $list->average_rating < 3) {
                $rating = "Kurang Puas";
            } elseif ($list->average_rating >= 3 and $list->average_rating < 4) {
                $rating = "Biasa Saja";
            } elseif ($list->average_rating >= 4 and $list->average_rating < 5) {
                $rating = "Cukup Puas";
            } elseif ($list->average_rating >= 5) {
                $rating = "Puas";
            }
            $row[] = $list->therapist_name;
            $row[] = $list->average_rating == null ? "-" : floor($list->average_rating * 10) / 10;
            $row[] = $rating;
            $data[] = $row;
        }
        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function treatmentdone_therapist(Request $request)
    {
        $summary = [];
        $detail_usage = [];
        $therapist_data = Therapist::all();
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        if ($request->therapist) {
            $therapist = [];
            for ($i = 0; $i < count($request->therapist); $i++) {
                $therapist[] = $request->therapist[$i];
            }

            $detail_usage = Usage::whereBetween('usage_date', [$from, $to])
                ->where('status_usage', 0)
                ->with(['usage_detail.usage_therapist' => function ($query) use ($therapist) {
                    $query->whereIn('therapist_id', $therapist);
                }])
                ->get();
        }
        $data = array();
        foreach ($detail_usage as $list) {
            foreach ($list->usage_detail as $detail) {
                foreach ($detail->usage_therapist as $gettherapist) {
                    $row = array();
                    $row[] = '<a href=' . route('usage-treatment.print', $list->id_usage) . ' target="_blank">' . $list->usage_code . '</a>';
                    $row[] = $list->usage_date;
                    $row[] = '<a href=' . route('invoice.print', $detail->invoice->id_invoice) . ' target="_blank">' . $detail->inv_code . '</a>';
                    $row[] = $detail->invoice->inv_date;
                    $row[] = '<a href=' . route('customers.show', $list->customer_id) . ' target="_blank">' . $list->customer->induk_customer . '</a>';
                    $row[] = $list->customer->full_name;
                    $row[] = $list->user->username;
                    $row[] = $detail->product_treatment->product_treatment_code;
                    $row[] = $detail->product_treatment->product_treatment_name;

                    foreach ($detail->invoice_package->invoice_detail as $inv) {
                        if ($inv->package) {
                            $row[] = 'Package';
                        } else {
                            if (substr($detail->product_treatment->product_treatment_code, 0, 1) == 'T') {
                                $row[] = 'Treatment';
                            } else {
                                $row[] = 'Product';
                            }
                        }
                    }
                    $row[] = format_money($detail->value * $detail->used);
                    $row[] = $detail->used;
                    $row[] = format_money($detail->value * $detail->qty);
                    $row[] = $detail->qty;
                    $row[] = $detail->duration;
                    $row[] = $detail->point;
                    $row[] = $gettherapist->therapist->therapist_name;
                    $rating = "";
                    if ($list->rating == 1) {
                        $rating = "Tidak Puas";
                    } elseif ($list->rating == 2) {
                        $rating = "Kurang Puas";
                    } elseif ($list->rating == 3) {
                        $rating = "Biasa Saja";
                    } elseif ($list->rating == 4) {
                        $rating = "Cukup Puas";
                    } elseif ($list->rating == 5) {
                        $rating = "Puas";
                    }
                    $row[] = $rating;
                    $row[] = $list->remarks;
                    $row[] = '<a href=' . route('usage-treatment.show', $list->id_usage) . ' class="btn btn-info btn-xs" target="_blank"><i class="fa fa-eye"></i></a>';
                    $data[] = $row;
                }
            }
        }
        return DataTables::of($data)->escapeColumns([])->make(true);
    }
}

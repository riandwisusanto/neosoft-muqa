<?php

namespace App\Http\Controllers;

use App\Models\Outlet;
use App\Models\UserOutlet;
use App\Models\Warehouse;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class ReportLogisticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $from = Carbon::parse(now())->format('d/m/Y');
        $to = Carbon::parse(now())->format('d/m/Y');
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $warehouse = Warehouse::whereHas('warehouseOutlets', function ($query) use ($outlet_id) {
            $query->whereIn('outlet_id', $outlet_id);
        })->get();

        return view('report.logistics.index', compact('from', 'to', 'warehouse'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        $data = [];
        $fileQuery = file_get_contents(__DIR__. '/Queries/stockLogistics.sql.tpl');
        $logistics = DB::select($fileQuery, array(
            'froma' => $from,
            'fromb' => $from,
            'fromc' => $from,
            'fromd' => $from,
            'frome' => $from,
            'toa' => $to,
            'tob' => $to,
            'toc' => $to,
            'tod' => $to,
            'toe' => $to,
            'wr' => $request->warehouse,
            'wra' => $request->warehouse
        ));

        foreach($logistics as $index => $logistic) {
            $row = [];
            $row[] = $index + 1;
            $row[] = $logistic->name;
            $row[] = $logistic->stock_begin;
            $row[] = $logistic->stock_in;
            $row[] = $logistic->stock_out;
            $row[] = $logistic->stock_adj_in;
            $row[] = $logistic->stock_adj_out;
            $row[] = $logistic->stock_result;

            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function print(Request $request)
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $from = '';
        $to = '';

        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        $fileQuery = file_get_contents(__DIR__. '/Queries/stockLogistics.sql.tpl');
        $logistics = DB::select($fileQuery, array(
            'froma' => $from,
            'fromb' => $from,
            'fromc' => $from,
            'fromd' => $from,
            'frome' => $from,
            'toa' => $to,
            'tob' => $to,
            'toc' => $to,
            'tod' => $to,
            'toe' => $to,
            'wr' => $request->warehouse,
            'wra' => $request->warehouse
        ));

        $pdf = PDF::loadView('report.logistics.print', [
            'from' => $from,
            'to' => $to,
            'logistics' => $logistics,
            'outlet' => $outlet->first()->outlet_name
        ]);

        return $pdf->stream();

    }

    public function generatePdf($from, $to, $data)
    {
        $pdf = PDF::loadView('report.logistics.print', [
           'data' => $data,
           'from' => $from,
           'to' => $to
        ]);

        return $pdf;
    }

}

<?php

namespace App\Http\Controllers;

use App\Models\ItemAlert;
use Illuminate\Http\Request;

class ItemAlertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $before = ItemAlert::where('id_item', $request->id)->pluck('product_treatment_code')->toArray();

        $save_json = json_decode($request->save_json);
        $getItemId = [];
        foreach ($save_json as $key => $data) {
            $getItemId[] = $data->product_treatment_code;
        }

        $shouldDelete = array_diff($before, $getItemId);

        if (count($shouldDelete)) {
            ItemAlert::where('id_item', $request->id)
                                ->whereIn('product_treatment_code', $shouldDelete)
                                ->delete();
        }

        foreach ($save_json as $key => $data) {
            ItemAlert::firstOrCreate([
                'id_item'     => $request->id,
                'product_treatment_code'         => $data->product_treatment_code
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ItemAlert::leftJoin('product_treatments', 'product_treatments.id_product_treatment', 'item_alerts.product_treatment_code')
            ->where('id_item', $id)->get();
        return json_encode($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ItemAlert::where('id_item', $id)->get();

        $item = array();
        foreach ($data as $list) {
            $item[] = array(
                'item'                      => $list->product->product_treatment_name,
                'product_treatment_code'    => $list->product->id_product_treatment,
            );
        }

        return response()->json([
            'item' => $item,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Logistik;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

use App\Models\VStockCardRecap;

use DataTables;
use Auth;
use View;

class StockCardRecapController extends Controller
{
    protected $view = 'logistik.stock-card-recap.';
    protected $route = 'stock-card-recap';
    protected $title = 'Stock Card Recap';

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });

        View::share('view', $this->view);
        View::share('route', $this->route);
        View::share('title', $this->title);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->view.'index');
    }

    public function listData()
    {
        $collection = VStockCardRecap::orderBy('transdate', 'desc')->get();
        $data = array();
        foreach ($collection as $r) {
            $row = [];
            $row[] = date('d M Y', strtotime($r->transdate));
            $row[] = $r->item_code;
            $row[] = $r->item_name;
            $row[] = $r->uom;
            $row[] = number_format($r->in);
            $row[] = number_format($r->cost_in);
            $row[] = number_format($r->value_in);
            $row[] = number_format($r->out);
            $row[] = number_format($r->cost_out);
            $row[] = number_format($r->value_out);
            $row[] = number_format($r->balance);

            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }
}

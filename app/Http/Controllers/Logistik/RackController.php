<?php

namespace App\Http\Controllers\Logistik;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

use App\Models\Warehouse;
use App\Models\Rack;

use DataTables;
use Auth, View;

class RackController extends Controller
{
    protected $title = 'Racks';

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin') || Gate::allows('pharmacy')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });

        View::share('title', $this->title);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $warehouses = Warehouse::all();
        return view('logistik.racks.index', compact('warehouses'));
    }

    public function listData()
    {
        $collection = Rack::orderBy('created_at', 'desc')->get();


        $data = array();
        foreach ($collection as $r) {
            $row = [];
            $row[] = $r->main_rack_code;
            $row[] = $r->sub_rack_code;
            $row[] = $r->description;
            $row[] = $r->warehouse ? $r->warehouse->name : '-';
            
            $row[] = $r->is_active ? '<span class="label label-primary">Active</span>' : '<span class="label label-default">Not Active</span>';

            $row[] = '<div class="btn-group-vertical">
                        <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="editForm(\'' . $r->id_rack . '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                        <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="deleteData(\'' . $r->id_rack . '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>
                    </div>';

            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $save = new Rack;
        $save->main_rack_code = $request->main_rack_code;
        $save->sub_rack_code  = $request->sub_rack_code;
        $save->description    = $request->description;
        $save->warehouse_id   = $request->warehouse_id;
        $save->is_active      = $request->filled('is_active') ? 1 : 0;
        $save->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = Rack::find($id);

        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $model = Rack::find($id);

        $model->main_rack_code = $request->main_rack_code;
        $model->sub_rack_code  = $request->sub_rack_code;
        $model->description    = $request->description;
        $model->warehouse_id   = $request->warehouse_id;
        $model->is_active      = $request->filled('is_active') ? 1 : 0;
        $model->update();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Rack::find($id);
        if ($model->receive_detail->count() > 0) {
            echo "error";
        } else {
            $model->delete();
        }

    }
}

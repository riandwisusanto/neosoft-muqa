<?php

namespace App\Http\Controllers\Logistik;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

use App\Models\Supplier;
use App\Models\Item;
use App\Models\Uom;
use App\Models\Bom;
use App\Models\BomDetail;

use DataTables;
use Auth, View, DB;

use Carbon\Carbon;

class BomController extends Controller
{
    protected $title = 'Bill Of Material';
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });

        View::share('title', $this->title);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::where('is_active', '1')->get();
        $suppliers = Supplier::all();
        $uom = Uom::where('is_active', '1')->get();
        $code = Bom::generateCode();
        return view('logistik.bom.index', compact('items', 'suppliers', 'uom', 'code'));
    }

    public function listData()
    {
        $collection = Bom::orderBy('created_at', 'desc')->get();

        $data = array();
        foreach ($collection as $r) {
            $row = [];
            $row[] = $r->code;
            $row[] = $r->item ? $r->item->code : '-';
            $row[] = $r->description;
            $row[] = $r->batch_no;
            
            $row[] = $r->is_active ? '<span class="label label-primary">Active</span>' : '<span class="label label-default">Not Active</span>';

            $row[] = '<div class="btn-group-vertical">
                        <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="editForm(\'' . $r->id . '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                        <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="deleteData(\'' . $r->id . '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>
                    </div>';

            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $save = new Bom;
            $save->item_id     = $request->item_id;
            $save->batch_no    = $request->batch_no;
            $save->amount      = $request->amount;
            $save->description = $request->description;
            $save->is_active   = $request->filled('is_active') ? 1 : 0;;
            $save->save();

            $items = [];
            foreach ($request->items as $r) {
                $dummy = new BomDetail;
                $dummy->item_id     = $r['item_id'];
                $dummy->expire_date = $r['expire_date'];
                $dummy->uom_id      = $r['uom_id'];
                $dummy->price       = $r['price'] ? $r['price'] : 0;
                $dummy->qty         = $r['qty'] ? $r['qty'] : 0;
                $dummy->total       = $r['total'] ? $r['total'] : 0;

                $items[] = $dummy;
            }
            $save->bom_details()->saveMany($items);
            
            DB::commit();

            return Bom::generateCode();
        }catch(\Exception $e){
            DB::rollback();

            return response()->json([
                'code' => 500,
                'error' => 'Can not save the data'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = Bom::with('bom_details')->find($id);

        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $model = Bom::find($id);

            $model->item_id     = $request->item_id;
            $model->batch_no    = $request->batch_no;
            $model->amount      = $request->amount;
            $model->description = $request->description;
            $model->is_active   = $request->filled('is_active') ? 1 : 0;

            $items = [];
            foreach ($request->items as $r) {
                if(isset($r['id'])){
                    $dummy = BomDetail::find($r['id']);
                    $dummy->item_id     = $r['item_id'];
                    $dummy->expire_date = $r['expire_date'];
                    $dummy->uom_id      = $r['uom_id'];
                    $dummy->price       = $r['price'] ? $r['price']  : 0;
                    $dummy->qty         = $r['qty'] ? $r['qty']      : 0;
                    $dummy->total       = $r['total'] ? $r['total'] : 0;
                    $dummy->update();
                }else{
                    $dummy = new BomDetail;
                    $dummy->item_id     = $r['item_id'];
                    $dummy->expire_date = $r['expire_date'];
                    $dummy->uom_id      = $r['uom_id'];
                    $dummy->price       = $r['price'] ? $r['price']  : 0;
                    $dummy->qty         = $r['qty'] ? $r['qty']      : 0;
                    $dummy->total       = $r['total'] ? $r['total'] : 0;
        
                    $items[] = $dummy;
                }
            }
            $model->update();
            $model->bom_details()->saveMany($items);

            $delId = json_decode($request->delId);
            BomDetail::destroy($delId);

            DB::commit();
        }catch(\Exception $e){
            DB::rollback();

            return response()->json([
                'code' => 500,
                'error' => 'Can not save the data'
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Bom::find($id);
        // if(){

        // }else{
            $model->delete();
        // }
        return Bom::generateCode();
    }
}

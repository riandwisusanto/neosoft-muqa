<?php

namespace App\Http\Controllers\Logistik;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Models\Warehouse;
use App\Models\Outlet;
use App\Models\UserOutlet;
use App\Models\WarehouseOutlet;

use DataTables;
use Auth, View;

class WarehouseController extends Controller
{
    protected $title = 'Warehouse';

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin') || Gate::allows('pharmacy')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });

        View::share('title', $this->title);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();

        return view('logistik.warehouses.index', compact('outlet'));
    }

    public function listData()
    {
        $collection = Warehouse::orderBy('created_at', 'desc')->get();

        $data = array();
        foreach ($collection as $r) {
            $row = [];
            $row[] = $r->code;
            $row[] = $r->name;
            $row[] = '<p>'.$r->description.'</p>';
            $row[] = '<p>'.$r->addr.'</p>';

            $row[] = $r->is_active ? '<span class="label label-primary">Active</span>' : '<span class="label label-default">Not Active</span>';

            $row[] = '<div class="btn-group-vertical">
                        <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="editForm(\'' . $r->id_warehouse . '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                        <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="deleteData(\'' . $r->id_warehouse . '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>
                    </div>';

            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $save = new Warehouse;
        $save->code        = $request->code;
        $save->name        = $request->name;
        $save->description = $request->description;
        $save->addr        = $request->addr;
        $save->is_active   = $request->filled('is_active') ? 1 : 0;
        $save->save();

        foreach ($request->outlet as $key => $value) {
            $bank_outlets = new WarehouseOutlet;
            $bank_outlets->outlet_id = $value;
            $bank_outlets->warehouse_id = $save->id_warehouse;
            $bank_outlets->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Warehouse::find($id);
        $model->outlet = WarehouseOutlet::where('warehouse_id', $id)->pluck('outlet_id');

        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $model = Warehouse::find($id);

        $model->code        = $request->code;
        $model->name        = $request->name;
        $model->description = $request->description;
        $model->addr        = $request->addr;
        $model->is_active   = $request->filled('is_active') ? 1 : 0;
        $model->update();

        $before = WarehouseOutlet::where('warehouse_id', $id)->pluck('outlet_id')->toArray();
        $shouldDelete = array_diff($before, $request->outlet);
        if (count($shouldDelete)) {
            WarehouseOutlet::where('warehouse_id', $id)
                            ->whereIn('outlet_id', $shouldDelete)
                            ->delete();
        }

        foreach ($request->outlet as $outlet_id) {
            WarehouseOutlet::firstOrCreate([
                'warehouse_id' => $id,
                'outlet_id'  => $outlet_id
            ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Warehouse::find($id);
        // dd($model->receive_detail);
        if ($model->receive_detail->count() > 0) {
            echo "error";
        } else {
            $model->delete();
            $before = WarehouseOutlet::where('warehouse_id', $id)->pluck('outlet_id')->toArray();
            WarehouseOutlet::where('warehouse_id', $id)
                                ->whereIn('outlet_id', $before)
                                ->delete();
        }

    }
}

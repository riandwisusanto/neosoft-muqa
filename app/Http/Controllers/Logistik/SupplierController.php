<?php

namespace App\Http\Controllers\Logistik;

use App\Http\Controllers\Controller;
use App\Models\Outlet;
use App\Models\Province;
use App\Models\Country;
use App\Models\Supplier;

use App\Models\SupplierOutlet;
use App\Models\User;
use App\Models\UserOutlet;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Auth, View;

class SupplierController extends Controller
{
    protected $title = 'Supplier';

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin') || Gate::allows('pharmacy')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });

        View::share('title', $this->title);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_outlet = UserOutlet::where('user_id', auth()->user()->id)->pluck('outlet_id');
        $outlet = Outlet::whereIn('id_outlet', $user_outlet)->get();
        $province = Province::all();
        $code = Supplier::generateCode();
        return view('logistik.suppliers.index', compact('province', 'code', 'outlet'));
    }

    public function listData()
    {
        $collection = Supplier::orderBy('created_at', 'desc')->get();

        $data = array();
        foreach ($collection as $r) {
            $row = [];
            $row[] = $r->code;
            $row[] = $r->name;
            $row[] = $r->supplier_type;
            $row[] = $r->province->province;
            $row[] = $r->addr;
            $row[] = $r->post_code;
            $row[] = $r->is_active == 1 ? '<span class="label label-primary">Active</span>' : '<span class="label label-default">Not Active</span>';

            $row[] = '<div class="btn-group-vertical">
                        <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="editForm(\'' . $r->id_supplier . '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                        <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="deleteData(\'' . $r->id_supplier . '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>
                    </div>';

            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $save = new Supplier;
        $save->name = $request->name;
        $save->addr = $request->addr;
        $save->city_id = $request->city_id;
        $save->supplier_type = $request->supplier_type;
        $save->post_code = $request->post_code;
        $save->is_active = $request->filled('is_active') ? 1 : 0;
        $save->save();

        return Supplier::generateCode();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = Supplier::with('supplier_outlets')->find($id);
        $outlet = SupplierOutlet::where('supplier_id', $id)->pluck('outlet_id');

        return response()->json(['model' => $model, 'outlet' => $outlet]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $model = Supplier::find($id);

        $model->name = $request->name;
        $model->addr = $request->addr;
        $model->supplier_type = $request->supplier_type;
        $model->city_id = $request->city_id;
        $model->post_code = $request->post_code;
        $model->is_active = $request->filled('is_active') ? 1 : 0;
        $model->update();

        $model->supplier_outlets()->delete();

        if ($request->outlet) {
            for ($i = 0; $i < count($request->outlet); $i++) {
                SupplierOutlet::create([
                    'outlet_id' => $request->outlet[$i],
                    'supplier_id' => $model->id_supplier
                ]);
            }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Supplier::find($id);
        if ($model->purchase_orders->count() > 0 || $model->receives->count() > 0) {
            echo "error";
        } else {
            $model->delete();
            return Supplier::generateCode();
        }
    }
}

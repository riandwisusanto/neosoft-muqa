<?php

namespace App\Http\Controllers\Logistik;

use Illuminate\Http\Request;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

use App\Models\Supplier;
use App\Models\Item;
use App\Models\Uom;
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderDetail;
use App\Models\Logistics\Purchases\Order;
use App\Models\Logistics\Purchases\OrderDetail;

use DataTables;
use Auth, View, DB;

use Carbon\Carbon;

class PurchaseOrderController extends Controller
{
    protected $title = 'Purchase order';
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin') || Gate::allows('pharmacy')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });

        View::share('title', $this->title);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        $items = Item::where('is_active', '1')->get();
        $suppliers = Supplier::where('is_active', '1')->get();
        $uom = Uom::where('is_active', '1')->get();
        $status = $req->status;
        $code = PurchaseOrder::generateCode();

        return view('logistik.purchase-orders.index', compact('items', 'suppliers', 'uom', 'status', 'code'));
    }

    public function listData(Request $req)
    {
        $collection = PurchaseOrder::query();

        if($req->status) $collection = $collection->where('status', $req->status);

        $collection = $collection->orderBy('created_at', 'desc')->get();

        $data = array();
        foreach ($collection as $r) {
            $row = [];
            $row[] = $r->purchase_order_num;
            $row[] = date('d M Y', strtotime($r->purchase_order_date));
            $row[] = $r->description;
            $row[] = $r->terms;
            $row[] = '<span class="label '.($r->status == '4' || $r->status == '5' ? 'label-danger' : 'label-primary').'">'.$r->status_desc.'</span>';
            $row[] = date('d M Y', strtotime($r->due_date));
            $row[] = $r->supplier ? $r->supplier->name : '-';

            /*
                <a class="btn btn-primary btn-sm" href="'.route('purchase-orders.print', $r->id).'" target="_blank">
                    <i class="fa fa-print"></i> Print
                </a>
            */
            $row[] = '<div class="btn-group-vertical">
                        <a class="btn btn-primary btn-sm" href="'.route('purchase-orders.print', $r->id).'" target="_blank">
                            <i class="fa fa-print"></i> Print
                        </a>
                        <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="editForm(\'' . $r->id . '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                        '.($r->status != '5' ? '<a class="btn btn-default btn-sm update-status" href="javascript:void(0)" onclick="updateStatus('.$r->id.', '.$r->status.')">
                        <i class="fa fa-arrow-up"></i>
                        Update status
                        </a>' : '').'
                        '.($r->status != '3' ? '<a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="deleteData(\'' . $r->id . '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>' : '').'
                    </div>';

            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function print($id){
        $data = Order::findOrFail($id);
        $detail = OrderDetail::where('order_id', $data->id)->get();
        return view('logistik.purchase-orders.print', compact('data','detail'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();

        try {
            $save = new PurchaseOrder;
            $save->purchase_order_date = Carbon::createFromFormat('d/m/Y', $request->purchase_order_date)->format('Y-m-d');
            $save->supplier_id         = $request->id_supplier;
            $save->description         = $request->description;
            $save->due_date            = Carbon::createFromFormat('d/m/Y', $request->due_date)->format('Y-m-d');
            $save->terms               = $request->terms;
            $save->payment_method      = $request->payment_method;
            if($request->items){
                $save->save();
            }

            $items = [];
            foreach ($request->items as $r) {
                $dummy = new PurchaseOrderDetail;
                $dummy->item_id     = $r['item_id'];
                $dummy->description = $r['description'];
                $dummy->uom_id      = $r['uom_id'];
                $dummy->price       = $r['price'] ? $r['price'] : 0;
                $dummy->disc        = $r['disc'] ? $r['disc'] : 0;
                $dummy->tax         = $r['tax'] ? $r['tax'] : 0;
                $dummy->qty         = $r['qty'] ? $r['qty'] : 0;
                $dummy->total       = $r['total'] ? $r['total'] : 0;

                $items[] = $dummy;
            }
            $save->purchase_order_details()->saveMany($items);
            
            DB::commit();

            return PurchaseOrder::generateCode();
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'code' => 500,
                'error' => 'Can not save the data'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = PurchaseOrder::with('purchase_order_details')->find($id);
        $model->purchase_order_date = Carbon::createFromFormat('Y-m-d', $model->purchase_order_date)->format('d/m/Y');
        $model->due_date = Carbon::createFromFormat('Y-m-d', $model->due_date)->format('d/m/Y');

        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();

        try {
            $model = PurchaseOrder::find($id);

            $model->purchase_order_date = Carbon::createFromFormat('d/m/Y', $request->purchase_order_date)->format('Y-m-d');
            $model->supplier_id         = $request->id_supplier;
            $model->description         = $request->description;
            $model->due_date            = Carbon::createFromFormat('d/m/Y', $request->due_date)->format('Y-m-d');
            $model->terms               = $request->terms;
            $model->payment_method      = $request->payment_method;

            $items = [];
            foreach ($request->items as $r) {
                if(isset($r['id'])){
                    $dummy = PurchaseOrderDetail::find($r['id']);
                    $dummy->item_id     = $r['item_id'];
                    $dummy->description = $r['description'];
                    $dummy->uom_id      = $r['uom_id'];
                    $dummy->price       = $r['price'] ? $r['price'] : 0;
                    $dummy->disc        = $r['disc'] ? $r['disc'] : 0;
                    $dummy->tax         = $r['tax'] ? $r['tax'] : 0;
                    $dummy->qty         = $r['qty'] ? $r['qty'] : 0;
                    $dummy->total       = $r['total'] ? $r['total'] : 0;
                    $dummy->update();
                }else{
                    $dummy = new PurchaseOrderDetail;
                    $dummy->item_id     = $r['item_id'];
                    $dummy->description = $r['description'];
                    $dummy->uom_id      = $r['uom_id'];
                    $dummy->price       = $r['price'] ? $r['price'] : 0;
                    $dummy->disc        = $r['disc'] ? $r['disc'] : 0;
                    $dummy->tax         = $r['tax'] ? $r['tax'] : 0;
                    $dummy->qty         = $r['qty'] ? $r['qty'] : 0;
                    $dummy->total       = $r['total'] ? $r['total'] : 0;

                    $items[] = $dummy;
                }
            }
            if($request->items){
                $model->update();
            }
            $model->purchase_order_details()->saveMany($items);

            $delId = json_decode($request->delId);
            PurchaseOrderDetail::destroy($delId);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'code' => 500,
                'error' => 'Can not save the data'
            ], 500);
        }
    }

    public function updateStatus(Request $req, $id){
        $data = PurchaseOrder::findOrFail($id);
        $data->status = (int) $data->status + 1;
        $detail = PurchaseOrderDetail::where('purchase_order_id',$data->id)->get();
        foreach ($detail as $d):
            if($data->status == 3):
                $stock = Item::where('id_item',$d->item_id)->first();
                $stock->begin_stock = $stock->begin_stock + $d->qty;
                $stock->update();
            endif;
        endforeach;

        $data->update();
        {
            return 'success';
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = PurchaseOrder::find($id);
        if ($model->customer) {
            return 'Error';
        } else {
            $model->delete();
            return PurchaseOrder::generateCode();
        }
    }
}

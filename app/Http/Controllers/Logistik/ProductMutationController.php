<?php

namespace App\Http\Controllers\Logistik;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

use App\Models\Item;
use App\Models\Rack;
use App\Models\Warehouse;
use App\Models\ProductMutation;

use DataTables;
use Auth, View;

class ProductMutationController extends Controller
{
    protected $title = 'Product Mutation';
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });

        View::share('title', $this->title);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::all();
        $warehouses = Warehouse::all();
        return view('logistik.product-mutations.index', compact('items', 'warehouses'));
    }

    public function listData()
    {
        $collection = ProductMutation::orderBy('created_at', 'desc')->get();

        $data = array();
        foreach ($collection as $r) {
            $row = [];
            $row[] = $r->item ? $r->item->description : '-';
            $row[] = $r->qty;
            $row[] = $r->warehouse_begin_data ? $r->warehouse_begin_data->name : '-';
            $row[] = $r->warehouse_last_data ? $r->warehouse_last_data->name : '-';
            $row[] = $r->rack_begin_data ? $r->rack_begin_data->main_rack_code.' - '.$r->rack_begin_data->sub_rack_code : '-';
            $row[] = $r->rack_last_data ? $r->rack_last_data->main_rack_code.' - '.$r->rack_last_data->sub_rack_code : '-';
            $row[] = $r->description;
            
            $row[] = $r->is_active ? '<span class="label label-primary">Active</span>' : '<span class="label label-default">Not Active</span>';

            $row[] = '<div class="btn-group-vertical">
                        <a class="btn btn-success btn-sm" href="javascript:void(0)" onclick="editForm(\'' . $r->id . '\')">
                            <i class="fa fa-edit"></i>
                            Edit
                        </a>
                        <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="deleteData(\'' . $r->id . '\')">
                            <i class="fa fa-trash"></i>
                            Delete
                        </a>
                    </div>';

            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function getRack(Request $req){
        $data = Rack::query();
        if($req->warehouse_id){
            $data = $data->where('warehouse_id', $req->warehouse_id);
        }
        $data = $data->get();
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $save = new ProductMutation;
        $save->item_id         = $request->item_id;
        $save->qty             = $request->qty;
        $save->warehouse_begin = $request->warehouse_begin;
        $save->warehouse_last  = $request->warehouse_last;
        $save->rack_begin      = $request->rack_begin;
        $save->rack_last       = $request->rack_last;
        $save->description     = $request->description;
        $save->is_active       = $request->filled('is_active') ? 1 : 0;
        $save->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $model = ProductMutation::find($id);

        return response()->json($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = ProductMutation::find($id);

        $model->item_id         = $request->item_id;
        $model->qty             = $request->qty;
        $model->warehouse_begin = $request->warehouse_begin;
        $model->warehouse_last  = $request->warehouse_last;
        $model->rack_begin      = $request->rack_begin;
        $model->rack_last       = $request->rack_last;
        $model->description     = $request->description;
        $model->is_active       = $request->filled('is_active') ? 1 : 0;
        $model->update();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = ProductMutation::find($id);
        if($model){
            $model->delete();
        }

    }
}

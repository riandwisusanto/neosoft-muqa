<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Models\Outlet;
use App\Models\UserOutlet;
use Auth;

class OutletController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('setting.outlet.index');
    }

    public function listData()
    {
        // $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        // $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();

        if (array_intersect(["SUPER_USER"], json_decode(Auth::user()->level))) {
            $outlet = Outlet::all();
        } else {
            $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
            $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
        }
        $no = 0;
        $data = array();
        foreach ($outlet as $list) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $list->outlet_name;
            $row[] = $list->outlet_code;
            $row[] = $list->outlet_phone;
            $row[] = $list->outlet_fax;
            $row[] = $list->status_outlet ? '&#10004;' : '';
            $row[] = '<img src=\''.'storage/' . $list->logo . '\' alt = \'no image\' class=\'img-responsive\'>';
            $action = '<div class="btn-group-vertical">
                <a onclick="editForm(' . $list->id_outlet . ')" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Edit</a>';
            if (auth()->user()->level == json_encode(["ADMINISTRATOR"])) {
                $action = $action . '<a onclick="deleteData(' . $list->id_outlet . ')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</a></div>';
            }
            $row[] = $list->address;
            $row[] = $action;
            $data[] = $row;
        }

        $output = array("data" => $data);
        return response()->json($output);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return response()->json($request);
        $outlet = new Outlet;
        $outlet->outlet_name = $request->name;
        $outlet->outlet_code = $request->code;
        $outlet->outlet_phone = $request->telephone;
        $outlet->address = $request->address;
        $outlet->outlet_fax = $request->fax;
        $outlet->status_outlet = 1;
        // if ($request->file('logo') !== null) {
        //     $file = $request->file('logo');
        //     $ext = $file->getClientOriginalExtension();
        //     $image = "outlet_" . date('YmdHis') . ".$ext";
        //     $request->file('logo')->move(public_path() . '/img/outlet', $image);
        //     $outlet->logo = 'img/outlet/' . $image;
        // } else {
        //     $outlet->logo = '';
        // }

        $img = $request->file('logo');
        if ($img) {
            $img_path = $img->store('outlet', 'public');
            $outlet->logo = $img_path;
        }

        $outlet->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $outlet = Outlet::find($id);
        echo json_encode($outlet);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $outlet = Outlet::find($id);
        $outlet->outlet_name = $request->name;
        $outlet->outlet_code = $request->code;
        $outlet->outlet_phone = $request->telephone;
        $outlet->address = $request->address;
        $outlet->outlet_fax = $request->fax;
        $outlet->status_outlet = 1;
        // if ($request->file('logo') !== null) {
        //     if (\File::exists($outlet->logo)) {
        //         \File::delete($outlet->logo);
        //     }
        //     $file = $request->file('logo');
        //     $ext = $file->getClientOriginalExtension();
        //     $image = "brand_" . date('YmdHis') . ".$ext";
        //     $request->file('logo')->move(public_path() . '/img/outlet', $image);
        //     $outlet->logo = 'img/outlet/' . $image;
        // }

        $img = $request->file('logo');
        if ($img) {
            if ($outlet->logo && file_exists(storage_path('app/public/' . $outlet->logo))) {
                \Storage::delete('public/' . $outlet->logo);
            }
            $img_path = $img->store('outlet', 'public');
            $outlet->logo = $img_path;
        }
        $outlet->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $outlet = Outlet::find($id);
        if ($outlet->invoice) {
            if (\File::exists($outlet->logo)) {
                \File::delete($outlet->logo);
            }
            $outlet->delete();
        } else {
            echo 'error';
        }
    }
}

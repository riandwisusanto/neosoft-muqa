<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;

use App\Models\Agent;
use App\Models\Invoice;

class AgentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('admin')) {
                return $next($request);
            }
            abort(403, 'You do not have enough access rights');
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('agent.index');
    }

    public function list()
    {
        return view('agent.list');
    }

    public function listData()
    {
        $agent = Agent::where('status', 0)->whereNull('uniq_code')->get();
        $data = array();
        $no = 1;
        foreach ($agent as $list) {
            $row = [];
            $row[] = $no++;
            $row[] = $list->created_at->format('d/m/Y');
            $row[] = $list->agent_name;
            $row[] = $list->phone;
            $row[] = $list->email;
            $row[] = $list->address;
            $row[] = '<div class="btn-group">
                        <a class="btn btn-success btn-xs"  onclick="approve(\'' . $list->id_agent . '\')">
                            <i class="fa fa-check"></i>
                        </a>
                        <a class="btn btn-danger btn-xs"  onclick="deleteData(\'' . $list->id_agent . '\')">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>';

            $data[] = $row;
        }
        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function rejectData()
    {
        $agent = Agent::where('status', 1)->get();
        $data = array();
        $no = 1;
        foreach ($agent as $list) {
            $row = [];
            $row[] = $no++;
            $row[] = $list->created_at->format('d/m/Y');
            $row[] = $list->agent_name;
            $row[] = $list->phone;
            $row[] = $list->email;
            $row[] = $list->address;
            $row[] = '<div class="btn-group">
                        <a class="btn btn-success btn-xs"  onclick="enableAgent(\'' . $list->id_agent . '\')">
                            <i class="fa fa-check"></i>
                        </a>
                    </div>';
            $data[] = $row;
        }
        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function active()
    {
        return view('agent.active');
    }

    public function activeData()
    {
        $agent = Agent::where('status', 0)->whereNotNull('uniq_code')->get();
        $data = array();
        $no = 1;
        foreach ($agent as $list) {
            $row = [];
            $row[] = $no++;
            $row[] = $list->created_at->format('d/m/Y');
            $row[] = $list->agent_name;
            $row[] = $list->phone;
            $row[] = $list->email;
            $row[] = $list->address;
            $row[] = '<div class="btn-group">
                        <a class="btn btn-info btn-xs"  onclick="detail(\'' . $list->id_agent . '\')">
                            <i class="fa fa-eye"></i>
                        </a>
                        <a class="btn btn-warning btn-xs"  onclick="takeOut(\'' . $list->id_agent . '\')">
                            Take Out
                        </a>
                    </div>';
            $data[] = $row;
        }
        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function store(Request $request)
    {
        $request->validate([
            'email' => 'unique:agents|max:255',
            'phone' => 'unique:agents|numeric',
        ]);

        $agent = new Agent;
        $agent->agent_name = $request->name;
        $agent->email = $request->email;
        $agent->agent_code = Agent::generateAgentCode();
        $agent->phone = $request->phone;
        $agent->address = $request->address;
        $agent->save();
    }

    public function detailAgent($id)
    {
        $invoice = Invoice::where('void_invoice', 0)
            ->where('agent_id', $id)->get()->groupBy('customer_id');

        $data = array();
        $payments = 0;
        foreach ($invoice as $list) {
            foreach ($list as $key) {
                foreach ($key->payment as $payment) {
                    $payments += str_replace(array('.', ','), "", $payment->amount);
                }
            }
            $row = [];
            $row[] = $list[0]->customer->induk_customer;
            $row[] = $list[0]->customer->full_name;
            $row[] = $list[0]->customer->phone;
            $row[] = $list[0]->customer->ktp;
            $row[] = $list[0]->customer->email;
            $row[] = format_money($payments);
            $data[] = $row;
        }
        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Agent::find($id);
        $update->username = Str::before($update->email, '@');
        $update->password = Hash::make('123456789');
        $update->active_date = now();
        $update->uniq_code = str_random(30);
        $update->update();
    }

    public function takeOut($id)
    {
        $update = Agent::find($id);
        $update->status = 1;
        $update->update();
    }

    public function enableAgent($id)
    {
        $update = Agent::find($id);
        $update->status = 0;
        $update->update();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Agent::find($id)->delete();
    }

    public function alreadyClient()
    {
        return view('agent.already');
    }

    public function storeClient(Request $request)
    {
        $request->validate([
            'email' => 'unique:agents|max:255',
            'phone' => 'unique:agents|numeric',
        ]);

        $agent = new Agent;
        $agent->agent_name = $request->name;
        $agent->email = $request->email;
        $agent->agent_code = $request->agent_code;
        $agent->phone = $request->phone;
        $agent->address = $request->address;
        $agent->save();
    }
}

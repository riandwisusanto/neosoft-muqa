<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;

class VerificationApiController extends Controller
{
    use VerifiesEmails;

    public function verify(Request $request)
    {
        $customerID = $request['id_customer'];
        $user = Customer::findOrFail($customerID);
        if ($user->email_verified_at == null) {
            $date = date("Y-m-d g:i:s");
            $user->email_verified_at = $date;
            $user->save();
            return view('verify-email.verify');
        } else {
            return view('verify-email.verify');
        }
    }

    public function resend(Request $request)
    {
        $customer = new Customer;
        //dd($customer);
        //dd($oke->email->sendEmailVerificationNotification());
        $cekCustomer = $customer->where('email', $request['email'])->whereNotNull('email_verified_at')->count();
        if ($cekCustomer) {
            return response()->json('User already have verified email!', 422);
        }

        //dd($request->get('email'));
        $sip = $customer->where('email', $request->get('email'))->first();
        //dd($customer);
        $sip->sendEmailVerificationNotification();
        return response()->json('The notification has been resubmitted');
    }

    // public function __construct()
    // {
    //     $this->middleware('throttle:6,1');
    // }
}

<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\ActivityGroup;
use App\Models\Bank;
use App\Models\Invoice;
use App\Models\InvoicePoint;
use App\Models\InvoiceDetail;
use App\Models\InvoiceBalance;
use App\Models\Outlet;
use App\Models\Payment;
use App\Models\Usage;
use App\Models\UserOutlet;
use App\Models\LogConvert;
use App\Models\Customer as CustomerModel;
use App\Models\Therapist;
use Carbon\carbon;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Mpdf\Mpdf;
use Auth;
use Customer;
use Illuminate\Support\Carbon as SupportCarbon;

class ReportCollectionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            if (Gate::allows('sales')) {
                return $next($request);
            }

            abort(403, 'You do not have enough access rights');
        });
    }
    //collection by Outlet
    public function index_collection_outlet()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
        // $outlet = Outlet::all();
        $from = Carbon::parse(now())->format('d-m-Y');
        $to = Carbon::parse(now())->format('d-m-Y');
        return view('report.collection.outlet.report')
            ->with('outlet', $outlet)
            ->with('from', $from)
            ->with('to', $to);
    }

    public function index_collection_invoice()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $from = Carbon::parse(now())->format('d-m-Y');
        $to = Carbon::parse(now())->format('d-m-Y');
        return view('report.collection.invoice.report')
            ->with('outlet', $outlet)
            ->with('from', $from)
            ->with('to', $to);
    }

    public function all_collection_outlet(Request $request)
    {
        $outlet_id = Outlet::first();
        $all_outlet = Outlet::all();
        $from = Carbon::now()->toDateString();
        $to = Carbon::now()->toDateString();

        if ($request->from && $request->to && $request->outlet) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
            $outlet_id = explode(',', $request->outlet);
            // for ($i = 0; $i < count($request->outlet); $i++) {
            //     $outlet_id[] = $request->outlet[$i];
            // }
        }

        $outlet = "";
        if (count($all_outlet) == count($outlet_id)) {
            $outlet = "All Outlet";
        } else {
            foreach ($outlet_id as $i => $list) {
                $outlet .= Outlet::where('id_outlet', $list)->value('outlet_name');
                if (($i+1) != count($outlet_id)) {
                    $outlet .= ", ";
                }
            }
        }

        $invoices = Invoice::whereBetween('inv_date', [$from, $to])
            ->where('void_invoice', 0)
            ->whereIn('outlet_id', $outlet_id)
            ->orderBy('id_invoice', 'ASC')
            ->get();

        $invoice_id = Invoice::whereBetween('inv_date', [$from, $to])
            ->where('void_invoice', 0)
            ->whereIn('outlet_id', $outlet_id)
            ->orderBy('id_invoice', 'ASC')
            ->pluck('id_invoice')->toArray();

        $invoice_details = InvoiceDetail::whereIn('inv_id', $invoice_id)->pluck('inv_id')->toArray();

        $data = array();
        foreach ($invoices as $list) {
            foreach($list->invoice_detail as $detailIndex => $detail){
                $row = array();
                //invoicenum & customer
                if ($detailIndex == 0) {
                    $row[] = $list->inv_code;
                    $row[] = $list->customer->full_name ." | ". $list->customer->induk_customer;
                    $row[] = $list->inv_date;
                } else {
                    if ($list->invoice_detail[$detailIndex]->inv_id == $list->invoice_detail[$detailIndex - 1]->inv_id) {
                        $row[] = "";
                        $row[] = "";
                        $row[] = "";
                    } else {
                        $row[] = $list->inv_code;
                        $row[] = $list->customer->full_name ." | ". $list->customer->induk_customer;
                        $row[] = $list->inv_date;
                    }
                }
                //treatment logic
                    $type_line = $detail->type_line;
                    if(substr($detail->package_code,0,1) == "T"){
                        foreach($detail->invoice_package as $package){
                            $treatment = $package->product_treatment->product_treatment_name;
                        }
                        $hargatreatment = $detail->current_price;
                        if($detail->discount_2 == ''){
                            $disctreatment = $detail->discount_1;
                        }else{
                            $disctreatment = $detail->discount_1 . " + " . $detail->discount_2;
                        }
                        $totaltreatment = $detail->subtotal;
                    }else{
                        $treatment = "";
                        $hargatreatment = 0;
                        $disctreatment = 0;
                        $totaltreatment = 0;
                    }
                    if($detail->therapist_id != ''){
                        $terapis = $detail->therapist->therapist_name ?? '(deleted)';
                    }else if($list->therapist_id != ''){
                        $terapis = $list->therapist->therapist_name;
                    }else{
                        $terapis = "";
                    }
                //treatment
                $row[] = $treatment;
                //price
                $row[] = $hargatreatment;
                //disc and disc rp
                if($type_line == 1){
                    $row[] = "0";
                    $row[] = $disctreatment;
                }else{
                    $row[] = $disctreatment;
                    $row[] = "0";
                }
                //total
                $row[] = $totaltreatment;
                //therapist
                $row[] = $terapis;
                //product logic

                    if(substr($detail->package_code,0,1) == "P"){
                        foreach($detail->invoice_package as $package){
                            $product = $package->product_treatment->product_treatment_name;
                        }
                        $qty = $detail->qty;
                        $hargaproduct = $detail->current_price;
                        if($detail->discount_2 == ''){
                            $discproduct = $detail->discount_1;
                        }else{
                            $discproduct = $detail->discount_1 . " + " . $detail->discount_2;
                        }
                        $totalproduct = $detail->subtotal;
                    }else{
                        $product = '';
                        $qty = 0;
                        $hargaproduct = 0;
                        $discproduct = 0;
                        $totalproduct = 0;
                    }
                //product
                $row[] = $product;
                //qty
                $row[] = $qty;
                //price
                $row[] = $hargaproduct;
                //disc and disc rp
                if($type_line == 1){
                    $row[] = $discproduct;
                    $row[] = "0";
                }else{
                    $row[] = "0";
                    $row[] = $discproduct;
                }
                //total
                $row[] = $totalproduct;
                $data[] = $row;
            }
        }

        $invoices2 = Invoice::whereBetween('inv_date', [$from, $to])
            ->where('void_invoice', 0)
            ->whereIn('outlet_id', $outlet_id)
            ->pluck('id_invoice')->toArray();
        $data2 = array();
        $items = array("Product", "Treatment");
        foreach($items as $item){
            $row = array();
            $row[] = $item;
            if($item == "Product"){
                $subtotal = InvoiceDetail::whereIn('inv_id', $invoices2)
                            ->where('package_code', 'like', 'P%')
                            ->sum('current_price');

                $grandtotal = InvoiceDetail::whereIn('inv_id', $invoices2)
                            ->where('package_code', 'like', 'P%')
                            ->sum('subtotal');
            }else{
                $subtotal = InvoiceDetail::whereIn('inv_id', $invoices2)
                            ->where('package_code', 'like', 'T-%')
                            ->sum('current_price');

                $grandtotal = InvoiceDetail::whereIn('inv_id', $invoices2)
                            ->where('package_code', 'like', 'T-%')
                            ->sum('subtotal');
            }
            // return $subtotal;
            $row[] = format_money($subtotal);
            $row[] = format_money($grandtotal);
            $data2[] = $row;
        }

        $invoices3 = Invoice::whereBetween('inv_date', [$from, $to])
            ->where('void_invoice', 0)
            ->whereIn('outlet_id', $outlet_id)
            ->pluck('id_invoice')->toArray();
        $data3 = array();
        $bank = Bank::all();

        foreach($bank as $list){
            $row = array();
            $row[] = $list->bank_name;
            $total = Payment::whereIn('inv_id', $invoices3)
                    ->where('bank_id', $list->id_bank)
                    ->sum('amount');
            $row[] = format_money($total);
            $data3[] = $row;
        }

        $invoices4 = Invoice::whereBetween('inv_date', [$from, $to])
            ->where('void_invoice', 0)
            ->whereIn('outlet_id', $outlet_id)
            ->pluck('id_invoice')->toArray();
        $data4 = array();
        $row = array();
        $customers = Invoice::whereIn('id_invoice', $invoices4)->get()->groupBy('customer_id')->count();
        $row[] = $customers;
        $total = Payment::whereIn('inv_id', $invoices4)
                    ->sum('amount');
        $row[] = format_money($total);
        $data4[] = $row;


        $this->generatePDF($from, $to, $outlet, $data, $data2, $data3, $data4);
        // return view('report.collection.outlet.print-pdf', compact('from','to','outlet','data','data2','data3','data4'));
    }

    public function detailCollectionInvoice(Request $request)
    {
        $invoices = Invoice::cpoReportBalance($request);

        $data = array();
        foreach ($invoices as $list) {
            $customer = CustomerModel::where('id_customer', $list->customer_id)->first();

            if ($list->id_balance == 'id_balance') {
                $invoice_detail = InvoiceDetail::where('inv_id', $list->id_invoice)->get();
                $payments = Payment::where('inv_id', $list->id_invoice)->whereNull('balance_id')->get();

                if ($payments && ($payments[0]->amount > 0 && $list->status_inv == 1))
                    $tipe = 'LUNAS';
                else
                    $tipe = 'BELUM LUNAS';

                foreach ($invoice_detail as $detailIndex => $detail) {
                    $row = array();
                    $log_convert = LogConvert::where('to_invoice_id', $list->inv_code)->get();
                    //invoicenum & customer
                    if ($detailIndex == 0) {
                        if (count($log_convert) > 0) {
                            $row[] = '<a href=" ' . route("invoice-convert.print", $list->id_invoice) . ' " target="_blank">' . $list->inv_code . '</a>';
                        } else {
                            $row[] = '<a href="' . route("invoice.print", $list->id_invoice) . '" target="_blank">' . $list->inv_code . '</a>';
                        }
                        $row[] = $tipe;
                        $row[] = $customer->full_name . " | " . $customer->induk_customer;
                        $row[] = $list->inv_date;
                    } else {
                        if ($invoice_detail[$detailIndex]->inv_id == $invoice_detail[$detailIndex - 1]->inv_id) {
                            $row[] = "";
                            $row[] = "";
                            $row[] = "";
                            $row[] = "";
                        } else {
                            if (count($log_convert) > 0) {
                                $row[] = '<a href=" ' . route("invoice-convert.print", $list->id_invoice) . ' " target="_blank">' . $list->inv_code . '</a>';
                            } else {
                                $row[] = '<a href="' . route("invoice.print", $list->id_invoice) . '" target="_blank">' . $list->inv_code . '</a>';
                            }
                            $row[] = "";
                            $row[] = $customer->full_name . " | " . $customer->induk_customer;
                            $row[] = $list->inv_date;
                        }
                    }
                    //treatment logic
                    $treatment = "";
                    $bayar = 0;
                    $code = "";
                    $type_line = $detail->type_line;

                    if($detail->package)
                    {
                        $treatment = $detail->package->package_name;
                        $code = $detail->package->package_code;
                    }else{
                        $treatment = $detail->product_treatment->product_treatment_name;
                        $code = $detail->product_treatment->product_treatment_code;
                    }
                    $hargatreatment = $detail->current_price;
                    if ($detail->discount_2 == '') {
                        $disctreatment = $detail->discount_1;
                    } else {
                        $disctreatment = $detail->discount_1 . " + " . $detail->discount_2;
                    }
                    $totaltreatment = $detail->subtotal;

                    if ($detail->therapist_id != '') {
                        $terapis = $detail->therapist->therapist_name ?? '(deleted)';
                    } else if ($list->therapist_id != '') {
                        $tid = Therapist::where('id_therapist', $list->therapist_id)->first();
                        $terapis = $tid->therapist_name;
                    } else {
                        $terapis = "";
                    }
                    //treatment
                    $row[] = $code;
                    $row[] = $treatment;
                    $row[] = $detail->qty;
                    //price
                    $row[] = $hargatreatment;
                    //disc and disc rp
                    if ($type_line == 1) {
                        $row[] = "0";
                        $row[] = $disctreatment;
                    } else {
                        $row[] = $disctreatment;
                        $row[] = "0";
                    }
                    //total
                    $row[] = $totaltreatment;

                    $amounts = [];
                    if ($detailIndex == 0) {
                        foreach ($payments as $payment) {
                            $amounts[] = $payment->amount;
                        }
                        // $amounts = $detailIndex == 0 ? $payments[0]->amount : '';
                    }
                    $row[] = implode('<br>', $amounts);
                    //therapist
                    $row[] = $terapis;

                    $row[] = "";

                    //bayar
                    $banks = [];
                    foreach ($payments as $payment) {
                        $banks[] = $payment->bank->bank_name;
                    }
                    $row[] = implode('<br>', $banks);

                    $data[] = $row;
                }
            } else {
                $payments = Payment::where('inv_id', $list->id_invoice)->where('balance_id', $list->id_balance)->get();

                $row = array();
                $row[] = '<a href="' . route("invoice-balance.print", $list->id_invoice) . '" target="_blank">' . $list->inv_code . '</a>';
                $row[] = '';
                $row[] = $customer->full_name . " | " . $customer->induk_customer;
                $row[] = $list->inv_date;

                //treatment
                $row[] = '';
                $row[] = '';
                $row[] = '';
                //price
                $row[] = "0";
                //disc and disc rp
                $row[] = "0";
                $row[] = "0";
                //total
                $row[] = "0";
                $row[] = $payments[0]->amount;
                //therapist
                $row[] = "-";

                $row[] = "";
                $row[] = "";
                //bayar
                $banks = [];
                foreach ($payments as $payment) {
                    $banks[] = $payment->bank->bank_name;
                }
                $row[] = implode('<br>', $banks);

                $data[] = $row;
            }
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function grandtotalCollectionInvoice(Request $request)
    {
        $invoices = Invoice::cpoReportBalance($request);
        $inv_ids = [];
        $data = [];
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();

        $bank = Bank::where('status_bank', 1)
        ->whereHas('bank_outlets', function ($q) use ($outlet_id) {
            return $q->whereIn('outlet_id', $outlet_id);
        })->get();

        $inv = [];

        foreach ($invoices as $list) {
            if ($list->id_balance == 'id_balance') {
                $payments = DB::select(DB::raw("select id_payment, amount, bank_id from payments where inv_id = $list->id_invoice and balance_id is null" ));
            } else {
                $payments = DB::select(DB::raw("select id_payment, amount, bank_id from payments where inv_id = $list->id_invoice and balance_id = $list->id_balance" ));
            }

            if (!isset($payments[0]))  {
                continue;
            }

            // $payment = $payments[0];
            foreach ($payments as $payment) {
                $inv[$payment->bank_id] = !isset($inv[$payment->bank_id]) ? $payment->amount : $inv[$payment->bank_id] + $payment->amount;
            }

        }

        foreach($bank as $list){
            $row = array();
            $row[] = $list->bank_name;
            $row[] = !isset($inv[$list->id_bank]) ? '0' : format_money($inv[$list->id_bank]);
            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function totalCollectionInvoice(Request $request)
    {
        $bayar = 0;
        $customers = [];
        $invoices = Invoice::cpoReportBalance($request);

        foreach ($invoices as $list) {
            if ($list->id_balance == 'id_balance') {
                $payments = DB::select(DB::raw("select id_payment, amount from payments where inv_id = $list->id_invoice and balance_id is null" ));
            } else {
                $payments = DB::select(DB::raw("select id_payment, amount from payments where inv_id = $list->id_invoice and balance_id = $list->id_balance" ));
            }

            if (!isset($payments[0]))  {
                continue;
            }

            in_array($list->customer_id, $customers) ? '' : array_push($customers, $list->customer_id);
            foreach ($payments as $payment) {
                $bayar += $payment->amount;
            }
        }
        $row[] = count($customers);
        $row[] = format_money($bayar);
        $data[] = $row;

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function generatePDF($from, $to, $outlet, $data, $data2, $data3, $data4)
    {
        $detail = '';

        foreach ($data as $list) {
            $row = '<tr>';
            foreach ($list as $value) {
                $row .= '<td>'. $value .'</td>';
            }
            $row .= "</tr>";

            $detail .= $row;
        }

        $productTreatment = '';
        foreach ($data2 as $list) {
            $row = '<tr>';
            foreach ($list as $value) {
                $row .= '<td>'. $value .'</td>';
            }
            $row .= "</tr>";

            $productTreatment .= $row;
        }

        $payment = '';
        foreach ($data3 as $list) {
            $row = '<tr>';
            foreach ($list as $value) {
                $row .= '<td>'. $value .'</td>';
            }
            $row .= "</tr>";

            $payment .= $row;
        }

        $customer = '';
        foreach ($data4 as $list) {
            $row = '<tr>';
            foreach ($list as $value) {
                $row .= '<td>'. $value .'</td>';
            }
            $row .= "</tr>";

            $customer .= $row;
        }

        $html = '
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Print PDF</title>
            <style>
                @page {
                    margin: 24px;
                }
                body {
                    font-family: sans-serif !important;
                    font-size: 11px;
                }

                h1, h4 {
                    text-align: center;
                    margin: 0px;
                }

                h1 {
                    margin-bottom: 5px;
                }

                table {
                    width: 100%;
                    border-collapse: collapse;
                    border: 1px solid #000;
                }

                table th,
                table td {
                    padding: 4px;
                    border: 1px solid #000;
                }

                table td {
                    vertical-align: top;
                }

                #invoice-grandtotal,
                #invoice-payment,
                #invoice-total {
                    float: left;
                    width: 30%;
                    margin: 0 30px;
                }

                .sparator {
                    width: 100%;
                    height: 31px;
                    clear: both;
                }
            </style>
        </head>
        <body>
            <h1>Collection by Outlet Report</h1>
            <h4>'. $outlet .': Period '. date("d/m/Y", strtotime($from)) .' - '. date("d/m/Y", strtotime($to)) .'</h4>

            <div class="sparator"></div>

            <table>
                <thead>
                    <tr>
                        <th>NOFAK</th>
                        <th>CUST. | NO.MEMBER</th>
                        <th>TANGGAL</th>
                        <th>TREATMENT</th>
                        <th>HARGA</th>
                        <th>DISC</th>
                        <th>DISC.RP</th>
                        <th>TOTAL</th>
                        <th>TERAPIS</th>
                        <th>PRODUK</th>
                        <th>QTY</th>
                        <th>HARGA</th>
                        <th>DISC.RP</th>
                        <th>DISC</th>
                        <th>TOTAL</th>
                    </tr>
                </thead>
                <tbody>
                    '.$detail.'
                </tbody>
            </table>

            <div class="sparator"></div>

            <div id="invoice-grandtotal">
                <table border="1px">
                    <thead>
                        <tr>
                            <th>Subtotal</th>
                            <th>Harga</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        '. $productTreatment .'
                    </tbody>
                </table>
            </div>

            <div id="invoice-payment">
                <table border="1px">
                    <thead>
                        <tr>
                            <th>Jenis Pembayaran</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        '. $payment .'
                    </tbody>
                </table>
            </div>

            <div id="invoice-total">
                <table border="1px">
                    <thead>
                        <tr>
                            <th>Jml Customer</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        '. $customer .'
                    </tbody>
                </table>
            </div>

            <div class="sparator"></div>
        </body>
        </html>
        ';


        $mpdf = new Mpdf([
            'format' => 'A4-L',
            'setAutoTopMargin' => 'stretch',
            'autoMarginPadding' => 5
        ]);

        $mpdf->WriteHTML($html, \Mpdf\HTMLParserMode::DEFAULT_MODE);
        $mpdf->Output();
    }

    public function detailCollectionOutlet(Request $request)
    {
        $outlet_id = Outlet::first();
        $from = Carbon::now()->toDateString();
        $to = Carbon::now()->toDateString();

        if ($request->from && $request->to && $request->outlet) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
            // $outlet_id = [];
            // for ($i = 0; $i < count($request->outlet); $i++) {
            //     $outlet_id[] = $request->outlet[$i];
            // }
        }

        $sqlInvoice = "
        SELECT
            i.id_invoice,
            i.inv_code,
            i.ongkir,
            DATE_FORMAT(i.inv_date, '%%d/%%m/%%Y') as inv_date,
            DATE_FORMAT(i.created_at, '%%H:%%i:%%s') as inv_time,
            i.therapist_id,
            t.therapist_name,
            CONCAT(c.full_name, ' | ', c.induk_customer) as customer_name,
            c.birth_date,
            c.phone,
            c.gender,
            c.address,
            ct.province,
            id.id_inv_detail,
            id.inv_id,
            id.package_code,
            id.current_price,
            id.qty,
            id.discount_1,
            id.discount_2,
            id.subtotal,
            id.type_line,
            pt.product_treatment_name as product,
            tr.product_treatment_name as treatment,
            pkg.package_name as package,
            JSON_ARRAYAGG(pay.bank_id) bank_id,
            JSON_ARRAYAGG(bk.bank_name) bank_name,
            u.name as cashier,
            i.izzibook_sync_status
        FROM
            invoices i
            JOIN invoice_details id ON i.id_invoice = id.inv_id
            JOIN payments pay ON i.id_invoice = pay.inv_id
            JOIN users u ON i.staff_id = u.id
            LEFT JOIN banks bk ON bk.id_bank = pay.bank_id
            LEFT JOIN
                product_treatments tr ON tr.product_treatment_code = id.package_code AND SUBSTRING(id.package_code, 1, 2) ='T-'
            LEFT JOIN
                product_treatments pt ON pt.product_treatment_code = id.package_code AND SUBSTRING(id.package_code, 1, 2) = 'P-'
            LEFT JOIN
                packages pkg on pkg.package_code = id.package_code AND substring(id.package_code, 1, 3) = 'PKG'
            LEFT JOIN customers c ON i.customer_id = c.id_customer
            LEFT JOIN provinces ct ON c.city_id = ct.id_province
            LEFT JOIN therapists t ON i.therapist_id = t.id_therapist
        WHERE
            i.inv_date BETWEEN ? AND ?
        AND
            i.void_invoice = 0
        AND
            i.outlet_id IN (%s)
        GROUP BY id_inv_detail
        ORDER BY i.id_invoice ASC
        ";

        $outlets = Outlet::pluck('id_outlet')->first();
        if (isset($request->outlet)) {
            $outlets = implode(',', $request->outlet);
        }
        $sqlInvoice = sprintf($sqlInvoice, $outlets);

        $result = DB::select($sqlInvoice, [$from, $to]);
        $data = [];
        $totalProduk =0;
        $totalTreatment =0;
        $totalOngkir = 0;

        foreach ($result as $detailIndex => $list) {
            $ageInYear = date('Y') - Carbon::createFromFormat('Y-m-d', $list->birth_date)->format('Y');
                $row = [];
                //invoicenum & customer
                if($detailIndex == 0){
                    $row[] = '<a href="' . route("invoice.print-thermal", $list->id_invoice) . '" target="_blank">' . $list->inv_code . '</a>';
                    $row[] = $list->izzibook_sync_status == 1 ? '<i class="fa fa-check fa-lg"></i>' : '';
                    $row[] = $list->customer_name;
                    $row[] = $ageInYear;
                    $row[] = $list->gender;
                    if (array_intersect(["OUTLET_SUPERVISOR"], json_decode(Auth::user()->level))) {
                        $row[] = $list->phone;
                        $row[] = $list->address. ', '. $list->province;
                    }
                    $row[] = $list->inv_date;
                    $row[] = $list->inv_time;
                }else{
                    if ($result[$detailIndex]->inv_id == $result[$detailIndex - 1]->inv_id) {
                        $row[] = "";
                        $row[] = "";
                        $row[] = "";
                        $row[] = "";
                        $row[] = "";
                        if (array_intersect(["OUTLET_SUPERVISOR"], json_decode(Auth::user()->level))) {
                            $row[] = "";
                            $row[] = "";
                        }
                        $row[] = "";
                        $row[] = "";
                    } else {
                        $row[] = '<a href="' . route("invoice.print-thermal", $list->id_invoice) . '" target="_blank">' . $list->inv_code . '</a>';
                        $row[] = $list->izzibook_sync_status == 1 ? '<i class="fa fa-check fa-lg"></i>' : '';
                        $row[] = $list->customer_name;
                        $row[] = $ageInYear;
                        $row[] = $list->gender;
                        if (array_intersect(["OUTLET_SUPERVISOR"], json_decode(Auth::user()->level))) {
                            $row[] = $list->phone;
                            $row[] = $list->address. ', '. $list->province;
                        }
                        $row[] = $list->inv_date;
                        $row[] = $list->inv_time;
                    }
                }

                if($detailIndex == 0)
                {
                    $price_treatment = $list->treatment || $list->package ? $list->current_price : 0;
                    $price_product = $list->product ? $list->current_price : 0;

                    $row[] = $list->treatment.$list->package;
                    $row[] = number_format($price_treatment);

                    if($list->discount_2 == ''){
                        $discproduct = $list->discount_1;
                        $convdisc_treatment = $price_treatment * ($list->discount_1 / 100);
                        $convdisc_product = $price_product * ($list->discount_1 / 100);
                    }else{
                        $discproduct = $list->discount_1 . " + " . $list->discount_2;
                    }

                    if ($list->treatment || $list->package) {
                        $row[] = $list->type_line == 0 ? $discproduct: 0;
                        $row[] = $list->type_line == 0 ? number_format($convdisc_treatment): 0;
                        $row[] = $list->type_line == 1 ? $discproduct : 0;
                    } else {
                        $row[] = 0;
                        $row[] = 0;
                        $row[] = 0;
                    }

                    $row[] = $list->treatment || $list->package ? number_format($list->subtotal) : 0;

                    $row[] = $list->therapist_id ? $list->therapist_name : '';
                    $row[] = $list->product;
                    $row[] = $list->qty;
                    $row[] = number_format($price_product);

                    if ($list->product) {
                        $row[] = $list->type_line == 1 ? $discproduct : 0;
                        $row[] = $list->type_line == 0 ? $discproduct : 0;
                        $row[] = $list->type_line == 0 ? number_format($convdisc_product) : 0;
                    } else {
                        $row[] = 0;
                        $row[] = 0;
                        $row[] = 0;
                    }

                    $row[] = $list->product ? number_format($list->subtotal) : 0;
                    $ongkir = isset($list->ongkir) ? format_money($list->ongkir) : 0;
                    $row[] = $ongkir;
                    $totalOngkir +=  isset($list->ongkir) ? $list->ongkir : 0;

                }else{
                    if ($result[$detailIndex]->id_inv_detail == $result[$detailIndex - 1]->id_inv_detail) {
                        $row[] = "";
                        $row[] = "";
                        $row[] = "";
                        $row[] = "";
                        $row[] = "";
                        $row[] = "";
                        $row[] = "";
                        $row[] = "";
                        $row[] = "";
                        $row[] = "";
                        $row[] = "";
                        $row[] = "";
                        $row[] = "";
                        $row[] = "";
                        $row[] = "";

                    } else {
                        $price_treatment = $list->treatment || $list->package ? $list->current_price : 0;
                        $price_product = $list->product ? $list->current_price : 0;

                        $row[] = $list->treatment.$list->package;
                        $row[] = number_format($price_treatment);

                        if($list->discount_2 == ''){
                            $discproduct = $list->discount_1;
                            $convdisc_treatment = $price_treatment * ($list->discount_1 / 100);
                            $convdisc_product = $price_product * ($list->discount_1 / 100);

                        }else{
                            $discproduct = $list->discount_1 . " + " . $list->discount_2;
                        }

                        if ($list->treatment || $list->package) {
                            $row[] = $list->type_line == 0 ? $discproduct : 0;
                            $row[] = $list->type_line == 0 ? number_format($convdisc_treatment) : 0;
                            $row[] = $list->type_line == 1 ? $discproduct : 0;
                        } else {
                            $row[] = 0;
                            $row[] = 0;
                            $row[] = 0;
                        }

                        $row[] = $list->treatment || $list->package ? number_format($list->subtotal) : 0;
                        $row[] = $list->therapist_id ? $list->therapist_name : '';
                        $row[] = $list->product;
                        $row[] = $list->qty;
                        $row[] = number_format($price_product);

                        if ($list->product) {
                            $row[] = $list->type_line == 1 ? $discproduct : 0;
                            $row[] = $list->type_line == 0 ? $discproduct : 0;
                            $row[] = $list->type_line == 0 ? number_format($convdisc_product) : 0;
                        } else {
                            $row[] = 0;
                            $row[] = 0;
                            $row[] = 0;
                        }

                        $row[] = $list->product ? number_format($list->subtotal) : 0;
                    }

                }
                if ($detailIndex != 0 && $result[$detailIndex]->inv_id == $result[$detailIndex - 1]->inv_id) {
                    $row[] = "";
                } else if($detailIndex != 0) {
                    $ongkir = isset($list->ongkir) ? format_money($list->ongkir) : 0;
                    $row[] = $ongkir;
                    $totalOngkir +=  isset($list->ongkir) ? $list->ongkir : 0;
                }

                $bankName = json_decode($list->bank_name);
                $bankNameStr = '';

                foreach($bankName as $name) {
                    $bankNameStr .= "<li>" . $name . "</li>";
                }

                $row[] = $bankNameStr;
                $row[] = $list->cashier;

                $data[] = $row;
                $totalProduk += $list->product ? str_replace(",", "", $list->subtotal) : 0;
                $totalTreatment += $list->treatment ? str_replace(",", "", $list->subtotal) : 0;
        }

        $footer =[];
        $footer[] = '';
        $footer[] = '';
        $footer[] = '';
        $footer[] = '';
        $footer[] = '';
        if (array_intersect(["OUTLET_SUPERVISOR"], json_decode(Auth::user()->level))) {
            $footer[] = "";
            $footer[] = "";
        }
        $footer[] = '';
        $footer[] = '';
        $footer[] = '';
        $footer[] = '';
        $footer[] = '';
        $footer[] = '';
        $footer[] = '<strong>TOTAL TREATMENT</strong>';
        $footer[] = '<strong>'.format_money($totalTreatment).'</strong>';
        $footer[] = '';
        $footer[] = '';
        $footer[] = '';
        $footer[] = '';
        $footer[] = '';
        $footer[] = '';
        $footer[] = '<strong>TOTAL PRODUK</strong>';
        $footer[] = '<strong>'.format_money($totalProduk).'</strong>';
        $footer[] = '<strong>'.format_money($totalOngkir).'</strong>';
        $footer[] = '';
        $footer[] = '';
        $data[] = $footer;

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function subtotalCollectionOutlet(Request $request)
    {
        $outlet_id = Outlet::first();
        $from = Carbon::now()->toDateString();
        $to = Carbon::now()->toDateString();

        if ($request->from && $request->to && $request->outlet) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
            $outlet_id = [];
            for ($i = 0; $i < count($request->outlet); $i++) {
                $outlet_id[] = $request->outlet[$i];
            }
        }

        $invoices = Invoice::whereBetween('inv_date', [$from, $to])
            ->where('void_invoice', 0)
            ->whereIn('outlet_id', $outlet_id)
            ->pluck('id_invoice')->toArray();
        // return $invoices;
        $data = array();
        $items = array("Product", "Treatment");
        // return $items;
        foreach($items as $item){
            $row = array();
            $row[] = $item;
            if($item == "Product"){
                $subtotal = InvoiceDetail::whereIn('inv_id', $invoices)
                            ->where('package_code', 'like', 'P%')
                            ->sum('current_price');

                $grandtotal = InvoiceDetail::whereIn('inv_id', $invoices)
                            ->where('package_code', 'like', 'P%')
                            ->sum('subtotal');
            }else{
                $subtotal = InvoiceDetail::whereIn('inv_id', $invoices)
                            ->where('package_code', 'like', 'T-%')
                            ->sum('current_price');

                $grandtotal = InvoiceDetail::whereIn('inv_id', $invoices)
                            ->where('package_code', 'like', 'T-%')
                            ->sum('subtotal');
            }
            // return $subtotal;
            $row[] = format_money($subtotal);
            $row[] = format_money($grandtotal);
            $data[] = $row;
        }


        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function cashierCollectionOutlet(Request $request)
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        // $outlet_id = Outlet::first();
        $from = Carbon::now()->toDateString();
        $to = Carbon::now()->toDateString();

        if ($request->from && $request->to && $request->outlet) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
            $outlet_id = $request->outlet;
        }

        // return ['from' => $from, 'to' => $to, 'outlet_id' => $outlet_id];

        $sql = '
            SELECT
                inv.staff_id, u.name, o.outlet_name, COUNT(inv.id_invoice) as created_invoice
            FROM
                invoices inv
            JOIN users u ON inv.staff_id = u.id
            JOIN outlets o ON inv.outlet_id = o.id_outlet
            WHERE
                inv.void_invoice = 0
                    AND inv.outlet_id IN (%s)
                    AND inv_date BETWEEN ? AND ?
                    GROUP BY inv.staff_id
                    ORDER BY outlet_name ASC, created_invoice DESC
        ';

        $result = DB::select(sprintf($sql, implode(', ', $outlet_id)), [$from, $to]);

        $data = [];
        foreach ($result as $cashier) {
            $row = [];
            $row[] = $cashier->name;
            $row[] = $cashier->outlet_name;
            $row[] = $cashier->created_invoice;

            $data[] = $row;

        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function grandtotalCollectionOutlet(Request $request)
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        // $outlet_id = Outlet::first();
        $from = Carbon::now()->toDateString();
        $to = Carbon::now()->toDateString();

        if ($request->from && $request->to && $request->outlet) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
            $outlet_id = [];
            for ($i = 0; $i < count($request->outlet); $i++) {
                $outlet_id[] = $request->outlet[$i];
            }
        }

        $invoices = Invoice::whereBetween('inv_date', [$from, $to])
            ->where('void_invoice', 0)
            ->whereIn('outlet_id', $outlet_id)
            ->pluck('id_invoice')->toArray();
        // return $invoices;
        $data = array();
        $bank = Bank::whereHas('bank_outlets', function ($q) use ($outlet_id) {
            return $q->whereIn('outlet_id', $outlet_id);
        })->get();
        // return $items;

        foreach($bank as $list){
            $row = array();
            $row[] = $list->bank_name;
            $total = Payment::whereIn('inv_id', $invoices)
                    ->where('bank_id', $list->id_bank)
                    ->sum('amount');
            $row[] = format_money($total);
            $data[] = $row;
        }


        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function totalCollectionOutlet(Request $request)
    {
        $outlet_id = Outlet::first();
        $from = Carbon::now()->toDateString();
        $to = Carbon::now()->toDateString();

        if ($request->from && $request->to && $request->outlet) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
            $outlet_id = [];
            for ($i = 0; $i < count($request->outlet); $i++) {
                $outlet_id[] = $request->outlet[$i];
            }
        }

        $invoices = Invoice::whereBetween('inv_date', [$from, $to])
            ->where('void_invoice', 0)
            ->whereIn('outlet_id', $outlet_id)
            ->pluck('id_invoice')->toArray();
        // return $invoices;
        $data = array();
        $row = array();
        $customers = Invoice::whereIn('id_invoice', $invoices)->get()->groupBy('customer_id')->count();
        // return $customers;
        $row[] = $customers;
        $total = Payment::whereIn('inv_id', $invoices)
                    ->sum('amount');
        $row[] = format_money($total);
        $data[] = $row;


        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function index_collection_payment()
    {
        $bank = Bank::all();
        $from = Carbon::parse(now())->format('d/m/Y');
        $to = Carbon::parse(now())->format('d/m/Y');
        $countBank = 0;
        $payment = [];
        $invoice = [];
        $detail_invoice = [];
        $outlet = Outlet::all();

        return view('report.collection.payment.index')
            ->with('payment', $payment)
            ->with('invoice', $invoice)
            ->with('detail_invoice', $detail_invoice)
            ->with('from', $from)
            ->with('to', $to)
            ->with('countBank', $countBank)
            ->with('outlet', $outlet)
            ->with('bank', $bank);
    }

    public function bank_payment_collection(Request $request)
    {
        $invoice = [];
        $outlet_id = [];
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        if ($request->bank && $request->outlet) {
            $bank_id = [];

            for ($i = 0; $i < count($request->bank); $i++) {
                $bank_id[] = $request->bank[$i];
            }

            for ($i = 0; $i < count($request->outlet); $i++) {
                $outlet_id[] = $request->outlet[$i];
            }

        }

        $data = array();
        foreach ($outlet_id as $list) {
            $row = [];
            $outletName = Outlet::where('id_outlet', $list)->pluck('outlet_name')->toArray();
            $row[] = $outletName;

            $invoice = Invoice::whereBetween('inv_date', [$from, $to])
            ->where('void_invoice', 0)
            ->where('outlet_id',$list)
            ->pluck('id_invoice')->toArray();
            $payment = Payment::whereIn('bank_id',$bank_id)->whereIn('inv_id', $invoice)->pluck('inv_id')->toArray();

            $row[] = count(array_unique($payment));

            $numbofsales = InvoiceDetail::whereIn('inv_id', $payment)->sum('subtotal');
            $row[] = format_money($numbofsales);

            $sumPayment = Payment::whereIn('bank_id',$bank_id)->whereIn('inv_id', $invoice)->sum('amount');
            $row[] = format_money($sumPayment);
            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function payment_type_collection(Request $request)
    {
        $payment = [];
        $bank_id = [];

        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        if ($request->bank) {
            $outlet_id = [];

            for ($i = 0; $i < count($request->bank); $i++) {
                $bank_id[] = $request->bank[$i];
            }

            // for ($i = 0; $i < count($request->outlet); $i++) {
            //     $outlet_id[] = $request->outlet[$i];
            // }

        }

        $data = array();
        foreach ($bank_id as $list) {
            $row = [];

            $bankName = Bank::where('id_bank', $list)->pluck('bank_name')->toArray();
            $bankCode = Bank::where('id_bank', $list)->pluck('bank_code')->toArray();

            $invoice = Invoice::whereBetween('inv_date', [$from, $to])
            ->where('void_invoice', 0)
            ->pluck('id_invoice')->toArray();
            $payment = Payment::where('bank_id',$list)->whereIn('inv_id', $invoice)->pluck('inv_id')->toArray();

            $row[] = $bankName;
            $row[] = $bankCode;
            $row[] = count(array_unique($payment));

            $countFilter = count($request->filter);
            for ($i = 0; $i < $countFilter; $i++) {
                if ($countFilter == 1) {
                    if ($request->filter[$i] == 1) {
                        $sumPayment = Payment::where('bank_id',$list)->whereIn('inv_id', $invoice)
                        ->whereNull('balance_id')
                        ->get();
                    }else{
                        $sumPayment = Payment::where('bank_id',$list)->whereIn('inv_id', $invoice)
                        ->whereNotNull('balance_id')
                        ->get();
                    }
                }else{
                    $sumPayment = Payment::where('bank_id',$list)->whereIn('inv_id', $invoice)
                    ->get();
                }
            }

            $total_balance = 0;
            $total_payment = 0;
            foreach ($sumPayment as $sum) {
                if ($sum->balance_id) {
                    $total_balance += str_replace(",", "", $sum->amount);
                }
                $total_payment += str_replace(",", "", $sum->amount);
            }
            $row[] = format_money($total_payment);
            $row[] = format_money($total_balance);

            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }
    public function detail_payment_collection(Request $request)
    {
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        // $payments=[];

        // if ($request->bank && $request->outlet) {
        //     $bank_id = [];
        //     $outlet_id = [];

        //     for ($i = 0; $i < count($request->bank); $i++) {
        //         $bank_id[] = $request->bank[$i];
        //     }

        //     for ($i = 0; $i < count($request->outlet); $i++) {
        //         $outlet_id[] = $request->outlet[$i];
        //     }

        //     $detail_invoice = Invoice::whereBetween('inv_date', [$from, $to])
        //     ->where('void_invoice', 0)
        //     ->whereIn('outlet_id' ,$outlet_id)
        //     ->pluck('id_invoice')
        //     ->toArray();
        //     $payments = Payment::whereIn('inv_id',$detail_invoice)->whereIn('bank_id',$bank_id);

        // }
        $outlets = [0];
        $banks = [0];
        if ($request->bank) {
            $banks = $request->bank;
        }

        if ($request->outlet) {
            $outlets = $request->outlet;
        }

        $sql = file_get_contents(__DIR__ . '/Queries/collectionByPaymentType.sql.tpl');
        $sql = sprintf($sql,implode(',', $banks));

        $result = DB::select($sql, [$from, $to]);

        $data = array();
        foreach ($result as $list) {

            $row = [];
            $row[] = "<a href='/customers/" . $list->id_invoice ."/detail' target='_blank'>".$list->inv_code.'</a>';
            $row[] = $list->izzibook_sync_status == 1 ? '<i class="fa fa-check fa-lg"></i>' : '';
            $row[] = $list->inv_date;
            $row[] = $list->outlet_name;
            $row[] = $list->induk_customer;
            $row[] = $list->full_name;
            $row[] = $list->received_by;
            $row[] = $list->bank_name;
            $row[] = $list->consultant_name;
            $row[] = $list->therapist_name;
            $row[] = $list->amount;
            $data[] = $row;
        }
        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function index_collection_redeem_point() {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
        // $outlet = Outlet::all();
        $from = Carbon::parse(now())->format('d-m-Y');
        $to = Carbon::parse(now())->format('d-m-Y');
        return view('report.collection.redeem.report')
            ->with('outlet', $outlet)
            ->with('from', $from)
            ->with('to', $to);
    }

    public function summary_collection_redeem_point(Request $request) {
        $outlet_id = Outlet::first();
        $from = Carbon::now()->toDateString();
        $to = Carbon::now()->toDateString();
        $outlet_id = [];

        if ($request->from && $request->to && $request->outlet) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
            for ($i = 0; $i < count($request->outlet); $i++) {
                $outlet_id[] = $request->outlet[$i];
            }
        }

        $invoice = InvoicePoint::with([
            'invoice_details' => function ($query) {
                $query->with(['product_point']);
            },
            'customer',
            'staff'
        ])
        ->whereBetween('created_at', [$from, $to])
        ->whereIn('outlet_id', $outlet_id)
        ->where('status_inv_point', 1)
        ->get();

        $data = [];
    }

    public function detail_collection_redeem_point(Request $request) {
        $outlet_id = Outlet::first();
        $from = Carbon::now()->toDateString();
        $to = Carbon::now()->toDateString();
        $outlet_id = [];

        if ($request->from && $request->to && $request->outlet) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
            for ($i = 0; $i < count($request->outlet); $i++) {
                $outlet_id[] = $request->outlet[$i];
            }
        }

        $from = $from . ' 00:00:00';
        $to   = $to   . ' 23:59:59';

        $invoice = InvoicePoint::with([
            'invoice_details' => function ($query) {
                $query->with(['product_point']);
            },
            'customer',
            'staff'
        ])
        ->whereBetween('created_at', [$from, $to])
        ->whereIn('outlet_id', $outlet_id)
        ->where('status_inv_point', 1)
        ->get();

        // dd($invoice[1]->invoice_details);
        $data = [];
        foreach($invoice as $inv) {
            foreach($inv->invoice_details as $detail_index => $inv_detail) {
                $row = [];
                if ($detail_index < 1) {
                    $row[] = "<a href='".url("customers/$inv->id_invoice_point/redeem-detail")."'>".$inv->inv_point_code."";
                    $row[] = $inv->customer->full_name . " | " . $inv->customer->induk_customer;
                    $row[] = $inv->outlet->outlet_name;
                    $row[] = carbon::createFromFormat('Y-m-d H:i:s', $inv->created_at)->format('d-m-Y H:i:s');
                } else {
                    $row[] = '';
                    $row[] = '';
                    $row[] = '';
                    $row[] = '';
                }

                $row[] = $inv_detail->product_point ? $inv_detail->product_point->product_point_name : '(deleted)';
                $row[] = $inv_detail->product_point ? $inv_detail->product_point->point : '(deleted)';
                $row[] = $inv_detail->product_point ? $inv_detail->qty : '(deleted)';
                $row[] = $inv_detail->product_point ? $inv_detail->product_point->point * $inv_detail->qty : '(deleted)';

                if ($detail_index < 1) {
                    $row[] = $inv->total;
                    $row[] = $inv->staff->name;
                    $row[] = $inv->remarks;
                } else {
                    $row[] = '';
                    $row[] = '';
                    $row[] = '';
                }
                $data[] = $row;
            }
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function index_collection_activity()
    {
        $activity = Activity::all();
        $group = ActivityGroup::all();
        $outlet = Outlet::all();
        $from = Carbon::parse(now())->format('d/m/Y');
        $to = Carbon::parse(now())->format('d/m/Y');
        $detail_invoice = [];

        return view('report.collection.activity.index')
            ->with('from', $from)
            ->with('to', $to)
            ->with('activity', $activity)
            ->with('group', $group)
            ->with('outlet', $outlet)
            ->with('detail_invoice', $detail_invoice);
    }

    public function collection_activity(Request $request)
    {
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }
        $outlets = [0];
        $activities = [0];

        if ($request->activity) {
            $activities = $request->activity;
        }

        if ($request->outlet) {
            $outlets = $request->outlet;
        }

        $sql = file_get_contents(__DIR__ . '/Queries/collectionByActivity.sql.tpl');
        $sql = sprintf($sql, implode(',', $outlets), implode(',', $activities));

        $result = DB::select($sql, [$from, $to]);
        // $detail_invoice = Invoice::whereBetween('inv_date', [$from, $to])
        //     ->where('void_invoice', 0);
        // if ($request->outlet) {
        //     $detail_invoice->whereIn('outlet_id', $request->outlet);
        // }
        // if ($request->activity) {
        //     $activity_id = $request->activity;
        //     $detail_invoice->with(['invoice_detail' => function ($query) use ($activity_id) {
        //         $query->whereIn('activity_id', $activity_id);
        //     }]);
        // }
        // $detail_invoice = $detail_invoice->get();
        $data = [];
        foreach ($result as $res) {
            $row = [];
            $row[] = '<a href = "' . route('invoice.print', $res->id_invoice) . '" target = "_blank" >' . $res->inv_code . '</a >';
            $row[] = $res->inv_date;
            $row[] = $res->outlet_name;
            $row[] = '<a href="' . route('customer.active', $res->customer_id) . '" target = "_blank">' . $res->induk_customer . '</a >';
            $row[] = $res->full_name;
            $row[] = $res->received_by;
            $row[] = $res->marketing_source_name;
            $row[] = $res->activity_name;
            $row[] = $res->consultant_name;
            $row[] = $res->therapist_name;
            $row[] = $res->item_sku;
            $row[] = $res->item_name;

            $itemType = 'package';

            if (substr($res->item_sku, 1, 1) == '-') {
                if (substr($res->item_sku, 0, 1) == 'T') {
                    $itemType = 'treatment';
                } else {
                    $itemType = 'product';
                }
            }

            $row[] = $itemType;

            $row[] = $res->nominal;
            $row[] = $res->discount_1;
            $row[] = $res->discount_2;
            $row[] = $res->type_line;
            $row[] = $res->qty;
            $row[] = $res->total;
            $row[] = $res->remarks;
            $row[] = '<a href="' . route('customer.active', $res->id_invoice) . '" class="btn btn-info btn-xs" target="_blank"><i class="fa fa-eye"></i></a>';
            $data[] = $row;
        }

        // foreach ($detail_invoice as $list) {
        //     foreach ($list->invoice_detail as $detail) {
        //         foreach ($detail->invoice_package as $package) {
        //             $row = array();
        //             if (count($list->log_convert) > 0) {
        //                 $row[] = '<a href = ' . route('invoice-convert.print', $list->id_invoice) . ' target = "_blank" >' . $list->inv_cod . '</a >';
        //             } else {
        //                 $row[] = '<a href = ' . route('invoice.print', $list->id_invoice) . ' target = "_blank" >' . $list->inv_code . '</a >';
        //             }
        //             $row[] = $list->inv_date;
        //             $row[] = $list->outlet->outlet_name;
        //             $row[] = '<a href = ' . route('customer.active', $list->id_invoice) . 'target = "_blank">' . $list->customer->induk_customer . '</a >';
        //             $row[] = $list->customer->full_name;
        //             $row[] = $list->user->username;
        //             $row[] = $list->marketing_source->marketing_source_name;
        //             $row[] = isset($detail->activity) ? $detail->activity->activity_name : 'NONE';
        //             $row[] = isset($list->consultant) ? $list->consultant->consultant_name : 'NONE';
        //             $row[] = $list->therapist->therapist_name;
        //             $row[] = $detail->package_code;

        //             if ($detail->package) {
        //                 $row[] = $package->product_treatment->product_treatment_name;
        //                 $row[] = 'Package';
        //             } else {
        //                 $row[] = $detail->product_treatment->product_treatment_name;
        //                 if (substr($detail->product_treatment->product_treatment_code, 0, 1) == 'T') {
        //                     $row[] = 'Treatment';
        //                 } else {
        //                     $row[] = 'Product';
        //                 }
        //             }
        //             $row[] = format_money($package->nominal);
        //             if ($detail->type_line == 0) {
        //                 $disc = '%';
        //             } else {
        //                 $disc = 'Rp';
        //             }
        //             $row[] = $detail->discount . $disc;
        //             $row[] = $package->qty + $package->used;
        //             $row[] = format_money(str_replace(",", "", $package->nominal) * ($package->qty + $package->used));
        //             $row[] = $list->remarks;
        //             $row[] = '<a href=' . route('customer.active', $list->id_invoice) . ' class="btn btn-info btn-xs" target="_blank"><i class="fa fa-eye"></i></a>';
        //             $data[] = $row;
        //         }
        //     }
        // }
        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function choose_activity(Request $request)
    {
        if ($request->group) {
            $activity = Activity::where('activity_group_id', $request->group)->get();
            foreach ($activity as $value) {
                echo "<option value='" . $value->id_activity . "' selected>" . $value->activity_name . "</option>";
            }
        }
    }

    public function index_collection_void_invoice()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $from = Carbon::parse(now())->format('d-m-Y');
        $to = Carbon::parse(now())->format('d-m-Y');
        return view('report.void-invoice.index')
            ->with('outlet', $outlet)
            ->with('from', $from)
            ->with('to', $to);
    }

    public function voidInvoiceCollectionOutlet(Request $request)
    {
        $outlet_id = Outlet::first();
        $from = Carbon::now()->toDateString() . " 00:00:00";
        $to = Carbon::now()->toDateString() . " 23:59:59";

        if ($request->from && $request->to && $request->outlet) {
            $from = Carbon::parse($request->from)->format('Y-m-d') . " 00:00:00";
            $to = Carbon::parse($request->to)->format('Y-m-d') . " 23:59:59";
            $outlet_id = [];
            for ($i = 0; $i < count($request->outlet); $i++) {
                $outlet_id[] = $request->outlet[$i];
            }
        }
        $detail_invoice = Invoice::whereBetween('updated_at', [$from, $to])
            ->where('void_invoice', 1)
            ->whereIn('outlet_id', $outlet_id)
            ->orderBy('id_invoice', 'ASC')
            ->get();
        $data = array();
        foreach ($detail_invoice as $list) {
            $row = array();
            if (count($list->log_convert) > 0) {
                $row[] = $list->inv_code;
            } else {
                $row[] = $list->inv_code;
            }

            $row[] = $list->inv_date;

            if ($list->status_inv == 1) {
                $row[] = 'Full';
            } else {
                $row[] = 'DP';
            }

            $row[] = $list->outlet->outlet_name;
            $row[] = '<a href=" ' . route("customers.show", $list->customer_id) . ' " target="_blank">' . $list->customer->induk_customer . '</a>';
            $row[] = $list->customer->full_name;
            $row[] = $list->user->username;

            $PaymentType = '';
            $paid = 0;
            foreach ($list->payment as $payment) {
                if (!$payment->balance_id) {
                    $PaymentType .= "&#10004;" . $payment->bank->bank_name . "<br>";
                    $paid += str_replace(",", "", $payment->amount);
                }
            }

            $row[] = $PaymentType;
            $row[] = isset($list->consultant) ? $list->consultant->consultant_name : 'NONE';
            $row[] = isset($list->therapist) ? $list->therapist->therapist_name : 'NONE';
            $row[] = isset($list->marketing_source) ? $list->marketing_source->marketing_source_name : '';
            $row[] = $list->total;
            $row[] = format_money($paid);
            $row[] = ($list->log_void_invoice->created_at)->format('d/m/Y');
            $row[] = $list->log_void_invoice->user->username;
            $row[] = $list->log_void_invoice->remarks;
            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function index_collection_void_usage()
    {
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();
        $from = Carbon::parse(now())->format('d-m-Y');
        $to = Carbon::parse(now())->format('d-m-Y');
        return view('report.void-usage.index')
            ->with('outlet', $outlet)
            ->with('from', $from)
            ->with('to', $to);
    }

    public function voidUsageCollectionOutlet(Request $request)
    {
        $outlet_id = Outlet::first();
        $from = Carbon::now()->toDateString() . " 00:00:00";
        $to = Carbon::now()->toDateString() . " 23:59:59";

        if ($request->from && $request->to && $request->outlet) {
            $from = Carbon::parse($request->from)->format('Y-m-d') . " 00:00:00";
            $to = Carbon::parse($request->to)->format('Y-m-d') . " 23:59:59";
            $outlet_id = [];
            for ($i = 0; $i < count($request->outlet); $i++) {
                $outlet_id[] = $request->outlet[$i];
            }
        }
        $detail_usage = Usage::whereBetween('updated_at', [$from, $to])
            ->where('status_usage', 1)
            ->whereIn('outlet_id', $outlet_id)
            ->orderBy('id_usage', 'ASC')
            ->get();
        $data = [];
        foreach($detail_usage as $list) {
            $row = [];
            $row[] = $list->usage_code;
            $row[] = $list->usage_date;
            $row[] = $list->outlet->outlet_name;
            $row[] = '<a href=" ' . route("customers.show", $list->customer_id) . ' " target="_blank">' . $list->customer->induk_customer . '</a>';
            $row[] = $list->customer->full_name;
            $row[] = $list->user->username;
            $row[] = isset($list->log_void_usage) ? Carbon::createFromFormat($list->log_void_usage->created_at)->format('d/m/Y') : '-';
            $row[] = isset($list->log_void_usage) ? $list->log_void_usage->user->username : '<i>system</i>';
            $row[] = isset($list->log_void_usage) ? $list->log_void_usage->remarks : '-';
            $data[] = $row;
        }
        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function indexCustomer()
    {
        $from = Carbon::parse(now())->format('d/m/Y');
        $to = Carbon::parse(now())->format('d/m/Y');
        $outlet_id = UserOutlet::where('user_id', Auth::user()->id)->pluck('outlet_id')->toArray();
        $outlet = Outlet::whereIn('id_outlet', $outlet_id)->get();

        return view('report.collection.customer.index', compact('from', 'to', 'outlet'));
    }

    public function dataCustomer(Request $request)
    {
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');

        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        $outletId = [];
        if ($request->outlet) {
            for ($i = 0; $i < count($request->outlet); $i++) {
                $outletId[] = $request->outlet[$i];
            }
        } else {
            $outletId[] = 0;
        }

        $sql = '
            SELECT
                c.full_name,
                c.phone,
                c.address,
                DATE_FORMAT(c.join_date, "%%d/%%m/%%Y") AS join_date,
                SUM(inv.total) AS sales_summary,
                SUM(inv.remaining) as outstanding_payment
            FROM
                customers c
                    JOIN
                invoices inv ON c.id_customer = inv.customer_id
                    JOIN
                customer_outlets co ON c.id_customer = co.customer_id
            WHERE
                inv.void_invoice = 0
                    AND co.outlet_id IN (%s)
                    AND inv.inv_date BETWEEN ? AND ?
            GROUP BY inv.customer_id ORDER BY sales_summary DESC
        ';

        $sql = sprintf($sql, implode(',', $outletId));

        $result = DB::select($sql, [$from, $to]);

        $data = [];
        foreach ($result as $index => $item) {
            $row = [];
            $row[] = $index + 1;
            $row[] = $item->full_name;
            if (array_intersect(["OUTLET_SUPERVISOR"], json_decode(Auth::user()->level))) {
            $row[] = $item->phone;
            $row[] = $item->address ?? '-';
            }
            $row[] = $item->join_date;
            $row[] = format_money($item->sales_summary);
            $row[] = format_money($item->outstanding_payment);

            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function dataMarketingCustomer(Request $request)
    {
        // dd($request->all());
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');

        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }

        $outletId = [];
        if ($request->outlet) {
            for ($i = 0; $i < count($request->outlet); $i++) {
                $outletId[] = $request->outlet[$i];
            }
        } else {
            $data = [];
            // $outletId[] = 0;
            return DataTables::of($data)->make(true);
        }
        if (count($outletId) < 1) {
            $data = [];
            return DataTables::of($data)->make(true);
        }
        $outletReq = $request->outlet;
        $result = CustomerModel::with([
                        'marketing_source',
                        'customer_outlet'
                    ])
                    ->whereBetween('join_date',[$from,$to])
                    ->whereHas('customer_outlet', function ($query) use ($outletReq) {
                        $query->whereIn('outlet_id',$outletReq);
                    })
                    ->get();

        $data = [];
        foreach ($result as $index => $item) {
            $row = [];
            $row[] = $index + 1;
            $row[] = $item->full_name;
            if (array_intersect(["OUTLET_SUPERVISOR"], json_decode(Auth::user()->level))) {
                $row[] = $item->phone;
                $row[] = $item->address ?? '-';
            }
            $row[] = $item->marketing_source->marketing_source_name ?? 'NONE';



            $data[] = $row;
        }

        return DataTables::of($data)->escapeColumns([])->make(true);
    }

    public function dataSummaryCustomer(Request $request)
    {
        $from = Carbon::parse(now())->format('Y-m-d');
        $to = Carbon::parse(now())->format('Y-m-d');
        if ($request->from && $request->to) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
        }
        $outletId = [];
        if ($request->outlet) {
            for ($i = 0; $i < count($request->outlet); $i++) {
                $outletId[] = $request->outlet[$i];
            }
        } else {
            $outletId[] = 0;
        }

        $data = [];
        $fileCustomerNewTotal = file_get_contents(__DIR__ . '/Queries/customerNewTotal.sql.tpl');
        $queryCustomerTotal = sprintf($fileCustomerNewTotal, implode(',', $outletId));
        $summary = DB::select($queryCustomerTotal, [
            'from_1'  => $from,
            'from_2'  => $from,
            'from_3'  => $from,
            'to_1'  => $to,
        ]);

        $result = $summary[0];
        $list = [
            ['NEW'],
            [$result->new_patients]
        ];

        $list2 = [
            ['EXISTING'],
            [$result->existing_patients]
        ];

        $data[] = $list;
        $data[] = $list2;

        return DataTables::of($data)->make(true);
    }
}

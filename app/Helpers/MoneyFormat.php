<?php
function format_money($number)
{
    $result = number_format($number);
    return $result;
}

function remove_format_money($number) {
    $res = str_replace(array('.', ','), "", $number);
    return $res;
}

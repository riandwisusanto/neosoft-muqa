<?php
use App\Models\Invoice;
use App\Models\InvoiceDetail;
use App\Models\InvoicePackage;
use App\Models\Item;
use App\Models\UserOutlet;
use Illuminate\Support\Facades\Auth;
use App\Models\Logistics\Item as LogisticsItem;
use App\Models\VItemStock;

use Carbon\carbon;



function outstanding_invoice()
{
    $data = Invoice::where('remaining', '>', 0)->where('void_invoice', 0)->get();
    return $data;
}

function expired()
{
    $month = date('Y-m-d', strtotime("+3 month", strtotime(now())));
    $start = Carbon::create($month)->startOfMonth();
    $end = Carbon::create($month)->endOfMonth();
    $invoicePackage = InvoicePackage::where('qty', '>', 0)->pluck('inv_detail_id')->toArray();
    $invoiceDetail = InvoiceDetail::whereIn('id_inv_detail', $invoicePackage)->pluck('inv_id')->toArray();
    $data = Invoice::whereIn('id_invoice', $invoiceDetail)->whereBetween('expired_date', [$start, $end])->get();

    return $data;
}

function buffer_stock()
{
    $bufferStock = LogisticsItem::whereRaw('sellable_stock <= buffer_stock')
            ->where('type', 'product')    
            ->get();
    return $bufferStock;
}

/*function item()
{
    $balance = VItemStock::where('item_code',0)->get('balance');
    $data = Item::where('buffer_stock','=',$balance)->get();
    return $data;
}
*/
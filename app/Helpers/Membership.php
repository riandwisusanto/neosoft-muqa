<?php
use App\Models\Invoice;
use App\Models\Customer;
use App\Models\Rank;

function rank($id)
{
    $summary = Invoice::where('customer_id', $id)
        ->where('void_invoice', 0)
        ->where('status_inv', 1)
        ->sum('total');

    $getRank = Rank::where('value_1', '<=', $summary)
    ->where('value_2', '>=', $summary)
    ->first();

    //dd($getRank->desc);
    return $getRank;
    //dd($getRank);
}

function start($id)
{
    $summary = Invoice::where('customer_id', $id)
        ->where('void_invoice', 0)
        ->where('status_inv', 1)
        ->sum('total');

    if ($summary >= 1 && $summary < 5000000) {
        $pangkat = 1;
    } elseif ($summary >= 5000000 && $summary < 9100000) {
        $pangkat = 2;
    } elseif ($summary >= 9100000 && $summary < 15100000) {
        $pangkat = 3;
    } elseif ($summary > 15100000) {
        $pangkat = 4;
    } else {
        $pangkat = 0;
    }

    //dd($pangkat);

    return $pangkat;
}

function member($pangkat)
{
    //$pangkat = Cek_Privilege($id_customer);
    if ($pangkat == 4) {
        $privilege = "PLATINUM";
    } elseif ($pangkat == 3) {
        $privilege = "GOLD";
    } elseif ($pangkat == 2) {
        $privilege = "SILVER";
    } elseif ($pangkat == 1) {
        $privilege = "BASIC";
    } else {
        $privilege = "NEW MEMBER";
    }

    return $privilege;
}

function PrivilegeBonus($pangkat)
{
    if ($pangkat == 4) {
        $privilege = "<strong>PLATINUM</strong> <ul><li>Entitled to get discounts of up to 25% for purchasing treatment series packages / 5 non package packages</li><li>Eligible to get a 5% Max discount for product purchases</li></ul>";
    } elseif ($pangkat == 3) {
        $privilege = "<strong>GOLD</strong> <ul><li>Entitled to get discounts of up to 20% for purchasing treatment series packages / 5 non package packages</li><li>There is no product discount</li></ul>";
    } elseif ($pangkat == 2) {
        $privilege = "<strong>SILVER</strong> <ul><li>Entitled to get discounts of up to 10% for purchasing treatment series packages / 5 non package packages</li><li>There is no product discount</li></ul>";
    } elseif ($pangkat == 1) {
        $privilege = "<strong>RED</strong> <ul><li>New Client will get a 5% discount when buying a series of non-package package</li><li>There is no product discount</li></ul>";
    } else {
        $privilege = "...";
    }

    return $privilege;
}

function point($id)
{
    $rank = Invoice::where('customer_id', $id)
        ->where('void_invoice', 0)
        ->where('status_inv', 1)
        ->sum('total');

    $agent = Invoice::join('agents', 'agents.id_agent', 'invoices.agent_id')
        ->join('customers', 'customers.induk_customer', 'agents.agent_code')
        ->where('void_invoice', 0)
        ->where('customers.id_customer', $id)
        ->sum('total');

    $sum_agent = ($agent * 0.1) / 25000;
    $sum_client = $rank / 25000;

    $result = $sum_agent + $sum_client;

    return $result;
}

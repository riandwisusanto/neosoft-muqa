<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('admin', function ($user) {
            return count(array_intersect(["ADMINISTRATOR", "SUPER_USER"], json_decode($user->level)));
        });

        Gate::define('admin-bm-spv-fo-therapist', function ($user) {
            return count(array_intersect(["ADMINISTRATOR", "SUPER_USER", "BRANCH_MANAGER", "THERAPIST", "DOKTER", "FRONTDESK", "CASHIER"], json_decode($user->level)));
        });

        Gate::define('all', function ($user) {
            return count(array_intersect(["FRONTDESK", "FINANCE", "DOKTER", "BRANCH_MANAGER", "CASHIER", "ADMINISTRATOR", "THERAPIST"], json_decode($user->level)));
        });

        Gate::define('customer', function ($user) {
            return count(array_intersect(["FRONTDESK", "OUTLET_SUPERVISOR", "FINANCE", "BRANCH_MANAGER", "DOKTER", "CASHIER", "ADMINISTRATOR", "THERAPIST"], json_decode($user->level)));
        });

        Gate::define('sales', function ($user) {
            return count(array_intersect(["CASHIER", "FRONTDESK", "FINANCE", "ADMINISTRATOR", "BRANCH_MANAGER","OUTLET_SUPERVISOR"], json_decode($user->level)));
        });

        Gate::define('appointment', function ($user) {
            return count(array_intersect(["FRONTDESK", "DOKTER", "ADMINISTRATOR", "BRANCH_MANAGER", "FINANCE", "OUTLET_SUPERVISOR", "THERAPIST"], json_decode($user->level)));
        });
        Gate::define('queue', function ($user) {
            return count(array_intersect(["FRONTDESK", "DOKTER", "ADMINISTRATOR", "BRANCH_MANAGER", "THERAPIST"], json_decode($user->level)));
        });
        Gate::define('doctor', function ($user) {
            return count(array_intersect(["FRONTDESK", "DOKTER", "ADMINISTRATOR", "BRANCH_MANAGER", "THERAPIST"], json_decode($user->level)));
        });
        
        Gate::define('pharmacy', function ($user) {
            return count(array_intersect(["PHARMACY"], json_decode($user->level)));
        });

        Gate::define('therapist', function ($user) {
            return count(array_intersect(["THERAPIST"], json_decode($user->level)));
        });

    }
}

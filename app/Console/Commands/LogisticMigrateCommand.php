<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ProductTreatment;
use App\Models\Logistics\Item;
use App\Models\Logistics\Mutation;

class LogisticMigrateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'logistic:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate product & treatment to logistic table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $olds = ProductTreatment::all();

        foreach ($olds as $old) {
            $type = substr($old->product_treatment_code, 0, 1) == 'T' ? 'service' : 'product';

            $itemData = [
                'category_id'           => 1,          
                'uom_id'                => 7,               
                'name'                  => $old->product_treatment_name,                 
                'brand'                 => 'WITHOUT BRAND',
                'unit_value'            => 1,
                'type'                  => $type,
                'cogs'                  => 0,
                'dispense_method'       => 'fifo',
                'sales_information_id'  => $old->id_product_treatment
            ];

            $item = Item::create($itemData);

            Mutation::create([
                'warehouse_origin_id'       => null,
                'warehouse_destination_id'  => null,
                'rack_origin_id'            => null,
                'rack_destination_id'       => null,
                'sales_invoice_id'          => null,
                'purchase_invoice_id'       => null,
                'item_id'                   => $item->id,
                'transdate'                 => now(),
                'transtype'                 => 'in',
                'transdesc'                 => 'BEGIN EMPTY',
                'balance'                   => 0,
                'qty'                       => 0
            ]);

            echo "Migrated {$old->product_treatment_name}\n";
        }

        echo "Task Finished";
    }
}

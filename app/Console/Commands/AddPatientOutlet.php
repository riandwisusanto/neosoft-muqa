<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Customer;
use App\Models\CustomerOutlet;

class AddPatientOutlet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'patient:outlet';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add default patient outlet';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "starting..\n";
        $target = Customer::whereDoesntHave('customer_outlet')->get();


        foreach ($target as $customer) {
            CustomerOutlet::create([
                'customer_id' => $customer->id_customer,
                'outlet_id' => 1
            ]);

            echo "outlet added for {$customer->full_name}\n";
        }

        echo "operation completed.\n";
    }
}

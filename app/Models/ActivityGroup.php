<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityGroup extends Model
{
    protected $table = 'activity_groups';
    protected $primaryKey = 'id_activity_group';

    public function activities()
    {
        return $this->hasMany('App\Models\Activity');
    }

    public function activity_groups()
    {
        return $this->belongsTo('App\Models\Activity', 'id_activity_group', 'activity_group_id');
    }
}

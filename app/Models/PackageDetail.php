<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PackageDetail extends Model
{
    protected $table = 'package_details';
    protected $primaryKey = 'id_package_detail';

    protected $fillable = ['package_id', 'product_id', 'qty', 'price'];

    public function product_treatment()
    {
        return $this->belongsTo('App\Models\ProductTreatment', 'product_id', 'id_product_treatment');
    }
}

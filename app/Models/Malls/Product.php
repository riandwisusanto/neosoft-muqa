<?php

namespace App\Models\Malls;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /** @var string $table table name */
    protected $table = "mall_products";

    /** @var array $fillable fillable fields. */
    protected $fillable = [
        'product_id',
        'discount_1',
        'discount_2',
        'discount_type',
        'weight_uom_id',
        'weight',
        'description',
        'informations'
    ];

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function posProduct()
    {
        return $this->belongsTo('App\Models\ProductTreatment', 'product_id', 'id_product_treatment');
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function weightUom()
    {
        return $this->belongsTo('App\Models\Uom', 'weight_uom_id', 'id');
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function media()
    {
        return $this->hasMany('App\Models\Malls\Media', 'mall_product_id');
    }
}

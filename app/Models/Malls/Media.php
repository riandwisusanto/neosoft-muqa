<?php

namespace App\Models\Malls;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    /** @var string $table table name */
    protected $table = "mall_media";

    /** @var Type $var description */
    protected $fillable = [
        'mall_product_id',
        'title',
        'url'
    ];
}

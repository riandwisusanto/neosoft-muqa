<?php
 
namespace App\Models;
 
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
 
class Queue extends Model
{
   protected $table = 'queues';
   protected $primaryKey = 'id_queue';
 
   public static function generateQueueNumber()
   {
    $now = Carbon::now()->format('Y-m-d');
    $array = Queue::select('id_queue','come_date','queue_number')
        ->where('come_date', $now)
        ->latest('id_queue')
        ->first();
 
       if (isset($array)) {
           $number = $array->queue_number + 1;
       } else {
           $number = 01;
       }
 
       return $number;
 
 
   }
 
   public function customer()
   {
       return $this->belongsTo('App\Models\Customer', 'customer_id', 'id_customer');
   }

   public function outlet()
   {
       return $this->belongsTo('App\Models\Outlet', 'outlet_id', 'id_outlet');
   }
}
 
 


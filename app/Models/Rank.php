<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rank extends Model
{
    protected $table = 'ranks';
    protected $primaryKey = 'id_rank';

    public function getNameAttribute($name)
    {
        return strtoupper($name);
    }

}

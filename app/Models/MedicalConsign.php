<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MedicalConsign extends Model
{
    protected $fillable = [];
    
    public function customer() {
        return $this->belongsTo('App\Models\Customer','customer_id','id_customer');
    }
}

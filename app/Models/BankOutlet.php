<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankOutlet extends Model
{
    protected $table = 'bank_outlets';
    protected $primaryKey = 'id_BankOutlet';

    protected $fillable = ['outlet_id', 'bank_id'];


    public function outlet()
    {
        return $this->belongsTo(Outlet::class, 'outlet_id');
    }
}

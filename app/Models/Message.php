<?php
 
namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;
 
class Message extends Model
{
   protected $table = 'messages';
   protected $primaryKey = 'id_message';
   protected $fillable = ['message', 'image'];

 
   public function detail()
   {
       return $this->hasMany('App\Models\MessageDetail', 'message_id', 'id_message');
   }
 
}

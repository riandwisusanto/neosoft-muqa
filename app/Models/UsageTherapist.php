<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsageTherapist extends Model
{
    protected $table = 'usage_therapists';
    protected $primaryKey = 'id_usage_therapist';

    protected $fillable = ['usage_detail_id', 'therapist_id'];

    public function therapist()
    {
        return $this->belongsTo('App\Models\Therapist', 'therapist_id', 'id_therapist');
    }

    public function usage_detail()
    {
        return $this->belongsTo('App\Models\UsageDetail', 'usage_detail_id', 'id_usage_detail');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerConsultant extends Model
{
    protected $table = 'customer_consultants';
    protected $primaryKey = 'id_customer_consultant';
}

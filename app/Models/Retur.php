<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\Updater;

use Carbon\Carbon;

class Retur extends Model
{
    use Updater;

    protected $fillable = [
        'retur_num',
        'retur_date',
        'warehouse_id',
        'supplier_id',
        'description',
        'invoice_no',
        'invoice_date',
        'pay_method',
        'due_date',
        'created_uid',
        'updated_uid',
    ];

    public function warehouse(){
        return $this->belongsTo(Warehouse::class, 'warehouse_id');
    }

    public function supplier(){
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }

    public function purchase_order(){
        return $this->belongsTo(PurchaseOrder::class, 'purchase_order_id');
    }

    public function retur_details(){
        return $this->hasMany(ReturDetail::class);
    }

    public static function generateCode(){
        $now    = Carbon::now();
        $prefix = 'RT-'.$now->isoFormat('YYMM');
        $num    = 0;
        $data   = self::select('retur_num')
            ->whereMonth('created_at', $now->month)
            ->whereYear('created_at', $now->year)
            ->orderBy('retur_num', 'desc')
            ->first();

        if($data) $num = (int) substr($data->retur_num, strlen($prefix), 4);

        $num++;
        return $prefix.str_pad($num, 4, '0', STR_PAD_LEFT);
    }

    protected static function boot(){
        parent::boot();
        static::creating(function($model){
            $model->retur_num = self::generateCode();
        });

        static::deleting(function ($model) {
            $model->retur_details()->delete();
        });
    }
}

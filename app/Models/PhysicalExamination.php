<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhysicalExamination extends Model
{
    protected $table = 'physical_examinations';
    protected $primaryKey = 'id_physical_examination';
    protected $dates = ['examination_date'];
}

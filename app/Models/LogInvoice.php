<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogInvoice extends Model
{
    protected $table = 'log_invoices';
    protected $primaryKey = 'id_log_invoice';

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'staff_id', 'id');
    }

    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoice', 'invoice_id', 'id_invoice');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Consultant extends Model
{
    protected $table = 'consultants';
    protected $primaryKey = 'id_consultant';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'join_date' => 'datetime:d/m/Y',
    ];


    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function details()
    {
        return $this->hasMany('App\Models\ConsultantDetail', 'consultant_id', 'id_consultant');
    }

    public function consultant()
    {
        return $this->belongsTo('App\Models\Consultant', 'consultant_id', 'id_consultant');
    }

    public function consultant_outlets()
    {
        return $this->hasMany(ConsultantDetail::class, 'consultant_id');
    }


    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoice', 'id_consultant', 'consultant_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConsultantDetail extends Model
{
    /** @var Type $var description */
    protected $table = 'consultant_details';

    /** @var Type $var description */
    protected $primaryKey = 'consultant_detail_id';

    /** @var Type $var description */
    protected $fillable = ['outlet_id', 'consultant_id'];

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function outlet()
    {
        return $this->belongsTo(Outlet::class, 'outlet_id');
    }
}

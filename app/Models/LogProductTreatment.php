<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogProductTreatment extends Model
{
    //

    public function edited_by()
    {
        return $this->belongsTo(User::class, 'staff_id');
    }
}

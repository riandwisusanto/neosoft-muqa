<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnamnesaDetail extends Model
{
    protected $table = 'anamnesa_details';
    protected $primaryKey = 'id_anamnesa_detail';

    protected $fillable = ['anamnesa_id', 'product_id', 'qty', 'price', 'total', 'scenario_id', 'adjustment', 'notes'];

    public function product_treatment()
    {
        return $this->belongsTo('App\Models\ProductTreatment', 'product_id', 'id_product_treatment');
    }
    
    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function boms()
    {
        return $this->hasMany('App\Models\AnamnesaBom', 'anamnesa_detail_id');
    }

    public function invoice_package() {
        return $this->hasOne('App\Models\InvoicePackage','anamnesa_detail_id','id_anamnesa_detail');
    }
}

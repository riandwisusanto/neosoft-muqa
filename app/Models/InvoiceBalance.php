<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class InvoiceBalance extends Model
{
    protected $table = 'invoice_balances';
    protected $primaryKey = 'id_balance';

    public static function generateBalanceCode($outletId)
    {
        $now = Carbon::now();
        $outlet = Outlet::find($outletId);
        $array = InvoiceBalance::whereMonth('created_at', $now->month)
            ->whereYear('created_at', $now->year)
            ->where('outlet_id', $outletId)
            ->latest('balance_code')->first();

        if ($array) {
            $number = substr($array->balance_code, -4) + 1;
        } else {
            $number = 0001;
        }

        $idinvcode = $outlet->outlet_code . "-B-" . $now->isoFormat('YYYY') . $now->isoFormat('MM') . "-" . sprintf('%04d', $number);
        return $idinvcode;
    }

    public function outlet()
    {
        return $this->belongsTo('App\Models\Outlet', 'outlet_id', 'id_outlet');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id', 'id_customer');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'staff_id', 'id');
    }

    public function payment()
    {
        return $this->hasMany('App\Models\Payment', 'balance_id', 'id_balance');
    }

    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoice', 'invoice_id', 'id_invoice');
    }

    public function invoice_detail()
    {
        return $this->hasMany('App\Models\InvoiceDetail', 'inv_id', 'invoice_id');
    }

    public function log_void_balance()
    {
        return $this->hasOne('App\Models\LogVoidBalance', 'balance_id', 'id_balance');
    }

    public function setOutstandingAttribute($price)
    {
        $this->attributes['outstanding'] = str_replace(",", "", $price);
    }

    public function getOutstandingAttribute($price)
    {
        return format_money($price);
    }

    public function getBalanceDateAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }
}

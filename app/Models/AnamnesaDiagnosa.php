<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnamnesaDiagnosa extends Model
{
    protected $table = 'anamnesa_diagnosas';
    protected $primaryKey = 'id';

    protected $fillable = ['anamnesa_id', 'diagnosa_id'];
}

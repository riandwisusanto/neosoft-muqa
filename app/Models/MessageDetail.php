<?php
 
namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;
 
class MessageDetail extends Model
{
   protected $table = 'message_details';
   protected $primaryKey = 'id_message_details';
 
   public function customer()
   {
       return $this->belongsTo('App\Models\Customer', 'customer_id', 'id_customer');
   }
 
}

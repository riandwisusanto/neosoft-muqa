<?php

namespace App\Models\Logistics;

use Illuminate\Database\Eloquent\Model;

class WarehouseMutation extends Model
{

    /** @var Type $var description */
    protected $fillable = [
        'code',
        'warehouse_destination_id',
        'warehouse_origin_id',
        'purchase_order_id',
        'created_by',
        'remarks',
        'sender_id',
        'transdate'
    ];

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function details()
    {
        return $this->hasMany('App\Models\Logistics\WarehouseMutationDetail', 'warehouse_mutation_id');
    }

    /**
     * Generate Code
     * 
     * @return string
     */
    public static function generateCode()
    {
        $now = now();
        $array = WarehouseMutation::whereMonth('created_at', $now->month)
            ->whereYear('created_at', $now->year)
            ->latest('code')->first();

        $number = 0001;
       
        if ($array) {
            $number = substr($array->code, -4) + 1;
        }

        $code = "WM-" . $now->isoFormat('YY') . $now->isoFormat('MM') . sprintf('%04d', $number);
        return $code;
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function warehouseOrigin()
    {
        return $this->belongsTo('App\Models\Warehouse', 'warehouse_origin_id', 'id_warehouse');
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function warehouseDestination()
    {
        return $this->belongsTo('App\Models\Warehouse', 'warehouse_destination_id', 'id_warehouse');
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function outlet()
    {
        return $this->belongsTo('App\Models\Outlet', 'outlet_id', 'id_outlet');
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function purchaseOrder()
    {
        return $this->belongsTo('App\Models\Logistics\Purchases\Order', 'purchase_order_id');
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function creator()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function sender()
    {
        return $this->belongsTo('App\Models\User', 'sender_id');
    }
}

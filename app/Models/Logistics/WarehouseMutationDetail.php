<?php

namespace App\Models\Logistics;

use Illuminate\Database\Eloquent\Model;

class WarehouseMutationDetail extends Model
{

    /** @var Type $var description */
    protected $fillable = [
        'warehouse_mutation_id',
        'order_detail_id',
        'item_id',
        'delivered_qty'
    ];

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function item()
    {
        return $this->belongsTo('App\Models\Logistics\Item', 'item_id');
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function warehouseMutation()
    {
        return $this->belongsTo('App\Models\Logistics\WarehouseMutation', 'warehouse_mutation_id');
    }
}

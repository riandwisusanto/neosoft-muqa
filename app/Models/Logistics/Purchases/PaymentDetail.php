<?php

namespace App\Models\Logistics\Purchases;

use Illuminate\Database\Eloquent\Model;

class PaymentDetail extends Model
{
    /** @var string $table table name */
    protected $table = 'purchase_payment_details';

    /** @var array $fillable fillable fields */
    protected $fillable = [
        'payment_id',
        'invoice_id',
        'paidamount',
        'discount',
        'discount_type'
    ];

    /**
     * Header.
     *
     * @return Model
     **/
    public function payment()
    {
        return $this->belongsTo('App\Models\Logistics\Purchases\Payment', 'payment_id');
    }

    /**
     * Invoic .
     *
     * @return Model
     **/
    public function invoice()
    {
        return $this->belongsTo('App\Models\Logistics\Invoice', 'invoice_id');
    }
}

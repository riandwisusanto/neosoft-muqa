<?php

namespace App\Models\Logistics\Purchases;

use Illuminate\Database\Eloquent\Model;

class ReturnDetail extends Model
{
    /** @var array fillable fields */
    protected $fillable = [
        'return_id',
        'order_detail_id',
        'item_id',
        'return_qty'
    ];

    /**
     * Item.
     *
     * @return Model
     **/
    public function item()
    {
        return $this->belongsTo('App\Models\Logistics\Item', 'item_id');
    }

    /**
     * Order detail.
     *
     * @return Model
     **/
    public function orderDetail()
    {
        return $this->belongsTo('App\Models\Logistics\OrderDetail', 'order_detail_id');
    }

    /**
     * Return (Header).
     *
     * @return Model
     **/
    public function return()
    {
        return $this->belongsTo('App\Models\Logistics\PurchaseReturn', 'return_id');
    }
}

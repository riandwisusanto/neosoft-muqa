<?php

namespace App\Models\Logistics\Purchases;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetail extends Model
{
    /** @var string table name */
    protected $table = 'purchase_invoice_details';

    /** @var array fillable fields */
    protected $fillable = [
        'invoice_id',
        'order_detail_id',
        'item_id',
        'qty',
        'price',
        'subtotal',
        'discount',
        'discount_type'
    ];

    /**
     * Item.
     *
     * @return Model
     **/
    public function item()
    {
        return $this->belongsTo('App\Models\Logistics\Item', 'item_id');
    }

    /**
     * Order detail.
     *
     * @return Model
     **/
    public function orderDetail()
    {
        return $this->belongsTo('App\Models\Logistics\OrderDetail', 'order_detail_id');
    }

    /**
     * Invoice (Header).
     *
     * @return Model
     **/
    public function invoice()
    {
        return $this->belongsTo('App\Models\Logistics\Invoice', 'invoice_id');
    }
}

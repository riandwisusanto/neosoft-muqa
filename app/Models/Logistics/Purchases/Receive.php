<?php

namespace App\Models\Logistics\Purchases;

use Illuminate\Database\Eloquent\Model;

class Receive extends Model
{
    /** @var array fillable fields */
    protected $fillable = [
        'supplier_id',
        'received_by',
        'transdate',
        'remarks'
    ];

    protected $dates = ['transdate'];


    /**
     * Generate Code
     * 
     * @return string
     */
    public static function generateCode()
    {
        $now = now();
        $array = Receive::whereMonth('created_at', $now->month)
                            ->whereYear('created_at', $now->year)
                            ->latest('code')->first();

        $number = 0001;
       
        if ($array) {
            $number = substr($array->code, -4) + 1;
        }

        $code = "RI-" . $now->isoFormat('YY') . $now->isoFormat('MM') . sprintf('%04d', $number);
        return $code;
    }

    /**
     * Supplier.
     *
     * @return Model
     **/
    public function supplier()
    {
        return $this->belongsTo('App\Models\Supplier', 'supplier_id', 'id_supplier');
    }

    public function item()
    {
        return $this->belongsTo('App\Models\Logistics\Item', 'item_id','id');
    }
    /**
     * User.
     *
     * @return Model
     **/
    public function receiver()
    {
        return $this->belongsTo('App\Models\User', 'received_by');
    }

     /**
     * Receive details.
     *
     * @return Model
     **/
    public function details()
    {
        return $this->hasMany('App\Models\Logistics\Purchases\ReceiveDetail', 'receive_id');
    }
}

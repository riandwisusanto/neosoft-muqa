<?php

namespace App\Models\Logistics\Purchases;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /** @var array fillable fields */
    protected $fillable = [
        'supplier_id',
        'term_id',
        'payment_method_id',
        'warehouse_id',
        'transdate',
        'duedate',
        'total',
        'status',
        'remarks'
    ];
    protected $dates = ['transdate'];

    /**
     * Generate Code
     * 
     * @return string
     */
    public static function generateCode()
    {
        $now = now();
        $array = Order::whereMonth('created_at', $now->month)
            ->whereYear('created_at', $now->year)
            ->latest('code')->first();

        $number = 0001;
       
        if ($array) {
            $number = substr($array->code, -4) + 1;
        }

        $code = "PO-" . $now->isoFormat('YY') . $now->isoFormat('MM') . sprintf('%04d', $number);
        return $code;
    }

    /**
     * Supplier.
     *
     * @return Model
     **/
    public function supplier()
    {
        return $this->belongsTo('App\Models\Supplier', 'supplier_id', 'id_supplier');
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function warehouseMutation()
    {
        return $this->hasOne('App\Models\Logistics\WarehouseMutation', 'purchase_order_id');
    }

    /**
     * Supplier.
     *
     * @return Model
     **/
    public function warehouse()
    {
        return $this->belongsTo('App\Models\Warehouse', 'warehouse_id', 'id_warehouse');
    }

    /**
     * Term.
     *
     * @return Model
     **/
    public function term()
    {
        return $this->belongsTo('App\Models\Term', 'term_id');
    }

    /**
     * Payment method.
     *
     * @return Model
     **/
    public function paymentMethod()
    {
        return $this->belongsTo('App\Models\Bank', 'payment_method_id', 'id_bank');
    }

    /**
     * Order details.
     *
     * @return Model
     **/
    public function details()
    {
        return $this->hasMany('App\Models\Logistics\Purchases\OrderDetail', 'order_id');
    }
}

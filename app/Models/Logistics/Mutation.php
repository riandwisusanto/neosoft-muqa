<?php

namespace App\Models\Logistics;

use Illuminate\Database\Eloquent\Model;

class Mutation extends Model
{
    /** @var array fillable fields */
    protected $fillable = [
        'warehouse_origin_id',
        'warehouse_destination_id',
        'rack_origin_id',
        'rack_destination_id',
        'material_release_id',
        'sales_invoice_id',
        'purchase_invoice_id',
        'finished_product_id',
        'adjustment_id',
        'item_id',
        'transdate',
        'transtype',
        'price',
        'cogs',
        'ref_id',
        'balance',
        'transdesc',
        'qty'
    ];

    /**
     * Warehouse origin.
     *
     * @return Model
     **/
    public function warehouseOrigin()
    {
        return $this->belongsTo('App\Models\Warehouse', 'warehouse_origin_id', 'id_warehouse');
    }

    /**
     * Warehouse destination.
     *
     * @return Model
     **/
    public function warehouseDestination()
    {
        return $this->belongsTo('App\Models\Warehouse', 'warehouse_destination_id', 'id_warehouse');
    }

    /**
     * Rack origin.
     *
     * @return Model
     **/
    public function rackOrigin()
    {
        return $this->belongsTo('App\Models\Rack', 'rack_origin_id', 'id_rack');
    }

    /**
     * Rack destination.
     *
     * @return Model
     **/
    public function rackDestination()
    {
        return $this->belongsTo('App\Models\Rack', 'rack_destination_id', 'id_rack');
    }

    /**
     * Sales invoice.
     *
     * @return Model
     **/
    public function salesInvoice()
    {
        return $this->belongsTo('App\Models\Invoice', 'sales_invoice_id', 'id_invoice');
    }

    /**
     * Purchase order.
     *
     * @return Model
     **/
    public function purchaseOrder()
    {
        return $this->belongsTo('App\Models\Logistics\Purchases\Order', 'purchase_invoice_id');
    }

    /**
     * Inventory adjustment.
     *
     * @return Model
     **/
    public function adjustment()
    {
        return $this->belongsTo('App\Models\Logistics\InventoryAdjustment', 'adjustment_id');
    }

    /**
     * Inventory adjustment.
     *
     * @return Model
     **/
    public function release()
    {
        return $this->belongsTo('App\Models\Manufactures\MaterialRelease', 'material_release_id');
    }

    /**
     * Inventory adjustment.
     *
     * @return Model
     **/
    public function finishedProduct()
    {
        return $this->belongsTo('App\Models\Manufactures\FinishedProduct', 'finished_product_id');
    }

    /**
     * Item.
     *
     * @return Model
     **/
    public function item()
    {
        return $this->belongsTo('App\Models\Logistics\Item');
    }

    /**
     * Usage
     */
    public function usage() {
        return $this->belongsTo('App\Models\Usage', 'sales_invoice_id', 'id_usage');
    }
}

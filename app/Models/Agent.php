<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    protected $table = 'agents';
    protected $primaryKey = 'id_agent';

    public static function generateAgentCode()
    {
        $now = Carbon::now();
        $array = Agent::whereMonth('created_at', $now->month)
        ->whereYear('created_at', $now->year)
        ->get()
        ->toArray();

        if ($array == []) {
            $number = 001;
            $idinvcode =
                "EA-".$now->isoFormat('YYYY').
                $now->isoFormat('MM').
                sprintf('%03d', $number);
        } else {
            $number = substr(end($array)['agent_code'], -4)+1;
            $idinvcode =
                "EA-".$now->isoFormat('YYYY').
                $now->isoFormat('MM').
                sprintf('%03d', $number);
        }
        return $idinvcode;
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'agent_code', 'induk_customer');
    }
}

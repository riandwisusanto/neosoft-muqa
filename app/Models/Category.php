<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Updater;

class Category extends Model
{
    use Updater;

    protected $primaryKey = 'id_category';
    protected $fillable = [
        'name',
        'is_active',
        'created_by',
        'updated_by',
    ];

    public function items(){
        return $this->hasMany(Item::class, 'category_id');
    }
}

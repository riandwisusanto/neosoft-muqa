<?php

namespace App\Models;

use Carbon\carbon;
use Illuminate\Database\Eloquent\Model;
use DB;

class Invoice extends Model
{
    protected $table = 'invoices';
    protected $primaryKey = 'id_invoice';

    public function getInvDateAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

    public function getExpiredDateAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

    // public function setInvDateAttribute($date){
    //     return Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    // }

    // public function setExpiredDateAttribute($date){
    //     return Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    // }

    public static function generateInvoiceCode($outlet_id)
    {
        $now = Carbon::now();
        $outlet = Outlet::find($outlet_id);
        $array = Invoice::where('outlet_id', $outlet_id)
            ->whereMonth('created_at', $now->month)
            ->whereYear('created_at', $now->year)
            ->latest('inv_code')->first();

        if ($array) {
            $number = substr($array->inv_code, -4) + 1;
        } else {
            $number = 0001;
        }

        $idinvcode =
        $outlet->outlet_code . "-I-" . $now->isoFormat('YYYY') . $now->isoFormat('MM') . "-" . sprintf('%04d', $number);
        return $idinvcode;
    }

    public static function cpoReportBalance($request)
    {
        $from = Carbon::now()->toDateString();
        $to = Carbon::now()->toDateString();

        if ($request->from && $request->to && $request->outlet) {
            $from = Carbon::parse($request->from)->format('Y-m-d');
            $to = Carbon::parse($request->to)->format('Y-m-d');
            $outlet_id = [];
            for ($i = 0; $i < count($request->outlet); $i++) {
                $outlet_id[] = $request->outlet[$i];
            }
        } else {
            $outlets = Outlet::first();
            $outlet_id[] = $outlets->id_outlet;
        }

        $invoice_balances = DB::select(DB::raw("
            SELECT
                id_invoice, inv_code, inv_date, customer_id, total, status_inv, 'id_balance', 'status_balance', therapist_id
            from
                invoices
            where
                inv_date between '$from' and '$to'
                and void_invoice = 0
                and outlet_id in (" . implode(',',$outlet_id) . ")
            UNION ALL
            SELECT
                invoice_id, balance_code, balance_date,  customer_id, outstanding, 'status_inv', id_balance, status_balance, 'therapist_id'
            FROM
                invoice_balances
            where
                balance_date between '$from' and '$to'
                and outlet_id in (" . implode(',',$outlet_id) . ")
                and status_balance = 0
            order by 1,2
        "));

        return $invoice_balances;
    }

    public function outlet()
    {
        return $this->belongsTo('App\Models\Outlet', 'outlet_id', 'id_outlet');
    }

    public function marketing_source()
    {
        return $this->belongsTo('App\Models\MarketingSource', 'marketing_source_id', 'id_marketing_source');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id', 'id_customer');
    }

    public function consultant()
    {
        return $this->belongsTo('App\Models\Consultant', 'consultant_id', 'id_consultant');
    }

    public function therapist()
    {
        return $this->belongsTo('App\Models\Therapist', 'therapist_id', 'id_therapist');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'staff_id', 'id');
    }

    public function payment()
    {
        return $this->hasMany('App\Models\Payment', 'inv_id', 'id_invoice');
    }

    public function invoice_detail()
    {
        return $this->hasMany('App\Models\InvoiceDetail', 'inv_id', 'id_invoice');
    }

    public function log_convert()
    {
        return $this->hasMany('App\Models\LogConvert', 'to_invoice_id', 'inv_code');
    }

    public function log_void_invoice()
    {
        return $this->hasOne('App\Models\LogVoidInvoice', 'inv_id', 'id_invoice');
    }

    public function getTotalAttribute($price)
    {
        return format_money($price);
    }

    public function getRemainingAttribute($price)
    {
        return format_money($price);
    }

    public function usage_detail()
    {
        return $this->belongsTo('App\Models\UsageDetail', 'inv_code', 'inv_code');
    }

    public function agent()
    {
        return $this->belongsTo('App\Models\Agent', 'agent_id', 'id_agent');
    }

    public function setTotalAttribute($price)
    {
        $this->attributes['total'] = str_replace(array('.', ','), "", $price);
    }

    public function setRemainingAttribute($price)
    {
        $this->attributes['remaining'] = str_replace(array('.', ','), "", $price);
    }
}

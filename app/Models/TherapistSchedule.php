<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TherapistSchedule extends Model
{
    protected $primaryKey = 'id_therapist_schedule';
    protected $fillable = [
        'therapist_id',
        'day',
        'start_time',
        'end_time'
    ];
    public function therapist()
    {
        return $this->belongsTo('App\Models\Therapist', 'therapist_id', 'id_therapist');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DoctorReviewImage extends Model
{
    protected $table = "dr_img";

    protected $fillable = [
        'customer_id',
        'usage_detail_id',
        'staff_id',
        'date_in',
        'desc',
        'remarks',
        'folder'
    ];

    public function details()
    {
        return $this->hasMany('App\Models\DoctorReviewImageDetail', 'dr_img_id');
    }
}

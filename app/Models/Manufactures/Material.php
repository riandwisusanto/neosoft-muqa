<?php

namespace App\Models\Manufactures;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    /** @var array fillable fields */
    protected $fillable = [
        'work_order_detail_id',
        'raw_item_id',
        'uom_id',
        'qty',
        'unit_value'
    ];

    /**
     * Raw item.
     *
     * @return Model
     **/
    public function rawItem()
    {
        return $this->belongsTo('App\Models\Logistics\Item', 'raw_item_id');
    }

    /**
     * Raw item.
     *
     * @return Model
     **/
    public function workOrderDetail()
    {
        return $this->belongsTo('App\Models\Manufactures\WorkOrderDetail', 'work_order_detail_id');
    }

    /**
     * Unit of Measurement.
     *
     * @return Model
     **/
    public function uom()
    {
        return $this->belongsTo('App\Models\Uom', 'uom_id', 'id');
    }

    /**
     * Unit of Measurement.
     *
     * @return Model
     **/
    public function releases()
    {
        return $this->hasMany('App\Models\Manufactures\MaterialRelease', 'material_id');
    }
}

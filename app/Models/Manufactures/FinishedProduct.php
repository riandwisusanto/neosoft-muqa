<?php

namespace App\Models\Manufactures;

use Illuminate\Database\Eloquent\Model;

class FinishedProduct extends Model
{
    /** @var array fillable fields */
    protected $fillable = [
        'checked_at',
        'created_by',
        'checked_by',
        'item_id',
        'work_order_detail_id',
        'qty',
        'marked_as',
        'warehouse_destination_id',
        'rack_destination_id',
    ];

    /**
     * Generate Code
     * 
     * @return string
     */
    public static function generateCode()
    {
        $now = now();
        $array = FinishedProduct::whereMonth('created_at', $now->month)
            ->whereYear('created_at', $now->year)
            ->latest('code')->first();

        $number = 0001;
       
        if ($array) {
            $number = substr($array->code, -4) + 1;
        }

        $code = "FP-" . $now->isoFormat('YY') . $now->isoFormat('MM') . sprintf('%04d', $number);
        return $code;
    }

    /**
     * Item.
     *
     * @return Model
     **/
    public function item()
    {
        return $this->belongsTo('App\Models\Logistics\Item', 'item_id');
    }

    /**
     * User.
     *
     * @return Model
     **/
    public function checker()
    {
        return $this->belongsTo('App\Models\User', 'checked_by');
    }

    /**
     * Warehouse destination.
     *
     * @return Model
     **/
    public function warehouseDestination()
    {
        return $this->belongsTo('App\Models\Warehouse', 'warehouse_destination_id', 'id_warehouse');
    }

    /**
     * Rack destination.
     *
     * @return Model
     **/
    public function rackDestination()
    {
        return $this->belongsTo('App\Models\Rack', 'rack_destination_id', 'id_rack');
    }

    /**
     * Raw item.
     *
     * @return Model
     **/
    public function workOrderDetail()
    {
        return $this->belongsTo('App\Models\Manufactures\WorkOrderDetail', 'work_order_detail_id');
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function materials()
    {
        return $this->hasMany('App\Models\Manufactures\Material', 'work_order_detail_id', 'work_order_detail_id');
    }
}

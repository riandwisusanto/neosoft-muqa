<?php

namespace App\Models\Manufactures;

use Illuminate\Database\Eloquent\Model;

class WorkOrderDetail extends Model
{
    /** @var array fillable fields */
    protected $fillable = [
        'work_order_id',
        'item_id',
        'qty',
        'finished_qty',
        'mark_as_close'
    ];

    /**
     * Item.
     *
     * @return Model
     **/
    public function item()
    {
        return $this->belongsTo('App\Models\Logistics\Item', 'item_id');
    }

    /**
     * Materials.
     * 
     * @return type
     **/
    public function materials()
    {
        return $this->hasMany('App\Models\Manufactures\Material', 'work_order_detail_id');
    }

    /**
     * Materials.
     * 
     * @return type
     **/
    public function workOrder()
    {
        return $this->belongsTo('App\Models\Manufactures\WorkOrder', 'work_order_id');
    }
}

<?php

namespace App\Models\Manufactures;

use Illuminate\Database\Eloquent\Model;

class WorkOrder extends Model
{
    /** @var array $fillable fillable fields. */
    protected $fillable = [
        'code',
        'pic_id',
        'start_date',
        'due_date',
        'remarks'
    ];

    /**
     * Generate Code
     * 
     * @return string
     */
    public static function generateCode()
    {
        $now = now();
        $array = WorkOrder::whereMonth('created_at', $now->month)
            ->whereYear('created_at', $now->year)
            ->latest('code')->first();

        $number = 0001;
       
        if ($array) {
            $number = substr($array->code, -4) + 1;
        }

        $code = "WO-" . $now->isoFormat('YY') . $now->isoFormat('MM') . sprintf('%04d', $number);
        return $code;
    }

    /**
     * Person in Charge.
     *
     * @return Model
     **/
    public function pic()
    {
        return $this->belongsTo('App\Models\User', 'pic_id');
    }

    /**
     * Work order details.
     *
     * @return Model
     **/
    public function details()
    {
        return $this->hasMany('App\Models\Manufactures\WorkOrderDetail', 'work_order_id');
    }
}

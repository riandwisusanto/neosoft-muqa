<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DoctorReviewImageDetail extends Model
{
    protected $table = "dr_img_detail";

    protected $fillable = [
        'dr_image_id',
        'image',
    ];

    public function doctor_image()
    {
        return $this->belongsTo('App\Models\DoctorImage');
    }
}

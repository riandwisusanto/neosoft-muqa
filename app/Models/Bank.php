<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table = 'banks';
    protected $primaryKey = 'id_bank';

    public function payment()
    {
        return $this->belongsTo('App\Models\Payment', 'id_bank', 'bank_id');
    }

    public function bank_outlets()
    {
        return $this->hasMany(BankOutlet::class, 'bank_id');
    }
}

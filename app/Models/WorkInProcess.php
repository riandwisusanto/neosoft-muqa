<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\Updater;

class WorkInProcess extends Model
{
    use Updater;

    protected $fillable = [
        'work_order_id',
        'wip_date',
        'description',
        'due_date',
        'uom_id',
        'order_qty',
        'status',
        'is_active',
        'created_uid',
        'updated_uid',
    ];

    public function work_in_process_details(){
        return $this->hasMany(WorkInProcessDetail::class);
    }

    public function work_order(){
        return $this->belongsTo(WorkOrder::class);
    }

    public function uom(){
        return $this->belongsTo(Uom::class, 'uom_id');
    }

    protected static function boot(){
        parent::boot();
        static::deleting(function ($model) {
            $model->work_in_process_details()->delete();
        });
    }
}

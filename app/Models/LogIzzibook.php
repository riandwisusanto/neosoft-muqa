<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogIzzibook extends Model
{
    protected $table = 'izzibook_logs';
    protected $primaryKey = 'id';
}

<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Notifications\VerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Customer extends Authenticatable implements JWTSubject, MustVerifyEmail
{
    use Notifiable;
    protected $table = 'customers';
    protected $primaryKey = 'id_customer';

    protected $fillable = [
        'email', 'password',
    ];

    public static function generateIndukCustomer()
    {
        $now = Carbon::now();
        $array = Customer::whereMonth('created_at', $now->month)
            ->whereYear('created_at', $now->year)
            ->latest('induk_customer')->first();

        if ($array) {
            $number = substr($array->induk_customer, -4) + 1;
        } else {
            $number = 1;
        }

        $idkcs = "EM-" . $now->isoFormat('YY') . $now->isoFormat('MM') . sprintf('%04d', $number);

        return $idkcs;
    }

    public function getFullNameAttribute($name)
    {
        return ucwords($name);
    }

    public function getBirthPlaceAttribute($name)
    {
        return strtoupper($name);
    }

    public function invoice()
    {
        return $this->hasMany('App\Models\Invoice', 'customer_id', 'id_customer');
    }

    public function appointment()
    {
        return $this->hasMany('App\Models\Appointment', 'customer_id', 'id_customer');
    }

    public function getBirthDateAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

    public function customer_outlet()
    {
        return $this->hasMany('App\Models\CustomerOutlet', 'customer_id', 'id_customer');
    }

    public function outlet()
    {
        return $this->belongsToMany('App\Models\Outlet', 'customer_outlets', 'customer_id', 'outlet_id')->withPivot('id_customer_outlet');
    }

    public function consultant()
    {
        return $this->belongsToMany('App\Models\Consultant', 'customer_consultants', 'customer_id', 'consultant_id')->withPivot('id_customer_consultant');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country', 'country_id', 'id_country');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\Province', 'city_id', 'id_province');
    }

    public function docter_images()
    {
        return $this->hasMany('App\Models\DoctorReviewImage', 'customer_id', 'id_customer');
    }

    public function active_invoice_points()
    {
        return $this->hasMany(InvoicePoint::class, 'customer_id', 'id_customer')->whereDoesntHave('void');
    }

    public function void_invoice_points()
    {
        return $this->hasMany(InvoicePoint::class, 'customer_id', 'id_customer')->whereHas('void');
    }

    protected $hidden = [
        'password',
    ];

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new \App\Notifications\MailResetPasswordNotification($token));
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
    public function queue()
    {
        return $this->hasOne('App\Models\Queue', 'customer_id', 'id_customer');
    }

    public function queue_get()
    {
        return $this->hasMany('App\Models\Queue', 'customer_id','id_customer');
    }

    public function marketing_source() {
        return $this->hasOne('App\Models\MarketingSource','id_marketing_source','source_id');
    }
}

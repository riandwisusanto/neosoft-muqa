<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ProductTreatment extends Model
{

    protected $table = 'product_treatments';
    protected $primaryKey = 'id_product_treatment';

    public static function generateTreatmentCode($type){
        $now = Carbon::now();
        $array = ProductTreatment::whereMonth('created_at', $now->month)
            ->whereYear('created_at', $now->year)
            ->where('product_treatment_code', 'like', $type . '-%')
            ->latest('product_treatment_code')->first();

        if ($array) {
            $number = substr($array->product_treatment_code, -4) + 1;
        } else {
            $number = 0001;
        }

        $idtretcode = $type . "-" . $now->isoFormat('YY') . $now->isoFormat('MM') . sprintf('%04d', $number);
        return $idtretcode;

    }

    public function product_treatment_points(){
        return $this->hasMany(ProductTreatmentPoint::class, 'product_treatment_id');
    }

    public function invoice_package()
    {
        return $this->belongsTo('App\Models\InvoicePackage', 'id_product_treatment', 'product_id');
    }

    public function getPriceAttribute($price){
        return format_money($price);
    }

    public function points()
    {
        return $this->hasMany(ProductTreatmentPoint::class, 'product_treatment_id');
    }

    public function point()
    {
        return $this->hasOne(ProductTreatmentPoint::class, 'product_treatment_id')
            ->orderBy('id_product_treatment_points', 'DESC');
    }

    public function product_treatment_outlets()
    {
        return $this->hasMany(ProductTreatmentOutlet::class, 'product_id');
    }

    public function outlets()
    {
        return $this->hasMany(ProductTreatmentOutlet::class, 'product_id');
    }

    public function package_detail()
    {
        return $this->belongsTo(PackageDetail::class, 'id_product_treatment', 'product_id');
    }

    public function price_logs()
    {
        return $this->hasMany(LogProductTreatment::class, 'product_treatment_id')->orderBy('created_at', 'desc');
    }

    /**
     * Logistic item.
     *
     * @return Model
     **/
    public function item()
    {
        return $this->hasOne('App\Models\Logistics\Item', 'sales_information_id', 'id_product_treatment');
    }

    public function getProductTreatmentNameAttribute($name)
    {
        return ucwords($name);
    }

}

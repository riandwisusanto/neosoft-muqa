<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class OldAnamnesa extends Model
{
    /** @var Type $var description */
    protected $fillable = ['customer_id', 'checkup_date', 'checkup_time', 'username', 'treatment_name', 'prescription_name', 'outlet_name', 'record_type', 'ax_dx'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\Updater;

class Warehouse extends Model
{
    use Updater;

    protected $primaryKey = 'id_warehouse';
    protected $fillable = [
        'name',
        'description',
        'addr',
        'is_active',
        'code',
    ];

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/

    public function receive_detail(){
        return $this->hasMany('App\Models\Logistics\Purchases\ReceiveDetail', 'warehouse_id');
    }

    public function warehouseOutlets()
    {
        return $this->hasMany('App\Models\WarehouseOutlet', 'warehouse_id');
    }
}

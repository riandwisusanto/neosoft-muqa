<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupplierOutlet extends Model
{
    protected $fillable = [
        'outlet_id', 'supplier_id'
    ];
}

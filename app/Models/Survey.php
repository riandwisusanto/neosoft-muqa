<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'surveys';
    protected $primarykey = 'id_survey';
}

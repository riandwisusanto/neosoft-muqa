<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityProductTreatment extends Model
{
    protected $table = 'activity_product_treatments';

    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = ['activity_id', 'package_id', 'product_treatment_id'];
}

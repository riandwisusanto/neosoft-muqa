<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemAlert extends Model
{
    protected $table = 'items';
    protected $primaryKey = 'product_treatment_code';

    protected $fillable = ['id_item', 'product_treatment_code'];

    public function product()
    {
        return $this->belongsTo('App\Models\ProductTreatment', 'product_treatment_code', 'id_product_treatment');
    }
}

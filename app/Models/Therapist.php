<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Therapist extends Model
{
    protected $table = 'therapists';
    protected $primaryKey = 'id_therapist';

    protected $fillable = ['therapist_name', 'group_id', 'join_date','description','type','img', 'status_therapist'];

    /** @var Type $var description */
    protected $casts = ['join_date' => 'datetime'];


    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function group()
    {
        return $this->belongsTo('App\Models\Group', 'group_id', 'group_id');
    }

    public function therapist_outlets()
    {
        return $this->hasMany(TherapistOutlet::class, 'therapist_id');
    }

    public function therapist_leave()
    {
        return $this->hasMany('App\Models\TherapistLeave', 'therapist_id', 'id_therapist');
    }

    public function therapist_schedule()
    {
        return $this->hasMany('App\Models\TherapistSchedule', 'therapist_id', 'id_therapist');
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class, 'id_therapist', 'therapist_id');
    }

    public function queue()
    {
        return $this->hasMany('App\Models\Queue', 'therapist_id', 'id_therapist');
    }
    public function queueScreen()
    {
        return $this->hasOne('App\Models\QueueScreen', 'therapist_id', 'id_therapist');
    }

}

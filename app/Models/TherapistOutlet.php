<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TherapistOutlet extends Model
{
    protected $table = 'therapist_outlets';
    protected $primaryKey = 'id_therapist_outlet';

    protected $fillable = ['outlet_id', 'therapist_id'];


    public function outlet()
    {
        return $this->belongsTo(Outlet::class, 'outlet_id');
    }
}

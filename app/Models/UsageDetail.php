<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsageDetail extends Model
{
    protected $table = 'usage_details';
    protected $primaryKey = 'id_usage_detail';

    public function product_treatment()
    {
        return $this->belongsTo('App\Models\ProductTreatment', 'product_id', 'id_product_treatment');
    }

    public function invoice_package()
    {
        return $this->belongsTo('App\Models\InvoicePackage', 'invoice_package_id', 'id_invoice_package');
    }

    public function usage()
    {
        return $this->belongsTo('App\Models\Usage', 'usage_id', 'id_usage');
    }

    public function item()
    {
        return $this->belongsTo('App\Models\Item', 'product_id', 'product_id');
    }


    public function usage_therapist()
    {
        return $this->hasMany('App\Models\UsageTherapist', 'usage_detail_id', 'id_usage_detail');
    }

    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoice', 'inv_code', 'inv_code');
    }
}

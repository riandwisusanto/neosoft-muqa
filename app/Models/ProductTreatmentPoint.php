<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductTreatmentPoint extends Model
{
    //
    protected $table = 'product_treatment_points';
    protected $primaryKey = 'id_product_treatment_points';

    protected $fillable = [
        'product_treatment_id',
        'point'
    ];

    public function product_treatment()
    {
        return $this->belongsTo(ProductTreatment::class, 'product_treatment_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserOutlet extends Model
{
    protected $table = 'user_outlets';
    protected $primaryKey = 'id_outlet';

    protected $fillable = ['outlet_id', 'user_id'];

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function outlet()
    {
        return $this->belongsTo('App\Models\Outlet', 'outlet_id', 'id_outlet');
    }
}

<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = 'activities';
    protected $primaryKey = 'id_activity';

    public static function generateActivityCode()
    {
        // $now = Carbon::now();
        // $array = Activity::whereYear('created_at', $now->year)->latest('activity_code')->first();

        $now = Carbon::now();
        $array = Activity::whereMonth('created_at', $now->month)
            ->whereYear('created_at', $now->year)
            ->latest('activity_code')->first();

        if ($array) {
            $number = substr($array->activity_code, -4) + 1;
        } else {
            $number = 0001;
        }

        $idkcs = "A" . "-" . $now->isoFormat('YY') . $now->isoFormat('MM') . sprintf('%04d', $number);
        return $idkcs;

        // if ($array) {
        //     $number = substr($array->activity_code, -4) + 1;
        // } else {
        //     $number = 0001;
        // }
        // $idinvcode = "ACT-" . $now->format('Y').sprintf('%04d', $number);

        // return $idinvcode;
    }

    public function activityGroup()
    {
        return $this->belongsTo('App\Models\ActivityGroup');
    }

    public function activity_outlets()
    {
        return $this->hasMany(ActivityOutlet::class, 'activity_id');
    }

    public function activity_product_treatments()
    {
        return $this->hasMany(ActivityProductTreatment::class, 'activity_id');
    }

    public function invoice_detail()
    {
        return $this->belongsTo('App\Models\InvoiceDetail', 'id_activity', 'activity_id');
    }

    public function getStartDateAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

    public function getEndDateAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }
}

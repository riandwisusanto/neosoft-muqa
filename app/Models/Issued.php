<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Traits\Updater;

use Carbon\Carbon;

class Issued extends Model
{
    use Updater;

    protected $fillable = [
        'warehouse_id',
        'customer_id',
        'issued_num',
        'issued_date',
        'description',
        'invoice_no',
        'invoice_date',
        'pay_method',
        'due_date',
        'status',
        'created_uid',
        'updated_uid',
    ];

    public function getStatusDescAttribute(){
        $text = '';
        if($this->status == '1'){
            $text = 'Create/Draft Retur';
        }else if($this->status == '2'){
            $text = 'Deliver Product';
        }else if($this->status == '3'){
            $text = 'Receive Product by Customer';
        }else if($this->status == '4'){
            $text = 'Canceled';
        }else{
            $text = 'Closed';
        }
        return $text;
    }

    public function customer(){
        return $this->belongsTo(Custommer::class, 'customer_id');
    }

    public function warehouse(){
        return $this->belongsTo(Warehouse::class, 'warehouse_id');
    }

    public function issued_details(){
        return $this->hasMany(IssuedDetail::class);
    }

    public static function generateCode(){
        $now    = Carbon::now();
        $prefix = 'IS-'.$now->isoFormat('YYMM');
        $num    = 0;
        $data   = self::select('issued_num')
            ->whereMonth('created_at', $now->month)
            ->whereYear('created_at', $now->year)
            ->orderBy('issued_num', 'desc')
            ->first();

        if($data) $num = (int) substr($data->issued_num, strlen($prefix), 4);

        $num++;
        return $prefix.str_pad($num, 4, '0', STR_PAD_LEFT);
    }

    protected static function boot(){
        parent::boot();
        static::creating(function($model){
            $model->issued_num = self::generateCode();
        });

        static::deleting(function ($model) {
            $model->issued_details()->delete();
        });
    }
}

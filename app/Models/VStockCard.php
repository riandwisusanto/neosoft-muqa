<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VStockCard extends Model
{
    protected $table = 'v_stock_card';
}

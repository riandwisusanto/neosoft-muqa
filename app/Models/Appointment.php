<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Appointment extends Model
{
    protected $table = 'appointments';
    protected $primaryKey = 'id_appointments';

    public function getComeDateAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

    public function room()
    {
        return $this->belongsTo('App\Models\Room', 'room_id', 'id_room');
    }

    public function product_treatment()
    {
        return $this->belongsTo('App\Models\ProductTreatment', 'treatment_id', 'id_product_treatment');
    }

    public function outlet()
    {
        return $this->belongsTo('App\Models\Outlet', 'outlet_id', 'id_outlet');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id', 'id_customer');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'staff_id', 'id');
    }

    public function therapist()
    {
        return $this->belongsToMany('App\Models\Therapist', 'appointment_therapists', 'appointment_id', 'therapist_id');
    }

    public function appointment_therapist()
    {
        return $this->belongsTo('App\Models\AppointmentTherapist', 'appointment_id', 'id_appointments');
    }

    public function log_appointment()
    {
        return $this->hasMany('App\Models\LogAppointment', 'appointment_id', 'id_appointments');
    }
}

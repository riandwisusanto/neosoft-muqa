<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Anamnesa extends Model
{
    protected $table = 'anamnesas';
    protected $primaryKey = 'id_anamnesa';

    /** @var Type $var description */
    protected $fillable = ['customer_id', 'anamnesa', 'created', 'suggestion', 'complain', 'date_anamnesa'];


    public static function generateTreatmentCode($type)
    {
        $now = Carbon::now();
        $array = Anamnesa::whereMonth('created_at', $now->month)
            ->whereYear('created_at', $now->year)
            ->where('anamnesa_code', 'like', $type . '-%')
            ->latest('anamnesa_code')->first();

        if ($array) {
            $number = substr($array->anamnesa_code, -4) + 1;
        } else {
            $number = 0001;
        }

        $idtretcode = $type . "-" . $now->isoFormat('YY') . $now->isoFormat('MM') . sprintf('%04d', $number);
        return $idtretcode;
    }

    public static function generateCode()
    {
        $now = Carbon::now();
        $array = Anamnesa::whereMonth('created_at', $now->month)
            ->whereYear('created_at', $now->year)
            ->latest('anamnesa_code')->first();

        if ($array) {
            $number = substr($array->anamnesa_code, -4) + 1;
        } else {
            $number = 0001;
        }

        $idtretcode = "ANM-" . $now->isoFormat('YY') . $now->isoFormat('MM') . sprintf('%04d', $number);
        return $idtretcode;
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function diagnosas()
    {
        return $this->hasMany('App\Models\AnamnesaDiagnosa', 'anamnesa_id', 'id_anamnesa');
    }

    // will be deprecated in future,
    // please see details() method.
    public function anamnesa_detail()
    {
        return $this->hasMany('App\Models\AnamnesaDetail', 'anamnesa_id', 'id_anamnesa');
    }

    // more readable name for relations,
    // used for API v1.
    public function details()
    {
        return $this->hasMany('App\Models\AnamnesaDetail', 'anamnesa_id', 'id_anamnesa');
    }

    public function getDateAnamnesaAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customer_id', 'id_customer');
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function creator()
    {
        return $this->belongsTo('App\Models\User', 'created');
    }
}

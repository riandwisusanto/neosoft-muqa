<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';
    protected $primaryKey = 'id_payment';

    public function bank()
    {
        return $this->belongsTo('App\Models\Bank', 'bank_id', 'id_bank');
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoice', 'inv_id', 'id_invoice');
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function invoiceBalance()
    {
        return $this->belongsTo('App\Models\InvoiceBalance', 'balance_id', 'id_balance');
    }

    public function getAmountAttribute($price)
    {
        return format_money($price);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetail extends Model
{
    protected $table = 'invoice_details';
    protected $primaryKey = 'id_inv_detail';

    public function activity()
    {
        return $this->belongsTo('App\Models\Activity', 'activity_id', 'id_activity');
    }

    public function package()
    {
        return $this->belongsTo('App\Models\Package', 'package_code', 'package_code');
    }

    public function item()
    {
        return $this->belongsTo('App\Models\Item', 'package_code', 'code');
    }

    public function invoice_package()
    {
        return $this->hasMany('App\Models\InvoicePackage', 'inv_detail_id', 'id_inv_detail');
    }

    public function invoice()
    {
        return $this->hasMany('App\Models\Invoice', 'id_invoice', 'inv_id');
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function invoicePackage()
    {
        return $this->hasOne('App\Models\InvoicePackage', 'inv_detail_id', 'id_inv_detail');
    }

    public function product_treatment()
    {
        return $this->belongsTo('App\Models\ProductTreatment', 'package_code', 'product_treatment_code');
    }

    // public function getComeDateAttribute($date){
    //     return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    // }

    public function getCurrentPriceAttribute($price)
    {
        return format_money($price);
    }

    public function getSubtotalAttribute($price)
    {
        return format_money($price);
    }

    public function setCurrentPriceAttribute($price)
    {
        $this->attributes['current_price'] = str_replace(",", "", $price);
    }

    public function setSubtotalAttribute($price)
    {
        $this->attributes['subtotal'] = str_replace(",", "", $price);
    }
}

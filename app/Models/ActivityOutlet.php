<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityOutlet extends Model
{
    protected $table = 'activity_outlets';
    protected $primaryKey = 'id_ActivityOutlet';


    protected $fillable = ['outlet_id', 'activity_id'];


    public function outlet()
    {
        return $this->belongsTo(Outlet::class, 'outlet_id');
    }
}

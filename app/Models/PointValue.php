<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PointValue extends Model
{
    protected $table = 'point_values';
    protected $primaryKey = 'id_point_value';
}

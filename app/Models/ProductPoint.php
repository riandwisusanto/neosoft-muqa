<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductPoint extends Model
{
    //
    protected $table = 'product_points';
    protected $primaryKey = 'id_product_point';

    public function invoice_point_details()
    {
        return $this->hasMany(InvoicePointDetail::class, 'product_point_id', 'id_product_point');
    }

    public function invoice_point_detail()
    {
        return $this->hasOne(InvoicePointDetail::class, 'product_point_id', 'id_product_point');
    }
}

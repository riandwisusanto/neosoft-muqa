<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';
    protected $primaryKey = 'id_country';

    public function province()
    {
        return $this->belongsTo(Province::class, 'id_country', 'country_id');
    }
}

<?php

namespace App\Exports;

use App\Models\Customer;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CustomerExport implements FromCollection, WithHeadings
{
    // use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function headings(): array
    {
        return [
            'Reff old',
            'Reff',
            'Fullname',
            'Nickname',
            'Birth Date',
            'Phone',
            'Gender',
            'Address',
            'Province',
        ];
    }

    public function collection()
    {
        $sql = "
            SELECT
                c.induk_customer_old as reff_old,
                c.induk_customer as reff,
                c.full_name as fullname,
                c.nick_name as nickname,
                DATE_FORMAT(c.birth_date, '%d/%m/%Y') as birth_date,
                c.phone,
                c.gender,
                c.address,
                prov.province
            FROM
                customers c
                    LEFT JOIN
                provinces prov ON c.city_id = prov.id_province
        ";

        $customers = DB::select($sql);
        return collect($customers);
    }
}

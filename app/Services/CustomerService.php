<?php 
namespace App\Services;

use App\Services\IzzibookClient;
use App\Models\Customer;
use Carbon\carbon;
use App\Models\LogIzzibook;

class CustomerService {
    private $client = null;

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function __construct()
    {
        $this->client = new IzzibookClient();
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function store(Customer $data)
    {        
        // make izzibook log
        $izzibookLog = new LogIzzibook();
        $izzibookLog->table_name = 'customers';
        $izzibookLog->action = 'create';
        $body = [
            'Code' => $data->induk_customer,
            'Name' => $data->full_name,
            'Address' => $data->address,
            'City' => $data->city->province,
            'Email' => $data->email,
            'Phone' => $data->phone,
            'IsPpn' => 0
        ];

        $endpoint = config('services.izzibook_api.url') . config('services.izzibook_api.clinic_path') . 'Master_customer';

        $izzibookLog->payload = json_encode($body);
        $izzibookLog->response_body = '-';
        $izzibookLog->response_code = '-';
        $izzibookLog->req_time = date('Y-m-d H:i:s');
        $izzibookLog->endpoint =  $endpoint;
        $izzibookLog->method = 'POST';
        $izzibookLog->save();

        $result = $this->client->post('Master_customer', $body);

        // update izzibook log
        $updateIzzibookLog = LogIzzibook::findOrFail($izzibookLog->id);
        $updateIzzibookLog->response_body = json_encode($result->body);
        $updateIzzibookLog->response_code = json_encode($result->statusCode);
        $updateIzzibookLog->update();

        $c = Customer::find($data->id_customer);
        $c->izzibook_sync_status = 1;
        $c->save();

        return $result;
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function update(Customer $data)
    {
        // make izzibook log
        $izzibookLog = new LogIzzibook();
        $izzibookLog->table_name = 'customers';
        $izzibookLog->action = 'update';
        $body = [
            'Code' => $data->induk_customer,
            'Name' => $data->full_name,
            'Address' => $data->address,
            'City' => $data->city->province,
            'Email' => $data->email,
            'Phone' => $data->phone
        ];
        $izzibookLog->payload = json_encode($body);
        $izzibookLog->response_body = '-';
        $izzibookLog->response_code = '-';
        $izzibookLog->req_time = Carbon::now()->toDateString();
        $izzibookLog->endpoint = 'Master_customer';
        $izzibookLog->method = 'PUT';
        $izzibookLog->save();

        $result = $this->client->put('Master_customer', $body);

        // update izzibook log
        $updateIzzibookLog = LogIzzibook::findOrFail($izzibookLog->id);
        $updateIzzibookLog->response_body = json_encode($result->body);
        $updateIzzibookLog->response_code = json_encode($result->statusCode);
        $updateIzzibookLog->update();

        $c = Customer::find($data->id_customer);
        $c->izzibook_sync_status = 1;
        $c->save();

        return $result;
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function destroy(Customer $data)
    {        
        // make izzibook log
        $izzibookLog = new LogIzzibook();
        $izzibookLog->table_name = 'customers';
        $izzibookLog->action = 'delete';
        $body = [
            'Code' => $data->induk_customer
        ];
        $izzibookLog->payload = json_encode($body);
        $izzibookLog->response_body = '-';
        $izzibookLog->response_code = '-';
        $izzibookLog->req_time = Carbon::now()->toDateString();
        $izzibookLog->endpoint = 'Master_customer';
        $izzibookLog->method = 'DEL';
        $izzibookLog->save();

        $result = $this->client->delete('Master_customer', $body);

        // update izzibook log
        $updateIzzibookLog = LogIzzibook::findOrFail($izzibookLog->id);
        $updateIzzibookLog->response_body = json_encode($result->body);
        $updateIzzibookLog->response_code = json_encode($result->statusCode);
        $updateIzzibookLog->update();

        return $result;
    }
}
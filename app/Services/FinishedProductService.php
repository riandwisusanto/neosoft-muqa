<?php 
namespace App\Services;

use App\Services\IzzibookClient;
use App\Models\Manufactures\FinishedProduct;
use Carbon\Carbon;
use App\Models\LogIzzibook;

class FinishedProductService {
    private $client = null;

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function __construct()
    {
        $this->client = new IzzibookClient();
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function store(FinishedProduct $data)
    {        
        // make izzibook log
        $izzibookLog = new LogIzzibook();
        $izzibookLog->table_name = 'finished_product';
        $izzibookLog->action = 'create';
        $body = [
            'WarehouseCode' => '000',
            'InvoiceNo' => $data->code,
            'InvoiceDate' => Carbon::createFromFormat('Y-m-d', $data->checked_at)->format('d/M/Y'),
            'InvoiceNotes' => $data->marked_as,
            'Data' => [],
            'Operator'  => auth()->user()->name
        ];

        // convertion in 
        $body['Data'][] = [
            'ConvertionType'    => 'IN',
            'ProductCode'       => $data->item->sales->product_treatment_code,
            'ProductName'       => $data->item->name,
            'Unit'              => isset($data->item->uom) ? $data->item->uom->display : 'pieces',
            'Qty'               => $data->qty
        ];
        
        // convertion out
        foreach ($data->materials as $detail) {
            $body['Data'][] = [
                'ConvertionType'    => 'OUT',
                'ProductCode'       => $detail->rawItem->sales->product_treatment_code,
                'ProductName'       => $detail->rawItem->name,
                'Unit'              => isset($detail->rawitem->uom) ? $detail->rawitem->uom->display : 'pieces',
                'Qty'               => $detail->released_qty
            ];
        }

        $izzibookLog->payload = json_encode($body);
        $izzibookLog->response_body = '-';
        $izzibookLog->response_code = '-';
        $izzibookLog->req_time = date('Y-m-d H:i:s');
        $izzibookLog->endpoint = 'Transaction_stock_conversion';
        $izzibookLog->method = 'POST';
        $izzibookLog->save();

        $result = $this->client->post('Transaction_stock_conversion', $body);

        // update izzibook log
        $updateIzzibookLog = LogIzzibook::findOrFail($izzibookLog->id);
        $updateIzzibookLog->response_body = json_encode($result->body);
        $updateIzzibookLog->response_code = json_encode($result->statusCode);
        $updateIzzibookLog->update();

        return $result;
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function update(Customer $data)
    {        
        // make izzibook log
        $izzibookLog = new LogIzzibook();
        $izzibookLog->table_name = 'customers';
        $izzibookLog->action = 'update';
        $body = [
            'Code' => $data->induk_customer,
            'Name' => $data->full_name,
            'Address' => $data->address,
            'City' => $data->city->province,
            'Email' => $data->email,
            'Phone' => $data->phone
        ];
        $izzibookLog->payload = json_encode($body);
        $izzibookLog->response_body = '-';
        $izzibookLog->response_code = '-';
        $izzibookLog->req_time = Carbon::now()->toDateString();
        $izzibookLog->endpoint = 'Master_customer';
        $izzibookLog->method = 'PUT';
        $izzibookLog->save();

        $result = $this->client->put('Master_customer', $body);

        // update izzibook log
        $updateIzzibookLog = LogIzzibook::findOrFail($izzibookLog->id);
        $updateIzzibookLog->response_body = json_encode($result->body);
        $updateIzzibookLog->response_code = json_encode($result->statusCode);
        $updateIzzibookLog->update();

        return $result;
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function destroy(Customer $data)
    {        
        // make izzibook log
        $izzibookLog = new LogIzzibook();
        $izzibookLog->table_name = 'customers';
        $izzibookLog->action = 'delete';
        $body = [
            'Code' => $data->induk_customer
        ];
        $izzibookLog->payload = json_encode($body);
        $izzibookLog->response_body = '-';
        $izzibookLog->response_code = '-';
        $izzibookLog->req_time = Carbon::now()->toDateString();
        $izzibookLog->endpoint = 'Master_customer';
        $izzibookLog->method = 'DEL';
        $izzibookLog->save();

        $result = $this->client->delete('Master_customer', $body);

        // update izzibook log
        $updateIzzibookLog = LogIzzibook::findOrFail($izzibookLog->id);
        $updateIzzibookLog->response_body = json_encode($result->body);
        $updateIzzibookLog->response_code = json_encode($result->statusCode);
        $updateIzzibookLog->update();

        return $result;
    }
}
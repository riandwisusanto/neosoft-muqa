<?php 
namespace App\Services;

use App\Services\IzzibookClient;
use App\Models\Logistics\Purchases\Order;
use Carbon\Carbon;
use App\Models\LogIzzibook;

class PurchaseOrderService {
    private $client = null;

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function __construct()
    {
        $this->client = new IzzibookClient();
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function store(Order $data)
    {        
        // make izzibook log
        $izzibookLog = new LogIzzibook();
        $izzibookLog->table_name = 'purchase_order';
        $izzibookLog->action = 'create';
        $body = [
            'WarehouseCode' => '000',
            'SupplierCode' => $data->supplier->code,
            'SupplierName' => $data->supplier->name,
            'SupplierAddress' => $data->supplier->addr,
            'SupplierPhone' => '',
            'InvoiceNo' => $data->code,
            'InvoiceDate' => $data->transdate,
            'SupplierInvoiceNo' => '',
            'TOP' => 0,
            'IsPPN' => 0,
            'PPN' => 0,
            'InvoiceNotes' => $data->remarks,
            'Data' => []
        ];

        foreach ($data->details as $detail) {
            $body['Data'][] = [
                'ProductCode'   => $detail->item->sales->product_treatment_code,
                'ProductName'   => $detail->item->name,
                'Qty'           => $detail->qty,
                'Unit'          => isset($detail->item->uom) ? $detail->item->uom->display : 'pieces',
                'Price'         => $detail->price
            ];
        }

        $izzibookLog->payload = json_encode($body);
        $izzibookLog->response_body = '-';
        $izzibookLog->response_code = '-';
        $izzibookLog->req_time = date('Y-m-d H:i:s');
        $izzibookLog->endpoint = 'Transaction_purchase_order';
        $izzibookLog->method = 'POST';
        $izzibookLog->save();

        $result = $this->client->post('Transaction_purchase_order', $body);

        // update izzibook log
        $updateIzzibookLog = LogIzzibook::findOrFail($izzibookLog->id);
        $updateIzzibookLog->response_body = json_encode($result->body);
        $updateIzzibookLog->response_code = json_encode($result->statusCode);
        $updateIzzibookLog->update();

        return $result;
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function update(Customer $data)
    {        
        // make izzibook log
        $izzibookLog = new LogIzzibook();
        $izzibookLog->table_name = 'customers';
        $izzibookLog->action = 'update';
        $body = [
            'Code' => $data->induk_customer,
            'Name' => $data->full_name,
            'Address' => $data->address,
            'City' => $data->city->province,
            'Email' => $data->email,
            'Phone' => $data->phone
        ];
        $izzibookLog->payload = json_encode($body);
        $izzibookLog->response_body = '-';
        $izzibookLog->response_code = '-';
        $izzibookLog->req_time = Carbon::now()->toDateString();
        $izzibookLog->endpoint = 'Master_customer';
        $izzibookLog->method = 'PUT';
        $izzibookLog->save();

        $result = $this->client->put('Master_customer', $body);

        // update izzibook log
        $updateIzzibookLog = LogIzzibook::findOrFail($izzibookLog->id);
        $updateIzzibookLog->response_body = json_encode($result->body);
        $updateIzzibookLog->response_code = json_encode($result->statusCode);
        $updateIzzibookLog->update();

        return $result;
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function destroy(Customer $data)
    {        
        // make izzibook log
        $izzibookLog = new LogIzzibook();
        $izzibookLog->table_name = 'customers';
        $izzibookLog->action = 'delete';
        $body = [
            'Code' => $data->induk_customer
        ];
        $izzibookLog->payload = json_encode($body);
        $izzibookLog->response_body = '-';
        $izzibookLog->response_code = '-';
        $izzibookLog->req_time = Carbon::now()->toDateString();
        $izzibookLog->endpoint = 'Master_customer';
        $izzibookLog->method = 'DEL';
        $izzibookLog->save();

        $result = $this->client->delete('Master_customer', $body);

        // update izzibook log
        $updateIzzibookLog = LogIzzibook::findOrFail($izzibookLog->id);
        $updateIzzibookLog->response_body = json_encode($result->body);
        $updateIzzibookLog->response_code = json_encode($result->statusCode);
        $updateIzzibookLog->update();

        return $result;
    }
}
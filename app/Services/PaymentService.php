<?php 
namespace App\Services;

use App\Models\Payment;
use App\Services\IzzibookClient;
use Carbon\carbon;
use App\Models\LogIzzibook;


class PaymentService {
    private $client = null;

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function __construct()
    {
        $this->client = new IzzibookClient();
        $this->endpoint = config('services.izzibook_api.url') . config('services.izzibook_api.clinic_path') . 'Transaction_sales_payment';
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function store(Payment $data)
    {        
        // make izzibook log
        $izzibookLog = new LogIzzibook();
        $izzibookLog->table_name = 'payments';
        $izzibookLog->action = 'create';

        $customerCode = isset($data->invoice) ? $data->invoice->customer->induk_customer : $data->invoiceBalance->customer->induk_customer;
        $customerName = isset($data->invoice) ? $data->invoice->customer->full_name : $data->invoiceBalance->customer->full_name;
        $invCode = isset($data->invoice) ? $data->invoice->inv_code : $data->invoiceBalance->balance_code;
        $invDate = isset($data->invoice) ? $data->invoice->inv_date : $data->invoiceBalance->balance_date;

        $body = [
            'CustomerCode' => $customerCode,
            'CustomerName' => $customerName,
            'PaymentType' => $data->bank->bank_code,
            'PaymentName' => $data->bank->bank_name,
            'PaymentNo' => $invCode . '/'. $data->id_payment,
            'PaymentDate' => Carbon::createFromFormat('d/m/Y', $invDate)->format('d/M/Y'),
            'PaymentAdditionalInfo' => $data->info,
            'InvoiceNo' => $invCode,
            'PaymentTotal' => str_replace(',', '', $data->amount),
            'PaymentNotes' => '',
            'Data'  => []
        ];

        $izzibookLog->payload = json_encode($body);
        $izzibookLog->response_body = '-';
        $izzibookLog->response_code = '-';
        $izzibookLog->req_time = date('Y-m-d H:i:s');
        $izzibookLog->endpoint = $this->endpoint;
        $izzibookLog->method = 'POST';
        $izzibookLog->save();

        $result = $this->client->post('Transaction_sales_payment', $body);

        // update izzibook log
        $updateIzzibookLog = LogIzzibook::findOrFail($izzibookLog->id);
        $updateIzzibookLog->response_body = json_encode($result->body);
        $updateIzzibookLog->response_code = json_encode($result->statusCode);
        $updateIzzibookLog->update();

        $c = Payment::find($data->id_payment);
        $c->izzibook_sync_status = 1;
        $c->save();

        return $result;
    }    
}
<?php
namespace App\Services;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;

/**
 * Auth token reader/writer.
 */
class TokenRepository
{
    private $username = '';
    private $password = '';


    /** 
     * TokenRepository constructor.
     * 
     * @param string $filePath token storage path.
     * @param string $username izzibook auth username.
     * @param string $password izzibook auth password.
     **/
    public function __construct(string $username, string $password)
    {
        $this->cacheTag = 'izzibook:auth:'.$username;
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * Claims will exchange username and password into access token.
     *
     * @return string token.
     **/
    public function claims(): string
    {
        if ($cached = Cache::get($this->cacheTag)) {
            $auth = json_decode($cached);
            return $auth->token;
        }

        $auth = $this->getToken();
        return $auth->token;
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    private function getToken()
    {
        try {
            $client = new Client();
            $base_uri = config('services.izzibook_api.url') . config('services.izzibook_api.clinic_path');
            $resp = $client->post($base_uri. 'token', [
                'auth' => [$this->username, $this->password]
            ]);

            // TODO: handle auth error.
            $body = (string) $resp->getBody();
            $body = json_decode($body);

            $data = (object) [
                'token'     => $body->token,
                'expired_at' => $body->expired_at
            ];

            Cache::put($this->cacheTag, json_encode($data), $body->expired_at - time());
            return $data;
        } catch (\Exception $e) {
            return response()->json([
                'message'   => $e->getMessage()
            ], 500);
        }
        
    }
}

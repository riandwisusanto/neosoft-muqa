<?php 
namespace App\Services;

use App\Models\Invoice;
use App\Services\IzzibookClient;
use Carbon\carbon;
use App\Models\LogIzzibook;


class InvoiceService {
    private $client = null;

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function __construct()
    {
        $this->client = new IzzibookClient();
        $this->endpoint = config('services.izzibook_api.url') . config('services.izzibook_api.clinic_path') . 'Transaction_sales_invoice';
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function store(Invoice $data)
    {        
        // make izzibook log
        $izzibookLog = new LogIzzibook();
        $izzibookLog->table_name = 'invoices';
        $izzibookLog->action = 'create';

        $invoiceDetails = [];
        foreach ($data->invoice_detail as $detail) {
            $unit = isset($detail->product_treatment->item->uom) ? $detail->product_treatment->item->uom->display : 'pieces';
            $currentPrice = str_replace(array('.', ','), "", $detail->current_price);
            $subtotal = $currentPrice * $detail->qty;

            $price = $currentPrice;

            // check for discount with type percent .
            if ($detail->type_line == 0) {
                $price = $currentPrice - ($currentPrice * $detail->discount_1 / 100);

            
                if ($detail->discount_2 != null) {
                    $price = $price - ($price * $detail->discount_2 / 100);
                }
            } else { // it's nominal discount.
                $price = ($subtotal - $detail->discount_1 - $detail->discount_2) / $detail->qty;
            }
            
            $invoiceDetails[] = [
                'ProductCode' => $detail->package_code,
                'ProductName' => $detail->product_treatment->product_treatment_name,
                'Qty' => $detail->qty,
                'Unit' => $unit,
                'Price' => $price,
                'Disc1' => 0,
                'Disc2' => 0,
                'Disc3' => 0
            ];
        };
        
        $body = [
            'WarehouseCode' => $data->outlet->outlet_code,
            'CustomerCode' => $data->customer->induk_customer,
            'CustomerName' => $data->customer->full_name,
            'CustomerAddress' => $data->customer->address === NULL ? "" : $data->customer->address,
            'CustomerPhone' => $data->customer->phone,
            'InvoiceType' => 1,
            'InvoiceNo' => $data->inv_code,
            'InvoiceDate' => Carbon::createFromFormat('d/m/Y', $data->inv_date)->format('d/M/Y'),
            'IsPPN' => 0,
            'PPN' => 0,
            'InvoiceNotes' => $data->remarks === NULL ? "" : $data->remarks,
            'Data' => $invoiceDetails
        ];
        $izzibookLog->payload = json_encode($body);
        $izzibookLog->response_body = '-';
        $izzibookLog->response_code = '-';
        $izzibookLog->req_time = date('Y-m-d H:i:s');
        $izzibookLog->endpoint = $this->endpoint;
        $izzibookLog->method = 'POST';
        $izzibookLog->save();

        $result = $this->client->post('Transaction_sales_invoice', $body);

        // update izzibook log
        $updateIzzibookLog = LogIzzibook::findOrFail($izzibookLog->id);
        $updateIzzibookLog->response_body = json_encode($result->body);
        $updateIzzibookLog->response_code = json_encode($result->statusCode);
        $updateIzzibookLog->update();

        $c = Invoice::find($data->id_invoice);
        $c->izzibook_sync_status = 1;
        $c->save();

        return $result;
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function update(Invoice $data)
    {        
        // make izzibook log
        $izzibookLog = new LogIzzibook();
        $izzibookLog->table_name = 'invoices';
        $izzibookLog->action = 'update';

        $invoiceDetails = [];
        foreach ($data->invoice_detail as $detail) {
            $unit = isset($detail->product_treatment->item->uom) ? $detail->product_treatment->item->uom->display : 'pcs';

            $currentPrice = str_replace(array('.', ','), "", $detail->current_price);
            $subtotal = $currentPrice * $detail->qty;

            $price = $currentPrice;

            // check for discount with type percent .
            if ($detail->type_line == 0) {
                $price = $currentPrice - ($currentPrice * $detail->discount_1 / 100);

            
                if ($detail->discount_2 != null) {
                    $price = $price - ($price * $detail->discount_2 / 100);
                }
            } else { // it's nominal discount.
                $price = ($subtotal - $detail->discount_1 - $detail->discount_2) / $detail->qty;
            }

            $invoiceDetails[] = [
                'ProductCode' => $detail->package_code,
                'ProductName' => $detail->product_treatment->product_treatment_name,
                'Qty' => $detail->qty,
                'Unit' => $unit,
                'Price' => $price,
                'Disc1' => 0,
                'Disc2' => 0,
                'Disc3' => 0
            ];
        };

        $body = [
            'WarehouseCode' => $data->outlet->outlet_code,
            'CustomerCode' => $data->customer->induk_customer,
            'CustomerName' => $data->customer->full_name,
            'CustomerAddress' => $data->customer->address === NULL ? "" : $data->customer->address,
            'CustomerPhone' => $data->customer->phone,
            'InvoiceNo' => $data->inv_code,
            'InvoiceDate' => Carbon::createFromFormat('d/m/Y', $data->inv_date)->format('d/M/Y'),
            'IsPPN' => 0,
            'PPN' => 0,
            'InvoiceNotes' => $data->remarks === NULL ? "" : $data->remarks,
            'Data' => $invoiceDetails,
            'InvoiceType' => 1,
        ];

        $izzibookLog->payload = json_encode($body);
        $izzibookLog->response_body = '-';
        $izzibookLog->response_code = '-';
        $izzibookLog->req_time = Carbon::now()->toDateTimeString();
        $izzibookLog->endpoint = $this->endpoint;
        $izzibookLog->method = 'POST';
        $izzibookLog->save();

        $result = $this->client->post('Transaction_sales_invoice', $body);

        // update izzibook log
        $updateIzzibookLog = LogIzzibook::findOrFail($izzibookLog->id);
        $updateIzzibookLog->response_body = json_encode($result->body);
        $updateIzzibookLog->response_code = json_encode($result->statusCode);
        $updateIzzibookLog->update();

        return $result;
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function destroy(Invoice $data)
    {        
        // make izzibook log
        $izzibookLog = new LogIzzibook();
        $izzibookLog->table_name = 'invoices';
        $izzibookLog->action = 'delete';

        $body = [
            'WarehouseCode' => $data->outlet->outlet_code,
            'CustomerCode' => $data->customer->induk_customer,
            'InvoiceNo' => $data->inv_code . "/S",
        ];
        $izzibookLog->payload = json_encode($body);
        $izzibookLog->response_body = '-';
        $izzibookLog->response_code = '-';
        $izzibookLog->req_time = Carbon::now()->toDateTimeString();
        $izzibookLog->endpoint = $this->endpoint;
        $izzibookLog->method = 'DEL';
        $izzibookLog->save();

        $result = $this->client->post('Transaction_sales_invoice', $body);

        // update izzibook log
        $updateIzzibookLog = LogIzzibook::findOrFail($izzibookLog->id);
        $updateIzzibookLog->response_body = json_encode($result->body);
        $updateIzzibookLog->response_code = json_encode($result->statusCode);
        $updateIzzibookLog->update();

        return $result;
    }
}
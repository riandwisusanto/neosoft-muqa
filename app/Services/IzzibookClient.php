<?php 
namespace App\Services;
use GuzzleHttp\Client;
use App\Services\TokenRepository;

/**
 * Izzibook HTTP Client.
 */
class IzzibookClient {
    private $client = null;
    private $tokenRepository = null;

    /**
     * IzzibookClient constructor.
     * 
     * @param string $username izzibook auth username.
     * @param string $password izzibook auth password.
     **/
    public function __construct(string $username = '', string $password = '')
    {
        if ($username == '') {
            $username = config('services.izzibook_api.username');
        }

        if ($password == '') {
            $password = config('services.izzibook_api.password');
        }

        $base_uri = config('services.izzibook_api.url') . config('services.izzibook_api.clinic_path');

        $this->client = new Client(['base_uri' => $base_uri]);
        $this->tokenRepository = new TokenRepository($username, $password);
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function post(string $urlPath, $data)
    {
        $token = $this->tokenRepository->claims();
        
        $resp = $this->client->post($urlPath, [
            'body'  => json_encode($data),
            'headers' => [
                'Content-Type'  => 'application/json',
                'Accept'        => 'application/json',
                'Authorization' => 'Bearer '. $token
            ]
        ]);

        $body = (string) $resp->getBody();
        $body = json_decode($body);
        $statusCode = $resp->getStatusCode();

        return (object) [
            'statusCode' => $statusCode,
            'body' => $body
        ];
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function put(string $urlPath, $data)
    {
        $token = $this->tokenRepository->claims();
        
        $resp = $this->client->post($urlPath, [
            'body'  => json_encode($data),
            'headers' => [
                'Content-Type'  => 'application/json',
                'Accept'        => 'application/json',
                'Authorization' => 'Bearer '. $token
            ]
        ]);

        $body = (string) $resp->getBody();
        $body = json_decode($body);
        $statusCode = $resp->getStatusCode();

        return (object) [
            'statusCode' => $statusCode,
            'body' => $body
        ];
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Type $var Description
     * @return type
     * @throws conditon
     **/
    public function delete(string $urlPath, $data)
    {
        $token = $this->tokenRepository->claims();
        
        $resp = $this->client->delete($urlPath, [
            'body'  => json_encode($data),
            'headers' => [
                'Content-Type'  => 'application/json',
                'Accept'        => 'application/json',
                'Authorization' => 'Bearer '. $token
            ]
        ]);

        $body = (string) $resp->getBody();
        $body = json_decode($body);
        $statusCode = $resp->getStatusCode();

        return (object) [
            'statusCode' => $statusCode,
            'body' => $body
        ];
    }


    // TODO: add rest method (GET)
}
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIzzibookLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('izzibook_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('table_name');
            $table->string('action');
            $table->text('payload');
            $table->string('response_body');
            $table->string('response_code');
            $table->dateTime('req_time');
            $table->string('endpoint');
            $table->string('method');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('izzibook_logs');
    }
}

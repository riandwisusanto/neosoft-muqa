<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkInProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_in_processes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('wip_date');
            $table->integer('work_order_id');
            $table->text('description');
            $table->date('due_date');
            $table->integer('uom_id');
            $table->integer('order_qty');
            $table->string('status', 1);
            $table->string('is_active', 1);
            $table->timestamps();
            $table->string('created_uid', 85)->nullable();
            $table->string('updated_uid', 85)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_in_processes');
    }
}

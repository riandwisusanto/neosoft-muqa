<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->bigIncrements('id_package');
            $table->string('package_code')->unique();
            $table->string('package_name');
            $table->integer('price');
            $table->boolean('status_package')->default(1);
            $table->mediumText('remarks')->nullable();
            $table->double('commission', 10, 2)->nullable();
            $table->boolean('commissionType')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}

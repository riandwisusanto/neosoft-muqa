<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id_customer');
            $table->string('induk_customer')->unique();
            $table->string('full_name');
            $table->string('phone')->unique();
            $table->string('birth_place')->nullable();
            $table->date('birth_date');
            $table->date('join_date')->nullable();
            $table->enum('gender', ['M','F']);
            $table->text('address')->nullable();
            $table->string('email');
            $table->integer('staf_id')->nullable();
            $table->integer('religion')->nullable();
            $table->integer('country_id');
            $table->integer('city_id');
            $table->string('ktp')->nullable();
            $table->string('occupation')->nullable();
            $table->integer('zip')->nullable();
            $table->string('nextofikin')->nullable();
            $table->string('nextofikin_number')->nullable();
            $table->integer('source_id')->nullable();
            $table->string('source_remarks')->nullable();
            $table->string('purpose_treatment')->nullable();
            $table->integer('agen_id')->nullable();
            $table->string('password')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->bigInteger('point')->nullable();
            $table->string('img')->nullable();
            $table->boolean('status_customer')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}

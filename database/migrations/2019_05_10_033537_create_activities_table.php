<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->bigIncrements('id_activity');
            $table->string('activity_name');
            $table->string('activity_code')->unique();
            $table->integer('activity_group_id');
            $table->string('activity_pic');
            $table->date('start_date');
            $table->date('end_date');
            $table->text('remarks')->nullable();
            $table->float('discount_1', 10, 2)->nullable();
            $table->float('discount_2', 10, 2)->nullable();
            $table->boolean('disc_1_type')->default(0);
            $table->boolean('disc_2_type')->default(0);
            $table->boolean('status_activity')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
}

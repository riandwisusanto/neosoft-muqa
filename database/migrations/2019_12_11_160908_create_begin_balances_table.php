<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeginBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('begin_balances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('item_id');
            $table->date('begin_date');
            $table->integer('begin_balance');
            $table->integer('begin_cost');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('begin_balances');
    }
}

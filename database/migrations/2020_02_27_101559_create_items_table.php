<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->bigIncrements('id');
            // relations to product_treatment
            $table->unsignedBigInteger('sales_information_id')->nullable();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->unsignedBigInteger('uom_id')->nullable();
            $table->unsignedBigInteger('verification_id')->nullable();
            $table->date('verified_at')->nullable();
            $table->string('name');
            $table->string('brand')->nullable();
            $table->integer('stock')->nullable();
            $table->integer('sellable_stock')->nullable();
            $table->integer('buffer_stock')->nullable();
            $table->double('unit_value', 8, 2)->nullable();
            $table->enum('type', ['product', 'service']);
            $table->double('cogs', 11, 2)->default(0.00);
            $table->boolean('has_bom')->default(0);
            $table->enum('dispense_method', ['fifo', 'lifo', 'average']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}

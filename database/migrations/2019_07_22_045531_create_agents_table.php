<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agents', function (Blueprint $table) {
            $table->bigIncrements('id_agent');
            $table->string('agent_code');
            $table->string('agent_name', 100)->default('text');
            $table->bigInteger('phone');
            $table->string('email', 100)->unique();
            $table->text('address');
            $table->string('username', 100)->unique()->nullable();
            $table->string('password')->nullable();
            $table->date('active_date')->nullable();
            $table->boolean('status')->default(0);
            $table->string('uniq_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agents');
    }
}

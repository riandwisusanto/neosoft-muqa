<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnamnesasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anamnesas', function (Blueprint $table) {
            $table->bigIncrements('id_anamnesa');
            $table->string('anamnesa_code')->unique();
            $table->BigInteger('customer_id');
            $table->BigInteger('created');
            $table->text('suggestion');
            $table->string('anamnesa');
            $table->date('date_anamnesa');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anamnesas');
    }
}

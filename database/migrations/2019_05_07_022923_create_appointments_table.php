<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->bigIncrements('id_appointments');
            $table->date('come_date');
            $table->time('start_time');
            $table->time('end_time');
            $table->boolean('status_appointment')->default(1);
            $table->integer('outlet_id');
            $table->integer('customer_id');
            $table->integer('staff_id')->nullable();
            $table->integer('activity_id')->nullable();
            $table->integer('treatment_id')->nullable();
            $table->integer('room_id')->nullable();
            $table->text('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}

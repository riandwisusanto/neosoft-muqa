<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommissionSalesToProducttreatment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_treatments', function (Blueprint $table) {
            $table->float('commissionSalesDr',10,2)->nullable();
            $table->float('commissionSalesTr',10,2)->nullable();
            $table->boolean('commSalesTypeDr')->default(0);
            $table->boolean('commSalesTypeTr')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_treatments', function (Blueprint $table) {
            $table->dropColumn('commissionSalesDr');
            $table->dropColumn('commissionSalesTr');
            $table->dropColumn('commSalesTypeDr');
            $table->dropColumn('commSalesTypeTr');
            
        });
    }
}

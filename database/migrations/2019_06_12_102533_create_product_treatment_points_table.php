<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTreatmentPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_treatment_points', function (Blueprint $table) {
            $table->bigIncrements('id_product_treatment_points');
            $table->unsignedBigInteger('product_treatment_id');
            $table->integer('point');
            $table->timestamps();
            //index foreign key
            $table->foreign('product_treatment_id')->references('id_product_treatment')->on('product_treatments')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_treatment_points');
    }
}

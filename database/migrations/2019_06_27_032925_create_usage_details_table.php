<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsageDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usage_details', function (Blueprint $table) {
            $table->bigIncrements('id_usage_detail');
            $table->integer('usage_id');
            $table->string('inv_code');
            $table->integer('product_id');
            $table->integer('invoice_package_id');
            $table->integer('value');
            $table->integer('qty');
            $table->integer('used');
            $table->integer('duration');
            $table->integer('point');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usage_details');
    }
}

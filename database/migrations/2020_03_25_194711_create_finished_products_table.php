<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinishedProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finished_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code')->unique();
            $table->unsignedBigInteger('work_order_detail_id');
            $table->unsignedBigInteger('item_id');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('checked_by');
            $table->unsignedBigInteger('warehouse_destination_id');
            $table->unsignedBigInteger('rack_destination_id');
            $table->date('checked_at');
            $table->integer('qty');
            $table->enum('marked_as', ['ACCEPTED', 'REJECTED', 'CANCELED']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finished_products');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activities', function (Blueprint $table) {
            $table->bigInteger('product_id')->nullable()->after('status_activity')->unsigned();
            $table->bigInteger('package_id')->nullable()->after('status_activity')->unsigned();

            $table->foreign('product_id')->references('id_product_treatment')->on('product_treatments');
            $table->foreign('package_id')->references('id_package')->on('packages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activities', function (Blueprint $table) {
        });
    }
}

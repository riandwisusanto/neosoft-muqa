<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retur_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('retur_id');
            $table->integer('item_id');
            $table->integer('uom_id');
            $table->text('description');
            $table->integer('qty');
            $table->integer('price');
            $table->integer('disc');
            $table->integer('tax');
            $table->integer('total');
            $table->timestamps();
            $table->string('created_uid', 85)->nullable();
            $table->string('updated_uid', 85)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retur_details');
    }
}

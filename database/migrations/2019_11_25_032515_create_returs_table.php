<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRetursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('returs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('retur_num', 45);
            $table->date('retur_date');
            $table->integer('warehouse_id');
            $table->integer('supplier_id');
            $table->text('description');
            $table->string('invoice_no', 45);
            $table->date('invoice_date');
            $table->string('pay_method');
            $table->date('due_date');
            $table->integer('receive_id');
            $table->timestamps();
            $table->string('created_uid', 85)->nullable();
            $table->string('updated_uid', 85)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('returs');
    }
}

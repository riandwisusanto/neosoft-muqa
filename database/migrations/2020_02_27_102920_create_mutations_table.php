<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMutationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mutations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('warehouse_origin_id')->nullable();
            $table->unsignedBigInteger('warehouse_destination_id')->nullable();
            $table->unsignedBigInteger('rack_origin_id')->nullable();
            $table->unsignedBigInteger('rack_destination_id')->nullable();
            $table->unsignedBigInteger('sales_invoice_id')->nullable();
            $table->unsignedBigInteger('purchase_invoice_id')->nullable();
            $table->unsignedBigInteger('material_release_id')->nullable();
            $table->unsignedBigInteger('adjustment_id')->nullable();
            $table->unsignedBigInteger('finished_product_id')->nullable();
            $table->unsignedBigInteger('ref_id')->nullable();
            $table->unsignedBigInteger('item_id');
            $table->date('transdate');
            $table->double('price', 11, 2)->default(0.00);
            $table->double('cogs', 11, 2)->default(0.00);
            $table->enum('transtype', ['in', 'out']);
            $table->text('transdesc')->nullable();
            $table->integer('qty');
            $table->bigInteger('balance');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mutations');
    }
}

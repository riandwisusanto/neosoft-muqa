<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('racks', function (Blueprint $table) {
            $table->bigIncrements('id_rack');
            $table->string('main_rack_code', 45);
            $table->string('sub_rack_code', 45);
            $table->string('description', 45);
            $table->integer('warehouse_id');
            $table->string('is_active', 1)->default(0);
            $table->timestamps();
            $table->string('created_uid', 85)->nullable();
            $table->string('updated_uid', 85)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('racks');
    }
}

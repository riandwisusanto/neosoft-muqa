<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryAdjustmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_adjustments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('item_id');
            $table->unsignedBigInteger('ref_id')->nullable();
            $table->unsignedBigInteger('warehouse_id')->nullable();
            $table->unsignedBigInteger('adjusted_by')->nullable();
            $table->string('code');
            $table->date('transdate');
            $table->text('transdesc');
            $table->text('reason');
            $table->enum('stock_adjustment_type', ['in', 'out']);
            $table->bigInteger('stock_before');
            $table->bigInteger('stock_adjustment');
            $table->bigInteger('stock_after');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_adjustments');
    }
}

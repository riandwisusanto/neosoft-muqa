<?php

use Illuminate\Database\Seeder;

class Package extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('packages')->insert(
            array(
                array('id_package' => '354','package_code' => 'T0159','package_type' => '2','package_name' => 'Peeling C','price' => '750000', 'remarks' => '','status_package' => '1'),
                array('id_package' => '355','package_code' => 'C0180','package_type' => '1','package_name' => 'Dermis Silver Glow 2 (10g)','price' => '140000', 'remarks' => '','status_package' => '1'),
                array('id_package' => '356','package_code' => 'C0181','package_type' => '1','package_name' => 'Dermis Gold White + 1 (15g)','price' => '390000', 'remarks' => '','status_package' => '1'),
                array('id_package' => '357','package_code' => 'C0182','package_type' => '1','package_name' => 'Neck Silver H (15g)','price' => '220000', 'remarks' => '','status_package' => '1'),
                array('id_package' => '358','package_code' => 'C0183','package_type' => '1','package_name' => 'Dermis Diamond H (15g)','price' => '320000', 'remarks' => '','status_package' => '1'),
                array('id_package' => '359','package_code' => 'P0241','package_type' => '1','package_name' => 'Set Kompresan','price' => '75000', 'remarks' => '','status_package' => '1'),
                array('id_package' => '360','package_code' => 'C0184','package_type' => '1','package_name' => 'Mouisturizer H + (50g)','price' => '580000', 'remarks' => '','status_package' => '1'),
                array('id_package' => '361','package_code' => 'C0185','package_type' => '1','package_name' => 'Antibiotik Cream 1','price' => '220000', 'remarks' => '','status_package' => '1'),
                array('id_package' => '362','package_code' => 'C0251','package_type' => '1','package_name' => 'DDC Silver H M Gel (10g)','price' => '140000', 'remarks' => '','status_package' => '1'),
                array('id_package' => '363','package_code' => 'C0186','package_type' => '1','package_name' => 'Dermis Platinum Glow (10g)','price' => '150000', 'remarks' => '','status_package' => '1'),
                array('id_package' => '364','package_code' => 'C0187','package_type' => '1','package_name' => 'DDC A B (10g)','price' => '125000', 'remarks' => '','status_package' => '1'),
                array('id_package' => '365','package_code' => 'C0188','package_type' => '1','package_name' => 'Sunscreen 2 (50g)','price' => '235000', 'remarks' => '','status_package' => '1'),
                array('id_package' => '366','package_code' => 'C0189','package_type' => '1','package_name' => 'Dermis Diamond White + (10g)','price' => '330000', 'remarks' => '','status_package' => '1'),
                array('id_package' => '367','package_code' => 'C0190','package_type' => '1','package_name' => 'DDC X C (10g)','price' => '150000', 'remarks' => '','status_package' => '1'),
                array('id_package' => '368','package_code' => 'C0191','package_type' => '1','package_name' => 'Dermis Gold White 1 (15g)','price' => '420000', 'remarks' => '','status_package' => '1'),
                array('id_package' => '369','package_code' => 'C0195','package_type' => '1','package_name' => 'Dermis Glow A (15g)','price' => '200000', 'remarks' => '','status_package' => '1'),
                array('id_package' => '370','package_code' => 'C0197','package_type' => '1','package_name' => 'Dermis Gold Silver Glow 3 (15g)','price' => '320000', 'remarks' => '','status_package' => '1'),
                array('id_package' => '371','package_code' => 'C0192','package_type' => '1','package_name' => 'DDC Silver H (15g)','price' => '230000', 'remarks' => '','status_package' => '1'),
                array('id_package' => '372','package_code' => 'T0160','package_type' => '2','package_name' => 'Micro Botox','price' => '150000', 'remarks' => '','status_package' => '1'),
                array('id_package' => '373','package_code' => 'T0161','package_type' => '1','package_name' => 'Serum Dermapen','price' => '280000', 'remarks' => '','status_package' => '1'),
                array('id_package' => '374','package_code' => 'OM0010','package_type' => '1','package_name' => 'Antibiotik LV','price' => '10000', 'remarks' => '','status_package' => '1'),
                array('id_package' => '375','package_code' => 'OM0011','package_type' => '1','package_name' => 'Antihistamin CZ','price' => '14000', 'remarks' => '','status_package' => '1')
            )
        );
    }
}

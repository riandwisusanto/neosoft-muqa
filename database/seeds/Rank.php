<?php

use Illuminate\Database\Seeder;

class Rank extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ranks')->insert(
            array(
                array('id_rank' => '1','name' => 'RED','desc' => '> New Client will get a 5% discount when buying a series of non-package package.
            > There is no product discount.','value_1' => '1','value_2' => '5000000','created_at' => '2019-11-10 10:09:31','updated_at' => '2019-11-15 12:57:36'),
                array('id_rank' => '2','name' => 'SILVER','desc' => '> Entitled to get discounts of up to 10% for purchasing treatment series packages / 5 non package packages
            > There is no product discount','value_1' => '5000000','value_2' => '9100000','created_at' => NULL,'updated_at' => '2019-11-15 12:58:27'),
                array('id_rank' => '3','name' => 'GOLD','desc' => '> Entitled to get discounts of up to 20% for purchasing treatment series packages / 5 non package packages
            > There is no product discount','value_1' => '9100000','value_2' => '15100000','created_at' => '2019-11-15 12:59:10','updated_at' => '2019-11-15 12:59:10'),
                array('id_rank' => '4','name' => 'PLATINUM','desc' => '> Entitled to get discounts of up to 25% for purchasing treatment series packages / 5 non package packages.
            > Eligible to get a 5% Max discount for product purchases.','value_1' => '15100000','value_2' => '1000000000','created_at' => '2019-11-15 13:08:02','updated_at' => '2019-11-15 13:08:02'),
                array('id_rank' => '5','name' => 'NEW','desc' => '-','value_1' => '0','value_2' => '0','created_at' => '2019-11-15 13:08:38','updated_at' => '2019-11-15 13:08:38')
            )
        );
    }
}

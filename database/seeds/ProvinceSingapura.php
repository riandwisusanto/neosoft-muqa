<?php

use Illuminate\Database\Seeder;

class ProvinceSingapura extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('provinces')->insert(array(
            [
                'country_id' => 2,
                'province' => 'Singapore',
            ],
        ));
    }
}

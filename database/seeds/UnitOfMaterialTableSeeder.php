<?php

use Illuminate\Database\Seeder;

class UnitOfMaterialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id'                        => 1,
                'display'                   => 'Kilogram',
                'alias'                     => 'Kg',
                'smallest_unit'             => 'mg',
                'smallest_unit_alias'       => 'miligram',
                'numerator'                 => 1,
                'denominator'               => 1000
            ],
            [
                'id'                        => 2,
                'display'                   => 'Ons',
                'alias'                     => 'ons',
                'smallest_unit'             => 'mg',
                'smallest_unit_alias'       => 'miligram',
                'numerator'                 => 3,
                'denominator'               => 100
            ],
            [
                'id'                        => 4,
                'display'                   => 'miligram',
                'alias'                     => 'mg',
                'smallest_unit'             => 'mg',
                'smallest_unit_alias'       => 'miligram',
                'numerator'                 => 1,
                'denominator'               => 1
            ],
            [
                'id'                        => 5,
                'display'                   => 'Liter',
                'alias'                     => 'L',
                'smallest_unit'             => 'ml',
                'smallest_unit_alias'       => 'mililiter',
                'numerator'                 => 1,
                'denominator'               => 1000
            ],
            [
                'id'                        => 6,
                'display'                   => 'mililiter',
                'alias'                     => 'ml',
                'smallest_unit'             => 'ml',
                'smallest_unit_alias'       => 'mililiter',
                'numerator'                 => 1,
                'denominator'               => 1
            ],
        ];

        DB::table('uoms')->insert($data);
    }
}

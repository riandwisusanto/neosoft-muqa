<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConsultantDetailTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('consultant_details')->insert(
            array(
                array('consultant_detail_id' => '1','consultant_id' => '1','outlet_id' => '1'),
                array('consultant_detail_id' => '2','consultant_id' => '2','outlet_id' => '1'),
                array('consultant_detail_id' => '3','consultant_id' => '3','outlet_id' => '1')
            )
        );
    }
}

<?php

use Illuminate\Database\Seeder;

class Outlet extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('outlets')->insert(
            array(
                array('id_outlet' => '1','outlet_name' => 'Neosoft','outlet_code' => 'Neo','address' => 'ITC Fatnawati, Jakarta Selatan, DKI Jakarta','outlet_phone' => '021-888888','outlet_fax' => '021-888888','status_outlet' => '1','logo' => 'ESL.png'),
        )
        );
    }
}

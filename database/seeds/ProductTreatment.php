<?php

use Illuminate\Database\Seeder;

class ProductTreatment extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_treatments')->insert(
            array(
                array('id_product_treatment' => '441','product_treatment_code' => 'T-0159','product_treatment_name' => 'Peeling C','duration' => '0', 'price' => '0','status_product_treatment' => '1'),
                array('id_product_treatment' => '442','product_treatment_code' => 'P-0248','product_treatment_name' => 'Hyalumoist (8g)','duration' => '0', 'price' => '0','status_product_treatment' => '1'),
                array('id_product_treatment' => '443','product_treatment_code' => 'P-0249','product_treatment_name' => 'Hyalumoist (25g)','duration' => '0', 'price' => '0','status_product_treatment' => '1'),
                array('id_product_treatment' => '444','product_treatment_code' => 'P-0250','product_treatment_name' => 'Mofacord (25g)','duration' => '0', 'price' => '0','status_product_treatment' => '1'),
                array('id_product_treatment' => '445','product_treatment_code' => 'P-0251','product_treatment_name' => 'Antibiotik Cream 1','duration' => '0', 'price' => '0','status_product_treatment' => '1'),
                array('id_product_treatment' => '446','product_treatment_code' => 'P-0252','product_treatment_name' => 'Celeteque SPF 30 (50g)','duration' => '0', 'price' => '0','status_product_treatment' => '1'),
                array('id_product_treatment' => '447','product_treatment_code' => 'P-0253','product_treatment_name' => 'Xeraclm avane','duration' => '0', 'price' => '0','status_product_treatment' => '1'),
                array('id_product_treatment' => '448','product_treatment_code' => 'P-0254','product_treatment_name' => 'Eye Tuck (10,5g)','duration' => '0', 'price' => '0','status_product_treatment' => '1'),
                array('id_product_treatment' => '449','product_treatment_code' => 'P-0255','product_treatment_name' => 'Aha Lotion (8g)','duration' => '0', 'price' => '0','status_product_treatment' => '1'),
                array('id_product_treatment' => '450','product_treatment_code' => 'P-0256','product_treatment_name' => 'Skin Enhancer (4g)','duration' => '0', 'price' => '0','status_product_treatment' => '1'),
                array('id_product_treatment' => '451','product_treatment_code' => 'P-0257','product_treatment_name' => 'Vitacid 0,05 (4g)','duration' => '0', 'price' => '0','status_product_treatment' => '1'),
                array('id_product_treatment' => '452','product_treatment_code' => 'P-0258','product_treatment_name' => 'Melanox (7g)','duration' => '0', 'price' => '0','status_product_treatment' => '1'),
                array('id_product_treatment' => '453','product_treatment_code' => 'T-0160','product_treatment_name' => 'Micro Botox','duration' => '0', 'price' => '0','status_product_treatment' => '1'),
                array('id_product_treatment' => '454','product_treatment_code' => 'T-0161','product_treatment_name' => 'Serum Dermapen','duration' => '0', 'price' => '0','status_product_treatment' => '1'),
                array('id_product_treatment' => '455','product_treatment_code' => 'P-0259','product_treatment_name' => 'Antihistamin Cetirizen','duration' => '0', 'price' => '0','status_product_treatment' => '1')
            )
        );
    }
}

<?php

use Illuminate\Database\Seeder;

class ActivityGroup extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('activity_groups')->insert(
            array(
                array('id_activity_group' => '1','activity_group_name' => 'Digital','extra_field' => '0','status_activity_group' => '1')
            )
        );
    }
}

<?php

use Illuminate\Database\Seeder;

class Customer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert(
            array(
                array('induk_customer' => 'EM-120190840','full_name' => 'Elvira Rumangu','phone' => '085886208489','birth_place' => 'Amurang','birth_date' => '1989-10-18','join_date' => '2019-06-01','gender' => 'F','ktp' => '7105105810890001','address' => 'Buyungon-lingkungan IV','occupation' => '','email' => 'vir@gmail.com','staf_id' => '161','agen_id' => '0','religion' => '3','country_id' => '64','city_id' => '0','zip' => '0','status_customer' => '2','nextofikin' => '','nextofikin_number' => '0','source_id' => '7','source_remarks' => '','purpose_treatment' => ''),
                array('induk_customer' => 'EM-120190841','full_name' => 'Niken Sumarni','phone' => '081273149474','birth_place' => 'Klaten','birth_date' => '1977-06-13','join_date' => '2019-06-01','gender' => 'F','ktp' => '1904025306770001','address' => 'Jl. Manunggal beluluk','occupation' => '','email' => 'sum@gmail.com','staf_id' => '161','agen_id' => '0','religion' => '1','country_id' => '64','city_id' => '31','zip' => '0','status_customer' => '2','nextofikin' => '','nextofikin_number' => '0','source_id' => '7','source_remarks' => '','purpose_treatment' => ''),
                array('induk_customer' => 'EM-120190842','full_name' => 'Sania Dysa Hardi','phone' => '08127554450','birth_place' => 'Pekanbaru','birth_date' => '1996-06-09','join_date' => '2019-06-01','gender' => 'F','ktp' => '1471084906960023','address' => 'Jl HR Soebrantas','occupation' => '','email' => 'sania@gmail.com','staf_id' => '162','agen_id' => '0','religion' => '1','country_id' => '64','city_id' => '31','zip' => '0','status_customer' => '1','nextofikin' => '','nextofikin_number' => '0','source_id' => '5','source_remarks' => '','purpose_treatment' => ''),
                array('induk_customer' => 'EM-120190843','full_name' => 'Armi Sandra','phone' => '08127554450','birth_place' => 'Padang','birth_date' => '1972-02-25','join_date' => '2019-06-01','gender' => 'F','ktp' => '1471086502720001','address' => 'Jl HR Soebrantas','occupation' => '','email' => 'mi@gmail.com','staf_id' => '162','agen_id' => '0','religion' => '1','country_id' => '64','city_id' => '31','zip' => '0','status_customer' => '2','nextofikin' => '','nextofikin_number' => '2147483647','source_id' => '5','source_remarks' => '','purpose_treatment' => ''),
                array('induk_customer' => 'EM-120190844','full_name' => 'Aryanti Erika Saroso','phone' => '021565874623','birth_place' => 'Jakarta','birth_date' => '1961-06-07','join_date' => '2019-06-01','gender' => 'F','ktp' => '610612050695','address' => 'Jl. Bungur No.8','occupation' => '','email' => 'nti@gmail.com','staf_id' => '162','agen_id' => '0','religion' => '1','country_id' => '64','city_id' => '31','zip' => '0','status_customer' => '2','nextofikin' => '','nextofikin_number' => '0','source_id' => '7','source_remarks' => '','purpose_treatment' => ''),
                array('induk_customer' => 'EM-120190845','full_name' => 'Anggi  Anggraeni','phone' => '081380182250','birth_place' => 'Bogor','birth_date' => '1993-08-09','join_date' => '2019-06-01','gender' => 'F','ktp' => '3271014908930015','address' => 'Dekeng','occupation' => '','email' => 'ang@gmail.com','staf_id' => '162','agen_id' => '0','religion' => '1','country_id' => '64','city_id' => '18','zip' => '0','status_customer' => '1','nextofikin' => '','nextofikin_number' => '0','source_id' => '7','source_remarks' => '','purpose_treatment' => ''),
                array('induk_customer' => 'EM-120190846','full_name' => 'Dwi Pudyasmoro Basuki Ari','phone' => '02564851652','birth_place' => 'Surakarta','birth_date' => '1967-10-15','join_date' => '2019-06-01','gender' => 'M','ktp' => '6474011510670004','address' => 'Jl. Gn Jaya Wijaya No.21','occupation' => '','email' => 'basuki@gmail.com','staf_id' => '162','agen_id' => '0','religion' => '1','country_id' => '64','city_id' => '19','zip' => '0','status_customer' => '2','nextofikin' => '','nextofikin_number' => '0','source_id' => '5','source_remarks' => '','purpose_treatment' => ''),
                array('induk_customer' => 'EM-120190847','full_name' => 'M. Mawardi Saleh','phone' => '08121908100','birth_place' => 'Jakarta','birth_date' => '1970-09-01','join_date' => '2019-06-01','gender' => 'F','ktp' => '3171070109700001','address' => 'Jl. Bend Hilir VIII/9','occupation' => '','email' => 'sal@gmail.com','staf_id' => '162','agen_id' => '0','religion' => '1','country_id' => '64','city_id' => '30','zip' => '0','status_customer' => '1','nextofikin' => '','nextofikin_number' => '0','source_id' => '5','source_remarks' => '','purpose_treatment' => '')
            )
        );
    }
}

<?php

use Illuminate\Database\Seeder;

class Therapist extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('therapists')->insert(
            array(
                array('id_therapist' => '1','therapist_name' => 'dr. Rubby Aditya, Sp.KK','group_id' => '1','join_date' => '2019-05-28','status_therapist' => '1'),
                array('id_therapist' => '2','therapist_name' => 'dr. Hafiza Fikri Fadel, Sp.KK','group_id' => '1','join_date' => '2019-05-28','status_therapist' => '1'),
                array('id_therapist' => '3','therapist_name' => 'dr. Melyarna Putri, M.Gz','group_id' => '1','join_date' => '2019-05-28','status_therapist' => '1'),
                array('id_therapist' => '4','therapist_name' => 'dr. Ayu Diandra Sari, M.Gz','group_id' => '1','join_date' => '2019-05-28','status_therapist' => '1'),
                array('id_therapist' => '5','therapist_name' => 'Nurse Chaesar Dwi Anisa','group_id' => '1','join_date' => '2019-05-28','status_therapist' => '1'),
                array('id_therapist' => '6','therapist_name' => 'Syifa Shika Novianti','group_id' => '1','join_date' => '2019-05-28','status_therapist' => '1'),
                array('id_therapist' => '7','therapist_name' => 'Rosneli','group_id' => '1','join_date' => '2019-05-28','status_therapist' => '1'),
                array('id_therapist' => '8','therapist_name' => 'Rofiqoh cahya','group_id' => '1','join_date' => '2019-05-28','status_therapist' => '1'),
                array('id_therapist' => '9','therapist_name' => 'Eva norita','group_id' => '1','join_date' => '2019-05-28','status_therapist' => '1'),
                array('id_therapist' => '10','therapist_name' => 'dr. Astri Adelia, Sp.KK','group_id' => '1','join_date' => '2019-07-02','status_therapist' => '1'),
                array('id_therapist' => '11','therapist_name' => 'Ruth','group_id' => '1','join_date' => '2019-07-16','status_therapist' => '1')
            )
        );
    }
}

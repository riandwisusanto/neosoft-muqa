<?php

use Illuminate\Database\Seeder;

class Country extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert(array(
            [
             'id_country' => '1',
             'country' => 'INDONESIA',
            ],
            [
             'id_country' => '2',
             'country' => 'SINGAPORE',
            ],
        ));
    }
}

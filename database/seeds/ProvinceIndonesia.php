<?php

use Illuminate\Database\Seeder;

class ProvinceIndonesia extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $url_city = "https://api.rajaongkir.com/starter/city?key=add4fffd108b6bfd316850ad837cb86c";
        $json_str = file_get_contents($url_city);
        $json_obj = json_decode($json_str);
        $province = [];
        
        foreach ($json_obj->rajaongkir->results as $provinces) {
            $province[] = [
                'id_province' => $provinces->city_id,
                'country_id' => 1,
                'province' => $provinces->city_name,
            ];
        }

        DB::table('provinces')->insert($province);
    }
}

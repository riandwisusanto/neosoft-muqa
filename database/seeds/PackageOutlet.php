<?php

use Illuminate\Database\Seeder;

class PackageOutlet extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('package_outlets')->insert(
            array(
                array('id_package_outlets' => '522','package_id' => '353','outlet_id' => '1'),
                array('id_package_outlets' => '523','package_id' => '354','outlet_id' => '1'),
                array('id_package_outlets' => '526','package_id' => '355','outlet_id' => '1'),
                array('id_package_outlets' => '527','package_id' => '356','outlet_id' => '1'),
                array('id_package_outlets' => '528','package_id' => '357','outlet_id' => '1'),
                array('id_package_outlets' => '529','package_id' => '358','outlet_id' => '1'),
                array('id_package_outlets' => '530','package_id' => '359','outlet_id' => '1'),
                array('id_package_outlets' => '531','package_id' => '360','outlet_id' => '1'),
                array('id_package_outlets' => '532','package_id' => '361','outlet_id' => '1'),
                array('id_package_outlets' => '533','package_id' => '362','outlet_id' => '1'),
                array('id_package_outlets' => '535','package_id' => '363','outlet_id' => '1'),
                array('id_package_outlets' => '536','package_id' => '364','outlet_id' => '1'),
                array('id_package_outlets' => '537','package_id' => '365','outlet_id' => '1'),
                array('id_package_outlets' => '538','package_id' => '366','outlet_id' => '1'),
                array('id_package_outlets' => '539','package_id' => '367','outlet_id' => '1'),
                array('id_package_outlets' => '540','package_id' => '368','outlet_id' => '1'),
                array('id_package_outlets' => '545','package_id' => '369','outlet_id' => '1'),
                array('id_package_outlets' => '546','package_id' => '370','outlet_id' => '1'),
                array('id_package_outlets' => '549','package_id' => '371','outlet_id' => '1'),
                array('id_package_outlets' => '550','package_id' => '372','outlet_id' => '1'),
                array('id_package_outlets' => '551','package_id' => '373','outlet_id' => '1'),
                array('id_package_outlets' => '552','package_id' => '374','outlet_id' => '1'),
                array('id_package_outlets' => '553','package_id' => '375','outlet_id' => '1')
            )
        );
    }
}

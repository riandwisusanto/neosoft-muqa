<?php 

return [
    'dashboard' => 'DASHBOARD',
    'appointments' => 'APPOINTMENTS',
    'room' => 'by Room',
    'therapist' => 'Therapists/Doctors',
    'byoutlets' => 'by Outlets',
    'settings' => 'SETTINGS',
    'useraccess' => 'User Access',
    'outlet' => 'Outlet',
    'sales' => 'SALES',
    'createinvoice' => 'Create Invoice',
    'invoicebalance' => 'Invoice Balance',
    'convertinvoice' => 'Convert Invoice',
    'redeempoint' => 'Redeem Point',
    'patient' => 'PATIENT',
    'patientlist' => 'Patient',
    'inactivepatient' => 'Inactive Patient',
    'patientbirthday' => "Patient's Birthday",
    'salesagents' => 'SALES AGENTS',
    'agentregister' => 'Agent Register',
    'agentlist' => 'Agent List',
    'activeagent' => 'Active Agent',
    'management' => 'MANAGEMENT',
    'treatments' => 'Treatments',
    'packages' => 'Packages',
    'banks' => 'Banks',
    'consultants' => 'Staff',
    'groups' => 'Groups',
    'marketingactivites' => 'Marketing Activities',
    'marketingsources' => 'Marketing Sources',
    'rooms' => 'Rooms',
    'city' => 'City',
    'country' => 'Country',
    'productpoints' => 'Product Points',
    'loyaltyranks' => 'Loyalty Ranks',
    'logistics' => 'LOGISTICS',
    'master' => 'Master',
    'supplier' => 'Supplier',
    'warehouse' => 'Warehouse',
    'rack' => 'Rack',
    'item' => 'Item',
    'purchasing' => 'Purchasing',
    'purchaseorder' => 'Purchase Order',
    'receiveitem' => 'Receive Item',
    'warehousing' => 'Warehousing',
    'stockcard' => 'Stock Card',
    'stockopname' => 'Stock Opname',
    'inventoryadjustment' => 'Inventory Adjustment',
    'manufacture' => 'MANUFACTURE',
    'workorder' => 'Work Order',
    'materialrelease' => 'Material Release',
    'finishedproduct' => 'Finished Product',
    'onlineshop' => 'ONLINE SHOP',
    'products' => 'Products',
    'salesorders' => 'Sales Orders',
    'doctorreview' => 'DOCTOR REVIEW',
    'reports' => 'REPORTS',
    'collectionbyoutlet' => 'Collection by Outlet',
    'collectionbyinvoice' => 'Collection by Invoice',
    'collectionbyactivity' => 'Collection by Activity',
    'collectionbypaymenttype' => 'Collection by Payment Type',
    'itemsoldbysales' => 'Item Sold by Sales',
    'itemsoldbycollection' => 'Item Sold by Collection',
    'treatmentdonebyoutlet' => 'Treatment Done by Outlet',
    'treatmentdonebytherapist' => 'Treatment Done by Therapist',
    'productdonebyoutlet' => 'Product Done by Outlet',
    'uerevenue' => 'U/E Revenue',
    'salescommission' => 'Sales Commission',
    'consultantcommission' => 'Consultant Commission',
    'therapistcommission' => 'Therapist Commission',
    'treatmentcommission' => 'Treatment Commission',
    'helpdesk' => 'Helpdesk',
    'survey' => 'Survey',
    'help' => 'Video Tutorial',
];

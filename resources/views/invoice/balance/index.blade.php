@extends('base')

@section('title')
Invoice Balance
@endsection

@section('breadcrumb')
@parent
<li>Invoice Balance</li>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <span style="font-size:20px; font-weight: bold;">INVOICE BALANCE</span>
                <p style="font-size:15px; font-weight: bold;"><i>Create invoice balance for down payment.</i></p>
            </div>
            <form class="form form-balance" data-toggle="validator" method="post">
                {{ csrf_field() }} {{ method_field('POST') }}
                <div class="box-body">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <label class="col-md-2 control-label">Customer *</label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="customer" name="customer" autofocus="on"
                                    required>
                                <input type="hidden" name="customer_id" id="customer_id">
                                <div id="suggesstion-box"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Full Name</label>
                            <div class="col-md-10">
                                <p class="form-control-static" id="full"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Contact No</label>
                            <div class="col-md-10">
                                <p class="form-control-static" id="contact"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Date of Birth</label>
                            <div class="col-md-10">
                                <p class="form-control-static" id="birthDate"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Outlet</label>
                            <div class="col-md-5">
                                <select id="outlet" name="outlet" class="form-control select2 select2-hidden-accessible"
                                    style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    @if (count($outlet) >= 1)
                                        <option selected="selected" value="">-- Select Outlet --</option>
                                    @endif
                                    @foreach ($outlet as $list)
                                    <option value="{{ $list->id_outlet }}">{{ $list->outlet_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Payment Date</label>
                            <div class="col-md-5">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="date_balance"
                                        name="date_balance" name="date" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Invoice</label>
                            <div class="col-md-5">
                                <select id="invoice_outstanding" name="invoice_outstanding"
                                    class="form-control select2 select2-hidden-accessible" style="width: 100%;"
                                    tabindex="-1" aria-hidden="true" required>

                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Invoice Detail</label>
                            <div class="col-md-10">
                                <div class="form-horizontal">
                                    <div class="col-md-3">
                                        <label>Total</label>
                                        <input type="text" id="total_paid" class="form-control" disabled required>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Paid</label>
                                        <input type="text" id="total_tmp" class="form-control" disabled required>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Outstanding</label>
                                        <input type="text" id="outstanding" name="outstanding" class="form-control" readonly required>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Invoice Date</label>
                                        <input type="text" id="invoice_date" class="form-control" disabled required>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Payment(s)</label>
                            <div class="col-md-10">
                                <div class="form-horizontal">
                                    <div class="col-md-3">
                                        <label>Payment Method</label>
                                        <select class="form-control" name="payment_type[]" required="on">
                                            @foreach ($bank as $item)
                                            <option value="{{ $item->id_bank }}">{{ $item->bank_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Amount</label>
                                        <input type="text" class="form-control" id="amount" name="amount[]" rel="amount"
                                            placeholder="Amount(s)" required>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Info</label>
                                        <input type="text" class="form-control" id="payment_info" name="payment_info[]"
                                            placeholder="Info">
                                    </div>
                                    <div class="col-md-1">
                                        <label>Action</label>
                                        <p>
                                            <input id="pay" value="1" type="hidden">
                                            <button type="button" id="add" class="btn btn-info btn-xs"
                                                onclick="addPaymant()"><i class="fa fa-plus"></i></button>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="DivPayment"></div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Paid Balance </label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="paid" name="paid" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Unpaid Balance </label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="unpaid" name="unpaid" value="0" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Remarks </label>
                            <div class="col-md-5">
                                <textarea class="form-control" name="remarks">Balance :</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer text-right">
                    <button type="submit" class="btn btn-primary btn-save simpan"><i class="fa fa-floppy-o"></i> Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('script')

<script>
    $('#date_balance').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    $('#date_balance').datepicker('setDate', 'today')
    $(function(){
        findCustomer();
        loadInvoice();
        hitung();

        $('.form-balance').validator().on('submit', function(e){
            if(!e.isDefaultPrevented()){
                let id = $('#customer_id').val();
                let id_invoice = $('#invoice_outstanding').val();
                $.ajax({
                    url : "{{ route('invoice-balance.store') }}",
                    type : "POST",
                    data : $('.form-balance').serialize(),
                    success : function(data){                    
                        swal({
                            title: "Successfully Create Invoice",
                            icon: "success",
                            buttons: ["Finish", "Print"],
                            dangerMode: true,
                        })
                        .then((print) => {
                            if (print) {
                                window.open("/invoice-balance/"+id_invoice+"/print")
                                window.location.href = "/invoice-balance/"+data.id_balance;
                            } else {
                                window.location.href = "/invoice-balance/"+data.id_balance;
                            }
                        });
                    },
                    error : function(){
                        alert("Can not Save the Data!");
                    }
                });
                return false;
            }
        });
    });
    function addPaymant() {
        hitung();
        var _l = $("input[rel=amount]").length+1;
        var inputbaru = "<div class='form-group' id='row" + _l + "'>"+
                "<label class='col-md-2 control-label'></label>"+
                    "<div class='col-md-10'>"+
                        "<div class='form-horizontal'>"+
                            "<div class='col-md-3'>"+
                                "<label>Payment Method ("+ _l +")</label>"+
                                "<select class='form-control' name='payment_type[]' required='on'>"+
                                    "@foreach ($bank as $item)"+
                                        "<option value='{{ $item->id_bank }}'>{{ $item->bank_name }}</option>"+
                                    "@endforeach"+
                                "</select>"+
                            "</div>"+
                            "<div class='col-md-3'>"+
                                "<label>Amount ("+ _l +")</label>"+
                                "<input type='text' class='form-control' id='amount' name='amount[]' rel='amount' placeholder='Amount(s)' required>"+
                            "</div>"+
                            "<div class='col-md-3'>"+
                                "<label>Info ("+ _l +")</label>"+
                                "<input type='text' class='form-control' id='payment_info' name='payment_info[]' placeholder='Info'>"+
                            "</div>"+
                            "<div class='col-md-1'>"+
                                "<label>Action</label>"+
                                "<button class='  btn btn-info btn-xs' onclick='hapusPayment(\"#row" + _l + "\"); return false;'><i class='fa fa-minus'></i></button>"+
                            "</div>"+
                        "</div>"+
                    "</div>";

        $("#DivPayment").append(inputbaru);
        hitung();
        return false;
    }
    function hitung() {
        $("input[rel=amount]").bind('keyup',function(){
            var total = $('#outstanding').val().replace(/[^0-9]/g,'');
            
            var awal = 0;
            $("input[rel=amount]").each(function(){
                this.value=this.value.replace(/[^0-9]/g,'');
                if(this.value !='') awal += parseInt(this.value,10);
            });

            let totalpaid = total-awal;      
            
            $('#paid').val(awal.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
            $('#unpaid').val(totalpaid.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
            $('#unpaidinv').val(total-awal);
            // $('#paid').val(awal);
            // $('#unpaid').val(total-awal);
            // $('#unpaidinv').val(total-awal);
        });
    }

    function hapusPayment(pay) {
        $(pay).remove();
        hitung();
	}

    var typingTimer, waitTyping = 1250;
    function findCustomer(){
        $('#customer').keyup(function(){ 
            var query = $(this).val();
            
            clearTimeout(typingTimer);
            typingTimer = setTimeout(function () {
                lookupCustomer(query)
            }, waitTyping)
        });

        $('#customer').keydown(function() {
            clearTimeout(typingTimer)
        })
    }

        /**
        * Lookup customer data after user finish typing.
        */
        function lookupCustomer(query) {
            if(query != ''){
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('customer.keyword') }}",
                    method:"POST",
                    data:{query:query, _token:_token},
                    dataType: "json",
                    success:function(data){          
                        $('#suggesstion-box').fadeIn();  
                        $('#suggesstion-box').html(data);
                    }
                });
            }else{
                var _token = $('input[name="_token"]').val();
                $.ajax({
                url:"{{ route('customer.all') }}",
                method:"POST",
                data:{query:query, _token:_token},
                dataType: "json",
                success:function(data){
                    $('#suggesstion-box').fadeIn();  
                    $('#suggesstion-box').html(data);
                }
                });
            }
    }

    function detailCustomer(id, induk, name, phone, birth) {
        $('#customer').val(induk);  
        $('#suggesstion-box').fadeOut();  
        $('#customer_id').val(id);
        $('#full').html(name);
        $('#contact').text(phone);
        $('#birthDate').text(birth);

        $.ajax({
            url : "/invoice-balance/"+id+"/invoice",
            type: 'GET',
            dataType: "JSON",
            success: function(data) { 
                 $('#invoice_outstanding').html(data);
            },
            error : function () {
                alert("Can not Show the Data!");
            }
        });
    }

    function remove_format(money) {
        return money.replace(/[^0-9]/g,'');
    }

    function format_money(money){
        return money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }

    function loadInvoice() {
        $('#invoice_outstanding').change(function () {
            $.ajax({
                url: "invoice-balance/"+$(this).val()+"/loadInvoice",
                type: "GET",
                dataType: "JSON",
                success: function (data) {                    
                    let total_tmp = remove_format(data.total) - remove_format(data.remaining);
                    $('#total_paid').val(data.total);
                    $('#total_tmp').val(format_money(total_tmp));
                    $('#outstanding').val(data.remaining);
                    $('#invoice_date').val(data.inv_date);
                },
                error: function () {
                    alert("Can not Delete the Data!");
                }
            });
        });
    }
</script>
@endsection
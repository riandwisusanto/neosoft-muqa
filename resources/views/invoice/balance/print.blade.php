<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Balance Print</title>
    <style>
        * {
            font-size: 8pt;
            color: #000000;
            font-family: "Trebuchet MS", "Lucida Grande", "Lucida Sans Unicode", "Lucida Sans", Tahoma, sans-serif;
        }

        @page {
            margin: 0.3cm;
        }

        ul {
            margin: 0 -20;
            list-style: none;
        }

        ul li:before {
            content: "•";
            font-size: 1em;
            /* or whatever */
            padding-right: 5px;
        }


        .normal-header-td {
            border-bottom: 1px solid black;
        }

        .last-header-td {
            border-bottom: 1px solid black;
        }

        .block_center {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }

        .col-0_5,
        .col-1,
        .col-1_5,
        .col-2,
        .col-3,
        .col-4,
        .col-5,
        .col-6,
        .col-7,
        .col-8,
        .col-9,
        .col-10,
        .row {
            float: left;
            position: relative;
            min-height: 1px;
        }

        .col-0_5 {
            width: 5%;
        }

        .col-1 {
            width: 10%;
        }

        .col-1_5 {
            width: 15%;
        }

        .col-2 {
            width: 20%;
        }

        .col-3 {
            width: 30%;
        }

        .col-4 {
            width: 40%;
        }

        .col-5 {
            width: 50%;
        }

        .col-6 {
            width: 60%;
        }

        .col-7 {
            width: 70%;
        }

        .col-8 {
            width: 80%;
        }

        .col-9 {
            width: 90%;
        }

        .col-10 {
            width: 100%;
        }

        .floatRight {
            float: right;
        }

        .indentConditionParagraph {
            margin-left: 5px;
            width: 100%;
        }

        .indentConditions {
            float: left;
            width: 100%;
            margin-left: 15px;
        }

        .indentConditions-numbering {
            float: left;
            font-size: 9pt;
            vertical-align: top;
        }

        .indentConditions-bullets:before {
            content: "•";
            font-size: 1em;
            /* or whatever */
            padding-right: 5px;
        }

        .indentConditions-nestedConditions {
            float: left;
            width: 95%;
            margin-left: 5px;
        }

        .invoiceDetail-tbl {
            width: 100%;
            border: 1px solid black;
            min-height: 10em;
        }

        .invoiceDetail-tbl td {
            vertical-align: top;
        }

        .invoicePayment-tbl {
            width: 100%;
        }

        tr.lastRow {
            height: 30%;
        }

        .leftHeader {
            white-space: nowrap;
            text-align: right;
            width: 8%;
        }

        .leftHeaderData {
            width: 42%;
            padding-left: 10px;
        }

        .rightHeader {
            white-space: nowrap;
            text-align: right;
            width: 8%;
        }

        .rightHeaderData {
            width: 42%;
            padding-left: 10px;
        }

        .row {
            width: 100%;
        }

        .signature-title {
            height: 3em;
            font-weight: bold;
            font-size: 12px;
        }

        .signature-underline {
            height: 2em;
            font-weight: bold;
            font-size: 11px;
            border-bottom: black 2px solid;
        }

        .termsConditions {
            font-size: 10px;
            float: left;
            width: 100%;
        }

        .termsConditions-grandTitle {
            font-size: 9px;
            font-weight: bold;
            margin-top: 3px;
        }

        .termsConditions-subTitle {
            font-weight: bold;
            margin-top: 3px;
            font-size: 8px;
        }

        .termsConditions>h3 {
            font-size: 9px;
            margin: 0;
        }

        .termsConditions>ul>li,
        .termsConditions>div>ul>li,
        .indentConditions-nestedConditions>ul>li,
        .smallFont {
            font-size: 7pt;
        }

        .textRightAlign {
            text-align: right;
        }

        .pageBreakFooter {
            page-break-after: always;
        }

        .itembalance {
            font-size: 8px;
        }

        .itembalance-title {
            font-weight: bold;
        }

        .customHeight {
            height: 5px;
        }

        hr {
            border: black thin solid;
        }

        .taxInvoiceTextHeader {
            font-size: 16pt;
            font-weight: bold;
        }

        .taxInvoiceNumber {
            font-size: 12pt;
        }

        #termsId {
            width: 95%;
            position: absolute;
            bottom: 10;
        }

        thead {
            display: table-header-group;
        }

        tfoot {
            display: table-footer-group;
        }
    </style>
</head>

<body>
    <table id="mainTbl" style="width: 700px;" border="0" align="center">
        <thead>
            <tr>
                <td>
                    <table width="100%">
                        <tbody>
                            <tr>
                                <td valign="top" width="33%">
                                    {{ $balance->outlet->address }}<br /><br />
                                    <strong>T.</strong> {{ $balance->outlet->outlet_phone }}<br />
                                    <strong>F.</strong> {{ $balance->outlet->outlet_fax }}<br />
                                    <strong>GST Reg. No.</strong> <br />
                                    <strong>ACRA No.</strong>
                                </td>
                                <td valign="top" width="33%">
                                    <span class="block_center" style="text-align:center;">
                                        <span class="taxInvoiceTextHeader">BALANCE PAYMENT</span><br />
                                        <span class="taxInvoiceNumber">{{ $balance->balance_code }}</span>
                                    </span>
                                </td>
                                <td valign="top" width="33%">
                                    <img src="{{ asset('storage/'.$balance->outlet->logo) }}"
                                        style="float: right; width: 150px;">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="0">
                        <tbody>
                            <tr>
                                <td class="leftHeader"><strong>Outlet Code No.:</strong></td>
                                <td class="leftHeaderData">{{ @$balance->outlet->outlet_code }}</td>
                                <td class="rightHeader"><strong>Date :</strong></td>
                                <td class="rightHeaderData">{{ @$balance->balance_date }}</td>
                            </tr>
                            <tr>
                                <td class="leftHeader"><strong>Customer Name :</strong></td>
                                <td class="leftHeaderData">{{ $balance->customer->full_name }}
                                    ({{ $balance->customer->induk_customer }})</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <table class="invoiceDetail-tbl" border="0" cellspacing="0" cellpadding="3px">
                        <thead>
                            <tr>
                                <td class="normal-header-td" width="5%">No.</td>
                                <td class="normal-header-td">Invoice Num</td>
                                <td class="normal-header-td">Outstanding Amount</td>
                            </tr>
                        </thead>
                        <tbody>

                            <tr style="height:8px;">
                                <td>1</td>
                                <td>{{ $balance->balance_code }}</td>
                                <td>{{ $balance->outstanding }}</td>
                            </tr>

                            <tr class="">
                            </tr>
                        </tbody>
                    </table>
            <tr style="height:30px;">
                <td valign="top">Remarks: {{ $balance->remarks }} </td>
            </tr>
            <tr>
                <td>
                    <div class="col-6">
                        <div class="row">
                            <table class="invoicePayment-tbl" style="border:1px solid black; margin-top: 10px;"
                                border="0" cellspacing="0">
                                <tr>
                                    <td class="normal-header-td">Pay Mode</td>
                                    <td class="normal-header-td"></td>
                                    <td class="normal-header-td">Amount</td>
                                </tr>
                                @php
                                $total_amount = 0;
                                @endphp
                                @foreach ($payment as $pay)
                                @if ($pay->balance_id)
                                <tr>
                                    <td>{{ $pay->bank->bank_code }}</td>
                                    <td></td>
                                    <td>{{ $pay->amount }}</td>
                                </tr>
                                @php
                                $total_amount += str_replace(",", "", $pay->amount);
                                @endphp
                                @endif
                                @endforeach
                            </table>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="row">
                            <table cellspacing="0">
                                <tr>
                                    <td class="rightHeader">Total Paid : </td>
                                    <td class="leftHeaderData">{{ format_money($total_amount) }}</td>
                                </tr>
                                <tr>
                                    <td class="rightHeader">Balance : </td>
                                    <td class="leftHeaderData">{{ $invoice->remaining }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="row">
                        <div class="col-2">
                            <div class="row">
                                <div class="col-10 floatRight signature-title">Sign by staff :</div>
                                <div class="col-10 floatRight signature-underline"></div>
                                <div class="col-10 floatRight signature-title">{{ $invoice->user->username }}</div>
                            </div>
                        </div>
                        <div class="col-6"></div>
                        <div class="col-2">
                            <div class="row">
                                <div class="col-10 floatRight signature-title">Sign by customer :</div>
                                <div class="col-10 floatRight signature-underline"></div>
                                <div class="col-10 floatRight signature-title">{{ $balance->user->username }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-10 floatRight signature-underline"></div>
                </td>
            </tr>
            </td>
            </tr>
        </tbody>
        <tfoot id="termsId"></tfoot>
    </table>
</body>

</html>

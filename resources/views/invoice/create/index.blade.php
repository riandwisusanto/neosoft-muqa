@extends('base')

@section('title')
Create Invoice
@endsection

@section('breadcrumb')
@parent
<li>Create Invoice</li>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <span style="font-size:20px; font-weight: bold;">CREATE INVOICE</span>
                <p style="font-size:15px; font-weight: bold;"><i>Create invoice in full payment or down payment.</i></p>
            </div>
            <form class="form form-invoice" data-toggle="validator" method="post">
                <input type="hidden" name="anamnesaIDs" value="" id="anamnesaIDs">
                <div class="box-body">
                    <div class="form-horizontal">
                        {{ csrf_field() }} {{ method_field('POST') }}
                        <div class="form-group">
                            <label class="col-md-2 control-label">Outlet</label>
                            <div class="col-md-5">
                                @if (count($outlet) <= 1)
                                @foreach ($outlet as $list)
                                    <input type="hidden" id="outlet" name="outlet" value="{{ $list->id_outlet }}">
                                @endforeach
                                @else
                                    <select id="outlet" onchange="chooseOutlet(this)" name="outlet" class="form-control select2 select2-hidden-accessible"
                                        style="width: 100%;" tabindex="-1" aria-hidden="true">
                                        @if (count($outlet) >= 1)
                                        <option selected="selected" value="">-- Select Outlet --</option>
                                        @endif
                                        @foreach ($outlet as $list)
                                        <option data-id="{{ $list->id_outlet }}" value="{{ $list->id_outlet }}">{{ $list->outlet_name }}</option>
                                        @endforeach
                                    </select>
                            @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Input NO Antrian</label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="queue" name="queue" autofocus="on"
                                    autocomplete="off" >
                                <div id="suggesstion-box2"></div>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Customer *</label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="customer" name="customer" autofocus="on"
                                    autocomplete="off" required>
                                <input type="hidden" name="customer_id" id="customer_id">
                                <div id="suggesstion-box"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Full Name</label>
                            <div class="col-md-10">
                                <p class="form-control-static" id="full"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Nickname</label>
                            <div class="col-md-10">
                                <p class="form-control-static" id="nick"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Contact No</label>
                            <div class="col-md-10">
                                <p class="form-control-static" id="contact"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Age</label>
                            <div class="col-md-10">
                                <p class="form-control-static" id="age"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Date of Birth</label>
                            <div class="col-md-10">
                                <p class="form-control-static" id="birthDate"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Address</label>
                            <div class="col-md-10">
                                <p class="form-control-static" id="address"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Privilege Member</label>
                            <div class="col-md-10">
                                <p class="form-control-static" id="Summary"></p>
                                <p class="form-control-static" id="Priviege"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="fullname" class="col-md-2 control-label"></label>
                            <div class="col-md-10">
                                <p id="bonus" class="text-muted text-red well well-md no-shadow"
                                    style="margin-top: 0px;">

                                </p>
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="col-md-2 control-label">Marketing Source</label>
                            <div class="col-md-5">
                                <select id="marketing_source" name="marketing_source"
                                    class="form-control select2 select2-hidden-accessible" style="width: 100%;"
                                    tabindex="-1" aria-hidden="true">
                                    @if (count($marketing_source) >= 1)
                                    <option selected="selected" value="">-- Select Source --</option>
                                    @endif
                                    <option selected="selected" value="16">NONE</option>
                                    @foreach ($marketing_source as $list)
                                    <option value="{{ $list->id_marketing_source }}">{{ $list->marketing_source_name }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Invoice Date</label>
                            <div class="col-md-5">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    @if (array_intersect(['ADMINISTRATOR', 'SUPER_USER', 'OUTLET_SUPERVISOR'], json_decode(Auth::user()->level)))
                                    <input type="text" class="form-control pull-right" id="date_invoice"
                                        name="date_invoice">
                                    @else
                                    <input type="text" class="form-control pull-right" id="date_invoice"
                                        name="date_invoice" readonly>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Expired Date</label>
                            <div class="col-md-5">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="date_expired"
                                        name="date_expired" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Served 1 By</label>
                            <div class="col-md-5">
                                <select id="consultant" name="consultant"
                                    class="form-control select2 select2-hidden-accessible" style="width: 100%;"
                                    tabindex="-1" aria-hidden="true">
                                    <option selected="selected" disabled>-- Select Consultant --</option>
                                    @foreach ($consultant as $list)
                                    <option value="{{ $list->id_consultant }}">{{ $list->consultant_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Served 2 By</label>
                            <div class="col-md-5">
                                <select id="therapist" name="therapist"
                                    class="form-control select2 select2-hidden-accessible" style="width: 100%;"
                                    tabindex="-1" aria-hidden="true">
                                    <option selected="selected" disabled>-- Select Therapist --</option>
                                    @foreach ($therapist as $list)
                                    <option value="{{ $list->id_therapist }}">{{ $list->therapist_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Agent</label>
                            <div class="col-md-5">
                                <select id="agent" name="agent" class="form-control select2 select2-hidden-accessible"
                                    style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    <option selected="selected" value="0">-- Without Agent --</option>
                                    @foreach ($agent as $list)
                                    <option value="{{ $list->id_agent }}">{{ $list->agent_name }}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="col-md-5">
                                <p id="medical-alert" style="margin-top: 0px;">
                                </p>
                            </div>
                        </div>

                        <div class="form-group warehouse_option">
                            <label class="col-md-2 control-label">Warehouse</label>
                            <div class="col-md-5">
                                <select id="warehouse_id" name="warehouse_id" class="form-control select2 select2-hidden-accessible"
                                    style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                </select>
                            </div>
                        </div>

                        <div class="form-group" style="display: none">
                            <label class="col-md-2 control-label">List Anamnesa</label>
                            <div class="col-md-5">
                                <select id="listAnamnesa" name="listAnamnesa" class="form-control select2 select2-hidden-accessible"
                                    style="width: 100%;" tabindex="-1" aria-hidden="true">

                                </select>

                            </div>
                        </div>

                        <div class="panel panel-primary">
                            <div class="panel-heading text-center"><strong>Item(s)</strong> </div>
                            <div class="panel-body">
                                <div class="row hidden" id="package_section">
                                    <div id="render"></div>
                                    @include('invoice.package')
                                    <input type="hidden" name="save_json" id="save_json" value="[]">
                                </div>
                                @include('sweet::alert')
                            </div>
                        </div>
                        <div class="form-group" style="display: none">
                           <label class="col-md-2 control-label">Discount Global</label>
                           <div class="col-md-3">
                               <input type="number" min="0" class="form-control" min="0" value="0" name="discount" id="discount"  placeholder="Nominal/Percent" onchange="discountGlobal()">
                           </div>
                           <div class="col-md-2">
                               <select name="type" id="type" style="width:100%;" onchange="discountGlobal()">
                                   <option value="0">Rp</option>
                                   <option value="1">%</option>
                               </select>
                           </div>
                       </div>
                        <input type="hidden" class="form-control" id="totalPrice" name="totalPrice" placeholder="Total Price" readonly>
                        <div class="form-group">
                            <label class="col-md-2 control-label">DP Appointment Balance</label>
                            <div class="col-md-5">
                                <input type="hidden" id="dp_app_balance">
                                <label for="" class="control-label" id="dp_appointment_balance">0</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Total Potongan</label>
                            <div class="col-md-5">
                                <input class="form-control" type="text" name="totalPotongan" id="totalPotongan" placeholder="Total Potongan" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Ongkir</label>
                            <div class="col-md-5">
                                <input class="form-control" type="text" name="ongkir" id="ongkir" onchange="ongkirHandler()">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Total</label>
                            <div class="col-md-5">
                                <input class="form-control" type="text" name="total" id="total" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Payment(s)</label>
                            <div class="col-md-10">
                                <div class="form-horizontal">
                                    <div class="col-md-3">
                                        <label>Payment Method</label>
                                        <input type='hidden' class="bank_type" name='bank_type[]' value='1'>
                                        <select class="form-control payment_type" name="payment_type[]" required="on">
                                            @foreach ($bank as $item)
                                            <option value="{{ $item->id_bank }}">{{ $item->bank_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Amount</label>
                                        <input type="text" class="form-control" id="amount" name="amount[]" rel="amount"
                                            placeholder="Amount(s)" required>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Info</label>
                                        <input type="text" class="form-control" id="payment_info" name="payment_info[]"
                                            placeholder="Info">
                                    </div>
                                    <div class="col-md-3 text-center">
                                        <label style="font-size:12px">Add Other Payment</label>
                                        <p>
                                            <input id="pay" value="1" type="hidden">
                                            <button type="button" id="add" class="btn btn-info btn-xs"
                                                onclick="addPaymant()"><i class="fa fa-plus"></i></button>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div id="DivPayment"></div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Paid Balance </label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="paid" name="paid" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Unpaid Balance </label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="unpaid" name="unpaid" value="0" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Change</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="kembali" name="kembali" value="0" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Old Invoice </label>
                            <div class="col-md-5">
                                <input type="text" class="form-control input-md" id="old_invoice" name="old_invoice"
                                    placeholder="Migration Invoice">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Remarks </label>
                            <div class="col-md-5">
                                <textarea class="form-control" name="remarks"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer text-right">
                    <button type="submit"  id="save" class="btn btn-primary btn-save simpan"><i class="fa fa-floppy-o"></i> Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-anamnesa" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Item from Anamnesa</h4>
            </div>
            <div class="modal-body">

                <h4>Product</h4>
                <div class="table-responsive" id="anamnesa-product">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" id="anamnesa-checkall-product">
                                </th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Subtotal</th>
                                <th>Doctor</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>

                <h4>Treatment</h4>
                <div class="table-responsive" id="anamnesa-treatment">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" id="anamnesa-checkall-treatment">
                                </th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Subtotal</th>
                                <th>Doctor</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="addFromAnamnesa">Add</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('style')
<style>
    .form-horizontal .checkbox, .form-horizontal .radio {
        height: 46px;
    }
</style>
@endsection

@section('script')

<script>
    var table, table_package, outlet, save_json = [], stockInformation = {}, select_outlet = 0, anamnesaIDs = [], anamnesaData = [];
    const firstBankOptionValue = $('.payment_type option').first().val();

    var allItem = JSON.parse('{!! json_encode($package) !!}')

    $('#date_expired').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });

    $('#date_invoice').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    let date = new Date();
    $('#date_invoice').datepicker('setDate', 'today')
    // $('#date_invoice').val(date.getDate().toString().padStart(2, "0")+'/'+(date.getMonth() + 1).toString().padStart(2, "0")+'/'+date.getFullYear());
    $('#date_expired').datepicker('setDate', '+1y');

    $(document).on('change','.payment_type', function () {
        $(this).prev().val($(this).val());
    })

    $(function(){
        $('#total').on('change', function () {$('input[rel="amount"]').keyup()})
        $('.bank_type').val($('.payment_type option').first().val());
        $('#outlet').val('{{ $outlet[0]->id_outlet }}').trigger('change')
        // chooseOutlet();
        loadAnamnesa();
        findCustomer();
        findQueue();

        hitung();
        $(".warehouse_option").hide();
        $('.form-invoice').validator().on('submit', function(e){
            var id_customer = $('#customer_id').val();
            var cek_json = JSON.parse($('#save_json').val());
            $('#anamnesaIDs').val(JSON.stringify(anamnesaIDs))

            let purchaseTotal = remove_format($('#total').val())
            let paidTotal = remove_format($('#paid').val())
            let changeTotal = remove_format($('#kembali').val())

            // unpaid balance is untrusted, just compare total vs paid.
            if (purchaseTotal != (paidTotal - changeTotal)) {
                swal("Peringatan", "sistem mendeteksi kekurangan / kelebihan bayar, mohon cek ulang", "warning")
                return false
            }

            if (cek_json.length > 0) {
                if(!e.isDefaultPrevented()){
                    $('#save').attr('disabled', 'disabled');
                    $('#save').html('<i class="fa fa-refresh fa-spin"></i> Memproses...');
                    $.ajax({
                        url : "{{ route('invoice-create.store') }}",
                        type : "POST",
                        data : $('.form-invoice').serialize(),
                        success : function(data){
                            $('#save').attr('disabled', 'disabled');
                            $('#save').html('<i class="fa fa-refresh fa-spin"></i> Memproses...');
                            if (data === "error") {
                                swal("in the package there are product or treatment restrictions", {
                                    button: false,
                                }).then(() => {
                                    $('#save').removeAttr('disabled');
                                    $('#save').html('<i class="fa fa-save"></i> Save');
                                });
                            }else if (data === "error") {
                                swal("Stock Reached Minimum Quantity", {
                                    button: false,
                                }).then(() => {
                                    $('#save').removeAttr('disabled');
                                    $('#save').html('<i class="fa fa-save"></i> Save');
                                });
                            }else{
                                swal({
                                    title: "Successfully Create Invoice",
                                    icon: "success",
                                    buttons: {
                                        canceled:{
                                            text:'Cancel',
                                            value: 'cancel',
                                            className: 'swal-button btn-default'
                                        },
                                        print:{
                                            text:'Print',
                                            value: 'print',
                                            className: 'swal-button btn-danger'
                                        },
                                        sendwa:{
                                            text:'Send WhatsApp',
                                            value: 'sendwa',
                                            className: 'swal-button btn-success'
                                        }
                                    },
                                    dangerMode: true,
                                })
                                .then((option) => {
                                    switch (option) {
                                        default:
                                            window.location.href = "/customers/"+data.id_invoice+"/detail";
                                            break;
                                        case 'print':
                                            window.open("invoice-create/"+data.id_invoice+"/print-thermal")
                                            window.location.href = "/customers/"+data.id_invoice+"/detail";
                                        break;
                                        case 'sendwa':
                                            $.ajax({
                                                url : "invoice-create/"+data.id_invoice+"/wa",
                                                type : "GET",
                                                success : function(x){
                                                    if (data) {
                                                        swal({
                                                            text: 'Send WA Success',
                                                            icon: 'success'
                                                        })
                                                        window.location.href = "/customers/"+data.id_invoice+"/detail";
                                                    }
                                                    return false;
                                                },
                                                error : function(){
                                                    swal({
                                                        text: 'Send WA Error',
                                                        icon: 'error'
                                                    })
                                                    window.location.href = "/customers/"+data.id_invoice+"/detail";
                                                    return false;
                                                }
                                            });
                                        break;
                                    }
                                });
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown){
                            if (errorThrown == 'Unprocessable Entity') {
                                swal("Peringatan", jqXHR.responseJSON.message, "warning").then(() => {
                                    $('#save').removeAttr('disabled');
                                    $('#save').html('<i class="fa fa-save"></i> Save');
                                })
                                return
                            }

                            console.log({ textStatus, jqXHR, errorThrown })
                            console.log(jqXHR.responseText);
                            if (jqXHR.responseJSON.status == "error") {
                                    swal(jqXHR.responseJSON.message, {
                                    button: false,
                                }).then(() => {
                                        $('#save').removeAttr('disabled');
                                        $('#save').html('<i class="fa fa-save"></i> Save');
                                });
                            }else{
                                    swal("Can not Save the Data ", {
                                    button: false,
                                }).then(() => {
                                        $('#save').removeAttr('disabled');
                                        $('#save').html('<i class="fa fa-save"></i> Save');
                                });
                            }
                        }
                    });
                    return false;
                }
            }else{
                swal("The Package is Still Empty! Please Add Item", {
                    icon: "error",
                });
                return false;
            }
        });
        let outletId = $("#outlet").val();
        if(outletId)
        {
            select_outlet = 1;
            $.ajax({
                    url : `/invoice-create/${outletId}/chooseWarehouse`,
                    type: 'GET',
                    dataType: "JSON",
                    success: function(data) {
                        console.log(data)
                        if(data.length <= 1)
                        {
                            var warehouse = '';
                            $.each(data, function (i, v) {
                                warehouse += `<option selected value="${v.id_warehouse}">${v.name}</option>`;
                            });
                            $('#warehouse_id').html(warehouse);
                            $('.warehouse_option').hide();
                        }else{
                            var warehouse = '<option selected disabled>Select Warehouse</option>';
                            $.each(data, function (i, v) {
                                warehouse += `<option  value="${v.id_warehouse}">${v.name}</option>`;
                            });
                            $('#warehouse_id').html(warehouse);
                            $('.warehouse_option').show();
                        }

                    },
                    error: function () {
                        alert("Can not Show the Data!");
                    }
            });
        }
    });


    $('#addFromAnamnesa').click(function () {
        let checks = document.getElementsByClassName('anamnesa-check')

        save_json = JSON.parse($('#save_json').val());
        for (let key in checks) {
            if (checks[key].checked) {
                let val = checks[key].value

                let row = anamnesaData.find(i => {
                    if ( ! i.product_treatment) {
                        return false
                    }

                    return i.product_treatment.product_treatment_code == val && i.is_paid == 0
                })

                if ( ! row) {
                    continue
                }

                let computedPrice = (row.total / row.qty);
                let choosePackage = allItem.findIndex(itm => {
                    if (row.kind == "package") {
                        return itm.package_code == row.package.package_code
                    }

                    return itm.product_treatment_code == row.product_treatment.product_treatment_code
                });

                var immediate = false;
                if (row.product_treatment && row.product_treatment.product_treatment_code.startsWith("P-")) {
                    immediate = true
                }

                var input = {
                    _id: key,
                    activity: "No Activity",
                    activity_id: "1",
                    current_price: format_money(row.computedPrice),
                    discount_1: 0,
                    discount_2: 0,
                    no: (key+1),
                    code: row.kind == "package" ? row.package.package_code : row.product_treatment.product_treatment_code,
                    package: row.kind == "package" ? row.package.package_name : row.product_treatment.product_treatment_name,
                    package_id: row.kind == "package" ? row.package.id_package : '',
                    id_package: row.kind == "package" ? row.package.id_package : '',

                    price: computedPrice,
                    qty: row.qty,
                    subtotal: format_money(row.total),
                    productTreatment_id: row.product_id,
                    anamnesa_detail_id: row.id_anamnesa_detail,
                    choose_package : choosePackage,
                    type_line: '%',
                    notes: row.notes,
                    editable: false,
                    type_line_id: '0',
                    isAnamnesa : "disabled",
                    immediate_usage: immediate
                };

                save_json.push(input);
            }
        }

        render();
        $('#modal-anamnesa').modal('hide')
    })

    $('#anamnesa-checkall-treatment').change(function () {
        if ($(this).prop('checked')) {
            $('#anamnesa-treatment .anamnesa-check').prop('checked', true)
        } else {
            $('#anamnesa-treatment .anamnesa-check').prop('checked', false)
        }
    })

    $('#anamnesa-checkall-product').change(function () {
        if ($(this).prop('checked')) {
            $('#anamnesa-product .anamnesa-check').prop('checked', true)
        } else {
            $('#anamnesa-product .anamnesa-check').prop('checked', false)
        }
    })

    function preventEnter(e) {

    }

    // function chooseOutlet() {

        // $('#outlet').change(function () {
            // outlet = $('#outlet').val();
            // $('#package_section').removeClass('hidden');

            // $.ajax({
            //     url : "/invoice-create/"+outlet+"/data/package",
            //     type: 'GET',
            //     dataType: "JSON",
            //     success: function(data) {
            //         $('select[name="package"]').empty();
            //         for (let index in data) {
            //             $('select[name="package"]').append('<option value="'+index+'">'+data[index]+'</option>')
            //         }
            //     },
            //     error: function () {
            //         alert('Can not Save The Data // });
        // });
    // }
    function chooseOutlet(e) {
        select_outlet = 1;
        let elementId = $(e).attr('id');
        let $el = $(`#${elementId} option[value="${e.value}"]`);
        let id = $el.attr('data-id');
        $.ajax({
                url : `/invoice-create/${id}/chooseWarehouse`,
                type: 'GET',
                dataType: "JSON",
                success: function(data) {
                    console.log(data)
                    if(data.length <= 1)
                    {
                        var warehouse = '';
                        $.each(data, function (i, v) {
                            warehouse += `<option selected value="${v.id_warehouse}">${v.name}</option>`;
                        });
                        $('#warehouse_id').html(warehouse);
                        $('.warehouse_option').hide();
                    }else{
                        var warehouse = '<option selected disabled>Select Warehouse</option>';
                        $.each(data, function (i, v) {
                            warehouse += `<option  value="${v.id_warehouse}">${v.name}</option>`;
                        });
                        $('#warehouse_id').html(warehouse);
                        $('.warehouse_option').show();
                    }

                },
                error: function () {
                    alert("Can not Show the Data!");
                }
        });
    }
    function addPaymant() {
        hitung();
        var _l = $("input[rel=amount]").length+1;
        var inputbaru = "<div class='form-group' id='row" + _l + "'>"+
                "<label class='col-md-2 control-label'></label>"+
                    "<div class='col-md-10'>"+
                        "<div class='form-horizontal'>"+
                            "<div class='col-md-3'>"+
                                "<label>Payment Method ("+ _l +")</label>"+
                                "<input type='hidden' name='bank_type[]' value='"+firstBankOptionValue+"'>"+
                                "<select class='form-control payment_type' name='payment_type[]' required='on'>"+
                                    "@foreach ($bank as $item)"+
                                        "<option value='{{ $item->id_bank }}'>{{ $item->bank_name }}</option>"+
                                    "@endforeach"+
                                "</select>"+
                            "</div>"+
                            "<div class='col-md-3'>"+
                                "<label>Amount ("+ _l +")</label>"+
                                "<input type='text' class='form-control' id='amount' name='amount[]' rel='amount' placeholder='Amount(s)' required>"+
                            "</div>"+
                            "<div class='col-md-3'>"+
                                "<label>Info ("+ _l +")</label>"+
                                "<input type='text' class='form-control' id='payment_info' name='payment_info[]' placeholder='Info'>"+
                            "</div>"+
                            "<div class='col-md-3 text-center'>"+
                                "<label style='font-size:12px'>Remove Payment</label>"+
                                "<p>"+
                                "<button class='  btn btn-info btn-xs' onclick='hapusPayment(\"#row" + _l + "\"); return false;'><i class='fa fa-minus'></i></button>"+
                                "</p>"+
                            "</div>"+
                        "</div>"+
                    "</div>";

        $("#DivPayment").append(inputbaru);
        hitung();
        return false;
    }

    function hitung() {
        $("input[rel=amount]").bind('keyup',function(){
            var total = $('#total').val();
            var awal = 0;
            $("input[rel=amount]").each(function(){
                this.value=this.value.replace(/[^0-9]/g,'');
                if(this.value !='') awal += parseInt(this.value,10);

            });

            let totalpaid = remove_format(total)-awal;
            let kembali = awal - remove_format(total);

            $('#paid').val(awal.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));

            if(totalpaid < 0)
            {
                $('#unpaid').val('0');
            }else{
                $('#unpaid').val(totalpaid.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
            }

            if(kembali < 0)
            {
                $('#kembali').val('0');
            }else{
                $('#kembali').val(kembali.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
            }


        });
    }

    function hapusPayment(pay) {
        $(pay).remove();
        hitung();
	}

    var typingTimer, waitTyping = 500;
    function findCustomer(){
        $('#customer').keyup(function(e){
            var code = e.key; // recommended to use e.key, it's normalized across devices and languages
            if(code==="Enter") e.preventDefault();

            var query = $(this).val();

            clearTimeout(typingTimer);
            typingTimer = setTimeout(function () {
                lookupCustomer(query)
            }, waitTyping)
        });

        $('#customer').keydown(function(e) {
            var code = e.key; // recommended to use e.key, it's normalized across devices and languages
            if(code==="Enter") e.preventDefault();
            clearTimeout(typingTimer)
        })
    }
    function findQueue(){
        $('#queue').keyup(function(){
            var query = $(this).val();

            clearTimeout(typingTimer);
            typingTimer = setTimeout(function () {
                lookupCustomerQueue(query)
            }, waitTyping)
        });

        $('#queue').keydown(function() {
            clearTimeout(typingTimer)
        })
    }

        /**
        * Lookup customer data after user finish typing.
        */
    function lookupCustomer(query) {
            if(query != ''){
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('customer.keyword') }}",
                    method:"POST",
                    data:{query:query, _token:_token},
                    dataType: "json",
                    success:function(data){
                        $('#suggesstion-box').fadeIn();
                        $('#suggesstion-box').html(data);
                    }
                });
            }else{
                var _token = $('input[name="_token"]').val();
                $.ajax({
                url:"{{ route('customer.all') }}",
                method:"POST",
                data:{query:query, _token:_token},
                dataType: "json",
                success:function(data){
                    $('#suggesstion-box').fadeIn();
                    $('#suggesstion-box').html(data);
                }
                });
            }
    }

    function lookupCustomerQueue(query) {
            if(query != ''){
                let outlet_id = $('#outlet').val()
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('customer.keywordQueue') }}",
                    method:"POST",
                    data:{query:query, _token:_token, outlet_id},
                    dataType: "json",
                    success:function(data){
                        $('#suggesstion-box2').fadeIn();
                        $('#suggesstion-box2').html(data);
                    }
                });
            }else{
                var _token = $('input[name="_token"]').val();
                $.ajax({
                url:"{{ route('customer.all') }}",
                method:"POST",
                data:{query:query, _token:_token},
                dataType: "json",
                success:function(data){
                    $('#suggesstion-box2').fadeIn();
                    $('#suggesstion-box2').html(data);
                }
                });
            }
    }
    function detailCustomer(id, induk, name,nickname,age,address, phone, birth) {
        anamnesaIDs = []
        $('#package_section').removeClass('hidden');
        $('#customer').val(induk);
        $('#suggesstion-box').fadeOut();
        $('#suggesstion-box2').fadeOut();
        $('#customer_id').val(id);
        $('#full').html(name);
        $('#nick').html(nickname);
        $('#age').html(age);
        $('#address').html(address);
        $('#contact').text(phone);
        $('#birthDate').text(birth);
        $('#anamnesa-product tbody').html('')
        $('#anamnesa-treatment tbody').html('')
        $.ajax({
        url : "/invoice-create/"+id+"/chooseAnamnesa",
        type: 'GET',
        dataType: "JSON",
        success: function(data) {
             $('#listAnamnesa').html(data);

             $('#listAnamnesa > option').each(function () {
                anamnesaIDs.push(this.value)
             })

             if (anamnesaIDs.length > 0) {
                 anamnesaIDs.forEach(anamnesaID => {
                    $('#listAnamnesa').val(anamnesaID).change()
                 })
                $('#modal-anamnesa').modal('show')
             }
        },
        error : function () {
            alert("Can not Show the Data!");
        }
        });

        rank(id);
        star(id);
        medical_alert(id);
        dp_balance(id);
        return false;
    }

    function rank(id){
        $.ajax({
            url: "ranks/customer/"+id,
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                let desc = '';
                if (data) {
                    desc = data.desc.replace(/\n/g, "<br />");
                    $("#bonus").html("<strong>"+ data.name + "</strong>" + "<br />" + desc);
                }
            },
            error: function () {
                alert("Can not Show the Data!");
            }
        });
    }

    function star(id) {
        $.ajax({
            url: "ranks/star/"+id,
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                let star = 0;
                if (data >= 1 && data < 5000000) {
                    star = 1;
                } else if (data >= 5000000 && data < 9100000) {
                    star = 2;
                } else if (data >= 9100000 && data < 15100000) {
                    star = 3;
                } else if (data > 15100000) {
                    star = 4;
                } else {
                    star = 0;
                }

                let starAfter = '';
                for($i=0; $i < star ;$i++){
                    starAfter += "<i class='fa fa-star text-yellow'></i>";
                }
                $("#Priviege").html(starAfter);
                $("#Summary").html(format_money(data));
            },
            error: function () {
                alert("Can not Show the Data!");
            }
        });
    }

    function ucwords (str) {
        return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
            return $1.toUpperCase();
        });
    }

    function medical_alert(id) {
        $.ajax({
            url: "medical-alert/"+id,
            type: "GET",
            dataType: "JSON",
            success: function (res) {
                $("#medical").remove();
                let data = res.medical

                let loop = '';
                for (let i = 0; i < data.length; i++) {
                    loop += '<i class="fa fa-exclamation-circle text-yellow" aria-hidden="true"></i> '+
                    '<strong>' +
                        ucwords(data[i].product_treatment_name)+
                        '<input type="hidden" name="restraction" id="restraction" value="'+data[i].id_product_treatment+'">'+
                    '</strong><br>'
                }

                let medical_alert = '<div class="col-md-12 col-xs-12" id="medical">'+
                    '<div class="user-block">'+
                        '<img class="img-circle img-bordered-sm" src="{{ asset("/img/medical.ico") }}" alt="user image">'+
                        '<span class="username"> Medical Alert </span>'+
                        '<span class="description">'+
                            loop+
                        '</span>'+
                    '</div>'+
                '</div>';

                let x = '<div class="col-md-12 col-xs-12">'+
                    '<div class="user-block">'+
                        '<span class="description">'+
                        '<i class="fa fa-exclamation-circle text-yellow" aria-hidden="true"></i> '+
                        '<strong>' +
                            res.customer.restriction
                        '</strong>'+
                        '</span>'+
                    '</div>'+
                '</div>';

                $("#medical-alert").append(medical_alert);
                $("#medical-alert").append(x);
            },
            error: function () {
                alert("Can not Show the Data!");
            }
        });
    }

    function dp_balance(id){
        $.ajax({
            url: "customers/"+id+"/dpAppBalance",
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                $("#dp_appointment_balance").html(data);
                $("#dp_app_balance").val(data);
                console.log(data);
            },
            error: function () {
            }
        });
    }

    function remove_format(money) {
        return money.toString().replace(/[^0-9]/g,'');
    }

    function format_money(money){
        if ( ! money) {
            return 0
        }
        return money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }

    function formatRupiah(angka){
        var number_string = angka.replace(/[^.\d]/g, '').toString(),
            split   		= number_string.split('.'),
            sisa     		= split[0].length % 3,
            rupiah     		= split[0].substr(0, sisa),
            ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);


        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? ',' : '';
            rupiah += separator + ribuan.join(',');
        }

        return rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        // return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    function formatAngka(angka){
            var number_string = angka.replace(/[^.\d]/g, '').toString();

            return rupiah = number_string;
            // return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    function changeType() {
        let type = $('#type_line').val();
        let disc1 = $('#discount_1').val();
        let disc2 = $('#discount_2').val();
        let price = $('#current_price').val();
        let qty = $('#qty').val();
        var keyupDisc1 = document.getElementById('discount_1');
        var keyupDisc2 = document.getElementById('discount_2');

        let stockError, stockValid = true,stockError2,stockValid2 = true;
        if(type == 1)
        {
            keyupDisc1.addEventListener('keyup', function(e){
                keyupDisc1.value = formatRupiah(this.value);
            });
            keyupDisc2.addEventListener('keyup', function(e){
                keyupDisc2.value = formatRupiah(this.value);
            });
        }else{
            keyupDisc1.addEventListener('keyup', function(e){
                keyupDisc1.value = formatAngka(this.value);
            });
            keyupDisc2.addEventListener('keyup', function(e){
                keyupDisc2.value = formatAngka(this.value);
            });
        }

        if (stockInformation.type == 'service') {
            let err = [];

            stockInformation.sellable.forEach((product, index) => {
                let min = product.qty * qty;
                if (min > parseInt(product.mutations_count,10)) {
                    let need = min - product.mutations_count;
                    err.push(`&#x25B6; ${product.name} diperlukan sebanyak ${need} unit lagi.`);
                    stockValid2 = false;
                }

                if (product.mutations_count <= product.buffer_stock ) {
                    swal(product.name + ' kurang dari sama dengan ' + product.buffer_stock + ' unit.', {
                        icon: "info",
                        button: true,
                    });
                }
            });

            // err.push('<br>Silahkan isi kekurangan stok terlebih dahulu untuk melanjutkan.');
            stockError2 = err.join('<br>');
        }

        if (stockInformation.type == 'product') {
            if ( stockInformation.sellable <= stockInformation.buffer_stock ) {
                swal(stockInformation.name + ' kurang dari sama dengan ' + stockInformation.buffer_stock + ' unit.', {
                    dangerMode: true,
                    button: true,
                });
                $('#qty').val(0)
            }
            if (qty > parseInt(stockInformation.sellable,10)) {
                let need = qty - stockInformation.sellable;
                stockError = `${stockInformation.name} hanya tersedia ${stockInformation.sellable} unit.`
                stockValid = false;
            }
        }

        if (stockInformation.type == 'package') {
            let grandErr = [];
            stockInformation.sellable.forEach(row => {
                if (row.type == 'service') {
                    let err = [];
                    row.sellable.forEach((product, index) => {
                        let min = product.qty * qty;
                        if (min > parseInt(product.mutations_count,10)) {
                            let need = min - product.mutations_count;
                            err.push(`&#x25B6; ${product.name} diperlukan sebanyak ${need} unit lagi.`);
                            stockValid2 = false;
                        }

                        if ( product.mutations_count <= product.buffer_stock ) {
                            swal(product.name + ' kurang dari sama dengan ' + product.buffer_stock + ' unit.', {
                                icon: "info",
                                button: true,
                            });
                        }
                    });
                    // err.push('<br>Silahkan isi kekurangan stok terlebih dahulu untuk melanjutkan.');
                    grandErr.push(err.join('<br>'));
                }

                if (row.type == 'product') {
                    if (qty > parseInt(row.sellable,10)) {
                        let need = qty - row.sellable;
                        let err = `${row.name} hanya tersedia ${row.sellable} unit.`
                        grandErr.push(err);
                        stockValid2 = false;
                    }

                    if (row.sellable <= row.buffer_stock ) {
                        swal(row.name + ' kurang dari sama dengan ' + row.buffer_stock + ' unit.', {
                            icon: "info",
                            button: true,
                        });
                    }
                }
            });

            stockError2 = grandErr.join('<br>');
        }

        if ( ! stockValid) {
            var content = document.createElement("div");
            content.innerHTML = stockError;

            swal({
                title: "Stok Tidak Mencukupi.",
                content,
                icon: "error",
                button: true,
            });

            $('#qty').val(0);
            return false;
        }
        if ( ! stockValid2) {
            var content = document.createElement("div");
            content.innerHTML = stockError2;

            swal({
                content,
                icon: "info",
                button: true,
            });
            return false;
        }

        let subtotal = qty * remove_format(price);

        let total1, grandTotal;
        if (disc2) {
            if (type == 0) {
                total1 = subtotal - (disc1 / 100) * subtotal;
                grandTotal = total1 - (disc2 / 100) * total1;
            } else {
                total1 = subtotal - remove_format(disc1);
                grandTotal = total1 - remove_format(disc2);
            }
        }else{
            if (type == 0) {
                grandTotal = subtotal - (disc1 / 100) * subtotal;
            } else {
                grandTotal = subtotal - remove_format(disc1);
            }
        }

        $('#sub_total').val(format_money(Math.round(grandTotal)));
    }

    function changeTypeWithId(id) {
        let type = $('#type_line_'+id).val();
        //let disc = $('#discount_'+id).val();
        let disc1 = $('#discount_1_'+id).val();
        let disc2 = $('#discount_2_'+id).val();
        let price = $('#current_price_'+id).val();
        let qty = $('#qty_'+id).val();
        $('#package').attr('disabled', 'disabled');
        $('#activity').attr('disabled', 'disabled');
        $('#qty').attr('disabled', 'disabled');
        $('#discount_1').attr('readonly', 'readonly');
        $('#discount_2').attr('readonly', 'readonly');
        var keyupDisc1 = document.getElementById('discount_1_' + id);
        var keyupDisc2 = document.getElementById('discount_2_' + id);

        if(type == 1)
        {
            keyupDisc1.addEventListener('keyup', function(e){
                keyupDisc1.value = formatRupiah(this.value);
            });
            keyupDisc2.addEventListener('keyup', function(e){
                keyupDisc2.value = formatRupiah(this.value);
            });
        }else{
            keyupDisc1.addEventListener('keyup', function(e){
                keyupDisc1.value = formatAngka(this.value);
            });
            keyupDisc2.addEventListener('keyup', function(e){
                keyupDisc2.value = formatAngka(this.value);
            });
        }

        let stockError, stockValid = true,stockError2,stockValid2 = true;

        if (stockInformation.type == 'service') {
            let err = [];

            stockInformation.sellable.forEach((product, index) => {
                let min = product.qty * qty;
                if (min > parseInt(product.mutations_count,10)) {
                    let need = min - product.mutations_count;
                    err.push(`&#x25B6; ${product.name} diperlukan sebanyak ${need} unit lagi.`);
                    stockValid2 = false;
                }

                if (product.mutations_count <= product.buffer_stock ) {
                    swal(product.name + ' kurang dari sama dengan ' + product.buffer_stock + ' unit.', {
                        button: true,
                        icon: "info",
                    });
                }
            });
            // err.push('<br>Silahkan isi kekurangan stok terlebih dahulu untuk melanjutkan.');
            stockError2 = err.join('<br>');
        }

        if (stockInformation.type == 'product') {
            if ( stockInformation.sellable <= stockInformation.buffer_stock ) {
                swal(stockInformation.name + ' kurang dari sama dengan ' + stockInformation.buffer_stock + ' unit.', {
                    dangerMode: true,
                    button: true,
                });
                $('#qty_'+id).val(0)
            }
            if (qty > parseInt(stockInformation.sellable,10)) {
                let need = qty - stockInformation.sellable;
                stockError = `${stockInformation.name} hanya tersedia ${stockInformation.sellable} unit.`
                stockValid = false;
            }
        }

        if (stockInformation.type == 'package') {
            let grandErr = [];
            stockInformation.sellable.forEach(row => {
                if (row.type == 'service') {
                    let err = [];
                    row.sellable.forEach((product, index) => {
                        let min = product.qty * qty;
                        if (min > parseInt(product.mutations_count,10)) {
                            let need = min - product.mutations_count;
                            err.push(`&#x25B6; ${product.name} diperlukan sebanyak ${need} unit lagi.`);
                            stockValid2 = false;
                        }

                        if ( product.mutations_count <= product.buffer_stock ) {
                            swal(product.name + ' kurang dari sama dengan ' + product.buffer_stock + ' unit.', {
                                button: true,
                                icon: "info",

                            });
                        }
                    });
                    // err.push('<br>Silahkan isi kekurangan stok terlebih dahulu untuk melanjutkan.');
                    grandErr.push(err.join('<br>'));
                }

                if (row.type == 'product') {
                    if (qty > parseInt(row.sellable,10)) {
                        let need = qty - row.sellable;
                        let err = `${row.name} hanya tersedia ${row.sellable} unit.`
                        grandErr.push(err);
                        stockValid2 = false;
                    }

                    if (row.sellable <= row.buffer_stock ) {
                        swal(row.name + ' kurang dari sama dengan ' + row.buffer_stock + ' unit.', {
                            button: true,
                            icon: "info",
                        });
                    }
                }
            });

            stockError2 = grandErr.join('<br>');
        }

        if ( ! stockValid) {
            var content = document.createElement("div");
            content.innerHTML = stockError;

            swal({
                title: "Stok Tidak Mencukupi.",
                content,
                icon: "error",
                button: true,
            });

            $('#qty_'+id).val(0);
            return false;
        }
        if ( ! stockValid2) {
            var content = document.createElement("div");
            content.innerHTML = stockError2;

            swal({
                content,
                icon: "info",
                button: true,
            });
            return false;
        }

        let subtotal = qty * remove_format(price);
        let total1, grandTotal;
        if (disc2) {
            if (type == 0) {
                total1 = subtotal - (disc1 / 100) * subtotal;
                grandTotal = total1 - (disc2 / 100) * total1;
            } else {
                total1 = subtotal - remove_format(disc1);
                grandTotal = total1 - remove_format(disc2);
            }
        }else{
            if (type == 0) {
                grandTotal = subtotal - (disc1 / 100) * subtotal;
            } else {
                grandTotal = subtotal - remove_format(disc1);
            }
        }

        $('#sub_total_x_'+id).val(format_money(Math.round(grandTotal)));
    }

    function getItemSellableStock(e) {
        let elementId = $(e).attr('id');
        let $el = $(`#${elementId} option[value="${e.value}"]`);
        let warehouse_id = 0;
        if (select_outlet == 1) {
            warehouse_id = $('select[name=warehouse_id] option').filter(':selected').val();
        }

        console.log(warehouse_id)

        let type = $el.attr('data-type').toLowerCase();

        let id = $el.attr('data-id');

        if (type == 'package') {
            id = $el.attr('data-package-id');
        }
        let endpoint = `/api/v1/logistics/items/sales_informations/${type}/${id}/${warehouse_id}/sellables`;
        let token = '{{ auth()->user()->api_token }}';

        $('input[id^="qty"]').attr('readonly', 'readonly');

        stockInformation ={};
        
        $.ajax({
            url: endpoint,
            type: 'GET',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${token}`);
                $("#AddItem").attr("disabled",true);
            },
            success: function (res) {
                // stockInformation = res.sellable;
                stockInformation = res.sellable

                $('input[id^="qty"').removeAttr('readonly');

                stockInformation.sellable = stockInformation.sellable || 0
                if (stockInformation.sellable > 0) {
                    if (elementId == 'package') {
                        $('#qty').val(1).change();
                    } else {
                        let parts = elementId.split('_');
                        $('#qty_'+parts[1]).val(1);
                    }
                } else {
                    if (elementId == 'package') {
                        $('#qty').val(1).change();
                    } else {
                        let parts = elementId.split('_');
                        $('#qty_'+parts[1]).val(0);
                    }
                }
                $("#AddItem").removeAttr("disabled", false);
            }
        });
    }

    function changeCountWithId(id) {

        changeTypeWithId(id);
    }

    function notZero(event) {
        const qty = $('input[id^="qty"]');
        if (event.code !== "Backspace") {
            const currVal = parseInt(qty.val());
            if (currVal < 1)
                qty.val(1);
        }
    }

    function changeCount(){

        changeType();
    }

    function loadAnamnesa() {
        $('#listAnamnesa').change(function () {
            $.ajax({
                url: "invoice-create/"+$(this).val()+"/loadAnamnesa",
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    anamnesaData.push(...data.load)
                    let products = data.load.filter(row => {
                        if ( ! row.product_treatment) return false
                        return row.product_treatment.product_treatment_code.startsWith('P') && ! row.is_paid
                    })

                    let treatments = data.load.filter(row => {
                        if ( ! row.product_treatment) return false
                        console.log({ code: row.product_treatment.product_treatment_code, is_paid: row.is_paid })
                        return row.product_treatment.product_treatment_code.startsWith('T') && ! row.is_paid
                    })

                    products.forEach((row, index) => {
                        let computedPrice = (row.total / row.qty);
                        let tableRow = `
                            <tr>
                                <td>
                                    <input type="checkbox" class="anamnesa-check" value="${row.product_treatment.product_treatment_code}">
                                </td>
                                <td>${row.product_treatment.product_treatment_code}</td>
                                <td>${row.product_treatment.product_treatment_name}</td>
                                <td>${row.qty}</td>
                                <td>${format_money(computedPrice)}</td>
                                <td>${format_money(computedPrice * row.qty)}</td>
                                <td>${data.creator_name}</td>
                            </tr>
                        `

                        $('#anamnesa-product tbody').append(tableRow)
                    })

                    treatments.forEach((row, index) => {
                        let computedPrice = (row.total / row.qty);
                        let tableRow = `
                            <tr>
                                <td>
                                    <input type="checkbox" class="anamnesa-check" value="${row.product_treatment.product_treatment_code}">
                                </td>
                                <td>${row.product_treatment.product_treatment_code}</td>
                                <td>${row.product_treatment.product_treatment_name}</td>
                                <td>${row.qty}</td>
                                <td>${format_money(computedPrice)}</td>
                                <td>${format_money(computedPrice * row.qty)}</td>
                                <td>${data.creator_name}</td>
                            </tr>
                        `

                        $('#anamnesa-treatment tbody').append(tableRow)
                    })

                    // save_json = JSON.parse($('#save_json').val());

                    // data.load.forEach((row, index) => {
                    //     let computedPrice = (row.total / row.qty);
                    //     let choosePackage = allItem.findIndex(itm => {
                    //         if (row.kind == "package") {
                    //             return itm.package_code == row.package.package_code
                    //         }

                    //         return itm.product_treatment_code == row.product_treatment.product_treatment_code
                    //     });

                    //     // console.log({ choosePackage })
                    //     var immediate = false;



                    //     var input = {
                    //         _id: index,
                    //         activity: "No Activity",
                    //         activity_id: "1",
                    //         current_price: format_money(row.computedPrice),
                    //         discount_1: 0,
                    //         discount_2: 0,
                    //         no: (index+1),
                    //         code: row.kind == "package" ? row.package.package_code : row.product_treatment.product_treatment_code,
                    //         package: row.kind == "package" ? row.package.package_name : row.product_treatment.product_treatment_name,
                    //         package_id: row.kind == "package" ? row.package.id_package : '',
                    //         id_package: row.kind == "package" ? row.package.id_package : '',

                    //         price: computedPrice,
                    //         qty: row.qty,
                    //         subtotal: format_money(row.total),
                    //         productTreatment_id: row.product_id,
                    //         anamnesa_detail_id: row.id_anamnesa_detail,
                    //         choose_package : choosePackage,
                    //         type_line: '%',
                    //         editable: false,
                    //         type_line_id: '0',
                    //         isAnamnesa : "disabled",
                    //         immediate_usage: immediate
                    //     };

                    //     save_json.push(input);
                    // });

                    // render();
                },
                error: function () {
                    alert("Can not Show the Data!");
                }
            });
        });
    }

    function ongkirHandler() {
        let absoluteTotal = 0;
        save_json.forEach(e => absoluteTotal += parseInt(e.subtotal.replace(/[^0-9]/g, '')))

        let ongkir = $("#ongkir").val()
        let total = $("#total").val()
        let sumOngkir = 0
        let sumTotal = 0
        if (ongkir !== '') {
            sumOngkir = parseInt(ongkir.replace(/[^0-9]/g, ''))
        }

        if (total !== '') {
            sumTotal = parseInt(total.replace(/[^0-9]/g, ''))
        }

        let sum =sumOngkir + absoluteTotal;
        $("#total").val(format_money(sum)).change()

        $('input[rel="amount"]').trigger('change')
    }

    // function loadAnamnesa() {
    //     $('#listAnamnesa').change(function () {
    //         $.ajax({
    //             url: "invoice-create/"+$(this).val()+"/loadAnamnesa",
    //             type: "GET",
    //             dataType: "JSON",
    //             success: function (data) {
    //                 save_json = JSON.parse($('#save_json').val());

    //                 data.load.forEach((row, index) => {
    //                     let computedPrice = (row.total / row.qty);
    //                     let choosePackage = allItem.findIndex(itm => {
    //                         if (row.kind == "package") {
    //                             return itm.package_code == row.package.package_code
    //                         }

    //                         return itm.product_treatment_code == row.product_treatment.product_treatment_code
    //                     });
    //                     var immediate = false;

    //                     var input = {
    //                         _id: index,
    //                         activity: "No Activity",
    //                         activity_id: "1",
    //                         current_price: format_money(row.computedPrice),
    //                         discount_1: 0,
    //                         discount_2: 0,
    //                         no: (index+1),
    //                         code: row.kind == "package" ? row.package.package_code : row.product_treatment.product_treatment_code,
    //                         package: row.kind == "package" ? row.package.package_name : row.product_treatment.product_treatment_name,
    //                         package_id: row.kind == "package" ? row.package.id_package : '',
    //                         id_package: row.kind == "package" ? row.package.id_package : '',

    //                         price: computedPrice,
    //                         qty: row.qty,
    //                         subtotal: format_money(row.total),
    //                         productTreatment_id: row.product_id,
    //                         anamnesa_detail_id: row.id_anamnesa_detail,
    //                         choose_package : choosePackage,
    //                         type_line: '%',
    //                         editable: false,
    //                         type_line_id: '0',
    //                         isAnamnesa : "disabled",
    //                         immediate_usage: immediate
    //                     };

    //                     save_json.push(input);
    //                 });

    //                 render();
    //             },
    //             error: function () {
    //                 alert("Can not Show the Data!");
    //             }
    //         });
    //     });
    // }

   function discountGlobal()
    {
        let type = $('#type').val();
        let discount = $('#discount').val();
        var save_json = JSON.parse($('#save_json').val());
        let grandtotal = [];
        let totalPrice = [];

        for (let i = 0; i < save_json.length; i++) {
            grandtotal.push(Number(remove_format(save_json[i].subtotal)));
            totalPrice.push(Number(remove_format(save_json[i].price) * save_json[i].qty));

        }

        let total = grandtotal.reduce((a, b) => a + b, 0)
        let price = totalPrice.reduce((a, b) => a + b, 0)

       let grandTotals = total;
       if(discount)
       {
           if (type == 0) {
                grandTotals= total - discount;
           } else {
               grandTotals = total - (discount / 100) * total;

           }
        }

       $('#total').val(format_money(Math.round(grandTotals))).change();
       $('#totalPrice').val(format_money(price));
       $('#totalPotongan').val(format_money(price - grandTotals));
    }


</script>
<script src="{{ asset('js/invoice.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />

@endsection


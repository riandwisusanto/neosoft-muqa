<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice Print</title>
    <style>
        * {
            font-size: 10pt;
            color: #000000;
            font-family: "Trebuchet MS", "Lucida Grande", "Lucida Sans Unicode", "Lucida Sans", Tahoma, sans-serif;
        }

        @page {
            margin: 0.1cm;
        }

        ul {
            margin: 0 -20;
            list-style: none;
        }

        ul li:before {
            content: "•";
            font-size: 1em;
            /* or whatever */
            padding-right: 5px;
        }


        .normal-header-td {
            border-bottom: 1px solid black;
        }

        .last-header-td {
            border-bottom: 1px solid black;
        }

        .block_center {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }

        .col-0_5,
        .col-1,
        .col-1_5,
        .col-2,
        .col-3,
        .col-4,
        .col-5,
        .col-6,
        .col-7,
        .col-8,
        .col-9,
        .col-10,
        .row {
            float: left;
            position: relative;
            min-height: 1px;
        }

        .col-0_5 {
            width: 5%;
        }

        .col-1 {
            width: 10%;
        }

        .col-1_5 {
            width: 15%;
        }

        .col-2 {
            width: 20%;
        }

        .col-3 {
            width: 30%;
        }

        .col-4 {
            width: 40%;
        }

        .col-5 {
            width: 50%;
        }

        .col-6 {
            width: 60%;
        }

        .col-7 {
            width: 70%;
        }

        .col-8 {
            width: 80%;
        }

        .col-9 {
            width: 90%;
        }

        .col-10 {
            width: 100%;
        }

        .floatRight {
            float: right;
        }

        .indentConditionParagraph {
            margin-left: 5px;
            width: 100%;
        }

        .indentConditions {
            float: left;
            width: 100%;
            margin-left: 15px;
        }

        .indentConditions-numbering {
            float: left;
            font-size: 9pt;
            vertical-align: top;
        }

        .indentConditions-bullets:before {
            content: "•";
            font-size: 1em;
            /* or whatever */
            padding-right: 5px;
        }

        .indentConditions-nestedConditions {
            float: left;
            width: 95%;
            margin-left: 5px;
        }

        .invoiceDetail-tbl {
            width: 100%;
            border: 1px solid black;
            min-height: 10em;
        }

        .invoiceDetail-tbl td {
            vertical-align: top;
        }

        .invoicePayment-tbl {
            width: 100%;
        }

        tr.lastRow {
            height: 30%;
        }

        .leftHeader {
            white-space: nowrap;
            vertical-align: top;
            width: 8%;
        }

        .leftHeaderData {
            width: 42%;
            vertical-align: top;
            padding-left: 10px;
        }

        .rightHeader {
            white-space: nowrap;
            vertical-align: top;
            width: 8%;
        }

        .mt-3 {
            margin-top: 1rem;
        }

        .mt-0 {
            margin-top: 0 !important;
            margin-bottom: 0;
        }

        .pl-2 {
            padding-left: 12px;
        }

        .rightHeaderData {
            width: 42%;
            vertical-align: top;
            padding-left: 10px;
        }

        .row {
            width: 100%;
        }

        .signature-title {
            height: 3em;
            font-weight: bold;
            font-size: 12px;
        }

        .signature-underline {
            height: 2em;
            font-weight: bold;
            font-size: 11px;
            border-bottom: black 2px solid;
        }

        .termsConditions {
            font-size: 10px;
            float: left;
            width: 100%;
        }

        .termsConditions-grandTitle {
            font-size: 9px;
            font-weight: bold;
            margin-top: 3px;
        }

        .termsConditions-subTitle {
            font-weight: bold;
            margin-top: 3px;
            font-size: 8px;
        }

        .termsConditions>h3 {
            font-size: 9px;
            margin: 0;
        }

        .termsConditions>ul>li,
        .termsConditions>div>ul>li,
        .indentConditions-nestedConditions>ul>li,
        .smallFont {
            font-size: 7pt;
        }

        .textRightAlign {
            text-align: right;
        }

        .pageBreakFooter {
            page-break-after: always;
        }

        .itembalance {
            font-size: 8px;
        }

        .itembalance-title {
            font-weight: bold;
        }

        .customHeight {
            height: 5px;
        }

        hr {
            border: black thin solid;
        }

        .taxInvoiceTextHeader {
            font-size: 16pt;
            font-weight: bold;
        }

        .taxInvoiceNumber {
            font-size: 12pt;
        }

        #termsId {
            width: 95%;
            position: absolute;
            bottom: 10;
        }

        thead {
            display: table-header-group;
        }

        tfoot {
            display: table-footer-group;
        }
    </style>
</head>

<body>
    <table id="mainTbl" style="width: 105mm;" border="0" align="center">
        <thead>
            <tr>
                <td>
                    <table width="100%">
                        <tbody>
                            <tr>
                                <td valign="top" width="100%">
                                    <span class="block_center" style="text-align:center;">
                                        <span class="taxInvoiceTextHeader">INVOICE</span><br />
                                        <span class="taxInvoiceNumber">{{ $invoice->inv_code }}</span>
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="row mt-3">
                        <div class="col-2">
                            <img src="/storage/{{ $invoice->outlet->logo }}" class="col-10">
                        </div>
                        <div class="col-1">&nbsp;</div>
                        <div class="col-6">
                            <p class="mt-0 pl-2"><b>{{ $invoice->outlet->outlet_name }}</b></p>
                            <p class="mt-0 pl-2">{{ $invoice->outlet->address }}</p>
                            <p class="mt-0 pl-2">{{ $invoice->outlet->outlet_phone }}</p>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="0">
                        <tbody>
                            <tr>
                                <td class="leftHeader"><strong>Outlet Code</strong></td>
                                <td class="leftHeaderData">{{ $invoice->outlet->outlet_code }}</td>
                                <td class="rightHeader"><strong>Queue No.</strong></td>
                                @php
                                    $dt = date('Y-m-d', strtotime($invoice->inv_date));
                                    $date = explode('/', $invoice->inv_date);
                                @endphp
                                <td class="rightHeaderData">{{ $queue ? $queue->queue_number : '-'}}</td>
                            </tr>
                            <tr>
                                <td class="leftHeader"><strong>Cashier</strong></td>
                                <td class="leftHeaderData">{{ $invoice->user->name }}</td>
                                <td class="rightHeader"><strong>Date</strong></td>
                                <td class="rightHeaderData">{{ $invoice->inv_date }}</td>
                            </tr>
                            <tr>
                                <td class="leftHeader"><strong>Cust No</strong></td>
                                <td class="leftHeaderData">{{ $invoice->customer->induk_customer }}</td>
                                <td class="rightHeader"><strong>Time</strong></td>
                                <td class="rightHeaderData">{{ $invoice->created_at->format('H:i') }}</td>
                            </tr>   
                            <tr>
                                <td class="leftHeader"><strong>Cust Name</strong></td>
                                <td class="leftHeaderData">{{ $invoice->customer->full_name }} <br>{{ $invoice->customer->nick_name }} / {{ $ageInYear }} th / {{ $invoice->customer->gender }}</td>
                                <td class="rightHeader"><strong>Served by</strong></td>
                                <td class="rightHeaderData">{{ isset($invoice->consultant) ? $invoice->consultant->consultant_name : '-' }}</td>
                            </tr>
                            <tr>
                                <td class="leftHeader"><strong>Address</strong></td>
                                <td class="leftHeaderData">{{ $invoice->customer->address }} {{ $invoice->customer->city->province }} </td>
                                <td class="rightHeader"><strong>Doctor</strong></td>
                                <td class="rightHeaderData">{{ isset($invoice->therapist) ? $invoice->therapist->therapist_name : '-' }}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <table class="invoiceDetail-tbl mt-3" border="0" cellspacing="0" cellpadding="3px">
                        <thead>
                            <tr>
                                
                                <td class="normal-header-td">Description</td>
                                <td class="normal-header-td">Price</td>
                                <td class="normal-header-td">Discount</td>
                                <td class="last-header-td">Sub Total</td>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $no=1;
                            @endphp
                            @foreach ($inv_detail as $list)
                            <tr style="height:8px;">
                                @if ($list->package)
                                <td>{{ $list->qty }}x {{ $list->package->package_name }}</td>
                                @else
                                <td>
                                    {{ $list->qty }}x {{ $list->product_treatment->product_treatment_name }}
                                    @if (isset($list->notes) && $list->notes != "")
                                        <br> <i class="small" style="font-size: 11px">Notes: {{ $list->notes }}</i>
                                    @endif

                                    @if ($list->immediate_usage && substr($list->package_code, 0, 1) == 'T')
                                        @php    
                                            $therapists = array_map(function ($row) {
                                                return $row['therapist']['therapist_name'];
                                            }, $list->invoicePackage->usageDetail->usage_therapist()->with('therapist')->get()->toArray());
                                        @endphp
                                        <br> <i class="small" style="font-size: 11px">Operator(s): {{ implode(', ', $therapists) }}</i>
                                    @endif
                                </td>
                                @endif
                                <td>{{ $list->current_price }}</td>
                                @php
                                if($list->type_line == 0){
                                    $discount1 = $list->discount_1; 
                                    $discount2 = $list->discount_2;
                                    $type = '%';
                                }else{
                                    $discount1 = number_format($list->discount_1);
                                    $discount2 = number_format($list->discount_2);
                                    $type = "IDR";
                                }
                                @endphp
                                @if ($list->discount_2)
                                    <td>{{ $discount1 }} {{ $type }} | {{ $discount2 }} {{ $type }}</td>
                                    @else
                                    <td>{{ $discount1 }} {{ $type }}</td>
                                @endif
                                <td>{{ $list->subtotal }}</td>
                            </tr>
                            @php
                            $no++;
                            @endphp
                            @endforeach
                            @php
                                $sumSubtotal = 0;
                                foreach($inv_detail as $inv_dtl) {
                                    $sumSubtotal += remove_format_money($inv_dtl->subtotal);
                                }
                            @endphp
                            <tr class="lastRow">
                                <td colspan="3" style="padding-top: 16px;"><strong>Total</strong></td>                                
                                <td style="padding-top: 16px;"><strong>{{ format_money($sumSubtotal) }}</strong></td>
                            </tr>
                            @if (isset($invoice->ongkir))
                            <tr class="lastRow">
                                <td colspan="3" ><strong>Ongkir</strong></td>                                
                                <td><strong>{{ format_money($invoice->ongkir) }}</strong></td>
                            </tr>
                            @endif

                            <tr class="lastRow">
                                <td colspan="3"><strong>Grand Total</strong></td>
                                <td><strong>{{ $invoice->total }}</strong></td>
                            </tr>
                            {{-- <tr class="lastRow">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td colspan="3"><strong>Bank Charge</strong></td>
                                <td><strong>{{ $invoice->total }}</strong></td>
                            </tr>

                            <tr class="lastRow">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td colspan="3"><strong>Grand Total</strong></td>
                                <td><strong>{{ $invoice->total }}</strong></td>
                            </tr> --}}
                        </tbody>
                    </table>
            <tr style="height:30px;">
                <td valign="top">Remarks: {{ $invoice->remarks }} </td>
            </tr>
            <tr>
                <td>
                    <div class="col-10">
                        <div class="row">
                            <table class="invoicePayment-tbl" style="border:1px solid black; margin-top: 10px;"
                                border="0" cellspacing="0">
                                <tr>
                                    <td class="normal-header-td">Pay Mode</td>
                                    <td class="normal-header-td">Amount</td>
                                </tr>
                                @php
                                $total_amount = 0;
                                @endphp
                                @foreach ($payment as $list)
                                @php
                                    $amount = str_replace(",", "", $list->amount);
                                    $valCharge  = $list->bank->bank_charge / 100;
                                    $nominalCharge = $valCharge * $amount;
                                    // $afterCharge = $nominalCharge + $amount;
                                    $afterCharge = $amount;
                                @endphp
                                <tr>
                                    <td>{{ $list->bank->bank_name }}</td>
                                    <td>{{ format_money($afterCharge) }}</td>
                                </tr>
                                @php
                                $total_amount += $afterCharge;
                                @endphp
                                @endforeach
                            </table>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-5">&nbsp;</div>
                        <div class="col-5">
                            <table cellspacing="0" class="row">
                                
                                <tr>
                                    <td>Outstanding :</td>
                                    <td class="textRightAlign">{{ $invoice->remaining }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="row mt-3">
                        <div class="col-4">
                            <div class="row">
                                <div class="col-10 floatRight signature-title">Sign by staff :</div>
                                <div class="col-10 floatRight signature-underline"></div>
                                <div class="col-10 floatRight signature-title">{{ $invoice->user->username }}</div>
                            </div>
                        </div>
                        <div class="col-2"></div>
                        <div class="col-4">
                            <div class="row">
                                <div class="col-10 floatRight signature-title">Sign by customer :</div>
                                <div class="col-10 floatRight signature-underline"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-10 floatRight signature-underline"></div>
                </td>
            </tr>
            </td>
            </tr>
        </tbody>
        <tfoot id="termsId"></tfoot>
    </table>
</body>

</html>

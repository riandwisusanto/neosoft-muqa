<script type="x-tmpl-mustache" id="add_item">
    <div class="container-fluid">
        <div class="form-group">
            <label class="col-md-2 control-label">Items Name</label>
            <div class="col-md-5">
                <input type="hidden" name="product_id" id="product_id" required>
                <select class="form-control input-sm" name="product" id="product" onChange="changeQty()" style="width: 100%" required>
                    <option></option>
                    @foreach($product as $item)
                    <option value="{{ $item->id_product_point }}">{{ $item->product_point_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 col-sm-12 col-xs-12 control-label"></label>
            <div class="col-md-1 col-sm-6 col-xs-12">
                <label>Point</label>
                <strong><p id="point">0</p></strong>
            </div>
            <div class="col-md-1 col-sm-4 col-xs-12">
                <label>Qty</label>
                <input type="hidden" id="current_point" name="current_point">
                <input type="number" min="0" class="form-control input-sm" id="qty" name="qty" onChange="changeCount()" value="0" required>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <label>Sub Total</label>
                <input type="text" id="sub_total" class="form-control input-sm" name="sub_total" readonly>
            </div>

            <div class="col-md-1 col-sm-12 col-xs-12 text-center" style="font-size:12px">
                <label></label>
                <p>
                <button type="button" class="btn btn-success btn-xs" id="AddItem"><i class="fa fa-check"></i> ADD ITEM</button>
                </p>
            </div>
        </div>
        <hr>
        {% #item %}
        <div id="data_{% _id %}">
            <div class="form-group">
                <label class="col-md-2 control-label">Product Name</label>
                <div class="col-md-5">
                    <input type="hidden" value="{% product_id %}" id="product_id_{% _id %}" name="product_id">
                    <input type="hidden" name="choose_product" id="choose_product_{% _id %}" value="{% choose_product %}">
                    <input type="text" value="{% product %}" class="form-control input-sm" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 col-sm-12 col-xs-12 control-label"></label>
                <div class="col-md-1 col-sm-6 col-xs-12">
                    <label>Point</label>
                    <strong><p id="pohttp://drx.test/invoice-pointint">{% point %}</p></strong>
                </div>
                <div class="col-md-1 col-sm-6 col-xs-12">
                    <label>Qty</label>
                    <input type="hidden" id="current_point" name="current_point" value="{% current_point %}">
                    <input type="number" min="0" class="form-control input-sm" id="qty" name="qty" onChange="changeCount()" value="{% qty %}" disabled>
                </div>
                <div class="col-md-3 http://drx.test/invoice-pointcol-sm-6 col-xs-12">
                    <label>Sub Total</label>
                    <input type="text" id="sub_total_{% _id %}" class="form-control input-sm" name="sub_total" value="{% subtotal %}" disabled>
                </div>
                <div class="col-md-1 col-sm-12 col-xs-12 text-right">
                    <label>Action</label>
                    <p>
                        <button type="button" id="edit_{% _id %}" class="btn edit btn-success btn-xs" data-id="{% _id %}"><i class="fa fa-edit"></i></button>
                        <button type="button" id="delete_{% _id %}" class="btn delete btn-danger btn-xs" data-id="{% _id %}"><i class="fa fa-trash"></i></button>
                    </p>
                </div>
            </div>
        </div>

        <div class="form hidden" id="form_{% _id %}">
            <div id="data_{% _id %}">
                <div class="form-group">
                    <label class="col-md-2 control-label">Product Name</label>
                    <input type="hidden" name="product_id" id="product_idx_{% _id %}" value="{% product_id %}">
                    <div class="col-md-5">
                        <select class="form-control input-sm" name="product" id="product_{% _id %}" onChange="changeQtyWithId(% _id %)" style="width: 100%">
                            <option></option>
                            @foreach($product as $item)
                            <option value="{{ $item->id_product_point }}">{{ $item->product_point_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 col-sm-12 col-xs-12 control-label"></label>
                    <div class="col-md-1 col-sm-6 col-xs-12">
                        <label>Point</label>
                        <strong><p id="point_{% _id %}">{% point %}</p></strong>
                    </div>
                    <div class="col-md-1 col-sm-6 col-xs-12">
                        <label>Qty</label>
                        <input type="hidden" id="current_point_{% _id %}" name="current_point" value="{% current_point %}">
                        <input type="number" min="0" class="form-control input-sm" id="qty_{% _id %}" name="qty" onChange="changeCountWithId({% _id %})" value="{% qty %}" required>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <label>Sub Total</label>
                        <input type="text" id="sub_total_x_{% _id %}" class="form-control input-sm" name="sub_total" value="{% subtotal %}" readonly>
                    </div>

                    <div class="col-md-1 col-sm-12 col-xs-12 text-right">
                        <label>Action</label>
                        <p>
                            <button type="button" id="save_{% _id %}" class="btn save btn-primary btn-xs" data-id="{% _id %}"><i class="fa fa-floppy-o"></i></button>
                            <button type="button" id="cancel_{% _id %}" class="btn cancel btn-warning btn-xs" data-id="{% _id %}"><i class="fa fa-ban"></i></button>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        {% /item %}
    </div>
</script>

@extends('base')

@section('title')
Convert Invoice
@endsection

@section('breadcrumb')
@parent
<li>Convert Invoice</li>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <span style="font-size:20px; font-weight: bold;">CONVERT INVOICE</span>
                <p style="font-size:15px; font-weight: bold;"><i>Exchange or convert product/treatment.</i></p>
            </div>
            <form class="form form-invoice" data-toggle="validator" method="post">
                {{ csrf_field() }} {{ method_field('POST') }}
                <div class="box-body">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <label class="col-md-2 control-label">Customer *</label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="customer" name="customer" autofocus="on"
                                    autocomplete="off" required>
                                <input type="hidden" name="customer_id" id="customer_id">
                                <div id="suggesstion-box"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Full Name</label>
                            <div class="col-md-10">
                                <p class="form-control-static" id="full"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Contact No</label>
                            <div class="col-md-10">
                                <p class="form-control-static" id="contact"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Date of Birth</label>
                            <div class="col-md-10">
                                <p class="form-control-static" id="birthDate"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Outlet</label>
                            <div class="col-md-5">
                                <select id="outlet" name="outlet" class="form-control select2 select2-hidden-accessible"
                                    style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    @if (count($outlet) >= 1)
                                        <option selected="selected" value="">-- Select Outlet --</option>
                                    @endif
                                    @foreach ($outlet as $list)
                                        <option value="{{ $list->id_outlet }}">{{ $list->outlet_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Source</label>
                            <div class="col-md-5">
                                <select id="marketing_source" name="marketing_source"
                                    class="form-control select2 select2-hidden-accessible" style="width: 100%;"
                                    tabindex="-1" aria-hidden="true">
                                    @if (count($marketing_source) >= 1)
                                        <option selected="selected" value="">-- Select Source --</option>
                                    @endif
                                    @foreach ($marketing_source as $list)
                                        <option value="{{ $list->id_marketing_source }}">{{ $list->marketing_source_name }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Invoice Date</label>
                            <div class="col-md-5">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="date_invoice"
                                        name="date_invoice" name="date" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Expired Date</label>
                            <div class="col-md-5">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="date_expired"
                                        name="date_expired" name="date" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Served 1 By</label>
                            <div class="col-md-5">
                                <select id="consultant" name="consultant"
                                    class="form-control select2 select2-hidden-accessible" style="width: 100%;"
                                    tabindex="-1" aria-hidden="true" required>
                                    <option selected="selected" value="">-- Select Consultant --</option>
                                    @foreach ($consultant as $list)
                                    <option value="{{ $list->id_consultant }}">{{ $list->consultant_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Served 2 By</label>
                            <div class="col-md-5">
                                <select id="therapist" name="therapist"
                                    class="form-control select2 select2-hidden-accessible" style="width: 100%;"
                                    tabindex="-1" aria-hidden="true" required>
                                    <option selected="selected" value="">-- Select Therapist --</option>
                                    @foreach ($therapist as $list)
                                    <option value="{{ $list->id_therapist }}">{{ $list->therapist_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Agent / Hairstylist</label>
                            <div class="col-md-5">
                                <select id="agent" name="agent"
                                    class="form-control select2 select2-hidden-accessible" style="width: 100%;"
                                    tabindex="-1" aria-hidden="true" required>
                                    <option selected="selected" value="0">-- Without Agent --</option>
                                    @foreach ($agent as $list)
                                    <option value="{{ $list->id_agent }}">{{ $list->agent_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="panel panel-default">
                            <div class="panel-heading"><strong>Package</strong> </div>
                            <div class="panel-body">
                                <div class="row" id="package_section">
                                    <div id="render"></div>
                                    @include('invoice.package')
                                    <input type="hidden" name="save_json" id="save_json" value="[]">
                                </div>
                            </div>
                        </div>

                        <div id="table-hidden" class="box box-default hidden">
                            <label for="package_code" class="col-md-2 control-label">Contra Form</label>
                            <div class="col-md-10">
                                <table class="table table-striped tabel-convert dt-responsive nowrap" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="30%">Invoice</th>
                                            <th>Value</th>
                                            <th width="30%">Item Name</th>
                                            <th align="right">Deducted</th>
                                            <th align="right">Balance</th>
                                            <th>Qty</th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-convert">
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Contra Amount</label>
                            <div class="col-md-5">
                                <input class="form-control" type="text" name="contra-amount" id="contra-amount"
                                    readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">SubTotal (After Contra)</label>
                            <div class="col-md-5">
                                <input type="hidden" name="total" id="total">
                                <input class="form-control" type="text" name="total_package" id="total_package"
                                    readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Payment(s)</label>
                            <div class="col-md-10">
                                <div class="form-horizontal">
                                    <div class="col-md-3">
                                        <label>Payment Method</label>
                                        <select class="form-control" name="payment_type[]" required="on">
                                            @foreach ($bank as $item)
                                            <option value="{{ $item->id_bank }}">{{ $item->bank_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Amount</label>
                                        <input type="text" class="form-control" id="amount" name="amount[]" rel="amount"
                                            placeholder="Amount(s)" required>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Info</label>
                                        <input type="text" class="form-control" id="payment_info" name="payment_info[]"
                                            placeholder="Info">
                                    </div>
                                    <div class="col-md-1">
                                        <label>Action</label>
                                        <p>
                                            <input id="pay" value="1" type="hidden">
                                            <button type="button" id="add" class="btn btn-info btn-xs"
                                                onclick="addPaymant()"><i class="fa fa-plus"></i></button>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div id="DivPayment"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Paid Balance </label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="paid" name="paid" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Unpaid Balance </label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="unpaid" name="unpaid" value="0" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Old Invoice </label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="old_invoice" name="old_invoice"
                                    placeholder="Migration Invoice">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Remarks </label>
                            <div class="col-md-5">
                                <textarea class="form-control" name="remarks">Convert :</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer text-right">
                    <button type="submit" class="btn btn-primary btn-save simpan"><i class="fa fa-floppy-o"></i> Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')

<script>
    var  save_json = [];
    $('#date_invoice, #date_expired').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });
    $('#date_invoice').datepicker('setDate', 'today')
    $('#date_expired').datepicker('setDate', '+1y')

    $(function(){
        //chooseOutlet();
        findCustomer();
        hitung();

        $('.form-invoice').validator().on('submit', function(e){
            var id_customer = $('#customer_id').val();
            var cek_json = JSON.parse($('#save_json').val());

            if (cek_json.length > 0) {
                if(!e.isDefaultPrevented()){
                    $.ajax({
                        url : "{{ route('invoice-convert.store') }}",
                        type : "POST",
                        data : $('.form-invoice').serialize(),
                        success : function(data){
                            swal({
                                title: "Successfully Create Invoice",
                                icon: "success",
                                buttons: ["Finish", "Print"],
                                dangerMode: true,
                            })
                            .then((print) => {
                                if (print) {
                                    window.open("invoice-convert/"+data.id_invoice+"/print")
                                    window.location.href = "/customers/"+data.id_invoice+"/detail";
                                } else {
                                    window.location.href = "/customers/"+data.id_invoice+"/detail";
                                }
                            });
                        },
                        error : function(){
                            alert("Can not Save The Data");
                        }
                    });
                    return false;
                }
            }else{
                    swal("The Package is Still Empty! Please Press Action", {
                                icon: "error",
                        });
                        return false;
            }
        });
    });


    // function chooseOutlet() {
    //     if (outlet == undefined) {
    //         $('#package_section').addClass('hidden');
    //     }

    //     $('#outlet').change(function () {
    //         outlet = $('#outlet').val();
    //         $('#package_section').removeClass('hidden');

    //         // $.ajax({
    //         //     url : "/invoice-create/"+outlet+"/data/package",
    //         //     type: 'GET',
    //         //     dataType: "JSON",
    //         //     success: function(data) {
    //         //         $('select[name="package"]').empty();
    //         //         for (let index in data) {
    //         //             $('select[name="package"]').append('<option value="'+index+'">'+data[index]+'</option>')
    //         //         }
    //         //     },
    //         //     error: function () {
    //         //         alert('Can not Save The Data');
    //         //     }
    //         // });
    //     });
    // }

    var typingTimer, waitTyping = 1250;    
    function findCustomer(){
        $('#customer').keyup(function(){
            var query = $(this).val();

            clearTimeout(typingTimer);
            typingTimer = setTimeout(function () {
                lookupCustomer(query)
            }, waitTyping)
        });

        $('#customer').keydown(function() {
            clearTimeout(typingTimer)
        })
    }

        /**
        * Lookup customer data after user finish typing.
        */
        function lookupCustomer(query) {
            if(query != ''){
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('customer.keyword') }}",
                    method:"POST",
                    data:{query:query, _token:_token},
                    dataType: "json",
                    success:function(data){
                        $('#suggesstion-box').fadeIn();
                        $('#suggesstion-box').html(data);
                    }
                });
            }else{
                var _token = $('input[name="_token"]').val();
                $.ajax({
                url:"{{ route('customer.all') }}",
                method:"POST",
                data:{query:query, _token:_token},
                dataType: "json",
                success:function(data){
                    $('#suggesstion-box').fadeIn();
                    $('#suggesstion-box').html(data);
                }
                });
            }
    }

    function detailCustomer(id, induk, name, phone, birth) {
        $('#customer').val(induk);
        $('#suggesstion-box').fadeOut();
        $('#customer_id').val(id);
        $('#full').html(name);
        $('#contact').text(phone);
        $('#birthDate').text(birth);

        $.ajax({
            url : "/invoice-convert/"+id+"/data",
            type: 'GET',
            dataType: "JSON",
            success: function(data) {
                $("#table-convert > tr").remove();
                if (data.length > 0) {
                    let tbody = '';

                    for (let i = 0; i < data.length; i++) {
                        if (data[i].qty) {
                            tbody += '<tr>'+
                                '<td>' + data[i].inv_code + '</td>'+
                                '<td>' + format_money(data[i].nominal) + '</td>'+
                                '<td>' + data[i].product_treatment_name + '</td>'+
                                '<td>' + data[i].used + '</td>'+
                                '<td>' + data[i].qty + '</td>'+
                                '<td>'+
                                    '<input style="width:50px" id="jumlah" onChange="changeCountConvert()" type="number" name="jumlah[]" value="0" min="0" max="'+data[i].qty+'">'+
                                    '<input id="total_convert" type="hidden" name="total_convert[]" value="' + data[i].nominal + '" >'+
                                    '<input id="invoice_code" type="hidden" name="invoice_code[]" value="' + data[i].inv_code + '" >'+
                                    '<input id="id_inv_package" type="hidden" name="id_inv_package[]" value="' + data[i].id_invoice_package + '" >'+
                                    '<input id="tot_qty" type="hidden" name="tot_qty[]" value="' + data[i].qty + '" >'+
                                '</td>'+
                            '</tr>';
                        }
                    }

                    $("#table-convert").append(tbody);
                    $('#table-hidden').removeClass('hidden')
                }else{
                    $("#table-convert > tr").remove();
                    $('#table-hidden').addClass('hidden')
                }
            }
        });
    }

    function changeCountConvert(){
        let arr = document.getElementsByName('total_convert[]');
        let arr2 = document.getElementsByName('jumlah[]');
        let tot=0;
		let total_contra=0;

		for(let i=0;i<arr.length;i++){
			if(parseInt(arr[i].value))
				tot += parseInt(arr[i].value);
    	}
	    for(let i=0;i<arr2.length;i++){
            if (parseInt(arr2[i].value))
                total_contra+=parseInt(arr2[i].value)*parseInt(arr[i].value);
        }

        $('#contra-amount').val(format_money(total_contra));
        let total = $('#total').val();

        let total_after = total_contra - remove_format(total);
        $('#total_package').val(format_money(total_after));
    }

    function addPaymant() {
        hitung();
        var _l = $("input[rel=amount]").length+1;
        var inputbaru = "<div class='form-group' id='row" + _l + "'>"+
                "<label class='col-md-2 control-label'></label>"+
                    "<div class='col-md-10'>"+
                        "<div class='form-horizontal'>"+
                            "<div class='col-md-3'>"+
                                "<label>Payment Method ("+ _l +")</label>"+
                                "<select class='form-control' name='payment_type[]' required='on'>"+
                                    "@foreach ($bank as $item)"+
                                        "<option value='{{ $item->id_bank }}'>{{ $item->bank_name }}</option>"+
                                    "@endforeach"+
                                "</select>"+
                            "</div>"+
                            "<div class='col-md-3'>"+
                                "<label>Amount ("+ _l +")</label>"+
                                "<input type='text' class='form-control' id='amount' name='amount[]' rel='amount' placeholder='Amount(s)' required>"+
                            "</div>"+
                            "<div class='col-md-3'>"+
                                "<label>Info ("+ _l +")</label>"+
                                "<input type='text' class='form-control' id='payment_info' name='payment_info[]' placeholder='Info'>"+
                            "</div>"+
                            "<div class='col-md-1'>"+
                                "<label>Action</label>"+
                                "<button class='  btn btn-info btn-xs' onclick='hapusPayment(\"#row" + _l + "\"); return false;'><i class='fa fa-minus'></i></button>"+
                            "</div>"+
                        "</div>"+
                    "</div>";

        $("#DivPayment").append(inputbaru);
        hitung();
        return false;
    }

    function hitung() {
        $("input[rel=amount]").bind('keyup',function(){
            var total = $('#total_package').val().replace(/,/g, '');

            var awal = 0;
            $("input[rel=amount]").each(function(){
                this.value=this.value.replace(/[^0-9]/g,'');
                if(this.value !='') awal += parseInt(this.value,10);
            });
            $('#paid').val(format_money(awal));
            let paid = parseInt(total, 10) + awal;
            if (paid > 0) {
                paid = 0;
                $('#unpaid').val(paid);
            }else{
                $('#unpaid').val(format_money(paid));
            }
        });
    }

    function hapusPayment(pay) {
        $(pay).remove();
        hitung();
	}

    function remove_format(money) {
        return money.toString().replace(/[^0-9]/g,'');
    }

    function format_money(money){
        return money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }

    function changeType() {
        let type = $('#type_line').val();
        let disc1 = $('#discount_1').val();
        let disc2 = $('#discount_2').val();
        let price = $('#current_price').val();
        let qty = $('#qty').val();
        let subtotal = qty * remove_format(price);

        let total1, grandTotal;
        if (disc2) {
            if (type == 0) {
                total1 = subtotal - (disc1 / 100) * subtotal;
                grandTotal = total1 - (disc2 / 100) * total1;
            } else {
                total1 = subtotal - disc1;
                grandTotal = total1 - disc2;
            }
        }else{
            if (type == 0) {
                grandTotal = subtotal - (disc1 / 100) * subtotal;
            } else {
                grandTotal = subtotal - disc1;
            }
        }

        $('#sub_total').val(format_money(Math.round(grandTotal)));
    }

    function changeTypeWithId(id) {
        let type = $('#type_line_'+id).val();
        let disc1 = $('#discount_1_'+id).val();
        let disc2 = $('#discount_2_'+id).val();
        let price = $('#current_price_'+id).val();
        let qty = $('#qty_'+id).val();
        let subtotal = qty * remove_format(price);

        let total1, grandTotal;
        if (disc2) {
            if (type == 0) {
                total1 = subtotal - (disc1 / 100) * subtotal;
                grandTotal = total1 - (disc2 / 100) * total1;
            } else {
                total1 = subtotal - disc1;
                grandTotal = total1 - disc2;
            }
        }else{
            if (type == 0) {
                grandTotal = subtotal - (disc1 / 100) * subtotal;
            } else {
                grandTotal = subtotal - disc1;
            }
        }

        $('#sub_total_x_'+id).val(format_money(Math.round(grandTotal)));
    }

    function changeCountWithId(id) {
        changeTypeWithId(id);
    }

    function changeCount(){
        changeType();
    }

    function getItemSellableStock(e) {
        let elementId = $(e).attr('id');
        let $el = $(`#${elementId} option[value="${e.value}"]`);

        let type = $el.attr('data-type').toLowerCase();

        let id = $el.attr('data-id');

        if (type == 'package') {
            id = $el.attr('data-package-id');
        }
        let endpoint = `/api/v1/logistics/items/sales_informations/${type}/${id}/sellables`;
        let token = '{{ auth()->user()->api_token }}';

        $('input[id^="qty"]').attr('readonly', 'readonly');

        $.ajax({
            url: endpoint,
            type: 'GET',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', `Bearer ${token}`);
            },
            success: function (res) {
                stockInformation = res.sellable;

                $('input[id^="qty"').removeAttr('readonly');

            
                if (stockInformation.sellable > 0) {
                    if (elementId == 'package') {
                        $('#qty').val(1);
                    } else {
                        let parts = elementId.split('_');
                        $('#qty_'+parts[1]).val(1);
                    }
                }
            }
        });
    }

</script>
<script src="{{ asset('js/invoice.js') }}"></script>
@endsection

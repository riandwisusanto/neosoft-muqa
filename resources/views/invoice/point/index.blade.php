@extends('base')

@section('title')
Redeem Point
@endsection

@section('breadcrumb')
@parent
<li>Redeem Point</li>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <span style="font-size:20px; font-weight: bold;">REDEEM POINT</span>
                <p style="font-size:15px; font-weight: bold;"><i>Redeem loyalty point.</i></p>
            </div>
            <form class="form form-invoice" data-toggle="validator" method="post">
                <div class="box-body">
                    <div class="form-horizontal">
                        {{ csrf_field() }} {{ method_field('POST') }}
                        <div class="form-group">
                            <label class="col-md-2 control-label">Customer *</label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="customer" name="customer" autofocus="on"
                                    autocomplete="off" required>
                                <input type="hidden" name="customer_id" id="customer_id">
                                <div id="suggesstion-box"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Full Name</label>
                            <div class="col-md-10">
                                <p class="form-control-static" id="full"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Contact No</label>
                            <div class="col-md-10">
                                <p class="form-control-static" id="contact"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Date of Birth</label>
                            <div class="col-md-10">
                                <p class="form-control-static" id="birthDate"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Current Customer Point</label>
                            <div class="col-md-10">
                                <p class="form-control-static" id="customerPoint"></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Outlet</label>
                            <div class="col-md-5">
                                <select id="outlet" name="outlet" class="form-control select2 select2-hidden-accessible"
                                    style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    @if (count($outlet) >= 1)
                                    <option selected="selected" value="">-- Select Outlet --</option>
                                    @endif
                                    @foreach ($outlet as $list)
                                    <option value="{{ $list->id_outlet }}">{{ $list->outlet_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><strong>Item(s)</strong> </div>
                            <div class="panel-body">
                                <div class="row" id="product_section">
                                    <div id="render"></div>
                                    @include('invoice.productPoint')
                                    <input type="hidden" name="save_json" id="save_json" value="[]">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Total</label>
                            <div class="col-md-5">
                                <input class="form-control" type="text" name="total" id="total" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Remarks </label>
                            <div class="col-md-5">
                                <textarea class="form-control" name="remarks"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer text-right">
                    <button type="submit" class="btn btn-primary btn-save simpan" id="simpan"><i class="fa fa-floppy-o"></i> Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')

<script>
    var table, table_product, outlet;

    $('#date_invoice').datepicker('setDate', 'today')
    $('#date_expired').datepicker('setDate', '+1y')

    $(function(){
        chooseOutlet();
        findCustomer();
        hitung();
        $('.form-invoice').validator().on('submit',function(e){
            var id_customer = $('#customer_id').val();
            var cek_json = JSON.parse($('#save_json').val());
            if (cek_json.length > 0) {
                let total = $('#total').val();
                let customerPoint = $('#customerPoint').text();
                console.log('total',total);
                console.log('customerPoint',customerPoint);
                console.log('test',parseInt(total) <= parseInt(customerPoint));
                if (parseInt(total) <= parseInt(customerPoint)){
                    if(!e.isDefaultPrevented()){
                        $.ajax({
                            url : "{{ route('invoice-point.store') }}",
                            type : "POST",
                            data : $('.form-invoice').serialize(),
                            success : function(data){
                                swal({
                                    title: "Successfully Redeem Point",
                                    icon: "success",
                                    buttons: ["Finish", "Print"],
                                    dangerMode: true,
                                })
                                    .then((print) => {
                                        if (print) {
                                            window.open("invoice-point/"+data.id_invoice+"/print-thermal")
                                            window.location.href = "/customers/"+data.id_invoice+"/redeem-detail";
                                        } else {
                                            window.location.href = "/customers/"+data.id_invoice+"/redeem-detail";
                                        }
                                    });
                            },
                            error : function(){
                                alert("Can not Save The Data");
                            }
                        });
                        return false;
                    }
                }else{
                    swal("Not Enough Point", {
                        icon: "error",
                    });
                    return false;
                }
            }else{
                swal("The Product is Still Empty! Please Press Action", {
                    icon: "error",
                });
                return false;
            }
        });
    });

    function format_money(money){
        return money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }

    function chooseOutlet() {

        $('#outlet').change(function () {
            outlet = $('#outlet').val();
        });
    }

    function hitung() {
        $("input[rel=amount]").bind('keyup',function(){
            var total = $('#total').val();
            var awal = 0;
            $("input[rel=amount]").each(function(){
                this.value=this.value.replace(/[^0-9]/g,'');
                if(this.value !='') awal += parseInt(this.value,10);

            });
        });
    }

    function findCustomer(){
        $('#customer').keyup(function(){
            var query = $(this).val();

            if(query != ''){
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('customer.keyword') }}",
                    method:"POST",
                    data:{query:query, _token:_token},
                    dataType: "json",
                    success:function(data){
                        $('#suggesstion-box').fadeIn();
                        $('#suggesstion-box').html(data);
                    }
                });
            }else{
                var _token = $('input[name="_token"]').val();
                $.ajax({
                url:"{{ route('customer.all') }}",
                method:"POST",
                data:{query:query, _token:_token},
                dataType: "json",
                success:function(data){
                    $('#suggesstion-box').fadeIn();
                    $('#suggesstion-box').html(data);
                }
                });
            }
        });
    }

    function detailCustomer(id, induk, name, nickname, year, address, phone, birth, point, email, address) {
        $('#customer').val(induk);
        $('#suggesstion-box').fadeOut();
        $('#customer_id').val(id);
        $('#full').html(name);
        $('#contact').text(phone);
        $('#birthDate').text(birth);
        $('#customerPoint').text(point ? point: '0');
    }

    function remove_format(money) {
        return money.toString().replace(/[^0-9]/g,'');
    }

    function format_money(money){
        return money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }

    function changeQty() {
        $('#qty').val(1);
        $('#sub_total').val(1);
    }

    function changeQtyWithId(id) {
        $('#qty_'+id).val(1);
        $('#sub_total_x'+id).val(1);
    }

    function changeType() {
        let point = $('#current_point').val();
        let qty = $('#qty').val();
        let subtotal = qty * remove_format(point);
        $('#sub_total').val(format_money(Math.round(subtotal)));
    }

    function changeTypeWithId(id) {
        let point = $('#current_point_'+id).val();
        let qty = $('#qty_'+id).val();
        let subtotal = qty * remove_format(point);

        $('#sub_total_x_'+id).val(format_money(Math.round(subtotal)));
    }

    function changeCountWithId(id) {
        changeTypeWithId(id);
    }

    function changeCount(){
        changeType();
    }

</script>
<script src="{{ asset('js/redeem_point.js') }}"></script>
@endsection

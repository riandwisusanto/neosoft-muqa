<div class="row">
    <div class="col-xs-12">
        <span style="font-size:20px; font-weight: bold;">TREATMENT</span>
        <br><br>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid table-responsive no-padding" style="box-shadow: none;">
            <form action="{{ route('customer.treatment-balance', $id) }}" method="POST">
                <table class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                        <tr class="text-capitalize">
                            <th></th>
                            <th class="all">Invoice Date</th>
                            <th>Expired Date</th>
                            <th>Treatment</th>
                            <th>Balance</th>
                            <th class="all" width="10%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{ csrf_field() }}
                        @foreach ($invoice as $list)
                        @if ($list->remaining == 0)
                            @foreach ($list->invoice_detail as $item)
                            @foreach ($item->invoice_package as $package)
                                <tr>
                                    <td>
                                        @php
                                        $date = str_replace('/', '-', $list->expired_date );
                                        $newDate = date("Y-m-d", strtotime($date));
                                        @endphp
                                        @if ($package->qty <= 0 || $newDate <= now()->toDateString())
                                            <input type="checkbox" name="check_code[]" id="check_code"
                                                value="{{ $package->id_invoice_package }}"
                                                disabled>
                                            @else
                                            <input type="checkbox" name="check_code[]" id="check_code"
                                                value="{{ $package->id_invoice_package }}">
                                            @endif
                                    </td>
                                    <td>{{ $list->inv_date }}</td>
                                    <td>{{ $list->expired_date }}</td>
                                    <td>{{ $package->product_treatment->product_treatment_name ?? "-" }}</td>
                                    <td>{{ $package->qty }}</td>
                                    <td>
                                        <a href="#" data-title="{{ $package->product_treatment->product_treatment_name ?? "-" }}" data-id="{{ $package->id_invoice_package }}" class="btn show-history btn-warning btn-xs">
                                            <i class="fa fa-credit-card"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            @endforeach
                        @endif
                        @endforeach
                    </tbody>
                </table>
                <button class="btn btn-success" type="submit"><i class="fa fa-handshake-o" aria-hidden="true"></i> Usage</button>
            </form>
        </div>
    </div>

    @include('customers.treatment.history')

    @push('script')
        <script>
            let loadingIcon = '<i class="fa fa-refresh fa-spin"></i>'
            let defaultIcon = '<i class="fa fa-credit-card"></i>'
            let emptyHistory = `
                <tr>
                    <td colspan="6" class="text-center">No Treatment History</td>
                </tr>
            `
            $('.show-history').click(function (e) {
                e.preventDefault()
                let id = $(this).data('id')
                let title = $(this).data('title')
                $('#historyModalLabel').html(`${title} Treatment History`)
                

                $.ajax({
                    url: `{{ url('/') }}/api/v1/customers/${id}/histories`,
                    beforeSend: function (xhr) {
                        $(this).html(loadingIcon)
                        $('#histories').html('')
                        xhr.setRequestHeader('Authorization', 'Bearer {{ auth()->user()->api_token }}')
                    },
                    success: function (res) {
                        $(this).html(defaultIcon)

                        if ( ! res.length) {
                            $('#histories').html(emptyHistory)
                            $('#historyModal').modal('show')
                            return
                        }

                        let html = ""
                        let row = ""
                        res.forEach((history, index) => {
                            row = `<tr>
                                <td>${index+1}</td>
                                <td>${history.usage_code}</td>
                                <td>${history.usage_date}</td>
                                <td>${history.used}</td>
                                <td>${history.therapists}</td>
                                <td>
                                    <a href="/usage-treatment/${history.id_usage}" class="btn btn-primary btn-sm">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </td>
                            </tr>`
                            html += row
                        });

                        $('#histories').html(html)
                        $('#historyModal').modal('show')
                    }
                })
            })
        </script>
    @endpush
</div>

@extends('base')

@section('title')
Treatment Balance
@endsection

@section('breadcrumb')
@parent
<li>Treatment Balance</li>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h4></h4>
            </div>
            <form class="form-horizontal form-treatment-balance" data-toggle="validator" method="post">
                {{ csrf_field() }}
                <div class="box-body">
                    <input type="hidden" name="customer_id" value="{{ $invoice->customer_id }}">
                    <input type="hidden" name="invoice_id" value="{{ $invoice->id_invoice }}">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Customer</label>
                            <div class="col-md-10">
                                <p class="form-control-static">
                                    {{ $invoice->customer->full_name }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Phone.</label>
                            <div class="col-md-10">
                                <p class="form-control-static">
                                    {{ $invoice->customer->phone }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Place, Date of Birth</label>
                            <div class="col-md-10">
                                <p class="form-control-static">
                                    {{ $invoice->customer->birth_place }}, {{ $invoice->customer->birth_date }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Outlet</label>
                            <div class="col-md-3">
                                <p class="form-control-static">
                                    <select id="outlet" name="outlet"
                                        class="form-control select2 select2-hidden-accessible" style="width: 100%;"
                                        tabindex="-1" aria-hidden="true" required>
                                        <option selected="selected" value="">-- Select Outlet --</option>
                                        @foreach ($outlet as $list)
                                        <option value="{{ $list->id_outlet }}">{{ $list->outlet_name }}</option>
                                        @endforeach
                                    </select>
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Utilisation Date</label>
                            <div class="col-md-3">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="datepicker" name="utilisationdate"
                                        data=Y-m-d name="date" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal">
                            <div id="table-hidden" class="box box-default table-responsive no-padding">
                                <div class="col-md-12">
                                    <table class="table table-striped tabel-convert">
                                        <thead>
                                            <tr>
                                                <th width="20%">Invoice Num</th>
                                                <th width="20%">Item Name</th>
                                                <th>Balance</th>
                                                <th>Usage</th>
                                                <th width="50px">Point</th>
                                                <th>Therapis</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($product as $list)
                                            <input type="hidden" name="product_id[]" value="{{ $list->product_id }}">
                                            <input type="hidden" name="inv_package_id[]"
                                                value="{{ $list->id_invoice_package }}">
                                            <input type="hidden" name="anamnesa_detail_id[]" value="{{ $list->anamnesa_detail_id }}">
                                            <input type="hidden" name="durasi[]"
                                                value="{{ $list->product_treatment->duration }}">
                                            <tr>
                                                @foreach ($list->invoice_detail as $invoice_detail)
                                                @foreach ($invoice_detail->invoice as $invoice)
                                                <td><input type="text" name="inv_code[]" readonly
                                                        value="{{ $invoice->inv_code }}"></td>
                                                @endforeach
                                                @endforeach
                                                <td>{{ $list->product_treatment->product_treatment_name }}</td>
                                                <td>{{ $list->qty }}</td>
                                                <td><input style="width:50px" type="number" name="usage[]" id="usage"
                                                        max="{{ $list->qty }}" min=1></td>
                                                <td>
                                                    <select id="point" name="point[]" required>
                                                        <option value="0">0</option>
                                                        @foreach ($list->product_treatment->product_treatment_points as
                                                        $point)
                                                        <option value="{{ $point->point }}">{{ $point->point }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td>
                                                    <select id="therapist"
                                                        name="therapist_{{ $list->id_invoice_package }}[]"
                                                        class="form-control select2 select2-hidden-accessible"
                                                        multiple="" data-placeholder="Select a Therapist"
                                                        style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                                        @foreach ($therapist as $list)
                                                        <option value="{{ $list->id_therapist }}">
                                                            {{ $list->therapist_name }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-6">
                            <label class="col-md-2 control-label">Remarks</label>
                            <div class="col-md-10">
                                <p class="form-control-static">
                                    <textarea name="remarks" id="remarks" cols="50" rows="5"></textarea>
                                </p>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="col-md-2 control-label">Sign Customer Here</label>
                            <div class="col-md-10">
                                <canvas id="signature-pad" class="signature-pad" width=400 height=200></canvas>
                            </div>
                            <input type="hidden" name="signature" id="signature" value="">
                        </div>
                    </div>
                    <div class="box-footer text-right">
                        <button class="btn btn-danger" type="button" id="clear"><i class="fa fa-trash-o" aria-hidden="true"></i> Clear</button>
                        <button class="btn btn-warning" type="button" id="undo"><i class="fa fa-arrow-left" aria-hidden="true"></i> Undo</button>
                        <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                            Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    @endsection

    @section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
    <script>
        $('#datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });
        $('#datepicker').datepicker('setDate', 'today')
        // $(function(){
          var canvas = document.getElementById('signature-pad');
          var signaturePad = new SignaturePad(canvas, {
          });
          var saveButton = document.getElementById('save');
          var clearButton = document.getElementById('clear');
        
        $(function () {
            var canvas = document.getElementById('signature-pad');
          var signaturePad = new SignaturePad(canvas, {
          });
          var saveButton = document.getElementById('save');
          var clearButton = document.getElementById('clear');
          
            $('.form-treatment-balance').validator().on('submit', function(e){
                $("#save, #clear, #undo").prop('disabled', true);
                if(!e.isDefaultPrevented()){
                    $('#signature').val(signaturePad.toDataURL('image/png'))
                    $.ajax({
                        url : "{{ route('usage-treatment.store') }}",
                        type : "POST",
                        data : $('.form-treatment-balance').serialize(),
                    success : function(data){
                        $("#save, #clear, #undo").prop('disabled', false);
                        if (data.status == 'success') {
                            swal({
                                title: "Successfully Create Usage",
                                icon: "success",
                                buttons: ["Finish", "Print"],
                                dangerMode: true,
                            })
                            .then((print) => {
                                if (print) {
                                    window.open("/usage-treatment/"+data+"/print");
                                    window.location.href = "{{ route('customers.show', $invoice->customer_id) }}";
                                } else {
                                    window.location.href = "{{ route('customers.show', $invoice->customer_id) }}";
                                }
                            });
                        } else {
                            swal({
                                title: "Item out of stock",
                                text: data.message,
                                icon: "warning",
                                dangerMode: true,
                            })
                        }
                    },
                    error : function(){
                        alert("Can not Save the Data!");
                    }
                    });
                    return false;
                }
            }); 
        });

        //clearSignature
        document.getElementById('clear').addEventListener('click', function () {
        signaturePad.clear();
        });

        //undoSignature
        document.getElementById('undo').addEventListener('click', function () {
            var data = signaturePad.toData();
        if (data) {
            data.pop(); // remove the last dot or line
            signaturePad.fromData(data);
        }
        });
    </script>
    @endsection
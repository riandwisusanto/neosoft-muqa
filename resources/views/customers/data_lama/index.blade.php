<div class="row">
    <div class="col-xs-12">
        <span style="font-size:20px; font-weight: bold;">DATA LAMA</span>
        <br><br>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="box box-solid" style="box-shadow: none;">
            <div class="box-body">
                <div class="form-horizontal">
                    <div class="row">
                        <div class="col-sm-12">
                            <form action="{{ route('customers.upload') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="rm" value="{{ $overview->induk_customer }}">
                            <input type="hidden" name="customer_id" value="{{ $overview->id_customer }}">
                            <div class="form-group row">
                                <div class="col-sm-4"><input type="file" name="anamnesa"></div>
                                <div class="col-sm-4"><input type="submit" value="UPLOAD" class="btn btn-sm btn-primary"></div>
                                
                            </div>
                            <div class="table-responsive">
                                <table class="table table-sm" style="min-width: 1550px">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th width="150px">Date</th>
                                            <th width="100px">Time</th>
                                            <th width="400px">Ax & Dx</th>
                                            <th width="200px">User</th>
                                            <th width="200px">Treatment</th>
                                            <th width="200px">Prescription Name</th>
                                            <th width="150px">Outlet</th>
                                            <th width="150px">Record Type</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if (count($oldAnamnesas) > 0)
                                        @foreach ($oldAnamnesas as $index => $row)
                                        <tr>
                                            <td>{{ $index + 1 }}</td>
                                            <td>{{ $row->checkup_date }}</td>
                                            <td>{{ $row->checkup_time }}</td>
                                            <td>{{ $row->ax_dx }}</td>
                                            <td>{{ $row->username }}</td>
                                            <td>{{ $row->treatment_name }}</td>
                                            <td>{{ $row->prescription_name }}</td>
                                            <td>{{ $row->outlet_name }}</td>
                                            <td>{{ $row->record_type }}</td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="8">No data available</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            </form>
                        </div>

                        <div class="col-sm-12">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .table-fixed {

        /* width: 100%; */
        tbody {
            height: 50px;
            overflow-y: auto;
            width: 100%;
        }

        thead,
        tbody,
        tr,
        td,
        th {
            display: block;
        }

        tbody {
            td {
                float: left;
            }
        }

        thead {
            tr {
                th {
                    float: left;
                    background-color: #f39c12;
                    border-color: #e67e22;
                }
            }
        }
    }
</style>

@include('customers.information.add_consultant')
@include('customers.information.add_outlet')


@push('script')
<script>
    var save_method_information, save_method;

    function addConsultant(){
        save_method_information = "add";
        $('input[name=_method]').val('POST');
        $("#modal-consultant").modal({
        backdrop: 'static',
        keyboard: false,
        show: true
        });
        $('#modal-consultant form')[0].reset();
        $('#group_id').val('').change()
        $('.modal-title').text('Add Consultant');
    }

    function addOutlet(){
        save_method_information = "add";
        $('input[name=_method]').val('POST');
        $("#modal-outlet").modal({
        backdrop: 'static',
        keyboard: false,
        show: true
        });
        $('#modal-outlet form')[0].reset();
        $('#group_id').val('').change()
        $('.modal-title').text('Add Outlet');
    }

    $('#datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });

    $(function(){
        chooseCountry();
        validation();
    });

    function getCity(country, city){
    $.ajax({
        url:'/customers/'+country+'/province',
        type:'GET',
        dataType: 'json',
            beforeSend: function() {
                $(".loader").removeClass('hidden');
                $(".loader").show();
            },
            success:function(data){
                $('select[name="city"]').empty();
                $(".loader").addClass('hidden');
                $(".loader").hide();
                for (let index in data) {
                    $('select[name="city"]').append('<option value="'+index+'">'+data[index]+'</option>')
                }
                if (city) {
                    $('select[name="city"]').val(city).change();
                }
                return false;
            }
    });
    }

    function chooseCountry() {
    $('#country').change(function(){
        var countryID = $(this).val();
        if(countryID){
        getCity(countryID);
        }
        return false;
    });
    }

    $('#modal-customers form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
         var id = $('#id').val();
         if(save_method == "add") url = "{{ route('customers.store') }}";
         else url = id;
         $.ajax({
            url : url,
            type : "POST",
            data : $('#modal-customers form').serialize(),
           success : function(data){
                if (data.status == false) {
                    alert('Something Wrong!!, Please Check Message Error!!!');
                    $("#msg_email").html(data.message.email).show();
                    $("#msg_phone").html(data.message.phone).show();
                }else{
                    $('#gender').val('').trigger('change');
                    $('#religion').val('').trigger('change');
                    $('#status').val('').trigger('change');
                    $('#country').val('').trigger('change');
                    $('#outlet').val('').trigger('change');
                    $('#city').val('').trigger('change');
                    $('#consultant').val('').trigger('change');
                    $('#modal-customers').modal('hide');
                    $("#msg_email").html('');
                    $("#msg_phone").html('');
                    location.reload(true);
                    return false;
                }
           },
           error : function(e){
               console.log(e);
             alert("Can not Save the Data!");
           }
         });
         return false;
     }
   });

    function editPatient(id){
        save_method = "edit";
        $('input[name=_method]').val('PATCH');
        $('#modal-customers form')[0].reset();
        $.ajax({
            url : "/customers/"+id+"/edit",
            type : "GET",
            dataType : "JSON",
            success : function(data){
            $('#modal-customers').modal('show');
            $('.modal-title').text('EDIT PATIENT');
            $('.modal-doc').text('Edit patient’s data.');
            $('#btn-more').css('margin-left', '50%');
            $('#id').val(data.edit.id_customer);
            $('#fullname').val(data.edit.full_name);
            $('#nick_name').val(data.edit.nick_name);
            $('#phone').val(data.edit.phone);
            $('#email').val(data.edit.email);
            $('#place').val(data.edit.birth_place);
            $('#datepicker').val(data.edit.birth_date);
            $('#join_date').val(data.edit.join_date);
            $('#gender').val(data.edit.gender).trigger('change');
            $('#religion').val(data.edit.religion).trigger('change');
            $('#status').val(data.edit.status_customer).trigger('change');
            $('#noktp').val(data.edit.ktp);
            $('#address').val(data.edit.address);
            $('#occupation').val(data.edit.occupation);
            $('#zip').val(data.edit.zip);
            $('#country').val(data.edit.country_id).trigger('change');
            getCity(data.edit.country_id, data.edit.city_id);
            $('#outlet').removeAttr('required');
            $('#consultant').removeAttr('required');
            $('#group_outlet').hide();
            $('#group_consultant').hide();
            $('#familyname').val(data.edit.nextofikin);
            $('#familyphone').val(data.edit.nextofikin_number);
            $('#source').val(data.edit.source_id).trigger('change');
            $('#source_remarks').val(data.edit.source_remarks);
            $('#purpose_treatment').val(data.edit.purpose_treatment);
            },
            error : function(e){
            alert("Can not Show the Data!");
            }
        });
        return false;
    }

    // validas
    function validation() {
    $("#noktp").keypress(function(data){
        //console.log(data.which);
        if(data.which < 48 || data.which > 57)
        {
            $("#msg_ktp").html("Please insert real IC numbers").show().fadeOut(4000);
            return false;
        }
        });

        $("#phone").on('keyup change',function(){
            $("#phone").each(function(){
                this.value=this.value.replace(/[^0-9]/g,'');
            });
        });

        $("#familyphone").keypress(function(data){
        if(data.which < 48 || data.which > 57)
        {
            $("#msg_family").html("Please insert real numbers phone").show().fadeOut(4000);
            return false;
        }
        });

        $("#zip").keypress(function(data){
        //console.log(data.which);
        if(data.which < 48 || data.which > 57)
        {
            $("#msg_zip").html("Please insert real Zip Code").show().fadeOut(4000);
            return false;
        }
        });
    }
</script>
@endpush

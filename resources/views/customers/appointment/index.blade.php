@foreach ($appointment as $list)
@php
    $warna = '';
    if ($list->status_appointment == 1){
        $warna = "danger";
    }elseif ($list->status_appointment == 2){
        $warna = "primary";
    }elseif ($list->status_appointment == 3){
        $warna = "success";
    }elseif ($list->status_appointment == 4){
        $warna = "warning";
    }else{
        $warna = "default";
    }
@endphp
<div class="row">
    <div class="col-xs-12">
        <div class="box box-{{ $warna }}">
            <div class="box-header with-border">
                <span style="font-size:20px;">Appointment</span>
                <p style="font-size:15px;"><i>Riwayat appointment perawatan/konsultasi pasien.</i></p>
            </div>
            <div class="box-header with-border">
                <h2 class="pull-right">{{ $list->come_date }}</h2>
            </div>

            <div class="box-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Date</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                {{ $list->come_date }}
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Outlet</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                {{ $list->outlet->outlet_name }}
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Status</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                @if ($list->status_appointment == 1)
                                Booking
                                @elseif ($list->status_appointment == 2)
                                Confirmed
                                @elseif ($list->status_appointment == 3)
                                Complete
                                @elseif ($list->status_appointment == 4)
                                Cancelled
                                @else
                                No Show
                                @endif
                            </p>
                        </div>
                    </div>
                    @if ($list->room)
                    <div class="form-group">
                        <label class="col-md-2 control-label">Room</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                {{ $list->room->room_name }}
                            </p>
                        </div>
                    </div>
                    @endif

                    <div class="form-group">
                        <label class="col-md-2 control-label">Item</label>
                        <div class="col-md-10">

                                <table class="table table-striped tabel-pembelian">
                                    <thead>
                                        <tr>
                                            <th>Start</th>
                                            <th>End</th>
                                            <th>Treatment</th>
                                            <th style="width:200px">Therapists</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            if($list->treatment_id){
                                                $product = $list->product_treatment->product_treatment_name;
                                            }else{
                                                $product = "Consultasi";
                                            }
                                        @endphp

                                            <tr>
                                                <td>{{ $list->start_time }}</td>
                                                <td>{{ $list->end_time }}</td>
                                                <td>{{ $product }}</td>
                                                @php
                                                $getTherapist='';
                                                foreach ($list->therapist as $therapist) {
                                                    $getTherapist .= $therapist->therapist_name.',';
                                                }
                                                @endphp
                                                <td>{{ $getTherapist }}</td>
                                            </tr>
                                    </tbody>
                                </table>

                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Remarks</label>
                    <div class="col-md-10">
                        <p class="form-control-static">
                            {{ $list->remarks }}
                        </p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Status Change</label>
                    <div class="col-md-10">

                            <table class="table table-striped table-bordered" style="width:100%">
                                <caption>Log Change</caption>
                                <thead>
                                    <tr>
                                        <th>Date Time</th>
                                        <th>Status</th>
                                        <th>Changed By</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($list->log_appointment as $logs)

                                    <tr>
                                        <td>{{ $logs->date_time }}</td>
                                        <td>
                                            @if ($logs->log_status == 1)
                                            Booking
                                            @elseif ($logs->log_status == 2)
                                            Confirmed
                                            @elseif ($logs->log_status == 3)
                                            Complete
                                            @elseif ($logs->log_status == 4)
                                            Cancelled
                                            @else
                                            No Show
                                            @endif
                                        </td>
                                        <td>{{ $logs->user->username }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endforeach

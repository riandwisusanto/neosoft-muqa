<div class="box box-solid">
        <div class="box-header with-border">
            <span style="font-size:20px;">Void Invoice</span>
            <p style="font-size:15px;"><i>Informasi penjualan yang dibatalkan.</i></p>
        </div> 
        <div class="box-body table-responsive no-padding">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Invoice</th>
                        <th>Invoice.date</th>
                        <th>Expired.date</th>
                        <th>Amount</th>
                        <th>Invoice By</th>
                        <th>Remarks</th>
                        <th>Void Date</th>
                        <th>Void By</th>
                        <th>Void Remarks</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($void_invoice as $invoice)
                        <tr>
                            <td>{{ $invoice->inv_code }}</td>
                            <td>{{ $invoice->inv_date }}</td>
                            <td>{{ $invoice->expired_date }}</td>
                            <td>{{ $invoice->total }}</td>
                            <td>{{ $invoice->user->username }}</td>
                            <td>{{ $invoice->remarks }}</td>
                            <td>{{ $invoice->log_void_invoice->created_at }}</td>
                            <td>{{ $invoice->log_void_invoice->user->username }}</td>
                            <td>{{ $invoice->log_void_invoice->remarks }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
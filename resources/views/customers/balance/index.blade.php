<div class="box box-solid">
    <div class="box-header with-border">
        <span style="font-size:20px;">Invoice Balance</span>
        <p style="font-size:15px;"><i>Riwayat pelunasan.</i></p>
    </div>
    <div class="box-body table-responsive no-padding">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Balance Code</th>
                    <th>Balance Date</th>
                    <th>Invoice Code</th>
                    <th>Invoice Date</th>
                    <th>Outstanding Amount</th>
                    <th>Balance Amount</th>
                    <th>Remarks</th>
                    <th>Created By</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                $balance_total = 0;
                @endphp
                @foreach ($balance as $item)
                <tr>
                    <td>{{ $item->balance_code }}</td>
                    <td>{{ $item->balance_date }}</td>
                    <td>{{ $item->invoice->inv_code }}</td>
                    <td>{{ $item->invoice->inv_date }}</td>
                    <td>{{ $item->outstanding }}</td>
                    @foreach ($item->payment as $pay)
                        @php
                            if ($pay->balance_id){
                                $balance_total += str_replace(",", "", $pay->amount);
                            }
                        @endphp
                    @endforeach
                    <td>{{ format_money($balance_total) }}</td>
                    <td>{{ $item->remarks }}</td>
                    <td>{{ $item->user->username }}</td>
                    <td><a href="{{ route('invoice-balance.show', $item->id_balance) }}" class='btn btn-primary btn-sm'><i class='fa fa-eye'></i></a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

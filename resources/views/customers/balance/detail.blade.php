@extends('base')

@section('title')
Balance Invoice
@endsection

@section('breadcrumb')
@parent
<li>Balance Invoice</li>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h4>{{ $balance->balance_code }}</h4>
            </div>
            <div class="box-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Balance Num</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                <a href="{{ route('invoice-balance.print', $balance->invoice_id) }}"
                                    target="_blank">{{ $balance->balance_code }}</a>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Outlet</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                {{ $balance->outlet->outlet_name }}
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Name</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                <a
                                    href="{{ route('customers.show', $balance->customer_id) }}">{{ $balance->customer->full_name }}</a>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Phone Number</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                {{ $balance->customer->phone }}
                            </p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Invoice Num</label>
                        <div class="col-md-10">
                            {{ $balance->invoice->inv_code }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Create Date</label>
                        <div class="col-md-10">
                            {{ $balance->invoice->created_at }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Balance Date</label>
                        <div class="col-md-10">
                            {{ $balance->balance_date }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Consultant By</label>
                        <div class="col-md-10">
                            {{ $balance->invoice->consultant->consultant_name }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Doctor</label>
                        <div class="col-md-10">
                            {{ $balance->invoice->therapist->therapist_name }}
                        </div>
                    </div>
                    <div class="form-horizontal">
                        <div id="table-hidden" class="box box-default table-responsive no-padding">
                            <label for="package_code" class="col-md-2 control-label">Purchased Item</label>
                            <div class="col-md-10">
                                <table class="table table-striped tabel-convert">
                                    <thead>
                                        <tr>
                                            <th width="20%">Code</th>
                                            <th width="20%">Name</th>
                                            <th>Paid Qty</th>
                                            <th>Disc</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($balance->invoice_detail as $invoice_detail)
                                            @php
                                                if ($invoice_detail->type_line == 0) {
                                                    $type = '%';
                                                }else {
                                                    $type = 'Rp';
                                                }
                                            @endphp
                                            @foreach ($invoice_detail->invoice_package as $invoice_package)
                                                <tr>
                                                    <td>{{ $invoice_package->product_treatment->product_treatment_code }}</td>
                                                    <td>{{ $invoice_package->product_treatment->product_treatment_name }}</td>
                                                    <td>{{ $invoice_package->qty }}</td>
                                                    <td>{{ $invoice_detail->discount }} {{ $type }}</td>
                                                </tr>
                                            @endforeach
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Total</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                {{ $balance->outstanding }}
                            </p>
                        </div>
                    </div>
                    <div class="form-horizontal">
                        <div id="table-hidden" class="box box-default table-responsive no-padding">
                            <label for="package_code" class="col-md-2 control-label">Payments</label>
                            <div class="col-md-10">
                                <table class="table table-striped tabel-convert">
                                    <thead>
                                        <tr>
                                            <th width="20%">Payment Code</th>
                                            <th width="20%">Date</th>
                                            <th>Payment Amount</th>
                                            <th>Info</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($balance->payment as $item)
                                        <tr>
                                            <td>{{ $item->bank->bank_code }}</td>
                                            <td>{{ $item->created_at }}</td>
                                            <td>{{ $item->amount }}</td>
                                            <td>{{ $item->info }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Remarks</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                {{ $balance->remarks }}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="box-footer text-right">
                    <button class="btn btn-success" onclick="editVoid({{ $balance->id_balance }})"><i class="fa fa-edit"
                            aria-hidden="true"></i> Edit</button>
                            @if (array_intersect(["ADMINISTRATOR"], json_decode(Auth::user()->level)))
                    <button class="btn btn-danger" onclick="voidUsage({{ $balance->id_balance }})"><i
                            class="fa fa-trash-o"></i> Void</button>
                @endif
                </div>
            </div>
        </div>
    </div>
</div>

@include('customers.balance.edit')
@include('customers.balance.void')
@endsection

@section('script')
<script>
    $('#date_balance').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });

    $(function () {
        $('#modal-balance form').validator().on('submit', function(e){
            if(!e.isDefaultPrevented()){
                $.ajax({
                    url : "{{ route('invoice-balance.void', $balance->id_balance) }}",
                    type : "POST",
                    data : $('#modal-balance form').serialize(),
                    success : function(data){
                        $('#modal-balance').modal('hide');
                        swal({
                            title: "Successfully Void Usage",
                            icon: "success",
                        })
                        .then((go) => {
                            window.location.href = "{{ route('customers.show', $balance->customer_id) }}";
                        });
                    },
                    error : function(){
                        alert("Can not Save the Data!");
                    }
                });
                return false;
            }
        });

        $('.form-edit').validator().on('submit', function(e){
            if(!e.isDefaultPrevented()){

                $.ajax({
                    url : "{{ route('invoice-balance.update', $balance->id_balance) }}",
                    type : "PATCH",
                    data : $('.form-edit').serialize(),
                success : function(data){

                    $('#modal-edit').modal('hide');
                    swal({
                        title: "Successfully Edit Balance Invoice",
                        icon: "success",
                    })
                    .then((go) => {
                        location.reload();
                    });
                },
                error : function(){
                    alert("Can not Save the Data!");
                }
                });
                return false;
            }
        });
    });

    function voidUsage(id) {
        $("#modal-balance").modal('show');
        $('.modal-title-balance').text('What is The Reason ?');
        $('input[name=_method]').val('PATCH');
        $('#modal-balance form')[0].reset();
    }

    function editVoid(id) {
        $("#modal-edit").modal('show');
        $('.modal-title-balance').text('{{ $balance->balance_code }}');
        $('input[name=_method]').val('PATCH');
        $('#modal-edit form')[0].reset();
        $('#id').val(id);

    }
</script>
@endsection

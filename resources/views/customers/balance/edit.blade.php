<div class="modal" id="modal-edit" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">
                        &times;</span> </button>
                <h3 class="modal-title-balance"></h3>
            </div>
            <div class="modal-body">
                <form class="form-horizontal form-edit" data-toggle="validator" method="post">
                    {{ csrf_field() }} {{ method_field('PATCH') }}
                    <input type="hidden" name="id" id="id">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Balance Number</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    {{ $balance->balance_code }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Customer</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    {{ $balance->customer->induk_customer }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Full Name</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    {{ $balance->customer->full_name }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Phone</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    {{ $balance->customer->phone }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Outlet</label>
                            <div class="col-md-4">
                                <select id="outlet" name="outlet" class="form-control select2 select2-hidden-accessible"
                                    style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    @foreach ($outlet as $oke)
                                    @php
                                    if ($oke->id_outlet == $balance->outlet_id){
                                    echo "<option value=".$oke->id_outlet." selected='selected'>".$oke->outlet_name."
                                    </option>";
                                    }else {
                                    echo "<option value=".$oke->id_outlet."> ".$oke->outlet_name." </option>";
                                    }
                                    @endphp
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Balance Date</label>
                            <div class="col-md-6">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="date_balance" name="date_balance" value="{{ $balance->balance_date }}" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal">
                            <div id="table-hidden" class="box box-default">
                                <label for="package_code" class="col-md-2 control-label">Purchased Item</label>
                                <div class="col-md-10">
                                    <table class="table table-striped tabel-convert">
                                        <thead>
                                            <tr>
                                                <th width="20%">Code</th>
                                                <th width="20%">Name</th>
                                                <th>Paid Qty</th>
                                                <th>Disc</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($balance->invoice_detail as $invoice_detail)
                                            @php
                                            $type = '%/Rp';
                                            if ($invoice_detail->activity){
                                            if ($invoice_detail->activity->disc_type == 1) {
                                            $type = '%';
                                            }else {
                                            $type = 'Rp';
                                            }
                                            $activityDisc = $invoice_detail->activity->discount;
                                            $activityName = $invoice_detail->activity->activity_name;
                                            }else{
                                            $activityDisc = $invoice_detail->discount;
                                            $activityName = 'Custom Disc. Amount';
                                            }
                                            @endphp
                                            @foreach ($invoice_detail->invoice_package as $invoice_package)
                                            <tr>
                                                <td>{{ $invoice_package->product_treatment->product_treatment_code }}
                                                </td>
                                                <td>{{ $invoice_package->product_treatment->product_treatment_name }}
                                                </td>
                                                <td>{{ $invoice_package->qty }}</td>
                                                <td>{{ $activityDisc }} {{ $type }}</td>
                                            </tr>
                                            @endforeach
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Total</label>
                            <div class="col-md-10">
                                <p class="form-control-static">
                                    {{ $balance->outstanding }}
                                </p>
                            </div>
                        </div>

                        <div class="form-group">
                            @foreach ($balance->payment as $payment)
                            <label class="col-md-2 control-label">Payment(s)</label>
                            <div class="col-md-10">
                                <div class="form-horizontal">
                                    <div class="col-md-3">
                                        <label>Payment Method</label>
                                        <input type="hidden" name="id_payment[]" value="{{ $payment->id_payment }}">
                                        <select class="form-control" name="payment_type[]" required="on">
                                            @foreach ($bank as $item)
                                            @php
                                            if ($item->id_bank == $payment->bank_id){
                                            echo "<option value=".$item->id_bank." selected='selected'>
                                                ".$item->bank_name." </option>";
                                            }else {
                                            echo "<option value=".$item->id_bank."> ".$item->bank_name." </option>";
                                            }
                                            @endphp
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Amount</label>
                                        <input type="text" class="form-control" id="amount" name="amount[]"
                                            value="{{ $payment->amount }}" rel="amount" placeholder="Amount(s)" disabled
                                            required>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Info</label>
                                        <input type="text" class="form-control" id="payment_info" name="payment_info[]"
                                            placeholder="Info">
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Remarks</label>
                            <div class="col-md-4">
                                <p class="form-control-static">
                                    <textarea name="remarks" id="" cols="50" rows="5">{{ $balance->remarks }}</textarea>
                                </p>
                            </div>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary simpan"><i class="fa fa-floppy-o"></i> Save
                        </button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                                class="fa fa-arrow-circle-left"></i>
                            Cancel</button>
                    </div>
                </form>
            </div>


        </div>
    </div>
</div>
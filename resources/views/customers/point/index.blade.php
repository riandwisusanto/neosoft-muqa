<div class="row">
    <div class="col-xs-12">
        <span style="font-size:20px; font-weight: bold;">POINTS</span>
        <br><br>
    </div>
</div>

<div class="box box-solid" style="box-shadow: none;">
    <div class="box-header">
        <div class="row">
            <div class="col-xs-12">
                <span style="font-size: 17px; font-weight: bold; cursor: pointer;" id="slide-toggle-redeem-points">Redeem Points <i class="fa fa-angle-down rotate" style="margin-left: 15px;"></i></span>
            </div>
        </div>
    </div>
    <div class="box-body table-responsive no-padding">
        <div class="table-redeem-points">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr class="text-capitalize">
                        <th>Redeem Invoice</th>
                        <th>Created Date</th>
                        <th>Point Amount</th>
                        <th>Remarks</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $last = $redeem_points->last();
                    @endphp
                    @foreach ($redeem_points->sortBy('created_date') as $invoice)
                        <tr>
                            <td>{{ $invoice->inv_point_code}} @if($last->id_invoice_point == $invoice->id_invoice_point)<span class="label label-danger">New</span>@endif</td>
                            <td>{{ $invoice->created_date }}</td>
                            <td>{{ $invoice->total }}</td>
                            <td>{{ $invoice->remarks }}</td>
                            <td><a href="{{ route('customer.redeem', $invoice->id_invoice_point) }}" class='btn btn-primary btn-sm'><i class='fa fa-eye'></i></a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        
    </div>
</div>

<div class="box box-solid" style="box-shadow: none;">
    <div class="box-header">
        <div class="row">
            <div class="col-xs-12">
                <span style="font-size: 17px; font-weight: bold; cursor: pointer;" id="slide-toggle-void-points">Void Points <i class="fa fa-angle-down rotate" style="margin-left: 15px;"></i></span>
            </div>
        </div>
    </div>
    <div class="box-body table-responsive no-padding">
        <div class="table-void-points">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                <tr class="text-capitalize">
                    <th>Redeem Invoice</th>
                    <th>Created Date</th>
                    <th>Void Date</th>
                    <th>Point Amount</th>
                    <th>Remarks</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $last = $void_redeem_points->last();
                @endphp
                @foreach ($void_redeem_points->sortBy('created_date') as $invoice)
                    <tr>
                        <td>{{ $invoice->inv_point_code}} @if($last->id_invoice_point == $invoice->id_invoice_point)<span class="label label-danger">New</span>@endif</td>
                        <td>{{ $invoice->created_date }}</td>
                        <td>{{ $invoice->void->created_date }}</td>
                        <td>{{ $invoice->total }}</td>
                        <td>{{ $invoice->remarks }}</td>
                        <td><a href="{{ route('customer.redeem', $invoice->id_invoice_point) }}" class='btn btn-primary btn-sm'><i class='fa fa-eye'></i></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
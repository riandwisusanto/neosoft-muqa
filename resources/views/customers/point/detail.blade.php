@extends('base')

@section('title')
    {{ $invoice->created_date }}
@endsection

@section('breadcrumb')
    @parent
    <li>Customers</li>
    <li>Detail Customer</li>
    <li>Invoice</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h4>{{ $invoice->inv_code }}</h4>
                </div>
                <div class="box-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Outlet</label>
                            <div class="col-md-10">
                                <p class="form-control-static">
                                    {{ $invoice->outlet->outlet_name }} &nbsp;&nbsp;&nbsp; 
                                    <a href="/invoice-point/{{ $invoice->id_invoice_point }}/print"
                                        target="_blank">
                                        <i class="fa fa-print"></i>
                                    </a>
                                    &nbsp;
                                    <a href="/invoice-point/{{ $invoice->id_invoice_point }}/print-thermal"
                                        target="_blank">
                                        <i class="fa fa-mobile"></i>
                                    </a>
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Name</label>
                            <div class="col-md-10">
                                <p class="form-control-static">
                                    <a
                                        href="{{ route('customers.show', $invoice->customer_id) }}">{{ $invoice->customer->full_name }}</a>
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Phone Number</label>
                            <div class="col-md-10">
                                <p class="form-control-static">
                                    {{ $invoice->customer->phone }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Create Date</label>
                            <div class="col-md-10">
                                <p class="form-control-static">
                                    {{ $invoice->created_date }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Create By</label>
                            <div class="col-md-10">
                                <p class="form-control-static">
                                    {{ $invoice->staff->username }}
                                </p>
                            </div>
                        </div>
                        @if($invoice->void)
                            <div class="form-group">
                                <label class="col-md-2 control-label">Void Date</label>
                                <div class="col-md-10">
                                    <p class="form-control-static">
                                        {{ $invoice->void->created_date }}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Voided By</label>
                                <div class="col-md-10">
                                    <p class="form-control-static">
                                        {{ $invoice->void->staff->username }}
                                    </p>
                                </div>
                            </div>
                        @endif

                        <div class="form-horizontal">
                            <div id="table-hidden" class="box box-default">
                                <label for="package_code" class="col-md-2 control-label">Redeem Form</label>
                                <div class="col-md-10">
                                    <table class="table table-striped tabel-convert">
                                        <thead>
                                        <tr>
                                            <th width="20%">Name</th>
                                            <th>Unit Point</th>
                                            <th>Qty</th>
                                            <th>SubTotal</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($invoice->invoice_details as $item)
                                            <tr>
                                                <td>{{ $item->product_point->product_point_name ?? '-' }}</td>
                                                <td>{{ $item->current_point }}</td>
                                                <td>{{ $item->qty }}</td>
                                                <td>{{ $item->subtotal }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Total</label>
                            <div class="col-md-10">
                                <p class="form-control-static">
                                    {{ $invoice->total }}
                                </p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Remarks</label>
                            <div class="col-md-10">
                                <p class="form-control-static">
                                    {{ $invoice->remarks }}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer text-right">
                        @if(!$invoice->void)
                        <button class="btn btn-danger" onclick="voidInvoice({{$invoice->id_invoice_point}})" ><i
                                class="fa fa-trash-o"></i> Void</button>
                        @endif
                    </div>
                </div>
            </div>
            @include('customers.point.void')
@endsection

@section('script')
    <script>
        $('#date_invoice, #date_expired').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });

        $(function() {

            $('#modal-void form').validator().on('submit', function(e){
                if(!e.isDefaultPrevented()){
                    var id = $('#id').val();

                    $.ajax({
                        url : "/customers/"+id+"/void-redeem-invoice",
                        type : "POST",
                        data : $('#modal-void form').serialize(),
                        success : function(data){
                            $('#modal-void').modal('hide');
                            swal({
                                title: "Successfully Void Radeem Point",
                                icon: "success",
                            })
                                .then((data) => {
                                    window.location.href = "{{ route('customers.show', $invoice->customer_id) }}"
                                });
                        },
                        error : function(){
                            alert("Can not Save the Data!");
                        }
                    });
                    return false;
                }
            });
        })

        function voidInvoice(id) {
            $("#modal-void").modal('show');
            $('.modal-title-void').text('What is The Reason ?');
            $('input[name=_method]').val('PATCH');
            $('#modal-void form')[0].reset();
            $('#id').val(id);
        }
    </script>
@endsection

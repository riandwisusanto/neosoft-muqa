@extends('base')

@section('title')
Detail Patient
@endsection

@section('breadcrumb')
@parent
<li>Patient</li>
<li>Detail Patient</li>
@endsection

@php
$rank = rank($overview->id_customer);
@endphp

@section('content')
<style>
#modal-physical .form-horizontal .form-group {
    margin-left: 0;
    margin-right: 0;
}

.p-2 {
    padding: 8px;
}

.mt-3 {
    margin-top: 1rem;
}

.mt-5 {
    margin-top: 2rem;
}

.subtitle {
    font-size: 1.75rem;
}

.summary-value {
    font-size: 3.5rem;
    font-weight: bold;
    color: #454545;
    letter-spacing: 1px;
    margin-top: 8px;
}

.summary-divider {
    border-bottom: 1px solid #e4e4ed;
    margin: 1.5rem 0;
}

.nav-chart li a {
    margin-right: 1px;
}

.nav-tabs-custom .nav-tabs-button li a {
    background-color: #f4f4f4 !important;
    border: 1px solid #dddddd !important;
    border-radius: 0 !important;
    color: #363636 !important;
    padding: 6px 12px !important;
}

.nav-tabs-custom .nav-tabs-button {
    border-bottom: 0px !important;
    padding: 5px 10px !important;
}

.nav-tabs-custom .nav-tabs-button li a:hover,
.nav-tabs-custom .nav-tabs-button li.active a {
    background-color: #fff !important;
    border: 1px solid #d4d4dd !important;
    border-top: 1px solid #8a69ae !important;
    box-shadow: 0 2px 0 0 #8a69ae inset;
    color: #373737 !important;
}

.nav-tabs-custom .nav-tabs-button li.active {
    border-top-color: transparent !important;
}

.rotate {
    -webkit-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
    -webkit-transition: transform .5s ease;
    -o-transition: transform .5s ease;
    transition: transform .5s ease;
}

.rotate.left {
    -webkit-transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    -o-transform: rotate(90deg);
    transform: rotate(90deg);
    -webkit-transition: transform .5s ease;
    -o-transition: transform .5s ease;
    transition: transform .5s ease;
}
</style>
<div class="loader"></div>
<div class="row">
    <div class="col-xs-12">
        <!-- Custom Tabs -->
        <div class="box box-solid" style="margin-bottom: 0px; box-shadow: none; border-radius: 3px 3px 0px 0px;">
            <div class="box-header with-border">
                <span style="font-size:20px; font-weight: bold;">PATIENT DETAILS</span>
                <p style="font-size:15px; font-weight: bold;"><i>All patient’s information.</i></p>
            </div>
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="user-block row">
                            <!-- <img class="img-circle img-bordered-sm" src="{{ asset('dist/img/avatar2.png') }}" alt="user image"> -->
                            <div class="col-md-4">
                                <span class="username" style="margin-left: 5px">
                                    <a href="#">{{ ucwords($overview->full_name) }}</a>
                                </span>
                                <span class="description" style="margin-left: 5px">{{ $overview->induk_customer }}</span>
                                <span class="text-red" style="margin-left: 5px">
                                    <i class="fa fa-btc" aria-hidden="true"></i>
                                    <strong>{{ $overview->point }}</strong> Point
                                </span>
                            </div>
                            <div class="col-md-8">
                                <div style="margin-left: 5px">{{ $overview->nick_name }} &nbsp;&middot;&nbsp; {{ $overview->birth_date }} &nbsp;&middot;&nbsp; {{ $ageInYear }} tahun &nbsp;&middot;&nbsp; {{ $overview->gender == 'F' ? 'female' : 'male' }} </div>
                                <div style="margin-left: 5px">{{ $overview->address }}</div>
                                <div style="margin-left: 5px">{{ $overview->city->province ?? '' }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 text-right">
                        <div class="user-block pull-right">
                            <!-- <img class="pull-right img-circle img-bordered-sm" src="{{ asset('/img/medical.ico') }}" width="100px" alt="user image"> -->

                            <span class="username text-red" style="margin-right: 5px; margin-left: 0px;">
                                <a href="#" onclick="medicalAlert({{ $overview->id_customer }})">
                                Medical Alert
                                </a>
                                <hr style="margin: 5px 0px">
                            </span>

                            <span class="description" style="margin-right: 5px; margin-left: 0px;">
                                @foreach ($medical_alert as $alert)
                                <strong>{{ $alert->product->product_treatment_name ?? '-' }}</strong>
                                <i class="fa fa-exclamation-circle text-yellow" aria-hidden="true"></i><br>
                                @endforeach
                            </span>
                            <span class="description" style="margin-right: 5px; margin-left: 0px;">
                                <strong>{!! nl2br($overview->restriction) !!}</strong>
                                <i class="fa fa-exclamation-circle text-yellow" aria-hidden="true"></i><br>
                            </span>
                        </div>
                        <!--
                        <a class="btn btn-social btn-success" href="{{ route('invoice-balance.index') }}"><i class="fa fa-balance-scale"></i>Invoice Balance</a>
                        <a class="btn btn-social btn-info" href="{{ route('invoice-create.index') }}"><i class="fa fa-credit-card"></i>Create Invoice</a>
                        -->
                    </div>
                </div>
            </div>
        </div>
        <div class="nav-tabs-custom" style="border-radius: 0px 0px 3px 3px;">
            <ul class="nav nav-tabs nav-tabs-button" id="patientTab">
                @php
                    $active = "";

                    if (!isset($_GET['tab'])) {
                            $active = "active";
                    }
                @endphp
                @if (!isset($_GET['tab']) && isset($_GET['tab']) != 'overview')
                    <li><a href="#overview" class="first-tab" data-toggle="tab">Overview</a></li>
                @else
                <li class="{{ isset($_GET['tab']) && $_GET['tab'] == 'overview' ? 'active' : '' }}"><a href="#overview"  data-toggle="tab">Overview</a></li>
                @endif
                <li><a href="#information" data-toggle="tab">Information</a></li>
                <li><a href="#treatment" data-toggle="tab">Treatment</a></li>
                <li><a href="#invoice" data-toggle="tab">Invoice</a></li>
                <li><a href="#points" data-toggle="tab">Points</a></li>
                <li><a href="#photos" data-toggle="tab">Photos</a></li>
                <li class="{{ isset($_GET['tab']) && $_GET['tab'] == 'anamnesa' ? 'active' : '' }}"><a href="#anamnesa" data-toggle="tab">Anamnesa</a></li>
                <li><a href="#physical_examination" data-toggle="tab">Physical Examination</a></li>
                <li><a href="#consignment_form" data-toggle="tab">Consignment Form</a></li>
                <li><a href="#consignment_form_digital" data-toggle="tab">Consignment Form Digital</a></li>
                <li><a href="#data_lama" data-toggle="tab">Data Lama</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane {{ $active }} {{ isset($_GET['tab']) && $_GET['tab'] == 'overview' ? 'active' : '' }}" id="overview">
                    @include('customers.overview.index')
                </div>
                <div class="tab-pane" id="information">
                    @include('customers.information.index')
                </div>
                <div class="tab-pane" id="treatment">
                    @include('customers.treatment.index')
                </div>
                <div class="tab-pane" id="invoice">
                    @include('customers.invoice.index')
                </div>
                <div class="tab-pane" id="points">
                    @include('customers.point.index')
                </div>
                <div class="tab-pane" id="photos">
                    @include('customers.photo.index')
                </div>
                <div class="tab-pane {{ isset($_GET['tab']) && $_GET['tab'] == 'anamnesa' ? 'active' : '' }}" id="anamnesa">
                    @include('customers.anamnesa.index')
                </div>
                <div class="tab-pane" id="physical_examination">
                    @include('customers.physical.index')
                </div>
                <div class="tab-pane" id="consignment_form">
                    @include('customers.consignment.index')
                </div>
                <div class="tab-pane" id="consignment_form_digital">
                    @include('customers.consignment2.index')
                </div>
                <div class="tab-pane" id="data_lama">
                    @include('customers.data_lama.index')
                </div>
            </div>
            <!-- tab-content -->

        </div>
        <!-- nav-tabs-custom -->
    </div>
</div>
@include('customers.medicalAlert')
@include('customers.physical.form')
@include('customers.consignment.photo')
@include('customers.physical.photo')

@include('customers.consignment.form')
@include('customers.anamnesa.anamnesa')
@include('customers.consignment2.form')

@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
<script>
var save_method;
var canvas = document.querySelector('#drawablePhoto');
var signaturePad = new SignaturePad(canvas);

    $("a").click(function() {
        var link = this.getAttribute("href")
        if (link == "#") {
            event.preventDefault()
        }
    })

    $(document).ready(function(){
        $(".first-tab").parent().addClass("active");
        $(".loader").hide();
        $(".loader").addClass('hidden');
        $(".table-void-invoice").hide();
        $(".table-void-balance").hide();
        $(".table-invoice-expired").hide();

    });

    $('#date_anamnesa,#consignment_date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });

    $('#date_anamnesa').datepicker('setDate', 'today');
    $('#consignment_date').datepicker('setDate', 'today');
    $('#examination_date').datepicker('setDate', 'today');

function createAnamnesa() {
    document.location.href = '/new/#/anamnesas/{{ $overview->id_customer }}/create'
}

function showPictPhysical(id){

    $.ajax({
    url : "/"+id+"/showPictPhysical",
    type : "GET",
    dataType : "JSON",
    success : function(data){
        $('#modal-photos').modal('show');
        $('#titlePhysic').text('Detail Image');

        $('.tampil-images').html('<img src="/storage/'+data.image+'" class="img-responsive">');


    },
    error : function(){
        alert("Can not Show the Data!");
    }
    });
}

// function resizeCanvas() {

//      canvas.width = canvas.offsetWidth * ratio;
//      canvas.height = canvas.offsetHeight * ratio;
//      canvas.getContext("2d").scale(ratio, ratio);
//   }


function undoChange(e) {
    let data = signaturePad.toData();

    if (data) {
        data.pop(); // remove the last dot or line
        signaturePad.fromData(data);
    }
}

function createPreviewExamination(el) {
    let result = null;
    let file = el.files[0];
    let reader = new FileReader();

    reader.addEventListener('load', () => {


        let image = new Image();
        let imageRatio = 0;
        let modalWidth = $('#inner-drawable').width();

        image.addEventListener('load', () => {
            imageRatio = image.width / image.height;
            let modalHeight = modalWidth / imageRatio;
            canvas.width = modalWidth;
            canvas.height = modalHeight;
            console.log({
                imageWidth: image.width,
                imageHeight: image.height,
                modalHeight: modalHeight,
                modalWidth
            })

            // resizeCanvas();

            // if (window.outerWidth <= 1024) {
            //     canvas.getContext('2d').scale(2, 2);
            // }


            signaturePad.fromDataURL(reader.result, { ratio: 1 });

            //clearSignature
            document.getElementById('clear').addEventListener('click', function () {
                    signaturePad.clear();
                    signaturePad.fromDataURL(reader.result, { ratio: 1 });

                    });

            //undoSignature
            document.getElementById('undo').addEventListener('click', function () {
                var data = signaturePad.toData();
            if (data) {
                data.pop(); // remove the last dot or line
                signaturePad.fromData(data);
                signaturePad.fromDataURL(reader.result, { ratio: 1 });


            }
            });
        });

        image.src = reader.result;
    }, false);

    if (file) {
        reader.readAsDataURL(file);
    }
}

function showPict(id){

   $.ajax({
     url : "/"+id+"/showPict",
     type : "GET",
     dataType : "JSON",
     success : function(data){
        $('#modal-photo').modal('show');
        $('#titlep').text('Detail Image');

        $('.tampil-image').html('<img src="/storage/'+data.image+'" class="img-responsive">');
     },
     error : function(){
       alert("Can not Show the Data!");
     }
   });
}

    $('#modal-physical form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
         var id = $('#id').val();
         if(save_method == "add") url = "{{ route('customer.physical.storePhysical',$overview->id_customer) }}";
         else url = "{{ route('customer.anamnesa.updateAnamnesa',$overview->id_customer) }}";

         $('#photodata').val(signaturePad.toDataURL());
        //  new FormData($('#modal-physical form')[0])
         $.ajax({
            url : url,
            type : "POST",
            data : $('#modal-physical form').serialize(),
           success : function(data){
                console.log(data)
                $('#modal-physical').modal('hide');
                location.reload();

           },
           error : function(){
                swal({
                    text: 'Check file type, the file must be png, jpeg, svg !',
                    icon: 'error'
                })
            }
         });
         return false;
     }
   });

function addPhysical(){
   save_method = "add";
   $('input[name=_method]').val('POST');
   $("#modal-physical").modal({
        backdrop: 'static',
        keyboard: false,
        show: true
        });
    $('#modal-physical form')[0].reset();
    $('#titles').text('Add Physical');


}
    $('#modal-consignment form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
         var id = $('#id').val();
         if(save_method == "add") url = "{{ route('customer.consignment.storeConsignment',$overview->id_customer) }}";
         else url = "{{ route('customer.anamnesa.updateAnamnesa',$overview->id_customer) }}";

         $.ajax({
            url : url,
            type : "POST",
            data : new FormData($('#modal-consignment form')[0]),
            cache:false,
            contentType: false,
            processData: false,
           success : function(data){
             console.log(data)
              $('#modal-consignment').modal('hide');
              location.reload();

           },
           error : function(e){
            swal({
                    text: 'Check file type, the file must be png, jpeg, svg !',
                    icon: 'error'
            });
           }
         });
         return false;
     }
   });

function addConsignment(){
   save_method = "add";
   $('input[name=_method]').val('POST');
   $("#modal-consignment").modal({
        backdrop: 'static',
        keyboard: false,
        show: true
        });
    $('#modal-consignment form')[0].reset();
    $('#title').text('Add Consignment');


}

    $('#modal-anamnesa form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
         var id = $('#id').val();
         if(save_method == "add") url = "{{ route('customer.anamnesa.storeAnamnesa',$overview->id_customer) }}";
         else url = "{{ route('customer.anamnesa.updateAnamnesa',$overview->id_customer) }}";

         $.ajax({
            url : url,
            type : "POST",
            data : $('#modal-anamnesa form').serialize(),
           success : function(data){
             console.log(data)
              $('#modal-anamnesa').modal('hide');
              location.reload();
           },
           error : function(e){
             console.log(e)
             alert("Can not Save the Data!");
           }
         });
         return false;
     }
   });




function addAnamnesa(){
   save_method = "add";
   $('input[name=_method]').val('PUT');
   $("#modal-anamnesa").modal({
        backdrop: 'static',
        keyboard: false,
        show: true
        });
    $('#modal-anamnesa form')[0].reset();
    $('.modal-title').text('Add Anamnesa');


}

function editForm(id){
   save_method = "edit";
   $('input[name=_method]').val('POST');
   $('#modal-anamnesa form')[0].reset();
   $.ajax({
     url : "/"+id+"/editAnamnesa",
     type : "GET",
     dataType : "JSON",
     success : function(data){
       $('#modal-anamnesa').modal('show');
       $('.modal-title').text('Edit Anamnesa');

       $('#save_json').val(JSON.stringify(data.product));
       edit_product(data.product);

       $('#id_anamnesa').val(data.anamnesa.id_anamnesa);

       $('#anamnesa').val(data.anamnesa.anamnesa);
       $('#date_anamnesa').val(data.anamnesa.date_anamnesa);
       $('#suggestions').val(data.anamnesa.suggestion);

     },
     error : function(){
       alert("Can not Show the Data!");
     }
   });
}

    function edit_product(data) {
        Mustache.tags = ['{%', '%}'];
        $(function(){

    // Fungsi render digunakan untuk merender data dan template mustache ke dalam elemen yg sudah ditentukan
    function render()
    {
        var data = JSON.parse($('#save_json').val());

        // Memanggil fungsi getIndex sebelum ditampilkan
        getIndex(data);
        // Mengubah list array ke dalam bentuk data json lalu dimasukan ke dalam input type hidden
        $('#save_json').val(JSON.stringify(data));

        // Mendefinisikan template
        var tmpl = $('#add_item').html();
        // parse template ke dalam mustache template
        Mustache.parse(tmpl);
        // Merender dengan template dan data
        // variable 'item' adalah variable yang akan dilooping
        var html = Mustache.render(tmpl, { item : data });
        // mengisi html tbody dengan hasil renderan di atas
        $('#render').html(html);
        /*});*/

        // memanggil fungsi bind
        bind(data);
    }

    // PENTING. fungsi bind digunakan untuk memberikan event listener kepada elemen html setelah render selesai. Jika tidak dilakukan bind maka event pada elemen tidak akan berjalan.
    function bind(){
        // Memberikan event listener pada tombol pada saat tombol diklik
        $('select:not(.normal)').each(function () {
            $(this).select2({
                placeholder: "Please Select",
                width: 'resolve',
                dropdownParent: $(this).parent()
            });
        });
        var save_json = JSON.parse($('#save_json').val());
                        let grandtotal = [];
                        for (let i = 0; i < save_json.length; i++) {

                            grandtotal.push(Number(remove_format(save_json[i].price)));
                        }

                        let total = grandtotal.reduce((a,b) => a + b, 0)
                        $('#packprice').val(format_money(total));

                        chooseProduct();
                        chooseProductwithId();
                        $('.edit').on('click', chooseProductwithId);

        $('#AddItem').on('click', add);
        $('.delete').on('click', delete_item);
        $('.edit').on('click', edit);
        $('.cancel').on('click', canceledit);
        $('.save').on('click', saveedit);
    }

                    function chooseProduct() {
                        $('#type_id').change(function () {
                            $.ajax({
                                url: "/customers/"+$(this).val()+"/chooseProduct",
                                type: "GET",
                                dataType: "JSON",
                                success: function (data) {
                                    $('#price').val(data.price);
                                    $('#current_price').val(data.price);
                                    $('#qty').val(1);
                                },
                                error: function () {
                                    alert("Can not Delete the Data!");
                                }
                            });
                        });
                    }
                    function chooseProductwithId() {
                        var i = parseInt($(this).data('id'), 10);

                        $('#type_idx_'+i).change(function () {
                            $.ajax({
                                url: "/customers/"+$(this).val()+"/chooseProduct",
                                type: "GET",
                                dataType: "JSON",
                                success: function (data) {
                                    $('#price_'+i).val(data.price);
                                    $('#current_price_'+i).val(data.price);
                                    $('#qty_'+i).val(1);
                                },
                                error: function () {
                                    alert("Can not Delete the Data!");
                                }
                            });
                        });

                    }
    // Fungsi add untuk menambah item ke dalam list
    function add()
    {
        // Membaca nilai dari inputan
        var type_name = $('#type_id option:selected').text();


                        var type_id = $('#type_id').val();
                        var qty = $('#qty').val();
                        var price = $('#price').val();
                        var current_price = $('#current_price').val();

            //console.log(totalprice);

            if(therapy,qty){

                var input = {
                    'type_name' : type_name,
                            'type_id' : type_id,
                            'qty' : qty,
                            'price' : price,
                            'current_price' : current_price,
                };


                // Memasukan object ke dalam array
                data.push(input);

                // Merender ulang karena data sudah berubah

                render();
            } else {
                alert('Data is Empty');
            }

    }



    // fungsi edit untuk menampilkan form edit
    function edit()
    {
        // Mengambil id item yang akan dihapus
        var i = parseInt($(this).data('id'), 10);

        let type_id = $('#type_id_'+i).val();
        $('#type_idx_'+i).val(type_id).trigger('change');

        var row_data = $("#data_" + i);
        var row_form = $("#form_" + i);
        var input_data = $("#input_data");


        // menyembunyikan input form
        if(!input_data.hasClass('hidden')){
            input_data.addClass('hidden');
        }

        // menyembunyikan baris data
        if(!row_data.hasClass('hidden')){
            row_data.addClass('hidden');
        }

        // menampilkan baris form
        if(row_form.hasClass('hidden')){
            row_form.removeClass('hidden');
        }
    }

    // fungsi edit untuk menyembunyikan form edit
    function canceledit()
    {
        render();
    }

    function saveedit()
    {
        // Mengambil id item yang akan dihapus
        var i = parseInt($(this).data('id'), 10);

        // Membaca nilai dari inputan
        var type_id = $('#type_idx_' + i).val();
        var type_name = $('#type_idx_'+ i +' option:selected').text();
        var qty = $('#qty_' + i).val();
        var price = $('#price_' + i).val();
        var current_price = $('#current_price_' + i).val();

        // Menyimpan data pada list
        data[i].type_id = type_id;
        data[i].type_name = type_name;
        data[i].qty = qty;
        data[i].price = price;
        data[i].current_price = current_price;

        // Merender kembali karena data sudah berubah
        render();
    }

    // Fungsi delete_phone digunakan untuk menghapus elemen
    function delete_item(){
        // Mengambil id item yang akan dihapus
        var i = parseInt($(this).data('id'), 10);

        // menghapus list dari elemen array
        data.splice(i, 1);

        // Merender kembali karena data sudah berubah
        render();
    }

    // Fungsi getIndex digunakan untuk membuat penomoran dan id unik sebelum dirender
    function getIndex(data)
    {
        for (idx in data) {
            // setting _id digunakan untuk memudahkan dalam mengedit dan menghapus list, seperti id pada tabel dalam database.
            data[idx]['_id'] = idx;
            // setting no digunakan untuk penomoran
            data[idx]['no'] = parseInt(idx) + 1;
        }
    }

    // Memanggil fungsi render pada saat halaman pertama kali dimuat
    render();
    });

    }
    function changeCount() {
        let price = $('#current_price').val();
        let qty = $('#qty').val();
        let subtotal = qty * remove_format(price);

        $('#price').val(format_money(subtotal));

    }

    function changeCountwithId(id) {
        let price = $('#current_price_'+id).val();
        let qty = $('#qty_'+id).val();
        let subtotal = qty * remove_format(price);

        $('#price_'+id).val(format_money(subtotal));
    }

    function remove_format(money) {
        return money.toString().replace(/[^0-9]/g,'');
    }

    function format_money(money){
        return money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }
    $('#close-modal').on('click', function () {
        resetPointModal();
        $('#modal-anamnesa').modal('hide');
    });

    $("#slide-toggle-anamnesa").click(function(){
        $(".table-anamnesa-history").slideToggle("slow");
        $(this).children().toggleClass("left");
    });

    $("#slide-toggle-redeem-points").click(function(){
        $(".table-redeem-points").slideToggle("slow");
        $(this).children().toggleClass("left");
    });

    $("#slide-toggle-void-points").click(function(){
        $(".table-void-points").slideToggle("slow");
        $(this).children().toggleClass("left");
    });

    $("#slide-toggle-physical-examination").click(function(){
        $(".table-physical-examination").slideToggle("slow");
        $(this).children().toggleClass("left");
    });

    $("#slide-toggle-consignment-history").click(function(){
        $(".table-consignment-history").slideToggle("slow");
        $(this).children().toggleClass("left");
    });

    $("#slide-toggle-invoice-active-void").click(function(){
        $(".table-invoice-active-void").slideToggle("slow");
        $(this).children().toggleClass("left");
    });
    $("#slide-toggle-outstanding-invoice").click(function(){
        $(".table-outstanding-invoice").slideToggle("slow");
        $(this).children().toggleClass("left");
    });

    $("#slide-toggle-invoice-balance-void").click(function(){
        $(".table-invoice-balance-void").slideToggle("slow");
        $(this).children().toggleClass("left");
    });

    $("#slide-toggle-consignment2-history").click(function () {
        $(".table-consignment2-history").slideToggle("slow");
        $(this).children().toggleClass("left");
    });

    $("#switch-invoice-active-void").on('change',function(){
        if ($(this).is(":checked")) {
            $(".table-active-invoice").hide();
            $(".table-void-invoice").show();
        } else {
            $(".table-void-invoice").hide();
            $(".table-active-invoice").show();
        }
    });

    $("#switch-invoice-balance-void").on('change',function(){
        if ($(this).is(":checked")) {
            $(".table-invoice-balance").hide();
            $(".table-void-balance").show();
        } else {
            $(".table-void-balance").hide();
            $(".table-invoice-balance").show();
        }
    });

    $("#switch-invoice-outstanding-invoice").on('change',function(){
        if ($(this).is(":checked")) {
            $(".table-invoice-outstanding").hide();
            $(".table-invoice-expired").show();
        } else {
            $(".table-invoice-expired").hide();
            $(".table-invoice-outstanding").show();
        }
    });


  // function reset(){
  //   $('#before, #after').css('display', 'none');
  //   $('#before_option, #after_option').html('')
  //   $('#carousel-before, #carousel-after').css('display', 'none');
  // }
  $('#myModal').on('show.bs.modal', function (e) {
      var button = $(e.relatedTarget);
      var img = button.data('img');
      $('#imgDetail').attr('src', img);
  });

  /*
  $('#customer_id').change(function(){
    if ($("#tab_1-1").hasClass("active")) {
      var thisval = $(this).val();
      $('#customer_id2').val(thisval).trigger('change');
    }
  });
  $('#customer_id2').change(function(){
    if ($("#tab_2-2").hasClass("active")) {
      var thisval = $(this).val();
      $('#customer_id').val(thisval).trigger('change');
    }
  });
  */

  $('#treatment_option').change(function(){
    if ($("#tab_1-1").hasClass("active")) {
      var thisval = $(this).val();
      $('#treatment_options').val(thisval).trigger('change');
    }
  });
  $('#treatment_options').change(function(){
    if ($("#tab_2-2").hasClass("active")) {
      var thisval = $(this).val();
      $('#treatment_option').val(thisval).trigger('change');
    }
  });

  $(document).ready(function () {

      var customer_id = $('#customer_id, #customer_id2').val();
      $.ajax({
        type     : 'post',
        url      : '{{ route('dr-review.sandingkan') }}',
        data     : {
          id: customer_id,
          _token: '{{ csrf_token() }}'
        },
        dataType : 'json',
        success  : function (res) {
          var treatment = '<option selected disabled>Choose Treatment</option>';
          $.each(res.data, function (i, v) {
              treatment += `<option  value="${v.id_usage_detail}">(${v.usage_code}) ${v.product_treatment_name}</option>`;
          });

          $('#treatment_option').html(treatment);
          $('#treatment_options').html(treatment);
          $('#before_img').html('');
          $('#after_img').html('');

        }
      });

    $('.treatment').change(function(e){
      var id = $(this).val();
      $.ajax({
        type     : 'post',
        url      : '{{ route('dr-review.sandingkan.treatment') }}',
        data     : {
          id: id,
          _token: '{{ csrf_token() }}'
        },
        dataType : 'json',
        success  : function (res) {
          var treatment = '<option selected disabled>Choose Treatment Date</option>';

          $.each(res.data, function (i, v) {
              treatment += `<option  value="${v.id}">${v.date_in}</option>`;
          });

          $('#before_option').html(treatment);
          $('#after_option').html(treatment);

        }
      });
    });

    $('#before_option').change(function(e){
      var id = $(this).val();
      $.ajax({
        type     : 'post',
        url      : '{{ route('dr-review.sandingkan.photo') }}',
        data     : {
          id: id,
          _token: '{{ csrf_token() }}'
        },
        dataType : 'json',
        success  : function (res) {
          if(res.success){
            var item = '';

            $.each(res.data, function (i, v) {

              item += `
              <a href="!#" data-toggle="modal" data-target="#myModal" data-img="${v}">
              <img src="${v}" alt="..." class="img-thumbnail img-responsive" width="100%" style="margin-bottom:15px;">
              </a>
              `;

            });
            $('#before_img').html(item);
            $('#before_img').css('display','block');

          }
        }
      });
    });

    $('#after_option').change(function(e){
      var id = $(this).val();
      $.ajax({
        type     : 'post',
        url      : '{{ route('dr-review.sandingkan.photo') }}',
        data     : {
          id: id,
          _token: '{{ csrf_token() }}'
        },
        dataType : 'json',
        success  : function (res) {
          if(res.success){
            var item = '';
            $.each(res.data, function (i, v) {
              item += `
              <a href="!#" data-toggle="modal" data-target="#myModal" data-img="${v}">
                <img src="${v}" alt="..." class="img-thumbnail img-responsive" width="100%" style="margin-bottom:15px;">
              </a>
              `;

            });
            $('#after_img').html(item);
            $('#after_img').css('display','block');


          }
        }
      });
    });
  });

    $('#modal-medical form').validator().on('submit', function(e){
        if(!e.isDefaultPrevented()){
            $.ajax({
                url : "{{ route('medical-alert.store') }}",
                type : "POST",
                data : $('#modal-medical form').serialize(),
            success : function(data){
                $('#modal-medical').modal('hide');
                //   table.ajax.reload();
                location.reload();
                return false;
            },
            error : function(e){
                alert("Can not Save the Data!");
            }
            });
            return false;
        }
    });

    function addConsignmentDigital() {
        save_method = "add";
        $('input[name=_method]').val('POST');
        $("#modal-consignment-digital").modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
        // $('#modal-consignment-digital form')[0].reset();
        $('#title').text('Add Consignment Digital');
    }

    function consignDelete(id,opt) {
        let url = ''
        if (opt === 'medicalConsign') {
            url = `/customers/delMedicalConsign/${id}`
        }

        if (url) {
            document.querySelectorAll('.btn-del-consign').forEach(e => e.setAttribute('disabled','disabled'))
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    _token: '{{ csrf_token() }}'
                },
                success: function (data) {
                    window.location.reload()
                },
                error: function () {
                    alert("Can not Show the Data!");
                }
            });
        }
    }
</script>
<script src="{{ asset('js/medicalAlert.js') }}"></script>
<script src="{{ asset('js/anamnesa.js') }}"></script>

@stack('script')
@endsection

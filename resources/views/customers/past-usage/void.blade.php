<div class="modal d-relative" id="modal-usage" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            @include('customers.past-usage.loading')
            <form class="form-horizontal" data-toggle="validator" method="post">
                {{ csrf_field() }} {{ method_field('POST') }}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times fa-lg"></i>
                    </button>
                    <h3 class="modal-title-usage"></h3>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" id="id">
                    @foreach ($usage->usage_detail as $item)
                        <input type="hidden" name="inv_package[]" value="{{ $item->invoice_package_id }}" >
                        <input type="hidden" name="used[]" value="{{ $item->used }}">
                    @endforeach
                    <div class="form-group">
                        <label for="fullname" class="col-sm-3 control-label">Remarks</label>
                        <div class="col-sm-9">
                            <textarea name="remarks" id="remarks" cols="50" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save
                        </button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                                class="fa fa-arrow-circle-left"></i> Cancel</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
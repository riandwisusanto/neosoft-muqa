@extends('base')

@section('title')
Past Usage
@endsection

@section('style')
    <style>
        .emoji {
            width: 30px;
            height: 30px;
            border-radius: 100%;
            overflow: hidden;
        }
    </style>
@endsection

@section('breadcrumb')
@parent
<li>Past Usage</li>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h4>{{ $usage->usage_code }}</h4>
            </div>
            <div class="box-body ">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Usage Number</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                <a href="{{ route('usage-treatment.print', $usage->id_usage) }}"
                                    target="_blank">{{ $usage->usage_code }}</a>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Customer</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                {{ $usage->customer->induk_customer }}
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Full Name</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                <a
                                    href="{{ route('customers.show', $usage->customer_id) }}">{{ $usage->customer->full_name }}</a>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Phone Number</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                {{ $usage->customer->phone }}
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Outlet</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                {{ $usage->outlet->outlet_name }}
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Usage Date</label>
                        <div class="col-md-10">
                            {{ $usage->usage_date }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Create Date</label>
                        <div class="col-md-10">
                            {{ $usage->created_at }}
                        </div>
                    </div>

                    <div class="form-horizontal">
                        <div id="table-hidden" class="box box-default table-responsive no-padding">
                            <label for="package_code" class="col-md-2 control-label">Item</label>
                            <div class="col-md-10">
                                <table class="table table-striped tabel-convert">
                                    <thead>
                                        <tr>
                                            <th width="20%">Invoice</th>
                                            <th>Code</th>
                                            <th width="20%">Name</th>
                                            <th>Used</th>
                                            <th>Balance</th>
                                            <th>Therapist</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($usage->usage_detail as $item)
                                        <tr>
                                            @php
                                            $therapist = '';
                                            foreach ($item->usage_therapist as $getTherapist){
                                            $therapist .= "<li>".$getTherapist->therapist->therapist_name."</li>";
                                            }

                                            @endphp

                                            <td>{{ $item->inv_code }}</td>
                                            <td>{{ $item->product_treatment->product_treatment_code }}</td>
                                            <td>{{ $item->product_treatment->product_treatment_name }}</td>
                                            <td>{{ $item->used }}</td>
                                            <td>{{ $item->invoice_package->qty }}</td>
                                            <td>{!! $therapist !!}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Customer Satisfaction</label>
                        <div class="col-md-10">
                            <div class="form-control-static">
                                @if ($usage->rating == null)
                                    -
                                @elseif ($usage->rating == 1)
                                    <div class="emoji">
                                        <img src="{{ asset('img/rating-emoji.png') }}" width="150px" height="30px" title="Tidak Puas">
                                    </div>
                                @elseif ($usage->rating == 2)
                                    <div class="emoji">
                                        <img src="{{ asset('img/rating-emoji.png') }}" width="150px" height="30px" title="Kurang Puas" style="margin-left: -30px;">
                                    </div>
                                @elseif ($usage->rating == 3)
                                    <div class="emoji">
                                        <img src="{{ asset('img/rating-emoji.png') }}" width="150px" height="30px" title="Biasa Saja" style="margin-left: -60px;">
                                    </div>
                                @elseif ($usage->rating == 4)
                                    <div class="emoji">
                                        <img src="{{ asset('img/rating-emoji.png') }}" width="150px" height="30px" title="Cukup Puas" style="margin-left: -90px;">
                                    </div>
                                @elseif ($usage->rating == 5)
                                    <div class="emoji">
                                        <img src="{{ asset('img/rating-emoji.png') }}" width="150px" height="30px" title="Puas" style="margin-left: -120px;">
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Remarks</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                {{ $usage->remarks }}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="box-footer text-right">
                    <button class="btn btn-success" onclick="editVoid({{ $usage->id_usage }})"><i class="fa fa-edit"
                            aria-hidden="true"></i> Edit</button>
                    @if (array_intersect(['ADMINISTRATOR','OUTLET_SUPERVISOR','BRANCH_MANAGER',"SUPER_USER"], json_decode(Auth::user()->level)) || (array_intersect(['OUTLET_SUPERVISOR', 'BRANCH_MANAGER'], json_decode(Auth::user()->level)) && $usage->usage_date == date('d/m/Y')))
                    <button class="btn btn-danger" onclick="voidUsage({{ $usage->id_usage }})"><i
                            class="fa fa-trash-o"></i> Void</button>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="box box-solid">
    <div class="box-header with-border">
        <strong>Log</strong>
    </div>
    <div class="box-body">
        <div class="col-md-12">
            <label for="log_usage" class="control-label">Edited Usage Records</label>
            <table class="table table-striped tabel-convert">
                <thead>
                    <tr>
                        <th width="20%">Usage Number</th>
                        <th width="20%">Staff</th>
                        <th>Edited Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($log_usage as $logs)
                    <tr>
                        <td>{{ $logs->usage->usage_code }}</td>
                        <td>{{ $logs->user->username }}</td>
                        <td>{{ $logs->created_at }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@include('customers.past-usage.edit')
@include('customers.past-usage.void')
@endsection

@section('script')
<script>
    $('#date_usage').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });

    $(function () {
        $('#modal-usage form').validator().on('submit', function(e){
            if(!e.isDefaultPrevented()){

                $.ajax({
                    url : "{{ route('usage-treatment.void', $usage->id_usage) }}",
                    type : "POST",
                    data : $('#modal-usage form').serialize(),

                beforeSend: function() {
                    $('.btn-save').prop('disabled', true);
                    $('#loading').css('display','grid');
                },

                success : function(data){
                    $('#modal-usage').modal('hide');
                    $('.btn-save').prop('disabled', false);
                    $('#loading').css('display','none');
                    swal({
                        title: "Successfully Void Usage",
                        icon: "success",
                    })
                    .then((go) => {
                        window.location.href = "{{ route('customers.show', $usage->customer_id) }}";
                    });
                },
                error : function(){
                    alert("Can not Save the Data!");
                    $('.btn-save').prop('disabled', false);
                    $('#loading').css('display','none');
                }
                });
                return false;
            }
        });

        $('.form-edit').validator().on('submit', function(e){
            if(!e.isDefaultPrevented()){

                $.ajax({
                    url : "{{ route('usage-treatment.update', $usage->id_usage) }}",
                    type : "PATCH",
                    data : $('.form-edit').serialize(),
                success : function(data){
                    $('#modal-edit').modal('hide');
                    swal({
                        title: "Successfully Edit Usage",
                        icon: "success",
                    })
                    .then((go) => {
                        window.location.href = "{{ route('customers.show', $usage->customer_id) }}";
                    });
                },
                error : function(){
                    alert("Can not Save the Data!");
                }
                });
                return false;
            }
        });
    });

    function voidUsage(id) {
        $("#modal-usage").modal('show');
        $('.modal-title-usage').text('What is The Reason ?');
        $('input[name=_method]').val('PATCH');
        $('#modal-usage form')[0].reset();
    }

    function editVoid(id) {
        $("#modal-edit").modal('show');
        $('.modal-title-usage').text('{{ $usage->usage_code }}');
        $('input[name=_method]').val('PATCH');
        $('#modal-edit form')[0].reset();
        $('#id').val(id);

        $('#date_usage, #date_expired').datepicker({
            format: 'dd MM yyyy',
            autoclose: true,
            todayHighlight: true
        });

        $('#date_usage, #date_expired').datepicker('setDate', 'today')
    }
</script>
@endsection

<div class="box box-solid">
        <div class="box-header with-border">
            <span style="font-size:20px;">Past Usage</span>
            <p style="font-size:15px;"><i>Riwayat perawatan pasien.</i></p>
        </div>
        <div class="box-body table-responsive no-padding">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Usage Code</th>
                        <th>Usage Date</th>
                        <th>Created By</th>
                        <th>Outlet</th>
                        <th>Item Code</th>
                        <th>Item Name</th>
                        <th>Invoice Num</th>
                        <th>Therapist</th>
                        <th>Remarks</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($usage as $item)
                    <tr>
                        <td><a href="{{ route('usage-treatment.print', $item->id_usage) }}" target="_blank">{{ $item->usage_code }}</a></td>
                        <td>{{ $item->usage_date }}</td>
                        <td>{{ $item->user->username }}</td>
                        <td>{{ $item->outlet->outlet_name }}</td>
                        @php
                            $product = '';
                            $invoice = '';
                            $therapist = '';
                            $treatment = '';

                            foreach($item->usage_detail as $data){
                                $invoice .= '&#10004;'.$data->inv_code."</br>";
                                $product .= '&#10004;'.$data->product_treatment->product_treatment_code."</br>";
                                $treatment .= '&#10004;'.$data->product_treatment->product_treatment_name."</br>";

                                foreach ($data->usage_therapist as $getTherapist){
                                    $therapist .= '&#10004;'.$getTherapist->therapist->therapist_name."</br>";
                                }
                            }
                        @endphp

                        <td>{!! $product !!}</td>
                        <td>{!! $treatment !!}</td>

                        <td>{!! $invoice !!}</td>
                        <td>{!! $therapist !!}</td>
                        <td>{{ $item->remarks }}</td>
                        <td><a href="{{ route('usage-treatment.show', $item->id_usage) }}" class='btn btn-primary btn-sm'><i class='fa fa-eye'></i></a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

<div class="modal" id="modal-edit" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">
                        &times;</span> </button>
                <h3 class="modal-title-usage"></h3>
            </div>
            <div class="modal-body">
                <form class="form-horizontal form-edit" data-toggle="validator" method="post">
                    {{ csrf_field() }} {{ method_field('PATCH') }}
                    <input type="hidden" name="id" id="id">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Usage Number</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    {{ $usage->usage_code }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Customer</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    {{ $usage->customer->induk_customer }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Full Name</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    {{ $usage->customer->full_name }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Phone</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    {{ $usage->customer->phone }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Customer</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    {{ $usage->customer->birth_place }}, {{ $usage->customer->birth_date }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Outlet</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    {{ $usage->outlet->outlet_name }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Usage Date</label>
                            <div class="col-md-6">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="date_usage" name="date_usage"
                                        name="date" required>
                                </div>
                            </div>
                        </div>

                        <div class="form-horizontal">
                            <div id="table-hidden" class="box box-default">
                                <div class="col-md-12">
                                    <table class="table table-striped tabel-convert">
                                        <thead>
                                            <tr>
                                                <th width="20%">SKU</th>
                                                <th width="20%">Name</th>
                                                <th>Usage</th>
                                                <th>Balance</th>
                                                <th width="50px">Point</th>
                                                <th>Therapis</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($usage->usage_detail as $list)
                                            <input type="hidden" name="product_id[]" value="{{ $list->product_id }}">
                                            <input type="hidden" name="inv_package_id[]"
                                                value="{{ $list->invoice_package_id }}">
                                            <input type="hidden" name="id_usage[]" value="{{ $list->id_usage_detail }}">
                                            <tr>
                                                <td>{{ $list->product_treatment->product_treatment_code }}</td>
                                                <td>{{ $list->product_treatment->product_treatment_name }}</td>
                                                <td>{{ $list->qty }}</td>
                                                <td>{{ $list->invoice_package->qty }}</td>
                                                <td>

                                                    <select id="point" name="point[]" required>
                                                        <option value="0">0</option>
                                                        @foreach ($list->product_treatment->product_treatment_points as
                                                        $point)
                                                        @php
                                                        $select = '';
                                                        if ($list->point == $point->point){
                                                        $select = 'selected';
                                                        }

                                                        @endphp
                                                        <option value="{{ $point->point }}" {{ $select }}>
                                                            {{ $point->point }}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td>

                                                    <select id="therapist"
                                                        name="therapist_{{ $list->invoice_package_id }}[]"
                                                        class="form-control select2 select2-hidden-accessible"
                                                        multiple="" style="width: 100%;" tabindex="-1"
                                                        aria-hidden="true" required>
                                                        @foreach ($therapist_all as $item)
                                                        @php
                                                        foreach($list->usage_therapist as $data){
                                                        // echo $item;
                                                        if ($data->therapist_id == $item->id_therapist) {
                                                        echo "<option value=".$item->id_therapist." selected>
                                                            ".$item->therapist_name."</option>";
                                                        }else {
                                                        echo "<option value=".$item->id_therapist.">
                                                            ".$item->therapist_name."</option>";
                                                        }
                                                        }
                                                        @endphp
                                                        @endforeach
                                                    </select>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Remarks</label>
                            <div class="col-md-4">
                                <p class="form-control-static">
                                    <textarea name="remarks" id="" cols="50" rows="5">{{ $usage->remarks }}</textarea>
                                </p>
                            </div>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary simpan"><i class="fa fa-floppy-o"></i> Save
                        </button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                                class="fa fa-arrow-circle-left"></i>
                            Cancel</button>
                    </div>
                </form>
            </div>


        </div>
    </div>
</div>
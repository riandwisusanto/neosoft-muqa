<div class="modal" id="modal-anamnesa" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
                <form class="form-horizontal" data-toggle="validator" method="post">
                {{ csrf_field() }} {{ method_field('POST') }}
                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times fa-lg"></i>
                    </button>
                    <h3 class="modal-title"></h3>
                </div>
                <div class="modal-body" >
                    <input type="hidden" id="id" name="id_customer">
                    <input type="hidden" id="id_anamnesa" name="id_anamnesa">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Anamnesa Date</label>
                                                <div class="input-group date">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control pull-right" id="date_anamnesa"
                                                        name="date_anamnesa" value="<?= date('d/m/Y') ?>" required>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="summary_id" class="control-label">Anamnesa</label>
                                            <textarea class="form-control" id="anamnesa" name="anamnesas"  required></textarea>

                                        </div>
                                        <div class="form-group">
                                            <label for="summary_id" class="control-label">Suggestion</label>
                                            <textarea class="form-control" id="suggestions" name="suggestion"></textarea>
                                            </div>
                                        </div>
                                      
                                </div>     
                            </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="render"></div>
                            @include('customers.anamnesa.detail_anamnesa')
                            <input type="hidden" name="save_json" id="save_json" value="[]" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 ">
                                <div class="col-md-4 col-md-offset-8">
                                    <label class="control-label">Total</label>
                                    <input type="text" disabled class="form-control" id="packprice" name="packprice">
                                </div>
                        </div>
                    </div>
                      <br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal" id="close-modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
                            </div>
                        </div>
                    </div>
                 
                </div>
            <!-- </form> -->
        </div>
     </div>
</div>

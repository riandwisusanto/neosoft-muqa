<div class="row">
    <div class="col-xs-12">
        <span style="font-size:20px; font-weight: bold;">ANAMNESA</span>
    </div>
    @if (!array_intersect(["THERAPIST"], json_decode(Auth::user()->level)))
        <div class="col-xs-12" style="margin-top: 5px;">
            <button onclick="createAnamnesa()" class="btn btn-success" >Create New Anamnesa</button>
            <br><br>
        </div>
    @endif
</div>
<div class="box box-solid" style="box-shadow: none;">
    <div class="box-header">
        <div class="row">
            <div class="col-xs-12">
                <span style="font-size: 17px; font-weight: bold; cursor: pointer;" id="slide-toggle-anamnesa">Anamnesa History <i class="fa fa-angle-down rotate" style="margin-left: 15px;"></i></span>
            </div>
        </div>
    </div>
    <div class="box-body table-responsive no-padding">
        <div class="table-anamnesa-history">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr class="text-capitalize">
                        <th>Created by</th>
                        <th>Anamnesa Date</th>
                        <th>Anamnesa</th>
                        <th>Suggestion</th>
                        <th>Complain</th>
                        <th>Diagnosa</th>
                        <th>Product/Treatment</th>
                        <th>Qty</th>
                        <th>Price</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                    @php
                    $dataDiagnosa = [
                        ["id" => 1, "name" => "ACNE", "isChecked" => false ],
                        ["id" => 2, "name" => "BAU BADAN", "isChecked" => false ],
                        ["id" => 3, "name" => "CR DOKTER", "isChecked" => false ],
                        ["id" => 4, "name" => "HIPERHIDROSIS", "isChecked" => false ],
                        ["id" => 5, "name" => "HIPERPIGMENTASI", "isChecked" => false ],
                        ["id" => 6, "name" => "HIPOPIGMENTASI", "isChecked" => false ],
                        ["id" => 7, "name" => "HIRSUTISME", "isChecked" => false ],
                        ["id" => 8, "name" => "HS", "isChecked" => false ],
                        ["id" => 9, "name" => "IRITASI", "isChecked" => false ],
                        ["id" => 10, "name" => "KELOID", "isChecked" => false ],
                        ["id" => 11, "name" => "KERATORIS SEBOROIK", "isChecked" => false ],
                        ["id" => 12, "name" => "KERING", "isChecked" => false ],
                        ["id" => 13, "name" => "KERONTOKAN RAMBUT", "isChecked" => false ],
                        ["id" => 14, "name" => "KETOMBE", "isChecked" => false ],
                        ["id" => 15, "name" => "KUSAM", "isChecked" => false ],
                        ["id" => 16, "name" => "MENOLAK TAPER", "isChecked" => false ],
                        ["id" => 17, "name" => "MERAH", "isChecked" => false ],
                        ["id" => 18, "name" => "MERINTIS", "isChecked" => false ],
                        ["id" => 19, "name" => "NEVUS", "isChecked" => false ],
                        ["id" => 20, "name" => "OBESITAS", "isChecked" => false ],
                        ["id" => 21, "name" => "OCHORONOSIS", "isChecked" => false ],
                        ["id" => 22, "name" => "PARUT ACNE", "isChecked" => false ],
                        ["id" => 23, "name" => "PENUAAN KULIT", "isChecked" => false ],
                        ["id" => 24, "name" => "PX TIDAK DATANG", "isChecked" => false ],
                        ["id" => 25, "name" => "SELULIT", "isChecked" => false ],
                        ["id" => 26, "name" => "STRETCH MARK", "isChecked" => false ],
                        ["id" => 27, "name" => "TAPER HQ", "isChecked" => false ],
                        ["id" => 28, "name" => "TAPER ST", "isChecked" => false ],
                        ["id" => 29, "name" => "TATTO", "isChecked" => false ],
                        ["id" => 30, "name" => "TIDAK ADA KELUHAN", "isChecked" => false ],
                        ["id" => 31, "name" => "UPSELLING", "isChecked" => false ],
                        ["id" => 32, "name" => "VERUKA KUTIL", "isChecked" => false ],
                        ["id" => 33, "name" => "ALERGI", "isChecked" => false ],
                        ["id" => 34, "name" => "FLEK", "isChecked" => false ],
                        ["id" => 35, "name" => "GATAL", "isChecked" => false ],
                        ["id" => 36, "name" => "KANTUNG MATA", "isChecked" => false ],
                        ["id" => 37, "name" => "KERUTAN", "isChecked" => false ],
                        ["id" => 38, "name" => "KOMEDO", "isChecked" => false ],
                        ["id" => 39, "name" => "MENGELUPAS", "isChecked" => false ],
                        ["id" => 40, "name" => "NORMAL", "isChecked" => false ],
                        ["id" => 41, "name" => "OLILY", "isChecked" => false ],
                        ["id" => 42, "name" => "PEMULA", "isChecked" => false ],
                        ["id" => 43, "name" => "PERIH", "isChecked" => false ],
                        ["id" => 44, "name" => "POST DOKTER", "isChecked" => false ],
                        ["id" => 45, "name" => "POST TREATMENT", "isChecked" => false ],
                        ["id" => 46, "name" => "PX BARU", "isChecked" => false ],
                        ["id" => 47, "name" => "SCAR", "isChecked" => false ],
                        ["id" => 48, "name" => "SENSITIF", "isChecked" => false ],
                        ["id" => 49, "name" => "TELANG EKSTASIS", "isChecked" => false ],
                        ["id" => 50, "name" => "PORI BESAR", "isChecked" => false],
                        ["id" => 51, "name" => "LEUKORDENA", "isChecked" => false],
                        ["id" => 52, "name" => "CERAH", "isChecked" => false],
                        ["id" => 53, "name" => "MATA PANDA", "isChecked" => false],
                        ["id" => 54, "name" => "MILIA", "isChecked" => false],
                        ["id" => 55, "name" => "SKINTAG", "isChecked" => false],
                        ["id" => 56, "name" => "MELASMA", "isChecked" => false],
                        ["id" => 57, "name" => "OILY SKIN", "isChecked" => false],
                        ["id" => 58, "name" => "SLIMMING PROGRAM", "isChecked" => false],
                        ["id" => 59, "name" => "PASIEN BARU", "isChecked" => false],
                        ["id" => 60, "name" => "LEUKODERMA", "isChecked" => false],
                        ["id" => 61, "name" => "KOMPLAIN", "isChecked" => false],
                        ["id" => 62, "name" => "CHUBBY CHEEK", "isChecked" => false],
                        ["id" => 63, "name" => "DOUBLE CHIN", "isChecked" => false],



                    ];

                    @endphp


                    @foreach ($anamnesa as $item)
                    @php
                    $product ='';
                    $qty ='';
                    $price ='';
                    $diagnosa = '';

                    foreach($item->anamnesa_detail as $detail) {
                        @$product .= '&middot;'.$detail->product_treatment->product_treatment_name.'<br><i class="small">'.$detail->notes.'</i><br><br>';
                        @$qty .= "(".$detail->qty.")"."<br>";
                        @$price .=number_format($detail->total / $detail->qty)."<br>";
                    }

                    foreach ($item->diagnosas as $detail) {
                        $diagnosaIndex = array_search($detail->diagnosa_id, array_column($dataDiagnosa, 'id'));
                        $diagnosa .= "&middot;".$dataDiagnosa[$diagnosaIndex]['name']."<br>";
                    }

                    @endphp
                    <tr>
                        <td>{{ isset($item->creator) ? $item->creator->name : '-' }}</td>
                        <td>{{ $item->date_anamnesa }}</td>
                        <td>{{ $item->anamnesa }}</td>
                        <td>{{ $item->suggestion }}</td>
                        <td>{{ $item->complain ?? '-' }}</td>
                        <td>{!! $diagnosa !!}</td>
                        <td>{!! @$product !!}</td>
                        <td>{!! @$qty !!}</td>
                        <td>{!! @$price !!}</td>
                        <td>
                            @php
                                $anamnesaDetailId = $item->anamnesa_detail->pluck('id_anamnesa_detail')->all();
                                $inv_pkg_anam = App\Models\InvoicePackage::whereIn('anamnesa_detail_id', $anamnesaDetailId)->where('used','>', 0)->get();
                                $inv_pkg = App\Models\InvoicePackage::whereIn('anamnesa_detail_id', $anamnesaDetailId)->get();
                                if ( count($inv_pkg_anam) > 0 || count($inv_pkg) > 0) {
                                    $statusUsageAndInvoice = true;
                                } else {
                                    $statusUsageAndInvoice = false;
                                }
                                $datePlusOne = \Carbon\Carbon::createFromFormat('d/m/Y',$item->date_anamnesa)->addDay()->format('d/m/Y');
                            @endphp
                            @if (Auth::user()->id == $item->created && (date('d/m/Y') == $item->date_anamnesa || date('d/m/Y') == $datePlusOne))
                            <form action="{{route('customers.anamnesa.destroy',$item->id_anamnesa)}}" method="post" onsubmit="confirmDelete(event)">
                                {{ csrf_field() }} @csrf @method('patch')
                                @if ( !$statusUsageAndInvoice )
                                        <button type="submit" class="btn btn-danger btn-sm">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        @endif
                                        <a href="{{ url('/') }}/new/#/anamnesas/{{ $item->id_anamnesa }}/edit"
                                            class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>

                                        <a href="{{ url('/') }}/new/#/anamnesas/{{ $item->id_anamnesa }}"
                                            class="btn btn-warning btn-sm"><i class="fa fa-credit-card"></i></a>
                                    </form>
                            @else
                            <a href="{{ url('/') }}/new/#/anamnesas/{{ $item->id_anamnesa }}" class="btn btn-warning btn-sm" ><i
                                class="fa fa-credit-card"></i></a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    function confirmDelete(e) {
        let result = confirm('anda yakin akan menghapus anamnesa ini?')

        if ( ! result) {
            e.preventDefault();

        } else {
            return true
        }
    }
</script>

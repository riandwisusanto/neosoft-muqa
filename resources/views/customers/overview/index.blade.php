<div class="row">
    <div class="col-xs-12">
        <span style="font-size:20px; font-weight: bold;">OVERVIEW</span>
        <br><br>
    </div>
</div>
<div class="box box-solid" style="box-shadow: none;">
    <div style="border-radius: 0px 0px 3px 3px;">
        <div class="row">
            <div class="col-md-6">
                <span class="subtitle">Patient Activity</span>
            </div>
            <div class="col-md-6">
                <ul class="nav nav-chart nav-tabs nav-tabs-button" style="padding: 0px !important; float: right">
                    <li class="active"><a href="#tab_1-1" data-toggle="tab"
                            id="nav-tab-1">Week</a></li>
                    <li><a href="#tab_2-2" data-toggle="tab"
                            id="nav-tab-2">Month</a></li>
                    <li><a href="#tab_3-3" data-toggle="tab"
                        id="nav-tab-3">Year</a></li>
                </ul>
            </div>
        </div>
        

        <div class="tab-content" style="border:1px solid #d2d6de">

            <div class="tab-pane active" id="tab_1-1">
                <div class="box-body">
                    <div class="row">
                        <div class="col-12 p-2">
                            <canvas id="weeklyChart" height="80px"></canvas>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tab-pane" id="tab_2-2">
                <div class="box-body">
                    <div class="row">
                        <div class="col-12 p-2">
                            <canvas id="monthlyChart" height="80px"></canvas>
                        </div>
                    </div>

                </div>
            </div>
            <div class="tab-pane" id="tab_3-3">
                <div class="box-body">
                    <div class="row">
                        <div class="col-12 p-2">
                            <canvas id="yearlyChart" height="80px"></canvas>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <div class="row mt-5">
            <div class="col-md-6">
                <div class="subtitle">Sales Summary</div>
                <div>
                    <span class="summary-value">Rp. {{ $salesSummary->total }}</span>
                    <!-- <button class="btn btn-sm btn-primary" style="float: right">
                        <i class="fa fa-eye"></i>
                    </button> -->
                </div>

                <div class="summary-divider"></div>

                <div class="subtitle">Outstanding Payment</div>
                <div>
                    <span class="summary-value">Rp. {{ $outstandingSummary->total }}</span>
                    <!-- <button class="btn btn-sm btn-primary" style="float: right">
                        <i class="fa fa-eye"></i>
                    </button> -->
                </div>
            </div>
            <div class="col-md-6">
                <div class="subtitle">Treatment Balance</div>

                <div class="table-responsive mt-3">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Invoice</th>
                                <th>Treatment</th>
                                <th>Balance</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($treatments as $item)
                                <tr>
                                    <td>{{ $item->inv_code }}</td>
                                    <td>{{ $item->product_treatment_name }}</td>
                                    <td>{{ $item->qty }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="text-center">
                    <a class="btn btn-warning to-treatment" href="#">More</a>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-6">
                <div class="subtitle">Treatment History</div>

                <div class="table-responsive mt-3">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Code</th>
                                <th>Treatment</th>
                            </tr>
                        </thead>
                        <tbody>

                        @foreach ($overviews['usage'] as $item)
                            <tr>
                                <td>{{ $item['usage_date'] }}</td>
                                @php
                                    $product = '';
                                    $invoice = '';
                                    $therapist = '';
                                    $treatment = '';

                                    foreach($item['usage_detail'] as $data){
                                        $invoice .= '&#10004;'.$data['inv_code']."</br>";
                                        $product .= '&#10004;'.$data['product_treatment']['product_treatment_code']."</br>";
                                        $treatment .= '&#10004;'.$data['product_treatment']['product_treatment_name']."</br>";
                                    }
                                @endphp
                                <td>{!! $product !!}</td>
                                <td>{!! $treatment !!}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="text-center">
                    <a class="btn btn-warning to-treatment" href="#">More</a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="subtitle">Anamnesa</div>

                <div class="table-responsive mt-3">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Anamnesa</th>
                                <th>Suggestion</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($overviews['anamnesas'] as $row)
                            <tr>
                                <td>{{ $row['date_anamnesa'] }}</td>
                                <td>{{ $row['anamnesa'] }}</td>
                                <td>{{ $row['suggestion'] }}</td>    
                                <td>
                                    <a class="btn btn-primary btn-sm" href="/new/#/anamnesas/{{ $row['id_anamnesa'] }}">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="text-center">
                    <a class="btn btn-warning to-anamnesa" href="#">More</a>
                </div>
            </div>
        </div>
    </div>
</div>

@push('script')
<script type="text/javascript" src="{{ asset('plugins/chartjs/Chart.js') }}"></script>
<script>
    function getPatientActivities() {
        $.ajax({
            url: '{{ route('patient.activities', [$overview->id_customer]) }}',
            type: 'GET',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer {{ auth()->user()->api_token }}')
            },
            success: function (res) {
                drawWeeklyChart(res.weekly)
                drawMonthlyChart(res.monthly)
                drawYearlyChart(res.yearly)
            }
        })
    }

    function openTab(tabName) {
        return function (e) {
            e.preventDefault()

            $(`#patientTab a[href="#${tabName}"]`).tab('show')
            $('html, body').animate({ scrollTop: "0" })
        }
    }

    $('.to-anamnesa').click(openTab("anamnesa"))
    $('.to-treatment').click(openTab("treatment"))

    

    function drawWeeklyChart(chartData) {

        let data = []
        let labels = []

        for (let label in chartData) {
            data.push(chartData[label])
            labels.push(label)
        }

        let ctx = document.getElementById('weeklyChart').getContext('2d');
        let chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: [{
                    label: 'This Week',
                    data: data,
                    backgroundColor: '#8a69ae55',
                    borderColor: '#8a69ae',
                    borderWidth: 1.5
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            userCallback: function(label, index, labels) {
                                // when the floored value is the same as the value we have a whole number
                                if (Math.floor(label) === label) {
                                    return label;
                                }
                            },
                        }
                    }]
                }
            }
        });
    }

    chartResize()   

    function chartResize() {
        let yearlyChart = document.getElementById('yearlyChart')
        let monthlyChart = document.getElementById('monthlyChart')
        let weeklyChart = document.getElementById('weeklyChart')

        if ($(window).width() < 575.98) {
            yearlyChart.height = 240
            monthlyChart.height = 240
            weeklyChart.height = 240
        } else {
            yearlyChart.height = 80
            monthlyChart.height = 80
            weeklyChart.height = 80
        }
    }

  $(window).resize(chartResize);

    function drawMonthlyChart(chartData) {

        let data = []
        let labels = []

        for (let label in chartData) {
            data.push(chartData[label])
            labels.push(label)
        }

        let ctx = document.getElementById('monthlyChart').getContext('2d');
        let chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: [{
                    label: 'This Month',
                    data: data,
                    backgroundColor: '#8a69ae55',
                    borderColor: '#8a69ae',
                    borderWidth: 1.5
                }]
            },
            options: {
                scales: {
                    
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            userCallback: function(label, index, labels) {
                                // when the floored value is the same as the value we have a whole number
                                if (Math.floor(label) === label) {
                                    return label;
                                }
                            },
                        }
                    }]
                }
            }
        });
    }

    function drawYearlyChart(chartData) {

        let data = []
        let labels = []

        for (let label in chartData) {
            data.push(chartData[label])
            labels.push(label)
        }

        let ctx = document.getElementById('yearlyChart').getContext('2d');
        let chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: [{
                    label: 'This Year',
                    data: data,
                    backgroundColor: '#8a69ae55',
                    borderColor: '#8a69ae',
                    borderWidth: 1.5
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            userCallback: function(label, index, labels) {
                                // when the floored value is the same as the value we have a whole number
                                if (Math.floor(label) === label) {
                                    return label;
                                }
                            },
                        }
                    }]
                }
            }
        });
    }

    getPatientActivities()
</script>
@endpush

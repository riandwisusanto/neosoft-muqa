<div class="box box-solid">
    <div class="box-header with-border">
        <span style="font-size:20px;">Expired Invoice</span>
        <p style="font-size:15px;"><i>Riwayat pembelian dengan status expired.</i></p>
    </div>
    <div class="box-body table-responsive no-padding">
        <table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>Invoice</th>
					<th>Invoice.date</th>
					<th>Expired.date</th>
					<th>Amount</th>
					<th>Paid amount</th>
					<th>Remarks</th>
					<th>Paid</th>
					<th>Usage</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
                @php
                    $now = now()->toDateString();
                @endphp
                @foreach ($expired_invoice as $invoice)
                    @php
                        $payment = 0;
                        foreach ($invoice->payment as $payments) {
                            $payment += str_replace(",", "", $payments->amount);
                        }

                        if ($invoice->remaining == 0) {
                            $status_paid = '<i class="fa fa-check-circle" aria-hidden="true"></i>';
                        }else {
                            $status_paid = '<i class="fa fa-times-circle" aria-hidden="true"></i>';
                        }

                        foreach ($invoice->invoice_detail as $package) {
                            foreach ($package->invoice_package as $qty) {
                                if ($qty->qty == 0) {
                                    $usage = '<i class="fa fa-check-circle" aria-hidden="true"></i>';
                                }else {
                                    $usage = '<i class="fa fa-times-circle" aria-hidden="true"></i>';
                                }
                            }
                        }
                    @endphp
                    <tr>
                        <td>{{ $invoice->inv_code }}</td>
                        <td>{{ $invoice->inv_date }}</td>
                        <td>{{ $invoice->expired_date }}</td>
                        <td>{{ $invoice->total }}</td>
                        <td>{{ format_money($payment) }}</td>
                        <td>{{ $invoice->remarks }}</td>
                        <td>{!! $status_paid !!}</td>
                        <td>{!! $usage !!}</td>
                        <td><a href="{{ route('customer.active', $invoice->id_invoice) }}" class='btn btn-primary btn-sm'><i class='fa fa-eye'></i></a></td>
                    </tr>
                @endforeach
			</tbody>
		</table>
    </div>
</div>

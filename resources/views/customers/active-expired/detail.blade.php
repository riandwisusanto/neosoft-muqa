@extends('base')

@section('title')
{{ $invoice->inv_date }} - {{ $invoice->expired_date }}
@endsection

@section('breadcrumb')
@parent
<li>Customers</li>
<li>Detail Customer</li>
<li>Invoice</li>
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h4>{{ $invoice->inv_code }}</h4>
            </div>
            <div class="box-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Invoice Num</label>
                        <div class="col-md-10">
                        <p class="form-control-static">
                            {{ $invoice->inv_code }} &nbsp;&nbsp;

                            @if (count($invoice->log_convert) > 0)
                            <a href="{{ route('invoice-convert.print', $invoice->id_invoice) }}"
                                target="_blank">
                                <i class="fa fa-print"></i>
                            </a>
                            @else
                            <a href="{{ route('invoice.print', $invoice->id_invoice) }}"
                                target="_blank">
                                <i class="fa fa-print"></i>
                            </a>
                            @endif

                            &nbsp;

                            @if (count($invoice->log_convert) > 0)
                            <a href="{{ route('invoice-convert.print', $invoice->id_invoice) }}"
                                target="_blank">
                                <i class="fa fa-mobile"></i>
                            </a>
                            @else
                            <a href="{{ route('invoice.print-thermal', $invoice->id_invoice) }}"
                                target="_blank">
                                <i class="fa fa-mobile"></i>
                            </a>
                            @endif
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Outlet</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                {{ $invoice->outlet->outlet_name }}
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Name</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                <a
                                    href="{{ route('customers.show', $invoice->customer_id) }}">{{ $invoice->customer->full_name }}</a>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Phone Number</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                {{ $invoice->customer->phone }}
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Invoice Date</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                {{ $invoice->inv_date }}
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Create Date</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                {{ $invoice->created_at }}
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Expired Date</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                {{ $invoice->expired_date }}
                            </p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Source Activity</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                {{ $invoice->marketing_source->marketing_source_name ?? "-" }}
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Consultant by</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                {{ isset($invoice->consultant) ? $invoice->consultant->consultant_name : '-' }}
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Therapist</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                {{ isset($invoice->therapist) ? $invoice->therapist->therapist_name : '-' }}
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Discount</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                        @php
                                        if (@$invoice->type == 0) {
                                            $discount = number_format(@$invoice->discount);
                                            $type = 'Rp';
                                        }else {
                                            $discount = @$invoice->discount;
                                            $type = '%';
                                        }
                                        @endphp
                                {{ @$discount }} {{ @$type}}
                            </p>
                        </div>
                    </div>

                    <div class="form-horizontal">
                        <div id="table-hidden" class="box box-default table-responsive no-padding">
                            <label for="package_code" class="col-md-2 control-label">Contra Form</label>
                            <div class="col-md-10">
                                <table class="table table-striped tabel-convert">
                                    <thead>
                                        <tr>
                                            <th width="20%">Code</th>
                                            <th width="20%">Name</th>
                                            <th>Unit Price</th>
                                            <th>Paid Qty</th>
                                            <th>Disc</th>
                                            <th>SubTotal</th>
                                            <th>Marketing Activity</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($invoice->invoice_detail as $item)
                                        @php
                                        if ($item->type_line == 0) {
                                            $discount1 = $item->discount_1;
                                            $discount2 = $item->discount_2;
                                            $type = '%';
                                        }else {
                                            $discount1 = number_format($item->discount_1);
                                            $discount2 = number_format($item->discount_2);
                                            $type = 'Rp';
                                        }
                                        @endphp

                                        <tr>
                                            <td>{{ $item->package_code }}</td>
                                            @if ($item->package)
                                            <td>{{ $item->package->package_name }}</td>
                                            @else
                                            <td>{{ $item->product_treatment->product_treatment_name ?? "-" }}</td>
                                            @endif
                                            <td>{{ $item->current_price }}</td>
                                            <td>{{ $item->qty }}</td>
                                            @if ($item->discount_2)
                                            <td>{{ $discount1 }} {{ $type }} | {{ $discount2 }} {{ $type }}</td>
                                            @else
                                            <td>{{ $discount1 }} {{ $type }}</td>
                                            @endif
                                            <td>{{ $item->subtotal }}</td>
                                            @if ($item->activity)
                                            <td>{{ $item->activity->activity_name }}</td>
                                            @else
                                            <td></td>
                                            @endif
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Paid</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                @php
                                if ($invoice->remaining == 0) {
                                $status_paid = '<i class="fa fa-check-circle" aria-hidden="true"></i>';
                                }else {
                                $status_paid = '<i class="fa fa-times-circle" aria-hidden="true"></i>';
                                }
                                @endphp
                                {!! $status_paid !!} Fully Paid
                            </p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Usage</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                @php
                                foreach ($invoice->invoice_detail as $package) {
                                foreach ($package->invoice_package as $qty) {
                                if ($qty->qty == 0) {
                                $usage = '<i class="fa fa-check-circle" aria-hidden="true"></i>';
                                }else {
                                $usage = '<i class="fa fa-times-circle" aria-hidden="true"></i>';
                                }
                                }
                                }
                                @endphp
                                {!! $usage !!} Fully Usage
                            </p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Total</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                {{ $invoice->total }}
                            </p>
                        </div>
                    </div>

                    <div class="form-horizontal">
                        <div id="table-hidden" class="box box-default table-responsive no-padding">
                            <label for="package_code" class="col-md-2 control-label">Payments</label>
                            <div class="col-md-10">
                                <table class="table table-striped tabel-convert">
                                    <thead>
                                        <tr>
                                            <th width="20%">Payment Code</th>
                                            <th width="20%">Date</th>
                                            <th>Payment Amount</th>
                                            <th>Info</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($invoice->payment as $item)
                                        <tr>
                                            <td>{{ $item->bank->bank_code }}</td>
                                            <td>{{ $item->created_at }}</td>
                                            <td>{{ $item->amount }}</td>
                                            <td>{{ $item->info }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Remarks</label>
                        <div class="col-md-10">
                            <p class="form-control-static">
                                {{ $invoice->remarks }}
                            </p>
                        </div>
                    </div>

                </div>
            </div>
            <div class="box-footer text-right">
                <button class="btn btn-success" onclick="editInvoice({{ $invoice->id_invoice }})"><i class="fa fa-edit"
                        aria-hidden="true"></i> Edit</button>
                @if (!$invoice->usage_detail)
                    @if (array_intersect(['ADMINISTRATOR','OUTLET_SUPERVISOR','BRANCH_MANAGER',"SUPER_USER"], json_decode(Auth::user()->level)) || (array_intersect(['OUTLET_SUPERVISOR', 'BRANCH_MANAGER'], json_decode(Auth::user()->level)) && $invoice->inv_date == date('d/m/Y')))
                    <button class="btn btn-danger" onclick="voidInvoice({{ $invoice->id_invoice }})"><i
                            class="fa fa-trash-o"></i> Void</button>
                    @endif
                @endif
            </div>
        </div>

        <div class="box box-solid">
            <div class="box-header with-border">
                <strong>Log</strong>
            </div>
            <div class="box-body">

                @if ($log)
                <div class="col-md-12">
                    @else
                    <div class="col-md-12 hidden">
                        @endif
                        <label for="log_usage" class="control-label">Edited Invoice Records</label>
                        <table class="table table-striped tabel-convert">
                            <thead>
                                <tr>
                                    <th width="20%">Invoice Number</th>
                                    <th width="20%">Staff</th>
                                    <th>Edited Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($log as $logs)
                                <tr>
                                    <td>{{ $logs->invoice->inv_code }}</td>
                                    <td>{{ $logs->user->username }}</td>
                                    <td>{{ $logs->created_at }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    @if ($log_usage)
                    <div class="col-md-12">
                        @else
                        <div class="col-md-12 hidden">
                            @endif
                            <hr class="bg-red">
                            <label for="log_usage" class="control-label">Log Usage</label>
                            <table class="table table-striped tabel-convert">
                                <thead>
                                    <tr>
                                        <th width="20%">Usage Number</th>
                                        <th width="20%">Outlet</th>
                                        <th>Usage Date</th>
                                        <th>Created by</th>
                                        <th>No. of item Used</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($log_usage as $logs)
                                    @if ($logs->usage->status_usage == 0)
                                    <tr>
                                        <td><a href="{{ route('usage-treatment.print', $logs->usage_id) }}"
                                                target="_blank">{{ $logs->usage->usage_code }}</a></td>
                                        <td>{{ $logs->usage->outlet->outlet_name }}</td>
                                        <td>{{ $logs->usage->usage_date }}</td>
                                        <td>{{ $logs->usage->user->username }}</td>
                                        <td>{{ $logs->used }}</td>
                                        <td><a href="{{ route('usage-treatment.show', $logs->usage_id) }}"
                                                class='btn btn-primary btn-sm'><i class='fa fa-eye'></i></a></td>
                                    </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        @if ($log_convert)
                        <div class="col-md-12">
                            @else
                            <div class="col-md-12 hidden">
                                @endif
                                <hr>
                                <label for="log_usage" class="control-label">Log Convert</label>
                                <table class="table table-striped tabel-convert">
                                    <thead>
                                        <tr>
                                            <th width="20%">From Invoice Code</th>
                                            <th width="20%">To Invoice Code</th>
                                            <th>Convert Value</th>
                                            <th>Created By</th>
                                            <th>Convert Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($log_convert as $items)
                                        <tr>
                                            <td>{{ $items->from_invoice_id }}</td>
                                            <td>{{ $items->to_invoice_id }}</td>
                                            <td>{{ format_money($items->convert_value) }}</td>
                                            <td>{{ $items->user->username }}</td>
                                            <td>{{ $items->created_at }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @include('customers.active-expired.edit')
            @include('customers.active-expired.void')
            @endsection

@section('script')
<script>
    $('#date_invoice, #date_expired').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });

    $(function() {

        $('#modal-edit form').validator().on('submit', function(e){
            if(!e.isDefaultPrevented()){
                $.ajax({
                    url : "{{ route('customer.update.active', $invoice->id_invoice) }}",
                    type : "POST",
                    data : $('#modal-edit form').serialize(),
                success : function(data){
                    $('#modal-edit').modal('hide');
                    swal({
                        title: "Successfully Update Invoice",
                        icon: "success",
                    })
                    .then((go) => {
                        location.reload();
                    });
                },
                error : function(){
                    alert("Can not Save the Data!");
                }
                });
                return false;
            }
        });

        $('.simpan').click(function(){
            $('.form-edit').submit();
        });

        $('#modal-void form').validator().on('submit', function(e){
            if(!e.isDefaultPrevented()){
                var id = $('#id').val();

                $.ajax({
                    url : "/customers/"+id+"/void-active-invoice",
                    type : "POST",
                    data : $('#modal-void form').serialize(),
                beforeSend: function() {
                    $('.btn-save').prop('disabled', true);
                    $('#loading').css('display','grid');
                },
                success : function(data){
                    $('#modal-void').modal('hide');
                    $('.btn-save').prop('disabled', false);
                    $('#loading').css('display','none');
                    swal({
                        title: "Successfully Void Invoice",
                        icon: "success",
                    })
                    .then((data) => {
                        window.location.href = "{{ route('customers.show', $invoice->customer_id) }}"
                    });
                },
                error : function(){
                    alert("Can not Save the Data!");
                    $('.btn-save').prop('disabled', false);
                    $('#loading').css('display','none');
                }
                });
                return false;
            }
        });
    })

    function voidInvoice(id) {
        $("#modal-void").modal('show');
        $('.modal-title-void').text('What is The Reason ?');
        $('input[name=_method]').val('PATCH');
        $('#modal-void form')[0].reset();
        $('#id').val(id);
    }

    function editInvoice(id) {
        $("#modal-edit").modal('show');
        $('.modal-title-invoice').text('{{ $invoice->inv_code }}');
        $('input[name=_method]').val('PATCH');
        $('#modal-edit form')[0].reset();
        $('#id').val(id);
    }

</script>
@endsection

<div class="modal" id="modal-edit" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">
                        &times;</span> </button>
                <h3 class="modal-title-invoice"></h3>
            </div>
            <div class="modal-body">
                <form class="form-horizontal form-edit" data-toggle="validator" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" id="id">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Invocie Number</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    {{ $invoice->inv_code }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Customer</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    {{ $invoice->customer->induk_customer }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Full Name</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    {{ $invoice->customer->full_name }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Phone</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    {{ $invoice->customer->phone }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Customer</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    {{ $invoice->customer->birth_place }}, {{ $invoice->customer->birth_date }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Outlet</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    {{ $invoice->outlet->outlet_name }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Marketing Source</label>
                            <div class="col-md-6">
                                <select id="marketing_source" name="marketing_source"
                                    class="form-control select2 select2-hidden-accessible" style="width: 100%;"
                                    tabindex="-1" aria-hidden="true" required>
                                    <option selected="selected">-- Select Source --</option>
                                    @foreach ($marketing_source as $list)
                                    @php
                                    if ($invoice->marketing_source_id == $list->id_marketing_source){
                                    echo "<option value=".$list->id_marketing_source." selected='selected'>
                                        ".$list->marketing_source_name." </option>";
                                    }else {
                                    echo "<option value=".$list->id_marketing_source."> ".$list->marketing_source_name."
                                    </option>";
                                    }
                                    @endphp
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Invoice Date</label>
                            <div class="col-md-6">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="date_invoice"
                                        name="date_invoice" value="{{ $invoice->inv_date }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Expired Date</label>
                            <div class="col-md-6">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="date_expired"
                                        name="date_expired" value="{{ $invoice->expired_date }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Served 1 By</label>
                            <div class="col-md-6">
                                <select id="consultant" name="consultant"
                                    class="form-control select2 select2-hidden-accessible" style="width: 100%;"
                                    tabindex="-1" aria-hidden="true" required>
                                    <option selected="selected">-- Select Consultant --</option>
                                    @foreach ($consultant as $list)
                                    @php
                                    if ($invoice->consultant_id == $list->id_consultant){
                                    echo "<option value=".$list->id_consultant." selected='selected'>
                                        ".$list->consultant_name." </option>";
                                    }else {
                                    echo "<option value=".$list->id_consultant."> ".$list->consultant_name." </option>";
                                    }
                                    @endphp
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Served 2 By</label>
                            <div class="col-md-6">
                                <select id="therapist" name="therapist"
                                    class="form-control select2 select2-hidden-accessible" style="width: 100%;"
                                    tabindex="-1" aria-hidden="true" required>
                                    <option selected="selected">-- Select Therapist --</option>
                                    @foreach ($therapist as $list)
                                    @php
                                    if ($invoice->therapist_id == $list->id_therapist){
                                    echo "<option value=".$list->id_therapist." selected='selected'>
                                        ".$list->therapist_name." </option>";
                                    }else {
                                    echo "<option value=".$list->id_therapist."> ".$list->therapist_name." </option>";
                                    }
                                    @endphp
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-convert form-horizontal">
                            <div class="col-md-12">
                                <table class="table table-striped tabel-convert">
                                    <thead>
                                        <tr>
                                            <th width="30%">Marketing Activity</th>
                                            <th>SKU</th>
                                            <th width="30%">Name</th>
                                            <th align="right">Paid</th>
                                            <th align="right">Disc</th>
                                            <th>SubTotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($invoice->invoice_detail as $item)
                                        @php
                                        $type = '%/Rp';
                                        if ($item->activity){
                                        if ($item->activity->disc_type == 1) {
                                        $type = '%';
                                        }else {
                                        $type = 'Rp';
                                        }
                                        $activityDisc = $item->activity->discount;
                                        $activityName = $item->activity->activity_name;
                                        }else{
                                        $activityDisc = $item->discount;
                                        $activityName = 'Custom Disc. Amount';
                                        }

                                        @endphp
                                        <tr>
                                            <td>{{ $activityName }}</td>
                                            <td>{{ $item->package_code }}</td>
                                            @if ($item->package)
                                                <td>{{ $item->package->package_name }}</td>
                                            @else
                                                <td>{{ $item->product_treatment->product_treatment_name ?? "-" }}</td>
                                            @endif
                                            
                                            <td>{{ $item->qty }}</td>
                                            <td>{{ $activityDisc }} {{ $type }}</td>
                                            <td>{{ $item->subtotal }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Total</label>
                            <div class="col-sm-5">
                                <input class="form-control" type="text" name="total" value="{{ $invoice->total }}"
                                    id="total" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Payment(s) </label>
                            <div class="col-sm-3">
                                <label>Payment Method:</label>
                            </div>
                            <div class="col-sm-2">
                                <label>Amount(s):</label>
                            </div>
                            <div class="col-sm-1">
                                <label>Info:</label>
                            </div>
                        </div>
                        @foreach ($invoice->payment as $payment)
                        <div class="form-group">
                            <label class="col-sm-2 control-label"></label>
                            <input type="hidden" name="id_payment[]" value="{{ $payment->id_payment }}">
                            <div class="col-sm-3">
                                <select class="form-control" name="payment_type[]" required="on">
                                    @foreach ($bank as $item)
                                    @php
                                    if ($item->id_bank == $payment->bank_id){
                                    echo "<option value=".$item->id_bank." selected='selected'> ".$item->bank_name."
                                    </option>";
                                    }else {
                                    echo "<option value=".$item->id_bank."> ".$item->bank_name." </option>";
                                    }
                                    @endphp
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control input-sm" id="amount" name="amount[]"
                                    value="{{ $payment->amount }}" rel="amount" autocomplete="off"
                                    placeholder="Amount(s)" required disabled>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control input-sm" id="payment_info" name="payment_info[]"
                                    autocomplete="off" placeholder="Info" value="{{ $payment->info }}">
                            </div>
                        </div>
                        @endforeach
                </form>
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-primary simpan"><i class="fa fa-floppy-o"></i> Save
                </button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                        class="fa fa-arrow-circle-left"></i>
                    Cancel</button>
            </div>
        </div>


    </div>
</div>
</div>

<div class="row">
    <div class="col-xs-12">
        <span style="font-size:20px; font-weight: bold;">PATIENT INFORMATION</span>
        <br><br>
    </div>
</div>
<div class="row">
    <div class="col-md-9 col-xs-12">
        <div class="box box-solid" style="box-shadow: none;">
            <div class="box-body">
                <div class="form-horizontal">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label class="col-md-6 control-label">Reff Number (old)</label>
                                <div class="col-md-6 control-label" style="text-align: left;">
                                    <p class="form-control-static">
                                        {{ $overview->induk_customer_old }}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-6 control-label">Reff Number</label>
                                <div class="col-md-6 control-label" style="text-align: left;">
                                    <p class="form-control-static">
                                        {{ $overview->induk_customer }}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-6 control-label">Full Name</label>
                                <div class="col-md-6 control-label" style="text-align: left;">
                                    <p class="form-control-static">
                                        {{ $overview->full_name }}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-6 control-label">Nick Name</label>
                                <div class="col-md-6 control-label" style="text-align: left;">
                                    <p class="form-control-static">
                                        {{ $overview->nick_name }}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-6 control-label">Place / Date of Birth</label>
                                <div class="col-md-6 control-label" style="text-align: left;">
                                    <p class="form-control-static">
                                        {{ $overview->birth_place }}, {{ $overview->birth_date }}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-6 control-label">Mobile phone</label>
                                <div class="col-md-6 control-label" style="text-align: left;">
                                    <p class="form-control-static">
                                        {{ $overview->phone }}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-6 control-label">E-mail</label>
                                <div class="col-md-6 control-label" style="text-align: left;">
                                    <p class="form-control-static">
                                        {{ $overview->email }}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-6 control-label">Gender</label>
                                <div class="col-md-6 control-label" style="text-align: left;">
                                    <p class="form-control-static">
                                        @if ($overview->gender == 'M')
                                        Male
                                        @else
                                        Female
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-6 control-label">Religion</label>
                                <div class="col-md-6 control-label" style="text-align: left;">
                                    <p class="form-control-static">
                                        @if ($overview->religion == 1)
                                        Islam
                                        @elseif ($overview->religion == 2)
                                        Buddha
                                        @elseif ($overview->religion == 3)
                                        Hindu
                                        @elseif ($overview->religion == 4)
                                        Katolik
                                        @elseif ($overview->religion == 5)
                                        Kristen
                                        @else
                                        Khonghucu
                                        @endif
                                    </p>
                                </div>
                            </div>
                            {{-- <div class="form-group">
                                <label class="col-md-6 control-label">Marital Status</label>
                                <div class="col-md-6 control-label" style="text-align: left;">
                                    <p class="form-control-static">
                                        @if ($overview->status == 1)
                                        Single
                                        @elseif ($overview->status == 2)
                                        Marriage
                                        @else
                                        Widow / Widower
                                        @endif
                                    </p>
                                </div>
                            </div> --}}
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label class="col-md-6 control-label">ID Card No</label>
                                <div class="col-md-6 control-label" style="text-align: left;">
                                    <p class="form-control-static">
                                        @if ($overview->ktp)
                                        {{ $overview->ktp }}
                                        @else
                                        -
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-6 control-label">Occupation</label>
                                <div class="col-md-6 control-label" style="text-align: left;">
                                    <p class="form-control-static">
                                        @if ($overview->occupation != null)
                                        {{ $overview->occupation }}
                                        @else
                                        -
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-6 control-label">Current Residential Addres</label>
                                <div class="col-md-6 control-label" style="text-align: left;">
                                    <p class="form-control-static">
                                        @if ($overview->address != null)
                                        {{ $overview->address }}
                                        @else
                                        -
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-6 control-label">Emergency Contact</label>
                                <div class="col-md-6 control-label" style="text-align: left;">
                                    <p class="form-control-static">
                                        @if ($overview->nextofikin_number != null)
                                        {{ $overview->nextofikin_number }}
                                        @else
                                        -
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-6 control-label">Purpose Treatment</label>
                                <div class="col-md-6 control-label" style="text-align: left;">
                                    <p class="form-control-static">
                                        @if ($overview->purpose_treatment != null)
                                        {{ $overview->purpose_treatment }}
                                        @else
                                        -
                                        @endif
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-6 control-label">Nationality</label>
                                <div class="col-md-6 control-label" style="text-align: left;">
                                    <p class="form-control-static">
                                        {{ $overview->country->country ?? '' }}
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-6 control-label">City</label>
                                <div class="col-md-6 control-label" style="text-align: left;">
                                    <p class="form-control-static">
                                        {{ $overview->city->province ?? '' }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <br>
                            <a onclick="editPatient({{ $overview->id_customer }})" class="btn btn-warning btn-sm btn-flat" style="min-width: 120px;">Edit</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        <div class="box-body" style="padding-top: 0px">
            <div class="box-group" id="accordion">
                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                <div class="box ">
                    <div class="box-header with-border clearfix">
                        <h4 class="box-title" style="color: #72afd2">
                            Consultant
                        </h4>
                        <a onclick="addConsultant()" class="btn btn-success btn-xs pull-right"><i class="fa fa-plus-circle"></i> Add</a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding" style="height: auto; overflow: auto">
                        <table class="table">
                            <thead style="position: sticky; top: 0; background-color: white">
                                <th>Consultant</th>
                                <th style="width: 50px">Action</th>
                            </thead>
                            <tbody>
                                @foreach ($overview->consultant as $item)
                                <tr>
                                    <td>{{ $item->consultant_name }}</td>
                                    <td>
                                        <form
                                            action="{{route('customers.consultant.destroy',[$item->pivot->id_customer_consultant])}}"
                                            method="post">
                                            @csrf
                                            <button type="submit" class="btn btn-danger btn-xs">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

        <div class="box-body">
            <div class="box-group" id="accordion">
                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                <div class="panel box ">
                    <div class="box-header with-border clearfix">
                        <h4 class="box-title" style="color: #72afd2">
                            Outlet
                        </h4>
                        <button onclick="addOutlet()" class="btn btn-success btn-xs pull-right"><i class="fa fa-plus-circle"></i> Add</button>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding" style="height: auto; overflow: auto">
                        <table class="table">
                            <thead style="position: sticky; top: 0; background-color: white">
                                <th>Outlet</th>
                                <th style="width: 50px">Action</th>
                            </thead>
                            <tbody>
                                @foreach ($overview->outlet as $item)
                                <tr>
                                    <td>{{ $item->outlet_name }}</td>
                                    <td>
                                        <form
                                            action="{{route('customers.outlet.destroy',[$item->pivot->id_customer_outlet])}}"
                                            method="post">
                                            @csrf
                                            <button type="submit" class="btn btn-danger btn-xs">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .table-fixed {

        /* width: 100%; */
        tbody {
            height: 50px;
            overflow-y: auto;
            width: 100%;
        }

        thead,
        tbody,
        tr,
        td,
        th {
            display: block;
        }

        tbody {
            td {
                float: left;
            }
        }

        thead {
            tr {
                th {
                    float: left;
                    background-color: #f39c12;
                    border-color: #e67e22;
                }
            }
        }
    }
</style>

@include('customers.information.add_consultant')
@include('customers.information.add_outlet')
@include('customers.form')

@push('script')
<script>
    var save_method_information, save_method;

    function addConsultant(){
        save_method_information = "add";
        $('input[name=_method]').val('POST');
        $("#modal-consultant").modal({
        backdrop: 'static',
        keyboard: false,
        show: true
        });
        $('#modal-consultant form')[0].reset();
        $('#group_id').val('').change()
        $('.modal-title').text('Add Consultant');
    }

    function addOutlet(){
        save_method_information = "add";
        $('input[name=_method]').val('POST');
        $("#modal-outlet").modal({
        backdrop: 'static',
        keyboard: false,
        show: true
        });
        $('#modal-outlet form')[0].reset();
        $('#group_id').val('').change()
        $('.modal-title').text('Add Outlet');
    }

    $('#datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        todayHighlight: true
    });

    $(function(){
        chooseCountry();
        validation();
    });

    function getCity(country, city){
    $.ajax({
        url:'/customers/'+country+'/province',
        type:'GET',
        dataType: 'json',
            beforeSend: function() {
                $(".loader").removeClass('hidden');
                $(".loader").show();
            },
            success:function(data){
                $('select[name="city"]').empty();
                $(".loader").addClass('hidden');
                $(".loader").hide();
                for (let index in data) {
                    $('select[name="city"]').append('<option value="'+index+'">'+data[index]+'</option>')
                }
                if (city) {
                    $('select[name="city"]').val(city).change();
                }
                return false;
            }
    });
    }

    function chooseCountry() {
    $('#country').change(function(){
        var countryID = $(this).val();
        if(countryID){
        getCity(countryID);
        }
        return false;
    });
    }

    $('#modal-customers form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
         var id = $('#id').val();
         if(save_method == "add") url = "{{ route('customers.store') }}";
         else url = id;
         $.ajax({
            url : url,
            type : "POST",
            data : $('#modal-customers form').serialize(),
           success : function(data){
                if (data.status == false) {
                    alert('Something Wrong!!, Please Check Message Error!!!');
                    $("#msg_email").html(data.message.email).show();
                    $("#msg_phone").html(data.message.phone).show();
                }else{
                    $('#gender').val('').trigger('change');
                    $('#religion').val('').trigger('change');
                    $('#status').val('').trigger('change');
                    $('#country').val('').trigger('change');
                    $('#outlet').val('').trigger('change');
                    $('#city').val('').trigger('change');
                    $('#consultant').val('').trigger('change');
                    $('#modal-customers').modal('hide');
                    $("#msg_email").html('');
                    $("#msg_phone").html('');
                    location.reload(true);
                    return false;
                }
           },
           error : function(e){
               console.log(e);
             alert("Can not Save the Data!");
           }
         });
         return false;
     }
   });

    function editPatient(id){
        save_method = "edit";
        $('input[name=_method]').val('PATCH');
        $('#modal-customers form')[0].reset();
        $.ajax({
            url : "/customers/"+id+"/edit",
            type : "GET",
            dataType : "JSON",
            success : function(data){
            $('#modal-customers').modal('show');
            $('.modal-title').text('EDIT PATIENT');
            $('.modal-doc').text('Edit patient’s data.');
            $('#btn-more').css('margin-left', '50%');
            $('#id').val(data.edit.id_customer);
            $('#fullname').val(data.edit.full_name);
            $('#nick_name').val(data.edit.nick_name);
            $('#phone').val(data.edit.phone);
            $('#email').val(data.edit.email);
            $('#place').val(data.edit.birth_place);
            $('#datepicker').val(data.edit.birth_date);
            $('#join_date').val(data.edit.join_date);
            $('#gender').val(data.edit.gender).trigger('change');
            $('#religion').val(data.edit.religion).trigger('change');
            $('#status').val(data.edit.status_customer).trigger('change');
            $('#noktp').val(data.edit.ktp);
            $('#address').val(data.edit.address);
            $('#occupation').val(data.edit.occupation);
            $('#zip').val(data.edit.zip);
            $('#country').val(data.edit.country_id).trigger('change');
            getCity(data.edit.country_id, data.edit.city_id);
            $('#outlet').removeAttr('required');
            $('#consultant').removeAttr('required');
            $('#group_outlet').hide();
            $('#group_consultant').hide();
            $('#familyname').val(data.edit.nextofikin);
            $('#familyphone').val(data.edit.nextofikin_number);
            $('#source').val(data.edit.source_id).trigger('change');
            $('#source_remarks').val(data.edit.source_remarks);
            $('#purpose_treatment').val(data.edit.purpose_treatment);
            },
            error : function(e){
            alert("Can not Show the Data!");
            }
        });
        return false;
    }

    // validas
    function validation() {
    $("#noktp").keypress(function(data){
        //console.log(data.which);
        if(data.which < 48 || data.which > 57)
        {
            $("#msg_ktp").html("Please insert real IC numbers").show().fadeOut(4000);
            return false;
        }
        });

        $("#phone").on('keyup change',function(){
            $("#phone").each(function(){
                this.value=this.value.replace(/[^0-9]/g,'');
            });
        });

        $("#familyphone").keypress(function(data){
        if(data.which < 48 || data.which > 57)
        {
            $("#msg_family").html("Please insert real numbers phone").show().fadeOut(4000);
            return false;
        }
        });

        $("#zip").keypress(function(data){
        //console.log(data.which);
        if(data.which < 48 || data.which > 57)
        {
            $("#msg_zip").html("Please insert real Zip Code").show().fadeOut(4000);
            return false;
        }
        });
    }
</script>
@endpush

<div class="modal" id="modal-outlet" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <form action="{{route('customers.outlet.store',$overview->id_customer)}}" class="form-horizontal" data-toggle="validator" method="post">
                {{ csrf_field() }} {{ method_field('POST') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times fa-lg"></i>   
                    </button>
                    <h3 class="modal-title"></h3>
                </div>
                <div class="modal-body" >
                    <input type="hidden" id="id_activity" name="id_activity">
                        <div class="form-group">
                            <label for="outlet_id" class="col-md-3 control-label">Outlet</label>
                            <div class="col-md-8">
                                <select id="outlet_id"  name="outlet" class="form-control">
                                    {{-- <option value=""></option> --}}
                                @foreach($list_outlet as $category)
                                    <option value="{{ $category->id_outlet }}">{{ $category->outlet_name }}</option>
                                @endforeach
                                </select> 
                            </div>
                        </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
                    </div>
                </div>
            </form>
        </div>
     </div>
</div>
<div class="modal" id="modal-consignment-digital" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
                <!-- <form  class="form-horizontal" data-toggle="validator" method="post">
                {{ csrf_field() }} {{ method_field('POST') }}
                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times fa-lg"></i>
                    </button>
                    <h3 class="modal-title" id="title"></h3>
                </div>
                <div class="modal-body" >
                    <input type="hidden" id="id" name="id_customer">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Consignment Date</label>
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right" id="consignment_date"
                                                    name="consignment_date" value="<?= date('d/m/Y') ?>" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="summary_id" class="control-label">Upload Consignment</label>
                                            <input type="file" name="image" class="form-control" id="image" required>

                                        </div>
                                </div>     
                            </div>
                    </div>
                      <br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal" id="close-modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
                            </div>
                        </div>
                    </div>
                 
                </div>
            </form> -->
            <div class="row">
                @php
                    $url = explode('/',url()->current());
                    $custId = end($url);
                @endphp
                <a style="text-decoration:none;color:white;" href="{{ url('/new/#/consent/MedicalConsign') }}/{{$custId}}" class="col-xs-12 btn btn-success">Medical Consign / Refusal Letter</a>
            </div>
        </div>
     </div>
</div>

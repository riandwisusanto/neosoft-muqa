<div class="row">
    <div class="col-xs-12">
        <span style="font-size:20px; font-weight: bold;">CONSIGNMENT FORM DIGITAL</span>
    </div>
    <div class="col-xs-12" style="margin-top: 5px;">
        <button onclick="addConsignmentDigital()" class="btn btn-success" >Create New Consignment Form Digital</button>
        <br><br>
    </div>
</div>
<div class="box box-solid" style="box-shadow: none;">
    <div class="box-header">
        <div class="row">
            <div class="col-xs-12">
                <span style="font-size: 17px; font-weight: bold; cursor: pointer;" id="slide-toggle-consignment2-history">Consignment History <i class="fa fa-angle-down rotate" style="margin-left: 15px;"></i></span>
            </div>
        </div>
    </div>
    <div class="box-body table-responsive no-padding">
        <div class="table-consignment2-history">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr class="text-capitalize">
                        <th>Consignment Date</th>
                        <th>Outlet</th>
                        <th>File</th>
                        <!-- <th>Action</th> -->
                    </tr>
                </thead>
                <tbody>
                @foreach($consignment as $con)
                    <tr>
                        <td>{{ $con->consignment_date->format('d/m/Y') }}</td>
                        <td>
                            @if ($con->customer->outlet)
                                @foreach ($con->customer->outlet as $outlet)
                                   • {{ $outlet->outlet_name }} <br>
                                @endforeach
                            @endif
                        </td>
                        <td><a class="btn" onclick="showPict({{ $con->id_customer_consignment}})"><img src="{{ asset('storage'.'/'.$con->image) }}" alt="" style="width: 100px;"></a></td>
                    </tr>
                @endforeach
                @foreach($consent as $row)
                    <tr>
                        <td>{{$row->created_at->format('d-m-Y')}}</td>
                        <td>
                            @foreach ($row->customer->outlet as $outlet)
                                • {{ $outlet->outlet_name }} <br>
                            @endforeach
                        </td>
                        <td>Medical Consign</td>
                        <td><a href="{{asset($row->file)}}" download="Medical Consign {{$row->customer->full_name}}.pdf"><i class="fa fa-download"></i> Download the pdf</a></td>
                        <td>
                            <button onclick="consignDelete({{$row->id}},'medicalConsign')" type="button" class="btn btn-danger btn-del-consign btn-xs">
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


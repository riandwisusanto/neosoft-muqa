<div class="row">
    <div class="col-xs-12">
        <span style="font-size:20px; font-weight: bold;">INVOICE</span>
        <br><br>
    </div>
</div>

<div class="box box-solid" style="box-shadow: none;">
    <div class="box-header">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <span style="font-size: 17px; font-weight: bold; cursor: pointer;" id="slide-toggle-invoice-active-void">Active/Void Invoice <i class="fa fa-angle-down rotate" style="margin-left: 15px;"></i></span>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="pull-right mobile-left">
                    <span style="font-size: 17px; margin-right: 10px;">ACTIVE INVOICE</span>
                    <label class="switch">
                        <input id="switch-invoice-active-void" type="checkbox">
                        <span class="slider round"></span>
                    </label>
                    <span style="font-size: 17px; margin-left: 10px;">VOID INVOICE</span>
                </div>
            </div>
        </div>
    </div>
    <div class="box-body table-responsive no-padding">
        <div class="table-invoice-active-void">

            <div class="table-active-invoice">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr class="text-capitalize">
                            <th>Invoice</th>
                            <th>Invoice date</th>
                            <th>Expired date</th>
                            <th>Amount</th>
                            <th>Paid amount</th>
                            <th>Remarks</th>
                            <th>Paid</th>
                            <th>Usage</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $last = $active_invoice->first();
                        @endphp
                        @foreach ($active_invoice as $invoice)
                            @php
                                $usage = '<i class="fa fa-times-circle" aria-hidden="true"></i>';
                                $payment = 0;
                                foreach ($invoice->payment as $payments) {
                                    $payment += str_replace(",", "", $payments->amount);
                                }

                                if ($invoice->remaining == 0) {
                                    $status_paid = '<i class="fa fa-check-circle" aria-hidden="true"></i>';
                                }else {
                                    $status_paid = '<i class="fa fa-times-circle" aria-hidden="true"></i>';
                                }

                                foreach ($invoice->invoice_detail as $package) {
                                    foreach ($package->invoice_package as $qty) {
                                        if ($qty->qty == 0) {
                                            $usage = '<i class="fa fa-check-circle" aria-hidden="true"></i>';
                                        }else {
                                            $usage = '<i class="fa fa-times-circle" aria-hidden="true"></i>';
                                        }
                                    }
                                }

                                if ($last->id_invoice == $invoice->id_invoice){
                                    $color = '<span class="label label-danger">New</span>';
                                }
                                else{
                                    $color = "";
                                }
                                
                            @endphp
                            <tr>
                                <td>{{ $invoice->inv_code }} {!! $color !!}</td>
                                <td>{{ $invoice->inv_date }}</td>
                                <td>{{ $invoice->expired_date }}</td>
                                <td>{{ $invoice->total }}</td>
                                <td>{{ format_money($payment) }}</td>
                                <td>{{ $invoice->remarks }}</td>
                                <td>{!! $status_paid !!}</td>
                                <td>{!! $usage !!}</td>
                                <td>
                                    
                                    <a href="{{ route('customer.active', $invoice->id_invoice) }}" class='btn btn-primary btn-sm'><i class='fa fa-eye'></i></a>
                                    <a onclick='printWa({{ $invoice->id_invoice }})' class='btn btn-success btn-sm'><i class="fa fa-whatsapp" aria-hidden="true"></i></a>               
                                    @if ( ! isset($invoice->customer_sign))
                                    <a href="{{ route('invoice.sign', $invoice->id_invoice) }}" class='btn btn-primary btn-sm'><i class='fa fa-pencil'></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="table-void-invoice">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr class="text-capitalize">
                            <th>Invoice</th>
                            <th>Invoice date</th>
                            <th>Expired date</th>
                            <th>Amount</th>
                            <th>Invoice By</th>
                            <th>Remarks</th>
                            <th>Void Date</th>
                            <th>Void By</th>
                            <th>Void Remarks</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($void_invoice as $invoice)
                            <tr>
                                <td>{{ $invoice->inv_code }}</td>
                                <td>{{ $invoice->inv_date }}</td>
                                <td>{{ $invoice->expired_date }}</td>
                                <td>{{ $invoice->total }}</td>
                                <td>{{ $invoice->user->username }}</td>
                                <td>{{ $invoice->remarks }}</td>
                                <td>{{ $invoice->log_void_invoice->created_at }}</td>
                                <td>{{ $invoice->log_void_invoice->user->username }}</td>
                                <td>{{ $invoice->log_void_invoice->remarks }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

<div class="box box-solid" style="box-shadow: none;">
    <div class="box-header">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <span style="font-size: 17px; font-weight: bold; cursor: pointer;" id="slide-toggle-invoice-balance-void">Invoice Balance/Void Balance <i class="fa fa-angle-down rotate" style="margin-left: 15px;"></i></span>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="pull-right mobile-left">
                    <span style="font-size: 17px; margin-right: 10px;">INVOICE BALANCE</span>
                    <label class="switch">
                        <input id="switch-invoice-balance-void" type="checkbox">
                        <span class="slider round"></span>
                    </label>
                    <span style="font-size: 17px; margin-left: 10px;">VOID BALANCE</span>
                </div>
            </div>
        </div>
    </div>
    <div class="box-body table-responsive no-padding">
        <div class="table-invoice-balance-void">

            <div class="table-invoice-balance">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr class="text-capitalize">
                            <th>Balance Code</th>
                            <th>Balance Date</th>
                            <th>Invoice Code</th>
                            <th>Invoice Date</th>
                            <th>Outstanding Amount</th>
                            <th>Balance Amount</th>
                            <th>Remarks</th>
                            <th>Created By</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $balance_total = 0;
                        @endphp
                        @foreach ($balance as $item)
                        <tr>
                            <td>{{ $item->balance_code }}</td>
                            <td>{{ $item->balance_date }}</td>
                            <td>{{ $item->invoice->inv_code }}</td>
                            <td>{{ $item->invoice->inv_date }}</td>
                            <td>{{ $item->outstanding }}</td>
                            @foreach ($item->payment as $pay)
                                @php
                                    if ($pay->balance_id){
                                        $balance_total += str_replace(",", "", $pay->amount);
                                    }
                                @endphp
                            @endforeach
                            <td>{{ format_money($balance_total) }}</td>
                            <td>{{ $item->remarks }}</td>
                            <td>{{ $item->user->username }}</td>
                            <td><a href="{{ route('invoice-balance.show', $item->id_balance) }}" class='btn btn-primary btn-sm'><i class='fa fa-eye'></i></a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="table-void-balance">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr class="text-capitalize">
                            <th>Balance</th>
                            <th>Balance date</th>
                            <th>Outstanding</th>
                            <th>Balance By</th>
                            <th>Remarks</th>
                            <th>Void Date</th>
                            <th>Void By</th>
                            <th>Void Remarks</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($void_balance as $balance)
                            <tr>
                                <td>{{ $balance->balance_code }}</td>
                                <td>{{ $balance->balance_date }}</td>
                                <td>{{ $balance->outstanding }}</td>
                                <td>{{ $balance->user->username }}</td>
                                <td>{{ $balance->remarks }}</td>
                                <td>{{ $balance->log_void_balance->created_at }}</td>
                                <td>{{ $balance->log_void_balance->user->username }}</td>
                                <td>{{ $balance->log_void_balance->remarks }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
<div class="box box-solid" style="box-shadow: none;">
    <div class="box-header">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <span style="font-size: 17px; font-weight: bold; cursor: pointer;" id="slide-toggle-outstanding-invoice">Outstanding Invoice/Expired Invoice<i class="fa fa-angle-down rotate" style="margin-left: 15px;"></i></span>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="pull-right mobile-left">
                    <span style="font-size: 17px; margin-right: 10px;">OUTSTANDING INVOICE</span>
                    <label class="switch">
                        <input id="switch-invoice-outstanding-invoice" type="checkbox">
                        <span class="slider round"></span>
                    </label>
                    <span style="font-size: 17px; margin-left: 10px;">EXPIRED INVOICE</span>
                </div>
            </div>
        </div>
    </div>
    <div class="box-body table-responsive no-padding">
        <div class="table-outstanding-invoice">
            <div class="table-invoice-outstanding">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr class="text-capitalize">
                            <th>Invoice Code</th>
                            <th>Payable</th>
                            <th>Paid</th>
                            <th>Outstanding</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($outstandingInvoice as $item)
                        @if ($item->remaining > 0 )
                            <tr>
                                <td>{{ $item->inv_code }}</td>
                                <td>{{ $item->total }}</td>
                                @php
                                    $total_pay = '';
                                    foreach($item->payment as $payment){
                                        $total_pay .= '<li>'.$payment->amount.'</li>';
                                    }
                                @endphp
                                <td>{!! $total_pay !!}</td>
                                <td>{{ $item->remaining }}</td>
                            </tr>
                            
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="table-invoice-expired">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr class="text-capitalize">
                        <th>Invoice</th>
                        <th>Invoice.date</th>
                        <th>Expired.date</th>
                        <th>Amount</th>
                        <th>Paid amount</th>
                        <th>Remarks</th>
                        <th>Paid</th>
                        <th>Usage</th>
                        <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php
                        $now = now()->toDateString();
                    @endphp
                    @foreach ($expired_invoice as $invoice)
                        @php
                            $payment = 0;
                            foreach ($invoice->payment as $payments) {
                                $payment += str_replace(",", "", $payments->amount);
                            }

                            if ($invoice->remaining == 0) {
                                $status_paid = '<i class="fa fa-check-circle" aria-hidden="true"></i>';
                            }else {
                                $status_paid = '<i class="fa fa-times-circle" aria-hidden="true"></i>';
                            }

                            foreach ($invoice->invoice_detail as $package) {
                                foreach ($package->invoice_package as $qty) {
                                    if ($qty->qty == 0) {
                                        $usage = '<i class="fa fa-check-circle" aria-hidden="true"></i>';
                                    }else {
                                        $usage = '<i class="fa fa-times-circle" aria-hidden="true"></i>';
                                    }
                                }
                            }
                        @endphp
                        <tr>
                            <td>{{ $invoice->inv_code }}</td>
                            <td>{{ $invoice->inv_date }}</td>
                            <td>{{ $invoice->expired_date }}</td>
                            <td>{{ $invoice->total }}</td>
                            <td>{{ format_money($payment) }}</td>
                            <td>{{ $invoice->remarks }}</td>
                            <td>{!! $status_paid !!}</td>
                            <td>{!! $usage !!}</td>
                            <td><a href="{{ route('customer.active', $invoice->id_invoice) }}" class='btn btn-primary btn-sm'><i class='fa fa-eye'></i></a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
function printWa(id){
    $.ajax({
        url : "/invoice-create/"+id+"/wa",
        type : "GET",
        success: function (data) {
            if (data) {
                swal({
                    text: 'Send WA Success',
                    icon: 'success'
                })
            }
            return false;
        },
        error: function (e) {
            alert("Can not Save the Data!");
        }
    });
    return false;
}
</script>
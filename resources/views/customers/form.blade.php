<div class="modal" id="modal-customers" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <form data-toggle="validator" method="post">
                {{ csrf_field() }} {{ method_field('POST') }}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times fa-lg"></i>
                    </button>
                    <span style="font-size:20px; font-weight: bold;" class="modal-title"></span>
                    <p style="font-size:15px; font-weight: bold;"><i class="modal-doc"></i></p>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <div class="row">
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="fullname">Full Name *</label>
                            <input type="text" class="form-control" id="fullname" name="fullname"
                                placeholder="Full Name" autocomplete="off" required>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="fullname">Nick Name *</label>
                            <input type="text" class="form-control" id="nick_name" name="nick_name"
                                placeholder="Nick Name" autocomplete="off" required>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="contact">Mobile phone </label>
                            <input type="text" class="form-control" id="phone" name="phone" autocomplete="off"
                                placeholder="Contact Number Active">
                            <span class="help-block text-red" id="msg_phone"></span>
                        </div>

                        <div class="form-group col-sm-12 col-md-6">
                            <label for="username">E-mail *</label>
                            <input type="text" class="form-control" id="email" name="email"
                                placeholder="Email Addres Active" required>
                            <span class="help-block text-red" id="msg_email"></span>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="gender">Gender</label>
                            <select style="width:100%" id="gender" type="text" class="form-control" name="gender"
                                required>
                                <option></option>
                                <option value="M">Male</option>
                                <option value="F">Female</option>
                            </select>
                            <span class="help-block with-errors"></span>
                        </div>

                        <div class="form-group col-sm-12 col-md-6">
                            <label for="country">Nationality *</label>
                            <select style="width:100%" class="form-control" name="country" id="country" required>
                                <option></option>
                                @foreach ($country as $list)
                                <option value="{{ $list->id_country }}">{{ $list->country }}</option>
                                @endforeach
                            </select>
                            <span class="help-block text-red"></span>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="city">City *</label>
                            <select style="width:100%" class="form-control" name="city" id="city" required>
                                <option value="">oke</option>
                            </select>
                            <span class="help-block text-red"></span>
                        </div>


                        <div class="form-group col-sm-12 col-md-6" id="group_outlet">
                            <label for="outlet">Outlet *</label>
                            <select style="width:100%" class="form-control" name="outlet" id="outlet" required>
                                <option></option>
                                @foreach ($outlet as $list)
                                <option value="{{ $list->id_outlet }}">{{ $list->outlet_name }}</option>
                                @endforeach
                            </select>
                            <span class="help-block text-red" id="msg_outlet"></span>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="place">Place / Date of Birth</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="place" name="place" placeholder="Place"
                                        required>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" id="datepicker"
                                            name="birth_date" autocomplete="off" required>
                                    </div>
                                </div>
                            </div>
                            <span class="help-block text-red"></span>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="role">Religion</label>
                            <select style="width:100%" class="form-control" name="religion" id="religion" required>
                                <option></option>
                                <option value="1">Islam</option>
                                <option value="2">Buddha</option>
                                <option value="3">Hindu</option>
                                <option value="4">Katolik</option>
                                <option value="5">Kristen</option>
                                <option value="6">Khonghucu</option>
                            </select>
                            <span class="help-block text-red"></span>
                        </div>
                        <div class="form-group col-sm-12 col-md-6" id="group_consultant">
                            <label for="consultant">Served By</label>
                            <select style="width:100%" class="form-control" name="consultant" id="consultant" required>
                                <option></option>
                                @foreach ($consultant as $list)
                                <option value="{{ $list->id_consultant }}">{{ $list->consultant_name }}</option>
                                @endforeach
                            </select>
                            <span class="help-block text-red" id="msg_consultant"></span>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                                <label for="address">Current Residential Addres *</label>
                                <textarea class="form-control" placeholder="Address (Optional)" name="address" id="address" rows="1" required></textarea>
                            </div>
                        <div class="text-center">
                            <a id="btn-more" class="btn btn-success btn-sm" role="button" data-toggle="collapse" href="#collapseExample"
                                aria-expanded="false" aria-controls="collapseExample">
                                <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                                <strong>
                                    More
                                </strong>
                                <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                            </a>
                        </div>
                        <hr>
                        <div class="collapse" id="collapseExample">
                            <div class="form-group col-sm-12 col-md-6">
                                <label for="role">Purpose Treatment</label>
                                <input type="text" class="form-control" name="purpose_treatment" id="purpose_treatment"
                                    placeholder="Purpose Treatment (Optional)">
                                <span class="help-block text-red"></span>
                            </div>
                            <div class="form-group col-sm-12 col-md-6">
                                <label for="noktp">ID Card No </label>
                                <input type="text" class="form-control" id="noktp" name="noktp"
                                    placeholder="ID Number (Optional)" autocomplete="off" onkeypress="">
                                <span class="help-block text-red" id="msg_ktp"></span>
                            </div>

                            <div class="form-group col-sm-12 col-md-6">
                                <label for="zip">Zip Code </label>
                                <input type="text" class="form-control" id="zip" name="zip" placeholder="Zip Code (Optional)" autocomplete="off">
                                <span class="help-block text-red" id="msg_zip"></span>
                            </div>

                            <div class="form-group col-sm-12 col-md-6">
                                <label for="join_date">Join Date</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="join_date" name="join_date"
                                        required>
                                </div>
                                <span class="help-block text-red"></span>
                            </div>
                            <div class="form-group col-sm-12 col-md-6">
                                <label for="occupation">Occupation </label>
                                <input type="text" class="form-control" id="occupation" name="occupation"
                                    placeholder="Occupation (Optional)" autocomplete="off">
                                <span class="help-block text-red"></span>
                            </div>

                            <div class="form-group col-sm-12 col-md-6">
                                <label for="place">Emergency Contact</label>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <input type="text" class="form-control" id="familyname" name="familyname"
                                            placeholder="Family Name (Optional)">
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="input-group date">
                                            <input type="text" class="form-control pull-right" id="familyphone"
                                                name="familyphone" placeholder="Family Phone (Optional)">
                                            <span class="help-block text-red" id="msg_family"></span>
                                        </div>
                                    </div>
                                </div>
                                <span class="help-block text-red"></span>
                            </div>
                            <div class="form-group col-sm-12 col-md-6">
                                <label for="role">How did you hear about us?</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <select style="width:100%" class="form-control" name="source" id="source"
                                            placeholer="Marketing Source (Optional)">
                                            <option></option>
                                            @foreach ($marketing_source as $list)
                                            <option value="{{ $list->id_marketing_source }}">
                                                {{ $list->marketing_source_name }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group date">
                                            <input type="text" class="form-control pull-right" name="source_remarks"
                                                id="source_remarks" placeholder="Source Remarks (Optional)">
                                        </div>
                                    </div>
                                </div>
                                <span class="help-block text-red"></span>
                            </div>
                            {{-- <div class="well">
                            </div> --}}
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                        <i class="fa fa-arrow-circle-left"></i> Cancel
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
    <div class="box-header with-border">
        <span style="font-size:20px;">Treatment Balance</span>
        <p style="font-size:15px;"><i>Informasi sisa perawatan.</i></p>
    </div>
        <div class="box box-primary">
            <form action="{{ route('customer.treatment-balance', $id) }}" method="POST">
                {{ csrf_field() }}
                @foreach ($invoice as $list)
                @if ($list->remaining == 0)

                <div class="box-header with-border">
                    <div class="col-md-4 pull-right">
                        @if($errors->any())
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong>{{ $errors->first() }}</strong>
                        </div>
                        @endif
                    </div>
                    <h2>{{ $list->inv_code }}</h2>
                </div>

                <div class="box-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Invoice Num</label>
                            <div class="col-md-10">
                                <p class="form-control-static">
                                    {{ $list->inv_code }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Invoice Date</label>
                            <div class="col-md-10">
                                <p class="form-control-static">
                                    {{ $list->inv_date }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Expired Date</label>
                            <div class="col-md-10">
                                <p class="form-control-static">
                                    {{ $list->expired_date }}
                                </p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Item</label>
                            <div class="col-md-10">

                                <table class="table table-striped tabel-pembelian">
                                    <thead>
                                        <tr>
                                            <th>Treatment Code</th>
                                            <th>Treatment Name</th>
                                            <th>Unit Value</th>
                                            <th>Paid</th>
                                            <th>Doing</th>
                                            <th>Balance</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($list->invoice_detail as $item)
                                        @foreach ($item->invoice_package as $package)
                                        <tr>
                                            <td>
                                                @php
                                                $date = str_replace('/', '-', $list->expired_date );
                                                $newDate = date("Y-m-d", strtotime($date));
                                                @endphp
                                                @if ($package->qty == 0 || $newDate <= now()->toDateString())
                                                    <input type="checkbox" name="check_code[]" id="check_code"
                                                        value="{{ $package->id_invoice_package }}"
                                                        disabled>{{ $package->product_treatment->product_treatment_code }}
                                                    ({{ $item->package_code }})
                                                    @else
                                                    <input type="checkbox" name="check_code[]" id="check_code"
                                                        value="{{ $package->id_invoice_package }}">{{ $package->product_treatment->product_treatment_code }}
                                                    ({{ $item->package_code }})
                                                    @endif
                                            </td>
                                            <td>{{ $package->product_treatment->product_treatment_name }}</td>
                                            <td>{{ format_money($package->nominal) }}</td>
                                            <td>{{ $package->used + $package->qty }}</td>
                                            <td>{{ $package->used }}</td>
                                            <td>{{ $package->qty }}</td>
                                        </tr>
                                        @endforeach
                                        @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 pull-right">
                        @if($errors->any())
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong>{{ $errors->first() }}</strong>
                        </div>
                        @endif
                    </div>
                </div>
                @endif
                @endforeach
                <div class="box-footer text-right">
                    <button class="btn btn-success" type="submit"><i class="fa fa-handshake-o" aria-hidden="true"></i>
                        Usage</button>
                </div>
            </form>
        </div>
    </div>
</div>

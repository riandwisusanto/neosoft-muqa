@extends('base')

@section('title')
Treatment Balance
@endsection

@section('style')
<style>
    .rating {
        margin-top: 10px;
    }

    /* HIDE RADIO */
    .rating [type=radio] {
        position: absolute;
        opacity: 0;
        width: 0;
        height: 0;
    }

    /* IMAGE STYLES */
    .rating [type=radio]+.img {
        cursor: pointer;
        height: 50px;
        width: 50px;
        margin-right: 10px;
        background-color: red;
        background-image: url('{{ asset('img/rating-emoji.png') }}');
        background-size: 250px 50px;
        background-position: 0px center;
        border-radius: 100%;
        opacity: 0.7;
    }

    .rating [type=radio]+.img:hover {
        opacity: 1;
    }

    .rating [type=radio]+.img.img2 {
        background-position: -50px center;
    }

    .rating [type=radio]+.img.img3 {
        background-position: -100px center;
    }

    .rating [type=radio]+.img.img4 {
        background-position: -150px center;
    }

    .rating [type=radio]+.img.img5 {
        background-position: -200px center;
    }

    /* CHECKED STYLES */
    .rating [type=radio]:checked+.img {
        box-shadow: 0px 0px 1px 3px #8a69ae;
        opacity: 1;
    }

    /* .stopwatch {
            float: right;
            margin: 0px !important;
        } */
</style>
@endsection

@section('breadcrumb')
@parent
<li>Treatment Balance</li>
@endsection
@section('content')
<div class="loader hidden"></div>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <!-- <h4></h4> -->
                <!-- <h3 id="stopwatch" class="stopwatch">00:00:00</h3> -->
            </div>
            <form class="form-horizontal form-treatment-balance" data-toggle="validator" method="post">
                {{ csrf_field() }}
                <div class="box-body">
                    <input type="hidden" name="customer_id" value="{{ $invoice->customer_id }}">
                    <input type="hidden" name="invoice_id" value="{{ $invoice->id_invoice }}">
                    <!-- <input id="usage-time" name="time" type="hidden"> -->
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Patient</label>
                            <div class="col-md-10">
                                <p class="form-control-static">
                                    {{ $invoice->customer->full_name }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Phone</label>
                            <div class="col-md-10">
                                <p class="form-control-static">
                                    {{ $invoice->customer->phone }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Place, Date of Birth</label>
                            <div class="col-md-10">
                                <p class="form-control-static">
                                    {{ $invoice->customer->birth_place }}, {{ $invoice->customer->birth_date }}
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Outlet</label>
                            <div class="col-md-3">
                                <p class="form-control-static">
                                    <select id="outlet" onChange="chooseOutlet(this)" name="outlet" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                        <option selected="selected" disabled>-- Select Outlet --</option>
                                        @foreach ($outlet as $list)
                                        <option data-id="{{ $list->id_outlet }}"
                                            value="{{ $list->id_outlet }}"
                                            >{{ $list->outlet_name }}
                                        </option>
                                        @endforeach
                                    </select>
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Utilisation Date</label>
                            <div class="col-md-3">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="datepicker" name="utilisationdate" data=Y-m-d name="date">
                                </div>
                            </div>
                        </div>
                        <div class="form-group warehouse_option">
                            <label class="col-md-2 control-label">Warehouse</label>
                            <div class="col-md-3">
                                <p class="form-control-static">
                                    <select id="warehouse_id" onChange="getItemSellableStock(this)" name="warehouse_id" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                    </select>
                                </p>
                            </div>
                        </div>
                        <div class="form-horizontal">
                            <div class="col-md-12">
                                <div id="table-hidden" class="box box-default no-padding">
                                    <div class="table-responsive">
                                        <table class="table table-striped tabel-convert">
                                            <thead>
                                                <tr>
                                                    <th width="20%">Invoice Num</th>
                                                    <th width="20%">Item Name</th>
                                                    <th>Balance</th>
                                                    <th>DP Balance</th>
                                                    <th>Usage</th>
                                                    <th width="50px">Point</th>
                                                    <th>Nurse/Doctor</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                $no = 1;
                                                @endphp
                                                <input type="hidden" id="count" value="{{ count($product)}}">

                                                @foreach ($product as $listIndex => $list)
                                                <input type="hidden" name="save_json" id="save_json_{{$no}}" value="[]">
                                                @foreach ($list->invoice_detail as $invoice_detail)
                                                @php
                                                foreach ($invoice_detail->invoice as $invoice)
                                                {
                                                $warehouse = $invoice->warehouse_id;
                                                }
                                                @endphp
                                                @endforeach
                                                @php
                                                if (substr($list->product_treatment->product_treatment_code, 0, 1) == 'T') {
                                                $type = 'Treatment';
                                                }else{
                                                $type = 'Product';
                                                }
                                                @endphp
                                                <input type="hidden" id="product_{{$no}}" name="product_id[]" value="{{ $list->product_id }}" data-type="{!! $type !!}">
                                                <input type="hidden" name="inv_package_id[]" value="{{ $list->id_invoice_package }}">
                                                <input type="hidden" name="anamnesa_detail_id[]" value="{{ $list->anamnesa_detail_id }}">
                                                <input type="hidden" name="durasi[]" value="{{ $list->product_treatment->duration }}">
                                                <tr>
                                                    @foreach ($list->invoice_detail as $invoice_detail)
                                                    @foreach ($invoice_detail->invoice as $invoice)
                                                    @php
                                                        $payAmount = 0;
                                                        $isPackage = substr($invoice_detail->package_code, 0, 3) === "PKG";
                                                        $package = $invoice->package;
                                                        $isOneKindItemPackage = $package ? count($package->package_detail) === 1 : false;
                                                        if($invoice->remaining > 0 && $isPackage)
                                                        {
                                                            foreach($invoice->payment as $pay)
                                                            {
                                                                $amount = str_replace(',', '',$pay->amount);
                                                                $payAmount += $amount;
                                                            }
                                                            $nominal = str_replace(',', '', $list->nominal);
                                                            $packItemQty = $list->qty + $list->used;

                                                            $totalItem = $nominal * $packItemQty;
                                                            $totalInvoice = str_replace (array('.', ','), "", $invoice->total);
                                                            $sumTotal = $totalInvoice;

                                                            $subDpPerItem = ((int)$payAmount / $sumTotal) * 100;

                                                            $totalDP = $totalItem - ((float)$subDpPerItem / 100) * $totalItem;

                                                            $sumDP = $totalItem - (float)$totalDP;
                                                            $sumQtyDP = floor($sumDP) /  str_replace(',', '', $list->nominal);
                                                            $balanceUsageOnDP = round($sumQtyDP) - $list->used;
                                                        }else{
                                                            $balanceUsageOnDP = $list->qty;
                                                        }
                                                    @endphp
                                                    <td><input type="text" class="form-control" name="inv_code[]" readonly value="{{ $invoice->inv_code }}"></td>
                                                    @endforeach
                                                    @endforeach
                                                    <td>{{ $list->product_treatment->product_treatment_name }}</td>
                                                    <td>{{ $list->qty }}</td>
                                                    <td>{{ $balanceUsageOnDP }}</td>
                                                    <td>
                                                        <input type="hidden" id="balance_dp_{{ $no }}" value="{{$balanceUsageOnDP}}">
                                                        <input style="width:60px" type="number" name="usage[]" id="usage_{{$no}}" max="{{ $list->qty }}" rel="usage" class="form-control">
                                                    </td>
                                                    <td>
                                                        <select id="point" name="point[]">
                                                            <option value="0">0</option>
                                                            @foreach ($list->product_treatment->product_treatment_points as
                                                            $point)
                                                            <option value="{{ $point->point }}">{{ $point->point }}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select id="therapist" name="therapist_{{ $list->id_invoice_package }}[]" class="form-control select2 select2-hidden-accessible" multiple="" data-placeholder="Select a Therapist" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                            @foreach ($therapist as $therapistList)
                                                            <option value="{{ $therapistList->id_therapist }}">
                                                                {{ $therapistList->therapist_name }}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                </tr>
                                                @if (substr($list->product_treatment->product_treatment_code, 0, 1) == 'T')
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <strong>Ingridients</strong>
                                                    </td>
                                                    <td colspan="5">
                                                        <table class="table table-sm">
                                                            <thead>
                                                                <tr>
                                                                    <th>No</th>
                                                                    <th>Material</th>
                                                                    <th>Value</th>
                                                                    <th width="100px">Qty</th>
                                                                    <th width="80px">Aksi</th>
                                                                </tr>
                                                            </thead>

                                                            <tbody data-treatment-id="{{ $list->product_treatment->id_product_treatment }}" data-treatment-index="{{ $listIndex }}">
                                                            @php
                                                            $anamDetailBoms = [];
                                                            if (isset($list->invoice_detail[0]->anamnesa_detail)) {
                                                                $anamDetailBoms = $list->invoice_detail[0]->anamnesa_detail->boms;
                                                            }
                                                            @endphp
                                                            @if (count($anamDetailBoms) > 0)
                                                                    @foreach ($list->invoice_detail[0]->anamnesa_detail->boms as $matIndex => $material)
                                                                    <tr>
                                                                        <td>{{ $matIndex + 1 }}</td>
                                                                        <td>{{ $material->rawItem->name }}</td>
                                                                        <td>{{ $material->unit_value }}{{ $material->rawItem->uom->alias }}</td>

                                                                        <td align="right">
                                                                            <input onchange="check_stock({{ $no }})" class="form-control material" style="width: 100px" data-material-name="{{$material->rawItem->name}}" data-max-stock="{{$material->rawItem->mutations->sum('qty')}}" data-treatment-id="{{ $list->product_treatment->id_product_treatment }}" data-treatment-index="{{ $listIndex }}" data-material-id="{{ $material->raw_item_id }}" data-material-product-treatment-id="{{$material->rawItem->sales_information_id}}" type="text" rel="qty" value="{{ $material->qty }}.00">
                                                                        </td>
                                                                        <td>
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="col-md-6">
                                                                                        <input class="form-check-input fix" type="checkbox" value="1" id="fix" checked="true">
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <button class="btn btn-sm btn-danger" data-treatment-id="{{ $list->product_treatment->id_product_treatment}}" onclick="deleteMaterial(this, event, {{$matIndex}})">
                                                                                            <i class="fa fa-trash"></i>
                                                                                        </button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    @endforeach

                                                                @else
                                                                    @foreach ($list->product_treatment->item->boms as $matIndex => $material)
                                                                    <tr>
                                                                        <td>{{ $matIndex + 1 }}</td>
                                                                        <td>{{ $material->rawItem->name }}</td>
                                                                        <td>{{ $material->unit_value }}{{ $material->rawItem->uom->alias }}</td>

                                                                        <td align="right">
                                                                            <input onchange="checkItemStock(this)" class="form-control material" style="width: 100px" data-material-name="{{$material->rawItem->name}}" data-max-stock="{{$material->rawItem->mutations->sum('qty')}}" data-treatment-id="{{ $list->product_treatment->id_product_treatment }}" data-treatment-index="{{ $listIndex }}" data-material-id="{{ $material->raw_item_id }}" type="text" rel="qty" value="{{ $material->qty }}">
                                                                        </td>
                                                                        <td>
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="col-md-6">
                                                                                        <input class="form-check-input fix" type="checkbox" value="1" id="fix" checked="true">
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <button class="btn btn-sm btn-danger" data-treatment-id="{{ $list->product_treatment->id_product_treatment}}" onclick="deleteMaterial(this, event, {{$matIndex}})">
                                                                                            <i class="fa fa-trash"></i>
                                                                                        </button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    @endforeach
                                                            @endif
                                                            </tbody>

                                                            <tfoot>
                                                                <tr>
                                                                    <td colspan="5" align="center">
                                                                        <button class="btn btn-sm btn-info" style="margin-top: 8px" onclick="openForm(this, event)" data-treatment-id="{{ $list->product_treatment->id_product_treatment }}" data-treatment-index="{{ $listIndex }}">
                                                                            <i class="fa fa-plus"></i> Add Material
                                                                        </button>
                                                                    </td>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </td>
                                                </tr>
                                                @endif
                                                @php
                                                $no++;
                                                @endphp
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-12 text-center">
                            <label class="col-12 control-label">Patient Satisfaction</label>
                            <div class="col-12 rating">
                                <label>
                                    <input type="radio" name="rating" value="1">
                                    <div class="img img1" title="Tidak Puas"></div>
                                </label>

                                <label>
                                    <input type="radio" name="rating" value="2">
                                    <div class="img img2" title="Kurang Puas"></div>
                                </label>

                                <label>
                                    <input type="radio" name="rating" value="3">
                                    <div class="img img3" title="Biasa Saja"></div>
                                </label>

                                <label>
                                    <input type="radio" name="rating" value="4">
                                    <div class="img img4" title="Cukup Puas"></div>
                                </label>

                                <label>
                                    <input type="radio" name="rating" value="5">
                                    <div class="img img5" title="Puas"></div>
                                </label>
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label class="col-12 control-label">Remarks</label>
                            <div class="col-12">
                                <p class="form-control-static">
                                    <textarea name="remarks" id="remarks" rows="5" style="width: 90%;"></textarea>
                                </p>
                            </div>
                        </div>
                        <div class="form-group col-sm-6">
                            <label class="col-12 control-label">Sign Patient Here</label>
                            <div class="col-12">
                                <canvas id="signature-pad" class="signature-pad" width=400 height=200></canvas>
                            </div>
                            <input type="hidden" name="signature" id="signature" value="">
                        </div>
                    </div>
                    <div class="box-footer text-right">
                        <button class="btn btn-danger" type="button" id="clear"><i class="fa fa-trash-o" aria-hidden="true"></i> Clear</button>
                        <button class="btn btn-warning" type="button" id="undo"><i class="fa fa-arrow-left" aria-hidden="true"></i> Undo</button>
                        <button class="btn btn-primary" type="submit" id="save"><i class="fa fa-floppy-o" aria-hidden="true"></i>
                            Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">

                <form class="form-horizontal" data-toggle="validator" id="form_data">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> &times; </span> </button>
                        <h3 class="modal-title">Add Material</h3>
                    </div>

                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Material</label>
                            <div class="col-md-9">
                                <select id="material_id" name="material_id" class="form-control select2 select2-hidden-accessible" data-placeholder="Select a Material" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                    <option value="">Select Item</option>
                                    @foreach ($materials as $list)

                                    <option value="{{ $list->id }}" data-max-stock="{{$list->maxStock ?? 0}}" data-material-uom="{{ isset($list->uom) ? $list->uom->alias : 'NONE' }}" data-unit-value="{{ $list->unit_value }}">{{ $list->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="material_qty" class="col-md-3 control-label">Qty</label>
                            <div class="col-md-9">
                                <input id="material_qty" type="text" class="form-control" name="material_qty" rel="material_qty" value="1.00">
                                <span class="help-block with-errors"></span>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
                        <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Add
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    @endsection

    @section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
    <!-- <script src="{{ asset('js/signature_pad.js') }}"></script> -->
    <script>
        $('#datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });
        $('#datepicker').datepicker('setDate', 'today')
        // $(function(){
        var canvas = document.getElementById('signature-pad');
        var signaturePad = new SignaturePad(canvas, {});

        function populateSignature() {
            var signatur = signaturePad.toDataURL('image/png');
            if(signaturePad.isEmpty())
            {
                $('#signature').val('');
            }else{
                $('#signature').val(signatur);
            }
        }

        $(function() {
            // $('#stopwatch').stopwatch().stopwatch('start');
            populateSignature()

            let outletId = $("#outlet").val();
            console.log(outletId);

            if(outletId) {
                $.ajax({
                        url : `/invoice-create/${outletId}/chooseWarehouse`,
                        type: 'GET',
                        dataType: "JSON",
                        success: function(data) {
                            if(data.length <= 1)
                            {
                                var warehouse = '';
                                $.each(data, function (i, v) {
                                    warehouse += `<option selected value="${v.id_warehouse}">${v.name}</option>`;
                                });
                                $('#warehouse_id').html(warehouse);
                                $('.warehouse_option').hide();
                            }else{
                                var warehouse = '<option selected disabled>Select Warehouse</option>';
                                $.each(data, function (i, v) {
                                    warehouse += `<option  value="${v.id_warehouse}">${v.name}</option>`;
                                });
                                $('#warehouse_id').html(warehouse);
                                $('.warehouse_option').show();
                            }

                        },
                        error: function () {
                            swal({
                                title: `Setting your warehouse first`,
                                icon: 'error'
                            })
                        }
                });
            }

            $('.form-treatment-balance').validator().on('submit', function(e) {
                // $('#stopwatch').stopwatch().stopwatch('stop');
                // $('#usage-time').val($('#stopwatch').html());
                if (!e.isDefaultPrevented()) {
                    populateSignature()
                    $('#save').attr('disabled', 'disabled');
                    $('#save').html('<i class="fa fa-refresh fa-spin"></i> Memproses...');
                    let formData = $('.form-treatment-balance').serialize()
                    formData += '&materials=' + JSON.stringify(collectMaterials())
                    $.ajax({
                        url: "{{ route('usage-treatment.store') }}",
                        type: "POST",
                        data: formData,
                        success: function(data) {
                            $('#save').attr('disabled', 'disabled');
                            $('#save').html('<i class="fa fa-refresh fa-spin"></i> Memproses...');
                            $("#clear, #undo").prop('disabled', false);
                            swal({
                                    title: "Successfully Create Usage",
                                    icon: "success",
                                    buttons: ["Finish", "Print"],
                                    dangerMode: true,
                                })
                                .then((print) => {
                                    if (print) {
                                        window.open("/usage-treatment/" + data.id_usage + "/print");
                                        window.location.href = "{{ route('customers.show', $invoice->customer_id) }}";
                                    } else {
                                        window.location.href = "{{ route('customers.show', $invoice->customer_id) }}";
                                    }
                                });
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            swal(`Can not Save the Data`,`${jqXHR.responseJSON?.message || '' }`, {
                                button: false,
                                icon : "info",
                            }).then(() => {
                                $('#save').removeAttr('disabled');
                                $('#save').html('<i class="fa fa-save"></i> Save');
                            });
                        }
                    });
                    return false;
                }
            });
        });

        function checkItemStock(el) {

            const val = validDigits($(el).val());
            const maxStock = parseFloat($(el).data().maxStock);
            const itemName = $(el).data().materialName;

            // if (val > maxStock) {
            //     swal({
            //         title: `Error item ${itemName} lebih dari stock. stock:${maxStock}`,
            //         icon: 'error'
            //     })

            //     $(el).val(maxStock);
            // }
        }

        function collectMaterials() {
            let result = []
            let i = 0
            $('.form-control.material').each(function() {
                let qty = parseFloat($(this).val())
                let treatment_id = $(this).attr('data-treatment-id')
                let material_id = $(this).attr('data-material-id')
                let treatment_index = $(this).attr('data-treatment-index')
                let fix = $('.form-check-input.fix')[i].checked

                result.push({
                    treatment_id,
                    material_id,
                    qty,
                    treatment_index,
                    fix
                })

                i++
            })

            return result
        }

        //clearSignature
        document.getElementById('clear').addEventListener('click', function() {
            signaturePad.clear();
        });

        //undoSignature
        document.getElementById('undo').addEventListener('click', function() {
            var data = signaturePad.toData();
            if (data) {
                data.pop(); // remove the last dot or line
                signaturePad.fromData(data);
            }
        });

        let activeAddTreatment = null,
            activeTreatmentIndex = null;

        function openForm(el, e) {
            e.preventDefault();

            $('#modal-form').modal('show');

            activeAddTreatment = $(el).attr('data-treatment-id')
            activeTreatmentIndex = $(el).attr('data-treatment-index')
        }

        function chooseOutlet(e) {
            let elementId = $(e).attr('id');
            let $el = $(`#${elementId} option[value="${e.value}"]`);
            let id = $el.attr('data-id');
            $.ajax({
                url: `/invoice-create/${id}/chooseWarehouse`,
                type: 'GET',
                dataType: "JSON",
                beforeSend: function() {
                    $(".loader").removeClass('hidden');
                },
                success: function(data) {
                    $(".loader").addClass('hidden');
                    if (data.length <= 1) {
                        var warehouse = '';
                        $.each(data, function(i, v) {
                            warehouse += `<option data-id="${v.id_warehouse}" selected value="${v.id_warehouse}">${v.name}</option>`;
                            var i;
                            var count = $("#count").val();
                            for (i = 1; i <= count; ++i) {
                                let type = $(`#product_${i}`).attr('data-type').toLowerCase();
                                let elementId = $(`#product_${i}`).val();

                                let endpoint = `/api/v1/logistics/items/sales_informations/${type}/${elementId}/${v.id_warehouse}/sellables`;
                                let token = '{{ auth()->user()->api_token }}';
                                let no = `${i}`;

                                $.ajax({
                                    url: endpoint,
                                    type: 'GET',
                                    beforeSend: function(xhr) {
                                        xhr.setRequestHeader('Authorization', `Bearer ${token}`);
                                        $(".loader").removeClass('hidden');
                                    },
                                    success: function(res) {
                                        stockinform = res.sellable;
                                        $(`#save_json_${no}`).val(JSON.stringify(stockinform));
                                        $(".loader").addClass('hidden');
                                    }
                                });

                            }
                        });
                        $('#warehouse_id').html(warehouse);
                        $('.warehouse_option').hide();

                    } else {
                        var warehouse = '<option selected disabled>Select Warehouse</option>';
                        $.each(data, function(i, v) {
                            warehouse += `<option data-id="${v.id_warehouse}" value="${v.id_warehouse}">${v.name}</option>`;
                        });
                        $('#warehouse_id').html(warehouse);
                        $('.warehouse_option').show();
                    }

                },
                error: function() {
                    alert("Can not Show the Data!");
                }
            });
        }

        function getItemSellableStock(e) {
            let elementId1 = $(e).attr('id');
            let $el = $(`#${elementId1} option[value="${e.value}"]`);
            let id_warehouse = $el.attr('data-id');

            var i;
            var count = $("#count").val();
            for (i = 1; i <= count; ++i) {
                let type = $(`#product_${i}`).attr('data-type').toLowerCase();
                let elementId = $(`#product_${i}`).val();

                let endpoint = `/api/v1/logistics/items/sales_informations/${type}/${elementId}/${id_warehouse}/sellables`;
                let token = '{{ auth()->user()->api_token }}';
                let no = `${i}`;

                $.ajax({
                    url: endpoint,
                    type: 'GET',
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader('Authorization', `Bearer ${token}`);
                    },
                    success: function(res) {
                        stockinform = res.sellable;
                        $(`#save_json_${no}`).val(JSON.stringify(stockinform));
                    }
                });
            }
        }

        $("input[rel=usage]").bind('keyup', function(){
            check_stock(this.id.split('_')[1])
            // if(this.value){
            //     var temp = validDigits(this.value,2)
            //     this.value=addCommas(temp);
            // }
        })

        $("input[rel=qty], input[rel=material_qty]").bind('keyup', function(){
            if(this.value){
                var temp = validDigits(this.value,2)
                this.value=addCommas(temp);
            }
        })

        $("input[rel=qty], input[rel=material_qty]").on('blur', function(){
            if(this.value){
                var temp=parseFloat(validDigits(this.value))
                this.value=addCommas(temp.toFixed(2));
            }
        })

        function check_stock(id) {
            var data = JSON.parse($(`#save_json_` + id).val());
            let qty = validDigits($('#usage_' + id).val(),2);
            let material = $('.material');
            let stockError, stockValid = true;

            // if (data.type == 'service') {
            //     let err = [];

            //     data.sellable.forEach((product, index) => {
            //         material.each((i,e) => {
            //             let mat = $(e)
            //             let matId = mat.data().materialProductTreatmentId;
            //             let productQty = parseFloat(mat.val());
            //             if (product.sales_information_id == matId) {
            //                 let min = productQty * qty;
            //                 product.buffer_stock = product.buffer_stock || 0
            //                 if (min > parseFloat(product.mutations_count, 10)) {
            //                     let need = min - product.mutations_count;
            //                     err.push(`&#x25B6; ${product.name} diperlukan sebanyak ${need} unit lagi.`);
            //                     stockValid = false;
            //                 }

            //                 if (product.mutations_count <= product.buffer_stock) {
            //                     swal(product.name + ' tersedia ' + product.buffer_stock + ' unit.', {
            //                         dangerMode: true,
            //                         button: true,
            //                         showCancelButton: false,
            //                     });
            //                 }
            //             }
            //         })
            //     });

            //     err.push('<br>Silahkan isi kekurangan stok terlebih dahulu untuk melanjutkan.');
            //     stockError = err.join('<br>');
            // }

            // if (data.type == 'product') {
            //     data.buffer_stock = data.buffer_stock || 0
            //     if (data.sellable <= data.buffer_stock) {
            //         swal(data.name + ' tersedia ' + data.buffer_stock + ' unit.', {
            //             dangerMode: true,
            //             button: true,
            //         });
            //     }
            //     if (qty > parseFloat(data.sellable, 10)) {
            //         let need = qty - data.sellable;
            //         stockError = `${data.name} hanya tersedia ${data.sellable} unit.`
            //         stockValid = false;
            //     }
            // }

            // if (!stockValid) {
            //     var content = document.createElement("div");
            //     content.innerHTML = stockError;

            //     swal({
            //         title: "Stok Tidak Mencukupi.",
            //         content,
            //         icon: "error",
            //         button: true,
            //     });

            //     $('#usage_' + id).val(0);
            //     return false;
            // }
            check_balance(id)
        }
        function check_balance(id)
        {
            let qty_usage = $('#usage_' + id).val();
            let qty_dp = $('#balance_dp_' + id).val();
            if( parseFloat(qty_usage,10) >  parseFloat(qty_dp,10))
            {
                swal('Balance tidak mencukupi !', {
                    icon: "info",
                    button: false,
                });
                $('#usage_' + id).val(0);
                return false;
            }

        }
        $('#modal-form').submit(function(e) {
            e.preventDefault()

            let materialId = $('#material_id').val()
            let selectedEl = $(`#material_id option[value="${materialId}"]`)
            let uomId = selectedEl.attr('data-material-uom')
            let unitValue = selectedEl.attr('data-unit-value')
            let maxStock = parseFloat(selectedEl.attr('data-max-stock'))
            let materialName = selectedEl.text()
            let totalData = $(`tbody[data-treatment-id="${activeAddTreatment}"] > tr`).length
            let qty = validDigits($('#material_qty').val())

            if (qty > maxStock) {
                swal({
                    title: `Error item ${materialName} lebih dari stock. stock:${maxStock}`,
                    icon: 'error'
                })
                $('#material_qty').val(maxStock).change();
            } else {
                let isExist = false;

                $(`input[data-treatment-id="${activeAddTreatment}"]`).each(function() {
                    const thisData = $(this).data();
                    if (parseFloat(thisData.materialId) === parseFloat(materialId)) {
                        isExist = true;
                    }
                })
                if (!isExist) {
                    $(`tbody[data-treatment-id="${activeAddTreatment}"]`).append(`
                        <tr>
                            <td>${totalData+1}</td>
                            <td>${materialName}</td>
                            <td>${unitValue}${uomId}</td>
                            <td align="right">
                                <input class="form-control material" style="width: 100px" onchange="checkItemStock(this)" data-material-name="${materialName}" data-max-stock="${maxStock}" data-treatment-id="${activeAddTreatment}" data-material-id="${materialId}" data-treatment-index="${activeTreatmentIndex}" type="text" rel="qty" value="${qty}">
                            </td>
                            <td>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <input class="form-check-input fix" type="checkbox" value="1" id="fix" checked="true">
                                        </div>
                                        <div class="col-md-6">
                                            <button class="btn btn-sm btn-danger" data-treatment-id="${activeAddTreatment}" onclick="deleteMaterial(this, event, ${totalData})">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    `)
                } else {
                    swal({
                        title: `Item ${materialName} sudah ada ditambah`,
                        icon: 'error'
                    })
                }

            }


            $('#material_id').val('').change()
            $('#material_qty').val('1').change()
            $('#modal-form').modal('hide')
        })

        function deleteMaterial(el, e, id) {
            e.preventDefault()
            let rows = $(el).parent().parent().parent().parent().parent();

            let treatmentId = $(el).attr('data-treatment-id');
            // let rows = $(`tbody[data-treatment-id="${treatmentId}"] tr`)

            rows.remove();

            rows = $(`tbody[data-treatment-id="${treatmentId}"] tr`)
            if (rows.length == 0) {
                return false;
            }

            // re arrange index
            for (let i = 0; i < rows.length; i++) {
                let numCol = $(rows[i]).children('td:first-child')

                let numCol2 = $(rows[i]).children('td:last-child');
                numCol2.html(`<div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <input class="form-check-input fix" type="checkbox" value="1" id="fix" checked="true">
                                        </div>
                                        <div class="col-md-6">
                                            <button class="btn btn-sm btn-danger" data-treatment-id="${activeAddTreatment}" onclick="deleteMaterial(this, event, ${totalData})">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>`);
                numCol.text(i + 1)
            }
        }
    </script>
    @endsection

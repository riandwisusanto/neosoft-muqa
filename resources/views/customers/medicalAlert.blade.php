<div class="modal" id="modal-medical" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <form class="form-horizontal" data-toggle="validator" method="post">
                {{ csrf_field() }} {{ method_field('POST') }}

                <div class="modal-header bg-default">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times fa-md"></i>
                    </button>
                    <span style="font-size:20px; font-weight: bold;" class="modal-title-medical">MEDICAL ALERTS</span>
                    <p style="font-size:15px; font-weight: bold;"><i>Special attention for patient’s allergies.</i></p>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="idx" name="id">

                    <div class="form-group">
                        <div id="render"></div>
                    </div>

                    <script type="x-tmpl-mustache" id="add_item">
                        <div class="col-md-10 col-xs-10">
                            <label>Items</label>
                            <select class="form-control input-sm" name="medical" id="medical" style="width: 100%">
                                <option></option>
                                @foreach ($productTreatment as $list)
                                    <option value="{{ $list->id_product_treatment }}">{{ $list->product_treatment_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2 col-xs-2">
                            <label style="width:100%">Add</label>
                            <button type="button" class="btn btn-info btn-xs" id="AddItem"><i class="fa fa-plus"></i></button>
                        </div>

                        {% #item %}
                            <div id="data_{% _id %}">
                                <div class="col-md-10 col-xs-10">
                                    <label>Items ({% no %})</label>
                                    <input type="text" class="form-control" value="{% medical %}" id="medical_{% _id %}" name="medical"  disabled required>
                                    <input type="hidden" class="form-control" value="{% medical_id %}" id="medical_id_{% _id %}" name="medical_id"  disabled required>
                                </div>
                                <div class="col-md-2 col-xs-2">
                                    <label style="width:100%">Action</label>
                                    <button type="button" id="edit_{% _id %}" class="btn edit btn-success btn-xs" data-id="{% _id %}"><i class="fa fa-edit"></i></button>
                                    <button type="button" id="delete_{% _id %}" class="btn delete btn-danger btn-xs" data-id="{% _id %}"><i class="fa fa-trash"></i></button>
                                </div>
                            </div>

                            <div class="form hidden" id="form_{% _id %}">
                                <div id="data_{% _id %}">
                                    <div class="col-md-10 col-xs-10">
                                        <label>Items ({% no %})</label>
                                        <select class="form-control input-sm" name="medical" id="medicalx_{% _id %}" style="width: 100%">
                                            <option></option>
                                            @foreach ($productTreatment as $list)
                                                <option value="{{ $list->id_product_treatment }}">{{ $list->product_treatment_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2 col-xs-2">
                                        <label style="width:100%">Action</label>
                                        <button type="button" id="save_{% _id %}" class="btn save btn-primary btn-xs" data-id="{% _id %}"><i class="fa fa-floppy-o"></i></button>
                                        <button type="button" id="cancel_{% _id %}" class="btn cancel btn-warning btn-xs" data-id="{% _id %}"><i class="fa fa-ban"></i></button>
                                    </div>
                                </div>
                            </div>
                        {% /item %}
                    </script>

                    <div class="form-group" style="padding: 20px">
                        <label for="">Allergies Notes</label>
                        <textarea class="form-control" name="restriction" id="restriction"></textarea>
                    </div>

                    <input type="hidden" name="save_json" id="save_json" value="[]">

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Save </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                            class="fa fa-arrow-circle-left"></i> Cancel</button>
                </div>

            </form>

        </div>
    </div>
</div>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('app.name') }} POS</title>
  <link rel="icon" href="{{ asset('img/logodelovely.jpeg') }}" type="image/x-icon">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('font-awesome/css/font-awesome.min.css') }}">
  {{-- select --}}
  <link rel="stylesheet" href="{{ asset('plugins/select2/select2.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/css/skins/_all-skins.min.css') }}">
  <!-- Morris chart -->
  {{-- <link rel="stylesheet" href="{{ asset('plugins/morris/morris.css') }}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}"> --}}
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{ asset('plugins/datepicker/datepicker3.css') }}">
  <link rel='stylesheet' href="{{ asset('datetimepicker/css/bootstrap-datetimepicker.min.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">

  <link rel="stylesheet" href="{{ asset('plugins/DataTables/DataTables/css/dataTables.bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/DataTables/responsive/css/responsive.bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/DataTables/fixedHeader/css/fixedHeader.bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/DataTables/fixedHeader/css/fixedHeader.dataTables.min.css') }}">

  <link rel="stylesheet" type="text/css" href="{{asset('dist/dropify/css/dropify.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('dist/dropify/css/dropify.min.css')}}">

  <link href='{{ asset('css/style.css') }}' rel='stylesheet' />
  <link href='{{ asset('css/fontGoogle.css') }}' rel='stylesheet' />
  @yield('style')
</head>

<body class="hold-transition skin-black-light skin-neosoft sidebar-mini">
  <div class="wrapper" id="neosoft-app">

    <header class="main-header">
      <!-- Logo -->
      <a href="#" class="logo bg-neopurple text-neowhite neoborder-right">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>POS</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>{{ config('app.name') }}</b>POS</span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top bg-neopurple">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle text-neowhite neoborder-right" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle text-neowhite neoborder-left" data-toggle="dropdown">
                <span>{{auth()->user()->name}}</span>
              </a>
            </li>
            <!-- Messages: style can be found in dropdown.less-->
            @include('notification')
            <!-- Control Sidebar Toggle Button -->
            <li>
              <a href="#" data-toggle="control-sidebar" class="neoborder-left"><i class="fa fa-gears text-neowhite"></i></a>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    @include('sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <button type="button" class="btn btn-default" onclick="window.history.go(-1); return false;"><i
              class="fa fa-arrow-circle-left"></i> Back</button>
          
        </h1>
        <ol class="breadcrumb">
          @section('breadcrumb')
          <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          @show
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        @yield('content')
      </section>
      <!-- /.content -->
    </div>

    <!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        {{-- <b>Version</b> 1.0.0 --}}
      </div>
      <strong>Copyright &copy; 2019 <a href="https://www.neosoft.co.id/home/">PT NEOSOFT TEKNOLOGI ASIA</a>.</strong> All rights
      reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <div class="tab-content">
        <div class="text-right">


          <a href="{{ route('logout') }}"
            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <button type="button" class="external-event btn btn-primary">

              <i class="fa fa-sign-out fa-fw"></i>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
              </form>
              Logout
            </button>
          </a>

          <a href="{{ route('useraccess.change-password') }}">
            <button type="button" class="external-event btn btn-danger"><i class="fa fa-refresh"></i> Change
              Password</button>
          </a>
        </div>
        <div class="tab-pane" id="control-sidebar-home-tab"></div>

      </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <!-- jQuery 3 -->
  <script src="{{ asset('plugins/jQuery/jquery.min.js') }}"></script>
  <!-- jquery debounce -->
  <script src="{{ asset('js/debounce.js') }}"></script>
  {{-- DataTables --}}
  <script src="{{ asset('plugins/DataTables/DataTables/js/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('plugins/DataTables/DataTables/js/dataTables.bootstrap.min.js') }}"></script>
  <script src="{{ asset('plugins/DataTables/responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('plugins/DataTables/responsive/js/responsive.bootstrap.min.js') }}"></script>
  {{-- <script src="https://unpkg.com/tooltip.js/dist/umd/tooltip.min.js"></script> --}}
  {{-- <script src="{{ asset('plugins/morris/morris.min.js') }}"></script> --}}
  <!-- jQuery UI 1.11.4 -->
  <script src="{{ asset('plugins/jQueryUI/jquery-ui.min.js') }}"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  {{-- <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script> --}}
  <!-- Bootstrap 3.3.7 -->
  <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
  <!-- Sparkline -->
  {{-- <script src="{{ asset('plugins/sparkline/jquery.sparkline.min.js') }}"></script>
  <!-- jvectormap -->
  <script src="{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
  <script src="{{ asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script> --}}
  <!-- jQuery Knob Chart -->
  {{-- <script src="{{ asset('plugins/knob/jquery.knob.js') }}"></script> --}}
  <!-- daterangepicker -->
  <script src="{{ asset('plugins/daterangepicker/moment.min.js') }}"></script>
  <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
  <!-- datepicker -->
  <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
  <script src="{{ asset('datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
  <!-- Slimscroll -->
  <script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
  <!-- FastClick -->
  <script src="{{ asset('plugins/fastclick/fastclick.min.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <!-- AdminLTE for demo purposes -->
  <script src="{{ asset('js/validator.js') }}"></script>
  <script src="{{ asset('plugins/sweetalert/sweetalert.min.js') }}"></script>
  <script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
  <script src="{{ asset('js/mustache.js') }}"></script>

  <script src="{{asset('dist/dropify/js/drop.js')}}"></script>
  <script src="{{asset('dist/dropify/js/dropify.js')}}"></script>
  <script src="{{asset('dist/dropify/js/dropify.min.js')}}"></script>


  <script type="text/javascript">
    $('[data-toggle="tooltip"]').tooltip({container: 'body'});   

    var url = window.location;
    
    // for sidebar menu entirely but not cover treeview
    $('ul.sidebar-menu a').filter(function() {
        return this.href == url;
    }).parent().siblings().removeClass('active').end().addClass('active');

    // for treeview
    $('ul.treeview-menu a').filter(function() {
        return this.href == url;
    }).parentsUntil(".sidebar-menu > .treeview-menu").siblings().removeClass('active').end().addClass('active');

    $('select:not(.normal)').each(function () {
        $(this).select2({
            placeholder: "Please Select",
            width: '100%',
            dropdownParent: $(this).parent()
        });
    });

    function addCommas(n){
      var rx=  /(\d+)(\d{3})/;
      return String(n).replace(/^\d+/, function(w){
        while(rx.test(w)){
          w= w.replace(rx, '$1,$2');
        }
        return w;
      });
    }

    function validDigits(n, dec){
      n= n.replace(/[^\d\.]+/g, '');
      var ax1= n.indexOf('.'), ax2= -1;
      if(ax1!= -1){
        ++ax1;
        ax2= n.indexOf('.', ax1);
        if(ax2> ax1) n= n.substring(0, ax2);
        if(typeof dec=== 'number') n= n.substring(0, ax1+dec);
      }
      return n;
    }
  </script>
  @yield('script')


</body>

</html>

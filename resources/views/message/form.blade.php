<div class="modal" id="modal-message" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <form class="form-horizontal" id="form_data" data-toggle="validator" method="post" enctype="multipart/form-data">
                {{ csrf_field() }} {{ method_field('POST') }}
                <!-- @csrf -->


                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times fa-lg"></i>
                    </button>
                    <h3 class="modal-title"></h3>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Customer</label>
                        <div class="col-md-6">
                            <select class="form-control select2 select2-hidden-accessible" multiple="multiple"
                                data-placeholder=" Select a Customer" name="customer_id[]" id="customer_id"
                                tabindex="-1" aria-hidden="true" required>
                                @foreach ($customer as $list)
                                    @php
                                    $rank = rank($list->id_customer);
                                    @endphp
                                    <option value="{{ $list->id_customer }}">{{ $list->full_name }} - {{ $rank->name ?? '' }} - {{$list->birth_date}} - {{$list->point}}</option>
                                @endforeach
                                
                                @foreach($customer1 as $list)
                                    @php
                                        $rank = rank($list->id_customer);
                                    @endphp
                                    <option id="option1" value="{{ $list->id_customer }}">{{ $list->full_name }} - {{ $rank->name ?? '' }} - {{$list->birth_date}} - {{$list->point}}</option>
                                @endforeach
                                @foreach($customer2 as $list)
                                    @php
                                        $rank = rank($list->id_customer);
                                    @endphp
                                    <option id="option2"  value="{{ $list->id_customer }}">{{ $list->full_name }} - {{ $rank->name ?? '' }} - {{$list->birth_date}} - {{$list->point}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                            <input id="chkall" onclick="selected_all()" type="checkbox"> Select All
                        </div>
                    </div> -->
                   
                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                            <input id="check1" onclick="select_1()" type="checkbox"> Select 1 (1-900)
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                            <input id="check2" onclick="select_2()" type="checkbox"> Select 2 (901-1800)
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Image</label>
                        <div class="col-md-6">
                            <input type="file" name="image" class="form-control" accept="image/*" id="fileName" onchange="Filevalidation()" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="desc" class="col-md-3 control-label">Message</label>
                        <div class="col-md-6">
                            <textarea type="text" class="form-control" id="message" name="message" placeholder="Message"
                                autocomplete="off" rows="5" required></textarea>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                            class="fa fa-arrow-circle-left"></i> Cancel</button>
                </div>

            </form>

        </div>
    </div>
</div>
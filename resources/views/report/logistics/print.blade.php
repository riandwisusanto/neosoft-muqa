<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Print PDF</title>

    <style>
        body {
            font-family: sans-serif !important;
            font-size: 11px;
        }

        table {
            width: 100%;
            border-collapse: separate;
            border-spacing: 0px;
            border-color: 1px solid #000;
        }

        table th,
        table td {
            padding: 4px;
        }

        table td {
            vertical-align: top;
        }

    </style>

</head>

<body>
    <div style="text-align: center" >
        <h1>LAPORAN STOK BARANG</h1>
        <h2>{{ $outlet }}</h2>
        <h3>TANGGAL: {{ $from }} sampai {{ $to }}</h3>
    </div>


    <h3>Inventory Location: {{ $outlet }}</h3>
    <table border="1" >
        <thead>
        <tr>
            <th>No</th>
            <th>Nama Barang</th>
            <th>Stock Awal</th>
            <th>Barang Masuk</th>
            <th>Barang Keluar</th>
            <th>Penyesuaian (-)</th>
            <th>Penyesuaian (+)</th>
            <th>Stock akhir</th>
        </tr>
        </thead>
        <tbody>
            @foreach($logistics as $index => $list)
                <tr>
                    <td>{{ $index+1 }}</td>
                    <td>{{ $list->name }}</td>
                    <td>{{ $list->stock_begin  }}</td>
                    <td>{{ $list->stock_in  }}</td>
                    <td>{{ $list->stock_out  }}</td>
                    <td>{{ $list->stock_adj_in  }}</td>
                    <td>{{ $list->stock_adj_out  }}</td>
                    <td>{{ $list->stock_result  }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

</body>

</html>


@extends('base')

@section('style')
    <link rel="stylesheet"
          href="{{ asset('plugins/DataTables/buttons/css/buttons.dataTables.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('plugins/DataTables/buttons/css/buttons.bootstrap.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
    <style>
        div.dataTables_wrapper div.dataTables_processing {
            position: relative;
            border: 0;
        }

        .btn-date-custom {
            border: none;
            background-color: transparent !important;
            font-size: 16px;
        }

        .btn-date-custom:hover {
            background-color: transparent !important;
            cursor: default;
        }

        .btn-date-custom:active {
            outline: none !important;
            box-shadow: none !important;
        }

    </style>
@endsection

@section('breadcrumb')
    @parent
    <li>Report</li>
    <li>Stock Logistics</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                <span style="font-size:22px;">Stock Logistics <button type="button" id="btn_from" class="btn
                btn-default" style="margin-left:20px;">{{ $from }}</button> - <button type="button"
                                                                                                                                                            id="btn_to" class="btn btn-default">{{ $to }}</button></span>
                    <p style="font-size:15px;"><i>stock logistics list repot.</i></p>
                </div>
                <div class="box-header with-border">
                    <a onclick="periodeForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Change Period</a>
                    <a onclick="printPDF()" class="btn btn-success"><i class="fa fa-print"></i> Print PDF</a>
                </div>

                <div class="box-body">
                    <h3>Stock Logistic Report List</h3>

                    <table class="table table-striped table-bordered nowrap table-invoice-detail" style="width:100%;">
                        <thead>
                        <tr>
                            <th>NO</th>
                            <th>NAMA BARANG</th>
                            <th>STOCK AWAL</th>
                            <th>BARANG MASUK</th>
                            <th>BARANG KELUAR</th>
                            <th>PENYESUAIAN (+)</th>
                            <th>PENYESUAIAN (-)</th>
                            <th>STOK AKHIR</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @include('report.logistics.form')
@endsection

@section('script')
    <script rel="stylesheet"
            src="{{ asset('plugins/DataTables/buttons/js/dataTables.buttons.min.js') }}"></script>
    <script rel="stylesheet"
            src="{{ asset('plugins/DataTables/buttons/js/buttons.flash.min.js') }}"></script>
    <script rel="stylesheet" src="{{ asset('plugins/DataTables/JSZip/jszip.min.js') }}"></script>
    <script rel="stylesheet" src="{{ asset('plugins/DataTables/pdfmake/pdfmake.min.js') }}">
    </script>
    <script rel="stylesheet" src="{{ asset('plugins/DataTables/pdfmake/vfs_fonts.js') }}"></script>
    <script rel="stylesheet"
            src="{{ asset('plugins/DataTables/buttons/js/buttons.html5.min.js') }}"></script>
    <script rel="stylesheet"
            src="{{ asset('plugins/DataTables/buttons/js/buttons.print.min.js') }}"></script>
    <script rel="stylesheet"
            src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}">
    </script>
    <script rel="stylesheet"
            src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}">
    </script>

    <script type="text/javascript">
        var table;

        // $('#from, #to').on('keyup change', function () {
        //     $('#from, #to').each(function () {
        //         this.value = this.value.replace(/[^0-9]/g, '/');
        //     });
        // });

        var d = new Date();
        var currMonth = d.getMonth();
        var currYear = d.getFullYear();
        var startDate = new Date(currYear, currMonth, 1);

        // $("#from").datepicker();
        // $("#from").datepicker("setDate", startDate);

        $('#to').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true
        });

        $('#from').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true
        })

        $(function () {
            const d = new Date();
            const n = d.toLocaleDateString();
            $('#from').change(function (data) {
                $('#btn_from').html(data.target.value.replace(/[^0-9]/g, '/'));
            });

            $('#to').change(function (data) {
                $('#btn_to').html(data.target.value.replace(/[^0-9]/g, '/'));
            });

            $('#from').change(function (data) {
                $('#btn_from').html(data.target.value.replace(/[^0-9]/g, '/'));
            });

            $('#to').change(function (data) {
                $('#btn_to').html(data.target.value.replace(/[^0-9]/g, '/'));
            });

            $('#modal-form form').validator().on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    $('#from, #to').each(function () {
                        this.value = this.value.replace(/[^0-9]/g, '-');
                    });
                    table.ajax.url("{{ route('index.logistics.show') }}?" + $(
                        '#modal-form form').serialize()).load();
                    $("#modal-form").modal('hide');
                }
                return false;
            });

            table = $('.table-invoice-detail').DataTable({
                language: {
                    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'
                },
                scrollY: "500px",
                scrollX: true,
                scrollCollapse: true,
                paging: false,
                bSort: false,
                dom: 'Bfrtip',
                buttons: [{
                    extend: 'excelHtml5',
                    title: 'STOCK-LOGISTICS-' + n
                }],
                "processing": true,
                "serverside": true,
                "ajax": {
                    "url": "{{ route('index.logistics.show') }}",
                    "type": "GET"
                }
            });
        });

        function selected_all() {
            if ($("#chkall").is(':checked')) {
                $("#outlet > option").prop("selected", "selected");
                $("#outlet").trigger("change");
            } else {
                $("#outlet").find('option').prop("selected", false);
                $("#outlet").trigger("change");
            }
        }

        function selected_all2() {
            if ($("#chkall2").is(':checked')) {
                $("#status > option").prop("selected", "selected");
                $("#status").trigger("change");
            } else {
                $("#status").find('option').prop("selected", false);
                $("#status").trigger("change");
            }
        }

        function periodeForm() {
            $('#modal-form').modal('show');
        }

        function printPDF() {
            from = $('#from').val();
            to = $('#to').val();
            warehouse = $('#warehouse').val();
            link = '/report/logistics/print?from='+from+'&to='+to+'&warehouse=' + warehouse
            window.open(link, "_blank")
        }

    </script>
@endsection

@extends('base')

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/buttons/css/buttons.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/buttons/css/buttons.bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
<style>
    div.dataTables_wrapper div.dataTables_processing {
        position: relative;
        border: 0;
    }

    .btn-date-custom {
        border: none;
        background-color: transparent !important;
        font-size: 16px;
    }

    .btn-date-custom:hover {
        background-color: transparent !important;
        cursor: default;
    }

    .btn-date-custom:active {
        outline: none !important;
        box-shadow: none !important;
    }
</style>
@endsection


@section('breadcrumb')
@parent
<li>Report</li>
<li>Item</li>
<li>Sold Sales</li>
@endsection

@section('content')
<div class="loader"></div>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-sm-8">
                        <span style="font-size:20px; font-weight: bold;">ITEM SOLD BY SALES</span>
                        <p style="font-size:15px; font-weight: bold;"><i>Sales report for sold treatment/product/packages.</i></p>
                    </div>
                    <div class="col-sm-4">
                        <div class="btn-float-right">
                            <button type="button" class="btn btn-default btn-date-custom">
                                <span id="btn_from">{{ $from }}</span>&nbsp;&nbsp;-&nbsp;&nbsp;<span id="btn_to">{{ $to }}</span>
                            </button>
                            <a onclick="periodeForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Change Period</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box-body">
                <h3>Invoice Detail</h3>
                <table class="table table-striped table-bordered nowrap table-invoice-detail">
                    <thead>
                        <tr>
                            <th>Invoice Code</th>
                            <th>Date</th>
                            <th>Outlet</th>
                            <th>Customer Ref</th>
                            <th>Customer</th>
                            <th>Age</th>
                            @if (array_intersect(["OUTLET_SUPERVISOR"], json_decode(Auth::user()->level)))
                            <th>Phone</th>
                            <th>Address</th>
                            @endif
                            <th>Received By</th>
                            <th>Source By</th>
                            <th>Activity</th>
                            <th>Served 1</th>
                            <th>Served 2</th>
                            <th>SKU Code</th>
                            <th>Item Name</th>
                            <th>Item Type</th>
                            <th>Price</th>
                            <th>Disc</th>
                            <th>Qty</th>
                            <th>Total Price</th>
                            <th>Remarks</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@include('report.item-sold.form')
@endsection

@section('script')
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/dataTables.buttons.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/buttons.flash.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/JSZip/jszip.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/pdfmake/pdfmake.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/pdfmake/vfs_fonts.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/buttons.html5.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/buttons.print.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}">
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>

<script type="text/javascript">
    var table, from, to;
    
$('#from, #to').on('keyup change',function(){
    $('#from, #to').each(function(){
        this.value=this.value.replace(/[^0-9]/g,'/');
    });
});

$('#from, #to').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true
});
$(function(){

    $('#from').change(function (data) {
        $('#btn_from').html(data.target.value.replace(/[^0-9]/g,'/'));
    });

    $('#to').change(function (data) {
        $('#btn_to').html(data.target.value.replace(/[^0-9]/g,'/'));
    });
    $(".loader").addClass('hidden');

    var d = new Date();
    var n = d.toLocaleDateString();
    table = $('.table-invoice-detail').DataTable( {
        language: {processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'},
        scrollY:        "500px",
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,
        bSort:false,
        dom: 'Bfrtip',
        fixedColumns:   {
            leftColumns: 1,
            rightColumns: 1
        },
        buttons: [
            {
                extend: 'excelHtml5',
                title: 'Item Sold by Sales Outlet '+n
            }
        ],

        "processing" : true,
        "serverside" : true,
        "ajax" : {
            "url" : "{{ route('report.item.sold.outlet') }}",
            "type" : "GET"
            // "data" : {'_token' : $('meta[name=csrf-token]').attr('content')}
        }
    });



    $('#modal-form form').validator().on('submit', function(e){
        if(!e.isDefaultPrevented()){
            $('#from, #to').each(function(){
                this.value=this.value.replace(/[^0-9]/g,'-');
            });
        //   table.ajax.url("{{ route('report.item.sold.outlet') }}?"+$('#modal-form form').serialize()).load();
            $.ajax({
                type: "POST",
                url: "{{ route('report.item.sold.outlet') }}",
                data: $('#modal-form form').serialize(),
                beforeSend: function() {
                    $(".loader").removeClass('hidden');
                },
                success: function(data) {
                    table.clear();
                    table.rows.add(data.data);
                    table.draw();
                    $(".loader").fadeOut('slow');
                }
            });
            $("#modal-form").modal('hide');
            return false;
        }
    });
});



function selected_all() {
  if($("#chkall").is(':checked')){
      $("#item > option").prop("selected", "selected");
      $("#item").trigger("change");
  } else {
      $("#item").find('option').prop("selected", false);
      $("#item").trigger("change");
  }
}

function periodeForm(){
   $('#modal-form').modal('show');
}

</script>
@endsection

<div class="modal" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="form-horizontal" data-toggle="validator">
                {{ csrf_field() }}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true"> &times; </span> </button>
                    <h3 class="modal-title">Periode Report</h3>
                </div>

                <div class="modal-body">

                    <div class="form-group">
                        <label for="from" class="col-md-2 control-label">Date From</label>
                        <div class="col-md-8">
                            <input id="from" type="text" class="form-control" name="from" value="{{ $from }}" autofocus required>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="to" class="col-md-2 control-label">Date To</label>
                        <div class="col-md-8">
                            <input id="to" type="text" class="form-control" name="to" value="{{ $to }}" autofocus required>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Item (s)</label>
                        <div class="col-md-8">
                            <select id="item" name="item[]" class="form-control select2 select2-hidden-accessible"
                                    multiple="multiple" data-placeholder="Select a Item" style="width: 100%;"
                                    tabindex="-1" aria-hidden="true" required>
                                @for ($i = 0; $i < $count; $i++)
                                    @php
                                        if (substr($item[$i]->product_treatment_code, 0, 1) == 'T') {
                                            $type = 'Treatment';
                                        }elseif(substr($item[$i]->product_treatment_code, 0, 1) == 'P'){
                                            $type = 'Product';
                                        }else{
                                            $type = 'Package';
                                        }

                                        if($item[$i]->package_name){
                                            $name = $item[$i]->package_name;
                                            $id = $item[$i]->package_code;
                                        }else{
                                            $name = $item[$i]->product_treatment_name;
                                            $id = $item[$i]->product_treatment_code;
                                        }

                                    @endphp
                                    <option value="{{ $id }}">{{ $name }} ({{ $type }})</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label"></label>
                        <div class="col-md-8">
                            <input id="chkall" onclick="selected_all()" type="checkbox">Select All

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Outlet</label>
                        <div class="col-md-8">
                            <select id="outlet" name="outlet" class="form-control select2 select2-hidden-accessible"
                                    data-placeholder="Select a Outlet" style="width: 100%;"
                                    tabindex="-1" aria-hidden="true" required>
                                @foreach ($sold_outlet as $list)
                                    <option value="{{ $list->id_outlet }}">{{ $list->outlet_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    {{-- <div class="form-group">
                        <label class="col-md-2 control-label"></label>
                        <div class="col-md-8">
                            <input id="chkall" onclick="selected_all()" type="checkbox">Select All

                        </div>
                    </div> --}}
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Report
                    </button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal"><i
                            class="fa fa-arrow-circle-left"></i> Cancel</button>
                </div>

            </form>

        </div>
    </div>
</div>

<div class="modal" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <form class="form-horizontal" data-toggle="validator" id="form_data">
                {{ csrf_field() }}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true"> &times; </span> </button>
                    <h3 class="modal-title">Periode Report</h3>
                </div>

                <div class="modal-body">

                    <div class="form-group">
                        <label for="from" class="col-md-3 control-label">Date From</label>
                        <div class="col-md-6">
                            <input id="from" type="text" class="form-control" name="from" value="{{ $from }}" autofocus required>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="to" class="col-md-3 control-label">Date To</label>
                        <div class="col-md-6">
                            <input id="to" type="text" class="form-control" name="to" value="{{ $to }}" autofocus required>
                            <span class="help-block with-errors"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Outlet</label>
                        <div class="col-md-6">
                            <select id="outlet" name="outlet[]" class="form-control select2 select2-hidden-accessible"
                                    multiple="multiple" data-placeholder="Select a Outlet" style="width: 100%;"
                                    tabindex="-1" aria-hidden="true" required>
                                @foreach ($outlet as $list)
                                    <option value="{{ $list->id_outlet }}">{{ $list->outlet_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                            <input id="chkall" onclick="selected_all()" type="checkbox">Select All

                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Report
                    </button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal"><i
                            class="fa fa-arrow-circle-left"></i> Cancel</button>
                </div>

            </form>

        </div>
    </div>
</div>

@extends('base')

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/buttons/css/buttons.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/buttons/css/buttons.bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">

@endsection
@section('breadcrumb')
@parent
<li>Report</li>
<li>Void</li>
<li>Outlet</li>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <span style="font-size:22px;">Void Invoice by Outlet <button type="button" id="btn_from"
                        class="btn btn-default" style="margin-left:20px;">{{ $from }}</button> - <button type="button"
                        id="btn_to" class="btn btn-default">{{ $to }}</button></span>
                <p style="font-size:15px;"><i>Laporan void Invoice klinik dengan filter bedasaarkan outlet.</i></p>
            </div>
            <div class="box-header with-border">
                <a onclick="periodeForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Change Period</a>
            </div>
            <div class="box-body">
                <h3>Void Invoice</h3>
                <table class="table nowrap table-invoice-void" style="width:100%">
                    <thead>
                        <tr>
                            <th>Invoice Code</th>
                            <th>Payment Date</th>
                            <th>Type</th>
                            <th>Outlet</th>
                            <th>Customer Ref</th>
                            <th>Customer</th>
                            <th>Received By</th>
                            <th>Payment Type</th>
                            <th>Served 1</th>
                            <th>Served 2</th>
                            <th>Source</th>
                            <th>Amount</th>
                            <th>Paid</th>
                            <th>Void Date</th>
                            <th>Void By</th>
                            <th>Void Remarks</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>

        </div>
    </div>
</div>

@include('report.void-invoice.form')

@endsection

@section('script')
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/dataTables.buttons.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/buttons.flash.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/JSZip/jszip.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/pdfmake/pdfmake.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/pdfmake/vfs_fonts.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/buttons.html5.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/buttons.print.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}">
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>

<script type="text/javascript">
    var table_void, from, to;

    $('#from, #to').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true
    });

    $(function () {
        $('#from').change(function (data) {
            $('#btn_from').html(data.target.value);
        });

        $('#to').change(function (data) {
            $('#btn_to').html(data.target.value);
        });

        $('#modal-form form').validator().on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                table_void.ajax.url("{{ route('void-invoice.collection') }}?"+$('#modal-form form').serialize()).load();
                $("#modal-form").modal('hide');
            }
            return false;
        });
        var d = new Date();
        var n = d.toLocaleDateString();
        table_void = $('.table-invoice-void').DataTable( {
                language: {processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'},
                scrollY:        "500px",
                scrollX:        true,
                scrollCollapse: true,
                paging:         false,
                bSort:false,
                dom: 'Bfrtip',
                fixedColumns:   {
                    leftColumns: 1
                },
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'Detail-VIO-'+n
                    }
                ],
                "processing" : true,
                "serverside" : true,
                "ajax" : {
                    "url" : "{{ route('void-invoice.collection') }}",
                    "type" : "GET"
                }
            });

    });


    function selected_all() {
        if ($("#chkall").is(':checked')) {
            $("#outlet > option").prop("selected", "selected");
            $("#outlet").trigger("change");
        } else {
            $("#outlet").find('option').prop("selected", false);
            $("#outlet").trigger("change");
        }
    }

    function periodeForm() {
        $('#modal-form').modal('show');
    }

</script>
@endsection

@extends('base')

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/buttons/css/buttons.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/buttons/css/buttons.bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
<style>
    div.dataTables_wrapper div.dataTables_processing {
        position: relative;
        border: 0;
    }

    .btn-date-custom {
        border: none;
        background-color: transparent !important;
        font-size: 16px;
    }

    .btn-date-custom:hover {
        background-color: transparent !important;
        cursor: default;
    }

    .btn-date-custom:active {
        outline: none !important;
        box-shadow: none !important;
    }

    .DTFC_LeftBodyLiner {
        overflow-x: hidden;
    }
</style>
@endsection

@section('breadcrumb')
@parent
<li>Report</li>
<li>Treatment Done</li>
<li>Outlet</li>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-sm-8">
                        <span style="font-size:20px; font-weight: bold;">Treatment Done By Outlet</span>
                        <p style="font-size:15px;"><i>Treatment report based on outlet.</i></p>
                    </div>
                    <div class="col-sm-4">
                        <div class="btn-float-right">
                            <button type="button" class="btn btn-default btn-date-custom">
                                <span id="btn_from">{{ $from }}</span>&nbsp;&nbsp;-&nbsp;&nbsp;<span id="btn_to">{{ $to }}</span>
                            </button>
                            <a onclick="periodeForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Change Period</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-header">
                                <h3>Material/Ingredients</h3>
                            </div>
                            <div class="card-body">
                                <table class="table table-striped table-bordered nowrap table-material" style="width:100%;">
                                    <thead>
                                        <tr>
                                            <th style="width: 18px;">No</th>
                                            <th>Item</th>
                                            <th>Use Qty</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <h3>Invoice Detail</h3>
                <table class="table table-striped table-bordered nowrap table-invoice-detail">
                    <thead>
                        <tr>
                            <th>Usage Code</th>
                            <th>Usage Date</th>
                            <!-- <th>Invoice Code</th> -->
                            <th>Invoice Date</th>
                            <th>Outlet</th>
                            <th>{{ __('text.customer') }} Ref</th>
                            <th>{{ __('text.customer') }}</th>
                            <th>Created By</th>
                            <th>Sku Code</th>
                            <th>Item Name</th>
                            <th>Item Type</th>
                            <th>Used Value</th>
                            <th>Use Qty</th>
                            <th>Balance Value</th>
                            <th>Bal Qty</th>
                            <th>Duration</th>
                            <th>Point</th>
                            <th>Commision DrSpKK</th>
                            <th>Commision DrGP</th>
                            <th>Commision DrSpBP</th>
                            <th>Commision DrAnastesi</th>
                            <th>Commision Nurse</th>
                            <th>Remarks</th>
                            <th>Material Name</th>
                            <th>Material Qty</th>
                            <th>Material UOM</th>
                            <th>Material Price</th>
                            <th>Total</th>
                            <th>Nurse/Doctor</th>
                            <th>Nurse OnLoop</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

            </div>

        </div>
    </div>
</div>

@include('report.done.outlet.treatment.form')
@endsection

@section('script')
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/dataTables.buttons.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/buttons.flash.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/JSZip/jszip.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/pdfmake/pdfmake.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/pdfmake/vfs_fonts.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/buttons.html5.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/buttons.print.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}">
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>

<script type="text/javascript">
    var tableCollection, tableInvoiceDetail, tableMaterial, from, to;

    $('#from, #to').on('keyup change', function() {
        $('#from, #to').each(function() {
            this.value = this.value.replace(/[^0-9]/g, '/');
        });
    });

    $('#from, #to').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $(function() {

        $('#from').change(function(data) {
            $('#btn_from').html(data.target.value.replace(/[^0-9]/g, '/'));
        });

        $('#to').change(function(data) {
            $('#btn_to').html(data.target.value.replace(/[^0-9]/g, '/'));
        });

        var d = new Date();
        var n = d.toLocaleDateString();
        $('#modal-form form').validator().on('submit', function(e) {
            if (!e.isDefaultPrevented()) {
                $('#from, #to').each(function() {
                    this.value = this.value.replace(/[^0-9]/g, '-');
                });
                tableInvoiceDetail.ajax.url("{{ route('treatment-done.outlet.detail') }}?" + $('#modal-form form').serialize()).load();
                tableMaterial.ajax.url("{{ route('treatment-done.outlet.details.material') }}?" + $('#modal-form form').serialize()).load();

                $("#modal-form").modal('hide');
            }
            return false;
        });
        tableInvoiceDetail = $('.table-invoice-detail').DataTable({
            processing: true,
            language: {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'
            },
            serverSide: true,
            scrollY: "500px",
            scrollX: true,
            scrollCollapse: true,
            paging: false,
            bSort: false,
            dom: 'Bfrtip',
            fixedColumns: {
                leftColumns: 1,
                rightColumns: 1
            },
            buttons: [{
                    extend: 'excelHtml5',
                    title: 'TDO-' + n,
                    text: 'Export to Excel'
                },
                {
                    extend: 'pdf',
                    title: `TDO-` + n,
                    orientation: 'landscape',
                    customize: function(doc) {
                        doc.defaultStyle.fontSize = 6;
                        doc.styles.tableHeader.fontSize = 7;
                    },
                    text: 'Export to PDF'
                }
            ],
            "ajax": {
                "url": "{{ route('treatment-done.outlet.detail') }}",
                "type": "GET"
            }
        });

        tableMaterial = $('.table-material').DataTable({
            processing: true,
            language: {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'
            },
            serverSide: true,
            scrollY: "500px",
            scrollX: true,
            scrollCollapse: true,
            paging: false,
            bSort: false,
            dom: 'Bfrtip',
            buttons: [{
                    extend: 'excelHtml5',
                    title: 'TMI-' + n,
                    text: 'Export to Excel'
                },
                {
                    extend: 'pdf',
                    title: `TMI-` + n,
                    orientation: 'landscape',
                    text: 'Export to PDF'
                }
            ],
            "ajax": {
                "url": "{{ route('treatment-done.outlet.details.material') }}",
                "type": "GET"
            }
        });

    });

    function selected_all() {
        if ($("#chkall").is(':checked')) {
            $("#outlet > option").prop("selected", "selected");
            $("#outlet").trigger("change");
        } else {
            $("#outlet").find('option').prop("selected", false);
            $("#outlet").trigger("change");
        }
    }

    function periodeForm() {
        $('#modal-form').modal('show');
    }
</script>
@endsection

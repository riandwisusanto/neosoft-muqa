@extends('base')

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/buttons/css/buttons.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/buttons/css/buttons.bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">

@endsection
@section('breadcrumb')
@parent
<li>Report</li>
<li>Collection</li>
<li>Outlet</li>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <span style="font-size:22px;">Collection by Invoice <button type="button" id="btn_from"
                        class="btn btn-default" style="margin-left:20px;">{{ $from }}</button> - <button type="button"
                        id="btn_to" class="btn btn-default">{{ $to }}</button></span>
                <p style="font-size:15px;"><i>Laporan pendapatan klinik dengan filter bedasaarkan Invoice.</i></p>
            </div>
            <div class="box-header with-border">
                <a onclick="periodeForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Change Period</a>
                <!-- <a onclick="printPDF()" class="btn btn-success"><i class="fa fa-print"></i> Print PDF</a> -->
            </div>
            <div class="box-body">
                <h3>Collection By Invoice</h3>
                <table class="table table-bordered nowrap table-invoice-detail"  style="width:100%">
                    <thead>
                        <tr>
                            <th>NOFAK</th>
                            <th>STATUS</th>
                            <th>CUST. | NO.MEMBER</th>
                            <th>TANGGAL</th>
                            <th>SKU</th>
                            <th>ITEM</th>
                            <th>QTY</th>
                            <th>HARGA</th>
                            <th>DISC</th>
                            <th>DISC.RP</th>
                            <th>TOTAL</th>
                            <th>BAYAR</th>
                            <th>TERAPIS</th>
                            <!-- <th>PRODUK</th>
                            <th>QTY</th>
                            <th>HARGA</th>
                            <th>DISC.RP</th>
                            <th>DISC</th>
                            <th>TOTAL</th> -->
                            <th>CS</th>
                            <th>BYR</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                    <tfoot>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <!-- <th>PRODUK</th>
                        <th>QTY</th>
                        <th>HARGA</th>
                        <th>DISC.RP</th>
                        <th>DISC</th>
                        <th>TOTAL</th> -->
                        <th></th>
                        <th></th>
                        </tfoot>
                </table>
                <br>
                <div class="row">
                    <!-- <div class="col-sm-4">
                        <div class="card">
                            <div class="card-body">
                                <table class="table table-bordered nowrap table-invoice-subtotal">
                                    <thead>    
                                        <tr>
                                            <th>Subtotal</th>
                                            <th>Harga</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>    
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-sm-4">
                        <div class="card">
                            <div class="card-body">
                                <table class="table table-bordered nowrap table-invoice-grandtotal">
                                    <thead>
                                        <tr>
                                            <th>Jenis Pembayaran</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="card">
                            <div class="card-body">
                                <table class="table table-bordered nowrap table-invoice-total">
                                    <thead>
                                        <tr>
                                            <th>Jml Customer</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody style="text-align: right"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('report.collection.outlet.form')
@endsection

@section('script')
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/dataTables.buttons.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/buttons.flash.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/JSZip/jszip.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/pdfmake/pdfmake.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/pdfmake/vfs_fonts.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/buttons.html5.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/buttons.print.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}">
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>

<script type="text/javascript">
    var table_tot, table_balance, table, from, to;

    $('#from, #to').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true
    });

    $(function () {

        function addCommas(nStr)
        {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }

        $('#from').change(function (data) {
            $('#btn_from').html(data.target.value);
        });

        $('#to').change(function (data) {
            $('#btn_to').html(data.target.value);
        });

        $('#modal-form form').validator().on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                table.ajax.url("{{ route('detail.collection.invoice') }}?" + $('#modal-form form').serialize()).load();
                // tablesub.ajax.url("{{ route('subtotal.collection') }}?" + $('#modal-form form').serialize()).load();
                tablegrandtotal.ajax.url("{{ route('grandtotal.collection.invoice') }}?" + $('#modal-form form').serialize()).load();
                tabletotal.ajax.url("{{ route('total.collection.invoice') }}?" + $('#modal-form form').serialize()).load();
                $("#modal-form").modal('hide');
            }
            return false;
        });
        var d = new Date();
        var n = d.toLocaleDateString();
        table = $('.table-invoice-detail').DataTable({
            language: {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'
            },
            scrollY: "500px",
            scrollX: true,
            scrollCollapse: true,
            paging: false,
            bSort: false,
            dom: 'Bfrtip',
            fixedColumns: {
                leftColumns:1
            },
            buttons: [{
                extend: 'excelHtml5',
                title: 'Detail-CBO-' + n
            }],
            "processing": true,
            "serverside": true,
            "ajax": {
                "url": "{{ route('detail.collection.invoice') }}",
                "type": "GET"
            },
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
    
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    // console.log({ i })

                    if (typeof i === 'string' && i.indexOf('br') > -1) {
                        let total = 0;
                        let vals = i.split('<br>')
                        for (let x in vals) {
                            total += parseFloat(vals[x].replaceAll(',', ''))
                        }

                        return total
                    }

                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                harga_product = api
                    .column( 10 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                paid = api
                    .column( 11 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // // Update harga product
                $( api.column( 10 ).footer() ).html(
                    ' Rp. ' + addCommas(harga_product)
                );
                // // Update harga product
                $( api.column( 11 ).footer() ).html(
                    ' Rp. ' + addCommas(paid)
                );
            }
        });

        // tablesub = $('.table-invoice-subtotal').DataTable({
        //     language: {
        //         processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'
        //     },
        //     scrollY: "500px",
        //     scrollX: false,
        //     scrollCollapse: true,
        //     paging: false,
        //     bSort: false,
        //     dom: 'Bfrtip',
        //     fixedColumns: {
        //         leftColumns:1
        //     },
        //     buttons: [{
        //         extend: 'excelHtml5',
        //         title: 'SubTotal-CBO-' + n
        //     }],
        //     "processing": true,
        //     "serverside": true,
        //     "ajax": {
        //         "url": "{{ route('subtotal.collection') }}",
        //         "type": "GET"
        //     }
        // });

        tablegrandtotal = $('.table-invoice-grandtotal').DataTable({
            language: {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'
            },
            scrollY: "500px",
            scrollX: false,
            scrollCollapse: true,
            paging: false,
            bSort: false,
            dom: 'Bfrtip',
            fixedColumns: {
                leftColumns:1
            },
            buttons: [{
                extend: 'excelHtml5',
                title: 'GrandTotal-CBO-' + n
            }],
            "processing": true,
            "serverside": true,
            "ajax": {
                "url": "{{ route('grandtotal.collection.invoice') }}",
                "type": "GET"
            }
        });

        tabletotal = $('.table-invoice-total').DataTable({
            language: {
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'
            },
            scrollY: "500px",
            scrollX: false,
            scrollCollapse: true,
            paging: false,
            bSort: false,
            dom: 'Bfrtip',
            fixedColumns: {
                leftColumns:1
            },
            buttons: [{
                extend: 'excelHtml5',
                title: 'Total-CBO-' + n
            }],
            "processing": true,
            "serverside": true,
            "ajax": {
                "url": "{{ route('total.collection.invoice') }}",
                "type": "GET"
            }
        });
    });

    function printPDF() {
        from = $('#from').val();
        to = $('#to').val();
        outlet = $('#outlet').val();
        link = '/collection/invoice/print-pdf?from='+from+'&to='+to+'&outlet='+outlet
        window.open(link, "_blank")
    }

    function selected_all() {
        if ($("#chkall").is(':checked')) {
            $("#outlet > option").prop("selected", "selected");
            $("#outlet").trigger("change");
        } else {
            $("#outlet").find('option').prop("selected", false);
            $("#outlet").trigger("change");
        }
    }

    function periodeForm() {
        $('#modal-form').modal('show');
    }

</script>
@endsection

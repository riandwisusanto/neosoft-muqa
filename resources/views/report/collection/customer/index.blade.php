@extends('base')

@section('style')
    <link rel="stylesheet"
          href="{{ asset('plugins/DataTables/buttons/css/buttons.dataTables.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('plugins/DataTables/buttons/css/buttons.bootstrap.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
    <style>
        div.dataTables_wrapper div.dataTables_processing {
            position: relative;
            border: 0;
        }

        .btn-date-custom {
            border: none;
            background-color: transparent !important;
            font-size: 16px;
        }

        .btn-date-custom:hover {
            background-color: transparent !important;
            cursor: default;
        }

        .btn-date-custom:active {
            outline: none !important;
            box-shadow: none !important;
        }

    </style>
@endsection

@section('breadcrumb')
    @parent
    <li>Report</li>
    <li>Collection by Customer</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-sm-8">
                            <span style="font-size:20px; font-weight: bold;">Collection by Customer</span>
                            <p style="font-size:15px; font-weight: bold;"><i>collection by customer report list.</i></p>
                        </div>
                        <div class="col-sm-4">
                            <div class="btn-float-right">
                                <button type="button" class="btn btn-default btn-date-custom">
                                    <span id="btn_from">{{ $from }}</span>&nbsp;&nbsp;-&nbsp;&nbsp;<span
                                        id="btn_to">{{ $to }}</span>
                                </button>
                                <a onclick="periodeForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i>
                                    Change
                                    Period</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <h3>Summary</h3>
                    <table id="summmary-customer" class="table table-striped table-bordered nowrap" style="width: 40%">
                        <thead>
                        <tr>
                            <th>Status</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <h3>Collection by Customer Report List</h3>
                    <table class="table table-striped table-bordered nowrap table-invoice-detail" style="width:100%;">
                        <thead>
                        <tr>
                            <th>NO</th>
                            <th>CUSTOMER</th>
                            @if (array_intersect(["OUTLET_SUPERVISOR"], json_decode(Auth::user()->level)))
                            <th>PHONE</th>
                            <th>ADDRESS</th>
                            @endif
                            <th>JOIN DATE</th>
                            <th>SALES SUMMARY (Rp)</th>
                            <th>OUTSTANDING PAYMENT (Rp)</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <h3>Collection by Marketing Source</h3>
                    <table class="table table-striped table-bordered nowrap table-marketing-source" style="width:100%;">
                        <thead>
                        <tr>
                            <th>NO</th>
                            <th>CUSTOMER</th>
                            @if (array_intersect(["OUTLET_SUPERVISOR"], json_decode(Auth::user()->level)))
                            <th>PHONE</th>
                            <th>ADDRESS</th>
                            @endif
                            <th>MARKETING SOURCE</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @include('report.collection.customer.form')
@endsection

@section('script')
    <script rel="stylesheet"
            src="{{ asset('plugins/DataTables/buttons/js/dataTables.buttons.min.js') }}"></script>
    <script rel="stylesheet"
            src="{{ asset('plugins/DataTables/buttons/js/buttons.flash.min.js') }}"></script>
    <script rel="stylesheet" src="{{ asset('plugins/DataTables/JSZip/jszip.min.js') }}"></script>
    <script rel="stylesheet" src="{{ asset('plugins/DataTables/pdfmake/pdfmake.min.js') }}">
    </script>
    <script rel="stylesheet" src="{{ asset('plugins/DataTables/pdfmake/vfs_fonts.js') }}"></script>
    <script rel="stylesheet"
            src="{{ asset('plugins/DataTables/buttons/js/buttons.html5.min.js') }}"></script>
    <script rel="stylesheet"
            src="{{ asset('plugins/DataTables/buttons/js/buttons.print.min.js') }}"></script>
    <script rel="stylesheet"
            src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}">
    </script>
    <script rel="stylesheet"
            src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}">
    </script>

    <script type="text/javascript">
        var table, tableSummary, tableMarketing;

        $('#from, #to').on('keyup change', function () {
            $('#from, #to').each(function () {
                this.value = this.value.replace(/[^0-9]/g, '/');
            });
        });

        $('#from, #to').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true
        });

        $(function () {
            const d = new Date();
            const n = d.toLocaleDateString();
            $('#from').change(function (data) {
                $('#btn_from').html(data.target.value.replace(/[^0-9]/g, '/'));
            });

            $('#to').change(function (data) {
                $('#btn_to').html(data.target.value.replace(/[^0-9]/g, '/'));
            });

            $('#from').change(function (data) {
                $('#btn_from').html(data.target.value.replace(/[^0-9]/g, '/'));
            });

            $('#to').change(function (data) {
                $('#btn_to').html(data.target.value.replace(/[^0-9]/g, '/'));
            });

            $('#modal-form form').validator().on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    $('#from, #to').each(function () {
                        this.value = this.value.replace(/[^0-9]/g, '-');
                    });
                    tableSummary.ajax.url("{{ route('index.summary.customer.show') }}?" + $("#modal-form form").serialize()).load()
                    table.ajax.url("{{ route('index.collection.customer.show') }}?" + $(
                        '#modal-form form').serialize()).load();
                    tableMarketing.ajax.url("{{ route('index.marketing.customer.show') }}?" + $("#modal-form form").serialize()).load()
                    $("#modal-form").modal('hide');
                }
                return false;
            });

            tableSummary = $("#summmary-customer").DataTable({
                language: {
                    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'
                },
                scrollY: "500px",
                scrollX: true,
                scrollCollapse: true,
                paging: false,
                bSort: true,
                dom: 'Bfrtip',
                buttons: [{
                    extend: 'excelHtml5',
                    title: 'SUMMARY-CUSTOMER' + n
                }],
                "processing": true,
                "serverside": true,
                "ajax": {
                    "url": "{{ route('index.summary.customer.show') }}",
                    "type": "GET"
                }
            })

            table = $('.table-invoice-detail').DataTable({
                language: {
                    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'
                },
                scrollY: "500px",
                scrollX: true,
                scrollCollapse: true,
                paging: false,
                bSort: true,
                dom: 'Bfrtip',
                buttons: [{
                    extend: 'excelHtml5',
                    title: 'COLLECTION-CUSTOMER' + n
                }],
                "processing": true,
                "serverside": true,
                "ajax": {
                    "url": "{{ route('index.collection.customer.show') }}",
                    "type": "GET"
                }
            });

            tableMarketing = $('.table-marketing-source').DataTable({
                language: {
                    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'
                },
                scrollY: "500px",
                scrollX: true,
                scrollCollapse: true,
                paging: false,
                bSort: true,
                dom: 'Bfrtip',
                buttons: [{
                    extend: 'excelHtml5',
                    title: 'MARKETING-CUSTOMER' + n
                }],
                "processing": true,
                "serverside": true,
                "ajax": {
                    "url": "{{ route('index.marketing.customer.show') }}",
                    "type": "GET"
                }
            });
        });

        function selected_all() {
            if ($("#chkall").is(':checked')) {
                $("#outlet > option").prop("selected", "selected");
                $("#outlet").trigger("change");
            } else {
                $("#outlet").find('option').prop("selected", false);
                $("#outlet").trigger("change");
            }
        }

        function selected_all2() {
            if ($("#chkall2").is(':checked')) {
                $("#status > option").prop("selected", "selected");
                $("#status").trigger("change");
            } else {
                $("#status").find('option').prop("selected", false);
                $("#status").trigger("change");
            }
        }


        function periodeForm() {
            $('#modal-form').modal('show');
        }

    </script>
@endsection

@extends('base')

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/buttons/css/buttons.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/buttons/css/buttons.bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
<style>
    div.dataTables_wrapper div.dataTables_processing {
        position: relative;
        border: 0;
    }

    .btn-date-custom {
        border: none;
        font-size: 16px;
    }

    .btn-date-custom:hover {
        background-color: transparent !important;
        cursor: default;
    }

    .btn-date-custom:active {
        outline: none !important;
        box-shadow: none !important;
    }
</style>
@endsection


@section('breadcrumb')
@parent
<li>Report</li>
<li>Collection</li>
<li>Payment Type</li>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-sm-8">
                        <span style="font-size:20px; font-weight: bold;">COLLECTION BY PAYMENT TYPE</span>
                        <p style="font-size:15px; font-weight: bold;"><i>Collection report based on payment methods.</i></p>
                    </div>
                    <div class="col-sm-4">
                        <div class="btn-float-right">
                            <button type="button" class="btn btn-default btn-date-custom">
                                <span id="btn_from">{{ $from }}</span>&nbsp;&nbsp;-&nbsp;&nbsp;<span id="btn_to">{{ $to }}</span>
                            </button>
                            <a onclick="periodeForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Change Period</a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="bankTable" class="box-body">
                <h3>Collection By Payment Type</h3>
                <table class="table table-striped table-bordered nowrap table-outlet">
                    <thead>
                        <tr>
                            <th>Outlet</th>
                            <th>Num Payment</th>
                            <th>Sales Amount</th>
                            <th>Paid Amount</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="box-body">
                <h3>Detail Collection By Payment Type</h3>
                <table class="table table-striped table-bordered nowrap table-payment">
                    <thead>
                        <tr>
                            <th>Payment Code</th>
                            <th>Payment Name</th>
                            <th>Num Payment</th>
                            <th>Paid Amount</th>
                            <th>Balance Amount</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>

            <div class="box-body">
                <h3>Collection Payment Detail</h3>
                <table class="table table-striped table-bordered nowrap table-invoice-detail" style="width: 100% !important;">
                    <thead>
                        <tr>
                            <th>Invoice Code</th>
                            <th>Izzibook</th>
                            <th>Payment Date</th>
                            <th>Outlet</th>
                            <th>Customer Ref</th>
                            <th>Customer</th>
                            <th>Received By</th>
                            <th>Payment Type</th>
                            <th>Served 1</th>
                            <th>Served 2</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>


        </div>
    </div>
</div>

@include('report.collection.payment.form')
@endsection

@section('script')
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/dataTables.buttons.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/buttons.flash.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/JSZip/jszip.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/pdfmake/pdfmake.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/pdfmake/vfs_fonts.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/buttons.html5.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/buttons.print.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}">
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>

<script type="text/javascript">
    var table, from, to, tablePayment, tableInvoiceDetail, tableOutlet;

        $('#from, #to').on('keyup change',function(){
            $('#from, #to').each(function(){
                this.value=this.value.replace(/[^0-9]/g,'/');
            });
        });

        $('#from, #to').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true
        });

        $(function(){
            $('#from').change(function (data) {
                $('#btn_from').html(data.target.value.replace(/[^0-9]/g,'/'));
            });

            $('#to').change(function (data) {
                $('#btn_to').html(data.target.value.replace(/[^0-9]/g,'/'));
            });
            
            $('#modal-form form').validator().on('submit', function(e){
                if(!e.isDefaultPrevented()){
                    $('.btn-save').html('<i class="fa fa-spinner fa-spin"></i>Memproses...');
                    $('#from, #to').each(function(){
                        this.value=this.value.replace(/[^0-9]/g,'-');
                    });
                    // tableOutlet.ajax.url("{{ route('bank.collection.payment') }}?"+$('#modal-form form').serialize()).load();
                    tablePayment.ajax.url( "{{ route('payment_type.collection.payment') }}?"+$('#modal-form form').serialize()).load();
                    tableInvoiceDetail.ajax.url( "{{ route('detail.collection.payment') }}?"+$('#modal-form form').serialize()).load();
                    $("#modal-form").modal('hide');
                }
                $('.btn-save').html('<i class="fa fa-save"></i>Report');
                return false;
            });
            var d = new Date();
            var n = d.toLocaleDateString();

            tablePayment = $('.table-payment').DataTable( {
                language: {processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'},
                scrollY:        "500px",
                scrollX:        true,
                scrollCollapse: true,
                paging:         false,
                bSort:false,
                dom: 'Bfrtip',
                fixedColumns:   {
                    leftColumns: 1
                },
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'CBP-'+n
                    }
                ],
                "searching": false,
                "processing" : true,
                "serverside" : true,
                "ajax" : {
                    "url" : "{{ route('payment_type.collection.payment') }}",
                    "type" : "GET"
                }
            });

          
            tableInvoiceDetail = $('.table-invoice-detail').DataTable( {
                language: {processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'},
                scrollY:        "500px",
                scrollX:        true,
                scrollCollapse: true,
                paging:         false,
                bSort:false,
                dom: 'Bfrtip',
                fixedColumns:   {
                    leftColumns: 1
                },
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'Detail-CBP-'+n
                    }
                ],
                "processing" : true,
                "serverside" : true,
                "ajax" : {
                    "url" : "{{ route('detail.collection.payment') }}",
                    "type" : "GET"
                }
            });

            tableOutlet = $('.table-outlet').DataTable( {
                language: {processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'},
                scrollY:        "500px",
                scrollX:        true,
                scrollCollapse: true,
                paging:         false,
                bSort:false,
                dom: 'Bfrtip',
                fixedColumns:   {
                    leftColumns: 1
                },
                buttons: [
                    {
                        extend: 'excelHtml5',
                        title: 'CBP-'+n
                    }
                ],
                "searching": false,
                "processing" : true,
                "serverside" : true,
                "ajax" : {
                    "url" : "{{ route('bank.collection.payment') }}",
                    "type" : "GET"
                }
            });
        });

        function selected_all() {
            if($("#chk_bank").is(':checked')){
                $("#bank > option").prop("selected", "selected");
                $("#bank").trigger("change");
            } else {
                $("#bank").find('option').prop("selected", false);
                $("#bank").trigger("change");
            }

            if($("#chk_filter").is(':checked')){
                $("#filter > option").prop("selected", "selected");
                $("#filter").trigger("change");
            } else {
                $("#filter").find('option').prop("selected", false);
                $("#filter").trigger("change");
            }
        }

        function periodeForm(){
            $('#modal-form').modal('show');
        }

</script>
@endsection

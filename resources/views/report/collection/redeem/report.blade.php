@extends('base')

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/buttons/css/buttons.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/buttons/css/buttons.bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">

@endsection
@section('breadcrumb')
@parent
<li>Report</li>
<li>Collection</li>
<li>Outlet</li>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <span style="font-size:22px;">Collection by Redeem Point <button type="button" id="btn_from"
                        class="btn btn-default" style="margin-left:20px;">{{ $from }}</button> - <button type="button"
                        id="btn_to" class="btn btn-default">{{ $to }}</button></span>
            </div>
            <div class="box-header with-border">
                <a onclick="periodeForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Change Period</a>
                <!-- <a onclick="printPDF()" class="btn btn-success"><i class="fa fa-print"></i> Print PDF</a> -->
            </div>
            <div class="box-body">
                <h3>Collection By Redeem Point Detail</h3>
                <table class="table table-bordered nowrap table-invoice-point-detail"  style="width:100%">
                    <thead>
                        <tr>
                            <th>NOFAK</th>
                            <th>CUST. | NO.MEMBER</th>
                            <th>OUTLET</th>
                            <th>TANGGAL & WAKTU</th>
                            <th>PRODUCT</th>
                            <th>POINT</th>
                            <th>QTY</th>
                            <th>TOTAL POINT</th>
                            <th>GRAND TOTAL POINT</th>
                            <th>STAFF</th>
                            <th>REMARKS</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>

@include('report.collection.redeem.form')
@endsection

@section('script')
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/dataTables.buttons.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/buttons.flash.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/JSZip/jszip.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/pdfmake/pdfmake.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/pdfmake/vfs_fonts.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/buttons.html5.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/buttons.print.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}">
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>

<script type="text/javascript">
    var table_tot, table_balance, table, from, to;

    $('#from, #to').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true
    });

    $(function () {
        $('#from').change(function (data) {
            $('#btn_from').html(data.target.value);
        });

        $('#to').change(function (data) {
            $('#btn_to').html(data.target.value);
        });

        $('#modal-form form').validator().on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                table.ajax.url("{{ route('detail.collection.redeemPoint') }}?" + $('#modal-form form').serialize()).load();
                $("#modal-form").modal('hide');
            }
            return false;
        });
        var d = new Date();
        var n = d.toLocaleDateString();

        table = $('.table-invoice-point-detail').DataTable({
                "processing": true,
                "serverSide": true,
                language: {
                    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'
                },
                scrollCollapse: true,
                scrollY: "500px",
                scrollX: true,
                bSort: false,
                dom: 'Blfrtip',
                lengthMenu: [[25,50,100, -1], [25,50, 100, "All"]],

                buttons: [{
                    extend: 'excelHtml5',
                    title: 'Detail-CBO-' + n,
                }],

                "iDisplayLength": 25,

                "ajax": {
                    "url": "{{ route('detail.collection.redeemPoint') }}",
                    "type": "GET"
                }
            });
    });

    // function printPDF() {
    //     from = $('#from').val();
    //     to = $('#to').val();
    //     outlet = $('#outlet').val();
    //     link = '/collection/outlet/print-pdf?from='+from+'&to='+to+'&outlet='+outlet
    //     window.open(link, "_blank")
    // }

    function selected_all() {
        if ($("#chkall").is(':checked')) {
            $("#outlet > option").prop("selected", "selected");
            $("#outlet").trigger("change");
        } else {
            $("#outlet").find('option').prop("selected", false);
            $("#outlet").trigger("change");
        }
    }

    function periodeForm() {
        $('#modal-form').modal('show');
    }

</script>
@endsection

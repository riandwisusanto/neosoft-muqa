<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Print PDF</title>
    <style>
        body {
            font-family: sans-serif !important;
            font-size: 11px;
        }

        h1, h4 {
            text-align: center;
            margin: 0px;
        }

        h1 {
            margin-bottom: 5px;
        }

        table {
            width: 100%;
            border-collapse: separate;
            border-spacing: 0px;
            border-color: 1px solid #000;
        }

        table th,
        table td {
            padding: 4px;
        }

        table td {
            vertical-align: top;
        }

        #invoice-grandtotal,
        #invoice-grandtotal {
            margin-right: 3.50%;
        }

        #invoice-grandtotal,
        #invoice-grandtotal,
        #invoice-total {
            float: left;
            width: 31%;
        }

        .sparator {
            width: 100%;
            height: 31px;
            clear: both;
        }
    </style>
</head>
<body>
    <h1>Collection by Outlet Report</h1>
    <h4>{{ $outlet }}: Period {{ date('d/m/Y', strtotime($from)) }} - {{ date('d/m/Y', strtotime($to)) }}</h4>
    
    <div class="sparator"></div>

    <table border="1px">
        <thead>
            <tr>
                <th>NOFAK</th>
                <th>CUST. | NO.MEMBER</th>
                <th>TANGGAL</th>
                <th>TREATMENT</th>
                <th>HARGA</th>
                <th>DISC</th>
                <th>DISC.RP</th>
                <th>TOTAL</th>
                <th>TERAPIS</th>
                <th>PRODUK</th>
                <th>QTY</th>
                <th>HARGA</th>
                <th>DISC.RP</th>
                <th>DISC</th>
                <th>TOTAL</th>
                <th>CS</th>
                <th>BYR</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $list)
            <tr>
                @foreach ($list as $value)
                <td>{{ $value }}</td>
                @endforeach
            </tr>
            @endforeach
        </tbody>
    </table>

    <div class="sparator"></div>

    <div id="invoice-grandtotal">
        <table border="1px">
            <thead>
                <tr>
                    <th>Subtotal</th>
                    <th>Harga</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data2 as $list)
                <tr>
                    @foreach ($list as $value)
                    <td>{{ $value }}</td>
                    @endforeach
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div id="invoice-grandtotal">
        <table border="1px">
            <thead>
                <tr>
                    <th>Jenis Pembayaran</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data3 as $list)
                <tr>
                    @foreach ($list as $value)
                    <td>{{ $value }}</td>
                    @endforeach
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div id="invoice-total">
        <table border="1px">
            <thead>
                <tr>
                    <th>Jml Customer</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data4 as $list)
                <tr>
                    @foreach ($list as $value)
                    <td>{{ $value }}</td>
                    @endforeach
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="sparator"></div>
</body>
</html>
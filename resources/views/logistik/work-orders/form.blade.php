<div class="modal" id="modal-{{ $route }}" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form data-toggle="validator" method="post">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times fa-lg"></i>
                    </button>
                    <h3 class="modal-title">Add {{ $title }}</h3>
                </div>
                <div class="modal-body" >
                    <div class="row">
                        <input type="hidden" id="id" name="id">
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="bom_id" class="control-label">Bill of materials  *</label>
                            <select name="bom_id" id="bom_id" class="form-control">
                                <option value="" selected disabled>Select bill of materials</option>
                                @foreach ($bom as $r)
                                <option value="{{ $r->id }}">{{ $r->code }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="item_id" class="control-label">Item  *</label>
                            <input type="hidden" id="item_id" name="item_id">
                            <input type="text" class="form-control" id="item_name" readonly>
                        </div>
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="wo_date" class="control-label">Date *</label>
                            <input type="text" name="wo_date" id="wo_date" class="form-control datepicker" required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="uom_id" class="control-label">Order UOM *</label>
                            <select name="uom_id" id="uom_id" class="form-control select2">
                                @foreach ($uom as $r)
                                <option value="{{ $r->id_uom }}">{{ $r->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="order_qty" class="control-label">Order Qty *</label>
                            <input type="text" name="order_qty" id="order_qty" class="form-control" required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="start_date" class="control-label">Start Date *</label>
                            <input type="text" name="start_date" id="start_date" class="form-control datepicker" required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="due_date" class="control-label">Due Date *</label>
                            <input type="text" name="due_date" id="due_date" class="form-control datepicker" required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="description" class="control-label">Description *</label>
                            <input type="text" name="description" id="description" class="form-control" required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12">
                            <label>Status</label>
                            <div>
                                <label><input type="checkbox" name="is_active" value="1" id="active"> Enable</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
                </div>
            </form>
        </div>
    </div>
 </div>

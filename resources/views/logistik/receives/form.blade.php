<div class="modal" id="modal-{{ $route }}" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form data-toggle="validator" method="post">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times fa-lg"></i>
                    </button>
                    <h3 class="modal-title">Add {{ $title }}</h3>
                </div>
                <div class="modal-body" >
                    <div class="row">
                        <input type="hidden" id="id" name="id">
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="receive_code" class="control-label">Code *</label>
                            <input type="text" name="receive_code" id="receive_code" class="form-control" disabled>
                        </div>
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="receive_date" class="control-label">Date *</label>
                            <input type="text" name="receive_date" id="receive_date" class="form-control datepicker" required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="invoice_num" class="control-label">Invoce num *</label>
                            <input type="text" name="invoice_num" id="invoice_num" class="form-control" required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="receive_by" class="control-label">Receive By</label>
                            <input type="text" name="receive_by" id="receive_by" class="form-control" required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="warehouse_id" class="control-label">Warehouse *</label>
                            <select name="warehouse_id" id="warehouse_id" class="form-control">
                                <option value="" selected disabled>Select warehouse</option>
                                @foreach ($warehouses as $r)
                                <option value="{{ $r->id_warehouse }}">{{ $r->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="purchase_order_id" class="control-label">Po *</label>
                            <select name="purchase_order_id" id="purchase_order_id" class="form-control">
                                @foreach ($pos as $r)
                                <option value="{{ $r->id }}">{{ $r->purchase_order_num }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="supplier_id" class="control-label">Supplier *</label>
                            <select name="supplier_id" id="supplier_id" class="form-control">
                                @foreach ($suppliers as $r)
                                <option value="{{ $r->id_supplier }}">{{ $r->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="description" class="control-label">Description *</label>
                            <input type="text" name="description" id="description" class="form-control" required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="description" class="control-label">Status *</label>
                            <p>
                                <label style="margin-left: 10px">
                                    <input type="radio" name="status" id="status-1" required value="1"> Fulfilled
                                </label>
                                <label style="margin-left: 10px">
                                    <input type="radio" name="status" id="status-2" required value="2"> Unfulfilled
                                </label>
                                <label style="margin-left: 10px">
                                    <input type="radio" name="status" id="status-3" required value="3"> Canceled
                                </label>
                                <label style="margin-left: 10px">
                                    <input type="radio" name="status" id="status-4" required value="4"> Closed
                                </label>
                            </p>
                        </div>
                        <style>
                            .table-custom th,
                            .table-custom td{
                                font-size: .9rem;
                            }
                        </style>
                        <div class="col-sm-12">
                            <input type="hidden" name="delId" id="del-po-item-id" value="[]">
                            <table class="table table-bordered table-custom">
                                <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Uom</th>
                                        <th>Rack</th>
                                        <th>Batch&nbsp;No</th>
                                        <th>Expire&nbsp;Date</th>
                                        <th>Qty</th>
                                        <th>Price</th>
                                        <th>Total</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="receive_details"></tbody>
                                <tfoot id="tfoot-details">
                                    <tr>
                                        <td>
                                            <select id="item_id" class="form-control select2">
                                                @foreach ($items as $r)
                                                <option value="{{ $r->id_item }}">{{ $r->description }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select id="uom_id" class="form-control select2">
                                                @foreach ($uom as $r)
                                                <option value="{{ $r->id_uom }}">{{ $r->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select id="rack_id" class="form-control select2"></select>
                                        </td>
                                        <td>
                                            <input type="text" id="batch_no" class="form-control">
                                        </td>
                                        <td>
                                            <input type="text" id="expire_date" class="form-control datepicker">
                                        </td>
                                        <td>
                                            <input type="number" id="qty" class="form-control" autocomplete="off">
                                        </td>
                                        <td>
                                            <input type="number" id="price" class="form-control" autocomplete="off">
                                        </td>
                                        <td>
                                            <input type="number" id="total" class="form-control" readonly>
                                        </td>
                                        <td>
                                            <button class="btn btn-primary btn-sm" type="button" role="button" id="add-table-item"><i class="fa fa-plus"></i></button>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
                </div>
            </form>
        </div>
    </div>
 </div>

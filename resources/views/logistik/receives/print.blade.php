<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Receive</title>
    <style>
        * {
            font-size: 8pt;
            color: #000000;
            font-family: "Trebuchet MS", "Lucida Grande", "Lucida Sans Unicode", "Lucida Sans", Tahoma, sans-serif;
        }

        @page {
            margin: 0.3cm;
        }

        ul {
            margin: 0 -20;
            list-style: none;
        }

        ul li:before {
            content: "•";
            font-size: 1em;
            /* or whatever */
            padding-right: 5px;
        }


        .normal-header-td {
            border-bottom: 1px solid black;
        }

        .last-header-td {
            border-bottom: 1px solid black;
        }

        .block_center {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }

        .col-0_5,
        .col-1,
        .col-1_5,
        .col-2,
        .col-3,
        .col-4,
        .col-5,
        .col-6,
        .col-7,
        .col-8,
        .col-9,
        .col-10,
        .row {
            float: left;
            position: relative;
            min-height: 1px;
        }

        .col-0_5 {
            width: 5%;
        }

        .col-1 {
            width: 10%;
        }

        .col-1_5 {
            width: 15%;
        }

        .col-2 {
            width: 20%;
        }

        .col-3 {
            width: 30%;
        }

        .col-4 {
            width: 40%;
        }

        .col-5 {
            width: 50%;
        }

        .col-6 {
            width: 60%;
        }

        .col-7 {
            width: 70%;
        }

        .col-8 {
            width: 80%;
        }

        .col-9 {
            width: 90%;
        }

        .col-10 {
            width: 100%;
        }

        .floatCenter {
            text-align: center;
        }

        .indentConditionParagraph {
            margin-left: 5px;
            width: 100%;
        }

        .indentConditions {
            float: left;
            width: 100%;
            margin-left: 15px;
        }

        .indentConditions-numbering {
            float: left;
            font-size: 9pt;
            vertical-align: top;
        }

        .indentConditions-bullets:before {
            content: "•";
            font-size: 1em;
            /* or whatever */
            padding-right: 5px;
        }

        .indentConditions-nestedConditions {
            float: left;
            width: 95%;
            margin-left: 5px;
        }

        .invoiceDetail-tbl {
            width: 100%;
            border: 1px solid black;
            border-right: 1px solid white;
            border-left: 1px solid white;
        }

        .invoiceDetail-tbl td {
            vertical-align: top;
        }

        .invoicePayment-tbl {
            width: 100%;
        }

        tr.lastRow {
            height: 30%;
        }

        .leftHeader {
            white-space: nowrap;
            width: 8%;
        }

        .leftHeaderData {
            width: 42%;
        }

        .rightHeader {
            white-space: nowrap;
            width: 8%;
        }

        .rightHeaderData {
        }

        .row {
            width: 100%;
        }

        .signature-title {
            height: 6em;
            font-weight: bold;
            font-size: 12px;
        }

        .signature-underline {
            height: 2em;
            font-weight: bold;
            font-size: 11px;
            border-bottom: black 2px solid;
        }

        .termsConditions {
            font-size: 10px;
            float: left;
            width: 100%;
        }

        .termsConditions-grandTitle {
            font-size: 9px;
            font-weight: bold;
            margin-top: 3px;
        }

        .termsConditions-subTitle {
            font-weight: bold;
            margin-top: 3px;
            font-size: 8px;
        }

        .termsConditions>h3 {
            font-size: 9px;
            margin: 0;
        }

        .termsConditions>ul>li,
        .termsConditions>div>ul>li,
        .indentConditions-nestedConditions>ul>li,
        .smallFont {
            font-size: 7pt;
        }

        .textRightAlign {
            text-align: right;
        }

        .pageBreakFooter {
            page-break-after: always;
        }

        .itembalance {
            font-size: 8px;
        }

        .itembalance-title {
            font-weight: bold;
        }

        .customHeight {
            height: 5px;
        }

        hr {
            border: black thin solid;
        }

        .taxInvoiceTextHeader {
            font-size: 16pt;
            font-weight: bold;
        }

        .taxInvoiceNumber {
            font-size: 12pt;
        }

        #termsId {
            width: 95%;
            position: absolute;
            bottom: 10;
        }

        thead {
            display: table-header-group;
        }

        tfoot {
            display: table-footer-group;
        }
    </style>
</head>

<body>
    <table id="mainTbl" style="width: 700px;" border="0" align="center">
        <thead>
            <tr>
                <td>
                    <table width="100%">
                        <tbody>
                            <tr>
                                <td valign="top" width="33%">
                                  <strong>
                                      Delovely Wow Cantiknya<br />
                                      Jl.Darmo Baru Timur IV No. 3 RT 01 / RW 04 Kel.Sonokwijenan Kec.Sukomanunggal<br />
                                      Surabaya, Jawa Timur,
                                  </strong>
                                  <br />
                                  <strong>Tel. / Fax.</strong> <br />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="0">
                        <tbody>
                            <tr>
                                <td class="leftHeader"><strong>Supplier</strong></td>
                                <td>:</td>
                                <td class="leftHeaderData"></td>
                                <td class="rightHeader"><strong><u>RECEIVE GOODS</u></strong></td>
                                <td class="rightHeaderData"></td>
                                <td class="rightHeaderData"></td>
                            </tr>
                            <tr>
                                <td class="leftHeader">{{  $data->supplier ? $data->supplier->name : '-' }}</td>
                                <td></td>
                                <td class="leftHeaderData"></td>
                                <td class="rightHeader"><strong>No.</strong></td>
                                <td>:</td>
                                <td class="rightHeaderData">{{ $data->code }} </td>

                            </tr>
                            <tr>
                                <td class="leftHeader"><strong>-</strong></td>
                                <td></td>
                                <td class="leftHeaderData"></td>
                                <td class="rightHeader"><strong>Date</strong></td>
                                <td>:</td>
                                <td class="rightHeaderData">{{ $data->transdate->format('d-M-Y')}}</td>
                            </tr>
                            <tr>
                                <td class="leftHeader"><strong></strong></td>
                                <td></td>
                                <td class="leftHeaderData"></td>
                                <td class="rightHeader"><strong>PO No.</strong></td>
                                <td>:</td>
                                <td class="rightHeaderData">
                                @foreach ($data->details->groupBy('id_order_detail') as $list)
                                 {{$list[0]->orderDetail->order['code']}}
                                @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td class="leftHeader"><strong></strong></td>
                                <td></td>
                                <td class="leftHeaderData"></td>
                                <td class="rightHeader"><strong>PO Date.</strong></td>
                                <td>:</td>
                                <td class="rightHeaderData">
                                @foreach ($data->details->groupBy('id_order_detail') as $list)
                                 {{$list[0]->orderDetail->order['transdate']->format('d-M-Y')}}
                                @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td class="leftHeader"><strong></strong></td>
                                <td></td>
                                <td class="leftHeaderData"></td>
                                <td class="rightHeader"><strong>Supplier Doc No.</strong></td>
                                <td>:</td>
                                <td class="rightHeaderData">{{ $data->supplier->code }} </td>
                            </tr>
                            <tr>
                                <td class="leftHeader"><strong></strong></td>
                                <td></td>
                                <td class="leftHeaderData"></td>
                                <td class="rightHeader"><strong>Supplier Doc Date.</strong></td>
                                <td>:</td>
                                <td class="rightHeaderData">{{ $data->transdate->format('d-M-Y')}}</td>
                            </tr>
                            <tr>
                                <td class="leftHeader"><strong></strong></td>
                                <td></td>
                                <td class="leftHeaderData"></td>
                                <td class="rightHeader"><strong>Warehouse Receive</strong></td>
                                <td>:</td>
                                <td class="rightHeaderData">
                                @foreach ($data->details->groupBy('warehouse_id') as $list)
                                    {{$list[0]->warehouse->name}}
                                @endforeach
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <br>
                    <table class="invoiceDetail-tbl" border="0" cellspacing="0" cellpadding="3px">
                        <thead>
                            <tr>
                                <td class="normal-header-td" width="5%">No.</td>
                                <td class="normal-header-td" width="25%">Product Code</td>
                                <td class="normal-header-td" width="30%">Description</td>
                                <td class="normal-header-td" width="15%">Uom</td>
                                <td class="normal-header-td">Qty PO</td>
                                <td class="normal-header-td">Qty Receive</td>
                            </tr>
                        </thead>
                        <tbody>
                              @php
                              $no=1;
                              @endphp
                              @foreach ($data->details as $list)
                                <tr  style="height:8px;">
                                  <td>{{ $no }}</td>
                                  <td>{{$list->item ? $list->item->sales['product_treatment_code'] : '-' }}</td>
                                  <td>{{$list->item ? $list->item->name : '-'}}</td>
                                  <td>{{$list->item ? $list->item->uom['display'] :'-'}}</td>
                                  <td>{{$list->orderDetail ? $list->orderDetail->qty  : ''}}</td>
                                  <td>{{$list->received_qty}}</td>

                                </tr>
                                @php
                                $no++;
                                @endphp
                              @endforeach
                              <tr style="height:20px;">
                              </tr>
                              <tr>
                                  <td colspan="4"></td>
                                  <td><strong>Total Qty Receive :</strong></td>
                                  <td>
                                      {{ $data->details()->sum('received_qty') }}
                                  </td>
                                </tr>
                        </tbody>
                    </table>
                    <br>
            <tr style="height:30px;">
                <td valign="top"><strong>Note :</strong> {{ $data->remarks }} </td>
            </tr>
            <tr>
                <td>
                    <div class="row">
                        <div class="col-2">
                            <div class="row">
                                <div class="col-10 floatCenter signature-title">Dibuat Oleh</div>
                                <div class="col-10 floatCenter">(.......................)</div>
                            </div>
                        </div>
                        <div class="col-2"></div>
                        <div class="col-2">
                            <div class="row">
                                <div class="col-10 floatCenter signature-title">Pengirim</div>
                                <div class="col-10 floatCenter">(.......................)</div>
                            </div>
                        </div>
                        <div class="col-2"></div>
                        <div class="col-2">
                            <div class="row">
                                <div class="col-10 floatCenter signature-title">Penerima</div>
                                <div class="col-10 floatCenter">(.......................)</div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            
            </td>
            </tr>
        </tbody>
        <tfoot id="termsId"></tfoot>
    </table>
    <script>
    </script>
</body>

</html>
<div class="modal" id="modal-warehouses" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
       <div class="modal-content">
     
    <form class="form-horizontal" data-toggle="validator" method="post">
    {{ csrf_field() }} {{ method_field('POST') }}
    
     <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <i class="fa fa-times fa-lg"></i>
         </button>
         <h3 class="modal-title">Add {{ $title }}</h3>
     </div>
         <div class="modal-body" >
            <input type="hidden" id="id" name="id">
            <div class="form-group">
                <label for="name" class="col-md-3 control-label">Code *</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="code" name="code" placeholder="Code" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-md-3 control-label">Name *</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group">
                <label for="addr" class="col-md-3 control-label">Description *</label>
                <div class="col-md-8">
                    <textarea name="description" id="description" class="form-control"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="addr" class="col-md-3 control-label">Address *</label>
                <div class="col-md-8">
                    <textarea name="addr" id="addr" class="form-control"></textarea>
                </div>
            </div>
            <div class="form-group" >
                <div class="form-check" id="group_outlet">
                         <label for="outlet" class="col-md-3 control-label">Branch *</label>
                        <div class="col-md-8">
                            <select  class="form-control select2 select2-hidden-accessible"multiple="multiple" data-placeholder="Select a Outlet"  name="outlet[]" id="outlet" tabindex="-1" aria-hidden="true"  required>
                                @foreach ($outlet as $list)
                                <option value="{{ $list->id_outlet }}">{{ $list->outlet_name }}</option>
                                @endforeach
                            </select>
                            <span class="help-block text-red" id="msg_outlet"></span>
                        </div>
                </div>
            </div>
            <div class="form-group">
                <label for="post_code" class="col-md-3 control-label">Status *</label>
                <div class="col-md-8">
                    <label>
                        <input type="checkbox" name="is_active" value="1" id="active" checked="checked"/> 
                        Enable
                    </label>
                </div>
            </div>
         </div>
         
         <div class="modal-footer">
             <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
             <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
         </div>
     
            </form>
        </div>
    </div>
 </div>

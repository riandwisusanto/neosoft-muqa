<div class="modal" id="modal-{{ $route }}" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form data-toggle="validator" method="post">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times fa-lg"></i>
                    </button>
                    <h3 class="modal-title">Add Retur</h3>
                </div>
                <div class="modal-body" >
                    <div class="row">
                        <input type="hidden" id="id" name="id">
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="retur_num" class="control-label">Retur Number *</label>
                            <input type="text" id="retur_num" class="form-control" disabled>
                        </div>
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="retur_date" class="control-label">Retur Date *</label>
                            <input type="text" name="retur_date" id="retur_date" class="form-control datepicker" required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="warehouse_id" class="control-label">Warehouse *</label>
                            <select name="warehouse_id" id="warehouse_id" class="form-control">
                                @foreach ($warehouses as $r)
                                <option value="{{ $r->id_warehouse }}">{{ $r->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="receive_id" class="control-label">Receive *</label>
                            <select name="receive_id" id="receive_id" class="form-control">
                                <option value="" > - Select Receive - </option>
                                @foreach ($receives as $r)
                                <option value="{{ $r->id }}" >{{ $r->receive_code }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="invoice_no" class="control-label">Invoice Num</label>
                            <input type="text" name="invoice_no" id="invoice_no" class="form-control datepicker" readonly required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="invoice_date" class="control-label">Invoice Date</label>
                            <input type="text" name="invoice_date" id="invoice_date" class="form-control datepicker" readonly required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="supplier_id" class="control-label">Supplier *</label>
                            <input type="hidden" name="supplier_id" id="supplier_id" required>
                            <input type="text" id="supplier_name" class="form-control" disabled>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="pay_method" class="control-label">Payment Method * </label>
                            <input type="text" name="pay_method" id="pay_method" class="form-control" required autocomplete="off" readonly>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="due_date" class="control-label">Due Date * </label>
                            <input type="text" name="due_date" id="due_date" class="form-control datepicker" required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="description" class="control-label">Description *</label>
                            <input type="text" name="description" id="description" class="form-control" required autocomplete="off">
                        </div>
                        <style>
                            .table-custom th,
                            .table-custom td{
                                font-size: .9rem;
                            }
                        </style>
                        <div class="col-sm-12">
                            <input type="hidden" name="delId" id="del-po-item-id" value="[]">
                            <table class="table table-bordered table-custom">
                                <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Uom</th>
                                        <th>Description</th>
                                        <th>Qty</th>
                                        <th>Price</th>
                                        <th>Disc</th>
                                        <th>Tax</th>
                                        <th>Total</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="{{ $route }}_details"></tbody>
                                <tfoot id="tfoot-details">
                                    <tr>
                                        <td>
                                            <select id="item_id" class="form-control select2">
                                                @foreach ($items as $r)
                                                <option value="{{ $r->id_item }}">{{ $r->description }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select id="uom_id" class="form-control select2">
                                                @foreach ($uom as $r)
                                                <option value="{{ $r->id_uom }}">{{ $r->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" id="description_item" class="form-control" autocomplete="off">
                                        </td>
                                        <td>
                                            <input type="number" id="qty" class="form-control" autocomplete="off">
                                        </td>
                                        <td>
                                            <input type="number" id="price" class="form-control" autocomplete="off">
                                        </td>
                                        <td>
                                            <input type="number" id="disc" class="form-control" autocomplete="off">
                                        </td>
                                        <td>
                                            <input type="number" id="tax" class="form-control" autocomplete="off">
                                        </td>
                                        <td>
                                            <input type="number" id="total" class="form-control" readonly>
                                        </td>
                                        <td>
                                            <button class="btn btn-primary btn-sm" type="button" role="button" id="add-table-item"><i class="fa fa-plus"></i></button>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
                </div>
            </form>
        </div>
    </div>
 </div>

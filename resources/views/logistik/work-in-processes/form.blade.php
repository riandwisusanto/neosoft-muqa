<div class="modal" id="modal-{{ $route }}" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form data-toggle="validator" method="post">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times fa-lg"></i>
                    </button>
                    <h3 class="modal-title">Add {{ $title }}</h3>
                </div>
                <div class="modal-body" >
                    <div class="row">
                        <input type="hidden" id="id" name="id">
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="work_order_id" class="control-label">Work Order *</label>
                            <select name="work_order_id" id="work_order_id" class="form-control" required>
                                <option value="" selected disabled>Select work order</option>
                                @foreach ($wo as $r)
                                <option value="{{ $r->id }}">{{ $r->bom ? ($r->bom->item ? $r->bom->item->code : '-') : '' }} | {{ date('d M Y', strtotime($r->wo_date)) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="wip_date" class="control-label">Date *</label>
                            <input type="text" name="wip_date" id="wip_date" class="form-control datepicker" required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="due_date" class="control-label">Due Date *</label>
                            <input type="text" name="due_date" id="due_date" class="form-control" required autocomplete="off" readonly>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="order_uom" class="control-label">Order UOM</label>
                            <input type="hidden" name="uom_id" id="uom_id" required>
                            <input type="text" name="uom_name" id="uom_name" class="form-control" readonly>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="order_qty" class="control-label">Order qty</label>
                            <input type="text" name="order_qty" id="order_qty" class="form-control" required autocomplete="off" readonly>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="description" class="control-label">Description *</label>
                            <input type="text" name="description" id="description" class="form-control" required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="description" class="control-label">Status *</label>
                            <p>
                                <label style="margin-left: 10px">
                                    <input type="radio" name="status" id="status-1" required value="1"> Completed
                                </label>
                                <label style="margin-left: 10px">
                                    <input type="radio" name="status" id="status-2" required value="2"> Incompleted
                                </label>
                                <label style="margin-left: 10px">
                                    <input type="radio" name="status" id="status-3" required value="3"> In Process
                                </label>
                                <label style="margin-left: 10px">
                                    <input type="radio" name="status" id="status-4" required value="4"> Failed Process
                                </label>
                                <label style="margin-left: 10px">
                                    <input type="radio" name="status" id="status-4" required value="5"> Cancel Process
                                </label>
                            </p>
                        </div>    
                        <style>
                            .table-custom th,
                            .table-custom td{
                                font-size: .9rem;
                            }
                        </style>
                        <div class="col-sm-12">
                            <input type="hidden" name="delId" id="del-po-item-id" value="[]">
                            <table class="table table-bordered table-custom">
                                <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Uom</th>
                                        <th>BOM Qty</th>
                                        <th>Qty</th>
                                        <th>Price</th>
                                        <th>Total</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="receive_details"></tbody>
                                <tfoot id="tfoot-details">
                                    <tr>
                                        <td>
                                            <select id="item_id" class="form-control select2">
                                                @foreach ($items as $r)
                                                <option value="{{ $r->id_item }}">{{ $r->description }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select id="uom_id" class="form-control select2">
                                                @foreach ($uom as $r)
                                                <option value="{{ $r->id_uom }}">{{ $r->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <input type="number" id="bom_qty" class="form-control" autocomplete="off">
                                        </td>
                                        <td>
                                            <input type="number" id="qty" class="form-control" autocomplete="off">
                                        </td>
                                        <td>
                                            <input type="number" id="price" class="form-control" autocomplete="off">
                                        </td>
                                        <td>
                                            <input type="number" id="total" class="form-control" readonly>
                                        </td>
                                        <td>
                                            <button class="btn btn-primary btn-sm" type="button" role="button" id="add-table-item"><i class="fa fa-plus"></i></button>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
                </div>
            </form>
        </div>
    </div>
 </div>

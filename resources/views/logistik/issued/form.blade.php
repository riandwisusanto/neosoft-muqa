<div class="modal" id="modal-{{ $route }}" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form data-toggle="validator" method="post">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times fa-lg"></i>
                    </button>
                    <h3 class="modal-title">Add {{ $title }}</h3>
                </div>
                <div class="modal-body" >
                    <div class="row">
                        <input type="hidden" id="id" name="id">
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="issued_num" class="control-label">Issued Number *</label>
                            <input type="text" id="issued_num" class="form-control" disabled>
                        </div>
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="issued_date" class="control-label">Date *</label>
                            <input type="text" name="issued_date" id="issued_date" class="form-control datepicker" required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="invoice_no" class="control-label">Invoice Customer *</label>
                            <select name="invoice_no" id="invoice_no" class="form-control">
                                    <option value="" selected disabled>Select warehouse</option>
                                    @foreach ($invoices as $r)
                                    <option value="{{ $r->id_invoice }}">{{ $r->inv_code }}</option>
                                    @endforeach
                                </select>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="invoice_date" class="control-label">Invoice Date</label>
                            <input type="text" name="invoice_date" id="invoice_date" class="form-control" readonly required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="warehouse_id" class="control-label">Warehouse *</label>
                            <select name="warehouse_id" id="warehouse_id" class="form-control">
                                <option value="" selected disabled>Select warehouse</option>
                                @foreach ($warehouses as $r)
                                <option value="{{ $r->id_warehouse }}">{{ $r->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="customer_id" class="control-label">Customer *</label>
                            <input type="hidden" name="customer_id" id="customer_id" value="" required>
                            <input type="text" class="form-control" id="customer_name" disabled>
                        </div>
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="pay_method" class="control-label">Payment Method  </label>
                            <input type="text" name="pay_method" id="pay_method" class="form-control" autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="due_date" class="control-label">Due Date * </label>
                            <input type="text" name="due_date" id="due_date" class="form-control datepicker" required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="description" class="control-label">Description *</label>
                            <input type="text" name="description" id="description" class="form-control" required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="description" class="control-label">Status *</label>
                            <p>
                                <label style="margin-left: 10px">
                                    <input type="radio" name="status" id="status-1" required value="1" checked> Create/Draft Retur
                                </label>
                                <label style="margin-left: 10px">
                                    <input type="radio" name="status" id="status-2" required value="2"> Deliver Product
                                </label>
                                <label style="margin-left: 10px">
                                    <input type="radio" name="status" id="status-3" required value="3"> Receive Product by Customer
                                </label>
                                <label style="margin-left: 10px">
                                    <input type="radio" name="status" id="status-4" required value="4"> Canceled
                                </label>
                                <label style="margin-left: 10px">
                                    <input type="radio" name="status" id="status-5" required value="5"> Closed
                                </label>
                            </p>
                        </div>
                        
                        <style>
                            .table-custom th,
                            .table-custom td{
                                font-size: .9rem;
                            }
                        </style>
                        <div class="col-sm-12">
                            <input type="hidden" name="delId" id="del-po-item-id" value="[]">
                            <table class="table table-bordered table-custom">
                                <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Uom</th>
                                        <th>Description</th>
                                        <th>Qty</th>
                                        <th>Price</th>
                                        <th>Disc</th>
                                        <th>Tax</th>
                                        <th>Total</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="{{ $route }}_details"></tbody>
                                <tfoot>
                                    <tr>
                                        <td>
                                            <select id="item_id" class="form-control select2">
                                                @foreach ($items as $r)
                                                <option value="{{ $r->id_item }}">{{ $r->description }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select id="uom_id" class="form-control select2">
                                                @foreach ($uom as $r)
                                                <option value="{{ $r->id_uom }}">{{ $r->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" id="description_item" class="form-control" autocomplete="off">
                                        </td>
                                        <td>
                                            <input type="number" id="qty" class="form-control" autocomplete="off">
                                        </td>
                                        <td>
                                            <input type="number" id="price" class="form-control" autocomplete="off">
                                        </td>
                                        <td>
                                            <input type="number" id="disc" class="form-control" autocomplete="off">
                                        </td>
                                        <td>
                                            <input type="number" id="tax" class="form-control" autocomplete="off">
                                        </td>
                                        <td>
                                            <input type="number" id="total" class="form-control" readonly>
                                        </td>
                                        <td>
                                            <button class="btn btn-primary btn-sm" type="button" role="button" id="add-table-item"><i class="fa fa-plus"></i></button>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
                </div>
            </form>
        </div>
    </div>
 </div>

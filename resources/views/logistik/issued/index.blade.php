@extends('base')

@section('title')
  List {{ $title }}
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
@endsection

@section('breadcrumb')
  @parent
  <li>{{ $title }}</li>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add {{ $title }}</a>
      </div>
      <div class="box-body">
        <table class="table table-striped table-bordered  nowrap table-city" style="width:100%">
        <thead>
          <tr>
            <th>Issued Num</th>
            <th>Issued Date</th>
            <th>Description</th>
            <th>Invoice Num</th>
            <th>Invoice Date</th>
            <th>Warehouse</th>
            <th>Status</th>
            <th class="all" width="10%">Action</th>
          </tr>
        </thead>
        <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@include($view.'form')
@endsection

@section('script')
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>
<script type="text/javascript">
  var table, save_method, code = '{{ $code }}';
  var items = {!! json_encode($items) !!};
  var uom   = {!! json_encode($uom) !!};
  var i = 1;
  var rack_data = [];



  function loadAjaxRack(warehouse_id){
    return new Promise((resolve, reject) => {
      $.ajax({
        url: '{{ route('product-mutations.get-rack') }}',
        data: {warehouse_id: warehouse_id},
        dataType: 'json',
        success: function (res) {
          resolve(res);
        },
        error: function(res){
          reject(res);
        }
      });
    })
  }

  $('.datepicker').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    todayHighlight: true
  });

  $(function(){
    table = $('.table-city').DataTable({
      processing    : true,
      serverside    : true,
      scrollY       : "300px",
      scrollX       : true,
      scrollCollapse: true,
      fixedColumns  : {
        leftColumns: 1
      },
      ajax : {
        url : "{{ route($route.'.data') }}",
        type : "GET"
      }
    });

    $('#modal-{{ $route }} form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
        var id = $('#id').val();
        if(save_method == "add") url = "{{ route($route.'.store') }}";
        else url = "/{{ $route }}/" + id;

        $.ajax({
          url : url,
          type : "POST",
          data : $('#modal-{{ $route }} form').serialize(),
          success : function(data){
            $('#modal-{{ $route }}').modal('hide');
            table.ajax.reload();
            if(save_method == "add") code = data;
          },
          error : function(e){
            swal({
              title : 'Can not save the data',
              icon  : "warning",
            });
          }
        });
        return false;
      }
    });

    $('#invoice_no').change(function(){
      let id = $(this).val();
      if(id){
        $.ajax({
          url: `{{ url('issued/get-invoice') }}/${id}`,
          dataType: "json",
          success: function (res) {
            $('#invoice_date').val(res.inv_date);
            $('#customer_id').val(res.customer_id);
            $('#customer_name').val(res.customer ? res.customer.full_name : '-');
          }
        });
      }
    })

    $('#qty, #price, #tax, #disc').on('change keyup', function(){
      let price = $('#price').val() ? parseInt($('#price').val()) : 0;
      let qty = $('#qty').val() ? parseInt($('#qty').val()) : 0;
      let disc = $('#disc').val() ? parseInt($('#disc').val()) : 0;
      let tax = $('#tax').val() ? parseInt($('#tax').val()) : 0;
      let subtotal = (price * qty) + tax - disc;

      $('#total').val(subtotal);
    })

    $('#warehouse_id').change(function(){
      var id = $(this).val();
      let rack = loadAjaxRack(id);
      rack.then(res => {
        rack_data = res;
        let html = rackOption();
        $('#rack_id').html(html);
        $('body').find('.select-rack').each((k, v) => {
          let elem = $($('body').find('.select-rack').get(k));
          if(elem.data('id')){
            elem.html(rackOption(elem.data('id')));
          }else{
            elem.html(rackOption());
          }
        });
      }).catch((msg) => console.log('error ', msg));
    });

    $('#add-table-item').click(function(){
      let description = $('#description_item').val();
      let item_id = $('#item_id').val();
      let rack_id = $('#rack_id').val();
      let uom_id = $('#uom_id').val();
      let price = $('#price').val();
      let total = $('#total').val();
      let qty = $('#qty').val();
      let disc = $('#disc').val();
      let tax = $('#tax').val();

      let html = `<tr>
        <td>
          <select id="item_id_${i}" name="items[${i}][item_id]" class="form-control select2">${ itemsOption(item_id) }</select>
        </td>
        <td>
          <select id="uom_id_${i}" name="items[${i}][uom_id]" class="form-control select2">${ uomOption(uom_id) }</select>
        </td>
        <td>
          <input type="text" id="description_${i}" name="items[${i}][description]" class="form-control" autocomplete="off">
        </td>
        <td>
          <input type="number" id="qty_${i}" name="items[${i}][qty]" class="form-control" required>
        </td>
        <td>
          <input type="number" id="price_${i}" name="items[${i}][price]" class="form-control" required>
        </td>
        <td>
          <input type="number" id="disc_${i}" name="items[${i}][disc]" class="form-control" required>
        </td>
        <td>
          <input type="number" id="tax_${i}" name="items[${i}][tax]" class="form-control" required>
        </td>
        <td>
          <input type="number" id="total_${i}" name="items[${i}][total]" class="form-control" readonly required>
        </td>
        <td>
          <button class="btn btn-danger btn-sm delete-item" type="button" role="button"><i class="fa fa-times"></i></button>
        </td>
      </tr>`;

      var receive_details = $('#{{ $route }}_details').append(html);
      $(`#item_id_${i}, #uom_id_${i}, #rack_id_${i}`).select2();
      $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
      });
      console.log(description)

      var description_aa = $(`#description_${i}`).val(description);
      var disc_aa = $(`#disc_${i}`).val(disc);
      var tax_aa = $(`#tax_${i}`).val(tax);
      var total_aa = $(`#total_${i}`).val(total);
      var price_aa = $(`#price_${i}`).val(price);
      var qty_aa = $(`#qty_${i}`).val(qty);

      subtotal();
      function subtotal () {
        price = price_aa.val() ? parseInt(price_aa.val()) : 0;
        qty = qty_aa.val() ? parseInt(qty_aa.val()) : 0;
        disc = disc_aa.val() ? parseInt(disc_aa.val()) : 0;
        tax = tax_aa.val() ? parseInt(tax_aa.val()) : 0;
        total = (price * qty) + tax - disc;

        total_aa.val(total);
      }
      $(`#price_${i}, #qty_${i}`).on('change keyup', function(){
        subtotal();
      })
      i++;
    })
  });

  $('body').on('click', '.delete-item', function(){
    let id = $(this).data('id');
    if(id){
      let delId = JSON.parse($('#del-po-item-id').val());
      delId = [...delId, id];
      $('#del-po-item-id').val(JSON.stringify(delId));
    }
    $(this).parent().parent().remove();
  })

  function itemsOption(id){
    let html = '';
    items.map(r => {
      html += `<option value="${r.id_item}" ${id == r.id_item ? 'selected' : ''}>${r.description}</option>`;
    })
    return html;
  }

  function uomOption(id){
    let html = '';
    uom.map(r => {
      html += `<option value="${r.id_uom}" ${id == r.id_uom ? 'selected' : ''}>${r.name}</option>`;
    })
    return html;
  }

  function rackOption(id){
    let html = '';
    rack_data.map(r => {
      html += `<option value="${r.id_rack}" ${id == r.id_rack ? 'selected' : ''}>${r.main_rack_code} - ${r.sub_rack_code}</option>`;
    })
    return html;
  }

  function addForm(){
    save_method = "add";
    $('input[name=_method]').val('POST');
    $("#modal-{{ $route }}").modal({
      backdrop: 'static',
      keyboard: false,
      show: true
    });
    $('#modal-{{ $route }} form')[0].reset();
    $('.modal-title').text('Add {{ $title }}');
    $('#active').removeAttr('checked');
    $('#notactive').removeAttr('checked');
    $('#{{ $route }}_details').html('');
    $('#del-po-item-id').val('[]');
    $('#issued_num').val(code);

    $('#status-2, #status-3, #status-4, #status-5').removeAttr('checked');
    $('#status-1').attr('checked', true);
  }

  function detailInfo(id) {
    
  }

  function editForm(id){
    save_method = "edit";
    $('input[name=_method]').val('PUT');
    $('#modal-{{ $route }} form')[0].reset();
    $('#active').removeAttr('checked');
    $('#notactive').removeAttr('checked');
    $('#del-po-item-id').val('[]');

    $.ajax({
      url : "/{{ $route }}/"+id+"/edit",
      type : "GET",
      dataType : "JSON",
      success : function(data){
        $('#modal-{{ $route }}').modal('show');
        $('.modal-title').text('Edit {{ $title }}');

        function dateFormat(date){
          return moment(date, 'YYYY-MM-DD').format('DD/MM/YYYY')
        }
      
        $('#id').val(data.id);
        $('#issued_num').val(data.issued_num);
        $('#issued_date').val(dateFormat(data.issued_date));
        $('#invoice_no').val(data.invoice_no);
        $('#invoice_date').val(dateFormat(data.invoice_date));
        $('#due_date').val(dateFormat(data.due_date));
        $('#pay_method').val(data.pay_method);
        $('#warehouse_id').val(data.warehouse_id).trigger('change');
        $('#customer_id').val(data.customer_id).trigger('change');
        $('#description').val(data.description);
        $('#{{ $route }}_details').html('');

        if(data.is_active == 1){
          $('#active').attr('checked', true);
        }else{
          $('#notactive').attr('checked', true);
        }

        $('#status-1, #status-2, #status-3, #status-4, #status-5').removeAttr('checked');
        if(data.status === 1){
          $('#status-1').attr('checked', true);
        }else if(data.status === 2){
          $('#status-2').attr('checked', true);
        }else if(data.status === 3){
          $('#status-3').attr('checked', true);
        }else if(data.status === 4){
          $('#status-4').attr('checked', true);
        }else{
          $('#status-5').attr('checked', true);
        }

        let html = ''
        data.issued_details.map(r => {
           html = `<tr>
            <td>
              <input type="hidden" id="id_${i}" name="items[${i}][id]">
              <select id="item_id_${i}" name="items[${i}][item_id]" class="form-control select2append">${ itemsOption(r.item_id) }</select>
            </td>
            <td>
              <select id="uom_id_${i}" name="items[${i}][uom_id]" class="form-control select2append">${ uomOption(r.uom_id) }</select>
            </td>
            <td>
              <input type="text" id="description_${i}" name="items[${i}][description]" class="form-control" autocomplete="off" required>
            </td>
            <td>
              <input type="number" id="qty_${i}" name="items[${i}][qty]" class="form-control" required>
            </td>
            <td>
              <input type="number" id="price_${i}" name="items[${i}][price]" class="form-control" required>
            </td>
            <td>
              <input type="number" id="disc_${i}" name="items[${i}][disc]" class="form-control" required>
            </td>
            <td>
              <input type="number" id="tax_${i}" name="items[${i}][tax]" class="form-control">
            </td>
            <td>
              <input type="number" id="total_${i}" name="items[${i}][total]" class="form-control" readonly required>
            </td>
            <td>
              <button class="btn btn-danger btn-sm delete-item" type="button" role="button" data-id="${r.id}"><i class="fa fa-times"></i></button>
            </td>
          </tr>`;

          var receive_details = $('#{{ $route }}_details').append(html);
          $('.select2append').select2();
          $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true
          });

          // aa : after append
          var id_aa = $(`#id_${i}`).val(r.id);
          var qty_aa = $(`#qty_${i}`).val(r.qty);
          var total_aa = $(`#total_${i}`).val(r.total);
          var price_aa = $(`#price_${i}`).val(r.price);
          var disc_aa = $(`#disc_${i}`).val(r.disc);
          var tax_aa = $(`#tax_${i}`).val(r.tax);
          var description_aa = $(`#description_${i}`).val(r.description);
          
          subtotal();
          function subtotal () {
            price = price_aa.val() ? parseInt(price_aa.val()) : 0;
            qty = qty_aa.val() ? parseInt(qty_aa.val()) : 0;
            disc = disc_aa.val() ? parseInt(disc_aa.val()) : 0;
            tax = tax_aa.val() ? parseInt(tax_aa.val()) : 0;
            total = (price * qty) + tax - disc;

            total_aa.val(total);
          }
          $(`#price_${i}, #qty_${i}`).on('change keyup', function(){
            subtotal();
          })
          i++;
        })
      },
      error : function(){
        alert("Can not Show the Data!");
      }
    });
  }

  function deleteData(id){
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this {{ $title }}!",
      icon: "warning",
      buttons: {
        canceled:{
          text:'Cancel',
          value: 'cancel',
          className: 'swal-button btn-default'
        },
        deleted:{
          text:'Delete',
          value: 'delete',
          className: 'swal-button btn-danger'
        }
      },
      dangerMode: true,
    }).then((willDelete) => {
      switch (willDelete) {
        default:
          swal("{{ $title }} is safe!");
        break;
        case 'delete':
          $.ajax({
            url : "/{{ $route }}/"+id,
            type : "POST",
            data : {'_method' : 'Delete', '_token' : $('meta[name=csrf-token]').attr('content')},
            success : function(data){
              if (data === "error") {
                swal({
                  text: '{{ $title }} is Used !',
                  icon: 'error'
                })
              } else {
                swal("{{ $title }} has been deleted!", {
                  icon: "success",
                });
                code = data;
              }                        
              table.ajax.reload();
            },
            error : function(){
              swal({
                text: 'Can not Delete the Data!',
                icon: 'error'
              })
            }
          });
        break;
      }
    });
  }
</script>
@endsection

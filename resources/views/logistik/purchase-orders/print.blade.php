<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print PO</title>
    <style>
      body {
        background: rgb(204,204,204);
      }
      page {
        background: white;
        display: block;
        margin: 0 auto;
        margin-bottom: 0.5cm;
        box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
      }
      page[size="A4"] {
        width: 21cm;
        height: 29.7cm;
        padding: 20mm 16mm;
      }
      @media print {
        body, page {
          margin: 0;
          box-shadow: none;
        }
        page {
          height: 100% !important;
          padding: 20mm 16mm;
        }
      }
      table { width: 100%; }
      h2, h3 { margin: 0px; }
      .logo { width: 130px; height: auto; margin: 0px auto;}
      .pull-right { float: right; }
      .text-center { text-align: center; }
    </style>
</head>

<body>
  <page size="A4">
    <table>
      <tr>
        <td colspan="2" class="text-center">
          <img class="logo" src="{{ asset('img/logo.png') }}" alt="">
        </td>
      </tr>
      <tr>
        <td style="padding-top: 30px;">
          KEPADA: <br>
          <span style="text-transform: capitalize;">{{ $data->supplier->name ?? '' }}</span> <br>
          {{ $data->supplier->addr ?? '' }} <br>
          {{ $data->supplier->province->province ?? '' }} <br><br>
          Fax: <br>
          Phone: <br>
          UP:
        </td>
        <td class="pull-right" style="padding-top: 30px;">
          <h2>DELOVELY</h2> <br>
          <h3>PURCHASE ORDER</h3> <br>
          No. PO: {{ $data->code }} <br>
          Requester: {{ $data->warehouse->name ?? '' }} <br>
          Currency: IDR <br>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <br><br>
          <table border="1" cellspacing="0" class="text-center">
            <tr style="font-size: 12px;">
              <th>NO</th>
              <th>KODE BARANG</th>
              <th>NAMA BARANG</th>
              <th>JUMLAH</th>
              <th>HARGA SATUAN</th>
              <th>JUMLAH HARGA</th>
              <th>DISKON</th>
              <th>PPN</th>
              <th>HARGA NET </th>
            </tr>
            @foreach($detail as $key => $list)
            <tr style="font-size: 12px;">
              <td>{{ $key+1 }}</td>
              <td>{{ "1102003000".($key+1) }}</td>
              <td>{{ $list->item->name ?? '' }}</td>
              <td>{{ $list->qty }}</td>
              <td>{{ "Rp.".number_format($list->price) }}</td>
              <td>{{ "Rp.".number_format($list->subtotal) }}</td>
              <td>-</td>
              <td>-</td>
              <td>{{ "Rp.".number_format($list->subtotal) }}</td>
            </tr>
            @endforeach
            <tr style="font-size: 12px; text-align: right;">
              @php
                $transdate = Carbon\Carbon::parse($data->transdate);
                $duedate = Carbon\Carbon::parse($data->duedate);
                $diff = $transdate->diffInDays($duedate);
              @endphp
              <td colspan="4" style="text-align: left;">Pembayaran: {{ $diff }} Net (Terms Date + {{ $diff }})</td>
              <td><b>JUMLAH</b></td>
              <td><b>{{ "Rp.".number_format($data->total) }}</b></td>
              <td></td>
              <td><b>-</b></td>
              <td><b>{{ "Rp.".number_format($data->total) }}</b></td>
            </tr>
          </table>
          <table style="margin-top: 20px;">
            <tr style="float: right; font-size: 14px; text-align: center;">
              <!-- <td>
                <br><br><br><br><br><br><br>
                Riky <br>
                Koordinator <br><br><br>
              </td> -->
              <td style="padding: 0px 20px;">
                <br><br><br><br><br><br><br>
                Nama Lengkap, S.Farm., Apt <br>
                SIPA NO: <br>
                {{ $data->sipa_number ?? '-' }} <br>
                <br>
                <!-- KU - Farmasi -->
              </td>
              <td>
                <!-- Muntok,  -->{{ date('d F Y', strtotime($data->transdate)) }} <br>
                Menyetujui
                <br><br><br><br><br><br>
                dr. Nama Lengkap <br>
                Chief Finance Officer <br><br><br>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </page>

  <script>
    window.print();
  </script>
</body>
</html>

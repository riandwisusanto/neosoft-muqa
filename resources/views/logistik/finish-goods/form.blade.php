<div class="modal" id="modal-{{ $route }}" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form data-toggle="validator" method="post">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times fa-lg"></i>
                    </button>
                    <h3 class="modal-title">Add {{ $title }}</h3>
                </div>
                <div class="modal-body" >
                    <div class="row">
                        <input type="hidden" id="id" name="id">
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="work_in_process_id" class="control-label">Work in process  *</label>
                            <select name="work_in_process_id" id="work_in_process_id" class="form-control">
                                <option value="" selected disabled>Select build of material</option>
                                @foreach ($work_in_processes as $r)
                                <option value="{{ $r->id }}">{{ $r->description }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-12 col-md-4">
                            <label for="item_id" class="control-label">Item  *</label>
                            <input type="hidden" id="item_id" name="item_id">
                            <input type="text" class="form-control" id="item_name" readonly>
                        </div>
                        <div class="form-group col-sm-12 col-md-4">
                            <label class="control-label">Order uom  *</label>
                            <input type="hidden" class="form-control" name="uom_id" id="uom_id" required>
                            <input type="text" class="form-control" name="uom_name" id="uom_name" readonly>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="qty" class="control-label">Qty</label>
                            <input type="text" name="qty" id="qty" class="form-control" readonly autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="price" class="control-label">Price</label>
                            <input type="text" name="price" id="price" class="form-control" readonly autocomplete="off">
                        </div>
                        
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="mfg_date" class="control-label">Mfg date *</label>
                            <input type="text" name="mfg_date" id="mfg_date" class="form-control datepicker" readonly autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="expired_date" class="control-label">Expire date  *</label>
                            <input type="text" name="expired_date" id="expired_date" class="form-control datepicker" required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="description" class="control-label">Description *</label>
                            <input type="text" name="description" id="description" class="form-control" required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12">
                            <label>Status</label>
                            <div>
                                <label><input type="radio" name="status" value="1" required id="status-1"> Available</label>
                                <label><input type="radio" name="status" value="2" required id="status-2"> Discontinued</label>
                                <label><input type="radio" name="status" value="3" required id="status-2"> Disposal</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
                </div>
            </form>
        </div>
    </div>
 </div>

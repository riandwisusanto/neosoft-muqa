@extends('base')

@section('title')
  List {{ $title }}
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
@endsection

@section('breadcrumb')
  @parent
  <li>{{ $title }}</li>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add {{ $title }}</a>
      </div>
      <div class="box-body">
        <table class="table table-striped table-bordered  nowrap table-city" style="width:100%">
        <thead>
          <tr>
            <th>Item</th>
            <th>Main&nbsp;rack&nbsp;code</th>
            <th>Sub&nbsp;rack&nbsp;code</th>
            <th>Werehouse</th>
            <th>Description</th>
            <th>Enable</th>
            <th class="all" width="10%">Action</th>
          </tr>
        </thead>
        <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@include('logistik.product-locations.form')
@endsection

@section('script')
<script type="text/javascript">
  var table, save_method;

  $(function(){
    table = $('.table-city').DataTable({
      "processing" : true,
      "serverside" : true,
      "ajax" : {
        "url" : "{{ route('product-locations.data') }}",
        "type" : "GET"
      }
    });

    $('#modal-product-locations form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
        var id = $('#id').val();
        if(save_method == "add") url = "{{ route('product-locations.store') }}";
        else url = "/product-locations/"+id;

        $.ajax({
          url : url,
          type : "POST",
          data : $('#modal-product-locations form').serialize(),
          success : function(data){
            console.log(data)
            $('#modal-product-locations').modal('hide');
            table.ajax.reload();
          },
          error : function(e){
            alert("Can not Save the Data!");
          }
        });
        return false;
      }
    });
  });
  $.fn.warehouseAjax = function(id_warehouse, id_rack){
    let _this = $(this);
    let rack = $('#' + _this.data('rack'));
    var id = id_warehouse ? id_warehouse : _this.val();
    var i_rack = id_rack ? id_rack : null;

    loadAjax(id);
    _this.change(function(){
      id = _this.val();
      loadAjax();
    })

    function loadAjax(){
      $.ajax({
        url: '{{ route('product-mutations.get-rack') }}',
        data: { warehouse_id: id },
        dataType: 'json',
        success: function (res) {
          let html = '';
          rack.html('')
          res.map(r => {
            html += `<option value="${r.id_rack}" ${i_rack == r.id_rack ? 'selected' : ''}>${r.main_rack_code} - ${r.sub_rack_code}</option>`;
          });
          rack.html(html).trigger('change');
        }
      });
    }
  }
  $('#warehouse_id').warehouseAjax();

  function addForm(){
    save_method = "add";
    $('input[name=_method]').val('POST');
    $("#modal-product-locations").modal({
      backdrop: 'static',
      keyboard: false,
      show: true
    });
    $('#modal-product-locations form')[0].reset();
    $('.modal-title').text('Add {{ $title }}');
    $('#active').removeAttr('checked');
    $('#notactive').removeAttr('checked');
  }

  function detailInfo(id) {
    
  }

  function editForm(id){
    save_method = "edit";
    $('input[name=_method]').val('PUT');
    $('#modal-product-locations form')[0].reset();
    $('#active').removeAttr('checked');
    $('#notactive').removeAttr('checked');

    $.ajax({
      url : "/product-locations/"+id+"/edit",
      type : "GET",
      dataType : "JSON",
      success : function(data){
        $('#modal-product-locations').modal('show');
        $('.modal-title').text('Edit {{ $title }}');
      
        $('#id').val(data.id);
        $('#item_id').val(data.item_id).trigger('change');
        $('#warehouse_id').val(data.warehouse_id).trigger('change');
        $('#description').val(data.description);
        $('#warehouse_id').warehouseAjax(data.warehouse_id, data.rack_id);

        if(data.is_active == 1){
          $('#active').attr('checked', true);
        }else{
          $('#notactive').attr('checked', true);
        }
      },
      error : function(){
        alert("Can not Show the Data!");
      }
    });
  }

  function deleteData(id){
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this Product location!",
      icon: "warning",
      buttons: {
        canceled:{
          text:'Cancel',
          value: 'cancel',
          className: 'swal-button btn-default'
        },
        deleted:{
          text:'Delete',
          value: 'delete',
          className: 'swal-button btn-danger'
        }
      },
      dangerMode: true,
    }).then((willDelete) => {
      switch (willDelete) {
        default:
          swal("{{ $title }} is safe!");
        break;
        case 'delete':
          $.ajax({
            url : "/product-locations/"+id,
            type : "POST",
            data : {'_method' : 'Delete', '_token' : $('meta[name=csrf-token]').attr('content')},
            success : function(data){
              if (data === "error") {
                swal({
                  text: '{{ $title }} is Used !',
                  icon: 'error'
                })
              } else {
                swal("{{ $title }} has been deleted!", {
                  icon: "success",
                });
              }                        
              table.ajax.reload();
            },
            error : function(){
              swal({
                text: 'Can not Delete the Data!',
                icon: 'error'
              })
            }
          });
        break;
      }
    });
  }
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>
@endsection

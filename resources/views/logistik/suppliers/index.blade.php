@extends('base')

@section('title')
List {{ $title }}
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
@endsection

@section('breadcrumb')
@parent
<li>{{ $title }}</li>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add {{ $title }}</a>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered  nowrap table-city" style="width:100%">
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Supplier Type</th>
                                <th>City</th>
                                <th>Address</th>
                                <th>Postal Code</th>
                                <th>Status</th>
                                <th class="all" width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>

@include('logistik.suppliers.form')
@endsection

@section('script')
<script type="text/javascript">
    var table, save_method, code = '{{ $code }}';

    $('#datepicker, #datepicker2').datepicker({
        format: 'dd MM yyyy',
        autoclose: true,
        todayHighlight: true
    });

    $(function () {
        table = $('.table-city').DataTable({
            "processing": true,
            "serverside": true,
            "ajax": {
                "url": "{{ route('suppliers.data') }}",
                "type": "GET"
            }
        });

        $('#modal-suppliers form').validator().on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                var id = $('#id').val();
                if (save_method == "add") url = "{{ route('suppliers.store') }}";
                else url = "/suppliers/" + id;

                $.ajax({
                    url: url,
                    type: "POST",
                    data: $('#modal-suppliers form').serialize(),
                    success: function (data) {
                        console.log(data)
                        $('#modal-suppliers').modal('hide');
                        table.ajax.reload();
                        code = data;
                    },
                    error: function (e) {
                        console.log(e)
                        alert("Can not Save the Data!");
                    }
                });
                return false;
            }
        });
    });

    function addForm() {
        save_method = "add";
        $('input[name=_method]').val('POST');
        $("#modal-suppliers").modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
        $('#modal-suppliers form')[0].reset();
        $('#active').attr('checked', '');
        $('.modal-title').text('Add {{ $title }}');
        $('#code').val(code);
    }

    function detailInfo(id) {

    }

    function editForm(id) {
        save_method = "edit";
        $('input[name=_method]').val('PUT');
        $('#modal-suppliers form')[0].reset();
        $('#active').removeAttr('checked');
        $.ajax({
            url: "/suppliers/" + id + "/edit",
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                console.log(data)
                $('#modal-suppliers').modal('show');
                $('.modal-title').text('Edit {{ $title }}');

                $('#id').val(data.model.id_supplier);
                $('#code').val(data.model.code);
                $('#name').val(data.model.name);
                $('#supplier_type').val(data.model.supplier_type);
                $('#post_code').val(data.model.post_code);
                $('#addr').val(data.model.addr);
                $('#province_id').val(data.model.province_id).change();
                $("#outlet").val(data.outlet).change()

                if (data.model.is_active == 1) {
                    $('#active').attr('checked', true);
                }
            },
            error: function () {
                alert("Can not Show the Data!");
            }
        });
    }

    function deleteData(id) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this {{ $title }}!",
            icon: "warning",
            buttons: {
                canceled: {
                    text: 'Cancel',
                    value: 'cancel',
                    className: 'swal-button btn-default'
                },
                deleted: {
                    text: 'Delete',
                    value: 'delete',
                    className: 'swal-button btn-danger'
                }
            },
            dangerMode: true,
        }).then((willDelete) => {
            switch (willDelete) {
                default:
                    swal("{{ $title }} is safe!");
                    break;
                case 'delete':
                    $.ajax({
                        url: "/suppliers/" + id,
                        type: "POST",
                        data: {
                            '_method': 'Delete',
                            '_token': $('meta[name=csrf-token]').attr('content')
                        },
                        success: function (data) {
                            if (data === "error") {
                                swal({
                                    text: '{{ $title }} is Used !',
                                    icon: 'error'
                                })
                            } else {
                                swal("{{ $title }} has been deleted!", {
                                    icon: "success",
                                });
                                code = data;
                            }
                            table.ajax.reload();
                        },
                        error: function () {
                            swal({
                                text: 'Can not Delete the Data!',
                                icon: 'error'
                            })
                        }
                    });
                    break;
            }
        });
    }

</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}">
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>
@endsection

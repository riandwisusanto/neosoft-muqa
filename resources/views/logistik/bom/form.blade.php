<div class="modal" id="modal-bom" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form data-toggle="validator" method="post">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times fa-lg"></i>
                    </button>
                    <h3 class="modal-title">Add {{ $title }}</h3>
                </div>
                <div class="modal-body" >
                    <div class="row">
                        <input type="hidden" id="id" name="id">
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="code" class="control-label">Code *</label>
                            <input type="text" id="code" class="form-control" disabled>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="prod_name" class="control-label">Prod name *</label>
                            <select name="item_id" id="item_id" class="form-control select2">
                                @foreach ($items as $r)
                                <option value="{{ $r->id_item }}">{{ $r->code }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="batch_no" class="control-label">Batch No *</label>
                            <input type="text" name="batch_no" id="batch_no" class="form-control" required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12 col-md-6">
                            <label for="amount" class="control-label">Amount *</label>
                            <input type="number" name="amount" id="amount" class="form-control" required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="description" class="control-label">Description *</label>
                            <input type="text" name="description" id="description" class="form-control" required autocomplete="off">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="post_code" class="control-label">Enable *</label>
                            <div>
                                <label><input type="checkbox" name="is_active" value="1" id="active" checked> Enable</label>
                            </div>
                        </div>
                        <style>
                            .table-custom th,
                            .table-custom td{
                                font-size: .9rem;
                            }
                        </style>
                        <div class="col-sm-12">
                            <input type="hidden" name="delId" id="del-po-item-id" value="[]">
                            <table class="table table-bordered table-custom">
                                <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Expire&nbsp;Date</th>
                                        <th>Uom</th>
                                        <th>Qty</th>
                                        <th>Price</th>
                                        <th>Total</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="bom_details"></tbody>
                                <tfoot id="tfoot-details">
                                    <tr>
                                        <td>
                                            <select id="item_id" class="form-control select2">
                                                @foreach ($items as $r)
                                                <option value="{{ $r->id_item }}">{{ $r->description }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" id="expire_date" class="form-control datepicker">
                                        </td>
                                        <td>
                                            <select id="uom_id" class="form-control select2">
                                                @foreach ($uom as $r)
                                                <option value="{{ $r->id_uom }}">{{ $r->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <input type="number" id="qty" class="form-control" autocomplete="off">
                                        </td>
                                        <td>
                                            <input type="number" id="price" class="form-control" autocomplete="off">
                                        </td>
                                        <td>
                                            <input type="number" id="total" class="form-control" readonly>
                                        </td>
                                        <td>
                                            <button class="btn btn-primary btn-sm" type="button" role="button" id="add-table-item"><i class="fa fa-plus"></i></button>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
                </div>
            </form>
        </div>
    </div>
 </div>

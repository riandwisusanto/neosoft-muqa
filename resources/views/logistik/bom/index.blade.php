@extends('base')

@section('title')
  List {{ $title }}
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
@endsection

@section('breadcrumb')
  @parent
  <li>{{ $title }}</li>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add {{ $title }}</a>
      </div>
      <div class="box-body">
        <table class="table table-striped table-bordered  nowrap table-city" style="width:100%">
        <thead>
          <tr>
            <th>Code</th>
            <th>Product code</th>
            <th>Description</th>
            <th>Batch no</th>
            <th>Enable</th>
            <th class="all" width="10%">Action</th>
          </tr>
        </thead>
        <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@include('logistik.bom.form')
@endsection

@section('script')
<script type="text/javascript">
  var table, save_method, code = '{{ $code }}';
  var items = {!! json_encode($items) !!};
  var uom   = {!! json_encode($uom) !!};
  var i = 1;

  $('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    todayHighlight: true
  });

  $(function(){
    table = $('.table-city').DataTable({
      "processing" : true,
      "serverside" : true,
      "ajax" : {
        "url" : "{{ route('bom.data') }}",
        "type" : "GET"
      }
    });

    $('#modal-bom form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
        var id = $('#id').val();
        if(save_method == "add") url = "{{ route('bom.store') }}";
        else url = "bom/"+id;

        $.ajax({
          url : url,
          type : "POST",
          data : $('#modal-bom form').serialize(),
          success : function(data){
            console.log(data)
            code = data;
            $('#modal-bom').modal('hide');
            table.ajax.reload();
            if(save_method == "add") code = data;
          },
          error : function(e){
            swal({
              title : 'Can not save the data',
              icon  : "warning",
            });
          }
        });
        return false;
      }
    });

    $('#qty, #price').on('change keyup', function(){
      let price = $('#price').val() ? parseInt($('#price').val()) : 0;
      let qty = $('#qty').val() ? parseInt($('#qty').val()) : 0;
      let subtotal = (price * qty);

      $('#total').val(subtotal);
    })

    $('#add-table-item').click(function(){
      let expire_date = $('#expire_date').val();
      let item_id = $('#item_id').val();
      let amount = $('#amount').val();
      let uom_id = $('#uom_id').val();
      let price = $('#price').val();
      let total = $('#total').val();
      let qty = $('#qty').val();

      let html = `<tr>
        <td>
          <select id="item_id_${i}" name="items[${i}][item_id]" class="form-control select2">${ itemsOption(item_id) }</select>
        </td>
        <td>
          <input type="text" id="expire_date_${i}" name="items[${i}][expire_date]" class="form-control datepicker" required>
        </td>
        <td>
          <select id="item_id_${i}" name="items[${i}][uom_id]" class="form-control select2">${ uomOption(uom_id) }</select>
        </td>
        <td>
          <input type="number" id="qty_${i}" name="items[${i}][qty]" class="form-control" required>
        </td>
        <td>
          <input type="number" id="price_${i}" name="items[${i}][price]" class="form-control" required>
        </td>
        <td>
          <input type="number" id="total_${i}" name="items[${i}][total]" class="form-control" required>
        </td>
        <td>
          <button class="btn btn-danger btn-sm delete-item" type="button" role="button"><i class="fa fa-times"></i></button>
        </td>
      </tr>`;

      var bom_details = $('#bom_details').append(html);
      $('.select2').select2();
      $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
      });

      var expire_date_aa = $(`#expire_date_${i}`).val(expire_date);
      var amount_aa = $(`#amount_${i}`).val(amount);
      var total_aa = $(`#total_${i}`).val(total);
      var price_aa = $(`#price_${i}`).val(price);
      var qty_aa = $(`#qty_${i}`).val(qty);
      
      subtotal();
      function subtotal () {
        price = price_aa.val() ? parseInt(price_aa.val()) : 0;
        qty = qty_aa.val() ? parseInt(qty_aa.val()) : 0;
        total = (price * qty);

        total_aa.val(total);
      }
      $(`#price_${i}, #qty_${i}`).on('change keyup', function(){
        subtotal();
      })
      i++;
      $('#tfoot-details').find('input').val('');
    })
  });

  $('body').on('click', '.delete-item', function(){
    let id = $(this).data('id');
    if(id){
      let delId = JSON.parse($('#del-po-item-id').val());
      delId = [...delId, id];
      $('#del-po-item-id').val(JSON.stringify(delId));
      console.log(delId)
    }
    $(this).parent().parent().remove();
  })

  function itemsOption(id){
    let html = '';
    items.map(r => {
      html += `<option value="${r.id_item}" ${id == r.id_item ? 'selected' : ''}>${r.description}</option>`;
    })
    return html;
  }

  function uomOption(id){
    let html = '';
    uom.map(r => {
      html += `<option value="${r.id_uom}" ${id == r.id_uom ? 'selected' : ''}>${r.name}</option>`;
    })
    return html;
  }

  function addForm(){
    save_method = "add";
    $('input[name=_method]').val('POST');
    $("#modal-bom").modal({
      backdrop: 'static',
      keyboard: false,
      show: true
    });
    $('#modal-bom form')[0].reset();
    $('.modal-title').text('Add {{ $title }}');
    $('#active').attr('checked', true);
    $('#bom_details').html('');
    $('#del-po-item-id').val('[]');
    $('#code').val(code);
  }

  function detailInfo(id) {
    
  }

  function editForm(id){
    save_method = "edit";
    $('input[name=_method]').val('PUT');
    $('#modal-bom form')[0].reset();
    $('#del-po-item-id').val('[]');

    $.ajax({
      url : "bom/"+id+"/edit",
      type : "GET",
      dataType : "JSON",
      success : function(data){
        $('#modal-bom').modal('show');
        $('.modal-title').text('Edit {{ $title }}');
      
        $('#id').val(data.id);
        $('#code').val(data.code);
        $('#item_id').val(data.item_id).trigger('change');
        $('#batch_no').val(data.batch_no);
        $('#amount').val(data.amount);
        $('#description').val(data.description);

        $('#bom_details').html('');

        if(data.is_active == 1){
          $('#active').attr('checked', true);
        }else{
          $('#active').removeAttr('checked');
        }
        let html = ''
        data.bom_details.map(r => {
           html = `<tr>
            <td>
              <input type="hidden" id="id_${i}" name="items[${i}][id]">
              <select id="item_id_${i}" name="items[${i}][item_id]" class="form-control select2">${ itemsOption(r.item_id) }</select>
            </td>
            <td>
              <input type="text" id="expire_date_${i}" name="items[${i}][expire_date]" class="form-control datepicker" required>
            </td>
            <td>
              <select id="item_id_${i}" name="items[${i}][uom_id]" class="form-control select2">${ uomOption(uom_id) }</select>
            </td>
            <td>
              <input type="number" id="qty_${i}" name="items[${i}][qty]" class="form-control" required>
            </td>
            <td>
              <input type="number" id="price_${i}" name="items[${i}][price]" class="form-control" required>
            </td>
            <td>
              <input type="number" id="total_${i}" name="items[${i}][total]" class="form-control" required>
            </td>
            <td>
              <button class="btn btn-danger btn-sm delete-item" type="button" role="button" data-id="${r.id}"><i class="fa fa-times"></i></button>
            </td>
          </tr>`;

          var bom_details = $('#bom_details').append(html);
          $('.select2').select2();
          $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true
          });

          // aa : after append
          var id_aa = $(`#id_${i}`).val(r.id);
          var qty_aa = $(`#qty_${i}`).val(r.qty);
          var total_aa = $(`#total_${i}`).val(r.total);
          var price_aa = $(`#price_${i}`).val(r.price);
          var amount_aa = $(`#amount_${i}`).val(r.amount);
          var expire_date_aa = $(`#expire_date_${i}`).val(r.expire_date);
          
          subtotal();
          function subtotal () {
            price = price_aa.val() ? parseInt(price_aa.val()) : 0;
            qty = qty_aa.val() ? parseInt(qty_aa.val()) : 0;
            total = (price * qty);

            total_aa.val(total);
          }
          $(`#price_${i}, #qty_${i}`).on('change keyup', function(){
            subtotal();
          })
          i++;
        })
      },
      error : function(){
        alert("Can not Show the Data!");
      }
    });
  }

  function deleteData(id){
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this {{ $title }}!",
      icon: "warning",
      buttons: {
        canceled:{
          text:'Cancel',
          value: 'cancel',
          className: 'swal-button btn-default'
        },
        deleted:{
          text:'Delete',
          value: 'delete',
          className: 'swal-button btn-danger'
        }
      },
      dangerMode: true,
    }).then((willDelete) => {
      switch (willDelete) {
        default:
          swal("{{ $title }} is safe!");
        break;
        case 'delete':
          $.ajax({
            url : "/bom/"+id,
            type : "POST",
            data : {'_method' : 'Delete', '_token' : $('meta[name=csrf-token]').attr('content')},
            success : function(data){
              if (data === "error") {
                swal({
                  text: '{{ $title }} is Used !',
                  icon: 'error'
                })
              } else {
                swal("{{ $title }} has been deleted!", {
                  icon: "success",
                });
                code = data;
              }
              table.ajax.reload();
            },
            error : function(){
              swal({
                text: 'Can not Delete the Data!',
                icon: 'error'
              })
            }
          });
        break;
      }
    });
  }
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>
@endsection

@extends('base')

@section('title')
Inactive Patient
@endsection

@section('breadcrumb')
@parent
<li>Inactive Patient</li>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-sm-8">
                        <span style="font-size:20px; font-weight: bold;">INACTIVE PATIENT</span>
                        <p style="font-size:15px; font-weight: bold;"><i>All inactive patients list.</i></p>
                    </div>
                    <div class="col-sm-4">
                        <div class="btn-float-right">
                            <a onclick="showFilter()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Change Period</a>
                            <a onclick="addWa()" class="btn btn-success"><i class="fa fa-whatsapp"></i> Add WA Blast</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped" style="width:100%">
                    <thead>
                        <tr>
                            <th>Patient</th>
                            <th>Reference no.</th>
                            <th>Contact</th>
                            <th>Email</th>
                            <th>Birthday</th>
                            <th>Last Invoice</th>
                            <th>Last Appointment</th>
                            <th>Last Usage</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@include('customerInactive.form')
@include('customerInactive.wa')
@endsection

@section('script')
<script type="text/javascript">
    var table, outlets = [], customers;
    $('#from, #to').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true
    });

    function selected_all() {
        if ($("#chkall").is(':checked')) {
            $("#outlet > option").prop("selected", "selected");
            $("#outlet").trigger("change");
        } else {
            $("#outlet").find('option').prop("selected", false);
            $("#outlet").trigger("change");
        }
    }

    $(function () {
        $('#modal-form form').validator().on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                table.ajax.url("{{ route('customerInactive.data') }}?" + $('#modal-form form')
                    .serialize()).load();
                customers = $('#range').val();
                outlets = $('#outlet').val();
                // console.log(outlets);
                $("#modal-form").hide();
            }
            return false;
        });

        table = $('.table').DataTable({
            "processing": true,
            "serverside": true,
            "responsive": true,
            "order": [1, 'asc'],

            "ajax": {
                "url": "{{ route('customerInactive.data') }}",
                "type": "GET"
            }
        });

        Filevalidation = () => {
            const fi = document.getElementById('fileName');
            // Check if any file is selected. 
            if (fi.files.length > 0) {
                for (let i = 0; i <= fi.files.length - 1; i++) {

                    const fsize = fi.files.item(i).size;
                    const file = Math.round((fsize / 1024));
                    // The size of the file. 
                    if (file >= 1024) {
                        swal("File too Big, please select a file less than 1mb")
                        // alert( 
                        //   "File too Big, please select a file less than 1mb");
                        fi.value = "";
                    }
                }
            }
        }
        $('#modal-message form').validator().on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                var id = $('#id').val();
                if (save_method == "add") url = "{{ route('customerInactive.store') }}";

                $.ajax({
                    url: url,
                    type: "POST",
                    data: new FormData($('#modal-message form')[0]),
                    processData: false,
                    contentType: false,
                    //  data : $('#modal-message form').serialize(),
                    success: function (data) {
                        swal({
                                title: "Successfully Create Blast",
                                icon: "success",
                                buttons: {
                                    canceled: {
                                        text: 'Cancel',
                                        value: 'cancel',
                                        className: 'swal-button btn-default'
                                    },
                                    sendwa: {
                                        text: 'Send WhatsApp',
                                        value: 'sendwa',
                                        className: 'swal-button btn-success'
                                    }
                                },
                                dangerMode: true,
                            })
                            .then((option) => {
                                switch (option) {
                                    default:
                                        window.location.href = "/customer-inactive";
                                        break;
                                    case 'sendwa':
                                        $.ajax({
                                            url: "/customer-inactive/" + data.id_message +
                                                "/wa",
                                            type: "GET",
                                            success: function (x) {
                                                if (data) {
                                                    swal({
                                                        text: 'Send WA Success',
                                                        icon: 'success'
                                                    })
                                                    window.location.href =
                                                        "/customer-inactive";
                                                }
                                                return false;
                                            },
                                            error: function () {
                                                swal({
                                                    text: 'Send WA Error',
                                                    icon: 'error'
                                                })
                                                return false;
                                            }
                                        });
                                        break;
                                }
                            });
                    },
                    error: function (e) {
                        console.log(e)
                        alert("Can not Save the Data!");
                    }
                });
                return false;
            }
        });
    });

    function showFilter() {
        $("#modal-form").show();
        $('#outlet').val(null).trigger('change');
        $('#modal-form form')[0].reset();
        $('.modal-title').text('Change Period');
    }
    
    function addWa() {
        if (customers != undefined) {
            $("#save").attr("disabled", false);
        } else {
            $("#save").attr("disabled", true);
        }
        save_method = "add";
        $('#customer').val(customers);
        $("#outlets").val(outlets);
        $('input[name=_method]').val('POST');
        $("#modal-message").modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
        $('#modal-message form')[0].reset();
        $('.modal-title').text('Add WA Blast');
    }
    

    function closeModal() {
        $("#modal-form").hide();
        $("#modal-message").hide();
    }

</script>
@endsection

<div class="modal" id="modal-message" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <form class="form-horizontal" id="form_data" data-toggle="validator" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }} {{ method_field('POST') }}
                <!-- @csrf -->


                <div class="modal-header">
                    <button type="button" class="close" onclick="closeModal()" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> &times; </span> </button>
                    <h3 class="modal-title"></h3>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <input type="hidden" id="customer" name="customer">
                    <input type="hidden" id="outlets" name="outlets">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Image</label>
                        <div class="col-md-6">
                            <input type="file" name="image" class="form-control" accept="image/*" id="fileName" onchange="Filevalidation()" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="desc" class="col-md-3 control-label">Message</label>
                        <div class="col-md-6">
                            <textarea type="text" class="form-control" id="message" name="message" placeholder="Message"
                                autocomplete="off" rows="5" required></textarea>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save" id="save"><i class="fa fa-floppy-o"></i> Save </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                            class="fa fa-arrow-circle-left"></i> Cancel</button>
                </div>

            </form>

        </div>
    </div>
</div>
<div class="modal" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <form class="form form-horizontal" id="form_data">
                {{ csrf_field() }}

                <div class="modal-header">
                    <button type="button" class="close" onclick="closeModal()" data-dismiss="modal"
                        aria-label="Close"><span aria-hidden="true"> &times; </span> </button>
                    <h3 class="modal-title">Periode Birthday</h3>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Range Date</label>
                        <div class="col-md-6">
                            <select id="range" name="range" class="form-control select2 select2-hidden-accessible" data-placeholder="Select a Outlet" style="width: 100%;"
                                tabindex="-1" aria-hidden="true">
                                <option value="0"><strong>One</strong> Years</option>
                                <option value="1"><strong>Six</strong> Months</option>
                                <option value="2"><strong>Three</strong> Months</option>
                                <option value="3"><strong>One</strong> Months</option>
                                <option value="4"><strong>One</strong> Weeks</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Outlet</label>
                        <div class="col-md-6">
                            <select id="outlet" name="outlet[]" class="form-control select2 select2-hidden-accessible"
                                multiple="multiple" data-placeholder="Select a Outlet" style="width: 100%;"
                                tabindex="-1" aria-hidden="true" required>
                                @foreach ($outlet as $list)
                                <option value="{{ $list->id_outlet }}">{{ $list->outlet_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                            <input id="chkall" onclick="selected_all()" type="checkbox">Select All
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" id="filter_submit" class="btn btn-primary btn-save"><i
                            class="fa fa-floppy-o"></i> Report
                    </button>
                    <button type="button" onclick="closeModal()" class="btn btn-warning" data-dismiss="modal"><i
                            class="fa fa-arrow-circle-left"></i> Cancel</button>
                </div>

            </form>

        </div>
    </div>
</div>
<div class="modal" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <form class="form form-horizontal" id="form_data">
                {{ csrf_field() }}

                <div class="modal-header">
                    <button type="button" class="close" onclick="closeModal()" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true"> &times; </span> </button>
                    <h3 class="modal-title">Periode Birthday</h3>
                </div>

                <div class="modal-body">

                    <div class="form-group">
                        <label for="from" class="col-md-3 control-label">Month</label>
                        <div class="col-md-6">
                            <select id="month" name="month[]" class="form-control select2 select2-hidden-accessible"
                                multiple="multiple" data-placeholder="Select a Month" style="width: 100%;"
                                tabindex="-1" aria-hidden="true">
                                    <option value="1">January</option>
                                    <option value="2">February</option>
                                    <option value="3">March</option>
                                    <option value="4">April</option>
                                    <option value="5">May</option>
                                    <option value="6">June</option>
                                    <option value="7">July</option>
                                    <option value="8">August</option>
                                    <option value="9">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Outlet</label>
                        <div class="col-md-6">
                            <select id="outlet" name="outlet[]" class="form-control select2 select2-hidden-accessible"
                                multiple="multiple" data-placeholder="Select a Outlet" style="width: 100%;"
                                tabindex="-1" aria-hidden="true">
                                @foreach ($outlet as $list)
                                    <option value="{{ $list->id_outlet }}">{{ $list->outlet_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                            <input id="chkall" onclick="selected_all()" type="checkbox">Select All
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" id="filter_submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Report
                    </button>
                    <button type="button" onclick="closeModal()" class="btn btn-warning" data-dismiss="modal"><i
                            class="fa fa-arrow-circle-left"></i> Cancel</button>
                </div>

            </form>

        </div>
    </div>
</div>

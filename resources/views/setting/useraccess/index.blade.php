@extends('base')

@section('title')
List User
@endsection

@section('breadcrumb')
@parent
<li>User</li>
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <span style="font-size:20px;">User Access</span>
                <p style="font-size:15px;"><i>Manajemen pengguna sistem. Hak mengaktifkan dan 
                menonaktifkan pengguna ada pada administrator.</i></p>
            </div>

            <div class="box-header with-border">
                <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add Users</a>
            </div>
            <div class="box-body">
                <table class="table table-striped table-bordered nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Fullname</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Branch</th>
                            <th>Enable</th>
                            <th class="all" width="10%">Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@include('setting.useraccess.form')
@endsection

@section('script')
<script type="text/javascript">
    var table, save_method;

  $(function(){

  table = $('.table').DataTable({
    scrollY:        "500px",
    scrollX:        true,
    scrollCollapse: true,
    paging:         false,
    fixedColumns:   {
        leftColumns: 1,
        rightColumns: 1
    },
    "ajax" : {
      "url" : "{{ route('useraccess.data') }}",
      "type" : "GET"
    }
  });

  $('#modal-useraccess form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
         var id = $('#id').val();
         if(save_method == "add") url = "{{ route('useraccess.store') }}";
         else url = "useraccess/"+id;

         $.ajax({
            url : url,
            type : "POST",
            data : $('#modal-useraccess form').serialize(),
           success : function(data){
              swal({
                  icon: "success"
              })
              $('#modal-useraccess').modal('hide');
              table.ajax.reload();
           },
           error : function(e){
              if (e.responseJSON.errors) {
                if (e.responseJSON.errors.email) {
                    $('#emailx').parents('.form-group').addClass('has-error').removeClass('has-success');
                    $('.emailx').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                    $('#email').text(e.responseJSON.errors.email).removeClass('hidden');
                  }
                    $('.alert').removeClass('hidden');
                    $('.alert').css('display', 'block').delay(10000).fadeOut();
                    $('#username').text(e.responseJSON.errors.username).removeClass('hidden');

              }
           }
         });
         return false;
     }
   });

});

function addForm(){
   save_method = "add";
   $('input[name=_method]').val('POST');
   $('#modal-useraccess').modal('show');
   $('#modal-useraccess form')[0].reset();
   $('.modal-title').text('Add User');
}

function editForm(id){
   save_method = "edit";
   $('input[name=_method]').val('PATCH');
   $('#modal-useraccess form')[0].reset();
   $.ajax({
     url : "useraccess/"+id+"/edit",
     type : "GET",
     dataType : "JSON",
     success : function(data){
       $('#modal-useraccess').modal('show');
       $('.modal-title').text('Edit User');

       $('#id').val(data.user.id);
       $('#fullname').val(data.user.name);
       $('#usernamex').val(data.user.username);
       $('#emailx').val(data.user.email);

       $('#role').val(data.level).change();
       $('#outlet').val(data.outlet).change();

       if (data.user.disc_approve == 1) {
        $('#disc_approve').val(data.user.disc_approve).attr('checked', true);
       } else {
        $('#disc_approve').val(data.user.disc_approve).attr('checked', false);
       }

       if (data.user.status_user == 1) {
        $('#status_user').val(data.user.status_user).attr('checked', true);
       } else {
        $('#status_user').val(data.user.status_user).attr('checked', false);
       }

     },
     error : function(){
       alert("Can not Show the Data!");
     }
   });
}

function resetPassword(id){
  if(confirm("Are you sure the password will be Reset?")){
    $.ajax({
      url : "{{route('useraccess.index')}}/reset",
      type : "POST",
      data : {'_method' : 'POST', '_token' : $('meta[name=csrf-token]').attr('content'),'user_id':id},
      success : function(data){
        alert("Reset Password Successfully!");
        table.ajax.reload();
      },
      error : function(e){
        alert("Can not Reset Password!");
      }
    });
  }
}

function deleteData(id){
  if(confirm("Are you sure the data will be Delete?")){
     $.ajax({
       url : "useraccess/"+id,
       type : "POST",
       data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
       success : function(data){
         table.ajax.reload();
       },
       error : function(){
         alert("Can not Delete the Data!");
       }
     });
   }
}
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}">
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>
@endsection

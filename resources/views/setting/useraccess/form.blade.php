<div class="modal" id="modal-useraccess" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <form class="form-horizontal" data-toggle="validator" method="post">
                {{ csrf_field() }} {{ method_field('POST') }}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times fa-lg"></i>
                    </button>
                    <h3 class="modal-title"></h3>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger hidden">
                        <ul>
                            <li id="email" class="hidden"></li>
                            <li id="username" class="hidden"></li>
                        </ul>
                    </div>
                    <input type="hidden" id="id" name="id">
                    <div class="form-group">
                        <label for="fullname" class="col-md-3 control-label">Full Name *</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="fullname" name="fullname"
                                placeholder="Full Name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="username" class="col-md-3 control-label">Username</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="usernamex" name="username"
                                placeholder="Username" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-md-3 control-label">Email</label>
                        <div class="col-md-8">
                            <input id="emailx" type="email" class="form-control" name="email" value="{{ old('email') }}"
                                required autocomplete="email" placeholder="Email">
                        </div>
                    </div>
                    @if (!array_intersect(["SUPER_USER"], json_decode(Auth::user()->level)))
                    <div class="form-group">
                        <label for="role" class="col-md-3 control-label">Role</label>
                        <div class="col-md-8">
                            <select style="width:100%" class="form-control select2 select2-hidden-accessible"
                                multiple="multiple" data-placeholder="Select a Role" style="width: 100%;" tabindex="-1"
                                aria-hidden="true" required id="role" name="role[]" required>
                                <option value="FRONTDESK">Frontdesk</option>
                                <option value="ADMINISTRATOR">Administrator</option>
                                <option value="CASHIER">Cashier</option>
                                <option value="BRANCH_MANAGER">Branch Manager</option>
                                <option value="DOKTER">Dokter</option>
                                <option value="THERAPIST">Therapist</option>
                                <option value="FINANCE">Finance</option>
                                <option value="OUTLET_SUPERVISOR">Outlet Supervisor</option>
                                <option value="PHARMACY">Pharmacy</option>

                            </select>
                        </div>
                    </div>
                    @else
                    <div class="form-group">
                        <label for="role" class="col-md-3 control-label">Role</label>
                        <div class="col-md-8">
                            <select style="width:100%" class="form-control select2 select2-hidden-accessible"
                                multiple="multiple" data-placeholder="Select a Role" style="width: 100%;" tabindex="-1"
                                aria-hidden="true" required id="role" name="role[]" required>
                                <option value="SUPER_USER">Super User</option>
                                <option value="ADMINISTRATOR">Administrator</option>
                                <option value="FRONTDESK">Frontdesk</option>
                                <option value="CASHIER">Cashier</option>
                                <option value="BRANCH_MANAGER">Branch Manager</option>
                                <option value="DOKTER">Dokter</option>
                                <option value="THERAPIST">Therapist</option>
                                <option value="FINANCE">Finance</option>
                                <option value="OUTLET_SUPERVISOR">Outlet Supervisor</option>
                                <option value="PHARMACY">Pharmacy</option>

                            </select>
                        </div>
                    </div>
                    @endif
                    <div class="form-group">
                        <label class="col-md-3 control-label">Branch</label>
                        <div class="col-md-8">
                            <select id="outlet" name="outlet[]" class="form-control select2 select2-hidden-accessible"
                                multiple="multiple" data-placeholder="Select a Outlet" style="width: 100%;"
                                tabindex="-1" aria-hidden="true" required>
                                @foreach ($outlet as $list)
                                <option value="{{ $list->id_outlet }}">{{ $list->outlet_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="approveble" class="col-sm-3 control-label">Enable</label>
                        <div class="col-sm-8">
                            <input type="checkbox" name="status_user" id="status_user">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                            class="fa fa-arrow-circle-left"></i> Cancel</button>
                </div>

            </form>

        </div>
    </div>
</div>

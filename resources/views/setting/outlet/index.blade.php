@extends('base')

@section('title')
List Outlet
@endsection

@section('breadcrumb')
@parent
<li>Outlet</li>
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add Outlets</a>
            </div>
            <div class="box-body">
                <table class="table table-striped table-bordered nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th width="10">#</th>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Phone</th>
                            <th>Fax</th>
                            <th>Status</th>
                            <th>Logo</th>
                            <th>Address</th>
                            <th class="all" width="10%">Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@include('setting.outlet.form')
@endsection

@section('script')
<script type="text/javascript">
    var table, save_method;

  $(function(){

  table = $('.table').DataTable({
    scrollY:        "500px",
    scrollX:        true,
    scrollCollapse: true,
    paging:         false,
    bSort:false,
    dom: 'Bfrtip',
    fixedColumns:   {
        leftColumns: 2,
        rightColumns: 1
    },
    "ajax" : {
      "url" : "{{ route('outlets.data') }}",
      "type" : "GET"
    }
  });

  $('#modal-outlets form').validator().on('submit', function(e){

      if(!e.isDefaultPrevented()){
         var id = $('#id').val();
         if(save_method == "add") url = "{{ route('outlets.store') }}";
         else url = "outlets/"+id;
         $.ajax({
            url : url,
            type : "POST",
            data : new FormData($('#modal-outlets form')[0]),
            cache:false,
            contentType: false,
            processData: false,
           success : function(data){
              $('#modal-outlets').modal('hide');
              table.ajax.reload();
           },
           error : function(e){
             console.log(e);
             alert("Can not Save the Data!");
           }
         });
         return false;
     }
   });

});

function addForm(){
   save_method = "add";
   $('input[name=_method]').val('POST');
   $('#modal-outlets').modal('show');
   $('#modal-outlets form')[0].reset();
   $('.modal-title').text('Add Outlet');
}

function editForm(id){
   save_method = "edit";
   $('input[name=_method]').val('PATCH');
   $('#modal-outlets form')[0].reset();
   $.ajax({
     url : "outlets/"+id+"/edit",
     type : "GET",
     dataType : "JSON",
     success : function(data){
       $('#modal-outlets').modal('show');
       $('.modal-title').text('Edit Outlet');
       $('#id').val(data.id_outlet);
       $('#name').val(data.outlet_name);
       $('#code').val(data.outlet_code);
       $('#address').text(data.address);
       $('#telephone').val(data.outlet_phone);
       $('#fax').val(data.outlet_fax);
       $('.tampil-logo').html('<img src="storage/'+data.logo+'" class="img-responsive">');
     },
     error : function(){
       alert("Can not Show the Data!");
     }
   });
}

function deleteData(id){
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this Outlet!",
        icon: "warning",
        buttons: {
            canceled:{
                text:'Cancel',
                value: 'cancel',
                className: 'swal-button btn-default'
            },
            deleted:{
                text:'Delete',
                value: 'delete',
                className: 'swal-button btn-danger'
            }
        },
        dangerMode: true,
    }).then((willDelete) => {
        switch (willDelete) {
            default:
                swal("Outlet is safe!");
                break;
            case 'delete':
                $.ajax({
                    url : "outlets/"+id,
                    type : "POST",
                    data : {'_method' : 'Delete', '_token' : $('meta[name=csrf-token]').attr('content')},
                    success : function(data){
                        if (data === "error") {
                            swal({
                                text: 'Outlet Is Used!',
                                icon: 'error'
                            })
                        }else{
                            swal("Outlet has been deleted!", {
                                icon: "success",
                            });
                        }
                        table.ajax.reload();
                    },
                    error : function(){
                        swal({
                            text: 'Can not Delete the Data!',
                            icon: 'error'
                        })
                    }
                });
                break;
        }
    });
}
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}">
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>
@endsection

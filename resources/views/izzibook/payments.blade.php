@extends('base')

@section('title')
Izzibook | Invoices
@endsection

@section('breadcrumb')
@parent
<li>Izzibook</li>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <span style="font-size:20px;">Izzibook Sync (Sales Payments)</span>
                <p style="font-size:15px;"><i>Catatan komunikasi POS Neosoft dengan Izzibook modul pembayaran penjualan.</i></p>
            </div>
            <div class="box-body">
                <table class="table table-all table-striped table-bordered dt-responsive nowrap" id="izzibookTable" style="width:100%!important;">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Endpoint</th>
                            <th>Request Time</th>
                            <th>Method</th>
                            <th>Action</th>
                            <th>Payload</th>
                            <th>Response Body</th>
                            <th>Response Code</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
    $(document).ready( function () {
        $('#izzibookTable').DataTable({
            "processing" : true,
            "serverSide" : true,
            'language': {
                'loadingRecords': '&nbsp;',
                'processing': 'Memuat data...'
            },
            "responsive" : {
                "details" : {
                    "type" : 'column',
                    "target" : 'tr'
                }
            },
            "columns":[
                {
                    "data": "divider"
                },
                {
                    "data": "endpoint"
                },
                {
                    "data": "req_time"
                },
                {
                    "data": "method"
                },
                {
                    "data": "action"
                },
                {
                    "data": "payload"
                },
                {
                    "data": "response_body"
                },
                {
                    "data": "response_code"
                },
            ],
            "columnDefs" : [ {
                "className" : 'control',
                "orderable" : false,
                "targets" :   0
            } ],
            "order" : [ 2, 'desc' ],
            "ajax" : {
                "url" : "{{ route('izzibook.payments.data') }}",
                "type" : "GET"
            },
        });
    } );

    function retry(code) {
        $.ajax({
            url: "/izzibook/payments/" + code,
            type: "GET",
            beforeSend: function () {
                swal({
                    title: 'Menunggu',
                    html: 'Memproses data',
                    onOpen: () => {
                        swal.showLoading()
                    }
                })
            },
            success: function (data) {
                console.log(data)
                swal({
                    title: 'Berhasil!',
                    icon: 'success'
                }).then(function () {
                    window.location.href = "/izzibook/payments";
                });
            },
            error: function (e) {
                console.log(e)
                swal({
                    title: 'Terjadi kesalahan',
                    icon: 'error'
                })
            }
        });
        return false;
    }
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}"></script>
@endsection

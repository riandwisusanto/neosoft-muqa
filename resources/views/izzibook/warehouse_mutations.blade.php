@extends('base')

@section('title')
Izzibook | Invoices
@endsection

@section('breadcrumb')
@parent
<li>Izzibook</li>
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <span style="font-size:20px;">Izzibook Sync (Stock Mutation)</span>
                <p style="font-size:15px;"><i>Catatan komunikasi POS Neosoft dengan Izzibook modul stock mutations.</i></p>
            </div>
            <div class="box-body">
                <table class="table" id="izzibookTable" style="word-break: break-word; width:100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Action</th>
                            <th>Payload</th>
                            <th>Response Body</th>
                            <th>Response Code</th>
                            <th>Request Time</th>
                            <th>Endpoint</th>
                            <th>Method</th>
                            <th class="all">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $index => $value)
                            @php 
                                $payload = json_decode($value->payload);
                                $invoiceCode = $payload->{"InvoiceNo"};
                                if ($value->response_body !== "-") {
                                    $response_body = json_decode($value->response_body);
                                    $response_status = $response_body->{"status"};
                                } else {
                                    $response_status = 0;
                                }
                            @endphp
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td>{{ $value->action }}</td>
                                <td>{{ $value->payload }}</td>
                                <td>{{ $value->response_body }}</td>
                                <td>{{ $value->response_code }}</td>
                                <td>{{ $value->req_time }}</td>
                                <td>https://neosoft.izzibook.co.id{{ $value->endpoint }}</td>
                                <td>{{ $value->method }}</td>
                                <td>
                                    @if ( $response_status == 0 )
                                        <button type="button" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Retry" onclick="retry(`{{ $invoiceCode }}`)"><i class="fa fa-refresh text-white"></i></button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript">
    $(document).ready( function () {
        $('#izzibookTable').DataTable({
            autoWidth: false,
            columns : [
                {width: '5%'},
                {width: '8%'},
                {width: '21%'},
                {width: '15%'},
                {width: '10%'},
                {width: '11%'},
                {width: '12%'},
                {width: '10%'},
                {width: '8%'},
            ]
        });
    } );

    function retry(code) {
        $.ajax({
            url: "/izzibook/warehouse_mutations/" + code,
            type: "GET",
            beforeSend: function () {
                swal({
                    title: 'Menunggu',
                    html: 'Memproses data',
                    onOpen: () => {
                        swal.showLoading()
                    }
                })
            },
            success: function (data) {
                console.log(data)
                swal({
                    title: 'Berhasil!',
                    icon: 'success'
                }).then(function () {
                    window.location.href = "/izzibook/warehouse_mutations";
                });
            },
            error: function (e) {
                console.log(e)
                swal({
                    title: 'Terjadi kesalahan',
                    icon: 'error'
                })
            }
        });
        return false;
    }
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}"></script>
@endsection

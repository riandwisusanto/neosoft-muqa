@extends('dr-review.base')
@section('content')
<section class="content" style="margin-top: 30px">
  <div class="box">
    <div class="box-header with-border">
          <span style="font-size:20px;">Patients History</span>
          <p style="font-size:15px;"><i>Data riwayat foto before dan after pasien.</i></p>
          <a href="{{ route('dr-review.index') }}" class="btn btn-warning" style="float: right;"><i class="fa fa-arrow-circle-left"></i> Back</a>
    </div>
    <div class="box-body">
      <table class="table table-bordered table-striped" id="table">
        <thead>
          <tr>
            <th style="width: 10%">No</th>
            <th>Patient Name</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @php($no = 1)
          @foreach ($customers as $r)
            <tr>
              <td>{{ $no }}</td>
              <td>{{ $r->full_name }}</td>
              <td><a href="{{ route('dr-review.history.detail', $r->id_customer) }}" class="btn btn-success">Detail</a></td>
            </tr>
            @php($no++)
          @endforeach
        </tbody>
      </table>
    </div>
    <div class="box-footer"></div>
  </div>
</section>
@endsection

@section('script')
<script>
  $(document).ready(function () {
    $('#table').DataTable({
      responsive : true
    })
    $('.select2').select2();
  });
</script>
@endsection

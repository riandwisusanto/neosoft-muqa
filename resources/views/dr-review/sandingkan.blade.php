@extends('dr-review.base')
@section('style')
<style>
  .w-100{
    width: 100%;
  }
  .carousel{
    position: relative;
    overflow: hidden;
  }
  .carousel .action{
    position: absolute;
    top: calc(50% - 15px);
    background-color: transparent;
    font-size: 30px;
    border: none;
    z-index: 99;
    color: #fff;
    outline: none;
    text-shadow: 0px 6px 8px rgba(2, 2, 2, .2);
  }
  .carousel .action.next{
    right: 10px;
  }
  .carousel .action.prev{
    left: 10px;
  }
</style>
@endsection
@section('content')
<section class="content" style="margin-top: 30px">
  <div class="box">
    <div class="box-header with-border">
        <span style="font-size:20px;">Before and After</span>
        <p style="font-size:15px;"><i>Form perbandingan foto before dan foto after pasien.</i></p>
    </div>
    <div class="box-body">
      <div class="row" style="padding: 8px;">
        <div class="col-sm-12">
          <div class="form-group">
            <label>Patient Name</label>
            <select name="customer_id" id="customer_id" class="form-control select2">
              <option selected>Choose Patient</option>
              @foreach ($customers as $r)
              <option value="{{ $r->id_customer }}">{{ $r->full_name }} - {{ $r->induk_customer }}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-sm-12" id="before" style="display: none;">
          <div class="form-group">
            <label>Choose Photo before</label>
            <select name="before" id="before_option" class="form-control"></select>
          </div>
        </div>
        <div class="col-sm-12" id="after" style="display: none;">
          <div class="form-group">
            <label>Choose Photo after</label>
            <select name="after" id="after_option" class="form-control"></select>
          </div>
        </div>
        <div class="col-sm-12 col-md-6">
          <label>Photo Before</label>
          <div id="carousel-before"  class="carousel">
            <button class="action prev"><span class="glyphicon glyphicon-chevron-left"></span></button>
            <button class="action next"><span class="glyphicon glyphicon-chevron-right"></span></button>
            <div class="owl-carousel owl-theme" id="owl-carousel-before"></div>
          </div>
        </div>
        <div class="col-sm-12 col-md-6">
          <label>Photo After</label>
          <div id="carousel-after"  class="carousel">
            <button class="action prev"><span class="glyphicon glyphicon-chevron-left"></span></button>
            <button class="action next"><span class="glyphicon glyphicon-chevron-right"></span></button>
            <div class="owl-carousel owl-theme" id="owl-carousel-after"></div>
          </div>
        </div>
        <div class="col-sm-12" style="margin-top: 30px;">
          <div class="form-group">
            <button class="btn btn-primary" onclick="reset()">Reset</button>
          </div>
        </div>
      </div>
    </div>
    <div class="box-footer"></div>
  </div>
</section>
@endsection

@section('script')
<script>
  function reset(){
    $('#before, #after').css('display', 'none');
    $('#before_option, #after_option').html('')
    $('#carousel-before, #carousel-after').css('display', 'none');
  }
  $(document).ready(function () {
    $('.select2').select2();
    $('.carousel').carousel();
    $('#customer_id').change(function(){
      var id = $(this).val();
      $.ajax({
        type     : 'post',
        url      : '{{ route('dr-review.sandingkan') }}',
        data     : {
          id: id,
          _token: '{{ csrf_token() }}'
        },
        dataType : 'json',
        success  : function (res) {
          var before = '<option selected disabled>Choose Photo</option>';
          var after  = '<option selected disabled>Choose Photo</option>';
          $.each(res.data, function (i, v) {
            if(v.desc == 'Before'){
              before += `<option value="${v.id}">Before (${v.date_in})</option>`;
            }else{
              after += `<option value="${v.id}">After (${v.date_in})</option>`;
            }
          });

          $('#before_option').html(before);
          $('#after_option').html(after);
          $('#before, #after').css('display', 'block');
        }
      });
    })
    $('#before_option').change(function(e){
      var id = $(this).val();
      $.ajax({
        type     : 'post',
        url      : '{{ route('dr-review.sandingkan.photo') }}',
        data     : {
          id: id,
          _token: '{{ csrf_token() }}'
        },
        dataType : 'json',
        success  : function (res) {
          if(res.success){
            var item = '';
            $('#carousel-before').css('display', 'block');
            $.each(res.data, function (i, v) {
              item += `<div><img src="${v}" alt="..." class="img-thumbnail img-responsive"></div>`;

            });
            $('#owl-carousel-before').html(item)
            .owlCarousel({
              items: 1,
              loop: true,
            });
            $('#carousel-before .action.next').click(function(e){
              e.preventDefault();
              $('#owl-carousel-before').trigger('next.owl.carousel');
            })
            $('#carousel-before .action.prev').click(function(e){
              e.preventDefault();
              $('#owl-carousel-before').trigger('prev.owl.carousel');
            })
          }
        }
      });
    })

    $('#after_option').change(function(e){
      var id = $(this).val();
      $.ajax({
        type     : 'post',
        url      : '{{ route('dr-review.sandingkan.photo') }}',
        data     : {
          id: id,
          _token: '{{ csrf_token() }}'
        },
        dataType : 'json',
        success  : function (res) {
          if(res.success){
            var item = '';
            $('#carousel-after').css('display', 'block');
            $.each(res.data, function (i, v) {
              item += `<div><img src="${v}" alt="..." class="img-thumbnail img-responsive"></div>`;

            });
            $('#owl-carousel-after').html(item)
            .owlCarousel({
              items: 1,
              loop: true,
            });
            $('#carousel-after .action.next').click(function(e){
              e.preventDefault();
              $('#owl-carousel-after').trigger('next.owl.carousel');
            })
            $('#carousel-after .action.prev').click(function(e){
              e.preventDefault();
              $('#owl-carousel-after').trigger('prev.owl.carousel');
            })
          }
        }
      });
    })
  });
</script>
@endsection

@extends('dr-review.base')
@section('content')
<section class="content" style="margin-top: 30px">
  <div class="box">
    <div class="box-header with-border">
          <span style="font-size:20px;">Patients Review</span>
          <p style="font-size:15px;"><i>Form dokter/terapis/BC untuk upload foto before dan after pasien.</i></p>
    </div>
    <div class="box-body">
      <form action="{{ route($route.'upload') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row" style="padding: 8px;">
          <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
              <label>Patient Name</label>
              <select name="customer_id" class="form-control select2">
                <option selected disabled>Choose Patient</option>
                @foreach ($customers as $r)
                <option value="{{ $r->id_customer }}">{{ $r->full_name }} - {{ $r->induk_customer }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col-sm-12 col-md-6 col-lg-4">
            <div class="form-group">
              <label>Choose Photo</label>
              <input type="file" name="image[]" class="form-control" multiple required>
              <p class="help-block">Format file jpg, png (can Upload many Photos at once)</p>
            </div>
          </div>
          <div class="col-md-12 col-lg-4">
            <div class="form-group">
              <label>Information</label>
              <div class="radio">
                <label>
                  <input type="radio" name="desc" value="Before">
                    Berfore
                </label>
              </div>
              <div class="radio">
                <label>
                  <input type="radio" name="desc" value="After">
                  After
                </label>
              </div>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <label>Remark</label>
              <textarea name="remarks" class="form-control" rows="4" required></textarea>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <button class="btn btn-primary">Submit</button>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="box-footer"></div>
  </div>
</section>
@endsection

@section('script')
<script>
  $(document).ready(function () {
    $('.select2').select2();
  });
</script>
@endsection

@extends('dr-review.base')
@section('content')
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Detail Photo</h4>
      </div>
      <div class="modal-body">
        <img src="" alt="" id="imgDetail" style="width: 100%;">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>Close</button>
      </div>
    </div>
  </div>
</div>
<section class="content" style="margin-top: 30px">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Show Photo</h3>
      <a href="{{ route('dr-review.history') }}" class="btn btn-warning" style="float: right;"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
    </div>
    <div class="box-body">
      <table class="table table-bordered table-striped" id="table">
        <thead>
          <tr>
            <th style="width: 10%">No</th>
            <th>Photo</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @php($no = 1)
          @foreach ($data->details as $r)
            <tr>
              <td>{{ $no }}</td>
              <td><img src="{{ asset($data->folder.'/'.$r->image) }}" alt="" style="width: 100px;"></td>
              <td>
                <button data-toggle="modal" data-target="#myModal" data-img="{{ asset($data->folder.'/'.$r->image) }}" class="btn btn-success"><i class="fa fa-eye"></i> Perbesar</button>
              </td>
            </tr>
            @php($no++)
          @endforeach
        </tbody>
      </table>
    </div>
    <div class="box-footer"></div>
  </div>
</section>
@endsection

@section('script')
<script>
  $(document).ready(function () {
    $('#table').DataTable({
      responsive : true
    })
    $('#myModal').on('show.bs.modal', function (e) {
      var button = $(e.relatedTarget);
      var img = button.data('img');
      $('#imgDetail').attr('src', img);
    })
    $('.select2').select2();
    $('body').on('click', '.delete-confirmation', function(e){
      e.preventDefault();
      swal({
        title             : 'Delete this the Data?',
        text              : "Data cannot be restored if it has been erased",
        type              : 'warning',
        showCancelButton  : true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor : '#d33',
        confirmButtonText : 'Yes, Delete Succesfully!'
      }).then((result) => {
        if (result) {
          $(this).parent().submit();
          // alert('asd');
        }
      })
    });
  });
</script>
@endsection

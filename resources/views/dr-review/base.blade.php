<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Dokter Review</title>
  <link rel="icon" href="{{ asset('img/logo.png') }}" type="image/x-icon">
  <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('font-awesome/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/select2/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/css/skins/_all-skins.min.css') }}">

  <link rel="stylesheet" href="{{ asset('plugins/DataTables/DataTables/css/dataTables.bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/DataTables/responsive/css/responsive.bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/DataTables/fixedHeader/css/fixedHeader.bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/DataTables/fixedHeader/css/fixedHeader.dataTables.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/owl-carousel2/assets/owl.carousel.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/owl-carousel2/assets/owl.theme.default.min.css') }}">
  @yield('style')

</head>
<body class="hold-transition skin-blue layout-top-nav">
  <div class="wrapper" style="height: auto; min-height: 100%;">
    @include('dr-review.inc.header')
    <div class="content-wrapper" style="min-height: 536px;">
      <div class="container">
        @yield('content')
      </div>
    </div>
    @include('dr-review.inc.footer')
  </div>

  <script src="{{ asset('plugins/jQuery/jquery.min.js') }}"></script>
  <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('plugins/DataTables/DataTables/js/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('plugins/DataTables/DataTables/js/dataTables.bootstrap.min.js') }}"></script>
  <script src="{{ asset('plugins/DataTables/responsive/js/dataTables.responsive.min.js') }}"></script>
  <script src="{{ asset('plugins/DataTables/responsive/js/responsive.bootstrap.min.js') }}"></script>
  <script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
  <script src="{{ asset('plugins/sweetalert/sweetalert.min.js') }}"></script>
  <script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
  <script src="{{ asset('plugins/owl-carousel2/owl.carousel.min.js') }}"></script>

  @include('dr-review.inc.alert')
  @yield('script')
</body>
</html>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
      {{-- <b>Version</b> 1.0.0 --}}
    </div>
    <strong>Copyright &copy; 2019 <a href="https://www.neosoft.co.id/home/">PT NEOSOFT TEKNOLOGI ASIA</a>.</strong> All rights
    reserved.
  </footer>

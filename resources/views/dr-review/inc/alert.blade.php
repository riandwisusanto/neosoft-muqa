@if (count($errors->all()))
<script>
var errors = {!! json_encode($errors->all()) !!};
var html = '<p>';
errors.forEach(function(v, i){
  html += v + '<br>';
})
html += '</p>';
swal('Validasi gagal!', html, 'error')
</script>
@endif

@if ($succ = session()->get('swal_success'))
<script>
swal('Successfully!', '{{ $succ }}', 'success');
</script>
@endif


@if ($er = session()->get('swal_error'))
<script>
swal('Failed!', '{{ $er }}', 'error');
</script>
@endif

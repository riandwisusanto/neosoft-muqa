<header class="main-header">
  <nav class="navbar navbar-static-top">
    <div class="container">
      <div class="navbar-header">
        <a href="{{ route('dr-review.index') }}" class="navbar-brand"><b>Dokter</b> Review</a>
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
          <i class="fa fa-bars"></i>
        </button>
      </div>

      <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
        <ul class="nav navbar-nav">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Menu <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="{{ route('dr-review.upload') }}">Patients Review</a></li>
              <li><a href="{{ route('dr-review.history') }}">Patients History</a></li>
              <li><a href="{{ route('dr-review.sandingkan') }}">Before and After</a></li>
              <li class="divider"></li>
              <li><a href="#!" onclick="document.getElementById('logout-form').submit()">Logout</a></li>
            </ul>
          </li>
          <li>
              <a href="/">{{ config('app.name') }} <strong>POS</strong><span class="sr-only">POS</span></a>
          </li>
        </ul>
      </div>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown tasks-menu"></li>
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              <span class="hidden-xs">{{ auth()->user()->name }}</span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
                <p>{{ auth()->user()->name }}</p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                </div>
                <div class="pull-right">
                  <a href="#!" onclick="document.getElementById('logout-form').submit()" class="btn btn-default btn-flat">Sign out</a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                  </form>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</header>

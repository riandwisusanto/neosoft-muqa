<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>POS Agent</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('font-awesome/css/font-awesome.min.css') }}">
  {{-- select --}}
  <link rel="stylesheet" href="{{ asset('plugins/select2/select2.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">

  <link rel="stylesheet" href="{{ asset('plugins/iCheck/square/blue.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('dist/css/skins/_all-skins.min.css') }}">
  <link href='{{ asset('css/style.css') }}' rel='stylesheet' />

  <link href='{{ asset('css/fontGoogle.css') }}' rel='stylesheet' />

</head>

<body class="hold-transition register-page">
  <div class="register-box">
    <div class="register-logo">
      <a href="../../index2.html"><b>POS</b>Agent</a>
    </div>
    <div class="register-box-body">
      <p class="login-box-msg">Register a Client Agent</p>

      <div class="alert alert-danger hidden">
          <ul>
            <li id="email" class="hidden"></li>
            <li id="phone" class="hidden"></li>
          </ul>
      </div>

      <form id="form-created" data-toggle="validator" method="post">
        {{ csrf_field() }} {{ method_field('POST') }}
        <div class="form-group has-feedback">
          <input type="text" class="form-control" name="agent_code" id="agent_code" placeholder="Agent Code" autocomplete="off" readonly>
          <span class="glyphicon glyphicon-barcode form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="text" class="form-control" name="name" id="name" placeholder="Full name" required autocomplete="off">
          <div id="suggesstion-box"></div>
          <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="email" name="email" id="emailx" class="form-control" placeholder="Email" required>
          <span class="glyphicon emailx glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="text" class="form-control" id="phonex" name="phone" placeholder="Phone" required>
          <span class="glyphicon phonex glyphicon-phone form-control-feedback"></span>
        </div>

        <div class="form-group has-feedback">
          <textarea name="address" id="address" class="form-control" cols="40" rows="5" placeholder="Address"
            required></textarea>
        </div>
        <div class="row">
          <div class="col-xs-8">
            <div class="checkbox icheck">
              <label>
                <input type="checkbox" required> I agree to the <a href="#">terms</a>
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-xs-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.form-box -->
  </div>
  <!-- /.register-box -->

  <!-- jQuery 3 -->
  <script src="{{ asset('plugins/jQuery/jquery.min.js') }}"></script>

  <script src="{{ asset('plugins/morris/morris.min.js') }}"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="{{ asset('plugins/jQueryUI/jquery-ui.min.js') }}"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script>
  <!-- Bootstrap 3.3.7 -->
  <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
  <!-- Sparkline -->
  <script src="{{ asset('plugins/sparkline/jquery.sparkline.min.js') }}"></script>
  <!-- jvectormap -->
  <script src="{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
  <script src="{{ asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
  <!-- jQuery Knob Chart -->
  <script src="{{ asset('plugins/knob/jquery.knob.js') }}"></script>
  <script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
  <!-- daterangepicker -->
  <script src="{{ asset('plugins/daterangepicker/moment.min.js') }}"></script>
  <script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>
  <!-- datepicker -->
  <script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
  <!-- Slimscroll -->
  <script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
  <!-- FastClick -->
  <script src="{{ asset('plugins/fastclick/fastclick.min.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->

  <script src="{{ asset('dist/js/demo.js') }}"></script>
  <script src="{{ asset('js/validator.js') }}"></script>
  <script src="{{ asset('plugins/sweetalert/sweetalert.min.js') }}"></script>

  <script>
    $(function () {
        findCustomer();

    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });

    //$('.alert').css('display', 'block').delay(10000).fadeOut();

    $('#form-created').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
         $.ajax({
            url : "{{ route('agent.client.store') }}",
            type : "POST",
            data : $('#form-created').serialize(),
           success : function(data){
              swal({
                  title: "Successfully Create",
                  icon: "success",
                  buttons: "Oke",
              })
              .then((print) => {
                  swal("wait 1 x 24 hours, admin will process")
                  .then((oke) => {
                      window.location.href = "{{ route('agent.list') }}";
                  });
              });
           },
           error : function(e){
               console.log(e);

             if (e.responseJSON.errors) {
               if (e.responseJSON.errors.email) {
                 $('#emailx').parents('.form-group').addClass('has-error').removeClass('has-success');
                 $('.emailx').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                 $('#email').text(e.responseJSON.errors.email).removeClass('hidden');
                }

                if (e.responseJSON.errors.phone) {
                    $('#phonex').parents('.form-group').addClass('has-error').removeClass('has-success');
                    $('.phonex').removeClass('glyphicon-ok').addClass('glyphicon-remove');
                    // $('.alert').removeClass('hidden');
                    // $('.alert').css('display', 'block').delay(10000).fadeOut();
                    $('#phone').text(e.responseJSON.errors.phone).removeClass('hidden');
                }
             }
             }
         });
         return false;
      }
    });
  });

  function findCustomer(){
        $('#name').keyup(function(){
            var query = $(this).val();

            if(query != ''){
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{ route('customer.keyword') }}",
                    method:"POST",
                    data:{query:query, _token:_token},
                    dataType: "json",
                    success:function(data){
                        $('#suggesstion-box').fadeIn();
                        $('#suggesstion-box').html(data);
                    }
                });
            }else{
                var _token = $('input[name="_token"]').val();
                $.ajax({
                url:"{{ route('customer.all') }}",
                method:"POST",
                data:{query:query, _token:_token},
                dataType: "json",
                success:function(data){
                    $('#suggesstion-box').fadeIn();
                    $('#suggesstion-box').html(data);
                }
                });
            }
        });
    }

    function detailCustomer(id, induk, name, phone, birth, rank, bonus, point, email, address) {
        $('#agent_code').val(induk);
        $('#name').val(name);
        $('#emailx').val(email);
        $('#phonex').val(phone);
        $('#address').val(address);
        $('#suggesstion-box').fadeOut();
    }

  </script>
</body>

</html>

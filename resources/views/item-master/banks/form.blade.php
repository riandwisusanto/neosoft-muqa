<div class="modal" id="modal-banks" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
       <div class="modal-content">
     
    <form class="form-horizontal" data-toggle="validator" method="post">
    {{ csrf_field() }} {{ method_field('POST') }}
    
     <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <i class="fa fa-times fa-lg"></i>   
         </button>
         <h3 class="modal-title"></h3>
     </div>
         <div class="modal-body" >
             <input type="hidden" id="id" name="id">
             <div class="form-group">
                 <label for="bank_code" class="col-md-3 control-label">Bank Code *</label>
                 <div class="col-md-8">
                     <input type="text" class="form-control" id="bank_code" name="bank_code" placeholder="Bank Code" autocomplete="off" required>
                 </div>
             </div>
             <div class="form-group">
                 <label for="bank_name" class="col-md-3 control-label">Bank Name *</label>
                 <div class="col-md-8">
                     <input type="text" class="form-control" id="bank_name" name="bank_name" placeholder="Bank Name" autocomplete="off" required>
                 </div>
             </div>
             <div class="form-group">
                 <label for="bank_charge" class="col-md-3 control-label">Bank Charge *</label>
                 <div class="col-md-8">
                     <input type="number" step="0.001" class="form-control" id="bank_charge" name="bank_charge" placeholder="Bank Charge" autocomplete="off" required>
                 </div>
             </div>
           <div class="form-group" >
                <div class="form-check" id="group_outlet">
                         <label for="outlet" class="col-md-3 control-label">Branch *</label>
                        <div class="col-md-8">
                            <select  class="form-control select2 select2-hidden-accessible"multiple="multiple" data-placeholder="Select a Outlet"  name="outlet[]" id="outlet" tabindex="-1" aria-hidden="true"  required>
                                @foreach ($outlet as $list)
                                <option value="{{ $list->id_outlet }}">{{ $list->outlet_name }}</option>
                                @endforeach
                            </select>
                            <span class="help-block text-red" id="msg_outlet"></span>
                        </div>
                </div>
            </div>

            <div class="form-group">
                <label for="outlet" class="col-md-3 control-label">Bank Type</label>
                <div class="col-md-8" style="
                                    display: flex;
                                    align-items: center;
                                    gap: 20px;">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" id="cash" type="radio" name="bank_type" value="0">
                        <label class="form-check-label">Cash</label>
                    </div>
    
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" id="non_cash" type="radio" name="bank_type" value="1">
                        <label class="form-check-label">Non Cash</label>
                    </div>
                </div>
            </div>

             <div class="form-check" id="status_bank_modal">
                 <label for="bank_status" class="col-md-3 control-label"></label>
                 <div class="col-md-8">
                     <input type="checkbox" class="form-check-input" id="status_bank" name="status_bank" value="1">
                     <label for="bank_status" class="form-check-label">Enable</label>
                 </div>
             </div>
         </div>
         
         <div class="modal-footer">
             <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
             <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
         </div>
     
    </form>
 
          </div>
       </div>
 </div>

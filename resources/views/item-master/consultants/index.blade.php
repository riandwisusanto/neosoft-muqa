@extends('base')

@section('title')
List Consultants
@endsection



@section('breadcrumb')
@parent
<li>Consultants</li>
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">

@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-sm-8">
                        <span style="font-size:20px; font-weight: bold;">CONSULTANTS MANAGEMENT</span>
                        <p style="font-size:15px; font-weight: bold;"><i>Manage all clinic consultants.</i></p>
                    </div>
                    <div class="col-sm-4">
                        <a onclick="addForm()" class="btn btn-success btn-float-right"><i class="fa fa-plus-circle"></i> Add Consultants</a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped table-bordered nowrap table-consultant" style="width:100%">
                    <thead>
                        <tr>
                            <th>Consultant Name</th>
                            <th>Join Date</th>
                            <th>Branch</th>
                            <th>Enable</th>
                            <th class="all" width="10%">Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@include('item-master.consultants.form')
@endsection

@section('script')
<script type="text/javascript">
    var table, save_method;

    $('#datepicker, #datepicker2').datepicker({
        format: 'dd MM yyyy',
        autoclose: true,
        todayHighlight: true
    });


    $(function () {
        table = $('.table-consultant').DataTable({
            // "processing": true,
          //scrollY:        "500px",
          scrollX:        true,
          scrollCollapse: true,
          paging:         false,
          fixedColumns:   {
              leftColumns: 1,
          },
            "serverside": true,
            "ajax": {
                "url": "{{ route('consultants.data') }}",
                "type": "GET"
            }
        });

        $('#modal-consultants form').validator().on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                var id = $('#id').val();

                url = "{{ route('consultants.store') }}";

                if (save_method == 'edit') {
                    url = "consultants/" + id;
                }


                console.log(url)

                $.ajax({
                    url: url,
                    type: "POST",
                    data: $('#modal-consultants form').serialize(),
                    success: function (data) {
                        $('#modal-consultants').modal('hide');
                        table.ajax.reload();
                    },
                    error: function () {
                        alert("Can not Save the Data!");
                    }
                });
                return false;
            }
        });

    });

    function addForm() {
        save_method = "add";
        $('#outlet').val('');
        $("#outlet").trigger("change");
        $('input[name=_method]').val('POST');
        $("#modal-consultants").modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
        $('#modal-consultants form')[0].reset();
        $('#branch').val([]).change();

        $('.modal-title').text('Add Consultant');
        $('#kode').attr('readonly', false);
        document.getElementById('status_consultant_modal').style.visibility = 'hidden';

    }

    function detailInfo(id) {

    }

    function editForm(id) {
        save_method = "edit";
        $('input[name=_method]').val('PUT');
        $('#modal-consultants form')[0].reset();
        $.ajax({
            url: "consultants/" + id + "/edit",
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                $('#modal-consultants').modal('show');
                $('.modal-title').text('Edit Consultant');

                $('#id').val(data.id_consultant);
                $('#consultant_name').val(data.consultant_name);

                let branch = data.details.map(row => {
                    return row.outlet_id;
                })

                $('#branch').val(branch).change();
                $('#status_consultant').prop('checked', data.status_consultant);
                 document.getElementById('status_consultant_modal').style.visibility = 'visible';


            },
            error: function () {
                alert("Can not Show the Data!");
            }
        });
    }

    function deleteData(id) {
    //javascritpt normal
    //     if (confirm("Are you sure the data will be Delete?")) {
    //         $.ajax({
    //             url: "consultants/" + id,
    //             type: "POST",
    //             data: {
    //                 '_method': 'DELETE',
    //                 '_token': $('meta[name=csrf-token]').attr('content')
    //             },
    //             success: function (data) {
    //                 table.ajax.reload();
    //             },
    //             error: function () {
    //                 alert("Can not Delete the Data!");
    //             }
    //         });
    //     }
        //sweet alert
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this Consultant!",
            icon: "warning",
            buttons: {
                canceled:{
                    text:'Cancel',
                    value: 'cancel',
                    className: 'swal-button btn-default'
                },
                disabled:{
                    text:'Disable',
                    value: 'disable',
                    className: 'swal-button btn-warning'
                },
                deleted:{
                    text:'Delete',
                    value: 'delete',
                    className: 'swal-button btn-danger'
                }
            },
            dangerMode: true,
        })
        .then((willDelete) => {
            switch (willDelete) {
                default:
                    swal("Consultant is safe!");
                    break;
                case 'disable':
                    $.ajax({
                        url : "/consultants/"+id+"?disable=true",
                        type : "POST",
                        data : {'_method' : 'PUT', '_token' : $('meta[name=csrf-token]').attr('content')},
                        success : function(data){
                            swal("Consultant has been disabled!", {
                                icon: "success",
                            });
                            table.ajax.reload();
                        },
                        error : function(){
                            swal({
                                text: 'Can not Disabled the Data!',
                                icon: 'error'
                            })
                        }
                    });
                    break;
                case 'delete':
                    $.ajax({
                        url : "/consultants/"+id,
                        type : "POST",
                        data : {'_method' : 'Delete', '_token' : $('meta[name=csrf-token]').attr('content')},
                        success : function(data){
                                    if (data === "error") {
                                        swal({
                                            text: 'Consultant is Used !',
                                            icon: 'error'
                                        })
                                    } else {
                                        swal("Consultant been deleted!", {
                                            icon: "success",
                                        });
                                    }
                                    table.ajax.reload();
                         },
                        error : function(){
                            swal({
                                text: 'Can not Delete the Data!',
                                icon: 'error'
                            })
                        }
                    });
                    break;
            }
        });
    }

    // validasi
    $(document).ready(function () {
        $("#noktp").keypress(function (data) {
            //console.log(data.which);
            if (data.which < 48 || data.which > 57) {
                $("#msg_ktp").html("Please insert real IC numbers").show().fadeOut(4000);
                return false;
            }
        });

        $("#contact").keypress(function (data) {
            //console.log(data.which);
            if (data.which < 48 || data.which > 57) {
                $("#msg_phone").html("Please insert real numbers phone").show().fadeOut(4000);
                return false;
            }
        });

        $("#familyphone").keypress(function (data) {
            //console.log(data.which);
            if (data.which < 48 || data.which > 57) {
                $("#msg_family").html("Please insert real numbers phone").show().fadeOut(4000);
                return false;
            }
        });
    });
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>
@endsection

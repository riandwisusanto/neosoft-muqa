<script type="x-tmpl-mustache" id="add_item_point">
        <label class="col-xs-12 col-md-4 control-label">Point</label>
        <div class="col-xs-12">

            <div class="col-md-3 col-xs-12">
                <div class="form-group">
                    <label>Point</label>
                    <input type="number" class="form-control" id="point" name="point" required>
                </div>
            </div>
                
            <div class="col-md-2 col-xs-12">
                    <label style="width:100%">Action</label>
                    <button type="button" class="btn btn-info btn-xs" id="AddItem_point"><i class="fa fa-plus"></i></button>        
            </div>
        </div>
        
        {% #item %}
    
        
        <label class="col-xs-2 control-label"></label>
        <div class="col-xs-12">
            <div id="data_point_{% _id %}">
                
                <div class="col-md-3 col-xs-12">
                    <label>Point</label>
                    <input type="number" class="form-control" value="{% point %}" id="point" name="point"  disabled required>
                </div>
                <div class="col-md-2 col-xs-12">
                    <label style="width:100%">Action</label>
                    <button type="button" id="edit_point_{% _id %}" class="btn edit_point btn-info btn-xs" data-id="{% _id %}"><i class="fa fa-edit"></i></button>
                    <button type="button" id="delete_point_{% _id %}" class="btn delete_point btn-info btn-xs" data-id="{% _id %}"><i class="fa fa-trash"></i></button>
                </div>
            </div>
    
        </div>
    
        
        <label class="col-xs-2 control-label"></label>
        <div class="col-xs-12">
            <div class="form hidden" id="form_point_{% _id %}">
                <div id="data_point_{% _id %}">
                    <div class="form-group">
                        <div class="col-md-3 col-xs-12">
                            <label>Point</label>
                            <input type="number" class="form-control" id="point_{% _id %}" name="point" value="{% point %}" onChange="changeCountwithId({% _id %})" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2 col-xs-12">
                            <label style="width:100%">Action</label>
                            <button type="button" id="save_point_{% _id %}" class="btn save_point btn-info btn-xs" data-id="{% _id %}"><i class="fa fa-floppy-o"></i></button>
                            <button type="button" id="cancel_point_{% _id %}" class="btn cancel_point btn-info btn-xs" data-id="{% _id %}"><i class="fa fa-ban"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {% /item %}
    </script>

<script type="x-tmpl-mustache" id="add_item">    
    <label class="col-xs-12 col-md-4 control-label">Package Detail</label>
    <div class="col-xs-12">
        <div class="col-md-6 col-xs-12">
            <div class="form-group">
                <label>Product/Treatment</label>
                <select class="form-control" name="type_id" id="type_id" style="width: 100%" required>
                    <option></option>
                    @foreach ($productTreatment as $list)
                        <option value="{{ $list->id_product_treatment }}">{{ $list->product_treatment_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        
        <div class="col-md-2 col-xs-12">
            <div class="form-group">
                <label>Qty</label>
                <input type="hidden" class="form-control" id="current_price" name="current_price" value="{% current_price %}" required>
                <input type="number" class="form-control" id="qty" name="qty" onChange="changeCount()" required>
            </div>
        </div>

        <div class="col-md-2 col-xs-12">
            <div class="form-group">
                <label>Price</label>
                <input type="text" class="form-control" id="price" name="price" required>
            </div>
        </div>
            
        <div class="col-md-2 col-xs-12">
                <label style="width:100%">Action</label>
                <button type="button" class="btn btn-primary btn-xs" id="AddItem"><i class="fa fa-plus"></i></button>        
        </div>
    </div>
    
    {% #item %}

    
    <label class="col-xs-2 control-label"></label>
    <div class="col-xs-12">
        <div id="data_{% _id %}">
            <div class="col-md-6 col-xs-12">
                <label>Product/Treatment</label>
                <input type="hidden" class="form-control" value="{% type_id %}" id="type_id_{% _id %}" name="type_id"  disabled required>
                <input type="text" class="form-control" value="{% type_name %}" id="type_name_{% _id %}" name="type_name"  disabled required>
            </div>
            <div class="col-md-2 col-xs-12">
                <label>Qty</label>
                <input type="number" class="form-control" value="{% qty %}" id="qty" name="qty"  disabled required>
            </div>
            <div class="col-md-2 col-xs-12">
                <label>Price</label>
                <input type="hidden" class="form-control" id="current_price" name="current_price" value="{% current_price %}" required>
                <input type="text" class="form-control" id="price" name="price" value="{% price %}" disabled required>
            </div>
            <div class="col-md-2 col-xs-12">
                <label style="width:100%">Action</label>
                <button type="button" id="edit_{% _id %}" class="btn edit btn-success btn-xs" data-id="{% _id %}"><i class="fa fa-edit"></i></button>
                <button type="button" id="delete_{% _id %}" class="btn delete btn-danger btn-xs" data-id="{% _id %}"><i class="fa fa-trash"></i></button>
            </div>
        </div>

    </div>

    
    <label class="col-xs-2 control-label"></label>
    <div class="col-xs-12">
        <div class="form hidden" id="form_{% _id %}">
            <div id="data_{% _id %}">
                <div class="form-group">
                    <div class="col-md-6 col-xs-12">
                        <label>Product/Treatment</label>
                        <select class="form-control" name="type_id" id="type_idx_{% _id %}" style="width: 100%" required>
                            @foreach ($productTreatment as $list)
                                <option value="{{ $list->id_product_treatment }}">{{ $list->product_treatment_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-2 col-xs-12">
                        <label>Qty</label>
                        <input type="number" class="form-control" id="qty_{% _id %}" name="qty" value="{% qty %}" onChange="changeCountwithId({% _id %})" required>
                    </div>
                </div>
                <div class="form-group">    
                    <div class="col-md-2 col-xs-12">
                        <label>Price</label>
                        <input type="hidden" class="form-control" id="current_price_{% _id %}" name="current_price_{% _id %}" value="{% current_price %}" required>
                        <input type="text" class="form-control" id="price_{% _id %}" name="price" value="{% price %}" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-2 col-xs-12">
                        <label style="width:100%">Action</label>
                        <button type="button" id="save_{% _id %}" class="btn save btn-primary btn-xs" data-id="{% _id %}"><i class="fa fa-floppy-o"></i></button>
                        <button type="button" id="cancel_{% _id %}" class="btn cancel btn-warning btn-xs" data-id="{% _id %}"><i class="fa fa-ban"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {% /item %}
</script>
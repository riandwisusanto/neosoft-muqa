@extends('base')

@section('title')
    Package
@endsection

@section('breadcrumb')
    @parent
    <li>Package</li>
@endsection

@section('style')
    <style>
        .price-log-item{
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            align-items: flex-start;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-sm-8">
                            <span style="font-size:20px; font-weight: bold;">PACKAGES MANAGEMENT</span>
                            <p style="font-size:15px; font-weight: bold;"><i>Manage all treatment or product packages.</i></p>
                        </div>
                        <div class="col-sm-4">
                            <a onclick="addForm()" class="btn btn-success btn-float-right"><i class="fa fa-plus-circle"></i> Add Packages</a>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-striped table-bordered dt-responsive nowrap table-package" style="width:100%">
                        <thead>
                        <tr>
                            <th>Package Code</th>
                            <th>Package Name</th>
                            <th>Current Date</th>
                            <th>Price</th>
                            <th>Remarks</th>
                            <th>Enable</th>
                            <th class="all" width="10%">Action</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@include('item-master.package.form')
@endsection

@section('script')
    <script type="text/javascript">
        var table, save_method;

        $('#datepicker, #datepicker2').datepicker({
            format: 'dd MM yyyy',
            autoclose: true,
            todayHighlight: true
        });

        var commissionSales = document.getElementById('commission');
        commissionSales.addEventListener('keyup', function(e){
            // tambahkan 'Rp.' pada saat form di ketik
            // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            commissionSales.value = formatAngka(this.value);
        });

        $(function () {
            table = $('.table-package').DataTable({
                // "processing": true,
                "serverside": true,
                "ajax": {
                    "url": "{{ route('packages.data') }}",
                    "type": "GET"
                }
            });
            tablePriceLog = $('#price_log_table').DataTable({
                processing: false,
                serverSide: false,
                paging: false,
                scrollY: 70,
                scrollX:        true,
                scrollCollapse: true,
                searching: false,
                ordering:  false,
                bInfo: false,
            });


            $('#modal-packages form').validator().on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    var id = $('#id').val();
                    var cek_json = JSON.parse($('#save_json').val());

                    if (cek_json.length > 0) {
                        if(save_method == "add") url = "{{ route('packages.store') }}";
                        else url = "packages/"+id;

                        $.ajax({
                            url: url,
                            type: "POST",
                            data: $('#modal-packages form').serialize(),
                            success: function (data) {
                                $('#modal-packages').modal('hide');
                                window.location.href = '/packages';
                            },
                            error: function () {
                                alert("Can not Save the Data!");
                            }
                        });
                        return false;
                    }else{
                        swal("The Package is Still Empty! Please Press Action", {
                            icon: "error",
                        });
                        return false;
                    }
                }
            });

        });

        function addForm() {
            save_method = "add";
            $('#outlet').val('');
            $("#outlet").trigger("change");
            $('input[name=_method]').val('POST');
            $("#modal-packages").modal({
                backdrop: 'static',
                keyboard: false,
                show: true
            });
            $('#branch').val([]).change()
            $('.modal-title').text('Package Add');
            $('#kode').attr('readonly', false);
            $('#price_log_group').hide();
            $('#status_package_modal').hide();
        }

        function editForm(id) {
            save_method = "edit";
            $('input[name=_method]').val('PATCH');
            $('#modal-packages form')[0].reset();
            $.ajax({
                url: "packages/" + id + "/edit",
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    $('#modal-packages').modal('show');
                    $('.modal-title').text('Edit Package');

                    $('#save_json_point').val(JSON.stringify(data.point));
                    edit_point(data.point);

                    $('#save_json').val(JSON.stringify(data.product));
                    edit_product(data.product);

                    $('#id').val(id);
                    $('#outlet').val(data.outlet).change();
                    $('#type').val(data.package.package_type).change();
                    $('#package_name').val(data.package.package_name);
                    $('#remarks').val(data.package.remarks);
                    $('#commission').val(data.package.commission);
                    $('#commissionType').val(data.package.commissionType);
                    $('#price_log_group').show();
                    data.package.price_logs.forEach((element,index)=>{
                        let date = new Date(element.created_at);
                        if (index == 0){
                            tablePriceLog.row.add([date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear()+' '+date.getHours()+':'+date.getMinutes(),element.edited_by.name,'<strong>(Current)</strong> '+element.price]).draw();
                        }else if (index == 1){
                            tablePriceLog.row.add([date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear()+' '+date.getHours()+':'+date.getMinutes(), element.edited_by.name,'<strong>(Previous)</strong> '+element.price]).draw();
                        }else {
                            tablePriceLog.row.add([date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear()+' '+date.getHours()+':'+date.getMinutes(), element.edited_by.name, element.price]).draw();
                        }
                    })
                    $('#status_package').prop('checked', data.package.status_package);
                    $('#status_package_modal').show();
                },
                error: function () {
                    alert("Can not Show the Data!");
                }
            });
        }

        function edit_point(data) {
            Mustache.tags = ['{%', '%}'];

            $(function(){
                // Mengubah data pada input type hidden ke dalam javascript array.
                var data = JSON.parse($('#save_json_point').val());

                // Fungsi render digunakan untuk merender data dan template mustache ke dalam elemen yg sudah ditentukan
                function render_point()
                {
                    // Memanggil fungsi getIndex sebelum ditampilkan
                    getIndex_point(data);
                    // Mengubah list array ke dalam bentuk data json lalu dimasukan ke dalam input type hidden
                    $('#save_json_point').val(JSON.stringify(data));

                    // Mendefinisikan template
                    var tmpl = $('#add_item_point').html();
                    // parse template ke dalam mustache template
                    Mustache.parse(tmpl);
                    // Merender dengan template dan data
                    // variable 'item' adalah variable yang akan dilooping
                    var html = Mustache.render(tmpl, { item : data });
                    // mengisi html tbody dengan hasil renderan di atas
                    $('#render_point').html(html);
                    /*});*/

                    // memanggil fungsi bind_point
                    bind_point(data);
                }

                // PENTING. fungsi bind_point digunakan untuk memberikan event listener kepada elemen html setelah render selesai. Jika tidak dilakukan bind_point maka event pada elemen tidak akan berjalan.
                function bind_point(){
                    // Memberikan event listener pada tombol pada saat tombol diklik
                    $('#AddItem_point').on('click', add_point);
                    $('.delete_point').on('click', delete_item_point);
                    $('.edit_point').on('click', edit_point);
                    $('.cancel_point').on('click', canceledit_point);
                    $('.save_point').on('click', saveedit_point);
                }

                // Fungsi add untuk menambah item ke dalam list
                function add_point()
                {
                    // Membaca nilai dari inputan

                    var point = $('#point').val();

                    // if (kode_pasien == '') {
                    //      alert('Silahkan Input Nama');
                    //      return false;
                    // }else if (type == '') {
                    //     alert('Price Not Valid');
                    //      return false;
                    //     alert('Silahkan Input Jumlah');
                    // }else if (quantity == '') {
                    //      return false;
                    // }else if (isNaN(quantity)) {
                    //     alert('Jumlah Not Valid');
                    //      return false;
                    // }

                    // Membuat object yg akan dimasukan kedalam array
                    var input = {
                        'point' : point,
                    };


                    // Memasukan object ke dalam array
                    data.push(input);

                    // Merender ulang karena data sudah berubah

                    render_point();

                }



                // fungsi edit untuk menampilkan form edit
                function edit_point()
                {
                    // Mengambil id item yang akan dihapus
                    var i = parseInt($(this).data('id'), 10);

                    var row_data = $("#data_point_" + i);
                    var row_form = $("#form_point_" + i);
                    var input_data = $("#input_data_point");


                    // menyembunyikan input form
                    if(!input_data.hasClass('hidden')){
                        input_data.addClass('hidden');
                    }

                    // menyembunyikan baris data
                    if(!row_data.hasClass('hidden')){
                        row_data.addClass('hidden');
                    }

                    // menampilkan baris form
                    if(row_form.hasClass('hidden')){
                        row_form.removeClass('hidden');
                    }
                }

                // fungsi edit untuk menyembunyikan form edit
                function canceledit_point()
                {
                    render_point();
                }

                function saveedit_point()
                {
                    // Mengambil id item yang akan dihapus
                    var i = parseInt($(this).data('id'), 10);

                    // Membaca nilai dari inputan
                    var point = $('#point_' + i).val();

                    // Menyimpan data pada list
                    data[i].point = point;

                    // Merender kembali karena data sudah berubah
                    render_point();
                }

                // Fungsi delete_phone digunakan untuk menghapus elemen
                function delete_item_point(){
                    // Mengambil id item yang akan dihapus
                    var i = parseInt($(this).data('id'), 10);
                    console.log(data);

                    // menghapus list dari elemen array
                    data.splice(i, 1);

                    // Merender kembali karena data sudah berubah
                    render_point();
                }

                // Fungsi getIndex digunakan untuk membuat penomoran dan id unik sebelum dirender
                function getIndex_point(data)
                {
                    for (idx in data) {
                        // setting _id digunakan untuk memudahkan dalam mengedit dan menghapus list, seperti id pada tabel dalam database.
                        data[idx]['_id'] = idx;
                        // setting no digunakan untuk penomoran
                        data[idx]['no'] = parseInt(idx) + 1;
                    }
                }

                // Memanggil fungsi render pada saat halaman pertama kali dimuat
                render_point();
            });

        }

        function edit_product(data) {
            Mustache.tags = ['{%', '%}'];

            $(function(){

                // Fungsi render digunakan untuk merender data dan template mustache ke dalam elemen yg sudah ditentukan
                function render()
                {
                    // Memanggil fungsi getIndex sebelum ditampilkan
                    getIndex(data);
                    // Mengubah list array ke dalam bentuk data json lalu dimasukan ke dalam input type hidden
                    $('#save_json').val(JSON.stringify(data));

                    // Mendefinisikan template
                    var tmpl = $('#add_item').html();
                    // parse template ke dalam mustache template
                    Mustache.parse(tmpl);
                    // Merender dengan template dan data
                    // variable 'item' adalah variable yang akan dilooping
                    var html = Mustache.render(tmpl, { item : data });
                    // mengisi html tbody dengan hasil renderan di atas
                    $('#render').html(html);
                    /*});*/

                    // memanggil fungsi bind
                    bind(data);
                }

                // PENTING. fungsi bind digunakan untuk memberikan event listener kepada elemen html setelah render selesai. Jika tidak dilakukan bind maka event pada elemen tidak akan berjalan.
                function bind(){
                    // Memberikan event listener pada tombol pada saat tombol diklik
                    $('select:not(.normal)').each(function () {
                        $(this).select2({
                            placeholder: "Please Select",
                            width: 'resolve',
                            dropdownParent: $(this).parent()
                        });
                    });

                    var save_json = JSON.parse($('#save_json').val());
                    let grandtotal = [];
                    for (let i = 0; i < save_json.length; i++) {

                        grandtotal.push(Number(remove_format(save_json[i].price)));
                    }

                    let total = grandtotal.reduce((a,b) => a + b, 0)
                    $('#packprice').val(format_money(total));

                    chooseProduct();
                    chooseProductwithId();
                    $('.edit').on('click', chooseProductwithId);

                    $('#AddItem').on('click', add);
                    $('.delete').on('click', delete_item);
                    $('.edit').on('click', edit);
                    $('.cancel').on('click', canceledit);
                    $('.save').on('click', saveedit);
                }

                function chooseProduct() {
                    $('#type_id').change(function () {
                        $.ajax({
                            url: "packages/"+$(this).val()+"/chooseProduct",
                            type: "GET",
                            dataType: "JSON",
                            success: function (data) {
                                $('#price').val(data.price);
                                $('#current_price').val(data.price);
                                $('#qty').val(1);
                            },
                            error: function () {
                                alert("Can not Delete the Data!");
                            }
                        });
                    });
                }

                function chooseProductwithId() {
                    var i = parseInt($(this).data('id'), 10);

                    $('#type_idx_'+i).change(function () {
                        $.ajax({
                            url: "packages/"+$(this).val()+"/chooseProduct",
                            type: "GET",
                            dataType: "JSON",
                            success: function (data) {
                                $('#price_'+i).val(data.price);
                                $('#current_price_'+i).val(data.price);
                                $('#qty_'+i).val(1);
                            },
                            error: function () {
                                alert("Can not Delete the Data!");
                            }
                        });
                    });

                }


                // Fungsi add untuk menambah item ke dalam list
                function add()
                {
                    // Membaca nilai dari inputan
                    var type_name = $('#type_id option:selected').text();


                    var type_id = $('#type_id').val();
                    var qty = $('#qty').val();
                    var price = $('#price').val();
                    var current_price = $('#current_price').val();

                    //console.log(totalprice);

                    // if (kode_pasien == '') {
                    //      alert('Silahkan Input Nama');
                    //      return false;
                    // }else if (type == '') {
                    //     alert('Price Not Valid');
                    //      return false;
                    //     alert('Silahkan Input Jumlah');
                    // }else if (quantity == '') {
                    //      return false;
                    // }else if (isNaN(quantity)) {
                    //     alert('Jumlah Not Valid');
                    //      return false;
                    // }

                    // Membuat object yg akan dimasukan kedalam array
                    var input = {
                        'type_name' : type_name,
                        'type_id' : type_id,
                        'qty' : qty,
                        'price' : price,
                        'current_price' : current_price,
                    };


                    // Memasukan object ke dalam array
                    data.push(input);

                    // Merender ulang karena data sudah berubah

                    render();

                }



                // fungsi edit untuk menampilkan form edit
                function edit()
                {
                    // Mengambil id item yang akan dihapus
                    var i = parseInt($(this).data('id'), 10);

                    let type_id = $('#type_id_'+i).val();
                    $('#type_idx_'+i).val(type_id).trigger('change');

                    var row_data = $("#data_" + i);
                    var row_form = $("#form_" + i);
                    var input_data = $("#input_data");


                    // menyembunyikan input form
                    if(!input_data.hasClass('hidden')){
                        input_data.addClass('hidden');
                    }

                    // menyembunyikan baris data
                    if(!row_data.hasClass('hidden')){
                        row_data.addClass('hidden');
                    }

                    // menampilkan baris form
                    if(row_form.hasClass('hidden')){
                        row_form.removeClass('hidden');
                    }
                }

                // fungsi edit untuk menyembunyikan form edit
                function canceledit()
                {
                    render();
                }

                function saveedit()
                {
                    // Mengambil id item yang akan dihapus
                    var i = parseInt($(this).data('id'), 10);

                    // Membaca nilai dari inputan
                    var type_id = $('#type_idx_' + i).val();
                    var type_name = $('#type_idx_'+ i +' option:selected').text();
                    var qty = $('#qty_' + i).val();
                    var price = $('#price_' + i).val();
                    var current_price = $('#current_price_' + i).val();

                    // Menyimpan data pada list
                    data[i].type_id = type_id;
                    data[i].type_name = type_name;
                    data[i].qty = qty;
                    data[i].price = price;
                    data[i].current_price = current_price;

                    // Merender kembali karena data sudah berubah
                    render();
                }

                // Fungsi delete_phone digunakan untuk menghapus elemen
                function delete_item(){
                    // Mengambil id item yang akan dihapus
                    var i = parseInt($(this).data('id'), 10);

                    // menghapus list dari elemen array
                    data.splice(i, 1);

                    // Merender kembali karena data sudah berubah
                    render();
                }

                // Fungsi getIndex digunakan untuk membuat penomoran dan id unik sebelum dirender
                function getIndex(data)
                {
                    for (idx in data) {
                        // setting _id digunakan untuk memudahkan dalam mengedit dan menghapus list, seperti id pada tabel dalam database.
                        data[idx]['_id'] = idx;
                        // setting no digunakan untuk penomoran
                        data[idx]['no'] = parseInt(idx) + 1;
                    }
                }

                // Memanggil fungsi render pada saat halaman pertama kali dimuat
                render();
            });

        }

        function deleteData(id) {
            if (confirm("Are you sure the data will be Delete?")) {
                $.ajax({
                    url: "packages/" + id,
                    type: "POST",
                    data: {
                        '_method': 'DELETE',
                        '_token': $('meta[name=csrf-token]').attr('content')
                    },
                    success: function (data) {
                        table.ajax.reload();
                    },
                    error: function () {
                        alert("Can not Delete the Data!");
                    }
                });
            }
        }

        function changeCount() {
            let price = $('#current_price').val();
            let qty = $('#qty').val();
            let subtotal = qty * remove_format(price);

            $('#price').val(format_money(subtotal));

        }

        function changeCountwithId(id) {
            let price = $('#current_price_'+id).val();
            let qty = $('#qty_'+id).val();
            let subtotal = qty * remove_format(price);

            $('#price_'+id).val(format_money(subtotal));
        }

        function remove_format(money) {
            return money.toString().replace(/[^0-9]/g,'');
        }

        function format_money(money){
            return money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
        }
        function formatAngka(angka){
                var number_string = angka.replace(/[^.\d]/g, '').toString();

                return rupiah = number_string;
                // return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }
        function deleteItem(id){
            // swal({
            //     title: "Are you sure?",
            //     text: "Once deleted, you will not be able to recover this Package!",
            //     icon: "warning",
            //     buttons: true,
            //     dangerMode: true,
            // })
            // .then((willDelete) => {
            //     if (willDelete) {
            //         $.ajax({
            //             url : "/packages/"+id,
            //             type : "POST",
            //             data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
            //             success : function(data){
            //                 swal("Package has been deleted!", {
            //                     icon: "success",
            //                 });
            //                 table.ajax.reload();
            //             },
            //             error : function(){
            //                 alert("Can not Delete the Data!");
            //             }
            //         });
            //     } else {
            //         swal("Package is safe!");
            //     }
            // });
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this Package!",
                icon: "warning",
                buttons: {
                    canceled:{
                        text:'Cancel',
                        value: 'cancel',
                        className: 'swal-button btn-default'
                    },
                    disabled:{
                        text:'Disable',
                        value: 'disable',
                        className: 'swal-button btn-warning'
                    },
                    deleted:{
                        text:'Delete',
                        value: 'delete',
                        className: 'swal-button btn-danger'
                    }
                },
                dangerMode: true,
            }).then((willDelete) => {
                switch (willDelete) {
                    default:
                        swal("Package is safe!");
                        break;
                    case 'disable':
                        $.ajax({
                            url : "/packages/"+id+"?disable=true",
                            type : "POST",
                            data : {'_method' : 'PUT', '_token' : $('meta[name=csrf-token]').attr('content')},
                            success : function(data){
                                swal("Package has been disabled!", {
                                    icon: "success",
                                });
                                table.ajax.reload();
                            },
                            error : function(){
                                swal({
                                    text: 'Can not Disabled the Data!',
                                    icon: 'error'
                                })
                            }
                        });
                        break;
                    case 'delete':
                        $.ajax({
                            url : "/packages/"+id,
                            type : "POST",
                            data : {'_method' : 'Delete', '_token' : $('meta[name=csrf-token]').attr('content')},
                            success : function(data){
                                if (data === "error") {
                                    swal({
                                        text: 'Package is Using Client!',
                                        icon: 'error'
                                    })
                                } else {
                                    swal("Package has been deleted!", {
                                        icon: "success",
                                    });
                                }
                                table.ajax.reload();
                            },
                            error : function(){
                                swal({
                                    text: 'Can not Delete the Data!',
                                    icon: 'error'
                                })
                            }
                        });
                        break;
                }
            });
        }

    </script>
    <script src="{{ asset('js/package.js') }}"></script>
    <script src="{{ asset('js/point_package.js') }}"></script>
@endsection

<div class="modal" id="modal-groups" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
       <div class="modal-content">
     
    <form class="form-horizontal" data-toggle="validator" method="post">
    {{ csrf_field() }} {{ method_field('POST') }}
    
     <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <i class="fa fa-times fa-lg"></i>   
         </button>
         <h3 class="modal-title"></h3>
     </div>
         <div class="modal-body" >
             <input type="hidden" id="id" name="id">
             <div class="form-group">
                 <label for="group_name" class="col-md-3 control-label">Group Name *</label>
                 <div class="col-md-8">
                     <input type="text" class="form-control" id="group_name" name="group_name" placeholder="Group Name" autocomplete="off" required>
                 </div>
             </div>
             <div class="form-group">
                 <label for="name" class="col-md-3 control-label">Name *</label>
                 <div class="col-md-8">
                     <input type="text" class="form-control" id="name" name="name" placeholder="Name" autocomplete="off" required>
                 </div>
             </div>
            <div class="form-check" id="status">
                <label for="status" class="col-md-3 control-label"></label>
                <div class="col-md-8">
                    <input type="checkbox" class="form-check-input status" id="status" name="status" value="1">
                    <label for="status" class="form-check-label">Enable</label>
                </div>
            </div>
         </div>
         
         <div class="modal-footer">
             <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
             <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
         </div>
     
    </form>
 
          </div>
       </div>
 </div>

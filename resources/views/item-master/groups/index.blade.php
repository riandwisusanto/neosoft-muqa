@extends('base')

@section('title')
  List Group
@endsection

@section('breadcrumb')
   @parent
   <li>Group</li>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <div class="row">
            <div class="col-sm-8">
                <span style="font-size:20px; font-weight: bold;">GROUPS MANAGEMENT</span>
                <p style="font-size:15px; font-weight: bold;"><i>Manage all clinic groups.</i></p>
            </div>
            <div class="col-sm-4">
                <a onclick="addForm()" class="btn btn-success btn-float-right"><i class="fa fa-plus-circle"></i> Add Group</a>
            </div>
        </div>
      </div>
      <div class="box-body">
        <table class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
        <thead>
          <tr>
              <th></th>
              <th>Group Name</th>
              <th>Name</th>
              <th>Enable</th>
              <th class="all" style="width: 10%">Action</th>
          </tr>
        </thead>
        <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@include('item-master.groups.form')
@endsection

@section('script')
<script type="text/javascript">
  var table, save_method;

  $(function(){
  table = $('.table').DataTable({
      "processing" : true,
      "serverside" : true,
      "responsive" : {
          "details" : {
              "type" : 'column',
              "target" : 'tr'
          }
      },
      "columnDefs" : [ {
          "className" : 'control',
          "orderable" : false,
          "targets" :   0
      } ],
      "order" : [ 1, 'asc' ],


      "ajax" : {
          "url" : "{{ route('groups.data') }}",
          "type" : "GET"
      }
  });

  $('#modal-groups form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
         var id = $('#id').val();
         if(save_method === "add") url = "{{ route('groups.store') }}";
         else url = "groups/"+id;

         $.ajax({
            url : url,
            type : "POST",
            data : $('#modal-groups form').serialize(),
           success : function(){
              $('#modal-groups').modal('hide');
              table.ajax.reload();
           },
           error : function(){
             alert("Can not Save the Data!");
           }
         });
         return false;
     }
   });

});

function addForm(){
   save_method = "add";
   $('#outlet').val('');
   $("#outlet").trigger("change");
   $('input[name=_method]').val('POST');
   $("#modal-groups").modal({
      backdrop: 'static',
      keyboard: false,
      show: true
    });
   $('#modal-groups form')[0].reset();
  $('#status').hide();
   $('.modal-title').text('Add Group');
}

function editForm(id){
   save_method = "edit";
   $('input[name=_method]').val('PUT');
   $('#modal-groups form')[0].reset();
   $.ajax({
     url : "groups/"+id+"/edit",
     type : "GET",
     dataType : "JSON",
     success : function(data){
       $('#modal-groups').modal('show');
       $('.modal-title').text('Edit Group');

       $('#id').val(data.group_id);
       $('#group_name').val(data.group_name);

       $('#name').val(data.name);
       $('.status').prop('checked', data.status);
       $('#status').show();

     },
     error : function(e){
         console.log(e);
       alert("Can not Show the Data!");
     }
   });
}

function deleteData(id){
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this Group!",
        icon: "warning",
        buttons: {
            canceled:{
                text:'Cancel',
                value: 'cancel',
                className: 'swal-button btn-default'
            },
            disabled:{
                text:'Disable',
                value: 'disable',
                className: 'swal-button btn-warning'
            },
            deleted:{
                text:'Delete',
                value: 'delete',
                className: 'swal-button btn-danger'
            }
        },
        dangerMode: true,
    }).then((willDelete) => {
        switch (willDelete) {
            default:
                swal("Group is safe!");
                break;
            case 'disable':
                $.ajax({
                    url : "/groups/"+id+"?disable=true",
                    type : "POST",
                    data : {'_method' : 'PUT', '_token' : $('meta[name=csrf-token]').attr('content')},
                    success : function(data){
                        swal("Group has been disabled!", {
                            icon: "success",
                        });
                        table.ajax.reload();
                    },
                    error : function(){
                        swal({
                            text: 'Can not Disabled the Data!',
                            icon: 'error'
                        })
                    }
                });
                break;
            case 'delete':
                $.ajax({
                    url : "/groups/"+id,
                    type : "POST",
                    data : {'_method' : 'Delete', '_token' : $('meta[name=csrf-token]').attr('content')},
                    success : function(data){
                        if(data === "error"){
                            swal("Group is Used!", {
                                icon: "error",
                             });
                            table.ajax.reload();
                        }else{
                            swal("Group has been deleted!", {
                                 icon: "success",
                            });
                            table.ajax.reload();
                        }
                    },
                    error : function(){
                        swal({
                            text: 'Can not Delete the Data!',
                            icon: 'error'
                        })
                    }
                });
                break;
        }
    });
}
</script>
@endsection

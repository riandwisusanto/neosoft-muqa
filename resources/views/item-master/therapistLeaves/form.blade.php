<div class="modal" id="modal-therapist-leaves" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
       <div class="modal-content">
     
    <form class="form-horizontal" data-toggle="validator" method="post">
    {{ csrf_field() }} {{ method_field('POST') }}
    
     <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <i class="fa fa-times fa-lg"></i>   
         </button>
         <h3 class="modal-title"></h3>
     </div>
         <div class="modal-body" >
             <input type="hidden" id="id" name="id">
             <input type="hidden" name="therapist_id" value="{{ $id }}">
            <div class="form-group">
                <label class="col-md-3 control-label">Start Date:</label>
                <div class="col-md-8">
                    <div class="input-group">
                        <input type="text" name="start_date" id="start_date" class="form-control" required autocomplete="off">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">End Date:</label>
                <div class="col-md-8">
                    <div class="input-group">
                        <input type="text" name="end_date" id="end_date" class="form-control" required autocomplete="off">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Description:</label>
                <div class="col-md-8">
                    <textarea name="description" id="description" class="form-control"></textarea>
                </div>
            </div>
         </div>
         
         <div class="modal-footer">
            <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
         </div>
     
    </form>
 
          </div>
       </div>
 </div>
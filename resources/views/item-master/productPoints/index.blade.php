@extends('base')

@section('title')
List Product Points
@endsection

@section('breadcrumb')
@parent
<li>Product Points</li>
@endsection

@section('style')
<style>
    .price-log-item {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        align-items: flex-start;
    }
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <div class="row">
                    <div class="col-sm-8">
                        <span style="font-size:20px; font-weight: bold;">PRODUCT POINTS</span>
                        <p style="font-size:15px; font-weight: bold;"><i>Manage all products can be redeemed by loyalty points.</i></p>
                    </div>
                    <div class="col-sm-4">
                        <a onclick="addForm()" class="btn btn-success btn-float-right"><i class="fa fa-plus-circle"></i> Add Product Point</a>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th>Product Point Name</th>
                            <th>Point</th>
                            <th>Description</th>
                            <th>Image</th>
                            <th>Enable</th>
                            <th class="all" width="10%">Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@include('item-master.productPoints.form')
@endsection

@section('script')
<script type="text/javascript">
    var table, save_method;

    $('#datepicker, #datepicker2').datepicker({
        format: 'dd MM yyyy',
        autoclose: true,
        todayHighlight: true
    });


    $(function () {
        table = $('.table').DataTable({
            // "processing": true,
            "serverside": true,
            "ajax": {
                "url": "{{ route('productpoints.data') }}",
                "type": "GET"
            }
        });

        $('#modal-productpoints form').validator().on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                var id = $('#id').val();
                if(save_method == "add") url = "{{ route('productpoints.store') }}";
                    else url = "productpoints/"+id;
                    $.ajax({
                        url : url,
                        type : "POST",
                        data : new FormData($('#modal-productpoints form')[0]),
                        cache:false,
                        contentType: false,
                        processData: false,
                    success : function(data){
                        $('#modal-productpoints').modal('hide');
                        table.ajax.reload();
                    },
                    error : function(e){
                        console.log(e);
                        alert("Can not Save the Data!");
                    }
                    });
                    return false;
            }
        });

    });

    function addForm() {
        save_method = "add";
        $('#outlet').val('');
        $("#outlet").trigger("change");
        $('input[name=_method]').val('POST');
        $("#modal-productpoints").modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
        $('#modal-productpoints form')[0].reset();
        $('#branch').val([]).change()
        $('.modal-title').text('Add Product Point');
        $('#status_product_point_modal').hide();
    }

    function detailInfo(id) {

    }

    function editForm(id) {
        save_method = "edit";
        $('input[name=_method]').val('PUT');
        $('#modal-productpoints form')[0].reset();
        $.ajax({
            url: "productpoints/" + id + "/edit",
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                $('#modal-productpoints').modal('show');
                $('.modal-title').text('Edit Product Point');

                $('#id').val(data.id_product_point);
                $('#name').val(data.product_point_name);
                $('#point').val(data.point);
                $('#description').text(data.description);
                $('.tampil-image').html('<img src="storage/'+data.img+'" class="img-responsive">');

                $('#status_product_point').prop('checked', data.status_product_point);
                $('#status_product_point_modal').show();

            },
            error: function () {
                alert("Can not Show the Data!");
            }
        });
    }

    function deleteData(id) {
        {{--if (confirm("Are you sure the data will be Delete?")) {--}}
        {{--    $.ajax({--}}
        {{--        url: "{{ route('productpoints.index') }}/" + id,--}}
        {{--        type: "POST",--}}
        {{--        data: {--}}
        {{--            '_method': 'DELETE',--}}
        {{--            '_token': $('meta[name=csrf-token]').attr('content')--}}
        {{--        },--}}
        {{--        success: function (data) {--}}
        {{--            table.ajax.reload();--}}
        {{--        },--}}
        {{--        error: function () {--}}
        {{--            alert("Can not Delete the Data!");--}}
        {{--        }--}}
        {{--    });--}}
        {{--}--}}
        //sweet alert
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this Product point!",
            icon: "warning",
            buttons: {
                canceled:{
                    text:'Cancel',
                    value: 'cancel',
                    className: 'swal-button btn-default'
                },
                disabled:{
                    text:'Disable',
                    value: 'disable',
                    className: 'swal-button btn-warning'
                },
                deleted:{
                    text:'Delete',
                    value: 'delete',
                    className: 'swal-button btn-danger'
                }
            },
            dangerMode: true,
        }).then((willDelete) => {
                switch (willDelete) {
                    default:
                        swal("Product point is safe!");
                        break;
                    case 'disable':
                        $.ajax({
                            url : "/productpoints/"+id+"?disable=true",
                            type : "POST",
                            data : {'_method' : 'PUT', '_token' : $('meta[name=csrf-token]').attr('content')},
                            success : function(data){
                                swal("Product point has been disabled!", {
                                    icon: "success",
                                });
                                table.ajax.reload();
                            },
                            error : function(){
                                swal({
                                    text: 'Can not Disabled the Data!',
                                    icon: 'error'
                                })
                            }
                        });
                        break;
                    case 'delete':
                        $.ajax({
                            url : "/productpoints/"+id,
                            type : "POST",
                            data : {'_method' : 'Delete', '_token' : $('meta[name=csrf-token]').attr('content')},
                            success : function(data){
                                swal("Product point has been deleted!", {
                                    icon: "success",
                                });
                                table.ajax.reload();
                            },
                            error : function(){
                                swal({
                                    text: 'Can not Delete the Data!',
                                    icon: 'error'
                                })
                            }
                        });
                        break;
                }
            });
    }
</script>
{{-- <script src="{{ asset('js/point.js') }}"></script> --}}
@endsection

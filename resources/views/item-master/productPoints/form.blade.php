<div class="modal" id="modal-productpoints" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md">
           <div class="modal-content">
         
        <form class="form-horizontal" data-toggle="validator" method="post">
        {{ csrf_field() }} {{ method_field('POST') }}
        
         <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <i class="fa fa-times fa-lg"></i>
             </button>
             <h3 class="modal-title"></h3>
         </div>
             <div class="modal-body" >
                 <input type="hidden" id="id" name="id">
                 {{-- <input type="hidden" id="type" name="type" value="T"> --}}
                 <div class="row">
                     <div class="col-md-12">                         
                        <div class="form-group">
                            <label for="name" class="col-md-3 control-label">Product Name</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Product Point Name" autocomplete="off" required>
                            </div>
                        </div>
                         <div class="form-group">
                             <label for="point" class="col-md-3 control-label">Point</label>
                             <div class="col-md-8">
                                 <input type="number" class="form-control" id="point" name="point" placeholder="Point" autocomplete="off" required>
                             </div>
                         </div>
                         <div class="form-group">
                             <label for="point" class="col-md-3 control-label">Description</label>
                             <div class="col-md-8">
                                <textarea name="description" id="description" placeholder="Enter..."  class="form-control" required></textarea>
                             </div>
                        </div>
                          <div class="form-group">
                             <label for="point" class="col-md-3 control-label">Image</label>
                             <div class="col-md-8">
                                <input type="file" id="img" name="img" class="form-control" required></input>
                                <div class="tampil-image"></div>
                             </div>
                        </div>

                        <div class="form-check" id="status_product_point_modal">
                            <label for="product_point_name" class="col-md-3 control-label"></label>
                            <div class="col-md-8">
                              <input type="checkbox" class="form-check-input" id="status_product_point" name="status_product_point" value="1">
                              <label for="product_point_name" class="form-check-label">Enable</label>
                            </div>
                        </div>
                 </div>
             </div>
             
             <div class="modal-footer">
                 <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                 <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
             </div>
        </form>
           </div>
     
              </div>
           </div>
     </div>

<div class="modal" id="modal-activity" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <form class="form-horizontal" data-toggle="validator" method="post">
                {{ csrf_field() }} {{ method_field('POST') }}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times fa-lg"></i>
                    </button>
                    <h3 class="modal-title"></h3>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id_activity" name="id_activity">
                    <div class="form-group">
                        <label for="activity_name" class="col-md-3 control-label">Activity Name</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="activity_name" name="activity_name" placeholder="Activity Name" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="activity_group_id" class="col-md-3 control-label">Activity Category</label>
                        <div class="col-md-8">
                            <select id="activity_group_id" name="activity_group_id" class="form-control">
                                @foreach($group as $category)
                                <option value="{{ $category->id_activity_group }}">{{ $category->activity_group_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="activity_product_treatment_id" class="col-md-3 control-label">Product Treatment</label>
                        <div class="col-md-8">
                            <select id="activity_product_treatment_id" name="activity_product_treatment_id[]" class="form-control" multiple>
                                @foreach($product_treatment as $treatment)
                                <option value="{{ $treatment->id_product_treatment }}">{{ $treatment->product_treatment_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                            <input id="chkall_product_treatment" onclick="selected_all_product_treatment()" type="checkbox">Select All
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="activity_package_id" class="col-md-3 control-label">Package</label>
                        <div class="col-md-8">
                            <select id="activity_package_id" name="activity_package_id[]" class="form-control" multiple> 
                                @foreach($package as $package)
                                <option value="{{ $package->id_package }}">{{ $package->package_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                            <input id="chkall_package" onclick="selected_all_package()" type="checkbox">Select All
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="activity_pic" class="col-md-3 control-label">Activity PIC</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="activity_pic" name="activity_pic" placeholder="Activity PIC" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="start_date" class="col-md-3 control-label">Start Date</label>
                        <div class="col-md-8">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" placeholder="Start Date" class="form-control pull-right" id="start_date" name="start_date" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="end_date" class="col-md-3 control-label">End Date</label>
                        <div class="col-md-8">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" placeholder="End Date" class="form-control pull-right" id="end_date" name="end_date" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="remarks" class="col-md-3 control-label">Remarks</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="remarks" name="remarks" placeholder="Remarks">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="discount_1" class="col-md-3 control-label">Discount 1</label>
                        <div class="col-md-8">
                            <input type="text" min="0" max="100" class="form-control" id="discount_1" name="discount_1" placeholder="Discount 1 (20.5 OR 2.6)" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="discount_2" class="col-md-3 control-label">Discount 2</label>
                        <div class="col-md-8">
                            <input type="text" min="0" max="100" class="form-control" id="discount_2" name="discount_2" placeholder="Discount 2 (20.5 OR 2.6)">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="disc_type" class="col-md-3 control-label">Discount Type</label>
                        <div class="col-md-8">
                            <select id="disc_type" name="disc_type" class="form-control">
                                <option value="0">Percent</option>
                                <option value="1">Rp</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-check" id="group_outlet">
                            <label for="outlet" class="col-md-3 control-label">Branch *</label>
                            <div class="col-md-8">
                                <select class="form-control select2 select2-hidden-accessible" multiple="multiple" data-placeholder="Select a Outlet" name="outlet[]" id="outlet" tabindex="-1" aria-hidden="true" required>
                                    @foreach ($outlet as $list)
                                    <option value="{{ $list->id_outlet }}">{{ $list->outlet_name }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block text-red" id="msg_outlet"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="reusable_activity" class="col-md-3 control-label">Status Promo</label>
                        <div class="col-md-8">
                            <select id="reusable_activity" name="reusable_activity" class="form-control">
                                <option value="0">Selamanya</option>
                                <option value="1">1 Bulan 1x</option>
                                <option value="2">1 Tahun 1x</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-check" id="status_activity_modal">
                        <label for="activity_name" class="col-md-3 control-label"></label>
                        <div class="col-md-8">
                            <input type="checkbox" class="form-check-input" id="status_activity" name="status_activity" value="1">
                            <label for="activity_name" class="form-check-label">Enable</label>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
                </div>

            </form>

        </div>
    </div>
</div>

<div class="modal" id="modal-activityGroup" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <form class="form-horizontal" data-toggle="validator" method="post">
                {{ csrf_field() }} {{ method_field('POST') }}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times fa-lg"></i>
                    </button>
                    <h3 class="modal-title"></h3>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id_activity_group" name="id_activity_group">
                    <div class="form-group">
                        <label for="activity_group_name" class="col-md-3 control-label">Activity Group Name</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="activity_group_name" name="activity_group_name" placeholder="Activity Group Name" required>
                        </div>
                    </div>
                    <div class="form-check" id="status_activity_group_modal">
                        <label for="activity_group_name" class="col-md-3 control-label"></label>
                        <div class="col-md-8">
                            <input type="checkbox" class="form-check-input" id="status_activity_group" name="status_activity_group" value="1">
                            <label for="activity_group_name" class="form-check-label">Enable</label>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
                </div>

            </form>

        </div>
    </div>
</div>

<div class="modal" id="modal-activityOutlet" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <form class="form-horizontal" data-toggle="validator" method="post">
                {{ csrf_field() }} {{ method_field('POST') }}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times fa-lg"></i>
                    </button>
                    <h3 class="modal-title"></h3>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id_ActivityOutlet" name="id_ActivityOutlet">

                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
                </div>

            </form>

        </div>
    </div>
</div>

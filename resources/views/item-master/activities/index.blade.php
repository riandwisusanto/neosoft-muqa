@extends('base')

@section('title')
    Marketing Activities
@endsection

@section('breadcrumb')
    @parent
    <li>Marketing Activities</li>
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
<style>
    #title-nav-tab-2,
    #btn-2 {
        display: none;
    }

    @media (max-width: 575.98px) {
        #btn-2,
        #btn-1 {
            clear: both;
            margin-top: 10px;
        }
    }
</style>
@endsection

@section('content')
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="pull-left header" id="title-nav-tab-1">
                <span style="font-size:20px; font-weight: bold;">MARKETING ACTIVITIES CATEGORY</span>
                <p style="font-size:15px; font-weight: bold;"><i>Manage all promotions, discounts, or marketing campaigns.</i></p>
            </li>
            <li class="pull-left header" id="title-nav-tab-2">
                <span style="font-size:20px; font-weight: bold;">MARKETING ACTIVITIES LIST</span>
                <p style="font-size:15px; font-weight: bold;"><i>Manage all promotions, discounts, or marketing campaigns.</i></p>
            </li>
        </ul>
        <ul class="nav nav-tabs" style="padding-left: 20px;">
            <br>
            <li class="active"><a href="#tab_1-1" data-toggle="tab" id="nav-tab-1">Marketing Activities Category</a></li>
            <li><a href="#tab_2-2" data-toggle="tab" id="nav-tab-2">Marketing Activities List</a></li>

            <a onclick="addFormActivityGroup()" class="btn btn-success btn-float-right" style="margin-right: 20px;" id="btn-1"><i class="fa fa-plus-circle"></i> Add Activities Category</a>
            <a onclick="addFormActivity()" class="btn btn-success btn-float-right" style="margin-right: 20px;" id="btn-2"><i class="fa fa-plus-circle"></i> Add Activity</a>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1-1">
                <div class="box-body">
                    <table class="table table-striped table-bordered dt-responsive nowrap tableActivityCategory" style="width:100%">
                        <thead>
                        <tr>
                            <th>Category Name</th>
                            <th>Enable</th>
                            <th class="all" width="10%">Action</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="tab-pane" id="tab_2-2">
                <div class="box-body">
                    <table class="table table-striped table-bordered nowrap tableActivity" style="width:100%">
                        <thead>
                        <tr>
                            <th>Activity Name</th>
                            <th>Activity Code</th>
                            <th>Category</th>
                            <th>PIC</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Disc 1</th>
                            <th>Disc 2</th>
                            <th>Remarks</th>
                            <th>Enable</th>
                            <th class="all" witdh="10%">Action</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('item-master.activities.form')
@endsection

@section('script')
    <script type="text/javascript">
        var tableActivity, tableActivityGroup, tableActivityOutlet, save_method;

        $('#start_date, #end_date').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });

        $(function(){
            format_money()
            //tableActivity = $('.tableActivity').DataTable({
                  //scrollY:        "500px",
            //      scrollX:        true,
            //      scrollCollapse: true,
            //      paging:         false,

            //     fixedColumns:   {
            //        leftColumns: 1,
            //    },
            //    "serverside" : true,
            //    "ajax" : {
            //        "url" : "{{ route('activity.data') }}",
            //        "type" : "GET"
            //    }
            //});

            function format_money(){
                var discount_1 = document.getElementById('discount_1');
                discount_1.addEventListener('keyup', function(e){
                    // tambahkan 'Rp.' pada saat form di ketik
                    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
                    discount_1.value = formatAngka(this.value);
                });
                var discount_2 = document.getElementById('discount_2');
                discount_2.addEventListener('keyup', function(e){
                    // tambahkan 'Rp.' pada saat form di ketik
                    // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
                    discount_2.value = formatAngka(this.value);
                });
            }

            function formatRupiah(angka){
                var number_string = angka.replace(/[^.\d]/g, '').toString(),
                    split   		= number_string.split('.'),
                    sisa     		= split[0].length % 3,
                    rupiah     		= split[0].substr(0, sisa),
                    ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);


                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? ',' : '';
                    console.log(separator);

                    rupiah += separator + ribuan.join(',');
                }

                return rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                // return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
            }
            function formatAngka(angka){
                var number_string = angka.replace(/[^.\d]/g, '').toString();

                return rupiah = number_string;
                // return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
            }

            tableActivityGroup = $('.tableActivityCategory').DataTable({
                "serverside" : true,
                "ajax" : {
                    "url" : "{{ route('activityGroup.data') }}",
                    "type" : "GET"
                }
            });


            $('#modal-activity form').validator().on('submit', function(e){
                if(!e.isDefaultPrevented()){
                    var id = $('#id_activity').val();
                    if(save_method == "add") url = "{{ route('activities.store') }}";
                    else url = "{{ route('activities.index') }}/"+id;

                    $.ajax({
                        url : url,
                        type : "POST",
                        data : $('#modal-activity form').serialize(),
                        success : function(data){
                            console.log(data)
                            $('#modal-activity').modal('hide');
                            tableActivity.ajax.reload();
                        },
                        error : function(e){
                            console.log(e)
                            alert("Can not Save the Data!");
                        }
                    });
                    return false;
                }
            });

            $('#modal-activityGroup form').validator().on('submit', function(e){
                if(!e.isDefaultPrevented()){
                    var id = $('#id_activity_group').val();
                    if(save_method == "add") url = "{{ route('activityGroup.store') }}";
                    else url = "{{ route('activityGroup.index') }}/"+id;

                    $.ajax({
                        url : url,
                        type : "POST",
                        data : $('#modal-activityGroup form').serialize(),
                        success : function(data){
                            console.log(data)
                            $('#modal-activityGroup').modal('hide');
                            tableActivityGroup.ajax.reload();
                        },
                        error : function(e){
                            console.log(e)
                            alert("Can not Save the Data!");
                        }
                    });
                    return false;
                }
            });

         });


        function selected_all_product_treatment() {
            if ($("#chkall_product_treatment").is(':checked')) {
                $("#activity_product_treatment_id > option").prop("selected", "selected");
                $("#activity_product_treatment_id").trigger("change");
            } else {
                $("#activity_product_treatment_id").find('option').prop("selected", false);
                $("#activity_product_treatment_id").trigger("change");
            }
        }

        function selected_all_package() {
            if ($("#chkall_package").is(':checked')) {
                $("#activity_package_id > option").prop("selected", "selected");
                $("#activity_package_id").trigger("change");
            } else {
                $("#activity_package_id").find('option').prop("selected", false);
                $("#activity_package_id").trigger("change");
            }
        }

        function addFormActivity(){
            save_method = "add";
            $('input[name=_method]').val('POST');
            $("#modal-activity").modal({
                backdrop: 'static',
                keyboard: false,
                show: true
            });
            $('#modal-activity form')[0].reset();
            $('#group_id').val('').change()

            $('.modal-title').text('Add Activity');
            document.getElementById('status_activity_modal').style.visibility = 'hidden';
        }

        function addFormActivityGroup(){
            save_method = "add";
            $('input[name=_method]').val('POST');
            $("#modal-activityGroup").modal({
                backdrop: 'static',
                keyboard: false,
                show: true
            });
            $('#modal-activityGroup form')[0].reset();
            $('#group_id').val('').change()
            $('.modal-title').text('Add Activities Category');

            document.getElementById('status_activity_group_modal').style.visibility = 'hidden';
        }


        function detailInfo(id) {

        }

        function editFormActivity(id){
            save_method = "edit";
            $('input[name=_method]').val('PUT');
            $('#modal-activity form')[0].reset();
            $.ajax({
                url : "activities/"+id+"/edit",
                type : "GET",
                dataType : "JSON",
                success : function(data){
                    //  console.log(data)
                    $('#modal-activity').modal('show');
                    $('.modal-title').text('Edit Activity');
                    $('#id_activity').val(data.id_activity);
                    $('#activity_name').val(data.activity_name);
                    $('#activity_pic').val(data.activity_pic);
                    $('#activity_group_id').val(data.activity_group_id);
                    $('#disc_type').val(data.disc_1_type).trigger('change');
                    $('#discount_1').val(data.discount_1);
                    $('#discount_2').val(data.discount_2);
                    $('#end_date').val(data.end_date);
                    $('#remarks').val(data.remarks);
                    $('#start_date').val(data.start_date);
                    $('#outlet').val(data.outlet).change();
                    $('#reusable_activity').val(data.reusable_activity).trigger('change');

                    let product_treatment_id = data.activity_product_treatments.map(row => {
                        return row.product_treatment_id
                    })

                    let package_id = data.activity_product_treatments.map(row => {
                        return row.package_id
                    })

                    $('#activity_product_treatment_id').val(product_treatment_id).change();
                    $('#activity_package_id').val(package_id).change();

                    $('#status_activity').prop('checked', data.status_activity);
                    document.getElementById('status_activity_modal').style.visibility = 'visible';


                },
                error : function(e){
                    console.log(e);
                    alert("Can not Show the Data!");
                }
            });
        }

        function editFormActivityGroup(id){
            save_method = "edit";
            $('input[name=_method]').val('PUT');
            $('#modal-activityGroup form')[0].reset();
            $.ajax({
                url : "{{ route('activityGroup.index') }}/"+id+"/edit",
                type : "GET",
                dataType : "JSON",
                success : function(data){
                    $('#modal-activityGroup').modal('show');
                    $('.modal-title').text('Edit Activities Category');

                    $('#id_activity_group').val(data.id_activity_group);
                    $('#activity_group_name').val(data.activity_group_name);

                    $('#status_activity_group').prop('checked', data.status_activity_group);
                    document.getElementById('status_activity_group_modal').style.visibility = 'visible';


                },
                error : function(){
                    alert("Can not Show the Data!");
                }
            });
        }

        function deleteDataActivity(id){
            swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this Activity!",
                    icon: "warning",
                    buttons: {
                        canceled:{
                            text:'Cancel',
                            value: 'cancel',
                            className: 'swal-button btn-default'
                        },
                        disabled:{
                            text:'Disable',
                            value: 'disable',
                            className: 'swal-button btn-warning'
                        },
                        deleted:{
                            text:'Delete',
                            value: 'delete',
                            className: 'swal-button btn-danger'
                        }
                    },
                    dangerMode: true,
                }).then((willDelete) => {
                    switch (willDelete) {
                        default:
                            swal("Activity is safe!");
                            break;
                        case 'disable':
                            $.ajax({
                                url : "/activities/"+id+"?disable=true",
                                type : "POST",
                                data : {'_method' : 'PUT', '_token' : $('meta[name=csrf-token]').attr('content')},
                                success : function(data){
                                    swal("Activity has been disabled!", {
                                        icon: "success",
                                    });
                                    tableActivity.ajax.reload();
                                },
                                error : function(){
                                    swal({
                                        text: 'Can not Disabled the Data!',
                                        icon: 'error'
                                    })
                                }
                            });
                            break;
                        case 'delete':
                         $.ajax({
                                url : "{{ route('activities.index') }}/"+id,
                                type : "POST",
                                data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
                                  success : function(data){
                                    if (data === "error") {
                                        swal({
                                            text: 'Activity is Used !',
                                            icon: 'error'
                                        })
                                    } else {
                                        swal("Activity  has been deleted!", {
                                            icon: "success",
                                        });
                                    }
                                    tableActivity.ajax.reload();
                                },
                                error : function(){
                                       swal({
                                        text: 'Can not Delete the Data!',
                                        icon: 'error'
                                    })

                                }
                            });
                            break;
                    }
                });
        }


        function deleteDataActivityGroup(id){

                swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this Activity Category!",
                    icon: "warning",
                    buttons: {
                        canceled:{
                            text:'Cancel',
                            value: 'cancel',
                            className: 'swal-button btn-default'
                        },
                        disabled:{
                            text:'Disable',
                            value: 'disable',
                            className: 'swal-button btn-warning'
                        },
                        deleted:{
                            text:'Delete',
                            value: 'delete',
                            className: 'swal-button btn-danger'
                        }
                    },
                    dangerMode: true,
                }).then((willDelete) => {
                    switch (willDelete) {
                        default:
                            swal("Activity Category is safe!");
                            break;
                        case 'disable':
                            $.ajax({
                                url : "{{ route('activityGroup.index') }}/"+id+"?disable=true",
                                type : "POST",
                                data : {'_method' : 'PUT', '_token' : $('meta[name=csrf-token]').attr('content')},
                                success : function(data){
                                    swal("Activity Category has been disabled!", {
                                        icon: "success",
                                    });
                                    tableActivityGroup.ajax.reload();
                                },
                                error : function(){
                                    swal({
                                        text: 'Can not Disabled the Data!',
                                        icon: 'error'
                                    })
                                }
                            });
                            break;
                        case 'delete':
                         $.ajax({
                                url : "{{ route('activityGroup.index') }}/"+id,
                                type : "POST",
                                data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
                                  success : function(data){
                                    if (data === "error") {
                                        swal({
                                            text: 'Activity Category is Used !',
                                            icon: 'error'
                                        })
                                    } else {
                                        swal("Activity Category has been deleted!", {
                                            icon: "success",
                                        });
                                    }
                                    tableActivityGroup.ajax.reload();
                                },
                                error : function(){
                                       swal({
                                        text: 'Can not Delete the Data!',
                                        icon: 'error'
                                    })

                                }
                            });
                            break;
                    }
                });
            }

            var first = true;
            function getDataTableActivity() {
                if (first == true) {
                    tableActivity = $('.tableActivity').DataTable({
                        scrollX:        true,
                        scrollCollapse: true,
                        paging:         false,

                        fixedColumns:   {
                            leftColumns: 1,
                        },
                        "serverside" : true,
                        "ajax" : {
                            "url" : "{{ route('activity.data') }}",
                            "type" : "GET"
                        }
                    });
                    first = false;
                }
            }

            $("#nav-tab-1").click(function(){
                $("#btn-1").show();
                $("#btn-2").hide();
                $("#title-nav-tab-1").show();
                $("#title-nav-tab-2").hide();
            });

            $("#nav-tab-2").click(function(){
                $("#btn-2").show();
                $("#btn-1").hide();
                $("#title-nav-tab-2").show();
                $("#title-nav-tab-1").hide();
                getDataTableActivity();
            });
    </script>

    <script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}"></script>
    <script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>
@endsection

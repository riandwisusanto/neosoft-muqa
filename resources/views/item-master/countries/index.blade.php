@extends('base')

@section('title')
  List Country
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">

@endsection

@section('breadcrumb')
   @parent
   <li>Country</li>
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <div class="row">
          <div class="col-sm-8">
              <span style="font-size:20px; font-weight: bold;">COUNTRY MANAGEMENT</span>
              <p style="font-size:15px; font-weight: bold;"><i>Manage all countries.</i></p>
          </div>
          <div class="col-sm-4">
              <a onclick="addForm()" class="btn btn-success btn-float-right"><i class="fa fa-plus-circle"></i> Add Country</a>
          </div>
        </div>
      </div>
      <div class="box-body">
        <table class="table table-striped table-bordered  nowrap table-countries" style="width:100%">
        <thead>
          <tr>
              <th>Country</th>
              <th class="all" width="10%">Action</th>
          </tr>
        </thead>
        <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@include('item-master.countries.form')
@endsection

@section('script')
<script type="text/javascript">
  var table, save_method;

  $('#datepicker, #datepicker2').datepicker({
    format: 'dd MM yyyy',
    autoclose: true,
    todayHighlight: true
  });

  $(function(){
  table = $('.table-countries').DataTable({

     "processing" : true,
    "serverside" : true,
    "ajax" : {
      "url" : "{{ route('countries.data') }}",
      "type" : "GET"
    }
  });

  $('#modal-countries form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented()){
         var id = $('#id').val();
         if(save_method == "add") url = "{{ route('countries.store') }}";
         else url = "countries/"+id;

         $.ajax({
            url : url,
            type : "POST",
            data : $('#modal-countries form').serialize(),
           success : function(data){
             console.log(data)
              $('#modal-countries').modal('hide');
              table.ajax.reload();
           },
           error : function(e){
             console.log(e)
             alert("Can not Save the Data!");
           }
         });
         return false;
     }
   });

});

function addForm(){
    save_method = "add";
    $('#outlet').val('');
    $("#outlet").trigger("change");
    $('input[name=_method]').val('POST');
    $("#modal-countries").modal({
        backdrop: 'static',
        keyboard: false,
        show: true
        });
    $('#modal-countries form')[0].reset();
        // document.getElementById('status_bank_modal').style.visibility = 'hidden';

    $('.modal-title').text('Add Country');
}

function detailInfo(id) {

}

function editForm(id){
   save_method = "edit";
   $('input[name=_method]').val('PUT');
   $('#modal-countries form')[0].reset();
   $.ajax({
     url : "countries/"+id+"/edit",
     type : "GET",
     dataType : "JSON",
     success : function(data){
       $('#modal-countries').modal('show');
       $('.modal-title').text('Edit Bank');

       $('#id').val(data.id_country);
       $('#country').val(data.country);


     },
     error : function(){
       alert("Can not Show the Data!");
     }
   });
}

function deleteData(id){
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this Country!",
        icon: "warning",
        buttons: {
            canceled:{
                text:'Cancel',
                value: 'cancel',
                className: 'swal-button btn-default'
            },
            // disabled:{
            //     text:'Disable',
            //     value: 'disable',
            //     className: 'swal-button btn-warning'
            // },
            deleted:{
                text:'Delete',
                value: 'delete',
                className: 'swal-button btn-danger'
            }
        },
        dangerMode: true,
    }).then((willDelete) => {
        switch (willDelete) {
            default:
                swal("Country is safe!");
                break;
            case 'disable':
                $.ajax({
                    url : "/countries/"+id+"?disable=true",
                    type : "POST",
                    data : {'_method' : 'PUT', '_token' : $('meta[name=csrf-token]').attr('content')},
                    success : function(data){
                        swal("Country has been disabled!", {
                            icon: "success",
                        });
                        table.ajax.reload();
                    },
                    error : function(){
                        swal({
                            text: 'Can not Disabled the Data!',
                            icon: 'error'
                        })
                    }
                });
                break;
            case 'delete':
                $.ajax({
                    url : "/countries/"+id,
                    type : "POST",
                    data : {'_method' : 'Delete', '_token' : $('meta[name=csrf-token]').attr('content')},
                    success : function(data){
                        if (data === "error") {
                            swal({
                                text: 'Country is Used !',
                                icon: 'error'
                            })
                        } else {
                            swal("Country has been deleted!", {
                                icon: "success",
                            });
                        }
                        table.ajax.reload();
                    },
                    error : function(){
                        swal({
                            text: 'Can not Delete the Data!',
                            icon: 'error'
                        })
                    }
                });
                break;
        }
    });
}


// validasi
  $(document).ready(function(){
    // $("#noktp").keypress(function(data){
    //   //console.log(data.which);
    // if(data.which < 48 || data.which > 57)
    //   {
    //     $("#msg_ktp").html("Please insert real IC numbers").show().fadeOut(4000);
    //     return false;
    //   }
    // });

    // $("#contact").keypress(function(data){
    //   //console.log(data.which);
    // if(data.which < 48 || data.which > 57)
    //   {
    //     $("#msg_phone").html("Please insert real numbers phone").show().fadeOut(4000);
    //     return false;
    //   }
    // });

    // $("#familyphone").keypress(function(data){
    //   //console.log(data.which);
    // if(data.which < 48 || data.which > 57)
    //   {
    //     $("#msg_family").html("Please insert real numbers phone").show().fadeOut(4000);
    //     return false;
    //   }
    // });
  });
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>
@endsection

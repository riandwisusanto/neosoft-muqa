@extends('base')

@section('title')
List Products
@endsection

@section('breadcrumb')
@parent
<li>Products</li>
@endsection

@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
<style>
    .price-log-item {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        align-items: flex-start;
    }
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <span style="font-size:20px; font-weight: bold;">PRODUCT MANAGEMENT</span>
                <p style="font-size:15px; font-weight: bold;"><i>Manage all products.</i></p>
            </div>
            
            <div class="box-body">
                <table class="table table-striped table-bordered nowrap table-product" style="width:100%">
                    <thead>
                        <tr>
                            <th>Products Code</th>
                            <th>Products Name</th>
                            <th>Create Date</th>
                            <th>Products Price</th>
                            <th>Duration</th>
                            <th>Point</th>
                            <th>Status</th>
                            <th class="all" width="10%">Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@include('item-master.products.form')
@endsection

@section('script')
<script type="text/javascript">
    var table, save_method;

        $('#datepicker, #datepicker2').datepicker({
            format: 'dd MM yyyy',
            autoclose: true,
            todayHighlight: true
        });

        $(function () {
            table = $('.table-product').DataTable({
                //scrollY:        "500px",
                scrollX:        true,
                scrollCollapse: true,
                paging:         false,
                fixedColumns:   {
                    leftColumns: 1,
                    rightColumns: 1
                },
                "ajax": {
                    "url": "{{ route('products.data') }}",
                    "type": "GET"
                }
            });

            tablePriceLog = $('#price_log_table').DataTable({
                processing: false,
                serverSide: false,
                paging: false,
                scrollY: 70,
                scrollX:        true,
                scrollCollapse: true,
                searching: false,
                ordering:  false,
                bInfo: false,
            }),

                format_money();

            $('#modal-products form').validator().on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    var id = $('#id').val();

                    url = "{{ route('products.store') }}";

                    if (save_method == 'edit') {
                        url = "{{ route('products.index') }}/" + id;
                    }
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: $('#modal-products form').serialize(),
                        success: function (data) {
                            $('#modal-products').modal('hide');
                            resetPointModal();
                            table.ajax.reload();
                        },
                        error : function(xhr, status, error){
                            var text = "";
                            $.each(xhr.responseJSON.errors, function (key, item) {
                                text += item;
                            });
                            swal({
                                text: text,
                                icon: 'error'
                            })
                        }
                    });
                    return false;
                }
            });

        });

        function resetPointModal(){
            $('#pointForm').html('<div id="pointForm">'+
                '<div class="form-group">'+
                '<label for="point" class="col-md-3 control-label">Point</label>'+
                '<div class="col-md-8">'+
                '<div class="input-group">'+
                '<input id="point" type="text" class="form-control" name="point">'+
                '<span class="input-group-btn">'+
                '<button type="button" name="button" onClick="addPoint()" class="btn btn-default" id="AddItem"><i class="fa fa-plus-circle"></i></button>'+
                '</span>'+
                '</div>'+
                '<span class="help-block with-errors"></span>'+
                '</div>'+
                '</div>'+
                '<input type="hidden" name="save_point" id="save_point" value="[]">');
            $('#save_point').attr('value','[]')
        }

        function addForm() {
            save_method = "add";
            $('input[name=_method]').val('POST');
            $("#modal-products").modal({
                backdrop: 'static',
                keyboard: false,
                show: true
            });

            $('#outlet').val('');
            $("#outlet").trigger("change");
            $('#modal-products form')[0].reset();
            $('.modal-title').text('Add Product');
            $('#price_log_group').hide();
            document.getElementById('status_product_treatment_modal').style.visibility = 'hidden';
        }

        function format_money(){
            var rupiah = document.getElementById('harga');
            rupiah.addEventListener('keyup', function(e){
                // tambahkan 'Rp.' pada saat form di ketik
                // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
                rupiah.value = formatRupiah(this.value);
            });

            let commissionSalesDr = document.getElementById('commissionSalesDr');
            var typeDr = $('#commSalesTypeDr').val();
            $('#commSalesTypeDr').on('change', function() {
                typeDr = this.value;
                commissionSalesDr.addEventListener('keyup', function(e){
                    if(typeDr == 1){
                            commissionSalesDr.value = formatRupiah(this.value);
                    }else{
                            commissionSalesDr.value = formatAngka(this.value);
                    }
                });

            });

            let commissionSalesTr = document.getElementById('commissionSalesTr');

            var typeTr = $('#commSalesTypeTr').val();
            $('#commSalesTypeTr').on('change', function() {
                typeTr = this.value;
                commissionSalesTr.addEventListener('keyup', function(e){
                    if(typeTr == 1){
                            commissionSalesTr.value = formatRupiah(this.value);
                    }else{
                            commissionSalesTr.value = formatAngka(this.value);
                    }
                });

            });
        }

        function formatRupiah(angka){
            var number_string = angka.replace(/[^.\d]/g, '').toString(),
                split   		= number_string.split('.'),
                sisa     		= split[0].length % 3,
                rupiah     		= split[0].substr(0, sisa),
                ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);


            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                separator = sisa ? ',' : '';
                console.log(separator);

                rupiah += separator + ribuan.join(',');
            }

            return rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            // return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }

        function formatAngka(angka) {
            var number_string = angka.replace(/[^.\d]/g, '').toString();

            return rupiah = number_string;
            // return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }

        function detailInfo(id) {

        }

        function deleteData(id) {
            swal({
                    title: 'Hapus data?',
                    text: "Data yang dihapus tidak dapat dikembalikan",
                    icon: 'warning',
                    buttons: {
                        cancel: {
                            text: "Batal",
                            value: null,
                            visible: true,
                            className: "",
                            closeModal: true,
                        },
                        confirm: {
                            text: "Hapus",
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: true
                        }
                    }
                }).then((value) => {
                    if (value) {
                        $.ajax({
                            url: "products/" + id + "/delete?_token={{ csrf_token() }}",
                            type: "POST",
                            success: function (data) {
                                swal({
                                    title: 'Berhasil',
                                    text: 'data telah dihapus'
                                })

                                table.ajax.reload();
                            }
                        })
                    }
                })
        }

        function editForm(id) {
            save_method = "edit";
            $('input[name=_method]').val('PUT');
            $('#modal-products form')[0].reset();
            $.ajax({
                url: "products/" + id + "/edit",
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    $('#modal-products').modal('show');
                    $('.modal-title').text('Edit Product');

                    $('#id').val(data.id_product_treatment);
                    $('#name').val(data.product_treatment_name);
                    $('#harga').val(data.price);
                    $('#price_log_header').show();
                    $('#durasi').val(data.duration);
                    $('#outlet').val(data.outlet).change();

                    
                    if(data.commSalesTypeDr == 1){
                        $('#commissionSalesDr').val(data.commissionSalesDr.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
                    }else{
                        $('#commissionSalesDr').val(data.commissionSalesDr);
                    }

                    $('#commSalesTypeDr').val(data.commSalesTypeDr).trigger('change');

                    if(data.commSalesTypeTr == 1){
                        $('#commissionSalesTr').val(data.commissionSalesTr.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
                    }else{
                        $('#commissionSalesTr').val(data.commissionSalesTr);
                    }

                    $('#commSalesTypeTr').val(data.commSalesTypeTr).trigger('change');

                    data.product_treatment_points.forEach(element => {
                        addPoint(element.id_product_treatment_points, element.point)
                    });
                    $('#price_log_group').show();
                    tablePriceLog.rows().remove().draw();
                    data.price_logs.forEach((element,index)=>{
                        let date = new Date(element.created_at);
                        if (index == 0){
                            tablePriceLog.row.add([date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear()+' '+date.getHours()+':'+date.getMinutes(),element.edited_by.name,'<strong>(Current)</strong> '+element.price]).draw();
                        }else if (index == 1){
                            tablePriceLog.row.add([date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear()+' '+date.getHours()+':'+date.getMinutes(), element.edited_by.name,'<strong>(Previous)</strong> '+element.price]).draw();
                        }else {
                            tablePriceLog.row.add([date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear()+' '+date.getHours()+':'+date.getMinutes(), element.edited_by.name, element.price]).draw();
                        }
                    })

                    $('#status_product_treatment').prop('checked', data.status_product_treatment);
                    document.getElementById('status_product_treatment_modal').style.visibility = 'visible';

                },
                error: function () {
                    alert("Can not Show the Data!");
                }
            });
        }

        $('#close-modal').on('click', function () {
            resetPointModal();
            $('#modal-products').modal('hide');
        })
        $('#close').on('click', function () {
            resetPointModal();
            $('#modal-products').modal('hide');
        })
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>
@endsection

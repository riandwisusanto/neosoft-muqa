<div class="modal" id="modal-products" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <form class="form-horizontal" data-toggle="validator" method="post">
                {{ csrf_field() }} {{ method_field('POST') }}

                <div class="modal-header">
                    <button type="button" id="close" class="close" aria-label="Close">
                        <i class="fa fa-times fa-lg"></i>
                    </button>
                    <h3 class="modal-title"></h3>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    {{-- <input type="hidden" id="type" name="type" value="T"> --}}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name" class="col-md-3 control-label">Product Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="name" name="name"
                                        placeholder="Product Name" autocomplete="off" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="harga" class="col-md-3 control-label">Price</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="harga" name="harga" placeholder="Price"
                                        rel="price" required>
                                </div>
                            </div>

                            <div class="form-group" id="price_log_group">
                                <label for="price_log" class="col-md-3 control-label">Price Log</label>
                                <div class="col-md-10 col-md-offset-2">
                                    <table id="price_log_table" class="table table-striped table-bordered nowrap"
                                        style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>Edited At</th>
                                                <th>Edited By</th>
                                                <th>Price</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="durasi" class="col-md-3 control-label">Duration</label>
                                <div class="col-md-8">
                                    <input type="number" class="form-control" id="durasi" name="durasi"
                                        placeholder="Ex : 240" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Branch</label>
                                <div class="col-md-8">
                                    <select id="outlet" name="outlet[]"
                                        class="form-control select2 select2-hidden-accessible" multiple="multiple"
                                        data-placeholder="Select a Outlet" style="width: 100%;" tabindex="-1"
                                        aria-hidden="true" required>
                                        @foreach ($outlets as $list)
                                        <option value="{{ $list->id_outlet }}">{{ $list->outlet_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="commissionSalesDr" class="col-md-3 control-label" style="font-size:12px;">Sales Commission for Doctor (%) Or (Rp)</label>
                                <div class="col-md-5">
                                    <input type="text" min="0" max="100" class="form-control" id="commissionSalesDr"
                                        name="commissionSalesDr" placeholder="Sales Commission For Doctor (2.5 OR 25)" autocomplete="off">
                                </div>
                                <div class="col-md-3">
                                    <select id="commSalesTypeDr" name="commSalesTypeDr" class="form-control">
                                        <option value="0">Percent</option>
                                        <option value="1">Rp</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="commissionSalesTr" class="col-md-3 control-label" style="font-size:12px;">Sales Commission for Therapist (%) Or (Rp)</label>
                                <div class="col-md-5">
                                    <input type="text" min="0" max="100" class="form-control" id="commissionSalesTr"
                                        name="commissionSalesTr" placeholder="Sales Commission For Therapist (2.5 OR 25)" autocomplete="off">
                                </div>
                                <div class="col-md-3">
                                    <select id="commSalesTypeTr" name="commSalesTypeTr" class="form-control">
                                        <option value="0">Percent</option>
                                        <option value="1">Rp</option>
                                    </select>
                                </div>
                            </div>
                            <div id="pointForm">
                                <div class="form-group">
                                    <label for="point" class="col-md-3 control-label">Point</label>
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <input id="point" type="number" class="form-control" name="point">
                                            <span class="input-group-btn">
                                                <button type="button" name="button" onClick="addPoint()"
                                                    class="btn btn-default" id="AddItem"><i
                                                        class="fa fa-plus-circle"></i></button>
                                            </span>
                                        </div>
                                        <span class="help-block with-errors"></span>
                                    </div>
                                </div>
                                <input type="hidden" name="save_point" id="save_point" value="[]">
                            </div>
                            <div class="form-check" id="status_product_treatment_modal">
                                <label for="product_treatment_name" class="col-md-3 control-label"></label>
                                <div class="col-md-8">
                                    <input type="checkbox" class="form-check-input" id="status_product_treatment"
                                        name="status_product_treatment" value="1">
                                    <label for="product_treatment_name" class="form-check-label">Status</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                    <button type="button" id="close-modal" class="btn btn-danger"><i
                            class="fa fa-arrow-circle-left"></i>
                        Cancel</button>
                </div>

            </form>

        </div>
    </div>
</div>
<script>
    function addPoint(idPoint, point){
    if(typeof point == 'undefined') point =  $('#point').val();
    if(typeof idPoint == 'undefined') idPoint = new Date().getTime();
    let data_point = JSON.parse($('#save_point').val());
    data_point.push({
      id: idPoint,
      point: point
    });
    $('#save_point').attr('value', JSON.stringify(data_point));
    let formPoint = $('#pointForm').html();
    formPoint = formPoint+
                          '<div class="form-group" id="data_'+idPoint+'">'+
                              '<div for="point" class="col-md-3"></div>'+
                              '<div class="col-md-6">'+
                                '<div class="input-group">'+
                                    '<input type="text" id="input_'+idPoint+'" class="form-control" value="'+point+'" disabled>'+
                                    '<span class="input-group-btn">'+
                                      '<button type="button" id="edit_'+idPoint+'" class="btn btn-default" onClick="editPoint('+idPoint+')" id="edit_'+idPoint+'" class="btn-edit-item" data-id="0"><i class="fa fa-edit"></i></button>'+
                                      '<button type="button" id="save_'+idPoint+'" class="btn btn-default hidden" onClick="savePoint('+idPoint+')" id="edit_'+idPoint+'" class="btn-edit-item" data-id="0"><i class="fa fa-save"></i></button>'+
                                      '<button type="button" id="delete_'+idPoint+'"class="btn btn-default" onClick="deletePoint('+idPoint+')" id="delete_'+idPoint+'" class="btn-delete-item" data-id="0"><i class="fa fa-trash"></i></button>'+
                                    '</span>'+
                                '</div>'+
                              '</div>'+
                          '</div>'+
                        '</div>'
    $('#pointForm').html(formPoint);
  }

  function editPoint(id){
    $('#input_'+id).removeAttr('disabled');
    $('#edit_'+id).addClass('hidden');
    $('#save_'+id).removeClass('hidden');

  }

  function savePoint(id){
    $('#input_'+id).attr('disabled','');
    $('#edit_'+id).removeClass('hidden');
    $('#save_'+id).addClass('hidden');
    let point =  $('#input_'+id).val();
    let data_point = JSON.parse($('#save_point').val());
    data_point = data_point.map(data=>{
      if(data.id === id){
        data.point = point
      }
      return data;
    })
    $('#save_point').attr('value',JSON.stringify(data_point));
  }

  function deletePoint(id){
    $('#data_'+id).remove();
    let data_point = JSON.parse($('#save_point').val());
    data_point = data_point.filter(data=>{
      return data.id != id;
    })
    $('#save_point').attr('value',JSON.stringify(data_point));
  }

</script>

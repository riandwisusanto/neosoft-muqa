@extends('base')
@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/buttons/css/buttons.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/buttons/css/buttons.bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
<style>
</style>
@endsection
@section('content')
<div class="row">
    <!-- /.col -->
    <div class="col-xl-10 col-md-12">
        <div class="box box-solid">
           
        </div>
    </div>
</div>
<div class="nav-tabs-custom">
  <div class="box-header with-border">
    <span style="font-size:20px;">Dashboard</span>
    <p style="font-size:15px;"><i>Manajemen laporan (collections, revenue, sales pasien baru, dan sales pasien lama) ditampilkan dalam bentuk grafik, pelaporan secara real time.</i></p>
  </div>
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs ui-sortable-handle">
      <li class="active"><a href="#dashboard" id="nav-tab-1" data-toggle="tab" aria-expanded="true" >Dashboard</a></li>
      <li class=""><a href="#product-treatment" id="nav-tab-2" data-toggle="tab" aria-expanded="true" >Product Treatment</a></li>
    </ul>

    <form class="form-horizontal" data-toggle="validator" id="form_data" style="margin-bottom: 0px;">
      {{ csrf_field() }}
      <div class="box-body">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-4">
            <div class="form-group" style="margin-bottom: 0px;">
              <label class="col-xs-12 col-sm-12 col-md-5 control-label" for="outlets" style="text-align: left !important;">Select Time Period</label>
              <div class="col-xs-12 col-sm-12 col-md-7">
                  <select style="width: 100%;" id="daterange" class="form-control" name="daterange" required>
                    <option value="today">Today</option>
                    <option value="yesterday">Yesterday</option>
                    <option value="thisweek">This Week</option>
                    <option value="lastweek">Last Week</option>
                    <option value="thismonth">This Month</option>
                    <option value="lastmonth">Last Month</option>
                    <option value="thisyear">This Year</option>
                    <option value="lastyear">Last Year</option>
                    <option value="quarter1">Quarter I</option>
                    <option value="quarter2">Quarter II</option>
                    <option value="quarter3">Quarter III</option>
                    <option value="quarter4">Quarter IV</option>
                    <option value="semester1">Semester I</option>
                    <option value="semester2">Semester II</option>
                    <option value="customperiod">Custom Period</option>
                  </select>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-2" id="date-range-picker">
            <div class="form-group" style="margin-bottom: 0px;">
              <div class="col-xs-12">
                <input style="width: 100%;" type="text" name="daterangepicker" id="daterangepicker" class="form-control" autocomplete="off" value="" required>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-3">
            <div class="form-group" style="margin-bottom: 0px;">
              <div class="col-xs-12">
                <select style="width: 100%;" id="outlet" class="form-control" name="outlet[]" multiple="multiple" required>
                  @foreach ($outlet as $list)
                    <option value="{{ $list->id_outlet }}" selected="selected">{{ $list->outlet_name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-2">
            <input type="submit" value="Apply" class="btn btn-primary">
          </div>
        </div>
      </div>
    </form>

    <div class="tab-content no-padding">
      <div class="chart tab-pane active" id="dashboard" style="position: relative; min-height: 500px;">
        @include('dashboard.dashboard')
      </div>
      <div class="chart tab-pane" id="product-treatment" style="position: relative; min-height: 500px;">
        @include('dashboard.product-treatment')
      </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('plugins/chartjs/Chart.js') }}"></script>
<script>
  var table_geografis, table_productranking, table_treatmentranking, patientChartData, collectionChartData, collectionChartLabels, productTreatmentChartLabels, productChart, treatmentChart, percentage1 = 0.2, percentage2 = 0.3;

  var collectionChartCanvas = document.getElementById('collectionChart');
  var collectionChart = new Chart(collectionChartCanvas, {
    type: 'bar',
    // type: 'horizontalBar',
    data: {
      // labels: a,
      datasets: [{
        barPercentage: percentage1,
        label: "Collection",
        // data: b,
        backgroundColor: "rgba(139, 105, 174, 0.7)",
        hoverBackgroundColor: "rgba(139, 105, 174, 1)"
      }]
    },
    options: {
      maintainAspectRatio: false,
      legend: {
        onClick: (e) => e.stopPropagation()
      },
      tooltips: {
        callbacks: {
          // title: function(tooltipItems, data) {
          //   return data.datasets[tooltipItems[0].datasetIndex].label;
          // },
          label: function(tooltipItem, data) {
            datasetsLabel = data.datasets[tooltipItem.datasetIndex].label;
            tooltipItem = tooltipItem.yLabel.toString();
            tooltipItem = tooltipItem.split(/(?=(?:...)*$)/);
            tooltipItem = tooltipItem.join(',');
            return ' ' + datasetsLabel + ': Rp. ' + tooltipItem;
          }
        }
      },
      scales: {
        yAxes: [{
          gridLines: {
            display: true,
            color: "rgba(139, 105, 174, 0.3)"
          },
          ticks: {
            beginAtZero: true,
            // callback: function(value, index, values) {
            //   value = value.toString();
            //   value = value.split(/(?=(?:...)*$)/);
            //   value = value.join(',');
            //   return 'Rp. ' + value;
            // }
            callback: function(label, index, labels) {
              return label/1000+'K'
              // if (Math.floor(label) === label) {
              //   return label/1000+'K'
              // }
            }
          }
        }],
        xAxes: [{
          // barPercentage: 0.3,
          gridLines: {
            display: true,
            color: "rgba(139, 105, 174, 0.3)"
          }
        }]
      }
    }
  });

  var productTreatmentChartCanvas = document.getElementById('productTreatmentChart');
  var productTreatmentChart = new Chart(productTreatmentChartCanvas, {
    type: 'bar',
    data: {
      // labels: a,
      datasets: [{
        label: "Product",
        // data: b,
        backgroundColor: "rgba(139, 105, 174, 0.7)",
        hoverBackgroundColor: "rgba(139, 105, 174, 1)"
      }, {
        label: "Treatment",
        // data: c,
        backgroundColor: "rgba(186, 207, 193, 0.7)",
        hoverBackgroundColor: "rgba(186, 207, 193, 1)"
      }]
    },
    options: {
      maintainAspectRatio: false,
      legend: {
        onClick: (e) => e.stopPropagation()
      },
      scales: {
        yAxes: [{
          gridLines: {
            display: true,
            color: "rgba(139, 105, 174, 0.3)"
          },
          ticks: {
            beginAtZero: true,
            userCallback: function(label, index, labels) {
              if (Math.floor(label) === label) {
                return label;
              }
            }
          }
        }],
        xAxes: [{
          barPercentage: percentage2,
          // barPercentage: 0.6,
          gridLines: {
            display: true,
            color: "rgba(139, 105, 174, 0.3)"
          }
        }]
      }
    }
  });

  var patientChartCanvas = document.getElementById('patientChart');
  var patientChartConfig = {
    type: 'doughnut',
    data: {
      labels: ["New Patient", "Existing Patient"],
      datasets: [{

        backgroundColor: [
          "rgba(186, 207, 193, 0.7)", "rgba(139, 105, 174, 0.7)"
        ],
        hoverBackgroundColor: [
          "rgba(186, 207, 193, 1)", "rgba(139, 105, 174, 1)"
        ],
        // data: [0,0]
      }]
    },
    options: {
      cutoutPercentage: 85,
      legend: {
        display: true,
        position: 'left',
        onClick: (e) => e.stopPropagation()
      },
      tooltips: {
        enabled: true
      }
    }
  };
  var patientChart = new Chart(patientChartCanvas, patientChartConfig);

  function chartResize() {
    if ($(window).width() < 575.98) {
      percentage1 = 0.7;
      percentage2 = 1;
    } else if ($(window).width() < 992) {
      percentage1 = 0.3;
      percentage2 = 0.6;
    }
    updateCollectionChart(collectionChartLabels,collectionChartData);
    updateProductTreatmentChart(productTreatmentChartLabels,productChart,treatmentChart);
    updatePatientChart(patientChartData);
  }
  $(window).resize(function(){
    chartResize();
  });

  $.getJSON('/dashboard/alldata', function(data) {
    $('#collection').html(data.collection);
    $('#revenue').html(data.revenue);
    $('#patient-total').html(data.customer);
    $('#new-patient').html(data.new_customer);
    $('#old-patient').html(data.old_customer);
    $('#new-patient-percent').html(data.new_customer_percent);
    $('#old-patient-percent').html(data.old_customer_percent);
    patientChartData = data.patient_chart;
    collectionChartLabels = data.chart_collection_labels;
    collectionChartData = data.chart_collection_data;
    productTreatmentChartLabels = data.chart_product_treatment_labels;
    productChart = data.chart_product;
    treatmentChart = data.chart_treatment;
    chartResize();
  });
  
  function updateCollectionChart(a,b) {
    collectionChart.data.labels = a;
    collectionChart.data.datasets[0].data = b;
    collectionChart.data.datasets[0].barPercentage = percentage1;
    collectionChart.update();
  }
  
  function updatePatientChart(a){
    patientChartConfig.data.datasets[0].data = a;
    patientChart.update();
  }
  
  function updateProductTreatmentChart(a,b,c) {
    productTreatmentChart.data.labels = a;
    productTreatmentChart.data.datasets[0].data = b;
    productTreatmentChart.data.datasets[1].data = c;
    productTreatmentChart.data.datasets[0].barPercentage = percentage2;
    productTreatmentChart.data.datasets[1].barPercentage = percentage2;
    productTreatmentChart.update();
  }

  $('#daterangepicker').daterangepicker();

  $("#daterange").on('change',function(){
    if ($(this).val() == "customperiod") {
      $('#date-range-picker').show()
    } else {
      $('#date-range-picker').hide()
    }
  });

  $(function() {
    $('#date-range-picker').hide();

    table_geografis = $('.table-geografis').DataTable( {
        processing: true,
        serverside: false,
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,
        "lengthChange": false,
        "searching": false,
        "info": false,
        "columnDefs": [{
          "searchable": false,
          "orderable": false,
          "targets": 0
        }],
        "order": [[ 5, 'desc' ]],
        "ajax" : {
          "url" : "{{ route('dashboard.geografis') }}",
          "type" : "GET"
        }
    });
    table_geografis.on( 'order.dt search.dt', function () {
      table_geografis.column(0, {search:'applied', order:'applied'}).nodes().each(function(cell, i) {
        cell.innerHTML = i+1;
      });
    }).draw();

    table_productranking = $('.table-productranking').DataTable( {
        processing: true,
        serverside: false,
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,
        "lengthChange": false,
        "searching": false,
        "info": false,
        "order": [[ 2, "desc" ]],
        "columnDefs": [
          {"orderable": false, "targets": [0]}
        ],
        "ajax" : {
          "url" : "{{ route('dashboard.productranking') }}",
          "type" : "GET"
        }
    });
    table_productranking.on( 'order.dt search.dt', function () {
      table_productranking.column(0, {search:'applied', order:'applied'}).nodes().each(function(cell, i) {
        cell.innerHTML = i+1;
      });
    }).draw();

    table_treatmentranking = $('.table-treatmentranking').DataTable( {
        processing: true,
        serverside: false,
        scrollX:        true,
        scrollCollapse: true,
        width: '100%',
        paging:         false,
        "lengthChange": false,
        "searching": false,
        "info": false,
        "order": [[ 2, "desc" ]],
        "columnDefs": [
          {"orderable": false, "targets": [0]}
        ],
        "ajax" : {
          "url" : "{{ route('dashboard.treatmentranking') }}",
          "type" : "GET"
        }
    });
    table_treatmentranking.on( 'order.dt search.dt', function () {
      table_treatmentranking.column(0, {search:'applied', order:'applied'}).nodes().each(function(cell, i) {
        cell.innerHTML = i+1;
      });
    }).draw();
  });

  $('#form_data').validator().on('submit', function(e){
    if(!e.isDefaultPrevented()){
      $.ajax({
        type: 'GET',
        url: '/dashboard/alldata',
        data : $('#form_data').serialize(),
        success: function(data){
          $('#collection').html(data.collection);
          $('#revenue').html(data.revenue);
          $('#patient-total').html(data.customer);
          $('#new-patient').html(data.new_customer);
          $('#old-patient').html(data.old_customer);
          $('#new-patient-percent').html(data.new_customer_percent);
          $('#old-patient-percent').html(data.old_customer_percent);
          patientChartData = data.patient_chart;
          collectionChartLabels = data.chart_collection_labels;
          collectionChartData = data.chart_collection_data;
          productTreatmentChartLabels = data.chart_product_treatment_labels;
          productChart = data.chart_product;
          treatmentChart = data.chart_treatment;
          updateCollectionChart(collectionChartLabels,collectionChartData);
          updateProductTreatmentChart(productTreatmentChartLabels,productChart,treatmentChart);
          updatePatientChart(patientChartData);
          return false;
        }
      });

      table_geografis.ajax.url("{{ route('dashboard.geografis') }}?"+$('#form_data').serialize()).load();
      table_productranking.ajax.url( "{{ route('dashboard.productranking') }}?"+$('#form_data').serialize()).load();
      table_treatmentranking.ajax.url( "{{ route('dashboard.treatmentranking') }}?"+$('#form_data').serialize()).load();
    }
    return false;
  });

</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/dataTables.buttons.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}">
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>
@endsection

@component('mail::message')
# Hello

Thanks for using our application

{{-- @component('mail::button', ['url' => '#'])
OKE
@endcomponent --}}

{{-- <img src="{{ asset('img/logo.png') }}" alt=""> --}}

{{-- @component('mail::panel')
This is the panel content.
@endcomponent --}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent

@extends('base')
@section('title')
Appointments
@endsection
@section('style')
<link href='{{ asset('calendar/core/main.min.css') }}' rel='stylesheet' />
<link href='{{ asset('calendar/timegrid/main.min.css') }}' rel='stylesheet' />
<link href='{{ asset('calendar/timeline/main.min.css') }}' rel='stylesheet' />
<link href='{{ asset('calendar/resource-timeline/main.min.css') }}' rel='stylesheet' />
<link href='{{ asset('css/headerCalendar.css') }}' rel='stylesheet' />
{{-- <link rel="stylesheet" href="https://fullcalendar.io/releases/list/4.1.0/main.min.css"> --}}
{{-- <style>
 /* .fc-header-left, fc-header-center, fc-header-right {
     width: 100%;
     display: block;
 } */
 #calendar .fc-today {
     background: #F4FFB3 !important;
 }
 #calendar .fc-event {
     font-size: .75em;
 }
 .fc-center{
     margin-left:120px;
 }
</style> --}}
<style>
 .fc-title{
     font-weight:bold;
     font-size:13px;
 }
 .fc-des{
     font-weight:normal;
 }
 /* .fc-right{
     margin-top:-135px;
 
 } */
</style>
@endsection
@section('breadcrumb')
@parent
<li>Appointments</li>
@endsection
@section('content')
<div class="row">
 <!-- /.col -->
 <div class="col-xl-10 col-md-12">
     <div class="box box-solid">
         <div class="box-header with-border">
             <div class="row">
                 <div class="col-sm-8">
                     <span style="font-size:20px; font-weight: bold;">APPOINTMENT BY ROOMS</span>
                     <p style="font-size:15px; font-weight: bold;"><i>Display all appointment by rooms.</i></p>
                 </div>
                 <div class="col-sm-4">
                         <div class="pull-right">
                             <button type="button" class="external-event btn btn-danger" onclick="addCustomer()"><i
                                     class="fa fa-plus"></i>
                                 New
                                 Customers</button>
                             <button type="button" class="external-event btn btn-primary" onclick="addAppointment()"><i
                                     class="fa fa-plus"></i>
                                 New Appointments</button>
                         </div>
                 </div>
             </div>
         </div>
         <div class="box-header with-border">
             <div class="col-xs-9 col-md-3">
                 <div class="input-group date">
                     <div class="input-group-addon">
                         <i class="fa fa-calendar"></i>
                     </div>
                     <input type="text" class="form-control pull-right" id="datepickerxx"
                         placeholder="Go To Date ...">
                 </div>
             </div>
             <div class="col-xs-2 col-md-2">
                 <button type="button" class="external-event btn btn-info" id="dateBtn"> Go...</button>
             </div>
             <div class="hidden-md hidden-lg col-xs-12">
                 <hr>
             </div>
          
         </div>
         <div class="box-body">
             <!-- THE CALENDAR -->
             <div id="calendar"></div>
         </div>
     </div>
     <!-- /.box-body -->
 </div>
 <!-- /. box -->
</div>
<div class="scrollup hidden-xs">
 <div class="box bg-purple">
     <div class="box-body">
         <!-- the events -->
         <div id="external-events">
             <div class="external-event bg-red">Book</div>
             <div class="external-event bg-aqua">Confirmed</div>
             <div class="external-event bg-green">Complete</div>
             <div class="external-event bg-yellow">Cancelled</div>
             <div class="external-event bg-navy">No Show</div>
         </div>
     </div>
     <!-- /.box-body -->
 </div>
</div>
<!-- /.col -->
@include('appointment.action.appointment')
@include('appointment.action.action')
@include('appointment.action.status')
@include('customers.form')
@endsection
@section('script')
<script type = "text/javascript" >
var table, save_method;
$('#start').datetimepicker({
 format: 'HH:mm'
});
$('#datepickerxx').datepicker({
 format: 'yyyy-mm-dd',
 autoclose: true,
 todayHighlight: true
});
$('#datepicker, #join_date, #point_date').datepicker({
 format: 'dd/mm/yyyy',
 autoclose: true,
 todayHighlight: true
});
$(function () {
 $('#modal-appointment form').validator().on('submit', function (e) {
     if (!e.isDefaultPrevented()) {
         var id = $('#id').val();
         if (save_method == "add") url = "{{ route('appointment.store') }}";
         else url = "appointment/" + id;
         $.ajax({
             url: "{{ route('appointment.store') }}",
             type: "POST",
             data: $('#modal-appointment form').serialize(),
             success: function (data) {
                 swal({
                         title: "Successfully Create Appointment",
                         icon: "success",
                     })
                     .then((go) => {
                         $('#modal-appointment').modal('hide');
                         $('#outlet').val('');
                         $('#status').val('');
                         location.reload();
                     });
             },
             error: function (xhr) {
                 swal({
                     title: xhr.responseJSON,
                     icon: "error",
                 })
             }
         });
         return false;
     }
 });
 $('#modal-customers form').validator().on('submit', function (e) {
     if (!e.isDefaultPrevented()) {
         $.ajax({
             url: "{{ route('customers.store') }}",
             type: "POST",
             data: $('#modal-customers form').serialize(),
             success: function (data) {
                 if (data.status == false) {
                     swal({
                         title: "Your Email or Mobile Phone has already been used, Please Check Message Error!!!",
                         icon: "warning",
                     })
                     $("#msg_email").html(data.message.email).show();
                     $("#msg_phone").html(data.message.phone).show();
                 } else {
                     $('#gender').val('').trigger('change');
                     $('#religion').val('').trigger('change');
                     $('#status').val('').trigger('change');
                     $('#country').val('').trigger('change');
                     $('#outlet').val('').trigger('change');
                     $('#city').val('').trigger('change');
                     $('#consultant').val('').trigger('change');
                     $('#modal-customers').modal('hide');
                     $("#msg_email").html('');
                     $("#msg_phone").html('');
                     swal({
                             title: "Successfully Create Customer",
                             icon: "success",
                         })
                         .then((go) => {
                             $('#modal-customers').modal('hide');
                             location.reload();
                         });
                 }
             },
             error: function () {
                 alert("Can not Save the Data!");
             }
         });
         return false;
     }
 });
 $('#modal-status form').validator().on('submit', function (e) {
     if (!e.isDefaultPrevented()) {
         var id_status = $('#id_status').val();
         $.ajax({
             url: "by-room/" + id_status + "/status",
             type: "POST",
             data: $('#modal-status form').serialize(),
             success: function (data) {
                 swal({
                         title: "Successfully Change Status",
                         icon: "success",
                     })
                     .then((go) => {
                         $('#modal-status').modal('hide');
                         location.reload();
                     });
             },
             error: function () {
                 alert("Can not Save the Data!");
             }
         });
         return false;
     }
 });
});
$(document).ready(function () {
 $('#customer').keyup($.debounce(300,function () {
     var query = $(this).val();
     if (query != '') {
         var _token = $('input[name="_token"]').val();
         $.ajax({
             url: "{{ route('customer.keyword') }}",
             method: "POST",
             data: {
                 query: query,
                 _token: _token
             },
             dataType: "json",
             success: function (data) {
                 $('#suggesstion-box').fadeIn();
                 $('#suggesstion-box').html(data);
             }
         });
     } else {
         var _token = $('input[name="_token"]').val();
         $.ajax({
             url: "{{ route('customer.all') }}",
             method: "POST",
             data: {
                 query: query,
                 _token: _token
             },
             dataType: "json",
             success: function (data) {
                 $('#suggesstion-box').fadeIn();
                 $('#suggesstion-box').html(data);
             }
         });
     }
 }));
});
document.addEventListener('DOMContentLoaded', function () {
 var calendarEl = document.getElementById('calendar');
 var date = $('#datepickerxx').val();
 if (!date) {
     date = Date.now();
 }
 var initialLocaleCode = 'id';
 var calendar = new FullCalendar.Calendar(calendarEl, {
     schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
     plugins: ['interaction', 'dayGrid', 'timeGrid', 'resourceTimeGrid', 'list'],
     defaultView: 'resourceTimeGrid',
     locale: 'id',
     timeZone: 'local',
     defaultDate: date,
     minTime: '06:00',
     maxTime: '23:00',
     contentHeight: 'auto',
     navLinks: false, // can click day/week names to navigate views
     eventLimit: true,
     editable: true,
     selectable: true,
     allDaySlot: false,
     
     titleFormat: { // will produce something like "Tuesday, September 18, 2018"
         month: 'numeric',
         year: 'numeric',
         day: 'numeric',
         weekday: 'long'
     },
     
     slotLabelFormat: [{
             month: 'long',
             year: 'numeric'
         }, // top level of text
         {
             hour: 'numeric',
             minute: '2-digit',
             meridiem: false,
             hour12: false
         }
       
     ],
     header: {
         right: 'prev,resourceTimeGrid,timeGridWeek,dayGridMonth,listDay,next',
         center: 'title',
         left: ''
     },
      views: {
         listDay: {
             buttonText: 'List'
         },
         resourceTimeGrid: {
             buttonText: 'Today'
         },
         timeGridWeek:{
          buttonText: 'Week'
         },
         timeGridDay: {
             buttonText: 'Day'
         },
         dayGridMonth: {
             buttonText: 'Month'
         },
         today: {
             buttonText: 'This Day'
         },
     },
     resources: [
         @foreach($room as $task) {
             id: '{{ $task->id_room }}',
             title: '{{ $task->room_name }}',
         },
         @endforeach
     ],
     eventRender: function (event) {
         var tooltip = event.event.extendedProps.description;
         
         // var name = event.event.title;
         // $(event.el).attr("title",  name +' : '+ tooltip)
         // $(event.el).attr("data-toggle", 'popover')
         // $(event.el).attr("title", event.event.title)
         // $(event.el).attr("data-content", tooltip)
         // $(event.el).popover({
         //     container: "body",
         //     placement: 'top',
         // })
         $(event.el).attr("title", tooltip)
         $(event.el).tooltip({
             container: "body",
             trigger: 'hover',
             placement: 'top',
         })
            
        $(event.el).find(".fc-title").append("<div class='fc-des'>"+event.event.extendedProps.descriptions)+"</div>";
        $(event.el).find(".fc-time").css('display','none');
     },
     // dayRender: function (info) {
     //     console.log(info.date.getHours());
     //     if (between(info.date.getHours(), '11', '12')) {
     //         info.el.bgColor = "grey";
     //     }
     // },
     events: [
         @foreach($appointment as $task) {
             @php
             if ($task->treatment_id) {
                 $product = $task->product_treatment->product_treatment_name;
             } else {
                 $product = 'Consultasi';
             }
             @endphp
             @if($task->customer)
             id: '{{ $task->customer_id }}',
                 id_appointment: '{{ $task->id_appointments }}',
                 resourceId: '{{ $task->room_id }}',
                 descriptions:'{{$product}}',
                 description: '{{ $product }} ({{ $task->start_time }} - {{ $task->end_time }}) ',
                 title: '{{ $task->customer->full_name }}',
                 start: '{{ DateTime::createFromFormat("d/m/Y", $task->come_date)->format("Y-m-d") }} {{ $task->start_time }}.000',
                 end: '{{ DateTime::createFromFormat("d/m/Y", $task->come_date)->format("Y-m-d") }} {{ $task->end_time }}.000',
                 @if($task->status_appointment == 1)
             backgroundColor: '#dd4b39',
                 @elseif($task->status_appointment == 2)
             backgroundColor: '#00c0ef',
                 @elseif($task->status_appointment == 3)
             backgroundColor: '#00a65a',
                 @elseif($task->status_appointment == 4)
             backgroundColor: '#f39c12',
                 @elseif($task->status_appointment == 5)
             backgroundColor: '#001f3f',
                 @endif
             @endif
         },
         @endforeach

     ],
   
     // ==============================================================================
     eventClick: function (calEvent) {
         console.log(calEvent);
         let id_customer = calEvent.event._def.publicId;
         let id_appointment = calEvent.event._def.extendedProps.id_appointment;
         let date = moment(calEvent.event.start).format('YYYY-MM-DD');
         EventClick(id_customer, id_appointment, date);
         // $(calEvent.jsEvent.originalTarget).dblclick(function() {
         //     $(calEvent.el).popover('hide')
         // });
     },
     dateClick: function (dateClick) {
         let title = 'Appointment';
         let id = 0;
         let time = 0;
         if (dateClick.resource && dateClick.resource) {
             title = dateClick.resource.title;
             id = dateClick.resource.id;
             let hours = dateClick.date.getHours();
             let minutes = dateClick.date.getMinutes();
             time = hours + ':' + minutes;
         }
         let before = dateClick.date;
         let date = formatDate(before);
         DateClickRoom(title, id, time, date);
     },
     // ==============================================================================
     //coming soon
     eventResize: function (resize) {
         if (swal("Sorry!!", "this feature is not yet available")) {
             resize.revert();
         }
     },
     eventDrop: function (dragDrop) {
         if (swal("Sorry!!", "this feature is not yet available")) {
             dragDrop.revert();
         }
     },
     // select : function (selectBlock) {
     //   swal("Sorry!!", "this feature is not yet available")
     // },
     selectMirror: function (hanyaPelengkap) {
         alert('hanya pelengkap')
     }
 });
 calendar.render();
 $("#dateBtn").click(function() {
     var date = calendar.gotoDate( $("#datepickerxx").val() )
     console.log(date);
 });

 $(".fc-center").addClass('col-md-12 text-center');
 $(".fc-left").addClass('col-md-4');


});
</script>
<script src='{{ asset('calendar/core/main.min.js') }}'></script>
<script src='{{ asset('calendar/core/locales-all.min.js') }}'></script>
<script src='{{ asset('calendar/interaction/main.min.js') }}'></script>
<script src='{{ asset('calendar/daygrid/main.min.js') }}'></script>
<script src='{{ asset('calendar/timegrid/main.min.js') }}'></script>
<script src='{{ asset('calendar/timeline/main.min.js') }}'></script>
<script src='{{ asset('calendar/resource-common/main.min.js') }}'></script>
<script src='{{ asset('calendar/resource-daygrid/main.min.js') }}'></script>
<script src='{{ asset('calendar/resource-timegrid/main.min.js') }}'></script>
<script src='{{ asset('calendar/resource-timeline/main.min.js') }}'></script>
<script src="{{ asset('js/room.js') }}"></script>
{{-- <script src="https://fullcalendar.io/releases/list/4.1.0/main.min.js"></script> --}}
@endsection
 
 
 


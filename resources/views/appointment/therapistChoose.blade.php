@extends('base')

@section('title')
Appointment
@endsection

@section('style')
<link href='{{ asset('calendar/core/main.min.css') }}' rel='stylesheet' />
<link href='{{ asset('calendar/timegrid/main.min.css') }}' rel='stylesheet' />
<link href='{{ asset('calendar/timeline/main.min.css') }}' rel='stylesheet' />
<link href='{{ asset('calendar/resource-timeline/main.min.css') }}' rel='stylesheet' />
<link href='{{ asset('css/headerCalendar.css') }}' rel='stylesheet' />
{{-- <link rel="stylesheet" href="https://fullcalendar.io/releases/list/4.1.0/main.min.css"> --}}
@endsection

@section('breadcrumb')
@parent
<li>Appointment</li>
@endsection

@section('content')

<div class="row">
    <!-- /.col -->
    <div class="col-xl-10 col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <div class="col-xs-9 col-md-4">
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="datepickerxx">
                    </div>
                </div>
                <div class="col-xs-2 col-md-1">
                    <button type="button" class="external-event btn btn-info" id="dateBtn"> Go...</button>
                </div>
                <div class="hidden-md hidden-lg col-xs-12">
                    <hr>
                </div>
                <div class="col-md-7">
                    <div class="col-md-6">
                        <button type="button" class="external-event btn btn-primary btn-sm"
                            onclick="addAppointment()"><i class="fa fa-plus"></i> New Appointments</button>
                        <a href="{{ url('by-therapist') }}">
                            <button type="button" class="external-event btn btn-danger btn-sm"><i class="fa fa-eye"></i>
                                All
                                Therapist</button>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <label for="">Filter Therapist</label>
                        <select id="therapist_choose" name="therapist"
                            class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1"
                            aria-hidden="true" required>
                            <option value=""></option>
                            @foreach ($therapist as $list)
                            <option value="{{ $list->id_therapist }}">{{ $list->therapist_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="box-body no-padding">
                <!-- THE CALENDAR -->
                <div id="calendar"></div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /. box -->
    </div>
</div>
<div class="scrollup hidden-xs">
    <div class="box bg-purple">
        <div class="box-body">
            <!-- the events -->
            <div id="external-events">
                <div class="external-event bg-red">Book</div>
                <div class="external-event bg-aqua">Confirmed</div>
                <div class="external-event bg-green">Complete</div>
                <div class="external-event bg-yellow">Cancelled</div>
                <div class="external-event bg-navy">No Show</div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<!-- /.col -->

@include('appointment.action.appointment')
@include('appointment.action.action')
@include('appointment.action.status')
@endsection


@section('script')
<script type = "text/javascript" >
var table, save_method;
$('#therapist').val({{ $therapistChoose->id_therapist }}).change();
$('#start').datetimepicker({
    format: 'HH:mm'
});
$('#datepickerxx').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    todayHighlight: true
});

$('#datepicker, #join_date').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    todayHighlight: true
});

$(function () {
    chooseTherapist();
    $('#modal-appointment form').validator().on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            var id = $('#id').val();
            if (save_method == "add") url = "{{ route('appointment.store') }}";
            else url = "appointment/" + id;

            $.ajax({
                url: "{{ route('appointment.store') }}",
                type: "POST",
                data: $('#modal-appointment form').serialize(),
                success: function (data) {
                    swal({
                            title: "Successfully Create Appointment",
                            icon: "success",
                        })
                        .then((go) => {
                            $('#modal-appointment').modal('hide');
                            $('#outlet').val('');
                            $('#status').val('');
                            location.reload();
                        });
                },
                error: function () {
                    alert("Can not Save the Data!");
                }
            });
            return false;
        }
    });

    $('#modal-status form').validator().on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            var id_status = $('#id_status').val();

            $.ajax({
                url: "/by-room/" + id_status + "/status",
                type: "POST",
                data: $('#modal-status form').serialize(),
                success: function (data) {
                    swal({
                            title: "Successfully Change Status",
                            icon: "success",
                        })
                        .then((go) => {
                            $('#modal-status').modal('hide');
                            location.reload();
                        });
                },
                error: function () {
                    alert("Can not Save the Data!");
                }
            });
            return false;
        }
    });
});

$(document).ready(function () {
    $('#customer').keyup($.debounce(300, function () {
        var query = $(this).val();
        if (query != '') {
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: "{{ route('customer.keyword') }}",
                method: "POST",
                data: {
                    query: query,
                    _token: _token
                },
                dataType: "json",
                success: function (data) {
                    $('#suggesstion-box').fadeIn();
                    $('#suggesstion-box').html(data);
                }
            });
        } else {
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: "{{ route('customer.all') }}",
                method: "POST",
                data: {
                    query: query,
                    _token: _token
                },
                dataType: "json",
                success: function (data) {
                    $('#suggesstion-box').fadeIn();
                    $('#suggesstion-box').html(data);
                }
            });
        }
    }));
});

document.addEventListener('DOMContentLoaded', function () {
    var calendarEl = document.getElementById('calendar');

    var date = $('#datepickerxx').val();
    if (!date) {
        date = Date.now();
    }

    var initialLocaleCode = 'id';
    var calendar = new FullCalendar.Calendar(calendarEl, {
        schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
        plugins: ['interaction', 'dayGrid', 'timeGrid', 'resourceTimeGrid', 'list'],
        defaultView: 'resourceTimeGrid',
        timeZone: 'local',
        defaultDate: date,
        minTime: '06:00',
        maxTime: '24:00',
        navLinks: true, // can click day/week names to navigate views
        eventLimit: true,
        editable: true,
        selectable: true,
        contentHeight: 'auto',

        titleFormat: { // will produce something like "Tuesday, September 18, 2018"
            month: 'numeric',
            year: 'numeric',
            day: 'numeric',
        },

        slotLabelFormat: [{
                month: 'long',
                year: 'numeric'
            }, // top level of text
            {
                hour: 'numeric',
                minute: '2-digit'
            }
        ],

        header: {
            right: 'prev,resourceTimeGrid,timeGridDay,dayGridMonth,listDay,today,next',
            left: '',
            center: 'title',
        },

        views: {
            listDay: {
                buttonText: 'List'
            },
            resourceTimeGrid: {
                buttonText: 'Therapist'
            },
            timeGridDay: {
                buttonText: 'Day'
            },
            dayGridMonth: {
                buttonText: 'Month'
            },
            today: {
                buttonText: 'Today'
            },
        },

        resources: [{
            id: '{{ $therapistChoose->id_therapist }}',
            title: '{{ $therapistChoose->therapist_name }}',
        }, ],

        eventRender: function (event) {
            var tooltip = event.event.extendedProps.description;
            // var name = event.event.title;
            // $(event.el).attr("title",  name +' : '+ tooltip)
            // $(event.el).attr("data-toggle", 'popover')
            // $(event.el).attr("title", event.event.title)
            // $(event.el).attr("data-content", tooltip)
            // $(event.el).popover({
            //     container: "body",
            //     placement: 'top',
            // })
            $(event.el).attr("title", tooltip)
            $(event.el).tooltip({
                container: "body",
                trigger: 'hover',
                placement: 'top',
            })
        },

        events: [
            @foreach($appointment as $task) {
                @php
                //dd($task);
                if ($task->treatment_id) {
                    $product = $task->product_treatment->product_treatment_name;
                } else {
                    $product = 'Consultasi';
                }
                @endphp

                id: '{{ $task->customer_id }}',
                id_appointment: '{{ $task->id_appointments }}',
                resourceId: '{{ $task->therapist_id }}',
                title: '{{ $task->customer->full_name }}',
                description: '{{ $product }} ({{ $task->start_time }} - {{ $task->end_time }}) ',
                start: '{{ DateTime::createFromFormat("d/m/Y", $task->come_date)->format("Y-m-d") }} {{ $task->start_time }}.000',
                end: '{{ DateTime::createFromFormat("d/m/Y", $task->come_date)->format("Y-m-d") }} {{ $task->end_time }}.000',

                @if($task->status_appointment == 1)
                    backgroundColor: '#dd4b39',
                @elseif($task->status_appointment == 2)
                    backgroundColor: '#00c0ef',
                @elseif($task->status_appointment == 3)
                    backgroundColor: '#00a65a',
                @elseif($task->status_appointment == 4)
                    backgroundColor: '#f39c12',
                @elseif($task->status_appointment == 5)
                    backgroundColor: '#001f3f',
                @endif
            },
            @endforeach
        ],


        // ==============================================================================

        eventClick: function (calEvent) {

            let id_customer = calEvent.event._def.publicId;
            let id_appointment = calEvent.event._def.extendedProps.id_appointment;
            let date = moment(calEvent.event.start).format('YYYY-MM-DD');

            // $(calEvent.jsEvent.originalTarget).dblclick(function() {
            //     $(calEvent.el).popover('hide')
            // });
            EventClick(id_customer, id_appointment, date);
        },
        dateClick: function (dateClick) {
            let title = 'Appointment';
            let id = {{ $therapistChoose->id_therapist }};
            let time = 0;

            if (dateClick.resource) {
                title = dateClick.resource.title;
                id = dateClick.resource.id;
                let hours = dateClick.date.getHours();
                let minutes = dateClick.date.getMinutes();
                time = hours + ':' + minutes;
            }

            let before = dateClick.date;
            let date = formatDate(before);

            DateClick(title, id, time, date);
        },

        // ==============================================================================
        //coming soon
        eventResize: function (resize) {
            if (swal("Sorry!!", "this feature is not yet available")) {
                resize.revert();
            }
        },
        eventDrop: function (dragDrop) {
            if (swal("Sorry!!", "this feature is not yet available")) {
                dragDrop.revert();
            }
        },
        // select : function (selectBlock) {
        //   swal("Sorry!!", "this feature is not yet available")
        // },
        selectMirror: function (hanyaPelengkap) {
            alert('hanya pelengkap')
        }
    });
    calendar.render();
    
    $("#dateBtn").click(function() {
        var date = calendar.gotoDate( $("#datepickerxx").val() )
        console.log(date);
    });
});

function chooseTherapist() {
    $('#therapist_choose').change(function () {
        window.location.href = "/by-therapist/" + $(this).val();
    });
}

</script>

<script src='{{ asset('calendar/core/main.min.js') }}'></script>
<script src='{{ asset('calendar/interaction/main.min.js') }}'></script>
<script src='{{ asset('calendar/daygrid/main.min.js') }}'></script>
<script src='{{ asset('calendar/timegrid/main.min.js') }}'></script>
<script src='{{ asset('calendar/timeline/main.min.js') }}'></script>
<script src='{{ asset('calendar/resource-common/main.min.js') }}'></script>
<script src='{{ asset('calendar/resource-daygrid/main.min.js') }}'></script>
<script src='{{ asset('calendar/resource-timegrid/main.min.js') }}'></script>
<script src='{{ asset('calendar/resource-timeline/main.min.js') }}'></script>
<script src="{{ asset('js/room.js') }}"></script>
{{-- <script src="https://fullcalendar.io/releases/list/4.1.0/main.min.js"></script> --}}
@endsection

<div class="modal" id="modal-reschedule" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
                <form data-toggle="validator" method="post">
                @csrf()
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true"> &times;
                        </span> </button>
                    <h3 class="modal-titles"></h3>

                    <div class="row">
                                    <div class="form-group">
                                        <label for="fullname" class="col-xs-1 control-label">Name </label>
                                        <div class="col-xs-6">
                                            : <span id="fulln"> </span>
                                        </div>
                                    </div>
                                </div>
                </div>
                <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                    <input type="hidden" name="customer_id" id="customer_id_new">
                    <input type="hidden" name="outlet" id="outlet_new">
                    <input type="hidden" name="activity" id="activity_id_new">
                    <input type="hidden" name="status" id="status_new">
                    <input type="hidden" name="pack_name" id="pack_name_new">

                    <div class="form-group col-sm-12 col-md-6" id="group_therapist">
                    
                    <select id="therapist_new" name="therapist[]" multiple=""
                        data-placeholder="Select a Therapist" style="width: 100%;" tabindex="-1"
                        aria-hidden="true" required>
                        <option value=""></option>
                        @foreach ($therapist as $list)
                        <option value="{{ $list->id_therapist }}">{{ $list->therapist_name }}</option>
                        @endforeach
                    </select>
                    </div>
                            <div class="form-group">
                                <label>Date:</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="point_date_new" name="point_date" required>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                                <div class="form-group">
                                    <label>Start Time:</label>
                                    <div class="input-group">
                                        <input type="text" name="start" id="start_new" class="form-control timepicker"
                                            required>
                                        <div class="input-group-addon">
                                            <i class="fa fa-clock-o"></i>
                                        </div>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                </div>

                </div>
            
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                            class="fa fa-arrow-circle-left"></i>
                        Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="modal-status" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <form data-toggle="validator" method="post">
        {{ csrf_field() }} {{ method_field('POST') }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> &times; </span> </button>
                <h3 class="modal-title-status"></h3>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id_status" id="id_status">
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <label>Status</label>
                            <select id="status" name="status" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                <option value=""></option>
                                <option value="1">Book</option>
                                <option value="2">Confirmed</option>
                                <option value="3">Complete</option>
                                <option value="4">Cancelled</option>
                                <option value="5">No Show</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
            </div>
        </form>
      </div>
    </div>
  </div>
       
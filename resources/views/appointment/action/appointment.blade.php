<div class="modal" id="modal-appointment" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form data-toggle="validator" method="post">
                {{ csrf_field() }} {{ method_field('POST') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true"> &times;
                        </span> </button>
                    <h3 class="modal-title"></h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="customer">Customer *</label>
                                <input type="text" class="form-control" id="customer" name="customer" autofocus="on"
                                    autocomplete="off" required>
                                <input type="hidden" name="customer_id" id="customer_id">
                                <div id="suggesstion-box"></div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="box box-primary">
                                <div class="row">
                                    <div class="form-group">
                                        <label for="fullname" class="col-xs-4 control-label">Full Name </label>
                                        <div class="col-xs-8">
                                            : <span id="full"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="fullname" class="col-xs-4 control-label">Contact No. </label>
                                        <div class="col-xs-8">
                                            : <span id="contact"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="fullname" class="col-xs-4 control-label">Date of Birth </label>
                                        <div class="col-xs-8">
                                            : <span id="birthDate"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Outlet</label>
                                <select id="outlet" name="outlet" class="form-control select2 select2-hidden-accessible"
                                    style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    <option selected="selected" value="">-- Select Outlet --</option>
                                    @foreach ($outlet as $list)
                                    <option value="{{ $list->id_outlet }}">{{ $list->outlet_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Marketing Activity</label>
                                <select id="activity" name="activity"
                                    class="form-control select2 select2-hidden-accessible" style="width: 100%;"
                                    tabindex="-1" aria-hidden="true">
                                    <option value=""></option>
                                    @foreach ($activity as $list)
                                    <option value="{{ $list->id_activity }}">{{ $list->activity_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label>Date:</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="point_date" name="point_date" required>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label>Status</label>
                                <select id="status" name="status" class="form-control select2 select2-hidden-accessible"
                                    style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    <option selected="selected" value="">-- Select Status --</option>
                                    <option value=""></option>
                                    <option value="1">Book</option>
                                    <option value="2">Confirmed</option>
                                    <option value="3">Complete</option>
                                    <option value="4">Cancelled</option>
                                    <option value="5">No Show</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label>Room</label>
                                <select id="id_rooms" name="id_rooms"
                                    class="form-control select2 select2-hidden-accessible" style="width: 100%;"
                                    tabindex="-1" aria-hidden="true">
                                    <option value=""></option>
                                    @foreach ($room as $item)
                                    <option value="{{ $item->id_room }}">{{ $item->room_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <label for="activity" class="col-xs-1 control-label">Treatment</label>
                        <div class="col-md-12 col-xs-12">
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Start Time:</label>
                                    <div class="input-group">
                                        <input type="text" name="start" id="start" class="form-control timepicker"
                                            required>
                                        <div class="input-group-addon">
                                            <i class="fa fa-clock-o"></i>
                                        </div>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Treatment</label>
                                    <select id="pack_name" onchange="chooseTreatment();" name="pack_name"
                                        class="form-control select2 select2-hidden-accessible" style="width: 100%;"
                                        tabindex="-1" aria-hidden="true" placeholder="type package here ... "
                                        autocomplete="off">
                                        <option value=""></option>
                                        @foreach ($productTreatment as $list)
                                        <option value="{{ $list->id_product_treatment }}">
                                            {{ $list->product_treatment_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <label>Therapist</label>
                                    <select id="therapist" name="therapist[]"
                                        class="form-control select2 select2-hidden-accessible" multiple=""
                                        data-placeholder="Select a Therapist" style="width: 100%;" tabindex="-1"
                                        aria-hidden="true" required>
                                        @foreach ($therapist as $list)
                                        <option value="{{ $list->id_therapist }}">{{ $list->therapist_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div id="siapOke" class="hidden text-center">
                                <h2>Consultasi</h2>
                            </div>
                            <div class="box box-primary" id="siap">
                                <div class="row">
                                    <div class="form-group">
                                        <label for="fullname" class="col-xs-4 control-label">Start </label>
                                        <div class="col-xs-8">
                                            : <span id="start_time"></span>
                                            <input type="hidden" id="start_input" name="start_input">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="fullname" class="col-xs-4 control-label">End </label>
                                        <div class="col-xs-8">
                                            : <span id="end"></span>
                                            <input type="hidden" id="end_time" name="end_time">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="fullname" class="col-xs-4 control-label">Code </label>
                                        <div class="col-xs-8">
                                            : <span id="code"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="fullname" class="col-xs-4 control-label">Name </label>
                                        <div class="col-xs-8">
                                            : <span id="name"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="fullname" class="col-xs-4 control-label">Duration </label>
                                        <div class="col-xs-8">
                                            : <span id="duration"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Remarks</label>
                                <textarea name="remarks" id="remarks" class="form-control" rows="3"
                                    placeholder="Enter ..."></textarea>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                            class="fa fa-arrow-circle-left"></i>
                        Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

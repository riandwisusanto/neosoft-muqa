<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Antrian</title>
    <style>
        * {
            font-size: 10pt;
            color: #000000;
            font-family: "Trebuchet MS", "Lucida Grande", "Lucida Sans Unicode", "Lucida Sans", Tahoma, sans-serif;
        }

        @page {
            margin: 0.1cm;
        }

        ul {
            margin: 0 -20;
            list-style: none;
        }

        ul li:before {
            content: "•";
            font-size: 1em;
            /* or whatever */
            padding-right: 5px;
        }


        .normal-header-td {
            border-bottom: 1px solid black;
        }

        .last-header-td {
            border-bottom: 1px solid black;
        }

        .block_center {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }

        .col-0_5,
        .col-1,
        .col-1_5,
        .col-2,
        .col-3,
        .col-4,
        .col-5,
        .col-6,
        .col-7,
        .col-8,
        .col-9,
        .col-10,
        .row {
            float: left;
            position: relative;
            min-height: 1px;
        }

        .col-0_5 {
            width: 5%;
        }

        .col-1 {
            width: 10%;
        }

        .col-1_5 {
            width: 15%;
        }

        .col-2 {
            width: 20%;
        }

        .col-3 {
            width: 30%;
        }

        .col-4 {
            width: 40%;
        }

        .col-5 {
            width: 50%;
        }

        .col-6 {
            width: 60%;
        }

        .col-7 {
            width: 70%;
        }

        .col-8 {
            width: 80%;
        }

        .col-9 {
            width: 90%;
        }

        .col-10 {
            width: 100%;
        }

        .floatRight {
            float: right;
        }

        .indentConditionParagraph {
            margin-left: 5px;
            width: 100%;
        }

        .indentConditions {
            float: left;
            width: 100%;
            margin-left: 15px;
        }

        .indentConditions-numbering {
            float: left;
            font-size: 9pt;
            vertical-align: top;
        }

        .indentConditions-bullets:before {
            content: "•";
            font-size: 1em;
            /* or whatever */
            padding-right: 5px;
        }

        .indentConditions-nestedConditions {
            float: left;
            width: 95%;
            margin-left: 5px;
        }

        .invoiceDetail-tbl {
            width: 100%;
            border: 1px solid black;
            min-height: 10em;
        }

        .half {
            min-height: 5em !important;
        }

        .invoiceDetail-tbl td {
            vertical-align: top;
        }

        .invoicePayment-tbl {
            width: 100%;
        }

        tr.lastRow {
            height: 30%;
        }

        .leftHeader {
            white-space: nowrap;
            vertical-align: top;
            width: 8%;
        }

        .leftHeaderData {
            width: 42%;
            padding-left: 10px;
        }

        .rightHeader {
            white-space: nowrap;
            vertical-align: top;
            width: 8%;
        }

        .mt-3 {
            margin-top: 1rem;
        }

        .mt-2 {
            margin-top: 0.5rem;
        }

        .rightHeaderData {
            width: 42%;
            padding-left: 10px;
        }

        .row {
            width: 100%;
        }

        .signature-title {
            height: 3em;
            font-weight: bold;
            font-size: 12px;
        }

        .signature-underline {
            height: 2em;
            font-weight: bold;
            font-size: 11px;
            border-bottom: black 2px solid;
        }

        .termsConditions {
            font-size: 10px;
            float: left;
            width: 100%;
        }

        .termsConditions-grandTitle {
            font-size: 9px;
            font-weight: bold;
            margin-top: 3px;
        }

        .termsConditions-subTitle {
            font-weight: bold;
            margin-top: 3px;
            font-size: 8px;
        }

        .termsConditions>h3 {
            font-size: 9px;
            margin: 0;
        }

        .termsConditions>ul>li,
        .termsConditions>div>ul>li,
        .indentConditions-nestedConditions>ul>li,
        .smallFont {
            font-size: 7pt;
        }

        .textRightAlign {
            text-align: right;
        }

        .pageBreakFooter {
            page-break-after: always;
        }

        .itembalance {
            font-size: 8px;
        }

        .itembalance-title {
            font-weight: bold;
        }

        .customHeight {
            height: 5px;
        }

        hr {
            border: black thin solid;
        }

        .taxInvoiceTextHeader {
            font-size: 14pt;
            font-weight: bold;
        }

        .taxInvoiceNumber {
            font-size: 12pt;
        }

        #termsId {
            width: 95%;
            position: absolute;
            bottom: 10;
        }

        thead {
            display: table-header-group;
        }

        tfoot {
            display: table-footer-group;
        }
    </style>
</head>
<body>
    <table id="mainTbl" style="width: 100mm;" border="0" align="center">
        <thead>
            <tr>
                <td>
                    <table width="100%">
                        <tbody>
                            <tr>
                                <td valign="top" width="100%">
                                    <span class="block_center" style="text-align:center;">
                                        <span class="taxInvoiceTextHeader">{{ $queue->outlet->outlet_code}} - {{$queue->outlet->outlet_name}}</span><br />
                                        <span class="taxInvoiceNumber">{{ $queue->created_at->format('d M Y H:i:s')}}</span><br />
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </thead>
        <br>
        <tbody>
            <tr>
                <td>
                    <table width="100%">
                        <tbody>
                            <tr>
                                <td valign="top" width="100%">
                                    <span class="block_center" style="text-align:center;margin-top:40px;">
                                        <span style="font-size: 22pt;font-weight: bold;">{{$queue->queue_number}}</span><br />
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
        <tfoot>
        <tr>
                <td>
                    <table width="100%">
                        <tbody>
                            <tr>
                                <td valign="top" width="100%">
                                    <span class="block_center" style="text-align:center;margin-top:40px;">
                                        <span class="taxInvoiceNumber">{{ $queue->customer->full_name}}</span><br />
                                        <span class="taxInvoiceNumber">({{ $queue->customer->induk_customer}})</span><br />
                                        <span class="taxInvoiceNumber">{{ $queue->customer->nick_name}} - {{ $year }} tahun - {{ $queue->customer->gender }}</span><br />
                                        <span class="taxInvoiceNumber">{{ $queue->customer->address}}</span><br />
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tfoot>
    </table>

    <script>
        window.print()
    </script>
 
</body>

</html>

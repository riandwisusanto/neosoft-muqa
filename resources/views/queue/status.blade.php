<div class="modal" id="modal-status" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <form data-toggle="validator" method="post">
        {{ csrf_field() }} {{ method_field('POST') }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> &times; </span> </button>
                <h3 class="modal-title-status">Status Queue</h3>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" id="id_status">
                <input type="hidden" name="therapist_id" id="id_therapist">

                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <label>Payment Status</label>
                            <select id="status_payment" name="status_payment" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                <option value=""></option>
                                <option value="1">PAY</option>
                                <option value="2">PAID</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Check Status</label>
                            <select id="status_queue" name="status_queue" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                <option value=""></option>
                                <option value="0">Waiting</option>
                                <option value="1">On</option>
                                <option value="2">Done</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Cancel</button>
            </div>
        </form>
      </div>
    </div>
  </div>
       
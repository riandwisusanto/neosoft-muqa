<div class="modal" id="modal-queue" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form data-toggle="validator" method="post">
                {{ csrf_field() }} {{ method_field('POST') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true"> &times;
                        </span> </button>
                    <h3 class="modal-title">New Queue</h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <input type="hidden" id="idx" name="id">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label for="customer">Customer *</label>
                                <input type="text" class="form-control" id="customer" name="customer" autofocus="on"
                                    autocomplete="off" required>
                                <input type="hidden" name="customer_id" id="customer_id">
                                <div id="suggesstion-box"></div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="box box-primary">
                                <div class="row">
                                    <div class="form-group">
                                        <label for="fullname" class="col-xs-4 control-label">Full Name </label>
                                        <div class="col-xs-8">
                                            : <span id="full"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="fullname" class="col-xs-4 control-label">Contact No. </label>
                                        <div class="col-xs-8">
                                            : <span id="contact"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="fullname" class="col-xs-4 control-label">Date of Birth </label>
                                        <div class="col-xs-8">
                                            : <span id="birthDate"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Outlet</label>
                                <select id="outlet" name="outlet" class="form-control select2 select2-hidden-accessible"
                                    style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                    <option selected="selected" value="">-- Select Outlet --</option>
                                    @foreach ($outlet as $list)
                                    <option value="{{ $list->id_outlet }}">{{ $list->outlet_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Date:</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="come_date" name="come_date" required>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Save </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                            class="fa fa-arrow-circle-left"></i>
                        Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

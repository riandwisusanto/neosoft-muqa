@extends('base')
@section('style')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/buttons/css/buttons.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/buttons/css/buttons.bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/DataTables/FixedColumns/css/fixedColumns.bootstrap.min.css') }}">
<style>
    div.dataTables_wrapper div.dataTables_processing {
        position: relative;
        border: 0;
    }
</style>
@endsection
<script type="text/javascript" src="{{ asset('js/chart.min.js') }}"></script>
<script type="text/javascript">
  var collection = {!! $data !!};
  var collection_month = {!! @$data_month !!};
  var collection_today = {!! @$data_today !!};

  var treatment = {!! $data_treatment !!};
  var product = {!! $data_product !!};
  var customer = {!! $data_customer !!};

  google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawVisualization);

    function drawVisualization() {
      // Some raw data (not necessarily accurate)
      var data_product = google.visualization.arrayToDataTable(product);
      var options_product = {
        title : 'Product Revenue by {{ $outlet_name }}',
        vAxis: {title: 'Used Value'},
        hAxis: {title: 'Month'},
        seriesType: 'bars',
        legend: { position: "none" },
        bar: {groupWidth: "80%"},
      }

      var data_treatment = google.visualization.arrayToDataTable(treatment);
      var options_treatment = {
        title : 'Treatment Revenue by {{ $outlet_name }}',
        vAxis: {title: 'Used Value'},
        hAxis: {title: 'Month'},
        seriesType: 'bars',
        legend: { position: "none" },
        bar: {groupWidth: "80%"},
      }

      var data_today = google.visualization.arrayToDataTable(collection_today);
      var options_today = {
        title : 'Today`s Collection by {{ $outlet_name }}',
        vAxis: {title: 'Today`s Collection'},
        hAxis: {title: 'Day'},
        seriesType: 'bars',
        bar: {groupWidth: "80%"},
        selectionMode: 'multiple',
        tooltip: {trigger: 'selection'},
        aggregationTarget: 'category',
      }

      var data_month = google.visualization.arrayToDataTable(collection_month);
      var options_month = {
        title : 'A Month`s Collection by {{ $outlet_name }}',
        vAxis: {title: 'A Month`s Collection'},
        hAxis: {title: 'Month'},
        seriesType: 'bars',
        bar: {groupWidth: "80%"},
        selectionMode: 'multiple',
        tooltip: {trigger: 'selection'},
        aggregationTarget: 'category',
      }

      var data = google.visualization.arrayToDataTable(collection);
      var options = {
        title : 'Collection by {{ $outlet_name }}',
        vAxis: {title: 'Collection'},
        hAxis: {title: 'Month'},
        seriesType: 'bars',
        bar: {groupWidth: "80%"},
        selectionMode: 'multiple',
        tooltip: {trigger: 'selection'},
        aggregationTarget: 'category',
      }

    //   var data_customer = google.visualization.arrayToDataTable(customer);
    //   var options_customer = {
    //     title : 'Customer by {{ $outlet_name }}',
    //     vAxis: {title: 'Customer'},
    //     hAxis: {title: 'Month'},
    //     seriesType: 'bars',
    //     bar: {groupWidth: "50%"},
    //   }

      var chart = new google.visualization.ComboChart(document.getElementById('chart_collection'));
      chart.draw(data, options);

      var chart_month = new google.visualization.ComboChart(document.getElementById('chart_collection_month'));
      chart_month.draw(data_month, options_month);

      var chart_today = new google.visualization.ComboChart(document.getElementById('chart_collection_today2'));
      chart_today.draw(data_today, options_today);

      var chart_treatment = new google.visualization.ComboChart(document.getElementById('chart_treatment'));
      chart_treatment.draw(data_treatment, options_treatment);

      var chart_product = new google.visualization.ComboChart(document.getElementById('chart_product'));
      chart_product.draw(data_product, options_product);

    //   var chart_customer = new google.visualization.ComboChart(document.getElementById('chart_customer'));
    //   chart_customer.draw(data_customer, options_customer);

      if (customer.length > 0) {
        var data_customer = google.visualization.arrayToDataTable(customer);
        var options_customer = {
            title : 'Customer by {{ $outlet_name }}',
            vAxis: {title: 'Customer'},
            hAxis: {title: 'Month'},
            seriesType: 'bars',
            bar: {groupWidth: "50%"},
        }

        var chart_customer = new google.visualization.ComboChart(document.getElementById('chart_customer'));
        chart_customer.draw(data_customer, options_customer);
      }
    }

</script>


@section('content')
<div class="row">
    <!-- /.col -->
    <div class="col-xl-10 col-md-12">
        <div class="box box-solid">
           
          </div>
    </div>
</div>
<div class="nav-tabs-custom">
  <div class="box-header with-border">
          <span style="font-size:20px;">Dashboard</span>
          <p style="font-size:15px;"><i>Manajemen laporan (collections, revenue, sales pasien baru, dan sales pasien lama) ditampilkan
dalam bentuk grafik, pelaporan secara real time.</i></p>
  </div>
    <!-- Tabs within a box -->
    <ul class="nav nav-tabs pull-right ui-sortable-handle">
        <li class="active"><a href="#collection" data-toggle="tab" aria-expanded="true">Collection</a></li>
        <li class=""><a href="#collection_month" data-toggle="tab" aria-expanded="true">A Month's Collection</a></li>

        <li class=""><a href="#collection_today2" data-toggle="tab" aria-expanded="true">Today's Collection</a></li>
        <li class=""><a href="#revenue" data-toggle="tab" aria-expanded="true">Revenue</a></li>
        <li class=""><a href="#customer" data-toggle="tab" aria-expanded="true">Customer</a></li>
        <li class=""><a href="#collection_today" data-toggle="tab" aria-expanded="true">Today</a></li>
        <li class="pull-left header">
            <button onclick="periodeForm()" class="btn btn-warning"><i class="fa fa-refresh"></i> Change Periode
            </button>
            @if (count($outlet) > 1)
            <button onclick="allOutlet()" class="btn btn-danger"><i class="fa fa-bar-chart"></i> All Outlet </button>
            @endif
            {{-- <button onclick="allOutlet()" class="btn btn-default"><i class="fa fa-bar-chart"></i> All Outlet </button> --}}
        </li>
    </ul>
    <div class="tab-content no-padding">
        <!-- Morris chart - Sales -->
        <div class="chart tab-pane active" id="collection" style="position: relative; height: 500px;">
            <div id="chart_collection" style="width: auto; height: 500px;"></div>
        </div>
        <div class="chart tab-pane active" id="collection_month" style="position: relative; height: 500px;">
            <div id="chart_collection_month" style="width: auto; height: 500px;"></div>
        </div>
        <div class="chart tab-pane active" id="collection_today2" style="position: relative; height: 500px;">
            <div id="chart_collection_today2" style="width: auto; height: 500px;"></div>
        </div>
        @if (json_decode($data_customer) != [])
            <div class="chart tab-pane active" id="customer" style="position: relative; height: 500px;">
                <div id="chart_customer" style="width: auto; height: 500px;"></div>
            </div>
        @endif
        {{-- <div class="chart tab-pane active" id="customer" style="position: relative; height: 500px;">
            <div id="chart_customer" style="width: auto; height: 500px;"></div>
        </div> --}}
        <div class="chart tab-pane active" id="revenue" style="position: relative; height: 500px;">
            <div class="col-md-6">
                <div id="chart_treatment" style="width: auto; height: 500px;"></div>
            </div>
            <div class="col-md-6">
                <div id="chart_product" style="width: auto; height: 500px;"></div>
            </div>
        </div>
        <div class="chart tab-pane active" id="collection_today" style="position: relative; height: auto;">
            <div class="box-body">
                <h3>Invoice Today</h3>
                <table class="table table-striped table-bordered nowrap table-invoice-detail" style="width:100%">
                    <thead>
                        <tr>
                            <th>Invoice Code</th>
                            <th>Payment Date</th>
                            <th>Outlet</th>
                            <th>Customer Ref</th>
                            <th>Customer</th>
                            <th>Received By</th>
                            <th>Payment Type</th>
                            <th>Amount</th>
                            <th>Paid</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>

    </div>
</div>

@include('periode')
@include('periode_all')
@endsection

@section('script')
<script>
    var table;
  $('#from, #to').datepicker({
    format: "MM yyyy",
    viewMode: "months",
    minViewMode: "months"
  });

  $(function() {
    table = $('.table-invoice-detail').DataTable( {
        processing: true,
        serverside: false,
        scrollY:        "500px",
        scrollX:        true,
        scrollCollapse: true,
        fixedColumns:   {
            leftColumns: 1
        },
        "ajax" : {
          "url" : "{{ route('invoice.dashboard', $outlet_id) }}",
          "type" : "GET"
        }
    });
  });

  function periodeForm(){
   $('#modal-form').modal('show');
  }

  function allOutlet(){
   $('#modal-outlet').modal('show');
  }


</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/buttons/js/dataTables.buttons.min.js') }}"></script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/dataTables.fixedColumns.min.js') }}">
</script>
<script rel="stylesheet" src="{{ asset('plugins/DataTables/FixedColumns/js/fixedColumns.bootstrap.min.js') }}"></script>
@endsection

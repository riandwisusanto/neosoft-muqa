<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$protectedOptions = [
    'prefix'        => 'api/v1',
    'namespace'     => 'Api\v1',
    'middleware'    => ['auth:api']
];

/*
|--------------------------------------------------------------------------
| 10 years challange.
|--------------------------------------------------------------------------
| 2010 = PHP is Bad, 2011 = PHP is Bad ... 2019 = PHP is Bad
| 2020 = But i still get paid to write web apps in PHP
|
*/
Route::group($protectedOptions, function () {
    Route::prefix('logistics')->group(function() {
        Route::get('items/options', 'Logistics\ItemController@options');
        Route::get('items/takings', 'Logistics\ItemController@takings');
        Route::post('items/takings', 'Logistics\ItemController@storeTaking');
        Route::get('items/sales_informations/{type}/{sales_information_id}/{warehouse_id}/sellables', 'Logistics\ItemController@sellableBySalesInformation');
        Route::get('items/{id}/warehouses/{warehouse_id}', 'Logistics\ItemController@itemOnWarehouse');
        Route::apiResource('items', 'Logistics\ItemController');

        Route::get('purchases/orders/suppliers/{supplier_id}', 'Logistics\Purchases\OrderController@bySupplier');
        Route::post('purchases/orders/{id}/close', 'Logistics\Purchases\OrderController@close');
        Route::apiResource('purchases/orders', 'Logistics\Purchases\OrderController');
        Route::get('purchases/receives/orders/{order_id}', 'Logistics\Purchases\ReceiveController@byOrder');
        Route::apiResource('purchases/receives', 'Logistics\Purchases\ReceiveController');

        Route::get('mutations/purchase_orders/{purchase_invoice_id}', 'Logistics\MutationController@byPurchaseOrder');
        Route::get('mutations/logs', 'Logistics\MutationController@logs');
        Route::post('mutations/adjustments', 'Logistics\MutationController@storeAdjustment');
        Route::get('mutations/adjustments/{id}', 'Logistics\MutationController@showAdjustment');
        Route::apiResource('mutations', 'Logistics\MutationController');

        Route::apiResource('warehouse_mutations', 'Logistics\WarehouseMutationController');
        Route::get('warehouse_mutations/orders/{id}', 'Logistics\WarehouseMutationController@byPurchaseOrder');
    });


    Route::prefix('manufactures')->group(function () {
        Route::post('/work_orders/details/{id}/close', 'Manufactures\WorkOrderController@close');
        Route::get('/work_orders/details/{id}', 'Manufactures\WorkOrderController@showDetail');
        Route::get('/work_orders/options', 'Manufactures\WorkOrderController@options');
        Route::apiResource('/work_orders', 'Manufactures\WorkOrderController');

        Route::get('/materials/{work_order_detail_id}/wodet', 'Manufactures\MaterialController@showByWorkOrderDetail');
        Route::get('/materials/releases', 'Manufactures\MaterialController@releases');
        Route::post('/materials/releases', 'Manufactures\MaterialController@storeReleases');

        Route::get('/products/lines/{id}', 'Manufactures\ProductController@showByLine');
        Route::resource('/products', 'Manufactures\ProductController');
    });

    Route::prefix('malls')->group(function () {
        Route::apiResource('/products', 'Malls\ProductController');
        Route::apiResource('/orders', 'Malls\OrderController');
    });

    Route::get('/collections/month/{month}/year/{year}', 'Whatsapp\CollectionController@getCollections');

    Route::post('/shipments/track', 'ShipmentController@track');
    Route::get('/outlets/options', 'OutletController@options');
    Route::get('/warehouses/options', 'WarehouseController@options');
    Route::get('/warehouses/{id}/stocks/{item_id}', 'WarehouseController@stock');
    Route::get('/uoms/options', 'UomController@options');
    Route::get('/suppliers/options', 'SupplierController@options');
    Route::get('/banks/options', 'BankController@options');
    Route::get('/users/options', 'UserController@options');
    Route::get('/products/options', 'ProductController@options');
    Route::get('/racks/options/{warehouse_id}', 'RackController@options');
    Route::apiResource('/anamnesas', 'AnamnesaController');

    Route::get('/users/outlets/options', 'UserController@outletOptions');

    Route::apiResource('/products', 'ProductController');

    Route::get('/customers', 'CustomerController@index');
    Route::get('/customers/queue', 'CustomerController@listQueueToday')->name('customers.queue');
	Route::post('/customers/goQueue', 'CustomerController@goQueue');

    Route::get('/customers/{id}/activities', 'CustomerController@activities')->name('patient.activities');
    Route::get('/customers/{id}/histories', 'CustomerController@histories')->name('patient.histories');
    Route::get('/customers/optioncountry', 'CustomerController@optioncountry');
	Route::get('/customers/optioncity/{country_id}', 'CustomerController@optionprovince');
	Route::get('/customers/optionoutlet', 'CustomerController@optionoutlet');
	Route::get('/customers/optionconsultant', 'CustomerController@optionconsultant');
	Route::get('/customers/optionsource', 'CustomerController@optionsource');
	Route::get('/customers/edit/{id}', 'CustomerController@editform');
	Route::post('/customers/store', 'CustomerController@store');
	Route::post('/customers/update/{id}', 'CustomerController@update');
	Route::delete('/customers/destroy/{id}', 'CustomerController@destroy');
    Route::get('/customers/{id}', 'CustomerController@show')->name('patient.show');
});

// old Neosoft POS routes.
Route::prefix('v1')->group(function () {


    Route::post('login', 'Api\AuthController@login')->name('login');
    // Send reset password mail
    Route::post('reset-password', 'Api\AuthController@sendPasswordResetLink');

    // handle reset password form process
    Route::post('reset/password', 'Api\AuthController@callResetPassword');

    Route::post('customer/create', 'Api\CustomerController@CustomerCreate');

    Route::get('email', 'Api\CustomerController@SendEmail');

    Route::get('email/verify/{id}', 'VerificationApiController@verify')->name('verification.verify');
    Route::get('email/resend', 'VerificationApiController@resend')->name('verification.resend');
    Route::get('location', 'Api\CustomerController@Location');

    Route::middleware(['customer-auth:customer'])->group(function () {
        Route::post('logout', 'Api\AuthController@logout')->name('logout');
        Route::prefix('customer')->group(function () {
            Route::get('invoice', 'Api\CustomerController@CustomerInvoice');
            Route::get('detail', 'Api\CustomerController@CustomerDetail');
            Route::get('past-usage', 'Api\CustomerController@CustomerPastUsage');
            Route::get('appointment', 'Api\CustomerController@CustomerAppointment');
            Route::post('update/{id}', 'Api\CustomerController@CustomerUpdate');
            Route::post('device/{id}', 'Api\CustomerController@CustomerDevice');
            Route::get('invoice', 'Api\CustomerController@ActiveInvoice');
        });

        Route::prefix('product')->group(function () {
            Route::get('all', 'Api\ProductTreatmentController@allProduct');
            Route::post('redeem/{id}', 'Api\ProductTreatmentController@redeemProduct');
            Route::get('my-redeem', 'Api\ProductTreatmentController@myRedeem');
        });


        Route::prefix('appointment')->group(function () {
            Route::get('/', 'Api\AppointmentController@AppointmentData');
            Route::get('detail', 'Api\AppointmentController@AppointmentDetail');
            Route::post('create', 'Api\AppointmentController@AppointmentCreate');
            Route::post('reschedule/{id}', 'Api\AppointmentController@AppointmentReschedule');
            Route::post('delete/{id}', 'Api\AppointmentController@cancelAppointment');
        });
    });
});

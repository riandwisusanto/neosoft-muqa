Mustache.tags = ['{%', '%}'];

$(function(){
    // Mengubah data pada input type hidden ke dalam javascript array.
    var save_json = JSON.parse($('#save_json').val());

    // Fungsi render digunakan untuk merender data dan template mustache ke dalam elemen yg sudah ditentukan
    function render()
    {
        // Memanggil fungsi getIndex sebelum ditampilkan
        getIndex();
        // Mengubah list array ke dalam bentuk data json lalu dimasukan ke dalam input type hidden
        $('#save_json').val(JSON.stringify(save_json));

        // Mendefinisikan template
        var tmpl = $('#add_item').html();
        // parse template ke dalam mustache template
        Mustache.parse(tmpl);
        // Merender dengan template dan data
        // variable 'item' adalah variable yang akan dilooping
        var html = Mustache.render(tmpl, { item : save_json });
        // mengisi html tbody dengan hasil renderan di atas
        $('#render').html(html);
        /*});*/

        // memanggil fungsi bind
        bind();
    }

    // PENTING. fungsi bind digunakan untuk memberikan event listener kepada elemen html setelah render selesai. Jika tidak dilakukan bind maka event pada elemen tidak akan berjalan.
    function bind(){
        // Memberikan event listener pada tombol pada saat tombol diklik
        $('select:not(.normal)').each(function () {
            $(this).select2({
                placeholder: "Please Select",
                width: 'resolve',
                dropdownParent: $(this).parent()
            });
        });

        var save_json = JSON.parse($('#save_json').val());
        let grandtotal = [];
        for (let i = 0; i < save_json.length; i++) {
            grandtotal.push(Number(remove_format(save_json[i].subtotal)));
        }

        let total = grandtotal.reduce((a,b) => a + b, 0)
        $('#total').val(format_money(total));

        chooseProduct();
        chooseProductWithId();
        $('.edit').on('click', chooseProductWithId);
        $('.edit').on('click', edit);

        $('#AddItem').on('click', add);
        $('.delete').on('click', delete_item);
        $('.cancel').on('click', canceledit);
        $('.save').on('click', saveedit);
    }

    // Fungsi add untuk menambah item ke dalam list
    function add()
    {

        // Membaca nilai dari inputan
        var product = $('#product option:selected').text();
        var choose_product = $('#product').val();
        var product_id = $('#product_id').val();
        var qty = $('#qty').val();
        var code = $('#code').text();
        var point = $('#point').text();
        var subtotal = $('#sub_total').val();
        var current_point = $('#current_point').val();

        var data = JSON.parse($('#save_json').val());
        let cek = 0;
        for (let i = 0; i < data.length; i++) {
            if (data[i].product_id == product_id) {
                cek++;
            }
        }

        if (cek < 1) {
            if (product && subtotal  && qty != 0) {
                var input = {
                    'product_id' : product_id,
                    'choose_product' : choose_product,
                    'product' : product,
                    'qty' : qty,
                    'code' : code,
                    'point' : point,
                    'subtotal' : subtotal,
                    'current_point' : current_point,
                };

                // Memasukan object ke dalam array
                save_json.push(input);
                // Merender ulang karena data sudah berubah
                render();
            }else{
                swal("Data is Empty", {
                    button: false,
                });
            }
        }else{
            swal("Product has been Added", {
                button: false,
            });
        }
    }


    // fungsi edit untuk menampilkan form edit
    function edit()
    {
        // Mengambil id item yang akan dihapus
        var i = parseInt($(this).data('id'), 10);

        let subtotal = $('#sub_total_'+i).val();
        $('#sub_total_'+i).val(subtotal).val();

        let choose_product = $('#choose_product_'+i).val();
        $('#product_'+i).val(choose_product).trigger('change');


        let activity = $('#activity_id_'+i).val();
        $('#activity_'+i).val(activity).trigger('change');

        let type_line = $('#type_line_id_'+i).val();
        $('#type_line_'+i).val(type_line).trigger('change');

        var row_data = $("#data_" + i);
        var row_form = $("#form_" + i);
        var input_data = $("#input_data");


        // menyembunyikan input form
        if(!input_data.hasClass('hidden')){
            input_data.addClass('hidden');
        }

        // menyembunyikan baris data
        if(!row_data.hasClass('hidden')){
            row_data.addClass('hidden');
        }

        // menampilkan baris form
        if(row_form.hasClass('hidden')){
            row_form.removeClass('hidden');
        }
    }

    // fungsi edit untuk menyembunyikan form edit
    function canceledit()
    {
        render();
    }

    function saveedit()
    {
        // Mengambil id item yang akan dihapus
        var i = parseInt($(this).data('id'), 10);

        // Membaca nilai dari inputan
        var product = $('#product_'+ i +' option:selected').text();
        var type_line = $('#type_line_' + i +' option:selected').text();

        var product_id = $('#product_idx_' + i).val();

        var choose_product = $('#product_'+ i).val();

        var type_line_id = $('#type_line_' + i).val();
        var qty = $('#qty_' + i).val();
        var code = $('#code_'+ i).text();
        var point = $('#point_'+ i).text();
        var subtotal = $('#sub_total_x_' + i).val();
        var current_point = $('#current_point_' + i).val();

        // Menyimpan data pada list
        save_json[i].product_id = product_id;
        save_json[i].choose_product = choose_product;
        save_json[i].type_line_id = type_line_id;
        save_json[i].product = product;
        save_json[i].type_line = type_line;
        save_json[i].qty = qty;
        save_json[i].code = code;
        save_json[i].point = point;
        save_json[i].subtotal = subtotal;
        save_json[i].current_point = current_point;

        // Merender kembali karena data sudah berubah
        render();
    }

    // Fungsi delete_phone digunakan untuk menghapus elemen
    function delete_item(){
        // Mengambil id item yang akan dihapus
        var i = parseInt($(this).data('id'), 10);

        // menghapus list dari elemen array
        save_json.splice(i, 1);

        // Merender kembali karena data sudah berubah
        render();
    }

    // Fungsi getIndex digunakan untuk membuat penomoran dan id unik sebelum dirender
    function getIndex()
    {
        for (idx in save_json) {
            // setting _id digunakan untuk memudahkan dalam mengedit dan menghapus list, seperti id pada tabel dalam database.
            save_json[idx]['_id'] = idx;
            // setting no digunakan untuk penomoran
            save_json[idx]['no'] = parseInt(idx) + 1;
        }
    }

    function chooseProduct() {
        $('#product').change(function () {
            $.ajax({
                url: "productpoints/"+$(this).val()+"/chooseProduct",
                type: "GET",
                dataType: "JSON",
                beforeSend: function (xhr) {
                    $("#AddItem").attr("disabled",true);
                },
                success: function (data) {
                    if (data.id_product_point) {
                        $('#product_id').val(data.id_product_point);
                    }else{
                        $('#product_id').val('');
                    }

                    $('#code').text(data.product_point_code);
                    $('#point').text(data.point);
                    $('#current_point').val(data.point);
                    $("#AddItem").removeAttr("disabled", false);
                },
                error: function () {
                    alert("Can not Delete the Data!");
                }
            });
        });
    }

    function chooseProductWithId() {
        var i = parseInt($(this).data('id'), 10);
        $('#product_'+i).change(function () {
            $.ajax({
                url: "productpoints/"+$(this).val()+"/chooseProduct",
                type: "GET",
                dataType: "JSON",
                beforeSend: function (xhr) {
                    $("#AddItem").attr("disabled",true);
                    $("#simpan").attr("disabled",true);
                },
                success: function (data) {
                    if (data.id_product) {
                        $('#product_idx_'+i).val(data.id_product);
                    }else{
                        $('#product_idx_'+i).val('');
                    }

                    $('#code_'+i).text(data.product_point_code);
                    $('#point_'+i).text(data.point);
                    $('#current_point_'+i).val(data.point);
                    $('#qty_'+i).val(1);
                    changeTypeWithId(i);
                    $("#AddItem").removeAttr("disabled", false);
                    $("#simpan").removeAttr("disabled", false);
                    //$('#sub_total_x_'+i).val(data.point);
                },
                error: function () {
                    alert("Can not Delete the Data!");
                }
            });
        });

    }
    // Memanggil fungsi render pada saat halaman pertama kali dimuat
    render();
});

Mustache.tags = ['{%', '%}'];

$(function () {
    // Mengubah data pada input type hidden ke dalam javascript array.
    var save_json = JSON.parse($('#save_json').val());

    // Fungsi render digunakan untuk merender data dan template mustache ke dalam elemen yg sudah ditentukan
    function render() {
        // Memanggil fungsi getIndex sebelum ditampilkan
        getIndex();
        // Mengubah list array ke dalam bentuk data json lalu dimasukan ke dalam input type hidden
        $('#save_json').val(JSON.stringify(save_json));

        // Mendefinisikan template
        var tmpl = $('#add_item').html();
        // parse template ke dalam mustache template
        Mustache.parse(tmpl);
        // Merender dengan template dan data
        // variable 'item' adalah variable yang akan dilooping
        var html = Mustache.render(tmpl, {
            item: save_json
        });
        // mengisi html tbody dengan hasil renderan di atas
        $('#render').html(html);
        /*});*/

        // memanggil fungsi bind
        bind();
    }

    // PENTING. fungsi bind digunakan untuk memberikan event listener kepada elemen html setelah render selesai. Jika tidak dilakukan bind maka event pada elemen tidak akan berjalan.
    function bind() {
        // Memberikan event listener pada tombol pada saat tombol diklik
        $('select:not(.normal)').each(function () {
            $(this).select2({
                placeholder: "Please Select",
                width: 'resolve',
                dropdownParent: $(this).parent()
            });
        });

        var save_json = JSON.parse($('#save_json').val());
        let grandtotal = [];
        for (let i = 0; i < save_json.length; i++) {

            grandtotal.push(Number(remove_format(save_json[i].price)));
        }
        
        let total = grandtotal.reduce((a,b) => a + b, 0)
        $('#packprice').val(format_money(total));

        chooseProduct();
        chooseProductwithId();
        $('.edit').on('click', chooseProductwithId);

        $('#AddItem').on('click', add);
        $('.delete').on('click', delete_item);
        $('.edit').on('click', edit);
        $('.cancel').on('click', canceledit);
        $('.save').on('click', saveedit);
    }

    function chooseProduct() {
        $('#type_id').change(function () {
            $.ajax({
                url: "/customers/" + $(this).val() + "/chooseProduct",
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    $('#price').val(data.price);
                    $('#current_price').val(data.price);
                    $('#qty').val(1);
                },
                error: function () {
                    alert("Can not Delete the Data!");
                }
            });
        });
    }

    function chooseProductwithId() {
        var i = parseInt($(this).data('id'), 10);

        $('#type_idx_' + i).change(function () {
            $.ajax({
                url: "/customers/" + $(this).val() + "/chooseProduct",
                type: "GET",
                dataType: "JSON",
                success: function (data) {
                    $('#price_' + i).val(data.price);
                    $('#current_price_' + i).val(data.price);
                    $('#qty_' + i).val(1);
                },
                error: function () {
                    alert("Can not Delete the Data!");
                }
            });
        });

    }


    // Fungsi add untuk menambah item ke dalam list
    function add() {
        // Membaca nilai dari inputan
        var type_name = $('#type_id option:selected').text();


        var type_id = $('#type_id').val();
        var qty = $('#qty').val();
        var price = $('#price').val();
        var current_price = $('#current_price').val();

        //console.log(totalprice);

        if (type_name) {
            var input = {
                'type_name': type_name,
                'type_id': type_id,
                'qty': qty,
                'price': price,
                'current_price': current_price,
            };


            // Memasukan object ke dalam array
            save_json.push(input);

            // Merender ulang karena data sudah berubah

            render();
        } else {
            alert('Data is Empty');
        }

        // Membuat object yg akan dimasukan kedalam array

    }



    // fungsi edit untuk menampilkan form edit
    function edit() {
        // Mengambil id item yang akan dihapus
        var i = parseInt($(this).data('id'), 10);

        let type_id = $('#type_id_' + i).val();
        $('#type_idx_' + i).val(type_id).trigger('change');

        var row_data = $("#data_" + i);
        var row_form = $("#form_" + i);
        var input_data = $("#input_data");


        // menyembunyikan input form
        if (!input_data.hasClass('hidden')) {
            input_data.addClass('hidden');
        }

        // menyembunyikan baris data
        if (!row_data.hasClass('hidden')) {
            row_data.addClass('hidden');
        }

        // menampilkan baris form
        if (row_form.hasClass('hidden')) {
            row_form.removeClass('hidden');
        }
    }

    // fungsi edit untuk menyembunyikan form edit
    function canceledit() {
        render();
    }

    function saveedit() {
        // Mengambil id item yang akan dihapus
        var i = parseInt($(this).data('id'), 10);

        // Membaca nilai dari inputan
        var type_id = $('#type_idx_' + i).val();
        var type_name = $('#type_idx_' + i + ' option:selected').text();
        var qty = $('#qty_' + i).val();
        var price = $('#price_' + i).val();
        var current_price = $('#current_price_' + i).val();

        // Menyimpan data pada list
        save_json[i].type_id = type_id;
        save_json[i].type_name = type_name;
        save_json[i].qty = qty;
        save_json[i].price = price;
        save_json[i].current_price = current_price;

        // Merender kembali karena data sudah berubah
        render();
    }

    // Fungsi delete_phone digunakan untuk menghapus elemen
    function delete_item() {
        // Mengambil id item yang akan dihapus
        var i = parseInt($(this).data('id'), 10);

        // menghapus list dari elemen array
        save_json.splice(i, 1);

        // Merender kembali karena data sudah berubah
        render();
    }

    // Fungsi getIndex digunakan untuk membuat penomoran dan id unik sebelum dirender
    function getIndex() {
        for (idx in save_json) {
            // setting _id digunakan untuk memudahkan dalam mengedit dan menghapus list, seperti id pada tabel dalam database.
            save_json[idx]['_id'] = idx;
            // setting no digunakan untuk penomoran
            save_json[idx]['no'] = parseInt(idx) + 1;
        }
    }

    // Memanggil fungsi render pada saat halaman pertama kali dimuat
    render();
});
